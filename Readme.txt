Map Editor of Dreams Beta v0.99
===============================

Key Info
========

Use Middle Mouse Button to scroll in graphic windows.
Shift + Mouse Wheel Up - Zoom In.
Shift + Mouse Wheel Down - Zoom Out.

Mouse Wheel Up / Page Up - Scroll Up.
Mouse Wheel Down / Page Down - Scroll Down.

Ctrl + Mouse Wheel Up / Page Up - Scroll Left.
Ctrl + Mouse Wheel Down / Page Down - Scroll Right.

Arrows - scroll a little.

Ctrl + Arrows - move selected cells.


Multiple selection mode:

Use Shift to add selection.
Use Alt to subtract selection.
Use Ctrl to cut selected contents.      
Use Ctrl+Alt to copy selected contents.


On map editing window:


When Pencil Tool or Single Select Tool are active, use Alt to quick tile pick.


Use Right Mouse Button to erase when Pencil or Brush Tool are active.


Use Ctrl to do drawing actions on focused layers instead of checked (Drawing enabled value is True) layers.


When Picker tool is active:

Use Left Mouse Button to pick tile from checked layers.

Use Right Mouse Button to find tile on active tile set and to find a brush layer.
