unit me_tileprops;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, VirtualTrees, PropEditor, StdCtrls, TntStdCtrls, TntDialogs,
  TileMapView, BitmapEx, TilesEx, PropContainer,
  me_project, me_palfrm, me_tilesfrm, me_tilesetpropsfrm;

const
 WM_FILLTILEPROPS =  WM_USER + 997;

type
  TTilePropsFrame = class;

  TTileRemovedEvent = procedure(Sender: TTilePropsFrame; ATile: TTileItem) of object;
  TTilePropsFrame = class(TFrame)
    SelTilePropLabel: TTntLabel;
    SelTilePropEditor: TPropertyEditor;
  private
    FPosted: Boolean;
    FOnRefreshGraphicContent: TNotifyEvent;
    FOnDataUpdated: TNotifyEvent;
    procedure WMFillTileProps(var Message: TMessage); message WM_FILLTILEPROPS;
    procedure ShowTileProperties(TileSet: TTileSet;
     TileItem: TTileItem; const VFlags: Int64);
    procedure TileValueChange(Sender: TPropertyListItem);
    procedure RefreshGraphicContent;
    procedure DataUpdated;        
  public
    PaletteFrame: TPaletteFrame;
    Project: TMapEditorProject;

    constructor Create(AOwner: TComponent); override;
    procedure ShowSelectedTileProps(Frame: TTilesFrame);
    procedure ShowTileProps(TileSet: TTileSet; TileIndex: Integer);

    property OnRefreshGraphicContent: TNotifyEvent
             read FOnRefreshGraphicContent
            write FOnRefreshGraphicContent;
    property OnDataUpdated: TNotifyEvent read FOnDataUpdated write FOnDataUpdated;
  end;

implementation

{$R *.dfm}

uses
 HexUnit, MyUtils, MyClasses;

{ TTilePropsFrame }

constructor TTilePropsFrame.Create(AOwner: TComponent);
begin
 inherited;
 SelTilePropEditor.PropertyList.OnValueChange := TileValueChange;
end;

procedure TTilePropsFrame.RefreshGraphicContent;
begin
 if Assigned(FOnRefreshGraphicContent) then
  FOnRefreshGraphicContent(Self);
end;

procedure TTilePropsFrame.ShowSelectedTileProps(Frame: TTilesFrame);
begin
 if not FPosted and
 ((Frame.Owner as TTileSetsFrame).PageControl.ActivePageIndex <> 0) then
 begin
  FPosted := True;
  PostMessage(Handle, WM_FILLTILEPROPS, Integer(Frame), 0);
 end;
end;

procedure TTilePropsFrame.ShowTileProperties(TileSet: TTileSet;
  TileItem: TTileItem; const VFlags: Int64);
var
 I: Integer;
 Prop: TPropertyListItem;
 AttrPtr: PVariant;
 V: Variant;
 Int: Int64;
 Float: Double;
 Bool: Boolean;
begin
 if TileSet <> nil then
  with SelTilePropEditor.PropertyList do
  begin
   if (TileItem <> TileSet.EmptyTile) or (VFlags <> 0) then
    for I := 0 to TileSet.AttrCount - 1 do
     with TileSet.AttrDefs[I]^ do
     begin
      Prop := nil;
      if adType and varArray <> 0 then
      begin
       Prop := AddString(adName, GetArrayString(Addr(adSelf), TileItem.Attributes[I]));
      end else
      begin
       AttrPtr := Addr(TileItem.Attributes[I]);
       if adEnum = nil then
       begin
        if VarIsArray(AttrPtr^) then
         V := AttrPtr^[VarArrayLowBound(AttrPtr^, 1)] else
         V := AttrPtr^;
        case adType and varTypeMask of
         varSmallInt,
         varInteger,
         varShortInt,
         varByte,
         varWord,
         varLongWord,
         varInt64:
         begin
          try
           Int := V;
          except
           Int := 0;
          end;
          if adNumSys = nsHexadecimal then
           Prop := AddHexadecimal(adName, Int, adMin, adMax, 2) else
           Prop := AddDecimal(adName, Int, adMin, adMax);
         end;
         varBoolean:
         begin
          try
           Bool := V;
          except
           Bool := False;
          end;
          Prop := AddBoolean(adName, Bool);
         end;
         varSingle,
         varDouble,
         varCurrency:
         begin
          try
           Float := V;
          except
           Float := 0;
          end;
          Prop := AddFloat(adName, Float, adPrecision);
         end;
         varOleStr,
         varString:
          Prop := AddString(adName, V);
        end;
       end else
        Prop := AddPickList(adName, adEnum,
             FindEnumMember(adEnum, AttrPtr^));
      end;

      if Prop <> nil then
      begin
       if VFlags and (Int64(1) shl I) <> 0 then
        with Prop do
         Parameters := Parameters or PL_MULTIPLE_VALUE;
       Prop.UserTag := I;
      end;
     end;
  end;
end;

procedure AttrModified(Sender: TPropertyListItem; History: THistorySet;
                        TileSet: TTileSet; Item: TTileItem;
                        const TempEnum: TWideStringArray);
var
 I, L, Idx: Integer;
 MemberIdx: array of Integer;
 AttrPtr: ^Variant;
 vtype: Word;
 Event: THistoryEvent;
 AStr: WideString;
const
 CStr: WideString = 'Tile set ''%s'', tile #%d: %s = %s';
begin
 if Item <> TileSet.EmptyTile then
 begin
  AStr := WideFormat('Attribute #%d ''%s''',
  [Sender.UserTag + 1, TileSet.AttrDefs[Sender.UserTag].adName]);
  case Sender.ValueType of
   vtDecimal, vtHexadecimal:
   begin
    Event := History.AddEvent(Item,
     WideFormat(CStr, [TileSet.Name, Item.TileIndex, AStr,
      IntToStr(Sender.Value)]));

    Item.Attributes[Sender.UserTag] := Sender.Value;
    Event.RedoData := Item;
   end;
   vtString: with TileSet.AttrDefs[Sender.UserTag]^ do
   begin
    if adType and varArray <> 0 then
    begin
     L := Min(adMaxArrayLen, Length(TempEnum));

     if adEnum <> nil then
     begin
      SetLength(MemberIdx, L);
      for I := 0 to L - 1 do
      begin
       Idx := FindEnumMember(TempEnum[I], adEnum);
       if Idx < 0 then
        Exit else
        MemberIdx[I] := Idx;
      end;
     end;

     Event := History.AddEvent(Item, '');

     AttrPtr := Addr(Item.Attributes[Sender.UserTag]);

     vtype := adType and varTypeMask;
     if not VarTypeIsValidArrayType(vtype) then
      vtype := varVariant;

     AttrPtr^ := VarArrayCreate([0, L - 1], vtype);

     if adEnum <> nil then
     begin
      for I := 0 to L - 1 do
      try
       AttrPtr^[I] := MemberIdx[I];
      except
       AttrPtr^[I] := Unassigned;
      end;
     end else
     begin
      for I := 0 to L - 1 do
      try
       AttrPtr^[I] := TempEnum[I];
      except
       AttrPtr^[I] := Unassigned;
      end;
     end;

     Sender.Changing := True;
     Sender.ValueStr := GetArrayString(Addr(adSelf), AttrPtr^);
     Sender.Changing := False;

     Event.Text :=
       WideFormat(CStr, [TileSet.Name, Item.TileIndex, AStr, Sender.ValueStr]);
    end else
    begin
     Event := History.AddEvent(Item, WideFormat(CStr,
      [TileSet.Name, Item.TileIndex, AStr, '''' + Sender.ValueStr + '''']));
     Item.Attributes[Sender.UserTag] := Sender.ValueStr;
    end;

    Event.RedoData := Item;
   end;
   vtPickString:
   begin
    if Sender.Parameters and PL_FIXEDPICK <> 0 then
    begin
     Event := History.AddEvent(Item,
      WideFormat(CStr, [TileSet.Name, Item.TileIndex, AStr, Sender.ValueStr]));
     Item.Attributes[Sender.UserTag] := Sender.PickListIndex;
    end else
    begin
     Event := History.AddEvent(Item,
      WideFormat(CStr, [TileSet.Name, Item.TileIndex, AStr, '''' + Sender.ValueStr + '''']));
     Item.Attributes[Sender.UserTag] := Sender.ValueStr;
    end;
    Event.RedoData := Item;
   end;
   vtFloat:
   begin
    Event := History.AddEvent(Item,
     WideFormat(CStr, [TileSet.Name, Item.TileIndex, AStr, Sender.ValueStr]));

    Item.Attributes[Sender.UserTag] := Sender.Float;
    Event.RedoData := Item;
   end;
  end;
 end;
end;

procedure TTilePropsFrame.TileValueChange(Sender: TPropertyListItem);
var
 Item: TTileItem;
 X, Y, Idx: Integer;
 Rect: TRect;
 TempEnum: TWideStringArray;
 obj: TObject;
 frm: TTilesFrame absolute obj;
 ts: TTileSet;
 st: TTileItem;
 ItemMode: Boolean;
 Event: THistoryEvent;
const
 CStrEmpty: WideString = 'Tile set ''%s'', tile #%d: Empty = %s';
 CStr1stCl: WideString = 'Tile set ''%s'', tile #%d: 1st color index = %d';

 CStrAllEmpty: WideString = 'Tile set ''%s'', selected tiles: Empty = %s';
 CStrAll1stCl: WideString = 'Tile set ''%s'', selected tiles: 1st color index = %d';
begin
 obj := TObject(Sender.Owner.UserTag);
 if obj is TTilesFrame then
 begin
  ts := frm.TileSet;
  if ts <> nil then
   st := ts.Indexed[frm.SelectedTile] else
   st := nil;
  ItemMode := False;
 end else
 begin
  st := obj as TTileItem;
  ts := st.Owner as TTileSet;
  ItemMode := True;
 end;
 if (ts <> nil) and (ItemMode or (
   (frm.TileSetView.SomethingSelected) and
   (frm.TileSetView.PasteBuffer = nil))) then
 begin
  case Sender.UserTag of
   -2:
   if ItemMode then
    Exit else
   if (frm.TileSetView.SelectionMode = selSingle) then
   begin
    if Sender.PickListIndex = 0 then
    begin
     Event := Project.History.AddEvent(ts,
        WideFormat(CStrEmpty, [ts.Name, frm.SelectedTile, 'False']));
     with ts do
     begin
      Item := AddFixed(frm.SelectedTile);
      Move(EmptyTile.TileData^, Item.TileData^, TileSize);
      Item.FirstColor := EmptyTile.FirstColor;
     end;
     Event.RedoData := ts;
     RefreshGraphicContent;
     PostMessage(Self.Handle, WM_FILLTILEPROPS, Integer(frm), 0)
    end else
    begin
     with ts do
     begin
      Item := Indexed[frm.SelectedTile];
      if Item <> EmptyTile then
      begin
       Event := Project.History.AddEvent(ts,
          WideFormat(CStrEmpty, [ts.Name, frm.SelectedTile, 'True']));
       Remove(Item);
       Event.RedoData := ts;       
      end;
     end;
     with Sender.Next as TPropertyListItem do
     begin
      Changing := True;
      Value := ts.EmptyTile.FirstColor;
      Changing := False;
     end;
     SelTilePropEditor.RootNodeCount := 2;
     RefreshGraphicContent;
    end;
    DataUpdated;
    Exit;
   end;
   -3:
   if ItemMode or (frm.TileSetView.SelectionMode = selSingle) then
   begin
    if ItemMode then
     Item := st else
     Item := ts.Indexed[frm.SelectedTile];
    Event := Project.History.AddEvent(ts,
     WideFormat(CStr1stCl, [ts.Name, Item.TileIndex, Sender.Value]));
    Item.FirstColor := Sender.Value;
    Event.RedoData := ts;
    if ts.DrawFlags and DRAW_PIXEL_MODIFY <> 0 then
     PaletteFrame.SelectColorRange(Item.FirstColor,
                                 1 shl ts.TileBitmap.BitsCount) else
     PaletteFrame.SelectColorRange(-1, 0);
    if not ItemMode then
     frm.TileSetView.ScrollToSelectedCell;
    RefreshGraphicContent;
    DataUpdated;
    Exit;
   end;
   else
   if ts.AttrDefs[Sender.UserTag].adType and varArray <> 0 then
   try
    GetEnumListFromString(Sender.ValueStr, TempEnum);
   except
    WideMessageDlg('Invalid array string', mtError, [mbOk], 0);
    if not ItemMode then
     PostMessage(Self.Handle, WM_FILLTILEPROPS, Integer(frm), 0) else
     PostMessage(Self.Handle, WM_FILLTILEPROPS, Integer(ts), st.Index);
    Exit;
   end;
  end;
  if ItemMode then
  begin
   if Sender.UserTag >= 0 then
   begin
    AttrModified(Sender, Project.History, ts, st, TempEnum);
    DataUpdated;
   end;
  end else
  with frm do
  begin
   Rect := TileSetView.SelectionRect;
   Rect.Left := Max(Rect.Left, 0);
   Rect.Right := Min(TileSetView.MapWidth, Rect.Right);
   Rect.Top := Max(Rect.Top, 0);
   Rect.Bottom := Min(TileSetView.MapHeight, Rect.Bottom);
   if (Rect.Right - Rect.Left > 0) and
      (Rect.Bottom - Rect.Top > 0) then
   begin
    case Sender.UserTag of
     -2: Event := Project.History.AddEvent(TileSet,
        WideFormat(CStrAllEmpty, [TileSet.Name,
         BoolToStr(Sender.PickListIndex <> 0, True)]));
     -3: Event := Project.History.AddEvent(TileSet,
           WideFormat(CStrAll1stCl, [TileSet.Name, Sender.Value]));
     else Event := nil;
    end;
    for Y := Rect.Top to Rect.Bottom - 1 do
    begin
     Idx := Y * TileSetView.MapWidth + Rect.Left;
     for X := Rect.Left to Rect.Right - 1 do
     begin
      if TileSetView.Selected[X, Y] then
      begin
       case Sender.UserTag of
        -2:
        with TileSet do
        begin
         if Sender.PickListIndex = 0 then
         begin
          Item := AddFixed(Idx);
          Move(EmptyTile.TileData^, Item.TileData^, TileSize);
          Item.FirstColor := EmptyTile.FirstColor;
         end else
         begin
          Item := TileSet.Indexed[Idx];
          if Item <> EmptyTile then
           Remove(Item);
         end;
        end;
        -3: TileSet.Indexed[Idx].FirstColor := Sender.Value;
        else
         AttrModified(Sender, Project.History, TileSet,
             TileSet.Indexed[Idx], TempEnum);
       end;
      end;
      Inc(Idx);
     end;
    end;
    if Event <> nil then
     Event.RedoData := TileSet;
   end;
   case Sender.UserTag of
    -2, -3:
    begin
     RefreshGraphicContent;
     PostMessage(Self.Handle, WM_FILLTILEPROPS, Integer(frm), 0);
    end;
   end;
   DataUpdated;
  end;
 end;
end;

procedure TTilePropsFrame.DataUpdated;
begin
 if Assigned(FOnDataUpdated) then
  FOnDataUpdated(Self);
end;

procedure TTilePropsFrame.WMFillTileProps(var Message: TMessage);
var
 UseTile, Tile, PrevTile: TTileItem;
 VFlags: Word;
 VFlags64: Int64;
 Rect: TRect;
 AllEmpty: Boolean;
 X, Y, I, Idx, Cnt: Integer;
 obj: TObject;
 ts: TTileSet;
 Something: Boolean;
begin
 FPosted := False;

 obj := TObject(Message.WParam);

 with SelTilePropEditor do
 begin
  ClearList;
  PropertyList.UserTag := 0;
  PaletteFrame.SelectColorRange(-1, 0);
  SelTilePropLabel.Caption := 'Tile properties';
  Something := False;  
  if obj <> nil then
  begin
   if obj is TTilesFrame then
   begin
    with TTilesFrame(obj) do
     if (TileSet <> nil) and
        TileSetView.SomethingSelected and
        (TileSetView.PasteBuffer = nil) then
     begin
      PropertyList.UserTag := Integer(obj);
      Rect := TileSetView.SelectionRect;
      Rect.Left   := Max(Rect.Left, 0);
      Rect.Right  := Min(TileSetView.MapWidth, Rect.Right);
      Rect.Top    := Max(Rect.Top, 0);
      Rect.Bottom := Min(TileSetView.MapHeight, Rect.Bottom);
      Something := (Rect.Right  - Rect.Left > 0) and
                   (Rect.Bottom - Rect.Top  > 0);
      if Something then
      begin
       if (Rect.Right - Rect.Left = 1) and
          (Rect.Bottom - Rect.Top = 1) then
        SelTilePropLabel.Caption := WideFormat('Tile set #%d, tile #%d',
                                               [TileSet.Index + 1,
                                                Rect.Top * TileSetView.MapWidth +
                                                Rect.Left]) else
        SelTilePropLabel.Caption := WideFormat('Tile set #%d, tiles ((%d, %d), (%d, %d))',
                                               [TileSet.Index + 1,
                                                Rect.Left,
                                                Rect.Top,
                                                Rect.Right - 1,
                                                Rect.Bottom - 1]);
      end;
     end;
   end else
   if obj is TTileSet then
   begin
    SelTilePropLabel.Caption := WideFormat('Tile set #%d, tile #%d',
                                           [TTileSet(obj).Index + 1,
                                            Message.lparam]);
    Something := True;
   end;

   if Something then
   begin
    VFlags := 0;
    VFlags64 := 0;
    UseTile := nil;
    PrevTile := nil;
    AllEmpty := True;
    Cnt := 0;    
    if obj is TTilesFrame then
    begin
     with TTilesFrame(obj) do
     begin
      ts := TileSet;
      for Y := Rect.Top to Rect.Bottom - 1 do
      begin
       Idx := Y * TileSetView.MapWidth + Rect.Left;
       for X := Rect.Left to Rect.Right - 1 do
       begin
        if TileSetView.Selected[X, Y] then
        begin
         Inc(Cnt);
         Tile := ts.Indexed[Idx];
         if Tile <> ts.EmptyTile then
          AllEmpty := False;

         if PrevTile <> nil then
         begin
          if (Tile = ts.EmptyTile) <> (PrevTile = ts.EmptyTile) then
           VFlags := VFlags or 2;
          if Tile.FirstColor <> PrevTile.FirstColor then
           VFlags := VFlags or 4;
          for I := 0 to ts.AttrCount - 1 do
           if not CompareVariants(Tile.Attributes[I], PrevTile.Attributes[I]) then
//           if Tile.Attributes[I] <> PrevTile.Attributes[I] then
            VFlags64 := VFlags64 or (1 shl I);
         end;
         if Tile <> ts.EmptyTile then
          UseTile := Tile;
         PrevTile := Tile;
        end;
        Inc(Idx);
       end;
      end;
     end;
    end else
    begin
     ts := TTileSet(obj);
     Tile := ts.Indexed[Message.lparam];
     if Tile <> ts.EmptyTile then
     begin
      PropertyList.UserTag := Integer(Tile);
      AllEmpty := False;
      UseTile := Tile;
     end;
    end;

    if UseTile = nil then
     UseTile := PrevTile;

    if UseTile <> nil then
    begin

     if AllEmpty and (Cnt > 1) then
      VFlags64 := -1;

     if obj <> ts then
      with PropertyList.AddBoolean('Empty', UseTile = ts.EmptyTile) do
      begin
       if VFlags and 2 <> 0 then
        Parameters := Parameters or PL_MULTIPLE_VALUE;
       UserTag := -2;
      end;
     with PropertyList.AddDecimal('1st color index', UseTile.FirstColor, 0, 255) do
     begin
      if VFlags and 4 <> 0 then
      begin
       Parameters := Parameters or PL_MULTIPLE_VALUE;
       PaletteFrame.SelectColorRange(-1, 0);
      end else
      if ts.DrawFlags and DRAW_PIXEL_MODIFY <> 0 then
       PaletteFrame.SelectColorRange(UseTile.FirstColor,
                          1 shl ts.TileBitmap.BitsCount) else
       PaletteFrame.SelectColorRange(-1, 0);                            
      UserTag := -3;
     end;
     if (UseTile <> ts.EmptyTile) or (VFlags and 2 <> 0) then
      ShowTileProperties(ts, UseTile, VFlags64);
    end;
   end;
  end;

  RootNodeCount := PropertyList.Count;
 end;
end;

procedure TTilePropsFrame.ShowTileProps(TileSet: TTileSet; TileIndex: Integer);
begin
 PostMessage(Handle, WM_FILLTILEPROPS, Integer(TileSet), TileIndex);
end;

end.
