unit me_plugadapters;

interface

uses
 Windows, SysUtils, Classes, TntClasses, Dialogs, NodeLst, MyClasses,
 MyClassesEx, PropContainer, BitmapEx, TilesEx, me_lib, me_plugin;

type
 TCustomAdapter = class(TInterfacedObject, IRoot)
  protected
    FSource: TObject;  
    FFreeAfterUse: Boolean;
    procedure Initialize; virtual;
  public
    function GetPtr: Pointer; stdcall;
    procedure FreeObject; stdcall;

    constructor Create(Source: TObject; FreeAfterUse: Boolean = False);
    destructor Destroy; override;
 end;

 TStreamClass = class of TStream;

 TStreamAdapter = class(TCustomAdapter, IStream32, IStream64)
  public    
    function Read(var Buffer; Count: LongInt): LongInt; stdcall;
    function Write(const Buffer; Count: LongInt): LongInt; stdcall;
    function GetPos: LongInt; stdcall;
    function GetPos64: Int64; stdcall;
    function IStream64.GetPos = GetPos64;
    function GetSize: LongInt; stdcall;
    function GetSize64: Int64; stdcall;
    function IStream64.GetSize = GetSize64;
    procedure SetPos(Value: LongInt); stdcall;
    procedure SetPos64(const Value: Int64); stdcall;
    procedure IStream64.SetPos = SetPos64;
    procedure SetSize(Value: LongInt); stdcall;
    procedure SetSize64(const Value: Int64); stdcall;
    procedure IStream64.SetSize = SetSize64;
    procedure Seek(Offset, SeekOrigin: LongInt); stdcall;
    procedure Seek64(const Offset: Int64; SeekOrigin: LongInt); stdcall;
    procedure IStream64.Seek = Seek64;
    function CopyFrom(const Stream: IReadWrite; Count: LongInt): LongInt;
      stdcall;
    function CopyFrom64(const Stream: IReadWrite;
      const Count: Int64): Int64; stdcall;
    function IStream64.CopyFrom = CopyFrom64;

    constructor Create(Source: TStream; FreeAfterUse: Boolean = False); overload;
    constructor Create(AClass: TStreamClass); overload;
 end;

 TMemoryStreamAdapter = class(TStreamAdapter, IMemoryStream)
    function GetMemory: Pointer;
    procedure Clear; stdcall;
    constructor Create(Source: TCustomMemoryStream; FreeAfterUse: Boolean = False);    
 end;

 TStrListRec = record
  case Integer of
   0: (List:  TObject);
   1: (ListA: TStringList);
   2: (ListW: TTntStringList);
 end;

 TStringsAdapter = class(TCustomAdapter, IStringList, IWideStringList)
  private
    FTempA: AnsiString;
    FTempW: WideString;
  public
    function GetString(Index: LongInt): PAnsiChar; stdcall;
    function GetStringW(Index: LongInt): PWideChar; stdcall;
    function IWideStringList.GetString = GetStringW;
    procedure SetString(Index: LongInt; Value: PAnsiChar); stdcall;
    procedure SetStringW(Index: LongInt; Value: PWideChar); stdcall;
    procedure IWideStringList.SetString = SetStringW;
    function GetText: PAnsiChar; stdcall;
    function GetTextW: PWideChar; stdcall;
    function IWideStringList.GetText = GetTextW;
    procedure SetText(Value: PAnsiChar); stdcall;
    procedure SetTextW(Value: PWideChar); stdcall;
    procedure IWideStringList.SetText = SetTextW;

    function Add(S: PAnsiChar): LongInt; stdcall;
    function AddW(S: PWideChar): LongInt; stdcall;
    function IWideStringList.Add = AddW;
    procedure Append(S: PAnsiChar); stdcall;
    procedure AppendW(S: PWideChar); stdcall;
    procedure IWideStringList.Append = AppendW;
    function Find(S: PAnsiChar; var Index: LongInt): Boolean; stdcall;
    function FindW(S: PWideChar; var Index: LongInt): Boolean; stdcall;
    function IWideStringList.Find = FindW;
    function IndexOf(S: PAnsiChar): LongInt; stdcall;
    function IndexOfW(S: PWideChar): LongInt; stdcall;
    function IWideStringList.IndexOf = IndexOfW;
    procedure Insert(Index: LongInt; S: PAnsiChar); stdcall;
    procedure InsertW(Index: LongInt; S: PWideChar); stdcall;
    procedure IWideStringList.Insert = InsertW;
    procedure Delete(Index: LongInt); stdcall;
    procedure Exchange(Index1, Index2: LongInt); stdcall;
    procedure Move(CurIndex, NewIndex: LongInt); stdcall;
    procedure Sort; stdcall;
    procedure AddStrings(const Strings: IStrings); stdcall;
    procedure Assign(const Source: IStrings); stdcall;
    function Equals(const Strings: IStrings): Boolean; stdcall;
    procedure LoadFromStream(const Stream: IReadWrite); stdcall;
    procedure SaveToStream(const Stream: IReadWrite); stdcall;
    procedure LoadFromFile(FileName: PWideChar); stdcall;
    procedure SaveToFile(FileName: PWideChar); stdcall;    
    function GetCount: LongInt; stdcall;
    procedure Clear; stdcall;

    constructor Create; overload;
    constructor Create(Source: TTntStringList; FreeAfterUse: Boolean = False); overload;
    constructor Create(Source: TStringList; FreeAfterUse: Boolean = False); overload;
 end;

 TIniFileAdapter = class(TCustomAdapter, IIniFile)
   private
    FTemp: WideString;
    function GetFileName: PWideChar; stdcall;
   public
    procedure LoadFromStream(const Stream: IReadWrite); stdcall;
    procedure SaveToStream(const Stream: IReadWrite); stdcall;
    procedure LoadFromFile(FileName: PWideChar); stdcall;
    procedure SaveToFile(FileName: PWideChar); stdcall;
    procedure SetStrings(const List: IWideStringList); stdcall;
    procedure WriteString(Section, Ident, Value: PWideChar); stdcall;
    function ReadString(Section, Ident, Default: PWideChar): PWideChar; stdcall;
    function ReadInteger(Section, Ident: PWideChar; Default: Longint): Longint; stdcall;
    function ReadInt64(Section, Ident: PWideChar; const Default: Int64): Int64; stdcall;    
    procedure WriteInteger(Section, Ident: PWideChar; Value: Longint; HexDigits: Byte); stdcall;
    procedure WriteInt64(Section, Ident: PWideChar; const Value: Int64; HexDigits: Byte); stdcall;
    function ReadBool(Section, Ident: PWideChar; Default: Boolean): Boolean; stdcall;
    procedure WriteBool(Section, Ident: PWideChar; Value: Boolean); stdcall;
    function ReadBinaryStream(Section, Name: PWideChar; const Value: IReadWrite): LongInt; stdcall;
    function ReadDate(Section, Name: PWideChar; const Default: TDateTime): TDateTime; stdcall;
    function ReadDateTime(Section, Name: PWideChar; const Default: TDateTime): TDateTime; stdcall;
    function ReadFloat(Section, Name: PWideChar; const Default: Double): Double; stdcall;
    function ReadTime(Section, Name: PWideChar; const Default: TDateTime): TDateTime; stdcall;
    procedure WriteBinaryStream(Section, Name: PWideChar; const Value: IReadWrite); stdcall;
    procedure WriteDate(Section, Name: PWideChar; const Value: TDateTime); stdcall;
    procedure WriteDateTime(Section, Name: PWideChar; const Value: TDateTime); stdcall;
    procedure WriteFloat(Section, Name: PWideChar; const Value: Double); stdcall;
    procedure WriteTime(Section, Name: PWideChar; const Value: TDateTime); stdcall;
    procedure ReadSection(Section: PWideChar; const Strings: IWideStringList); stdcall;
    procedure ReadSections(const Strings: IWideStringList); stdcall;
    procedure ReadSectionValues(Section: PWideChar; const Strings: IWideStringList); stdcall;
    procedure EraseSection(Section: PWideChar); stdcall;
    procedure DeleteKey(Section, Ident: PWideChar); stdcall;
    function ValueExists(Section, Ident: PWideChar): Boolean; stdcall;
    function SectionExists(Section: PWideChar): Boolean; stdcall;
    
    constructor Create(Source: TStreamIniFileW; FreeAfterUse: Boolean = False);
    destructor Destroy; override;
 end;

 TNodeAdapter = class;
 TNodeAdapterClass = class of TNodeAdapter;

 TNodeAdapter = class(TCustomAdapter, INode)
  public
    function GetUserTag: LongInt; stdcall;
    procedure SetUserTag(Value: LongInt); stdcall;

    function GetOwner: INode; stdcall;
    function GetNext: INode; stdcall;
    function GetPrev: INode; stdcall;

    function GetIndex: LongInt; stdcall;
 //public
    procedure Assign(const Source: INode); stdcall;
    procedure Changed; stdcall;
 end;

 TNodeListAdapter = class(TNodeAdapter, INodeList)
  protected
    FNodeAdapterClass: TNodeAdapterClass;
  public
    function GetCount: LongInt; stdcall;
    procedure SetCount(Value: LongInt); stdcall;
    function GetMaxCount: LongInt; stdcall;
    procedure SetMaxCount(Value: LongInt); stdcall;

    function GetRootNode: INode; stdcall;
    function GetLastNode: INode; stdcall;
 //public
    procedure Clear; stdcall;
    function IsChild(const Node: INode): Boolean; stdcall;
    function AddNode: INode; stdcall;
    function InsertNode(const Source: INode; AIndex: LongInt): INode; stdcall;
    function InsertList(const Source: INodeList; AIndex: LongInt): INode; stdcall;
    procedure Exchange(Index1, Index2: LongInt); stdcall;
    procedure RemoveNode(const Node: INode); stdcall;
    procedure RemoveByIndex(Index: LongInt); stdcall;
    procedure MoveTo(CurIndex, NewIndex: LongInt); stdcall;
 end;

 TPropertyAdapter = class(TNodeAdapter, IPropertyListItem)
  public
    function NameGet: PWideChar; stdcall;
    procedure NameSet(Value: PWideChar); stdcall;
    function ValueTypeGet: TValueType; stdcall;
    procedure ValueTypeSet(Value: TValueType); stdcall;
    function ValueStrGet: PWideChar; stdcall;
    procedure ValueStrSet(Value: PWideChar); stdcall;
    function DefValStrGet: PWideChar; stdcall;
    procedure DefValStrSet(Value: PWideChar); stdcall;
    function ValueGet: Int64; stdcall;
    procedure ValueSet(const Value: Int64); stdcall;
    function MinGet: Int64; stdcall;
    procedure MinSet(const Value: Int64); stdcall;
    function MaxGet: Int64; stdcall;
    procedure MaxSet(const Value: Int64); stdcall;
    function HexDigitsGet: Byte; stdcall;
    procedure HexDigitsSet(Value: Byte); stdcall;
    function ReadOnlyGet: Boolean; stdcall;
    procedure ReadOnlySet(Value: Boolean); stdcall;
    function ParamsGet: LongInt; stdcall;
    procedure ParamsSet(Value: LongInt); stdcall;
    function DataGet: Pointer; stdcall;
    function DataSizeGet: LongInt; stdcall;
    procedure DataSizeSet(Value: LongInt); stdcall;
    function GetPickListIndex: LongInt; stdcall;
    procedure SetPickListIndex(Value: LongInt); stdcall;
    function GetPickList: IWideStringList; stdcall;
    function SubPropsGet: IPropertyList; stdcall;
    function GetDataStream: IStream32; stdcall;
    function GetItemOwner: IPropertyListItem; stdcall;
    function GetMaxLength: LongInt; stdcall;
    procedure SetMaxLength(Value: LongInt); stdcall;
    function GetModified: Boolean; stdcall;
    function GetStep: Int64; stdcall;
    procedure SetStep(const Value: Int64); stdcall;
    function GetFloat: Double; stdcall;
    procedure SetFloat(const Value: Double); stdcall;
    function GetPrecision: Byte; stdcall;
    procedure SetPrecision(Value: Byte); stdcall;
{    function GetOnValueChange: TValueChangeProc; stdcall;
    procedure SetOnValueChange(Value: TValueChangeProc); stdcall;
 }
    procedure InitBoolean(AName: PWideChar; AValue: Boolean); stdcall;

    procedure InitFloat(AName: PWideChar; const AValue: Double;
             APrecision: Byte); stdcall;
    procedure InitDecimal(AName: PWideChar;
          const AValue, AMin, AMax: Int64); stdcall;
    procedure InitHexadecimal(AName: PWideChar;
          const AValue, AMin, AMax: Int64; ADigits: Byte); stdcall;

    procedure InitString(AName, AValue: PWideChar; Editable: Boolean); stdcall;

    procedure InitPickList(AName: PWideChar;
              const AList: IWideStringList;
              AIndex: LongInt;
              AFixed: Boolean); stdcall;
    procedure InitPickList2(AName, AList: PWideChar; AIndex: LongInt;
                           AFixed: Boolean); stdcall;
    procedure InitPickList3(AName, AValue, AList: PWideChar;
                           AFixed: Boolean); stdcall;
    procedure InitData(AName, Description: PWideChar); stdcall;

    constructor Create(Source: TPropertyListItem; FreeAfterUse: Boolean = False);
 end;

 TPropertyListAdapter = class(TNodeListAdapter, IPropertyList)
  protected
    procedure Initialize; override;
  public
    function PropertyGet(Index: LongInt): IPropertyListItem; stdcall;

    function AddProperty: IPropertyListItem; stdcall;

    function FloatAdd(AName: PWideChar; const AValue: Double;
             APrecision: Byte): IPropertyListItem; stdcall;
    function BooleanAdd(Name: PWideChar;
                       Value: Boolean): IPropertyListItem; stdcall;
    function DecimalAdd(Name: PWideChar;
                      const Value, Min, Max: Int64): IPropertyListItem; stdcall;
    function HexadecimalAdd(Name: PWideChar;
                            const Value, Min, Max: Int64;
                            Digits: Byte): IPropertyListItem; stdcall;
    function StringAdd(Name, Value: PWideChar;
                      Editable: Boolean): IPropertyListItem; stdcall;
    function PickListAdd(AName: PWideChar;
              const List: IWideStringList;
              Index: LongInt;
              Fixed: Boolean): IPropertyListItem; stdcall;
    function PickListAdd2(Name, List: PWideChar; Index: LongInt;
                        Fixed: Boolean): IPropertyListItem; stdcall;
    function PickListAdd3(Name, Value, List: PWideChar;
                        Fixed: Boolean): IPropertyListItem; stdcall;
    function FilePickAdd(Name, FileName, Filter, DefaultExt,
     InitialDir: PWideChar; Save: Boolean = False; Options: LongInt =
     FO_HideReadOnly or FO_EnableSizing): IPropertyListItem; stdcall;
    function DataAdd(Name, Description: PWideChar): IPropertyListItem; stdcall;

{    function GetOnValueChange: TValueChangeProc; stdcall;
    procedure SetOnValueChange(Value: TValueChangeProc); stdcall;
 }
    procedure LoadFromStream(const Stream: IReadWrite); stdcall;
    procedure SaveToStream(const Stream: IReadWrite); stdcall;
    procedure LoadFromFile(FileName: PWideChar); stdcall;
    procedure SaveToFile(FileName: PWideChar); stdcall;
    constructor Create(Source: TPropertyList; FreeAfterUse: Boolean = False);
 end;

 TSectionedItemAdapter = class;
 TSectionedItemAdapterClass = class of TSectionedItemAdapter;

 TSectionedItemAdapter = class(TNodeListAdapter, ISectionedItem)
  public
    function GetName: PWideChar; stdcall;
    procedure SetName(Value: PWideChar); stdcall;
    function GetSignature: LongWord; stdcall;
    function CalculateChecksum: LongWord; stdcall;
    function CalculateDataSize: LongInt; stdcall;
    function GetTotalSize: LongInt; stdcall;

    procedure LoadFromStream(const Stream: IReadWrite); stdcall;
    procedure SaveToStream(const Stream: IReadWrite); stdcall;
    procedure LoadFromFile(FileName: PWideChar); stdcall;
    procedure SaveToFile(FileName: PWideChar); stdcall;
 end;

 TSectionedListAdapter = class(TSectionedItemAdapter, ISectionedList)
  public
    function FindByContent(AChecksum: LongWord;
                           ADataSize: LongInt): ISectionedItem;

    function FindByName(AName: PWideChar): ISectionedItem;
    function FindIndexByFirstLetters(ALetters: PWideChar): LongInt;
    
    function AddChild(AName: PWideChar): ISectionedItem;
 end;

 TColorFormatAdapter = class(TCustomAdapter, IColorFormat)
  private
    FTemp: AnsiString;
  public
    function GetColorSize: LongInt; stdcall;
    procedure SetColorSize(Value: LongInt); stdcall;
    function GetUsedBits: LongInt; stdcall;
    procedure SetUsedBits(Value: LongInt); stdcall;
    function GetColorBits: LongInt; stdcall;

    function GetFlags: LongWord; stdcall;
    procedure SetFlags(Value: LongWord); stdcall;
    function GetAlpha: Boolean; stdcall;
    procedure SetAlpha(Value: Boolean); stdcall;
    function GetByteSwap: Boolean; stdcall;
    procedure SetByteSwap(Value: Boolean); stdcall;
    function GetChannelsCount: LongInt; stdcall;
    function GetFormatString: PAnsiChar; stdcall;
    procedure SetFormatString(Str: PAnsiChar); stdcall;

    function GetBlueMask: LongWord; stdcall;
    function GetGreenMask: LongWord; stdcall;
    function GetRedMask: LongWord; stdcall;
    function GetAlphaMask: LongWord; stdcall;

    function GetBlueCount: Byte; stdcall;
    function GetGreenCount: Byte; stdcall;
    function GetRedCount: Byte; stdcall;
    function GetAlphaCount: Byte; stdcall;

    function GetBlueShift: Byte; stdcall;
    function GetGreenShift: Byte; stdcall;
    function GetRedShift: Byte; stdcall;
    function GetAlphaShift: Byte; stdcall;

    function GetBlueRShift: Byte; stdcall;
    function GetGreenRShift: Byte; stdcall;
    function GetRedRShift: Byte; stdcall;
    function GetAlphaRShift: Byte; stdcall;
 //public
    procedure SetFormat(RBitMask, GBitMask, BBitMask, ABitMask: LongWord); stdcall;
    procedure Assign(const ClrFmt: IColorFormat); stdcall;
    procedure AssignMask(const ClrFmt: IColorFormat); stdcall;

    function ValueToRGBQuad(Value: LongWord): LongWord; stdcall;
    function ToRGBQuad(const Source): LongWord; stdcall;
    function FromRGBQuad(Source: LongWord): LongWord; stdcall;
    function ConvertValue(const DestFormat: IColorFormat;
                        const Source): LongWord; stdcall;
    function ConvertValue2(DestFormat: PAnsiChar; const Source): LongWord; stdcall;
    procedure ConvertToRGBQuad(Source, Dest: Pointer; Count: LongInt); stdcall;
    procedure ConvertFromRGBQuad(Source, Dest: Pointer; Count: LongInt); stdcall;
    procedure ConvertTo(const DestFormat: IColorFormat; Source, Dest: Pointer;
       Count: LongInt); stdcall;
    procedure ConvertTo2(DestFormat: PAnsiChar;
                        Source, Dest: Pointer; Count: LongInt); stdcall;
    procedure SetColor(Dest: Pointer; Index: LongInt; RGBQuad: LongWord); stdcall;
    procedure SetColor2(var Dest; RGBQuad: LongWord); stdcall;
    function SameAs(const CFormat: IColorFormat): Boolean; stdcall;

    constructor Create(Source: TColorFormat; FreeAfterUse: Boolean = False);
 end;

 TColorTableAdapter = class(TSectionedItemAdapter, IColorTable)
  private
    FTemp: AnsiString;
  public
    function GetTableSize: LongInt; stdcall;
    function GetColor(X: LongInt): LongWord; stdcall;
    procedure SetColor(X: LongInt; Value: LongWord); stdcall;
    function GetRGBQuad(X: LongInt): LongWord; stdcall;
    procedure SetRGBQuad(X: LongInt; Value: LongWord); stdcall;
    function GetFormatString: PAnsiChar; stdcall;
    procedure SetFormatString(Value: PAnsiChar); stdcall;
    function GetColorFormat: IColorFormat; stdcall;
    procedure SetColorFormat(const Value: IColorFormat); stdcall;
    function GetColorData: Pointer; stdcall;
    function GetColorsCount: LongInt; stdcall;
    procedure SetColorsCount(Value: LongInt); stdcall;
    function GetTransparentIndex: LongInt; stdcall;
    procedure SetTransparentIndex(Value: LongInt); stdcall;
 //public
    procedure Reset(const AColorFmt: IColorFormat; ACount: LongInt = -1); stdcall;
    procedure Reset2(AColorFmt: PAnsiChar; ACount: LongInt = -1); stdcall;
    procedure LoadActFromStream(const Stream: IReadWrite); stdcall;
    procedure SaveActToStream(const Stream: IReadWrite); stdcall;

    constructor Create(Source: TColorTable; FreeAfterUse: Boolean = False);
 end;

 TColorTableListAdapter = class(TSectionedListAdapter, IColorTableList)
  protected
    procedure Initialize; override;
  public
    function GetItem(Index: LongInt): IColorTable; stdcall;
    
    function AddColorTable(AName: PWideChar; const AColorFmt: IColorFormat;
                           ACount: LongInt = 256): IColorTable; stdcall;
    function AddColorTable2(AName: PWideChar; AColorFmt: PAnsiChar;
                           ACount: LongInt = 256): IColorTable; stdcall;

    constructor Create(Source: TColorTableList; FreeAfterUse: Boolean = False);
 end;

 TBitmapContainerAdapter = class(TCustomAdapter, IBitmapContainer)
  public
    function GetImageSize: LongInt; stdcall;
    function GetBitsCountTotal: LongInt; stdcall;
    function GetPaletteSize: LongInt; stdcall;
    function GetPaletteCount: LongInt; stdcall;

    function GetPixelModifier: LongWord; stdcall;
    procedure SetPixelModifier(Value: LongWord); stdcall;
    function GetImageData: Pointer; stdcall;
    procedure SetImageData(Value: Pointer); stdcall;
    function GetColorTable: Pointer; stdcall;
    procedure SetColorTable(Value: Pointer); stdcall;
    function GetColorFormat: IColorFormat; stdcall;
    procedure SetColorFormat(const Value: IColorFormat); stdcall;
    function GetTransparentColor: LongWord; stdcall;
    procedure SetTransparentColor(Value: LongWord); stdcall;
    function GetLeft: LongInt; stdcall;
    procedure SetLeft(Value: LongInt); stdcall;
    function GetTop: LongInt; stdcall;
    procedure SetTop(Value: LongInt); stdcall;
    function GetWidth: Word; stdcall;
    procedure SetWidth(Value: Word); stdcall;
    function GetWidthRemainder: Word; stdcall;
    procedure SetWidthRemainder(Value: Word); stdcall;
    function GetWidthBytes: LongInt; stdcall;
    function GetHeight: Word; stdcall;
    procedure SetHeight(Value: Word); stdcall;
    function GetImageFormat: Byte; stdcall;
    procedure SetImageFormat(Value: Byte); stdcall;
    function GetImageFlags: Word; stdcall;
    procedure SetImageFlags(Value: Word); stdcall;
 //public
    function GetFlagsCount: LongInt; stdcall; 
    procedure GetFlagsList(var Dest); stdcall;
    procedure SetFlagsList(const Value: array of Word); stdcall;

    constructor Create(Source: TBitmapContainer; FreeAfterUse: Boolean = False);
 end;

 TBitmapConverterAdapter = class(TCustomAdapter, IBitmapConverter)
  public
    procedure Reset(const SrcFlags, DstFlags: array of Word;
                   DrawFlags: LongInt = 0); stdcall;

    constructor Create(Source: TBitmapConverter; FreeAfterUse: Boolean = False);
 end;

 TTileItemAdapter = class(TNodeAdapter, ITileItem)
 //private
    function GetTileSize: LongInt; stdcall;
    function GetNamedAttr(Name: PWideChar): OleVariant; stdcall;
    procedure SetNamedAttr(Name: PWideChar; const Value: OleVariant); stdcall;
    function GetTileIndex: LongInt; stdcall;
    procedure SetTileIndex(Value: LongInt); stdcall;
    function GetTileHeight: LongInt; stdcall;
    function GetTileWidth: LongInt; stdcall;
    function GetTileData: Pointer; stdcall;
    function GetFirstColor: Byte; stdcall;
    procedure SetFirstColor(Value: Byte); stdcall;
    function GetBufferIndex: LongInt; stdcall;
    function GetAttribute(Index: LongInt): OleVariant; stdcall;
    procedure SetAttribute(Index: LongInt; const Value: OleVariant); stdcall;
 //public
    procedure Load(const Src: IBitmapContainer;
     X, Y: LongInt; ADrawFlags: LongInt = 0); stdcall;
    procedure CvtLoad(const Src: IBitmapContainer;
     X, Y: LongInt; const Cvt: IBitmapConverter); stdcall;
    procedure Draw(const Dest: IBitmapContainer;
     X, Y: LongInt; ADrawFlags: LongInt = 0); stdcall;
    procedure CvtDraw(const Dest: IBitmapContainer;
     X, Y: LongInt; const Cvt: IBitmapConverter); stdcall;

    procedure AssignAttributes(const Source: ITileItem); stdcall;

    constructor Create(Source: TTileItem; FreeAfterUse: Boolean = False);
 end;

 TTileSetAdapter = class(TSectionedItemAdapter, ITileSet)
  protected
    procedure Initialize; override;
  public
    function GetAttrEnumCount(Index: LongInt): LongInt; stdcall;
    procedure SetAttrEnumCount(Index, Value: LongInt); stdcall;
    function GetAttrEnumMember(AttrIndex, EnumIndex: LongInt): PWideChar; stdcall;
    procedure SetAttrEnumMember(AttrIndex, EnumIndex: LongInt; Value: PWideChar); stdcall;
    function GetAttrName(Index: LongInt): PWideChar; stdcall;
    procedure SetAttrName(Index: LongInt; Value: PWideChar); stdcall;
    function GetTilesCount: LongInt; stdcall;
    function GetIndexed(Index: LongInt): ITileItem; stdcall;
    function GetAttrCount: LongInt; stdcall;
    procedure SetAttrCount(Value: LongInt); stdcall;
    function GetAttrDef(Index: LongInt): PAttrDef; stdcall;
    function GetTransparentColor: LongWord; stdcall;
    procedure SetTransparentColor(Value: LongWord); stdcall;
    function GetTileHeight: LongInt; stdcall;
    function GetTileSize: LongInt; stdcall;
    function GetTileWidth: LongInt; stdcall;
    function GetBitCount: LongInt; stdcall;
    function GetTileItem(Index: LongInt): ITileItem; stdcall;
    function GetLastIndex: LongInt; stdcall;
    procedure SetLastIndex(Value: LongInt); stdcall;
    procedure SetTilesCount(Value: LongInt); stdcall;
    function GetEmptyTileIndex: LongInt; stdcall;
    function GetMaxIndex: LongInt; stdcall;
    procedure SetMaxIndex(Value: LongInt); stdcall;
    function GetFastList: Boolean; stdcall;
    procedure SetFastList(Value: Boolean); stdcall;    
    function GetHaveEmpty: Boolean; stdcall;

    function GetKeepBuffer: Boolean; stdcall;
    procedure SetKeepBuffer(Value: Boolean); stdcall;
    function GetDrawFlags: Word; stdcall;
    procedure SetDrawFlags(Value: Word); stdcall;
    function GetOptimizationFlags: Word; stdcall;
    procedure SetOptimizationFlags(Value: Word); stdcall;
    function GetTileBitmap: IBitmapContainer; stdcall;
    function GetEmptyTile: ITileItem; stdcall;
 //public
    function FindAttrByName(Name: PWideChar): LongInt; stdcall;
    function FindByData(const Source): ITileItem; stdcall;

    procedure DeleteAttr(Index: LongInt); stdcall;
    procedure MoveAttr(OldIndex, NewIndex: LongInt); stdcall;
    function AddTile: ITileItem; stdcall;
    function AddTileEx(const AttrList: array of OleVariant): ITileItem; stdcall;
    function AddFixed(Index: LongInt): ITileItem; stdcall;
    function AddOptimized(const Source): ITileItem; stdcall;
    function AddOptimizedEx(const Source; const Attr: array of OleVariant;
                          var XFlip, YFlip: Boolean): ITileItem; stdcall;

    procedure TileFlip(const Source; var Dest; X, Y: Boolean); stdcall;
    procedure TileLoad(const Src: IBitmapContainer; var Dest;
     X, Y: LongInt; ADrawFlags: LongInt = 0); stdcall;
    procedure TileCvtLoad(const Src: IBitmapContainer; var Dest;
     X, Y: LongInt; const Cvt: IBitmapConverter); stdcall;
    procedure TileDraw(const Source; const Dest: IBitmapContainer;
     X, Y: LongInt; ADrawFlags: LongInt = 0); stdcall;
    procedure TileCvtDraw(const Source; const Dest: IBitmapContainer;
     X, Y: LongInt; const Cvt: IBitmapConverter); stdcall;

    function FindEmptyIndex: LongInt; stdcall;

    procedure FillConverter(const Cvt: IBitmapConverter;
             const Flags: array of Word; ADrawFlags: Word = 0);  stdcall;
    procedure FillConvertRec(var CvtRec: TTileICvtRec;
                             const Flags: array of Word;
                              ADrawFlags: Word = 0); stdcall;

    procedure ReadFromStream(const Stream: IReadWrite;
     AIndex, ACount: LongInt; AOptimize: Boolean); stdcall;
    procedure WriteToStream(const Stream: IReadWrite;
     AIndex, ACount: LongInt; AUseEmptyTiles: Boolean); stdcall;

    procedure AssignAttrDefs(const Source: ITileSet); stdcall;
    procedure AssignTileFormat(const Source: ITileSet); stdcall;
    procedure ConvertFrom(const Source: ITileSet); stdcall;

    procedure SwitchFormat(const Flags: array of Word;
                           ATileWidth: LongInt = -1;
                           ATileHeight: LongInt = -1); stdcall;
    procedure Reallocate(const Flags: array of Word;
                         ATileWidth: LongInt = -1;
                         ATileHeight: LongInt = -1); stdcall;

    constructor Create(Source: TTileSet; FreeAfterUse: Boolean = False);
 end;

 TTileSetListAdapter = class(TSectionedListAdapter, ITileSetList)
  protected
    procedure Initialize; override;
  public
    function GetSet(X: LongInt): ITileSet; stdcall;
 //public
    procedure FillCvtRecList(AList: PTileICvtRec;
                const AFlags: array of Word; ADrawFlags: Word = 0); stdcall;
 //properties
    property Sets[X: LongInt]: ITileSet read GetSet; default;

    constructor Create(Source: TTileSetList; FreeAfterUse: Boolean = False);
 end;

 TMapLayerAdapter = class;
 TMapLayerAdapterClass = class of TMapLayerAdapter;

 TMapLayerAdapter = class(TSectionedItemAdapter, IMapLayer)
 //private
    function GetCellSize: Byte; stdcall;
    procedure SetCellSize(Value: Byte); stdcall;
    procedure SetTileSetIndex(Value: LongInt); stdcall;
    function GetTileSetIndex: LongInt; stdcall;
    function GetLayerDataSize: LongInt; stdcall;
    function GetCellInfo(X, Y: LongInt): TCellInfo; stdcall;
    procedure SetCellInfo(X, Y: LongInt; const Value: TCellInfo); stdcall;
    function GetTileIndex(X, Y: LongInt): LongInt; stdcall;
    procedure SetTileIndex(X, Y: LongInt; Value: LongInt); stdcall;
    function GetIndexMask: LongWord; stdcall;
    procedure SetIndexMask(Value: LongWord); stdcall;
    function GetIndexShift: ShortInt; stdcall;
    procedure SetIndexShift(Value: ShortInt); stdcall;
    function GetPaletteIndexMask: LongWord; stdcall;
    procedure SetPaletteIndexMask(Value: LongWord); stdcall;
    function GetPaletteIndexShift: ShortInt; stdcall;
    procedure SetPaletteIndexShift(Value: ShortInt); stdcall;
    function GetPriorityMask: LongWord; stdcall;
    procedure SetPriorityMask(Value: LongWord); stdcall;
    function GetPriorityShift: ShortInt; stdcall;
    procedure SetPriorityShift(Value: ShortInt); stdcall;
    function GetXFlipShift: ShortInt; stdcall;
    procedure SetXFlipShift(Value: ShortInt); stdcall;
    function GetYFlipShift: ShortInt; stdcall;
    procedure SetYFlipShift(Value: ShortInt); stdcall;
    function GetTilesFilled: Boolean; stdcall;
    procedure SetTilesFilled(Value: Boolean); stdcall;
    function GetDrawInfo: PCellInfo; stdcall;
    function GetVisibleRect: TRect; stdcall;
    function GetLayerData: Pointer; stdcall;
    function GetTileSet: ITileSet; stdcall;
    function GetFirstColor: Byte; stdcall;
    procedure SetFirstColor(Value: Byte); stdcall;
    function GetFlags: Byte; stdcall;
    procedure SetFlags(Value: Byte); stdcall;
 //public
    function CompareLayerFormat(const AFmt: TLayerFormatRec): Boolean;
    procedure FillLayerFormat(var AFmt: TLayerFormatRec);
    function GetLayerFormat: TLayerFormatRec;

    procedure ReadCellInfo(const Data; var Info: TCellInfo);
    function ReadTileIndex(var Data): LongInt;
    function ReadPaletteIndex(var Data): LongInt;
    function ReadPriority(var Data): LongInt;
    function ReadXFlip(var Data): Boolean;
    function ReadYFlip(var Data): Boolean;

    procedure WriteCellInfo(var Data; const Info: TCellInfo);
    procedure WriteTileIndex(var Data; Value: LongInt);
    procedure WritePaletteIndex(var Data; Value: LongInt);
    procedure WritePriority(var Data; Value: LongInt);
    procedure WriteXFlip(var Data; Value: Boolean);
    procedure WriteYFlip(var Data; Value: Boolean);
    function FindCellInfo(const Info: TCellInfo): LongInt;

    procedure AssignLayerFormat(const Source: IMapLayer);
    procedure AssignLayerInfo(const Source: IMapLayer);

    procedure UpdateFormat(const NewFormat: TLayerFormatRec);

    constructor Create(Source: TMapLayer; FreeAfterUse: Boolean = False);
 end;

 TBrushLayerAdapter = class(TMapLayerAdapter, IBrushLayer)
 //private
    function GetHotSpotX: LongInt; stdcall;
    procedure SetHotSpotX(Value: LongInt); stdcall;
    function GetHotSpotY: LongInt; stdcall;
    procedure SetHotSpotY(Value: LongInt); stdcall;
    function GetIdentityTag: LongInt; stdcall;
    procedure SetIdentityTag(Value: LongInt); stdcall;
    function GetTailsFilled: Boolean; stdcall;
    procedure SetTailsFilled(Value: Boolean); stdcall;
    function GetTailsDirections: TLinkDirections; stdcall;
    function GetRandomize: Boolean; stdcall;
    procedure SetRandomize(Value: Boolean); stdcall;

    function GetUnfinished: TLinkDirections; stdcall;
    procedure SetUnfinished(Value: TLinkDirections); stdcall;
    function GetExtended: TLinkDirections; stdcall;
    procedure SetExtended(Value: TLinkDirections); stdcall;

    function GetRandomTwin: IBrushLayer; stdcall;

    function GetLinkIndex(Direction: TLinkDirection): LongInt; stdcall;
    procedure SetLinkIndex(Direction: TLinkDirection; Value: LongInt); stdcall;
    function GetLink(Direction: TLinkDirection): IBrushLayer; stdcall;
 //public
    function SitsHere(const Target: IMapLayer; X, Y: LongInt): Boolean; stdcall;
    function SpottedInRect(const Target: IMapLayer;
     const Rect: TRect; var pt: TPoint): Boolean; stdcall;
    function RectOnMap(X, Y: LongInt): TRect; stdcall;

    function DrawTail(const Dest: IMap;
     DestX, DestY: LongInt; Direction: TLinkDirection): Boolean; stdcall;
    function Draw(const DestLayer: IMapLayer;
     DestX, DestY: LongInt): Boolean; stdcall;

    constructor Create(Source: TBrushLayer; FreeAfterUse: Boolean = False);
 end;

 TMapAdapter = class;
 TMapAdapterClass = class of TMapAdapter;

 TMapAdapter = class(TSectionedListAdapter, IMap)
  protected
    procedure Initialize; override;
  public
    function GetMapX: LongInt; stdcall;
    procedure SetMapX(Value: LongInt); stdcall;
    function GetMapY: LongInt; stdcall;
    procedure SetMapY(Value: LongInt); stdcall;
    function GetWidth: LongInt; stdcall;
    procedure SetWidth(Value: LongInt); stdcall;
    function GetHeight: LongInt; stdcall;
    procedure SetHeight(Value: LongInt); stdcall;
    function GetTileWidth: LongInt; stdcall;
    procedure SetTileWidth(Value: LongInt); stdcall;
    function GetTileHeight: LongInt; stdcall;
    procedure SetTileHeight(Value: LongInt); stdcall;
    function GetColorTable: IColorTable; stdcall;
    function GetCtIndex: LongInt; stdcall;
    procedure SetCtIndex(Value: LongInt); stdcall;

    function GetLayer(Index: LongInt): IMapLayer; stdcall;
 //public
    procedure AssignMapInfo(const Source: IMap); stdcall;
    function FindLayerByTileSet(const ATileSet: ITileSet): IMapLayer; stdcall;
    procedure SetMapSize(AWidth, AHeight: LongInt); stdcall;

    function IsTileSetUsed(const ATileSet: ITileSet): Boolean; stdcall;
    procedure UnhookTileSet(const ATileSet: ITileSet); stdcall;

    function AddLayer(Name: PWideChar): IMapLayer; stdcall;

    constructor Create(Source: TMap; FreeAfterUse: Boolean = False);
 end;

 TMapBrushAdapter = class(TMapAdapter, IMapBrush)
  protected
    procedure Initialize; override;
  public
    function GetExtends: IMapBrush; stdcall;
    function GetExtendsIndex: LongInt; stdcall;
    procedure SetExtendsIndex(Value: LongInt); stdcall;
 //public
    function FindExactBrushLayer(const Dest: IMap;
     X, Y: LongInt; var pt: TPoint): IBrushLayer; stdcall;
    function FindBrushLayer(const Dest: IMap;
     X, Y: LongInt; var pt: TPoint): IBrushLayer; stdcall;
    function Draw(const Dest: IMap; DestX, DestY: LongInt): Boolean; stdcall;

    procedure RepointLinks; stdcall;

    constructor Create(Source: TMapBrush; FreeAfterUse: Boolean = False);
 end;

 TBaseMapListAdapter = class(TSectionedListAdapter, IBaseMapList)
  public
    function GetColorTableList: IColorTableList; stdcall;
    function GetTileSetList: ITileSetList; stdcall;
    function GetMap(Index: LongInt): IMap; stdcall;
 //public
    function AddMap(AName: PWideChar;
     AWidth, AHeight, ATileW, ATileH: LongInt): IMap; stdcall;
    function IsColorTableUsed(const AColorTable: IColorTable): Boolean; stdcall;
    function IsTileSetUsed(const ATileSet: ITileSet): Boolean; stdcall;
    procedure UnhookTileSet(const ATileSet: ITileSet); stdcall;
    procedure UnhookColorTable(const AColorTable: IColorTable); stdcall;
 end;

 TBrushListAdapter = class(TBaseMapListAdapter, IBrushList)
  protected
    procedure Initialize; override;
  public
    procedure SetColorTableList(const Value: IColorTableList); stdcall;
    procedure SetTileSetList(const Value: ITileSetList); stdcall;

    function GetIdentitiesFilled: Boolean; stdcall;
    procedure SetIdentitiesFilled(Value: Boolean); stdcall;

    function GetMapBrush(Index: LongInt): IMapBrush; stdcall;
 //public
    procedure CleanUpTails;  stdcall;
    function EraseBrushContentAt(const Dest: IMap;
     const DestRect: TRect): Boolean; stdcall;
    function IsBrushUsed(const ABrush: IMapBrush): Boolean; stdcall;
    procedure UnhookBrush(const ABrush: IMapBrush); stdcall;

    constructor Create(Source: TBrushList; FreeAfterUse: Boolean = False);
 end;

 TMapListAdapter = class(TBaseMapListAdapter, IMapList)
  protected
    procedure Initialize; override;
  public
    function GetBrushList: IBrushList; stdcall;

    constructor Create(Source: TMapList; FreeAfterUse: Boolean = False);
 end;

 TUtilsAdapter = class(TInterfacedObject, IUtilsInterface)
 //Stream utils
   function CreateMemoryStream: IMemoryStream; stdcall;
   function CreateFileStream(FileName: PAnsiChar;
                             Mode: LongInt): IStream64; stdcall;
   function CreateFileStreamW(FileName: PWideChar;
                             Mode: LongInt): IStream64; stdcall;
   function CreateZLibCompressor(Level: LongInt; const Dest: IReadWrite): IStream32; stdcall;
   function CreateZLibDecompressor(const Source: IReadWrite): IStream32; stdcall;
 //Strings utils
   function CreateStringList: IStringList; stdcall;
   function CreateWideStringList: IWideStringList; stdcall;
 //Color utils
   function CreateColorFormat(Fmt: PAnsiChar): IColorFormat; stdcall;
   function CreateColorFormat2(RMask, GMask, BMask, AMask: LongWord): IColorFormat; stdcall;
   function CreateColorTableList: IColorTableList; stdcall;
 //BitmapEx utils
   function CreateBitmapContainer: IBitmapContainer; stdcall;
   function CreateBitmapConverter: IBitmapConverter; stdcall;
 //TilesEx utils
   function CreateTileSetList: ITileSetList; stdcall;
 //me_lib utils
   function CreateMapList: IMapList; stdcall;
   function CreateBrushList: IBrushList; stdcall;
 //PropertyList utils
   function CreatePropertyList: IPropertyList; stdcall;
 //Ini utils
   function CreateIniFile: IIniFile; stdcall;

//   function PropertyFromPointer(Ptr: Pointer): IPropertyListItem; stdcall;   
 end;

implementation

uses
 ZlibEx;

{ TCustomAdapter }

constructor TCustomAdapter.Create(Source: TObject; FreeAfterUse: Boolean);
begin
 FSource := Source;
 FFreeAfterUse := FreeAfterUse;
 Initialize;
end;

destructor TCustomAdapter.Destroy;
begin
 if FFreeAfterUse then
  FSource.Free;
 inherited;
end;

procedure TCustomAdapter.FreeObject;
begin
 FreeAndNIL(FSource);
end;

function TCustomAdapter.GetPtr: Pointer;
begin
 Result := Pointer(FSource);
end;

procedure TCustomAdapter.Initialize;
begin
 // do nothing
end;

{ TStreamAdapter }

function TStreamAdapter.CopyFrom(const Stream: IReadWrite;
  Count: LongInt): LongInt;
begin
 if Stream = NIL then
  Result := 0 else
  Result := TStream(FSource).CopyFrom(TStream(Stream.GetPtr), Count);
end;

function TStreamAdapter.CopyFrom64(const Stream: IReadWrite;
  const Count: Int64): Int64;
begin
 if Stream = NIL then
  Result := 0 else
  Result := TStream(FSource).CopyFrom(TStream(Stream.GetPtr), Count);
end;

constructor TStreamAdapter.Create(Source: TStream; FreeAfterUse: Boolean);
begin
 inherited Create(Source, FreeAfterUse);
end;

constructor TStreamAdapter.Create(AClass: TStreamClass);
begin
 inherited Create(AClass.Create, True);
end;

function TStreamAdapter.GetPos: LongInt;
begin
 Result := TStream(FSource).Position;
end;

function TStreamAdapter.GetPos64: Int64;
begin
 Result := TStream(FSource).Position;
end;

function TStreamAdapter.GetSize: LongInt;
begin
 Result := TStream(FSource).Size;
end;

function TStreamAdapter.GetSize64: Int64;
begin
 Result := TStream(FSource).Size;
end;

function TStreamAdapter.Read(var Buffer; Count: LongInt): LongInt;
begin
 Result := TStream(FSource).Read(Buffer, Count);
end;

procedure TStreamAdapter.Seek(Offset, SeekOrigin: LongInt);
begin
 TStream(FSource).Seek(Offset, SeekOrigin);
end;

procedure TStreamAdapter.Seek64(const Offset: Int64; SeekOrigin: LongInt);
begin
 TStream(FSource).Seek(Offset, TSeekOrigin(SeekOrigin));
end;

procedure TStreamAdapter.SetPos(Value: LongInt);
begin
 TStream(FSource).Position := Value;
end;

procedure TStreamAdapter.SetPos64(const Value: Int64);
begin
 TStream(FSource).Position := Value;
end;

procedure TStreamAdapter.SetSize(Value: LongInt);
begin
 TStream(FSource).Size := Value;
end;

procedure TStreamAdapter.SetSize64(const Value: Int64);
begin
 TStream(FSource).Size := Value;
end;

function TStreamAdapter.Write(const Buffer; Count: LongInt): LongInt;
begin
 Result := TStream(FSource).Write(Buffer, Count);
end;

{ TMemoryStreamAdapter }

procedure TMemoryStreamAdapter.Clear;
begin
 if FSource is TMemoryStream then
  TMemoryStream(FSource).Clear;
end;

constructor TMemoryStreamAdapter.Create(Source: TCustomMemoryStream;
  FreeAfterUse: Boolean);
begin
 inherited Create(TObject(Source), FreeAfterUse);
end;

function TMemoryStreamAdapter.GetMemory: Pointer;
begin
 Result := (FSource as TCustomMemoryStream).Memory;
end;

{ TStringsAdapter }

function TStringsAdapter.Add(S: PAnsiChar): LongInt;
begin
 with TStrListRec(FSource) do
  if List is TStringList then
   Result := ListA.Add(S) else
   Result := ListW.Add(S);
end;

procedure TStringsAdapter.AddStrings(const Strings: IStrings);
var
 Obj: TObject;
 I: Integer;
begin
 Obj := TObject(Strings.GetPtr);
 if Obj is TStringList then
 begin
  with TStrListRec(FSource) do
   if List is TStringList then
    ListA.AddStrings(TStringList(Obj)) else
    ListW.AddStrings(TStringList(Obj));
 end else
 if Obj is TTntStringList then
 begin
  with TStrListRec(FSource) do
  if List is TTntStringList then
   ListW.AddStrings(TTntStringList(Obj)) else
  with TTntStringList(Obj) do
   for I := 0 to Count - 1 do
    ListA.Add(AnsiStrings[I]);
 end;
end;

function TStringsAdapter.AddW(S: PWideChar): LongInt;
begin
 with TStrListRec(FSource) do
  if List is TTntStringList then
   Result := ListW.Add(S) else
   Result := ListA.Add(S);
end;

procedure TStringsAdapter.Append(S: PAnsiChar);
begin
 with TStrListRec(FSource) do
  if List is TStringList then
   ListA.Append(S) else
   ListW.Append(S);
end;

procedure TStringsAdapter.AppendW(S: PWideChar);
begin
 with TStrListRec(FSource) do
  if List is TTntStringList then
   ListW.Append(S) else
   ListA.Append(S);
end;

procedure TStringsAdapter.Assign(const Source: IStrings);
var
 Obj: TObject;
 I: Integer;
 S: AnsiString;
begin
 if Source <> nil then
 begin
  Obj := TObject(Source.GetPtr);
  if Obj is TStringList then
  begin
   with TStrListRec(FSource) do
    if List is TStringList then
     ListA.Assign(TStringList(Obj)) else
     ListW.Assign(TStringList(Obj));
  end else
  if Obj is TTntStringList then
  begin
   with TStrListRec(FSource) do
    if List is TTntStringList then
     ListW.Assign(TTntStringList(Obj)) else
   with TTntStringList(Obj) do
   begin
    ListA.BeginUpdate;
    try
     ListA.Clear;
     S := NameValueSeparator;
     ListA.NameValueSeparator := S[1];
     S := QuoteChar;
     ListA.QuoteChar := S[1];
     S := Delimiter;
     ListA.Delimiter := S[1];
     for I := 0 to Count - 1 do
      ListA.Add(AnsiStrings[I]);
    finally
     ListA.EndUpdate;
    end;
   end;
  end;
 end else
 with TStrListRec(FSource) do
  if List is TStringList then
   ListA.Clear else
   ListW.Clear;
end;

procedure TStringsAdapter.Clear;
begin
 with TStrListRec(FSource) do
  if List is TStringList then
   ListA.Clear else
   ListW.Clear;
end;

constructor TStringsAdapter.Create(Source: TStringList;
  FreeAfterUse: Boolean);
begin
 inherited Create(Source, FreeAfterUse);
end;

constructor TStringsAdapter.Create(Source: TTntStringList;
  FreeAfterUse: Boolean);
begin
 inherited Create(Source, FreeAfterUse);
end;

constructor TStringsAdapter.Create;
begin
 inherited Create(TTntStringList.Create, True);
end;

procedure TStringsAdapter.Delete(Index: LongInt);
begin
 with TStrListRec(FSource) do
  if List is TStringList then
   ListA.Delete(Index) else
   ListW.Delete(Index);
end;

function TStringsAdapter.Equals(const Strings: IStrings): Boolean;
var
 Obj: TObject;
 I: Integer;
begin
 Result := False;
 Obj := TObject(Strings.GetPtr);
 if Obj is TStringList then
 begin
  with TStrListRec(FSource) do
   if List is TStringList then
    Result := ListA.Equals(TStringList(Obj)) else
  with TStringList(Obj) do
  begin
    if ListW.Count <> Count then Exit;
    for I := 0 to Count - 1 do
     if ListW[I] <> Strings[I] then Exit;
    Result := True;
  end;
 end else
 if Obj is TTntStringList then
 begin
  with TStrListRec(FSource) do
   if List is TTntStringList then
    Result := ListW.Equals(TTntStringList(Obj)) else
  with TTntStringList(Obj) do
  begin
    if ListA.Count <> Count then Exit;
    for I := 0 to Count - 1 do
     if ListA[I] <> Strings[I] then Exit;
    Result := True;
  end;
 end;
end;

procedure TStringsAdapter.Exchange(Index1, Index2: LongInt);
begin
 with TStrListRec(FSource) do
  if List is TStringList then
   ListA.Exchange(Index1, Index2) else
   ListW.Exchange(Index1, Index2);
end;

function TStringsAdapter.Find(S: PAnsiChar; var Index: LongInt): Boolean;
begin
 with TStrListRec(FSource) do
  if List is TStringList then
   Result := ListA.Find(S, Index) else
   Result := ListW.Find(S, Index);
end;

function TStringsAdapter.FindW(S: PWideChar; var Index: LongInt): Boolean;
begin
 with TStrListRec(FSource) do
  if List is TTntStringList then
   Result := ListW.Find(S, Index) else
   Result := ListA.Find(S, Index);
end;

function TStringsAdapter.GetCount: LongInt;
begin
 with TStrListRec(FSource) do
  if List is TStringList then
   Result := ListA.Count else
   Result := ListW.Count;
end;

function TStringsAdapter.GetString(Index: LongInt): PAnsiChar;
begin
 with TStrListRec(FSource) do
  if List is TStringList then
   FTempA := ListA.Strings[Index] else
   FTempA := ListW.AnsiStrings[Index];
 Result := Pointer(FTempA);
end;

function TStringsAdapter.GetStringW(Index: LongInt): PWideChar;
begin
 with TStrListRec(FSource) do
  if List is TTntStringList then
   FTempW := ListW.Strings[Index] else
   FTempW := ListA.Strings[Index];
 Result := Pointer(FTempW);
end;

function TStringsAdapter.GetText: PAnsiChar;
begin
 with TStrListRec(FSource) do
  if List is TStringList then
   FTempA := ListA.Text else
   FTempA := ListW.Text;
 Result := Pointer(FTempA);
end;

function TStringsAdapter.GetTextW: PWideChar;
begin
 with TStrListRec(FSource) do
  if List is TTntStringList then
   FTempW := ListW.Text else
   FTempW := ListA.Text;
 Result := Pointer(FTempW);
end;

function TStringsAdapter.IndexOf(S: PAnsiChar): LongInt;
begin
 with TStrListRec(FSource) do
  if List is TStringList then
   Result := ListA.IndexOf(S) else
   Result := ListW.IndexOf(S);
end;

function TStringsAdapter.IndexOfW(S: PWideChar): LongInt;
begin
 with TStrListRec(FSource) do
  if List is TTntStringList then
   Result := ListW.IndexOf(S) else
   Result := ListA.IndexOf(S);
end;
procedure TStringsAdapter.Insert(Index: LongInt; S: PAnsiChar);
begin
 with TStrListRec(FSource) do
  if List is TStringList then
   ListA.Insert(Index, S) else
   ListW.Insert(Index, S);
end;

procedure TStringsAdapter.InsertW(Index: LongInt; S: PWideChar);
begin
 with TStrListRec(FSource) do
  if List is TTntStringList then
   ListW.Insert(Index, S) else
   ListA.Insert(Index, S);
end;

procedure TStringsAdapter.LoadFromFile(FileName: PWideChar);
var
 Temp: TTntStringList;
 I: Integer;
begin
 Temp := TTntStringList.Create;
 try
  Temp.LoadFromFile(FileName);
  with TStrListRec(FSource) do
   if List is TTntStringList then
    ListW.Assign(Temp) else
  begin
   ListA.Clear;
   for I := 0 to Temp.Count - 1 do
    ListA.Add(Temp.AnsiStrings[I]);
  end;
 finally
  Temp.Free;
 end;
end;

procedure TStringsAdapter.LoadFromStream(const Stream: IReadWrite);
var
 Temp: TTntStringList;
 I: Integer;
begin
 Temp := TTntStringList.Create;
 try
  Temp.LoadFromStream(TStream(Stream.GetPtr));
  with TStrListRec(FSource) do
   if List is TTntStringList then
    ListW.Assign(Temp) else
  begin
   ListA.Clear;
   for I := 0 to Temp.Count - 1 do
    ListA.Add(Temp.AnsiStrings[I]);
  end;
 finally
  Temp.Free;
 end;
end;

procedure TStringsAdapter.Move(CurIndex, NewIndex: LongInt);
begin
 with TStrListRec(FSource) do
  if List is TStringList then
   ListA.Move(CurIndex, NewIndex) else
   ListW.Move(CurIndex, NewIndex);
end;

procedure TStringsAdapter.SaveToFile(FileName: PWideChar);
var
 Stream: TStream;
begin
 Stream := TTntFileStream.Create(FileName, Classes.fmCreate);
 try
  with TStrListRec(FSource) do
   if List is TStringList then
    ListA.SaveToStream(Stream) else
    ListW.SaveToStream(Stream);
 finally
  Stream.Free;
 end;
end;

procedure TStringsAdapter.SaveToStream(const Stream: IReadWrite);
var
 Strm: TStream;
begin
 Strm := TStream(Stream.GetPtr);
 with TStrListRec(FSource) do
  if List is TStringList then
   ListA.SaveToStream(Strm) else
   ListW.SaveToStream(Strm);
end;

procedure TStringsAdapter.SetString(Index: LongInt; Value: PAnsiChar);
begin
 with TStrListRec(FSource) do
  if List is TStringList then
   ListA.Strings[Index] := Value else
   ListW.AnsiStrings[Index] := Value;
end;

procedure TStringsAdapter.SetStringW(Index: LongInt; Value: PWideChar);
begin
 with TStrListRec(FSource) do
  if List is TTntStringList then
   ListW.Strings[Index] := Value else
   ListA.Strings[Index] := Value;
end;

procedure TStringsAdapter.SetText(Value: PAnsiChar);
begin
 with TStrListRec(FSource) do
  if List is TStringList then
   ListA.Text := Value else
   ListW.Text := Value;
end;

procedure TStringsAdapter.SetTextW(Value: PWideChar);
begin
 with TStrListRec(FSource) do
  if List is TTntStringList then
   ListW.Text := Value else
   ListA.Text := Value;
end;

procedure TStringsAdapter.Sort;
begin
 with TStrListRec(FSource) do
  if List is TStringList then
   ListA.Sort else
   ListW.Sort;
end;

{ TIniFileAdapter }

constructor TIniFileAdapter.Create(Source: TStreamIniFileW;
  FreeAfterUse: Boolean);
begin
 inherited Create(Source, FreeAfterUse);
end;

procedure TIniFileAdapter.DeleteKey(Section, Ident: PWideChar);
begin
 TStreamIniFileW(FSource).DeleteKey(Section, Ident);
end;

destructor TIniFileAdapter.Destroy;
begin
 with TStreamIniFileW(FSource) do
  if FileName <> '' then
   UpdateFile;
 inherited;
end;

procedure TIniFileAdapter.EraseSection(Section: PWideChar);
begin
 TStreamIniFileW(FSource).EraseSection(Section);
end;

function TIniFileAdapter.GetFileName: PWideChar;
begin
 Result := Pointer(TStreamIniFileW(FSource).FileName);
end;

procedure TIniFileAdapter.LoadFromFile(FileName: PWideChar);
begin
 TStreamIniFileW(FSource).LoadFromFile(WideString(FileName));
end;

procedure TIniFileAdapter.LoadFromStream(const Stream: IReadWrite);
begin
 TStreamIniFileW(FSource).LoadFromStream(TStream(Stream.GetPtr));
end;

function TIniFileAdapter.ReadBinaryStream(Section, Name: PWideChar;
  const Value: IReadWrite): LongInt;
begin
 Result := TStreamIniFileW(FSource).ReadBinaryStream(Section, Name, TStream(Value.GetPtr));
end;

function TIniFileAdapter.ReadBool(Section, Ident: PWideChar;
  Default: Boolean): Boolean;
begin
 Result := TStreamIniFileW(FSource).ReadBool(Section, Ident, Default);
end;

function TIniFileAdapter.ReadDate(Section, Name: PWideChar;
  const Default: TDateTime): TDateTime;
begin
 Result := TStreamIniFileW(FSource).ReadDate(Section, Name, Default);
end;

function TIniFileAdapter.ReadDateTime(Section, Name: PWideChar;
  const Default: TDateTime): TDateTime;
begin
 Result := TStreamIniFileW(FSource).ReadDateTime(Section, Name, Default);
end;

function TIniFileAdapter.ReadFloat(Section, Name: PWideChar;
  const Default: Double): Double;
begin
 Result := TStreamIniFileW(FSource).ReadFloat(Section, Name, Default);
end;

function TIniFileAdapter.ReadInt64(Section, Ident: PWideChar;
  const Default: Int64): Int64;
begin
 Result := TStreamIniFileW(FSource).ReadInt64(Section, Ident, Default);
end;

function TIniFileAdapter.ReadInteger(Section, Ident: PWideChar;
  Default: LongInt): Longint;
begin
 Result := TStreamIniFileW(FSource).ReadInteger(Section, Ident, Default);
end;

procedure TIniFileAdapter.ReadSection(Section: PWideChar;
  const Strings: IWideStringList);
begin
 if Strings <> nil then
  TStreamIniFileW(FSource).ReadSection(Section, TTntStringList(Strings.GetPtr));
end;

procedure TIniFileAdapter.ReadSections(const Strings: IWideStringList);
begin
 if Strings <> nil then
  TStreamIniFileW(FSource).ReadSections(TTntStringList(Strings.GetPtr));
end;

procedure TIniFileAdapter.ReadSectionValues(Section: PWideChar;
  const Strings: IWideStringList);
begin
 TStreamIniFileW(FSource).ReadSectionValues(Section, TTntStringList(Strings.GetPtr));
end;

function TIniFileAdapter.ReadString(Section, Ident,
  Default: PWideChar): PWideChar;
begin
 FTemp := TStreamIniFileW(FSource).ReadString(Section, Ident, Default);
 Result := Pointer(FTemp);
end;

function TIniFileAdapter.ReadTime(Section, Name: PWideChar;
  const Default: TDateTime): TDateTime;
begin
 Result := TStreamIniFileW(FSource).ReadTime(Section, Name, Default);
end;

procedure TIniFileAdapter.SaveToFile(FileName: PWideChar);
begin
 TStreamIniFileW(FSource).SaveToFile(WideString(FileName));
end;

procedure TIniFileAdapter.SaveToStream(const Stream: IReadWrite);
begin
 TStreamIniFileW(FSource).SaveToStream(TStream(Stream.GetPtr));
end;

function TIniFileAdapter.SectionExists(Section: PWideChar): Boolean;
begin
 Result := TStreamIniFileW(FSource).SectionExists(Section);
end;

procedure TIniFileAdapter.SetStrings(const List: IWideStringList);
begin
 TStreamIniFileW(FSource).SetStrings(TTntStringList(List.GetPtr));
end;

function TIniFileAdapter.ValueExists(Section, Ident: PWideChar): Boolean;
begin
 Result := TStreamIniFileW(FSource).ValueExists(Section, Ident);
end;

procedure TIniFileAdapter.WriteBinaryStream(Section, Name: PWideChar;
  const Value: IReadWrite);
begin
 TStreamIniFileW(FSource).WriteBinaryStream(Section, Name, TStream(Value.GetPtr));
end;

procedure TIniFileAdapter.WriteBool(Section, Ident: PWideChar;
  Value: Boolean);
begin
 TStreamIniFileW(FSource).WriteBool(Section, Ident, Value);
end;

procedure TIniFileAdapter.WriteDate(Section, Name: PWideChar;
  const Value: TDateTime);
begin
 TStreamIniFileW(FSource).WriteDate(Section, Name, Value);
end;

procedure TIniFileAdapter.WriteDateTime(Section, Name: PWideChar;
  const Value: TDateTime);
begin
 TStreamIniFileW(FSource).WriteDateTime(Section, Name, Value);
end;

procedure TIniFileAdapter.WriteFloat(Section, Name: PWideChar;
  const Value: Double);
begin
 TStreamIniFileW(FSource).WriteFloat(Section, Name, Value);
end;

procedure TIniFileAdapter.WriteInt64(Section, Ident: PWideChar;
  const Value: Int64; HexDigits: Byte);
begin
 TStreamIniFileW(FSource).WriteInt64(Section, Ident, Value, HexDigits);
end;

procedure TIniFileAdapter.WriteInteger(Section, Ident: PWideChar; Value: LongInt;
  HexDigits: Byte);
begin
 TStreamIniFileW(FSource).WriteInteger(Section, Ident, Value, HexDigits);
end;

procedure TIniFileAdapter.WriteString(Section, Ident, Value: PWideChar);
begin
 TStreamIniFileW(FSource).WriteString(Section, Ident, Value);
end;

procedure TIniFileAdapter.WriteTime(Section, Name: PWideChar;
  const Value: TDateTime);
begin
 TStreamIniFileW(FSource).WriteTime(Section, Name, Value);
end;

{ TNodeAdapter }

procedure TNodeAdapter.Assign(const Source: INode);
begin
 if Source <> nil then
  TNode(FSource).Assign(TNode(Source.GetPtr)) else
  TNode(FSource).Assign(nil);
end;

procedure TNodeAdapter.Changed;
begin
 TNode(FSource).Changed;
end;

function TNodeAdapter.GetIndex: LongInt;
begin
 Result := TNode(FSource).Index;
end;

function TNodeAdapter.GetNext: INode;
begin
 with TNode(FSource) do
  if Next <> nil then
   Result := TNodeAdapterClass(Self.ClassType).Create(Next) else
   Result := nil;
end;

function TNodeAdapter.GetOwner: INode;
begin
 with TNode(FSource) do
  if Owner <> nil then
   Result := TNodeAdapter.Create(Owner) else
   Result := nil;
end;

function TNodeAdapter.GetPrev: INode;
begin
 with TNode(FSource) do
  if Prev <> nil then
   Result := TNodeAdapterClass(Self.ClassType).Create(Prev) else
   Result := nil;
end;

function TNodeAdapter.GetUserTag: LongInt;
begin
 Result := TNode(FSource).UserTag;
end;

procedure TNodeAdapter.SetUserTag(Value: LongInt);
begin
 TNode(FSource).UserTag := Value;
end;

{ TNodeListAdapter }

function TNodeListAdapter.AddNode: INode;
begin
 with TNodeList(FSource) do
  Result := FNodeAdapterClass.Create(AddNode);
end;

procedure TNodeListAdapter.Clear;
begin
 TNodeList(FSource).Clear;
end;

procedure TNodeListAdapter.Exchange(Index1, Index2: LongInt);
begin
 TNodeList(FSource).Exchange(Index1, Index2);
end;

function TNodeListAdapter.GetCount: LongInt;
begin
 Result := TNodeList(FSource).Count;
end;

function TNodeListAdapter.GetLastNode: INode;
begin
 with TNodeList(FSource) do
  if LastNode <> nil then
   Result := FNodeAdapterClass.Create(LastNode) else
   Result := nil;
end;

function TNodeListAdapter.GetMaxCount: LongInt;
begin
 Result := TNodeList(FSource).MaxCount;
end;

function TNodeListAdapter.GetRootNode: INode;
begin
 with TNodeList(FSource) do
  if RootNode <> nil then
   Result := FNodeAdapterClass.Create(RootNode) else
   Result := nil;
end;

function TNodeListAdapter.InsertList(const Source: INodeList;
  AIndex: LongInt): INode;
var
 Node: TNode;
begin
 if Source <> nil then
 begin
  Node := TNodeList(FSource).InsertList(TNodeList(Source.GetPtr), AIndex);
  if Node <> nil then
  begin
   Result := FNodeAdapterClass.Create(Node);
   Exit;
  end;
 end;
 Result := nil;
end;

function TNodeListAdapter.InsertNode(const Source: INode;
  AIndex: LongInt): INode;
var
 Node: TNode;
begin
 if Source <> nil then
 begin
  Node := TNodeList(FSource).InsertNode(TNode(Source.GetPtr), AIndex);
  if Node <> nil then
  begin
   Result := FNodeAdapterClass.Create(Node);
   Exit;
  end;
 end;
 Result := nil;
end;

function TNodeListAdapter.IsChild(const Node: INode): Boolean;
var
 N: TNode;
begin
 Result := Node <> nil;
 if Result then
 begin
  N := TNode(Node.GetPtr);
  Result := (N <> nil) and TNodeList(FSource).IsChild(N);
 end;
end;

procedure TNodeListAdapter.MoveTo(CurIndex, NewIndex: LongInt);
begin
 TNodeList(FSource).MoveTo(CurIndex, NewIndex);
end;

procedure TNodeListAdapter.RemoveByIndex(Index: LongInt);
begin
 TNodeList(FSource).Remove(Index);
end;

procedure TNodeListAdapter.RemoveNode(const Node: INode);
begin
 if Node <> nil then
  TNodeList(FSource).Remove(TNode(Node.GetPtr));
end;

procedure TNodeListAdapter.SetCount(Value: LongInt);
begin
 TNodeList(FSource).Count := Value;
end;

procedure TNodeListAdapter.SetMaxCount(Value: LongInt);
begin
 TNodeList(FSource).MaxCount := Value;
end;

{ TPropertyAdapter }

constructor TPropertyAdapter.Create(Source: TPropertyListItem;
  FreeAfterUse: Boolean);
begin
 inherited Create(Source, FreeAfterUse);
end;

function TPropertyAdapter.DataGet: Pointer;
begin
 Result := TPropertyListItem(FSource).DataStream.Memory;
end;

function TPropertyAdapter.DataSizeGet: LongInt;
begin
 Result := TPropertyListItem(FSource).DataStream.Size;
end;

procedure TPropertyAdapter.DataSizeSet(Value: LongInt);
begin
 TPropertyListItem(FSource).DataStream.Size := Value;
end;

function TPropertyAdapter.DefValStrGet: PWideChar;
begin
 Result := Pointer(TPropertyListItem(FSource).DefaultStr);
end;

procedure TPropertyAdapter.DefValStrSet(Value: PWideChar);
begin
 TPropertyListItem(FSource).DefaultStr := Value;
end;

function TPropertyAdapter.GetDataStream: IStream32;
begin
 Result := TStreamAdapter.Create(TPropertyListItem(FSource).DataStream);
end;

function TPropertyAdapter.GetFloat: Double;
begin
 Result := TPropertyListItem(FSource).Float;
end;

function TPropertyAdapter.GetItemOwner: IPropertyListItem;
begin
 with TPropertyListItem(FSource) do
  if ItemOwner <> nil then
   Result := TPropertyAdapter.Create(ItemOwner) else
   Result := nil;
end;

function TPropertyAdapter.GetMaxLength: LongInt;
begin
 Result := TPropertyListItem(FSource).MaxLength;
end;

function TPropertyAdapter.GetModified: Boolean;
begin
 Result := TPropertyListItem(FSource).Modified;
end;

{function TPropertyAdapter.GetOnValueChange: TValueChangeProc;
begin
 Result := TPropertyListItem(FSource).ValueChangeProc;
end;
 }
function TPropertyAdapter.GetPickList: IWideStringList;
begin
 Result := TStringsAdapter.Create(TPropertyListItem(FSource).PickList);
end;

function TPropertyAdapter.GetPickListIndex: LongInt;
begin
 Result := TPropertyListItem(FSource).PickListIndex;
end;

function TPropertyAdapter.GetPrecision: Byte;
begin
 Result := TPropertyListItem(FSource).Precision;
end;

function TPropertyAdapter.GetStep: Int64;
begin
 Result := TPropertyListItem(FSource).Step;
end;

function TPropertyAdapter.HexDigitsGet: Byte;
begin
 Result := TPropertyListItem(FSource).HexDigits;
end;

procedure TPropertyAdapter.HexDigitsSet(Value: Byte);
begin
 TPropertyListItem(FSource).HexDigits := Value;
end;

procedure TPropertyAdapter.InitBoolean(AName: PWideChar; AValue: Boolean);
begin
 TPropertyListItem(FSource).InitBoolean(AName, AValue);
end;

procedure TPropertyAdapter.InitData(AName, Description: PWideChar);
begin
 TPropertyListItem(FSource).InitData(AName, Description);
end;

procedure TPropertyAdapter.InitDecimal(AName: PWideChar; const AValue,
  AMin, AMax: Int64);
begin
 TPropertyListItem(FSource).InitDecimal(AName, AValue, AMin, AMax);
end;

procedure TPropertyAdapter.InitFloat(AName: PWideChar;
  const AValue: Double; APrecision: Byte);
begin
 TPropertyListItem(FSource).InitFloat(AName, AValue, APrecision);
end;

procedure TPropertyAdapter.InitHexadecimal(AName: PWideChar; const AValue,
  AMin, AMax: Int64; ADigits: Byte);
begin
 TPropertyListItem(FSource).InitHexadecimal(AName, AValue, AMin, AMax, ADigits);
end;

procedure TPropertyAdapter.InitPickList(AName: PWideChar;
  const AList: IWideStringList; AIndex: LongInt; AFixed: Boolean);
begin
 with TPropertyListItem(FSource) do
 if AList = nil then
 begin
  InitPickList(AName, [], AIndex);
  if AFixed then
   Parameters := PL_FIXEDPICK else
   Parameters := 0;
 end else
  InitPickList(AName, TTntStringList(AList.GetPtr), False, AIndex, AFixed);
end;

procedure TPropertyAdapter.InitPickList2(AName, AList: PWideChar;
                 AIndex: LongInt; AFixed: Boolean);
begin
 TPropertyListItem(FSource).InitPickList(AName, AList, AIndex, AFixed);
end;

procedure TPropertyAdapter.InitPickList3(AName, AValue, AList: PWideChar;
  AFixed: Boolean);
begin
 TPropertyListItem(FSource).InitPickList(AName, AValue, AList, AFixed);
end;

procedure TPropertyAdapter.InitString(AName, AValue: PWideChar;
  Editable: Boolean);
begin
 TPropertyListItem(FSource).InitString(AName, AValue, Editable);
end;

function TPropertyAdapter.MaxGet: Int64;
begin
 Result := TPropertyListItem(FSource).ValueMax;
end;

procedure TPropertyAdapter.MaxSet(const Value: Int64);
begin
 TPropertyListItem(FSource).ValueMax := Value;
end;

function TPropertyAdapter.MinGet: Int64;
begin
 Result := TPropertyListItem(FSource).ValueMin;
end;

procedure TPropertyAdapter.MinSet(const Value: Int64);
begin
 TPropertyListItem(FSource).ValueMin := Value;
end;

function TPropertyAdapter.NameGet: PWideChar;
begin
 Result := Pointer(TPropertyListItem(FSource).Name);
end;

procedure TPropertyAdapter.NameSet(Value: PWideChar);
begin
 TPropertyListItem(FSource).Name := Value;
end;

function TPropertyAdapter.ParamsGet: LongInt;
begin
 Result := TPropertyListItem(FSource).Parameters;
end;

procedure TPropertyAdapter.ParamsSet(Value: LongInt);
begin
 TPropertyListItem(FSource).Parameters := Value;
end;

function TPropertyAdapter.ReadOnlyGet: Boolean;
begin
 Result := TPropertyListItem(FSource).ReadOnly;
end;

procedure TPropertyAdapter.ReadOnlySet(Value: Boolean);
begin
 TPropertyListItem(FSource).ReadOnly := Value;
end;

procedure TPropertyAdapter.SetFloat(const Value: Double);
begin
 TPropertyListItem(FSource).Float := Value;
end;

procedure TPropertyAdapter.SetMaxLength(Value: LongInt);
begin
 TPropertyListItem(FSource).MaxLength := Value;
end;

{procedure TPropertyAdapter.SetOnValueChange(Value: TValueChangeProc);
begin
 TPropertyListItem(FSource).ValueChangeProc := Value;
end;
 }
procedure TPropertyAdapter.SetPickListIndex(Value: LongInt);
begin
 TPropertyListItem(FSource).PickListIndex := Value;
end;

procedure TPropertyAdapter.SetPrecision(Value: Byte);
begin
 TPropertyListItem(FSource).Precision := Value;
end;

procedure TPropertyAdapter.SetStep(const Value: Int64);
begin
 TPropertyListItem(FSource).Step := Value;
end;

function TPropertyAdapter.SubPropsGet: IPropertyList;
begin
 Result := TPropertyListAdapter.Create(TPropertyListItem(FSource).SubProperties);
end;

function TPropertyAdapter.ValueGet: Int64;
begin
 Result := TPropertyListItem(FSource).Value;
end;

procedure TPropertyAdapter.ValueSet(const Value: Int64);
begin
 TPropertyListItem(FSource).Value := Value;
end;

function TPropertyAdapter.ValueStrGet: PWideChar;
begin
 Result := Pointer(TPropertyListItem(FSource).ValueStr);
end;

procedure TPropertyAdapter.ValueStrSet(Value: PWideChar);
begin
 TPropertyListItem(FSource).ValueStr := Value;
end;

function TPropertyAdapter.ValueTypeGet: TValueType;
begin
 Result := TValueType(TPropertyListItem(FSource).ValueType);
end;

procedure TPropertyAdapter.ValueTypeSet(Value: TValueType);
begin
 TPropertyListItem(FSource).ValueType := PropContainer.TValueType(Value);
end;

{ TPropertyListAdapter }

function TPropertyListAdapter.AddProperty: IPropertyListItem;
begin
 Result := TPropertyAdapter.Create(TPropertyList(FSource).AddProperty);
end;

function TPropertyListAdapter.BooleanAdd(Name: PWideChar;
  Value: Boolean): IPropertyListItem;
begin
  Result := TPropertyAdapter.Create(
   TPropertyList(FSource).AddBoolean(Name, Value));
end;

constructor TPropertyListAdapter.Create(Source: TPropertyList;
  FreeAfterUse: Boolean);
begin
 inherited Create(Source, FreeAfterUse);
end;

function TPropertyListAdapter.DataAdd(Name,
  Description: PWideChar): IPropertyListItem;
begin
  Result := TPropertyAdapter.Create(
   TPropertyList(FSource).AddData(Name, Description));
end;

function TPropertyListAdapter.DecimalAdd(Name: PWideChar; const Value, Min,
  Max: Int64): IPropertyListItem;
begin
 Result := TPropertyAdapter.Create(
   TPropertyList(FSource).AddDecimal(Name, Value, Min, Max));
end;

function TPropertyListAdapter.FilePickAdd(Name, FileName, Filter,
  DefaultExt, InitialDir: PWideChar; Save: Boolean;
  Options: LongInt): IPropertyListItem;
var
 Temp: TOpenOptions;
 I: TOpenOption;
begin
 Temp := [];
 for I := Low(TOpenOption) to High(TOpenOption) do
 begin
  if Options and 1 <> 0 then Include(Temp, I);
  Options := Options shr 1;
 end;
 Result := TPropertyAdapter.Create(
   TPropertyList(FSource).AddFilePick(Name, FileName, Filter,
                           DefaultExt, InitialDir, Save, Temp), False);
end;


function TPropertyListAdapter.FloatAdd(AName: PWideChar;
  const AValue: Double; APrecision: Byte): IPropertyListItem;
begin
 Result := TPropertyAdapter.Create(
   TPropertyList(FSource).AddFloat(AName, AValue, APrecision));
end;
                     {
function TPropertyListAdapter.GetOnValueChange: TValueChangeProc;
begin
 Result := TPropertyListItem(FSource).ValueChangeProc;
end;                  }

function TPropertyListAdapter.HexadecimalAdd(Name: PWideChar; const Value,
  Min, Max: Int64; Digits: Byte): IPropertyListItem;
begin
 Result := TPropertyAdapter.Create(
   TPropertyList(FSource).AddHexadecimal(Name, Value, Min, Max, Digits));
end;

procedure TPropertyListAdapter.Initialize;
begin
 FNodeAdapterClass := TPropertyAdapter;
end;

procedure TPropertyListAdapter.LoadFromFile(FileName: PWideChar);
var
 Stream: TStream;
begin
 Stream := TTntFileStream.Create(FileName, fmOpenRead or fmShareDenyWrite);
 try
  TPropertyList(FSource).LoadFromStream(Stream);
 finally
  Stream.Free;
 end;
end;

procedure TPropertyListAdapter.LoadFromStream(const Stream: IReadWrite);
begin
 TPropertyList(FSource).LoadFromStream(TStream(Stream.GetPtr));
end;

function TPropertyListAdapter.PickListAdd(AName: PWideChar;
  const List: IWideStringList; Index: LongInt;
  Fixed: Boolean): IPropertyListItem;
begin
 Result := TPropertyAdapter.Create(
   TPropertyList(FSource).AddPickList(AName, TTntStringList(List.GetPtr), False, Index, Fixed));
end;

function TPropertyListAdapter.PickListAdd2(Name, List: PWideChar; Index: LongInt;
  Fixed: Boolean): IPropertyListItem;
begin
 Result := TPropertyAdapter.Create(
   TPropertyList(FSource).AddPickList(Name, List, Index, Fixed));
end;

function TPropertyListAdapter.PickListAdd3(Name, Value, List: PWideChar;
  Fixed: Boolean): IPropertyListItem;
begin
 Result := TPropertyAdapter.Create(
   TPropertyList(FSource).AddPickList(Name, Value, List, Fixed));
end;

function TPropertyListAdapter.PropertyGet(
  Index: LongInt): IPropertyListItem;
var
 Prop: TPropertyListItem;
begin
 Prop := TPropertyList(FSource).Properties[Index];
 if Prop <> nil then
  Result := TPropertyAdapter.Create(Prop) else
  Result := nil;
end;

procedure TPropertyListAdapter.SaveToFile(FileName: PWideChar);
var
 Stream: TStream;
begin
 Stream := TTntFileStream.Create(FileName, Classes.fmCreate);
 try
  TPropertyList(FSource).SaveToStream(Stream);
 finally
  Stream.Free;
 end;
end;

procedure TPropertyListAdapter.SaveToStream(const Stream: IReadWrite);
begin
 TPropertyList(FSource).SaveToStream(TStream(Stream.GetPtr));
end;

{procedure TPropertyListAdapter.SetOnValueChange(Value: TValueChangeProc);
begin
 TPropertyListItem(FSource).ValueChangeProc := Value;
end;
 }
function TPropertyListAdapter.StringAdd(Name, Value: PWideChar;
  Editable: Boolean): IPropertyListItem;
begin
 Result := TPropertyAdapter.Create(
   TPropertyList(FSource).AddString(Name, Value, Editable));
end;

{ TSectionedItemAdapter }

function TSectionedItemAdapter.CalculateChecksum: LongWord;
begin
 Result := TBaseSectionedList(FSource).Checksum;
end;

function TSectionedItemAdapter.CalculateDataSize: LongInt;
begin
 Result := TBaseSectionedList(FSource).DataSize;
end;

function TSectionedItemAdapter.GetName: PWideChar;
begin
 Result := Pointer(TBaseSectionedList(FSource).Name);
end;

function TSectionedItemAdapter.GetSignature: LongWord;
begin
 Result := TBaseSectionedList(FSource).Signature;
end;

function TSectionedItemAdapter.GetTotalSize: LongInt;
begin
 Result := TBaseSectionedList(FSource).TotalSize;
end;

procedure TSectionedItemAdapter.LoadFromFile(FileName: PWideChar);
begin
 TBaseSectionedList(FSource).LoadFromFile(FileName);
end;

procedure TSectionedItemAdapter.LoadFromStream(const Stream: IReadWrite);
begin
 TBaseSectionedList(FSource).LoadFromStream(TStream(Stream.GetPtr));
end;

procedure TSectionedItemAdapter.SaveToFile(FileName: PWideChar);
begin
 TBaseSectionedList(FSource).SaveToFile(FileName);
end;

procedure TSectionedItemAdapter.SaveToStream(const Stream: IReadWrite);
begin
 TBaseSectionedList(FSource).SaveToStream(TStream(Stream.GetPtr));
end;

procedure TSectionedItemAdapter.SetName(Value: PWideChar);
begin
 TBaseSectionedList(FSource).Name := Value;
end;

{ TSectionedList }

function TSectionedListAdapter.AddChild(AName: PWideChar): ISectionedItem;
begin
 Result := TSectionedItemAdapterClass(FNodeAdapterClass).Create(
   TBaseSectionedList(FSource).AddChild(AName));
end;

function TSectionedListAdapter.FindByContent(AChecksum: LongWord;
  ADataSize: LongInt): ISectionedItem;
var
 Item: TBaseSectionedList;
begin
 Item := TBaseSectionedList(FSource).FindByContent(ACheckSum, ADataSize);
 if Item <> nil then
  Result := TSectionedItemAdapterClass(FNodeAdapterClass).Create(Item) else
  Result := nil;
end;

function TSectionedListAdapter.FindByName(AName: PWideChar): ISectionedItem;
var
 Item: TBaseSectionedList;
begin
 Item := TBaseSectionedList(FSource).FindByName(AName);
 if Item <> nil then
  Result := TSectionedItemAdapterClass(FNodeAdapterClass).Create(Item) else
  Result := nil;
end;

function TSectionedListAdapter.FindIndexByFirstLetters(
  ALetters: PWideChar): LongInt;
begin
 Result := TBaseSectionedList(FSource).FindIndexByFirstLetters(ALetters);
end;

{ TColorFormatAdapter }

procedure TColorFormatAdapter.Assign(const ClrFmt: IColorFormat);
begin
 if ClrFmt <> nil then
  TColorFormat(FSource).Assign(TColorFormat(ClrFmt.GetPtr)) else
  TColorFormat(FSource).SetFormat(0, 0, 0, 0);
end;

procedure TColorFormatAdapter.AssignMask(const ClrFmt: IColorFormat);
begin
 if ClrFmt <> nil then
  TColorFormat(FSource).AssignMask(TColorFormat(ClrFmt.GetPtr)) else
  TColorFormat(FSource).SetFormat(0, 0, 0, 0);
end;

procedure TColorFormatAdapter.ConvertFromRGBQuad(Source, Dest: Pointer;
  Count: LongInt);
begin
 TColorFormat(FSource).ConvertFromRGBQuad(Source, Dest, Count);
end;

procedure TColorFormatAdapter.ConvertTo(const DestFormat: IColorFormat;
  Source, Dest: Pointer; Count: LongInt);
begin
 TColorFormat(FSource).ConvertTo(TColorFormat(DestFormat.GetPtr),
   Source, Dest, Count);
end;

procedure TColorFormatAdapter.ConvertTo2(DestFormat: PAnsiChar; Source,
  Dest: Pointer; Count: LongInt);
begin
 TColorFormat(FSource).ConvertTo(DestFormat, Source, Dest, Count);
end;

procedure TColorFormatAdapter.ConvertToRGBQuad(Source, Dest: Pointer;
  Count: LongInt);
begin
 TColorFormat(FSource).ConvertToRGBQuad(Source, Dest, Count);
end;

function TColorFormatAdapter.ConvertValue(const DestFormat: IColorFormat;
  const Source): LongWord;
begin
 Result := TColorFormat(FSource).ConvertValue(TColorFormat(DestFormat.GetPtr),
                                    Source);
end;

function TColorFormatAdapter.ConvertValue2(DestFormat: PAnsiChar;
  const Source): LongWord;
begin
 Result := TColorFormat(FSource).ConvertValue(DestFormat, Source);
end;

constructor TColorFormatAdapter.Create(Source: TColorFormat;
  FreeAfterUse: Boolean);
begin
 inherited Create(Source, FreeAfterUse);
end;

function TColorFormatAdapter.FromRGBQuad(Source: LongWord): LongWord;
begin
 Result := TColorFormat(FSource).FromRGBQuad(Source);
end;

function TColorFormatAdapter.GetAlpha: Boolean;
begin
 Result := TColorFormat(FSource).Alpha;
end;

function TColorFormatAdapter.GetAlphaCount: Byte;
begin
 Result := TColorFormat(FSource).AlphaCount;
end;

function TColorFormatAdapter.GetAlphaMask: LongWord;
begin
 Result := TColorFormat(FSource).AlphaMask;
end;

function TColorFormatAdapter.GetAlphaRShift: Byte;
begin
 Result := TColorFormat(FSource).AlphaByteShift;
end;

function TColorFormatAdapter.GetAlphaShift: Byte;
begin
 Result := TColorFormat(FSource).AlphaShift;
end;

function TColorFormatAdapter.GetBlueCount: Byte;
begin
 Result := TColorFormat(FSource).BlueCount;
end;

function TColorFormatAdapter.GetBlueMask: LongWord;
begin
 Result := TColorFormat(FSource).BlueMask;
end;

function TColorFormatAdapter.GetBlueRShift: Byte;
begin
 Result := TColorFormat(FSource).BlueByteShift;
end;

function TColorFormatAdapter.GetBlueShift: Byte;
begin
 Result := TColorFormat(FSource).BlueShift;
end;

function TColorFormatAdapter.GetByteSwap: Boolean;
begin
 Result := TColorFormat(FSource).ByteSwap;
end;

function TColorFormatAdapter.GetChannelsCount: LongInt;
begin
 Result := TColorFormat(FSource).ChannelsCount;
end;

function TColorFormatAdapter.GetColorBits: LongInt;
begin
 Result := TColorFormat(FSource).ColorBits;
end;

function TColorFormatAdapter.GetColorSize: LongInt;
begin
 Result := TColorFormat(FSource).ColorSize;
end;

function TColorFormatAdapter.GetFlags: LongWord;
begin
 Result := TColorFormat(FSource).Flags;
end;

function TColorFormatAdapter.GetFormatString: PAnsiChar;
begin
 FTemp := TColorFormat(FSource).FormatString;
 Result := Pointer(FTemp);
end;

function TColorFormatAdapter.GetGreenCount: Byte;
begin
 Result := TColorFormat(FSource).GreenCount;
end;

function TColorFormatAdapter.GetGreenMask: LongWord;
begin
 Result := TColorFormat(FSource).GreenMask;
end;

function TColorFormatAdapter.GetGreenRShift: Byte;
begin
 Result := TColorFormat(FSource).GreenByteShift;
end;

function TColorFormatAdapter.GetGreenShift: Byte;
begin
 Result := TColorFormat(FSource).GreenShift;
end;

function TColorFormatAdapter.GetRedCount: Byte;
begin
 Result := TColorFormat(FSource).RedCount;
end;

function TColorFormatAdapter.GetRedMask: LongWord;
begin
 Result := TColorFormat(FSource).RedMask;
end;

function TColorFormatAdapter.GetRedRShift: Byte;
begin
 Result := TColorFormat(FSource).RedByteShift;
end;

function TColorFormatAdapter.GetRedShift: Byte;
begin
 Result := TColorFormat(FSource).RedShift;
end;

function TColorFormatAdapter.GetUsedBits: LongInt;
begin
 Result := TColorFormat(FSource).UsedBits;
end;

function TColorFormatAdapter.SameAs(const CFormat: IColorFormat): Boolean;
begin
 Result := TColorFormat(FSource).SameAs(TColorFormat(CFormat.GetPtr));
end;

procedure TColorFormatAdapter.SetAlpha(Value: Boolean);
begin
 TColorFormat(FSource).Alpha := Value;
end;

procedure TColorFormatAdapter.SetByteSwap(Value: Boolean);
begin
 TColorFormat(FSource).ByteSwap := Value;
end;

procedure TColorFormatAdapter.SetColor(Dest: Pointer; Index: LongInt;
  RGBQuad: LongWord);
begin
 TColorFormat(FSource).SetColor(Dest, Index, RGBQuad);
end;

procedure TColorFormatAdapter.SetColor2(var Dest; RGBQuad: LongWord);
begin
 TColorFormat(FSource).SetColor(Dest, RGBQuad);
end;

procedure TColorFormatAdapter.SetColorSize(Value: LongInt);
begin
 TColorFormat(FSource).ColorSize := Value;
end;

procedure TColorFormatAdapter.SetFlags(Value: LongWord);
begin
 TColorFormat(FSource).Flags := Value;
end;

procedure TColorFormatAdapter.SetFormat(RBitMask, GBitMask, BBitMask,
  ABitMask: LongWord);
begin
 TColorFormat(FSource).SetFormat(RBitMask, GBitMask, BBitMask, ABitMask);
end;

procedure TColorFormatAdapter.SetFormatString(Str: PAnsiChar);
begin
 TColorFormat(FSource).FormatString := Str;
end;

procedure TColorFormatAdapter.SetUsedBits(Value: LongInt);
begin
 TColorFormat(FSource).UsedBits := Value;
end;

function TColorFormatAdapter.ToRGBQuad(const Source): LongWord;
begin
 Result := TColorFormat(FSource).ToRGBQuad(Source);
end;

function TColorFormatAdapter.ValueToRGBQuad(Value: LongWord): LongWord;
begin
 Result := TColorFormat(FSource).ValueToRGBQuad(Value);
end;

{ TColorTableAdapter }

constructor TColorTableAdapter.Create(Source: TColorTable;
  FreeAfterUse: Boolean);
begin
 inherited Create(Source, FreeAfterUse);
end;

function TColorTableAdapter.GetColor(X: LongInt): LongWord;
begin
 Result := TColorTable(FSource).RGBZ[X];
end;

function TColorTableAdapter.GetColorData: Pointer;
begin
 Result := TColorTable(FSource).ColorData;
end;

function TColorTableAdapter.GetColorFormat: IColorFormat;
begin
 Result := TColorFormatAdapter.Create(TColorTable(FSource).ColorFormat);
end;

function TColorTableAdapter.GetColorsCount: LongInt;
begin
 Result := TColorTable(FSource).ColorsCount;
end;

function TColorTableAdapter.GetFormatString: PAnsiChar;
begin
 FTemp := TColorTable(FSource).ColorFormatString;
 Result := Pointer(FTemp);
end;

function TColorTableAdapter.GetRGBQuad(X: LongInt): LongWord;
begin
 Result := TColorTable(FSource).BGRA[X];
end;

function TColorTableAdapter.GetTableSize: LongInt;
begin
 Result := TColorTable(FSource).TableSize;
end;

function TColorTableAdapter.GetTransparentIndex: LongInt;
begin
 Result := TColorTable(FSource).TransparentIndex;
end;

procedure TColorTableAdapter.LoadActFromStream(const Stream: IReadWrite);
begin
 TColorTable(FSource).LoadActFromStream(TStream(Stream.GetPtr));
end;

procedure TColorTableAdapter.Reset(const AColorFmt: IColorFormat;
  ACount: LongInt);
begin
 TColorTable(FSource).Reset(TColorFormat(AColorFmt.GetPtr), ACount);
end;

procedure TColorTableAdapter.Reset2(AColorFmt: PAnsiChar; ACount: LongInt);
begin
 TColorTable(FSource).Reset(AColorFmt, ACount);
end;

procedure TColorTableAdapter.SaveActToStream(const Stream: IReadWrite);
begin
 TColorTable(FSource).SaveActToStream(TStream(Stream.GetPtr));
end;

procedure TColorTableAdapter.SetColor(X: LongInt; Value: LongWord);
begin
 TColorTable(FSource).RGBZ[X] := Value;
end;

procedure TColorTableAdapter.SetColorFormat(const Value: IColorFormat);
begin
 TColorTable(FSource).ColorFormat := TColorFormat(Value.GetPtr);
end;

procedure TColorTableAdapter.SetColorsCount(Value: LongInt);
begin
 TColorTable(FSource).ColorsCount := Value;
end;

procedure TColorTableAdapter.SetFormatString(Value: PAnsiChar);
begin
 TColorTable(FSource).ColorFormatString := Value;
end;

procedure TColorTableAdapter.SetRGBQuad(X: LongInt; Value: LongWord);
begin
 TColorTable(FSource).BGRA[X] := Value;
end;

procedure TColorTableAdapter.SetTransparentIndex(Value: LongInt);
begin
 TColorTable(FSource).TransparentIndex := Value;
end;

{ TColorTableListAdapter }

function TColorTableListAdapter.AddColorTable(AName: PWideChar;
  const AColorFmt: IColorFormat; ACount: LongInt): IColorTable;
begin
 Result := TColorTableAdapter.Create(
  TColorTableList(FSource).AddColorTable(AName,
   TColorFormat(AColorFmt.GetPtr), ACount));
end;

function TColorTableListAdapter.AddColorTable2(AName: PWideChar;
  AColorFmt: PAnsiChar; ACount: LongInt): IColorTable;
begin
 Result := TColorTableAdapter.Create(
  TColorTableList(FSource).AddColorTable(AName, AColorFmt, ACount));
end;

constructor TColorTableListAdapter.Create(Source: TColorTableList;
  FreeAfterUse: Boolean);
begin
 inherited Create(Source, FreeAfterUse);
end;

function TColorTableListAdapter.GetItem(Index: LongInt): IColorTable;
var
 Item: TColorTable;
begin
 Item := TColorTableList(FSource).Items[Index];
 if Item <> nil then
  Result := TColorTableAdapter.Create(Item) else
  Result := nil;
end;

procedure TColorTableListAdapter.Initialize;
begin
 FNodeAdapterClass := TColorTableAdapter;
end;

{ TBitmapContainerAdapter }

constructor TBitmapContainerAdapter.Create(Source: TBitmapContainer;
  FreeAfterUse: Boolean);
begin
 inherited Create(Source, FreeAfterUse);
end;

function TBitmapContainerAdapter.GetBitsCountTotal: LongInt;
begin
 Result := TBitmapContainer(FSource).BitsCount;
end;

function TBitmapContainerAdapter.GetColorFormat: IColorFormat;
begin
 Result := TColorFormatAdapter.Create(TBitmapContainer(FSource).ColorFormat);
end;

function TBitmapContainerAdapter.GetColorTable: Pointer;
begin
 Result := TBitmapContainer(FSource).ColorTable;
end;

function TBitmapContainerAdapter.GetFlagsCount: LongInt;
begin
 with TBitmapContainer(FSource) do
  Result := Length(CompositeFlags) + 1;
end;

procedure TBitmapContainerAdapter.GetFlagsList(var Dest);
var
 PW: PWord;
begin
 PW := @Dest;
 with TBitmapContainer(FSource) do
 begin
  PW^ := ImageFlags;
  Inc(PW);
  Move(CompositeFlags[0], PW^, Length(CompositeFlags) shl 1);
 end;
end;

function TBitmapContainerAdapter.GetHeight: Word;
begin
 Result := TBitmapContainer(FSource).Height;
end;

function TBitmapContainerAdapter.GetImageData: Pointer;
begin
 Result := TBitmapContainer(FSource).ImageData;
end;

function TBitmapContainerAdapter.GetImageFlags: Word;
begin
 Result := TBitmapContainer(FSource).ImageFlags;
end;

function TBitmapContainerAdapter.GetImageFormat: Byte;
begin
 Result := Ord(TBitmapContainer(FSource).ImageFormat);
end;

function TBitmapContainerAdapter.GetImageSize: LongInt;
begin
 Result := TBitmapContainer(FSource).ImageSize;
end;

function TBitmapContainerAdapter.GetLeft: LongInt;
begin
 Result := TBitmapContainer(FSource).Left;
end;

function TBitmapContainerAdapter.GetPaletteCount: LongInt;
begin
 Result := TBitmapContainer(FSource).PaletteCount;
end;

function TBitmapContainerAdapter.GetPaletteSize: LongInt;
begin
 Result := TBitmapContainer(FSource).PaletteSize;
end;

function TBitmapContainerAdapter.GetPixelModifier: LongWord;
begin
 Result := TBitmapContainer(FSource).PixelModifier;
end;

function TBitmapContainerAdapter.GetTop: LongInt;
begin
 Result := TBitmapContainer(FSource).Top;
end;

function TBitmapContainerAdapter.GetTransparentColor: LongWord;
begin
 Result := TBitmapContainer(FSource).TransparentColor;
end;

function TBitmapContainerAdapter.GetWidth: Word;
begin
 Result := TBitmapContainer(FSource).Width;
end;

function TBitmapContainerAdapter.GetWidthBytes: LongInt;
begin
 with TBitmapContainer(FSource) do
  Result := Width + WidthRemainder;
end;

function TBitmapContainerAdapter.GetWidthRemainder: Word;
begin
 Result := TBitmapContainer(FSource).WidthRemainder;
end;

procedure TBitmapContainerAdapter.SetColorFormat(
  const Value: IColorFormat);
begin
 TBitmapContainer(FSource).ColorFormat := TColorFormat(Value.GetPtr);
end;

procedure TBitmapContainerAdapter.SetColorTable(Value: Pointer);
begin
 TBitmapContainer(FSource).ColorTable := Value;
end;

procedure TBitmapContainerAdapter.SetFlagsList(const Value: array of Word);
begin
 TBitmapContainer(FSource).SetFlags(Value);
end;

procedure TBitmapContainerAdapter.SetHeight(Value: Word);
begin
 TBitmapContainer(FSource).Height := Value;
end;

procedure TBitmapContainerAdapter.SetImageData(Value: Pointer);
begin
 TBitmapContainer(FSource).ImageData := Value;
end;

procedure TBitmapContainerAdapter.SetImageFlags(Value: Word);
begin
 TBitmapContainer(FSource).ImageFlags := Value;
end;

procedure TBitmapContainerAdapter.SetImageFormat(Value: Byte);
begin
 TBitmapContainer(FSource).ImageFormat := BitmapEx.TImageFormat(Value);
end;

procedure TBitmapContainerAdapter.SetLeft(Value: LongInt);
begin
 TBitmapContainer(FSource).Left := Value;
end;

procedure TBitmapContainerAdapter.SetPixelModifier(Value: LongWord);
begin
 TBitmapContainer(FSource).PixelModifier := Value;
end;

procedure TBitmapContainerAdapter.SetTop(Value: LongInt);
begin
 TBitmapContainer(FSource).Top := Value;
end;

procedure TBitmapContainerAdapter.SetTransparentColor(Value: LongWord);
begin
 TBitmapContainer(FSource).TransparentColor := Value;
end;

procedure TBitmapContainerAdapter.SetWidth(Value: Word);
begin
 TBitmapContainer(FSource).Width := Value;
end;

procedure TBitmapContainerAdapter.SetWidthRemainder(Value: Word);
begin
 TBitmapContainer(FSource).WidthRemainder := Value;
end;

{ TBitmapConverterAdapter }

constructor TBitmapConverterAdapter.Create(Source: TBitmapConverter;
  FreeAfterUse: Boolean);
begin
 inherited Create(Source, FreeAfterUse);
end;

procedure TBitmapConverterAdapter.Reset(const SrcFlags,
  DstFlags: array of Word; DrawFlags: LongInt);
begin
 TBitmapConverter(FSource).Reset(SrcFlags, DstFlags, DrawFlags);
end;

{ TTileItemAdapter }

procedure TTileItemAdapter.AssignAttributes(const Source: ITileItem);
begin
 if Source <> nil then
  TTileItem(FSource).AssignAttributes(TTileItem(Source.GetPtr)) else
  TTileItem(FSource).AssignAttributes(nil);
end;

constructor TTileItemAdapter.Create(Source: TTileItem;
  FreeAfterUse: Boolean);
begin
 inherited Create(Source, FreeAfterUse);
end;

procedure TTileItemAdapter.CvtDraw(const Dest: IBitmapContainer; X,
  Y: LongInt; const Cvt: IBitmapConverter);
begin
 TTileItem(FSource).Draw(TBitmapContainer(Dest.GetPtr), X, Y,
                         TBitmapConverter(Cvt.GetPtr));
end;

procedure TTileItemAdapter.CvtLoad(const Src: IBitmapContainer; X,
  Y: LongInt; const Cvt: IBitmapConverter);
begin
 TTileItem(FSource).Load(TBitmapContainer(Src.GetPtr), X, Y,
                         TBitmapConverter(Cvt.GetPtr));
end;

procedure TTileItemAdapter.Draw(const Dest: IBitmapContainer; X, Y,
  ADrawFlags: LongInt);
begin
 TTileItem(FSource).Draw(TBitmapContainer(Dest.GetPtr), X, Y, ADrawFlags);
end;

function TTileItemAdapter.GetAttribute(Index: LongInt): OleVariant;
begin
 Result := TTileItem(FSource).Attributes[Index];
end;

function TTileItemAdapter.GetBufferIndex: LongInt;
begin
 Result := TTileItem(FSource).BufferIndex;
end;

function TTileItemAdapter.GetFirstColor: Byte;
begin
 Result := TTileItem(FSource).FirstColor;
end;

function TTileItemAdapter.GetNamedAttr(Name: PWideChar): OleVariant;
begin
 Result := TTileItem(FSource).NamedAttr[Name];
end;

function TTileItemAdapter.GetTileData: Pointer;
begin
 Result := TTileItem(FSource).TileData;
end;

function TTileItemAdapter.GetTileHeight: LongInt;
begin
 Result := TTileItem(FSource).Height;
end;

function TTileItemAdapter.GetTileIndex: LongInt;
begin
 Result := TTileItem(FSource).TileIndex;
end;

function TTileItemAdapter.GetTileSize: LongInt;
begin
 Result := TTileItem(FSource).TileSize;
end;

function TTileItemAdapter.GetTileWidth: LongInt;
begin
 Result := TTileItem(FSource).Width;
end;

procedure TTileItemAdapter.Load(const Src: IBitmapContainer; X, Y,
  ADrawFlags: LongInt);
begin
 TTileItem(FSource).Load(TBitmapContainer(Src.GetPtr), X, Y, ADrawFlags);
end;

procedure TTileItemAdapter.SetAttribute(Index: LongInt;
  const Value: OleVariant);
begin
 TTileItem(FSource).Attributes[Index] := Value;
end;

procedure TTileItemAdapter.SetFirstColor(Value: Byte);
begin
 TTileItem(FSource).FirstColor := Value;
end;

procedure TTileItemAdapter.SetNamedAttr(Name: PWideChar;
  const Value: OleVariant);
begin
 TTileItem(FSource).NamedAttr[Name] := Value;
end;

procedure TTileItemAdapter.SetTileIndex(Value: LongInt);
begin
 TTileItem(FSource).TileIndex := Value;
end;

{ TTileSetAdapter }

function TTileSetAdapter.AddFixed(Index: LongInt): ITileItem;
begin
 Result := TTileItemAdapter.Create(TTileSet(FSource).AddFixed(Index));
end;

function TTileSetAdapter.AddOptimized(const Source): ITileItem;
begin
 Result := TTileItemAdapter.Create(TTileSet(FSource).AddOptimized(Source));
end;

function TTileSetAdapter.AddOptimizedEx(const Source;
  const Attr: array of OleVariant; var XFlip, YFlip: Boolean): ITileItem;
var
 Arr: array of Variant;
 I: Integer;
begin
 SetLength(Arr, Length(Attr));
 for I := 0 to Length(Attr) - 1 do
  Arr[I] := Attr[I];

 Result := TTileItemAdapter.Create(
   TTileSet(FSource).AddOptimized(Source, Arr, XFlip, YFlip));
end;

function TTileSetAdapter.AddTile: ITileItem;
begin
 Result := TTileItemAdapter.Create(TTileSet(FSource).AddTile);
end;

function TTileSetAdapter.AddTileEx(
  const AttrList: array of OleVariant): ITileItem;
var
 Arr: array of Variant;
 I: Integer;
begin
 SetLength(Arr, Length(AttrList));
 for I := 0 to Length(AttrList) - 1 do
  Arr[I] := AttrList[I];

 Result := TTileItemAdapter.Create(TTileSet(FSource).AddTile(Arr));
end;

procedure TTileSetAdapter.AssignAttrDefs(const Source: ITileSet);
begin
 if Source <> nil then
  TTileSet(FSource).AssignAttrDefs(TTileSet(Source.GetPtr)) else
  TTileSet(FSource).AttrCount := 0;
end;

procedure TTileSetAdapter.AssignTileFormat(const Source: ITileSet);
begin
 if Source <> nil then
  TTileSet(FSource).AssignTileFormat(TTileSet(Source.GetPtr)) else
  TTileSet(FSource).AssignTileFormat(nil);
end;

procedure TTileSetAdapter.ConvertFrom(const Source: ITileSet);
begin
 if Source <> nil then
  TTileSet(FSource).ConvertFrom(TTileSet(Source.GetPtr)) else
  TTileSet(FSource).ConvertFrom(nil);
end;

constructor TTileSetAdapter.Create(Source: TTileSet;
  FreeAfterUse: Boolean);
begin
 inherited Create(Source, FreeAfterUse);
end;

procedure TTileSetAdapter.DeleteAttr(Index: LongInt);
begin
 TTileSet(FSource).DeleteAttr(Index);
end;

procedure TTileSetAdapter.FillConverter(const Cvt: IBitmapConverter;
  const Flags: array of Word; ADrawFlags: Word);
begin
 TTileSet(FSource).FillConverter(TBitmapConverter(Cvt.GetPtr),
                                 Flags, ADrawFlags);
end;

procedure TTileSetAdapter.FillConvertRec(var CvtRec: TTileICvtRec;
  const Flags: array of Word; ADrawFlags: Word);
var
 Temp: TTileConvertRec;  
begin
 Finalize(CvtRec);
 ConvertRecInit(Temp);
 TTileSet(FSource).FillConvertRec(Temp, Flags, ADrawFlags);
 CvtRec.tcvtLoad := TBitmapConverterAdapter.Create(Temp.tcvtLoad, True);
 CvtRec.tcvtDraw[False, False] := TBitmapConverterAdapter.Create(
   Temp.tcvtDraw[False, False], True);
 CvtRec.tcvtDraw[False,  True] := TBitmapConverterAdapter.Create(
   Temp.tcvtDraw[False,  True], True);
 CvtRec.tcvtDraw[True,  False] := TBitmapConverterAdapter.Create(
   Temp.tcvtDraw[True,  False], True);
 CvtRec.tcvtDraw[True,   True] := TBitmapConverterAdapter.Create(
   Temp.tcvtDraw[True,   True], True);
end;

function TTileSetAdapter.FindAttrByName(Name: PWideChar): LongInt;
begin
 Result := TTileSet(FSource).FindAttrByName(Name);
end;

function TTileSetAdapter.FindByData(const Source): ITileItem;
var
 Tile: TTileItem;
begin
 Tile := TTileSet(FSource).FindByData(Source);
 if Tile <> nil then
  Result := TTileItemAdapter.Create(Tile) else
  Result := nil;
end;

function TTileSetAdapter.FindEmptyIndex: LongInt;
begin
 Result := TTileSet(FSource).FindEmptyIndex;
end;

function TTileSetAdapter.GetAttrCount: LongInt;
begin
 Result := TTileSet(FSource).AttrCount;
end;

function TTileSetAdapter.GetAttrDef(Index: LongInt): PAttrDef;
begin
 Result := Pointer(TTileSet(FSource).AttrDefs[Index]);
end;

function TTileSetAdapter.GetAttrEnumCount(Index: LongInt): LongInt;
var
 Def: MyClasses.PAttrDef;
begin
 Def := TTileSet(FSource).AttrDefs[Index];
 if Def <> nil then
  Result := Length(Def.adEnum) else
  Result := 0;
end;

function TTileSetAdapter.GetAttrEnumMember(AttrIndex,
  EnumIndex: LongInt): PWideChar;
var
 Def: MyClasses.PAttrDef;
begin
 Def := TTileSet(FSource).AttrDefs[AttrIndex];
 if Def <> nil then
  Result := Pointer(Def.adEnum[EnumIndex]) else
  Result := nil;
end;

function TTileSetAdapter.GetAttrName(Index: LongInt): PWideChar;
var
 Def: MyClasses.PAttrDef;
begin
 Def := TTileSet(FSource).AttrDefs[Index];
 if Def <> nil then
  Result := Pointer(Def.adName) else
  Result := nil;
end;

function TTileSetAdapter.GetBitCount: LongInt;
begin
 Result := TTileSet(FSource).TileBitmap.BitsCount;
end;

function TTileSetAdapter.GetDrawFlags: Word;
begin
 Result := TTileSet(FSource).DrawFlags;
end;

function TTileSetAdapter.GetEmptyTile: ITileItem;
begin
 Result := TTileItemAdapter.Create(TTileSet(FSource).EmptyTile);
end;

function TTileSetAdapter.GetEmptyTileIndex: LongInt;
begin
 Result := TTileSet(FSource).EmptyTileIndex;
end;

function TTileSetAdapter.GetFastList: Boolean;
begin
 Result := TTileSet(FSource).FastList;
end;

function TTileSetAdapter.GetHaveEmpty: Boolean;
begin
 Result := TTileSet(FSource).HaveEmpty;
end;

function TTileSetAdapter.GetIndexed(Index: LongInt): ITileItem;
var
 Tile: TTileItem;
begin
 Tile := TTileSet(FSource).Indexed[Index];
 if Tile <> nil then
  Result := TTileItemAdapter.Create(Tile) else
  Result := nil;
end;

function TTileSetAdapter.GetKeepBuffer: Boolean;
begin
 Result := TTileSet(FSource).KeepBuffer;
end;

function TTileSetAdapter.GetLastIndex: LongInt;
begin
 Result := TTileSet(FSource).LastIndex;
end;

function TTileSetAdapter.GetMaxIndex: LongInt;
begin
 Result := TTileSet(FSource).MaxIndex;
end;

function TTileSetAdapter.GetOptimizationFlags: Word;
begin
 Result := TTileSet(FSource).OptimizationFlags;
end;

function TTileSetAdapter.GetTileBitmap: IBitmapContainer;
begin
 Result := TBitmapContainerAdapter.Create(TTileSet(FSource).TileBitmap);
end;

function TTileSetAdapter.GetTileHeight: LongInt;
begin
 Result := TTileSet(FSource).TileHeight;
end;

function TTileSetAdapter.GetTileItem(Index: LongInt): ITileItem;
var
 Tile: TTileItem;
begin
 Tile := TTileSet(FSource).Items[Index];
 if Tile <> nil then
  Result := TTileItemAdapter.Create(Tile) else
  Result := nil;
end;

function TTileSetAdapter.GetTilesCount: LongInt;
begin
 Result := TTileSet(FSource).LastIndex + 1;
end;

function TTileSetAdapter.GetTileSize: LongInt;
begin
 Result := TTileSet(FSource).TileBitmap.ImageSize;
end;

function TTileSetAdapter.GetTileWidth: LongInt;
begin
 Result := TTileSet(FSource).TileBitmap.Width;
end;

function TTileSetAdapter.GetTransparentColor: LongWord;
begin
 Result := TTileSet(FSource).TileBitmap.TransparentColor;
end;

procedure TTileSetAdapter.Initialize;
begin
 FNodeAdapterClass := TTileItemAdapter;
end;

procedure TTileSetAdapter.MoveAttr(OldIndex, NewIndex: LongInt);
begin
 TTileSet(FSource).MoveAttr(OldIndex, NewIndex);
end;

procedure TTileSetAdapter.ReadFromStream(const Stream: IReadWrite; AIndex,
  ACount: LongInt; AOptimize: Boolean);
begin
 TTileSet(FSource).ReadFromStream(TStream(Stream.GetPtr),
                 AIndex, ACount, AOptimize);
end;

procedure TTileSetAdapter.Reallocate(const Flags: array of Word;
  ATileWidth, ATileHeight: LongInt);
begin
 TTileSet(FSource).Reallocate(Flags, ATileWidth, ATileHeight);
end;

procedure TTileSetAdapter.SetAttrCount(Value: LongInt);
begin
 TTileSet(FSource).AttrCount := Value;
end;

procedure TTileSetAdapter.SetAttrEnumCount(Index, Value: LongInt);
var
 Def: MyClasses.PAttrDef;
begin
 Def := TTileSet(FSource).AttrDefs[Index];
 if Def <> nil then
  SetLength(Def.adEnum, Value);
end;

procedure TTileSetAdapter.SetAttrEnumMember(AttrIndex, EnumIndex: LongInt;
  Value: PWideChar);
var
 Def: MyClasses.PAttrDef;
begin
 Def := TTileSet(FSource).AttrDefs[AttrIndex];
 if Def <> nil then
  Def.adEnum[EnumIndex] := Value;
end;

procedure TTileSetAdapter.SetAttrName(Index: LongInt; Value: PWideChar);
var
 Def: MyClasses.PAttrDef;
begin
 Def := TTileSet(FSource).AttrDefs[Index];
 if Def <> nil then
  Def.adName := Value;
end;

procedure TTileSetAdapter.SetDrawFlags(Value: Word);
begin
 TTileSet(FSource).DrawFlags := Value and DRAW_PIXEL_MODIFY;
end;

procedure TTileSetAdapter.SetFastList(Value: Boolean);
begin
 TTileSet(FSource).FastList := Value;
end;

procedure TTileSetAdapter.SetKeepBuffer(Value: Boolean);
begin
 TTileSet(FSource).KeepBuffer := Value;
end;

procedure TTileSetAdapter.SetLastIndex(Value: LongInt);
begin
 TTileSet(FSource).LastIndex := Value;
end;

procedure TTileSetAdapter.SetMaxIndex(Value: LongInt);
begin
 TTileSet(FSource).MaxIndex := Value;
end;

procedure TTileSetAdapter.SetOptimizationFlags(Value: Word);
begin
 TTileSet(FSource).OptimizationFlags := Value;
end;

procedure TTileSetAdapter.SetTilesCount(Value: LongInt);
begin
 TTileSet(FSource).TilesCount := Value;
end;

procedure TTileSetAdapter.SetTransparentColor(Value: LongWord);
begin
 TTileSet(FSource).TransparentColor := Value;
end;

procedure TTileSetAdapter.SwitchFormat(const Flags: array of Word;
  ATileWidth, ATileHeight: LongInt);
begin
 TTileSet(FSource).SwitchFormat(Flags, ATileWidth, ATileHeight);
end;

procedure TTileSetAdapter.TileCvtDraw(const Source;
  const Dest: IBitmapContainer; X, Y: LongInt;
  const Cvt: IBitmapConverter);
begin
 TTileSet(FSource).TileDraw(Source, TBitmapContainer(Dest.GetPtr), X, Y,
                                    TBitmapConverter(Cvt.GetPtr));
end;

procedure TTileSetAdapter.TileCvtLoad(const Src: IBitmapContainer;
  var Dest; X, Y: LongInt; const Cvt: IBitmapConverter);
begin
 TTileSet(FSource).TileLoad(TBitmapContainer(Src.GetPtr), Dest, X, Y,
                            TBitmapConverter(Cvt.GetPtr));
end;

procedure TTileSetAdapter.TileDraw(const Source;
  const Dest: IBitmapContainer; X, Y, ADrawFlags: LongInt);
begin
 TTileSet(FSource).TileDraw(Source, TBitmapContainer(Dest.GetPtr),
                                    X, Y, ADrawFlags);
end;

procedure TTileSetAdapter.TileFlip(const Source; var Dest; X, Y: Boolean);
begin
 TTileSet(FSource).TileFlip(Source, Dest, X, Y);
end;

procedure TTileSetAdapter.TileLoad(const Src: IBitmapContainer; var Dest;
  X, Y, ADrawFlags: LongInt);
begin
 TTileSet(FSource).TileLoad(TBitmapContainer(Src.GetPtr), Dest,
                            X, Y, ADrawFlags);
end;

procedure TTileSetAdapter.WriteToStream(const Stream: IReadWrite; AIndex,
  ACount: LongInt; AUseEmptyTiles: Boolean);
begin
 TTileSet(FSource).WriteToStream(TStream(Stream.GetPtr), AIndex, ACOunt,
                                                         AUseEmptyTiles);
end;

{ TTileSetListAdapter }

constructor TTileSetListAdapter.Create(Source: TTileSetList;
  FreeAfterUse: Boolean);
begin
 inherited Create(Source, FreeAfterUse);
end;

procedure TTileSetListAdapter.FillCvtRecList(AList: PTileICvtRec;
  const AFlags: array of Word; ADrawFlags: Word);
var
 Temp: TCvtRecList;
 I: Integer;
begin
 with TTileSetList(FSource) do
 begin
  FillCvtRecList(Temp, AFlags, ADrawFlags);
  for I := 0 to Count - 1 do
  begin
   Finalize(AList^);
   with Temp[I] do
   begin
    AList.tcvtLoad := TBitmapConverterAdapter.Create(tcvtLoad, True);
    AList.tcvtDraw[False, False] := TBitmapConverterAdapter.Create(
          tcvtDraw[False, False], True);
    AList.tcvtDraw[False,  True] := TBitmapConverterAdapter.Create(
          tcvtDraw[False,  True], True);
    AList.tcvtDraw[True,  False] := TBitmapConverterAdapter.Create(
          tcvtDraw[True,  False], True);
    AList.tcvtDraw[True,   True] := TBitmapConverterAdapter.Create(
          tcvtDraw[True,   True], True);
   end;
   Inc(AList);
  end;
 end;
end;

function TTileSetListAdapter.GetSet(X: LongInt): ITileSet;
var
 ts: TTileSet;
begin
 ts := TTileSetList(FSource).Sets[X];
 if ts <> nil then
  Result := TTileSetAdapter.Create(ts) else
  Result := nil;
end;

procedure TTileSetListAdapter.Initialize;
begin
 FNodeAdapterClass := TTileSetAdapter;
end;

{ TMapLayerAdapter }

procedure TMapLayerAdapter.AssignLayerFormat(const Source: IMapLayer);
begin
 if Source <> nil then
  TMapLayer(FSource).AssignLayerFormat(TMapLayer(Source.GetPtr)) else
  TMapLayer(FSource).AssignLayerFormat(nil);
end;

procedure TMapLayerAdapter.AssignLayerInfo(const Source: IMapLayer);
begin
 if Source <> nil then
  TMapLayer(FSource).AssignLayerInfo(TMapLayer(Source.GetPtr)) else
  TMapLayer(FSource).AssignLayerInfo(nil);  
end;

function TMapLayerAdapter.CompareLayerFormat(
  const AFmt: TLayerFormatRec): Boolean;
begin
 Result := TMapLayer(FSource).CompareLayerFormat(me_lib.TLayerFormatRec(AFmt))
end;

constructor TMapLayerAdapter.Create(Source: TMapLayer;
  FreeAfterUse: Boolean);
begin
 inherited Create(Source, FreeAfterUse);
end;

procedure TMapLayerAdapter.FillLayerFormat(var AFmt: TLayerFormatRec);
begin
 TMapLayer(FSource).FillLayerFormat(me_lib.TLayerFormatRec(AFmt));
end;

function TMapLayerAdapter.FindCellInfo(const Info: TCellInfo): LongInt;
begin
 Result := TMapLayer(FSource).FindCellInfo(me_lib.TCellInfo(Info));
end;

function TMapLayerAdapter.GetCellInfo(X, Y: LongInt): TCellInfo;
begin
 Result := TCellInfo(TMapLayer(FSource).Cells[X, Y]);
end;

function TMapLayerAdapter.GetCellSize: Byte;
begin
 Result := TMapLayer(FSource).CellSize;
end;

function TMapLayerAdapter.GetDrawInfo: PCellInfo;
begin
 Result := Addr(TMapLayer(FSource).DrawInfo);
end;

function TMapLayerAdapter.GetFirstColor: Byte;
begin
 Result := TMapLayer(FSource).FirstColor;
end;

function TMapLayerAdapter.GetFlags: Byte;
begin
 Result := TMapLayer(FSource).Flags;
end;

function TMapLayerAdapter.GetIndexMask: LongWord;
begin
 Result := TMapLayer(FSource).IndexMask;
end;

function TMapLayerAdapter.GetIndexShift: ShortInt;
begin
 Result := TMapLayer(FSource).IndexShift;
end;

function TMapLayerAdapter.GetLayerData: Pointer;
begin
 Result := TMapLayer(FSource).LayerData;
end;

function TMapLayerAdapter.GetLayerDataSize: LongInt;
begin
 Result := TMapLayer(FSource).LayerDataSize;
end;

function TMapLayerAdapter.GetLayerFormat: TLayerFormatRec;
begin
 TMapLayer(FSource).FillLayerFormat(me_lib.TLayerFormatRec(Result));
end;

function TMapLayerAdapter.GetPaletteIndexMask: LongWord;
begin
 Result := TMapLayer(FSource).PaletteIndexMask;
end;

function TMapLayerAdapter.GetPaletteIndexShift: ShortInt;
begin
 Result := TMapLayer(FSource).PaletteIndexShift;
end;

function TMapLayerAdapter.GetPriorityMask: LongWord;
begin
 Result := TMapLayer(FSource).PriorityMask;
end;

function TMapLayerAdapter.GetPriorityShift: ShortInt;
begin
 Result := TMapLayer(FSource).PriorityShift;
end;

function TMapLayerAdapter.GetTileIndex(X, Y: LongInt): LongInt;
begin
 Result := TMapLayer(FSource).TileIndex[X, Y];
end;

function TMapLayerAdapter.GetTileSet: ITileSet;
var
 ts: TTileSet;
begin
 ts := TMapLayer(FSource).TileSet;
 if ts <> nil then
  Result := TTileSetAdapter.Create(ts) else
  Result := nil;
end;

function TMapLayerAdapter.GetTileSetIndex: LongInt;
begin
 Result := TMapLayer(FSource).TileSetIndex;
end;

function TMapLayerAdapter.GetTilesFilled: Boolean;
begin
 Result := TMapLayer(FSource).TilesFilled;
end;

function TMapLayerAdapter.GetVisibleRect: TRect;
begin
 Result := TMapLayer(FSource).VisibleRect;
end;

function TMapLayerAdapter.GetXFlipShift: ShortInt;
begin
 Result := TMapLayer(FSource).XFlipShift;
end;

function TMapLayerAdapter.GetYFlipShift: ShortInt;
begin
 Result := TMapLayer(FSource).YFlipShift;
end;

procedure TMapLayerAdapter.ReadCellInfo(const Data; var Info: TCellInfo);
begin
 TMapLayer(FSource).ReadCellInfo(Data, me_lib.TCellInfo(Info));
end;

function TMapLayerAdapter.ReadPaletteIndex(var Data): LongInt;
begin
 Result := TMapLayer(FSource).ReadPaletteIndex(Data);
end;

function TMapLayerAdapter.ReadPriority(var Data): LongInt;
begin
 Result := TMapLayer(FSource).ReadPriority(Data);
end;

function TMapLayerAdapter.ReadTileIndex(var Data): LongInt;
begin
 Result := TMapLayer(FSource).ReadTileIndex(Data);
end;

function TMapLayerAdapter.ReadXFlip(var Data): Boolean;
begin
 Result := TMapLayer(FSource).ReadXFlip(Data);
end;

function TMapLayerAdapter.ReadYFlip(var Data): Boolean;
begin
 Result := TMapLayer(FSource).ReadYFlip(Data);
end;

procedure TMapLayerAdapter.SetCellInfo(X, Y: LongInt;
  const Value: TCellInfo);
begin
 TMapLayer(FSource).Cells[X, Y] := me_lib.TCellInfo(Value);
end;

procedure TMapLayerAdapter.SetCellSize(Value: Byte);
begin
 TMapLayer(FSource).CellSize := Value;
end;

procedure TMapLayerAdapter.SetFirstColor(Value: Byte);
begin
 TMapLayer(FSource).FirstColor := Value;
end;

procedure TMapLayerAdapter.SetFlags(Value: Byte);
begin
 TMapLayer(FSource).Flags := Value;
end;

procedure TMapLayerAdapter.SetIndexMask(Value: LongWord);
begin
 TMapLayer(FSource).IndexMask := Value;
end;

procedure TMapLayerAdapter.SetIndexShift(Value: ShortInt);
begin
 TMapLayer(FSource).IndexShift := Value;
end;

procedure TMapLayerAdapter.SetPaletteIndexMask(Value: LongWord);
begin
 TMapLayer(FSource).PaletteIndexMask := Value;
end;

procedure TMapLayerAdapter.SetPaletteIndexShift(Value: ShortInt);
begin
 TMapLayer(FSource).PaletteIndexShift := Value;
end;

procedure TMapLayerAdapter.SetPriorityMask(Value: LongWord);
begin
 TMapLayer(FSource).PriorityMask := Value;
end;

procedure TMapLayerAdapter.SetPriorityShift(Value: ShortInt);
begin
 TMapLayer(FSource).PriorityShift := Value;
end;

procedure TMapLayerAdapter.SetTileIndex(X, Y, Value: LongInt);
begin
 TMapLayer(FSource).TileIndex[X, Y] := Value;
end;

procedure TMapLayerAdapter.SetTileSetIndex(Value: LongInt);
begin
 TMapLayer(FSource).TileSetIndex := Value;
end;

procedure TMapLayerAdapter.SetTilesFilled(Value: Boolean);
begin
 TMapLayer(FSource).TilesFilled := Value;
end;

procedure TMapLayerAdapter.SetXFlipShift(Value: ShortInt);
begin
 TMapLayer(FSource).XFlipShift := Value;
end;

procedure TMapLayerAdapter.SetYFlipShift(Value: ShortInt);
begin
 TMapLayer(FSource).YFlipShift := Value;
end;

procedure TMapLayerAdapter.UpdateFormat(const NewFormat: TLayerFormatRec);
begin
 TMapLayer(FSource).UpdateFormat(me_lib.TLayerFormatRec(NewFormat));
end;

procedure TMapLayerAdapter.WriteCellInfo(var Data; const Info: TCellInfo);
begin
 TMapLayer(FSource).WriteCellInfo(Data, me_lib.TCellInfo(Info));
end;

procedure TMapLayerAdapter.WritePaletteIndex(var Data; Value: LongInt);
begin
 TMapLayer(FSource).WritePaletteIndex(Data, Value);
end;

procedure TMapLayerAdapter.WritePriority(var Data; Value: LongInt);
begin
 TMapLayer(FSource).WritePriority(Data, Value);
end;

procedure TMapLayerAdapter.WriteTileIndex(var Data; Value: LongInt);
begin
 TMapLayer(FSource).WriteTileIndex(Data, Value);
end;

procedure TMapLayerAdapter.WriteXFlip(var Data; Value: Boolean);
begin
 TMapLayer(FSource).WriteXFlip(Data, Value);
end;

procedure TMapLayerAdapter.WriteYFlip(var Data; Value: Boolean);
begin
 TMapLayer(FSource).WriteYFlip(Data, Value);
end;

{ TBrushLayerAdapter }

constructor TBrushLayerAdapter.Create(Source: TBrushLayer;
  FreeAfterUse: Boolean);
begin
 inherited Create(Source, FreeAfterUse);
end;

function TBrushLayerAdapter.Draw(const DestLayer: IMapLayer; DestX,
  DestY: LongInt): Boolean;
begin
 if DestLayer <> nil then
  Result := TBrushLayer(FSource).Draw(TMapLayer(DestLayer.GetPtr), DestX, DestY) else
  Result := False;
end;

function TBrushLayerAdapter.DrawTail(const Dest: IMap; DestX,
  DestY: LongInt; Direction: TLinkDirection): Boolean;
begin
 if Dest <> nil then
  Result := TBrushLayer(FSource).DrawTail(TMap(Dest.GetPtr), DestX, DestY,
                              me_lib.TLinkDirection(Direction)) else
  Result := False;
end;

function TBrushLayerAdapter.GetExtended: TLinkDirections;
begin
 Result := TLinkDirections(TBrushLayer(FSource).Extended);
end;

function TBrushLayerAdapter.GetHotSpotX: LongInt;
begin
 Result := TBrushLayer(FSource).HotSpotX;
end;

function TBrushLayerAdapter.GetHotSpotY: LongInt;
begin
 Result := TBrushLayer(FSource).HotSpotY;
end;

function TBrushLayerAdapter.GetIdentityTag: LongInt;
begin
 Result := TBrushLayer(FSource).IdentityTag;
end;

function TBrushLayerAdapter.GetLink(
  Direction: TLinkDirection): IBrushLayer;
var
 Link: TBrushLayer;
begin
 Link := TBrushLayer(FSource).Links[me_lib.TLinkDirection(Direction)];
 if Link <> nil then
  Result := TBrushLayerAdapter.Create(Link) else
  Result := nil;
end;

function TBrushLayerAdapter.GetLinkIndex(
  Direction: TLinkDirection): LongInt;
begin
 Result := TBrushLayer(FSource).LinkIndex[me_lib.TLinkDirection(Direction)];
end;

function TBrushLayerAdapter.GetRandomize: Boolean;
begin
 Result := TBrushLayer(FSource).Randomize;
end;

function TBrushLayerAdapter.GetTailsDirections: TLinkDirections;
begin
 Result := TLinkDirections(TBrushLayer(FSource).TailsDirections);
end;

function TBrushLayerAdapter.GetTailsFilled: Boolean;
begin
 Result := TBrushLayer(FSource).TailsFilled;
end;

function TBrushLayerAdapter.GetRandomTwin: IBrushLayer;
var
 Layer: TBrushLayer;
begin
 Layer := TBrushLayer(FSource).RandomTwin;
 if Layer <> nil then
  Result := TBrushLayerAdapter.Create(Layer) else
  Result := nil;
end;

function TBrushLayerAdapter.GetUnfinished: TLinkDirections;
begin
 Result := TLinkDirections(TBrushLayer(FSource).Unfinished);
end;

function TBrushLayerAdapter.RectOnMap(X, Y: LongInt): TRect;
begin
 Result := TBrushLayer(FSource).RectOnMap(X, Y);
end;

procedure TBrushLayerAdapter.SetExtended(Value: TLinkDirections);
begin
 TBrushLayer(FSource).Extended := me_lib.TLinkDirections(Value);
end;

procedure TBrushLayerAdapter.SetHotSpotX(Value: LongInt);
begin
 TBrushLayer(FSource).HotSpotX := Value;
end;

procedure TBrushLayerAdapter.SetHotSpotY(Value: LongInt);
begin
 TBrushLayer(FSource).HotSpotY := Value;
end;

procedure TBrushLayerAdapter.SetIdentityTag(Value: LongInt);
begin
 TBrushLayer(FSource).IdentityTag := Value;
end;

procedure TBrushLayerAdapter.SetLinkIndex(Direction: TLinkDirection;
  Value: LongInt);
begin
 TBrushLayer(FSource).LinkIndex[me_lib.TLinkDirection(Direction)] := Value;
end;

procedure TBrushLayerAdapter.SetRandomize(Value: Boolean);
begin
 TBrushLayer(FSource).Randomize := Value;
end;

procedure TBrushLayerAdapter.SetTailsFilled(Value: Boolean);
begin
 TBrushLayer(FSource).TailsFilled := Value;
end;

procedure TBrushLayerAdapter.SetUnfinished(Value: TLinkDirections);
begin
 TBrushLayer(FSource).Unfinished := me_lib.TLinkDirections(Value);
end;

function TBrushLayerAdapter.SitsHere(const Target: IMapLayer; X,
  Y: LongInt): Boolean;
begin
 Result := (Target <> nil) and
     TBrushLayer(FSource).SitsHere(TMapLayer(Target.GetPtr), X, Y);
end;

function TBrushLayerAdapter.SpottedInRect(const Target: IMapLayer;
  const Rect: TRect; var pt: TPoint): Boolean;
begin
 Result := (Target <> nil) and
    TBrushLayer(FSource).SpottedInRect(TMapLayer(Target.GetPtr), Rect, pt)
end;

{ TMapAdapter }

function TMapAdapter.AddLayer(Name: PWideChar): IMapLayer;
var
 Layer: TMapLayer;
begin
 Layer := TMap(FSource).AddChild(Name) as TMapLayer;
 Result := TMapLayerAdapterClass(FNodeAdapterClass).Create(Layer);
end;

procedure TMapAdapter.AssignMapInfo(const Source: IMap);
begin
 if Source <> nil then
  TMap(FSource).AssignMapInfo(TMap(Source.GetPtr)) else
  TMap(FSource).AssignMapInfo(nil);
end;

constructor TMapAdapter.Create(Source: TMap; FreeAfterUse: Boolean);
begin
 inherited Create(Source, FreeAfterUse);
end;

function TMapAdapter.FindLayerByTileSet(
  const ATileSet: ITileSet): IMapLayer;
var
 Layer: TMapLayer;
begin
 if ATileSet <> nil then
 begin
  Layer := TMap(FSource).FindLayerByTileSet(TTileSet(ATileSet.GetPtr));
  if Layer <> nil then
  begin
   Result := TMapLayerAdapterClass(FNodeAdapterClass).Create(Layer);
   Exit;
  end;
 end;
 Result := nil;
end;

function TMapAdapter.GetColorTable: IColorTable;
begin
 with TMap(FSource) do
  if ColorTable <> nil then
   Result := TColorTableAdapter.Create(ColorTable) else
   Result := nil;
end;

function TMapAdapter.GetCtIndex: LongInt;
begin
 Result := TMap(FSource).ColorTableIndex;
end;

function TMapAdapter.GetHeight: LongInt;
begin
 Result := TMap(FSource).Height;
end;

function TMapAdapter.GetLayer(Index: LongInt): IMapLayer;
var
 Layer: TMapLayer;
begin
 Layer := TMap(FSource).Nodes[Index] as TMapLayer;
 if Layer <> nil then
  Result := TMapLayerAdapterClass(FNodeAdapterClass).Create(Layer) else
  Result := nil;
end;

function TMapAdapter.GetMapX: LongInt;
begin
 Result := TMap(FSource).MapX;
end;

function TMapAdapter.GetMapY: LongInt;
begin
 Result := TMap(FSource).MapY;
end;

function TMapAdapter.GetTileHeight: LongInt;
begin
 Result := TMap(FSource).TileHeight;
end;

function TMapAdapter.GetTileWidth: LongInt;
begin
 Result := TMap(FSource).TileWidth;
end;

function TMapAdapter.GetWidth: LongInt;
begin
 Result := TMap(FSource).Width;
end;

procedure TMapAdapter.Initialize;
begin
 FNodeAdapterClass := TMapLayerAdapter;
end;

function TMapAdapter.IsTileSetUsed(const ATileSet: ITileSet): Boolean;
begin
 Result := (ATileSet <> nil) and
           TMap(FSource).IsTileSetUsed(TTileSet(ATileSet.GetPtr));
end;

procedure TMapAdapter.SetCtIndex(Value: LongInt);
begin
 TMap(FSource).ColorTableIndex := Value;
end;

procedure TMapAdapter.SetHeight(Value: LongInt);
begin
 TMap(FSource).Height := Value;
end;

procedure TMapAdapter.SetMapSize(AWidth, AHeight: LongInt);
begin
 TMap(FSource).SetMapSize(AWidth, AHeight);
end;

procedure TMapAdapter.SetMapX(Value: LongInt);
begin
 TMap(FSource).MapX := Value;
end;

procedure TMapAdapter.SetMapY(Value: LongInt);
begin
 TMap(FSource).MapY := Value;
end;

procedure TMapAdapter.SetTileHeight(Value: LongInt);
begin
 TMap(FSource).TileHeight := Value;
end;

procedure TMapAdapter.SetTileWidth(Value: LongInt);
begin
 TMap(FSource).TileWidth := Value;
end;

procedure TMapAdapter.SetWidth(Value: LongInt);
begin
 TMap(FSource).Width := Value;
end;

procedure TMapAdapter.UnhookTileSet(const ATileSet: ITileSet);
begin
 if ATileSet <> nil then
  TMap(FSource).UnhookTileSet(TTileSet(ATileSet.GetPtr));
end;

{ TMapBrushAdapter }

constructor TMapBrushAdapter.Create(Source: TMapBrush;
  FreeAfterUse: Boolean);
begin
 inherited Create(Source, FreeAfterUse);
end;

function TMapBrushAdapter.Draw(const Dest: IMap; DestX,
  DestY: LongInt): Boolean;
begin
 if Dest <> nil then
  Result := TMapBrush(FSource).Draw(TMap(Dest.GetPtr), DestX, DestY) else
  Result := False;
end;

function TMapBrushAdapter.FindBrushLayer(const Dest: IMap; X,
  Y: LongInt; var pt: TPoint): IBrushLayer;
var
 Layer: TBrushLayer;
begin
 if Dest <> nil then
 begin
  Layer := TMapBrush(FSource).FindBrushLayer(TMap(Dest.GetPtr), X, Y, pt);
  if Layer <> nil then
  begin
   Result := TBrushLayerAdapter.Create(Layer);
   Exit;
  end;
 end;
 Result := nil;
end;

function TMapBrushAdapter.FindExactBrushLayer(const Dest: IMap; X,
  Y: LongInt; var pt: TPoint): IBrushLayer;
var
 Layer: TBrushLayer;
begin
 if Dest <> nil then
 begin
  Layer := TMapBrush(FSource).FindExactBrushLayer(TMap(Dest.GetPtr), X, Y, pt);
  if Layer <> nil then
  begin
   Result := TBrushLayerAdapter.Create(Layer);
   Exit;
  end;
 end;
 Result := nil;
end;

function TMapBrushAdapter.GetExtends: IMapBrush;
begin
 with TMapBrush(FSource) do
  if Extends <> nil then
   Result := TMapBrushAdapter.Create(Extends) else
   Result := nil;
end;

function TMapBrushAdapter.GetExtendsIndex: LongInt;
begin
 Result := TMapBrush(FSource).ExtendsIndex;
end;

procedure TMapBrushAdapter.Initialize;
begin
 FNodeAdapterClass := TBrushLayerAdapter;
end;

procedure TMapBrushAdapter.RepointLinks;
begin
 TMapBrush(FSource).RepointLinks;
end;

procedure TMapBrushAdapter.SetExtendsIndex(Value: LongInt);
begin
 TMapBrush(FSource).ExtendsIndex := Value;
end;

{ TBaseMapListAdapter }

function TBaseMapListAdapter.AddMap(AName: PWideChar; AWidth, AHeight,
  ATileW, ATileH: Integer): IMap;
var
 Map: TMap;
begin
 Map := TBaseMapList(FSource).AddMap(AName, AWidth, AHeight, ATileW, ATileH);
 Result := TMapAdapterClass(FNodeAdapterClass).Create(Map);
end;

function TBaseMapListAdapter.GetColorTableList: IColorTableList;
begin
 with TBaseMapList(FSource) do
  if ColorTableList <> nil then
   Result := TColorTableListAdapter.Create(ColorTableList) else
   Result := nil;
end;

function TBaseMapListAdapter.GetMap(Index: Integer): IMap;
var
 Map: TMap;
begin
 Map := TBaseMapList(FSource).Maps[Index];
 if Map <> nil then
  Result := TMapAdapterClass(FNodeAdapterClass).Create(Map) else
  Result := nil;
end;

function TBaseMapListAdapter.GetTileSetList: ITileSetList;
begin
 with TBaseMapList(FSource) do
  if TileSetList <> nil then
   Result := TTileSetListAdapter.Create(TileSetList) else
   Result := nil;
end;

function TBaseMapListAdapter.IsColorTableUsed(
  const AColorTable: IColorTable): Boolean;
begin
 Result := (AColorTable <> nil) and
  TBaseMapList(FSource).IsColorTableUsed(TColorTable(AColorTable.GetPtr));
end;

function TBaseMapListAdapter.IsTileSetUsed(
  const ATileSet: ITileSet): Boolean;
begin
 Result := (ATileSet <> nil) and
  TBaseMapList(FSource).IsTileSetUsed(TTileSet(ATileSet.GetPtr));
end;

procedure TBaseMapListAdapter.UnhookColorTable(
  const AColorTable: IColorTable);
begin
 if AColorTable <> nil then
  TBaseMapList(FSource).UnhookColorTable(TColorTable(AColorTable.GetPtr));
end;

procedure TBaseMapListAdapter.UnhookTileSet(const ATileSet: ITileSet);
begin
 if ATileSet <> nil then
  TBaseMapList(FSource).UnhookTileSet(TTileSet(ATileSet.GetPtr));
end;

{ TBrushListAdapter }

procedure TBrushListAdapter.CleanUpTails;
begin
 TBrushList(FSource).CleanUpTails;
end;

constructor TBrushListAdapter.Create(Source: TBrushList;
  FreeAfterUse: Boolean);
begin
 inherited Create(Source, FreeAfterUse);
end;

function TBrushListAdapter.EraseBrushContentAt(const Dest: IMap;
  const DestRect: TRect): Boolean;
begin
 Result := (Dest <> nil) and
  TBrushList(FSource).EraseBrushContentAt(TMap(Dest.GetPtr), DestRect);
end;

function TBrushListAdapter.GetIdentitiesFilled: Boolean;
begin
 Result := TBrushList(FSource).IdentitiesFilled;
end;

function TBrushListAdapter.GetMapBrush(Index: Integer): IMapBrush;
var
 Brush: TMapBrush;
begin
 Brush := TBrushList(FSource).Nodes[Index] as TMapBrush;
 if Brush <> nil then
  Result := TMapBrushAdapter.Create(Brush) else
  Result := nil;
end;

procedure TBrushListAdapter.Initialize;
begin
 FNodeAdapterClass := TMapBrushAdapter;
end;

function TBrushListAdapter.IsBrushUsed(const ABrush: IMapBrush): Boolean;
begin
 Result := (ABrush <> nil) and
          TBrushList(FSource).IsBrushUsed(TMapBrush(ABrush.GetPtr));
end;

procedure TBrushListAdapter.SetColorTableList(
  const Value: IColorTableList);
begin
 if Value <> nil then
  TBrushList(FSource).ColorTableList := TColorTableList(Value.GetPtr) else
  TBrushList(FSource).ColorTableList := nil;
end;

procedure TBrushListAdapter.SetIdentitiesFilled(Value: Boolean);
begin
 TBrushList(FSource).IdentitiesFilled := Value;
end;

procedure TBrushListAdapter.SetTileSetList(const Value: ITileSetList);
begin
 if Value <> nil then
  TBrushList(FSource).TileSetList := TTileSetList(Value.GetPtr) else
  TBrushList(FSource).TileSetList := nil;
end;

procedure TBrushListAdapter.UnhookBrush(const ABrush: IMapBrush);
begin
 if ABrush <> nil then
  TBrushList(FSource).UnhookBrush(TMapBrush(ABrush.GetPtr));
end;

{ TMapListAdapter }

constructor TMapListAdapter.Create(Source: TMapList;
  FreeAfterUse: Boolean);
begin
 inherited Create(Source, FreeAfterUse);
end;

function TMapListAdapter.GetBrushList: IBrushList;
begin
 with TMapList(FSource) do
  if BrushList <> nil then
   Result := TBrushListAdapter.Create(BrushList) else
   Result := nil;
end;

procedure TMapListAdapter.Initialize;
begin
 FNodeAdapterClass := TMapAdapter;
end;

{ TUtilsAdapter }

function TUtilsAdapter.CreateBitmapContainer: IBitmapContainer;
begin
 Result := TBitmapContainerAdapter.Create(TBitmapContainer.Create, True);
end;

function TUtilsAdapter.CreateBitmapConverter: IBitmapConverter;
begin
 Result := TBitmapConverterAdapter.Create(TBitmapConverter.Create, True);
end;

function TUtilsAdapter.CreateBrushList: IBrushList;
begin
 Result := TBrushListAdapter.Create(TBrushList.Create, True);
end;

function TUtilsAdapter.CreateColorFormat(
  Fmt: PAnsiChar): IColorFormat;
begin
 Result := TColorFormatAdapter.Create(TColorFormat.Create(Fmt), True);
end;

function TUtilsAdapter.CreateColorFormat2(RMask, GMask, BMask,
  AMask: LongWord): IColorFormat;
begin
 Result := TColorFormatAdapter.Create(
       TColorFormat.CreateFromMask(RMask, GMask, BMask, AMask), True);
end;

function TUtilsAdapter.CreateColorTableList: IColorTableList;
begin
 Result := TColorTableListAdapter.Create(TColorTableList.Create, True);
end;

function TUtilsAdapter.CreateFileStream(FileName: PAnsiChar;
  Mode: Integer): IStream64;
begin
 Result := TStreamAdapter.Create(TTntFileStream.Create(AnsiString(FileName),
                             Mode), True);
end;

function TUtilsAdapter.CreateFileStreamW(FileName: PWideChar;
  Mode: Integer): IStream64;
begin
 Result := TStreamAdapter.Create(TTntFileStream.Create(FileName, Mode), True);
end;

function TUtilsAdapter.CreateIniFile: IIniFile;
begin
 Result := TIniFileAdapter.Create(TStreamIniFileW.Create, True);
end;

function TUtilsAdapter.CreateMapList: IMapList;
begin
 Result := TMapListAdapter.Create(TMapList.Create, True);
end;

function TUtilsAdapter.CreateMemoryStream: IMemoryStream;
begin
 Result := TMemoryStreamAdapter.Create(TMemStream.Create, True);
end;

function TUtilsAdapter.CreatePropertyList: IPropertyList;
begin
 Result := TPropertyListAdapter.Create(TPropertyList.Create, True);
end;

function TUtilsAdapter.CreateStringList: IStringList;
begin
 Result := TStringsAdapter.Create(TStringList.Create, True);
end;

function TUtilsAdapter.CreateTileSetList: ITileSetList;
begin
 Result := TTileSetListAdapter.Create(TTileSetList.Create, True);
end;

function TUtilsAdapter.CreateWideStringList: IWideStringList;
begin
 Result := TStringsAdapter.Create;
end;

function TUtilsAdapter.CreateZLibCompressor(Level: Integer;
  const Dest: IReadWrite): IStream32;
begin
 Result := TStreamAdapter.Create(
            TZCompressionStream.Create(TStream(Dest.GetPtr),
             TZCompressionLevel(Level)), True);
end;

function TUtilsAdapter.CreateZLibDecompressor(
  const Source: IReadWrite): IStream32;
begin
 Result := TStreamAdapter.Create(
       TZDecompressionStream.Create(TStream(Source.GetPtr)), True);
end;

{function TUtilsAdapter.PropertyFromPointer(
  Ptr: Pointer): IPropertyListItem;
begin
 try
  if TObject(Ptr) is TPropertyListItem then
   Result := TPropertyAdapter.Create(Ptr) else
   Result := nil;
 except
  Result := nil;
 end;
end;
 }
end.
