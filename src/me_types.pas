unit me_types;

interface

uses me_lib;

type
 TPBLayerRec = record
                lData: Pointer;
                lFormat: TLayerFormatRec;
               end;
 PPasteBuffer = ^TPasteBuffer;
 TPasteBuffer = packed record
  ID: Integer;
  Layers: array of TPBLayerRec;
  Width: Integer;
  Height: Integer;
 end;

implementation

end.
