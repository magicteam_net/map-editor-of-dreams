unit me_importtilesdlg;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, TntForms, StdCtrls, TntStdCtrls, ExtCtrls, me_tilesfrm, BitmapEx,
  VirtualTrees, PropContainer, PropEditor, TilesEx, ActnList, TntActnList,
  ImgList, ComCtrls, TntComCtrls, ToolWin, TntClasses, TntDialogs, TntSysUtils,
  TileMapView;

const
 WM_REFRESHPROPS =  WM_USER + 993;

 TAG_FNAME = -1;
 TAG_OFFST = -2;
 TAG_SOFFS = -3;
 TAG_S_IDX = -4;
 TAG_ST_FC = -5;

type
  TImportTilesDialog = class(TTntForm)
    PropEditor: TPropertyEditor;
    TilesFrame: TTilesFrame;
    Bevel: TBevel;
    HScrollBar: TScrollBar;
    OKBtn: TTntButton;
    CancelBtn: TTntButton;
    ToolBar: TTntToolBar;
    NewCompositeBtn: TTntToolButton;
    Separator: TTntToolButton;
    MoveUpBtn: TTntToolButton;
    MoveDownBtn: TTntToolButton;
    DeleteBtn: TTntToolButton;
    ActionList: TTntActionList;
    ImageList: TImageList;
    NewCompositeAction: TTntAction;
    MoveUpAction: TTntAction;
    MoveDownAction: TTntAction;
    DeleteAction: TTntAction;
    OpenDialog: TTntOpenDialog;
    Panel: TPanel;
    StatusBar: TTntStatusBar;
    LoadFirstColors: TTntButton;
    LoadFirstColorsAction: TTntAction;
    ChangeAllFirstColorsAction: TTntAction;
    TntButton1: TTntButton;
    procedure PropEditorEllipsisClick(Sender: TCustomPropertyEditor;
      Node: PVirtualNode; var Accepted: Boolean);
    procedure TntFormCreate(Sender: TObject);
    procedure TntFormDestroy(Sender: TObject);
    procedure PropEditorPropertyDown(Sender: TCustomPropertyEditor;
      Node: PVirtualNode; var Yes: Boolean);
    procedure PropEditorPropertyRemove(Sender: TCustomPropertyEditor;
      Node: PVirtualNode; var Yes: Boolean);
    procedure PropEditorPropertyUp(Sender: TCustomPropertyEditor;
      Node: PVirtualNode; var Yes: Boolean);
    procedure PropEditorGetPropertyName(Sender: TCustomPropertyEditor;
      Node: PVirtualNode; var Text: WideString);
    procedure TileSetValueChange(Sender: TPropertyListItem);
    procedure TileSetImageFlagsChange(Sender: TPropertyListItem);
    procedure NewCompositeActionExecute(Sender: TObject);
    procedure NewCompositeActionUpdate(Sender: TObject);
    procedure MoveActionExecute(Sender: TObject);
    procedure MoveUpActionUpdate(Sender: TObject);
    procedure DeleteActionUpdate(Sender: TObject);
    procedure MoveDownActionUpdate(Sender: TObject);
    procedure TilesFrameTileSetViewSelectionChanged(
      Sender: TCustomTileMapView);
    procedure HScrollBarChange(Sender: TObject);
    procedure HScrollBarScroll(Sender: TObject; ScrollCode: TScrollCode;
      var ScrollPos: Integer);
    procedure TilesFrameTileSetViewCustomMouseActionCheck(
      Sender: TCustomTileMapView; Shift: TShiftState; var Act: Boolean);
    procedure LoadFirstColorsActionExecute(Sender: TObject);
    procedure FirstColorsActionUpdate(Sender: TObject);
    procedure ChangeAllFirstColorsActionExecute(Sender: TObject);
  private
    FSetWidth: Integer;
    FTileSet: TTileSet;
    FStream: TStream;
    FFileName: WideString;
    FLastFCFileName: WideString;
    FNoEmpty: Boolean;
    function GetSelectedOffset: Int64;
    procedure SetColorTable(Value: TColorTable);
    procedure SetFileName(const Value: WideString);
    procedure SetSelectedIndex(Value: Integer);
    procedure SetSelectedOffset(const Value: Int64);
    procedure WMRefreshProps(var Message: TMessage); message WM_REFRESHPROPS;
    function GetStreamPosition: Int64;
    function GetStreamSize: Int64;
    procedure SetStreamPosition(Value: Int64);
    procedure SetSetWidth(Value: Integer);
    function GetSelectedIndex: Integer;
    function GetColorTable: TColorTable;
    procedure AdjustScrollBars;
  public
    property FileName: WideString read FFileName write SetFileName;
    property ColorTable: TColorTable read GetColorTable write SetColorTable;
    property StreamSize: Int64 read GetStreamSize;
    property StreamPosition: Int64 read GetStreamPosition write SetStreamPosition;

    property SelectedIndex: Integer read GetSelectedIndex write SetSelectedIndex;
    property SelectedOffset: Int64 read GetSelectedOffset write SetSelectedOffset;
    property SetWidth: Integer read FSetWidth write SetSetWidth;

    procedure RefreshProps;
    procedure PerformRefreshProps;    
    procedure UpdateTileSet(NewPos: Boolean = False);
  end;

 TTileSetRec = record
  TileSet: TTileSet;
  SetWidth: Integer;
 end;


function ImportTilesDialog(const AFileName: WideString;
         BaseTileSet: TTileSet; ColorTable: TColorTable;
         BaseCount, BaseWidth: Integer): TTileSetRec;

var
  ImportTilesDlg: TImportTilesDialog;

implementation

uses
  me_tilesetpropsfrm;

{$R *.DFM}

function ImportTilesDialog(const AFileName: WideString;
         BaseTileSet: TTileSet; ColorTable: TColorTable;
         BaseCount, BaseWidth: Integer): TTileSetRec;
begin
 try
  Application.CreateForm(TImportTilesDialog, ImportTilesDlg);
  if BaseTileSet <> nil then
   ImportTilesDlg.FTileSet.AssignTileFormat(BaseTileSet);

  ImportTilesDlg.FTileSet.Count := BaseCount;
  ImportTilesDlg.ColorTable := ColorTable;
  ImportTilesDlg.FSetWidth := BaseWidth;
  ImportTilesDlg.FileName := AFileName;

  if ImportTilesDlg.ShowModal = mrOk then
  begin
   Result.TileSet := ImportTilesDlg.FTileSet;
   Result.SetWidth := ImportTilesDlg.FSetWidth;
   ImportTilesDlg.FTileSet := nil;
  end else
   FillChar(Result, SizeOf(TTileSetRec), 0);
 finally
  FreeAndNil(ImportTilesDlg);
 end;
end;

{ TImportTilesDialog }

procedure TImportTilesDialog.PropEditorPropertyDown(
  Sender: TCustomPropertyEditor; Node: PVirtualNode; var Yes: Boolean);
var
 Data: PPropertyData;
 Idx: Integer;
 Value: Word;
 Flg: TCompositeFlags;
begin
 Data := Sender.GetNodeData(Node);
 if (Data <> nil) and (Data.Owner <> nil) then
  case Data.Item.UserTag of
   TAG_COMPDEF:
   begin
    Idx := Data.Item.Index;
    if Idx < Data.Owner.Count - 1 then
    begin
     with FTileSet do
     begin
      Flg := TileBitmap.FlagsList;
      Value := Flg[Idx];
      Flg[Idx] := Flg[Idx + 1];
      Flg[Idx + 1] := Value;

      SwitchFormat(Flg);
     end;
     UpdateTileSet;
     Exit;
    end;
   end;
  end;
 Yes := False;
end;

procedure TImportTilesDialog.PropEditorPropertyRemove(
  Sender: TCustomPropertyEditor; Node: PVirtualNode; var Yes: Boolean);
var
 Data: PPropertyData;
 Idx, L: Integer;
 Flg: TCompositeFlags;
begin
 Data := Sender.GetNodeData(Node);
 if (Data <> nil) and (Data.Owner <> nil) then
  case Data.Item.UserTag of
   TAG_COMPDEF:
   begin
    Idx := Data.Item.Index;

    with FTileSet do
    begin
     Flg := TileBitmap.FlagsList;
     L := Length(Flg) - 1;
     if Idx < L then
      Move(Flg[Idx + 1], Flg[Idx], (L - Idx) shl 1);
     SetLength(Flg, L);
     SwitchFormat(Flg);
    end;
    Sender.InvalidateNode(Node.Parent);

    UpdateTileSet;
    Exit;
   end;
  end;
 Yes := False;
end;

procedure TImportTilesDialog.PropEditorPropertyUp(
  Sender: TCustomPropertyEditor; Node: PVirtualNode; var Yes: Boolean);
var
 Data: PPropertyData;
 Idx: Integer;
 Value: Word;
 Flg: TCompositeFlags;
begin
 Data := Sender.GetNodeData(Node);
 if (Data <> nil) and (Data.Owner <> nil) then
  case Data.Item.UserTag of
   TAG_COMPDEF:
   begin
    Idx := Data.Item.Index;
    if Idx > 0 then
    begin
     with FTileSet do
     begin
      Flg := TileBitmap.FlagsList;
      Value := Flg[Idx];
      Flg[Idx] := Flg[Idx - 1];
      Flg[Idx - 1] := Value;

      SwitchFormat(Flg);
     end;
     UpdateTileSet;
     Exit;
    end;
   end;
  end;
 Yes := False;
end;

procedure TImportTilesDialog.PropEditorGetPropertyName(
  Sender: TCustomPropertyEditor; Node: PVirtualNode; var Text: WideString);
var
 Data: PPropertyData;
begin
 Data := Sender.GetNodeData(Node);
 if (Data.Owner <> nil) and (Data.Item.UserTag = TAG_COMPDEF) then
  Text := WideFormat('Composite #%d', [Data.Item.Index + 1]);
end;

function TImportTilesDialog.GetSelectedOffset: Int64;
begin
 Result := StreamPosition + SelectedIndex * FTileSet.TileSize;
end;

procedure TImportTilesDialog.SetColorTable(Value: TColorTable);
begin
 TilesFrame.ColorTable := Value;
end;

procedure TImportTilesDialog.SetFileName(const Value: WideString);
begin
 if FFileName <> Value then
 begin
  FFileName := Value;
  FreeAndNil(FStream);
  try
   FStream := TTntFileStream.Create(FFileName, fmOpenRead or fmShareDenyWrite);
  except
   FreeAndNil(FStream);
   WideMessageDlg('Error loading file', mtError, [mbOk], 0);
   FFileName := '';
  end;
  UpdateTileSet;  
  RefreshProps;
 end;
end;

procedure TImportTilesDialog.SetSelectedIndex(Value: Integer);
begin
 TilesFrame.SelectedTile := Value;
end;

procedure TImportTilesDialog.SetSelectedOffset(const Value: Int64);
begin
 StreamPosition := Value - SelectedIndex * FTileSet.TileSize;
end;

procedure TImportTilesDialog.PropEditorEllipsisClick(
  Sender: TCustomPropertyEditor; Node: PVirtualNode;
  var Accepted: Boolean);
begin
 OpenDialog.FileName := FileName;
 OpenDialog.Title := 'Select file to import colors from';
 Accepted := OpenDialog.Execute;
 if Accepted then
  FileName := OpenDialog.FileName;
end;

procedure TImportTilesDialog.TntFormCreate(Sender: TObject);
begin
 FTileSet := TTileSet.Create;
 PropEditor.PropertyList.OnValueChange := TileSetValueChange;
 RefreshProps;
end;

procedure TImportTilesDialog.TntFormDestroy(Sender: TObject);
begin
 FreeAndNil(FStream);
 FreeAndNil(FTileSet);
end;

procedure TImportTilesDialog.WMRefreshProps(var Message: TMessage);
var
 Cnt: Integer;
 I: Integer;
 Tile: TTileItem;
begin
 with PropEditor, PropertyList do
 begin
  ClearList;
  AddFilePick('File name', FFileName, 'All Files (*.*)|*.*', '', '').UserTag := TAG_FNAME;
  AddDecimal('File size', StreamSize, 0, High(Int64)).ReadOnly := True;
  AddHexadecimal('Offset', StreamPosition, 0, High(Int64), 8).UserTag := TAG_OFFST;
  with FTileSet do
  begin
   AddHexadecimal('Selected offset', SelectedOffset, 0, High(Int64), 8).UserTag := TAG_SOFFS;

   AddDecimal('Selected index', SelectedIndex, 0, LastIndex).UserTag := TAG_S_IDX;
   Tile := FTileSet.Items[SelectedIndex];
   if Tile <> nil then
    I := Tile.FirstColor else
    I := 0;
   AddDecimal('Selected tile 1st color', I, 0, 255).UserTag := TAG_ST_FC;

   with AddDecimal('Tiles count', TilesCount, 0, 1000000) do
    UserTag := TAG_TILECNT;

   with AddDecimal('Tiles in a row', FSetWidth, 1, 256) do
    UserTag := TAG_ROWSIZE;

   with AddBoolean('Use 1st color index', DrawFlags and DRAW_PIXEL_MODIFY <> 0) do
    UserTag := TAG_USE1CLR;

   with AddDecimal('Tile width', TileWidth, 1, 256) do
    UserTag := TAG_TLWIDTH;

   with AddDecimal('Tile height', TileHeight, 1, 256) do
    UserTag := TAG_THEIGHT;

   Cnt := BitCount;

   with AddDecimal('Bits per pixel', Cnt, 1, 32) do
    UserTag := TAG_TILEBPP;

   with AddDecimal('Transparent color index', TransparentColor, 0, High(LongWord)) do
    UserTag := TAG_TRCOLOR;

   with TileBitmap, AddDecimal('Composite count',
                   Length(CompositeFlags) + 1, 1, 32) do
   begin
    UserTag := TAG_COMPCNT;

    with SubProperties.AddHexadecimal('', ImageFlags, 0, 65535, 4) do
    begin
     UserTag := TAG_COMPDEF;
     SubProperties.OnValueChange := TileSetImageFlagsChange;
     FillImageFormatProperties(SubProperties, ImageFlags);
    end;

    for I := 0 to Length(CompositeFlags) - 1 do
     with SubProperties.AddHexadecimal('',
                         CompositeFlags[I], 0, 65535, 4) do
     begin
      UserTag := TAG_COMPDEF;
      SubProperties.OnValueChange := TileSetImageFlagsChange;
      FillImageFormatProperties(SubProperties, CompositeFlags[I]);
     end;
   end;
  end; 
  RootNodeCount := Count;  
 end;
end;

procedure TImportTilesDialog.RefreshProps;
begin
 PostMessage(Handle, WM_REFRESHPROPS, 0, 0);
end;

procedure TImportTilesDialog.TileSetImageFlagsChange(
  Sender: TPropertyListItem);
begin
 if not CheckTileSetFlags(Sender, FTileSet) then
 begin
  case Sender.Index of
   0: WideMessageDlg('Direct color is unsupported.', mtError, [mbOk], 0);
   7: WideMessageDlg('Cannot be higher than bits per unit.', mtError, [mbOk], 0);
  end;
  RefreshProps;
 end else
 FillTileSetFlags(Sender, FTileSet, False);
 TilesFrame.TileSetModified := True;
end;

procedure TImportTilesDialog.TileSetValueChange(Sender: TPropertyListItem);
var
 Diff, J, BCnt: Integer;
 Flg: TCompositeFlags;
 Tile: TTileItem;
begin
 case Sender.UserTag of
  TAG_FNAME:
  begin
   if Sender.ValueStr = '' then
   begin
    WideMessageDlg('File name must not be empty', mtError, [mbOk], 0);
    Sender.Changing := True;
    Sender.ValueStr := FileName;
    Sender.Changing := False;                     
   end else
   if not WideFileExists(Sender.ValueStr) then
   begin
    WideMessageDlg(WideFormat('File ''%s'' is not found', [Sender.ValueStr]),
                    mtError, [mbOk], 0);
    Sender.Changing := True;
    Sender.ValueStr := FileName;
    Sender.Changing := False;
   end else
    FileName := Sender.ValueStr;
  end;
  TAG_OFFST:
   StreamPosition := Sender.Value;
  TAG_SOFFS:
   SelectedOffset := Sender.Value;
  TAG_S_IDX:
   SelectedIndex := Sender.Value;
  TAG_ST_FC:
  begin
   Tile := FTileSet.Items[SelectedIndex];
   if Tile <> nil then
    Tile.FirstColor := Sender.Value;
   TilesFrame.TileSetView.Invalidate; 
  end;
  TAG_TILECNT:
  begin
   FTileSet.Count := Sender.Value;
   FTileSet.TilesCount := FTileSet.Count;
   UpdateTileSet(True);
   RefreshProps;
  end;
  TAG_ROWSIZE:
  begin
   FSetWidth := Sender.Value;
   TilesFrame.DoSetTileSet(FTileSet, True, Pointer(-1), FSetWidth);
  end;
  TAG_USE1CLR:
  begin
   if Sender.PickListIndex <> 0 then
    FTileSet.DrawFlags := DRAW_PIXEL_MODIFY else
    FTileSet.DrawFlags := 0;
   TilesFrame.TileSetModified := True;
  end;
  TAG_TLWIDTH:
  begin
   FTileSet.SwitchFormat([], Sender.Value);
   UpdateTileSet;
  end;
  TAG_THEIGHT:
  begin
   FTileSet.SwitchFormat([], -1, Sender.Value);
   UpdateTileSet;
  end;
  TAG_TILEBPP:
  case Sender.Value of
   1, 2, 4, 8:
   begin
    FTileSet.SwitchFormat([Sender.Value - 1]);
    UpdateTileSet;
   end;
   3, 5, 6, 7:
   begin
    FTileSet.SwitchFormat([(Sender.Value - 1) or IMG_PLANAR]);
    UpdateTileSet;
   end;
   else
   begin
    WideMessageDlg('Direct color is unsupported.', mtError, [mbOk], 0);
    Sender.Changing := True;
    Sender.Value := FTileSet.BitCount;
    Sender.Changing := False;
   end;;
  end;
  TAG_TRCOLOR:
  begin
   FTileSet.TransparentColor := Sender.Value;
   TilesFrame.TileSetModified := True;
  end;
  TAG_COMPCNT:
  with FTileSet do
  begin
   Diff := Sender.Value - (Length(TileBitmap.CompositeFlags) + 1);
   if (Diff > 0) and (TileBitmap.BitsCount + Diff > 8) then
   begin
    WideMessageDlg('Cannot add more composites because direct color is unsupported.', mtError, [mbOk], 0);
    Sender.Changing := True;
    Sender.Value := Length(TileBitmap.CompositeFlags) + 1;
    Sender.Changing := False;
   end else
   begin
    Flg := TileBitmap.FlagsList;
    SetLength(Flg, Sender.Value);
    SwitchFormat(Flg);
    UpdateTileSet;
   end;
  end;
  TAG_COMPDEF:
  begin
   Flg := FTileSet.TileBitmap.FlagsList;
   Flg[Sender.Index] := Sender.Value;
   BCnt := Length(Flg);
   for J := 0 to BCnt - 1 do
    Inc(BCnt, Flg[J] and 31);
   if BCnt > 8 then
   begin
    WideMessageDlg(WideFormat('Direct color is unsupported.', [Sender.Value]),
                 mtError, [mbOk], 0);
    RefreshProps;
   end else
   begin
    BCnt := Sender.Value and 31 + 1;
    Diff := (Sender.Value shr PLANE_FORMAT_SHIFT) and 3;
    while 1 shl Diff > BCnt do
     Dec(Diff);

    Flg[Sender.Index] := (Sender.Value and IMG_FORMAT_MASK) or
                         (Diff shl PLANE_FORMAT_SHIFT);
    FTileSet.SwitchFormat(Flg);
    UpdateTileSet;
   end;
  end;
 end;
end;

function TImportTilesDialog.GetStreamPosition: Int64;
begin
 if FStream <> nil then
  Result := FStream.Position else
  Result := 0;
end;

function TImportTilesDialog.GetStreamSize: Int64;
begin
 if FStream <> nil then
  Result := FStream.Size else
  Result := 0;
end;

procedure TImportTilesDialog.NewCompositeActionExecute(Sender: TObject);
var
 Data: PPropertyData;
 L: Integer;
 Flg: TCompositeFlags;
begin
 with PropEditor do
 begin
  Data := GetNodeData(FocusedNode);
  if (Data <> nil) and (Data.Owner <> nil) then
   case Data.Item.UserTag of
    TAG_TLCFMT, TAG_COMPCNT, TAG_COMPDEF:
    with FTileSet do
    begin
     if TileBitmap.BitsCount < 8 then
     begin
      Flg := TileBitmap.FlagsList;
      SetLength(Flg, Length(Flg) + 1);
      GetRoute(FocusedNode, LastRoute);

      L := Length(LastRoute);
      case Data.Item.UserTag of
       TAG_COMPCNT:
        SetLength(LastRoute, L + 1);
       TAG_COMPDEF:
        Dec(L);
       else
       begin
        Dec(L);
        SetLength(LastRoute, L);
        Dec(L);
       end;
      end;

      LastRoute[L] := Length(Flg) - 1;

      SwitchFormat(Flg);

      UserSelect := True;
      PerformRefreshProps;
      UserSelect := False;
      UpdateTileSet;
     end else
      WideMessageDlg('Cannot add more composites because direct color is unsupported.', mtError, [mbOk], 0);
    end;
   end;
 end;  
end;

procedure TImportTilesDialog.NewCompositeActionUpdate(Sender: TObject);
var
 Data: PPropertyData;
 Enable: Boolean;
begin
 Enable := PropEditor.FocusedNode <> nil;
 if Enable then
 begin
  Data := PropEditor.GetNodeData(PropEditor.FocusedNode);
  Enable := (Data.Owner <> nil) and
            (Data.Item.UserTag in [TAG_COMPCNT, TAG_COMPDEF]);
 end;
 NewCompositeAction.Enabled := Enable;
end;

procedure TImportTilesDialog.MoveActionExecute(Sender: TObject);
var
 Data: PPropertyData;
 Node: PVirtualNode;
begin
 Node := PropEditor.FocusedNode;
 if Node <> nil then
 begin
  Data := PropEditor.GetNodeData(Node);
  if (Data.Owner = nil) or (Data.Item.UserTag <> TAG_COMPDEF) then
   Exit;
  case (Sender as TAction).Tag of
   0: PropEditor.PropertyUp(Node);
   1: PropEditor.PropertyDown(Node);
   2: PropEditor.PropertyRemove(Node);
  end;
 end;
end;

procedure TImportTilesDialog.MoveUpActionUpdate(Sender: TObject);
var
 Data: PPropertyData;
 Enable: Boolean;
begin
 Enable := PropEditor.FocusedNode <> nil;
 if Enable then
 begin
  Data := PropEditor.GetNodeData(PropEditor.FocusedNode);
  Enable := (Data.Owner <> nil) and (Data.Item.UserTag = TAG_COMPDEF) and
            (Data.Item.Index > 0);
 end;
 MoveUpAction.Enabled := Enable;
end;

procedure TImportTilesDialog.MoveDownActionUpdate(Sender: TObject);
var
 Data: PPropertyData;
 Enable: Boolean;
begin
 Enable := PropEditor.FocusedNode <> nil;
 if Enable then
 begin
  Data := PropEditor.GetNodeData(PropEditor.FocusedNode);
  Enable := (Data.Owner <> nil) and (Data.Item.UserTag = TAG_COMPDEF) and
            (Data.Item.Index < Data.Owner.Count - 1);
 end;
 MoveDownAction.Enabled := Enable;
end;

procedure TImportTilesDialog.DeleteActionUpdate(Sender: TObject);
var
 Data: PPropertyData;
 Enable: Boolean;
begin
 Enable := PropEditor.FocusedNode <> nil;
 if Enable then
 begin
  Data := PropEditor.GetNodeData(PropEditor.FocusedNode);
  Enable := (Data.Owner <> nil) and (Data.Item.UserTag = TAG_COMPDEF) and
            (Data.Owner.Count > 1);
 end;
 DeleteAction.Enabled := Enable;
end;

procedure TImportTilesDialog.SetStreamPosition(Value: Int64);
begin
 if Value < 0 then Value := 0;
 if (FStream <> nil) and
    (FStream.Position <> Value) then
 begin
  FStream.Position := Value;
  UpdateTileSet;
 end;
end;

procedure TImportTilesDialog.TilesFrameTileSetViewSelectionChanged(
  Sender: TCustomTileMapView);
begin
 TilesFrame.TileSetViewSelectionChanged(Sender);
 RefreshProps;
end;

procedure TImportTilesDialog.UpdateTileSet(NewPos: Boolean);
var
 Size, TileSize, I: Integer;
 Tile: TTileItem;
 EmptyData: Pointer;
 OldPos: Int64;
 P: PByte;
begin
 AdjustScrollBars;
 FTileSet.United := True;
 Tile := FTileSet.RootNode as TTileItem;
 TileSize := FTileSet.TileSize;
 Size := FTileSet.Count * TileSize;
 FTileSet.FastList := False;
 EmptyData := FTileSet.EmptyTile.TileData;
 P := FTileSet.TileBuffer;
 for I := 0 to FTileSet.Count - 1 do
 begin
  Move(EmptyData^, P^, TileSize);
  Inc(P, TileSize);
 end;
 if FStream <> nil then
 begin
  OldPos := FStream.Position;
  FStream.Read(FTileSet.TileBuffer^, Size);
  FStream.Position := OldPos;
  while Tile <> nil do
  begin
   if not FNoEmpty and CompareMem(Tile.TileData, EmptyData, TileSize) then
    Tile.TileIndex := -1 else
    Tile.TileIndex := Tile.Index;
   Tile := Tile.Next as TTileItem;
  end;
 end else
 while Tile <> nil do
 begin
  Tile.TileIndex := -1;
  Tile := Tile.Next as TTileItem;
 end;
 if NewPos then
  TilesFrame.DoSetTileSet(FTileSet, True, Pointer(-1), FSetWidth,
                          FTileSet.LastIndex, -2, -2) else
  TilesFrame.DoSetTileSet(FTileSet, True, Pointer(-1), FSetWidth);
end;

procedure TImportTilesDialog.SetSetWidth(Value: Integer);
begin
 if FStream <> nil then
 begin
  FSetWidth := Value;
  UpdateTileSet;
 end;
end;

function TImportTilesDialog.GetSelectedIndex: Integer;
begin
 Result := TilesFrame.SelectedTile;
end;

function TImportTilesDialog.GetColorTable: TColorTable;
begin
 Result := TilesFrame.ColorTable;
end;

procedure TImportTilesDialog.PerformRefreshProps;
begin
 Perform(WM_REFRESHPROPS, 0, 0);
end;

procedure TImportTilesDialog.HScrollBarChange(Sender: TObject);
begin
 if FStream.Size - FTileSet.TileSize <= MaxInt then
 begin
  with Sender as TScrollBar do
   StreamPosition := Position;
 end;
end;

procedure TImportTilesDialog.AdjustScrollBars;
var
 X64, X: Int64;
 TSZ: Integer;
begin
 if FStream <> nil then
 begin
  TSZ := FTileSet.TileSize;
  X64 := FStream.Size - TSZ;
  if X64 > 0 then
  begin
   with HScrollBar do
   begin
    if X64 > MaxInt then
    begin
     Min := 0;
     Max := 2;
     X := StreamPosition;
     if X = 0 then
      Position := 0 else
     if X >= X64 then
      Position := 2 else
      Position := 1;
     LargeChange := 1;
     SmallChange := 1;
    end else
    begin
     Min := 0;
     Max := X64;
     Position := StreamPosition;
     SmallChange := TSZ;
     LargeChange := TSZ * FTileSet.Count;
    end;
    Enabled := True;
   end;
   Exit;
  end;
 end;
 HScrollBar.Enabled := False;
end;

procedure TImportTilesDialog.HScrollBarScroll(Sender: TObject;
  ScrollCode: TScrollCode; var ScrollPos: Integer);
var
 X64: Int64;
 TSZ: Integer;
begin
 TSZ := FTileSet.TileSize;
 X64 := FStream.Size - TSZ;
 if X64 > MaxInt then
 begin
  case ScrollCode of
   scLineUp:   StreamPosition := StreamPosition - TSZ;
   scLineDown: StreamPosition := StreamPosition + TSZ;
   scPageUp:   StreamPosition := StreamPosition - TSZ * FTileSet.Count;
   scPageDown: StreamPosition := StreamPosition + TSZ * FTileSet.Count;
   scPosition,
   scTrack,
   scTop,
   scBottom,
   scEndScroll: { do nothing };
  end;
  if StreamPosition = 0 then
   ScrollPos := 0 else
  if StreamPosition = X64 then
   ScrollPos := 2 else
   ScrollPos := 1;
 end;
end;

procedure TImportTilesDialog.TilesFrameTileSetViewCustomMouseActionCheck(
  Sender: TCustomTileMapView; Shift: TShiftState; var Act: Boolean);
var
 Tile: TTileItem;
begin
 if ssRight in Shift then
 begin
  TilesFrame.SelectedTile := TilesFrame.IndexUnderCursor;
  Application.ProcessMessages;

  Tile := FTileSet.Indexed[TilesFrame.SelectedTile];
  if Tile <> FTileSet.EmptyTile then
   Move(Tile.TileData^, FTileSet.EmptyTile.TileData^, FTileSet.TileSize) else
   FTileSet.InitEmptyTile;

  UpdateTileSet;
  Act := False;
 end;
end;

procedure TImportTilesDialog.LoadFirstColorsActionExecute(Sender: TObject);
var
 Sz, I: Integer;
 Stream: TStream;
 Buf: array of Byte;
begin
 if FTileSet.Count > 0 then
 begin
  OpenDialog.FileName := FLastFCFileName;
  OpenDialog.Title := 'Select first color indexes file';
  if OpenDialog.Execute then
  begin
   FLastFCFileName := OpenDialog.FileName;
   try
    Stream := TTntFileStream.Create(FLastFCFileName, fmOpenRead);
    try
     Stream.Position :=
       StrToInt64(WideInputBox(WideExtractFileName(FLastFCFileName),
            'Offset in a file', '0x000000'));
     SetLength(Buf, FTileSet.Count);
     Sz := Stream.Read(Buf[0], FTileSet.Count);
    finally
     Stream.Free;
    end;
    for I := 0 to Sz - 1 do
     FTileSet.Items[I].FirstColor := Buf[I];
    RefreshProps;
    TilesFrame.TileSetView.Invalidate;
   except
    on EConvertError do
     WideMessageDlg('Bad offset', mtError, [mbOk], 0);
    else
     WideMessageDlg('Error loading file', mtError, [mbOk], 0);
   end;
  end;
 end;
end;

procedure TImportTilesDialog.FirstColorsActionUpdate(Sender: TObject);
begin
 LoadFirstColorsAction.Enabled := FTileSet.Count > 0;
end;

procedure TImportTilesDialog.ChangeAllFirstColorsActionExecute(
  Sender: TObject);
var
 FC: Byte;
 I: Integer;
 Tile: TTileItem;
begin
 if FTileSet.Count > 0 then
 begin
  Tile := FTileSet.Items[TilesFrame.SelectedTile];
  if Tile <> nil then
   FC := Tile.FirstColor else
   FC := 0;
  try
   FC := StrToInt(WideInputBox('Input', 'First color index', IntToStr(FC)));
  except
   WideMessageDlg('Invalid integer value', mtError, [mbOk], 0);
   Exit;
  end;

  for I := 0 to FTileSet.Count - 1 do
   FTileSet.Items[I].FirstColor := FC;
   
  RefreshProps;
  TilesFrame.TileSetView.Invalidate;
 end;
end;

end.
