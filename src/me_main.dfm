object MainForm: TMainForm
  Left = 227
  Top = 204
  Width = 1033
  Height = 756
  Color = clBtnFace
  Constraints.MinHeight = 480
  Constraints.MinWidth = 480
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  Menu = MainMenu
  OldCreateOrder = False
  Position = poDesktopCenter
  ShowHint = True
  OnCloseQuery = MainFormCloseQuery
  OnCreate = MainFormCreate
  OnDestroy = MainFormDestroy
  OnKeyDown = MainFormKeyDown
  OnKeyUp = MainFormKeyUp
  PixelsPerInch = 96
  TextHeight = 13
  object LeftSplitter: TSplitter
    Left = 200
    Top = 89
    Height = 593
    AutoSnap = False
    Beveled = True
    ResizeStyle = rsUpdate
  end
  object RightSplitter: TSplitter
    Left = 822
    Top = 89
    Height = 593
    Align = alRight
    AutoSnap = False
    Beveled = True
    ResizeStyle = rsUpdate
  end
  object MainStatusBar: TTntStatusBar
    Left = 0
    Top = 682
    Width = 1025
    Height = 19
    AutoHint = True
    Panels = <
      item
        Width = 50
      end>
  end
  object ToolsPanel: TPanel
    Left = 0
    Top = 0
    Width = 1025
    Height = 89
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object Splitter7: TSplitter
      Left = 441
      Top = 0
      Height = 89
      Align = alRight
      AutoSnap = False
      Beveled = True
      ResizeStyle = rsUpdate
    end
    object ColTblPanel: TPanel
      Left = 444
      Top = 0
      Width = 581
      Height = 89
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object Splitter10: TSplitter
        Left = 380
        Top = 0
        Height = 89
        Align = alRight
        AutoSnap = False
        Beveled = True
        ResizeStyle = rsUpdate
      end
      object ColTblPropsPanel: TPanel
        Left = 383
        Top = 0
        Width = 198
        Height = 89
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object ColTblPropsLabel: TTntLabel
          Left = 0
          Top = 0
          Width = 198
          Height = 13
          Align = alTop
          Caption = 'Color table properties'
          ShowAccelChar = False
          Layout = tlCenter
        end
        object PalPropEditor: TPropertyEditor
          Tag = 4
          Left = 0
          Top = 13
          Width = 198
          Height = 76
          BoldChangedValues = False
          ButtonFillMode = fmShaded
          Align = alClient
          ScrollBarOptions.ScrollBars = ssVertical
          TabOrder = 0
          PropertyWidth = 105
          ValueWidth = 89
          Columns = <
            item
              Margin = 0
              Options = [coEnabled, coParentBidiMode, coParentColor, coResizable, coVisible, coFixed]
              Position = 0
              Width = 105
            end
            item
              Margin = 0
              Options = [coEnabled, coParentBidiMode, coParentColor, coResizable, coVisible, coFixed]
              Position = 1
              Width = 89
            end>
        end
      end
      object DrawSizesToolBar: TTntToolBar
        Left = 110
        Top = 0
        Width = 29
        Height = 89
        Align = alLeft
        EdgeInner = esNone
        EdgeOuter = esNone
        Flat = True
        Images = ImgList
        Indent = 3
        TabOrder = 1
        Wrapable = False
        object ToolSizeBtn1: TTntToolButton
          Left = 3
          Top = 0
          Action = BrushSmallAction
          Grouped = True
          Wrap = True
          Style = tbsCheck
        end
        object ToolSizeBtn2: TTntToolButton
          Left = 3
          Top = 22
          Action = BrushMediumAction
          Grouped = True
          Wrap = True
          Style = tbsCheck
        end
        object ToolSizeBtn3: TTntToolButton
          Left = 3
          Top = 44
          Action = BrushLargeAction
          Grouped = True
          Wrap = True
          Style = tbsCheck
        end
        object ToolSizeBtn4: TTntToolButton
          Left = 3
          Top = 66
          Action = BrushVeryLargeAction
          Grouped = True
          Style = tbsCheck
        end
      end
      object DrawModeToolBar: TTntToolBar
        Left = 81
        Top = 0
        Width = 29
        Height = 89
        Align = alLeft
        EdgeInner = esNone
        EdgeOuter = esNone
        Flat = True
        Images = ImgList
        Indent = 3
        TabOrder = 2
        Wrapable = False
        object ToolButton1: TTntToolButton
          Left = 3
          Top = 0
          Action = ToolDrawLineAction
          Grouped = True
          Wrap = True
          Style = tbsCheck
        end
        object ToolButton2: TTntToolButton
          Left = 3
          Top = 22
          Action = ToolDrawRectangleAction
          Grouped = True
          Wrap = True
          Style = tbsCheck
        end
        object ToolButton3: TTntToolButton
          Left = 3
          Top = 44
          Action = ToolDrawPolygonAction
          Grouped = True
          Wrap = True
          Style = tbsCheck
        end
        object ToolButton4: TTntToolButton
          Left = 3
          Top = 66
          Action = ToolDrawEllipseAction
          Grouped = True
          Style = tbsCheck
        end
      end
      object DrawingToolsBar: TTntToolBar
        Left = 29
        Top = 0
        Width = 52
        Height = 89
        Align = alLeft
        EdgeInner = esNone
        EdgeOuter = esNone
        Flat = True
        Images = ImgList
        Indent = 3
        TabOrder = 3
        Wrapable = False
        object ToolSingleSelectButton: TTntToolButton
          Left = 3
          Top = 0
          Action = ToolSingleSelectAction
          Grouped = True
          Style = tbsCheck
        end
        object ToolPencilButton: TTntToolButton
          Left = 26
          Top = 0
          Action = ToolPencilAction
          Grouped = True
          Wrap = True
          Style = tbsCheck
        end
        object ToolMultiSelectButton: TTntToolButton
          Left = 3
          Top = 22
          Action = ToolMultiSelectAction
          Grouped = True
          Style = tbsCheck
        end
        object ToolBrushButton: TTntToolButton
          Left = 26
          Top = 22
          Action = ToolBrushAction
          Grouped = True
          Wrap = True
          Style = tbsCheck
        end
        object ToolMagicWandButton: TTntToolButton
          Left = 3
          Top = 44
          Action = ToolMagicWandAction
          Grouped = True
          Style = tbsCheck
        end
        object ToolEraserButton: TTntToolButton
          Left = 26
          Top = 44
          Action = ToolEraserAction
          Grouped = True
          Wrap = True
          Style = tbsCheck
        end
        object ToolPickerButton: TTntToolButton
          Left = 3
          Top = 66
          Action = ToolDropperAction
          Grouped = True
          Style = tbsCheck
        end
        object ToolFloodFillButton: TTntToolButton
          Left = 26
          Top = 66
          Action = ToolFloodFillAction
          Grouped = True
          Style = tbsCheck
        end
      end
      object EditModeToolBar: TTntToolBar
        Left = 0
        Top = 0
        Width = 29
        Height = 89
        Align = alLeft
        EdgeInner = esNone
        EdgeOuter = esNone
        Flat = True
        Images = ImgList
        Indent = 3
        TabOrder = 4
        Wrapable = False
        object MapEditModeToolButton: TTntToolButton
          Left = 3
          Top = 0
          Action = MapEditModeAction
          Grouped = True
          Wrap = True
          Style = tbsCheck
        end
        object BrushEditModeToolButton: TTntToolButton
          Left = 3
          Top = 22
          Action = BrushEditModeAction
          Grouped = True
          Wrap = True
          Style = tbsCheck
        end
        object TileSetEditModeToolButton: TTntToolButton
          Left = 3
          Top = 44
          Action = TileSetEditModeAction
          Grouped = True
          Wrap = True
          Style = tbsCheck
        end
        object TileEditModeToolButton: TTntToolButton
          Left = 3
          Top = 66
          Action = TileEditModeAction
          Grouped = True
          Style = tbsCheck
        end
      end
      object ColTblToolBar: TTntToolBar
        Left = 155
        Top = 0
        Width = 26
        Height = 89
        Align = alLeft
        EdgeInner = esNone
        EdgeOuter = esNone
        Flat = True
        Images = ImgList
        Indent = 3
        TabOrder = 5
        Wrapable = False
        object ColTblNewToolBtn: TTntToolButton
          Left = 3
          Top = 0
          Action = ColorTableNewAction
          Wrap = True
        end
        object ColTblUpToolBtn: TTntToolButton
          Left = 3
          Top = 22
          Action = ColorTableUpAction
          Wrap = True
        end
        object ColTblDonToolBtn: TTntToolButton
          Left = 3
          Top = 44
          Action = ColorTableDownAction
          Wrap = True
        end
        object ColTblDelToolBtn: TTntToolButton
          Left = 3
          Top = 66
          Action = ColorTableDeleteAction
        end
      end
      object ColTblViewModeBar: TTntTabControl
        Left = 139
        Top = 0
        Width = 16
        Height = 89
        Align = alLeft
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        MultiLine = True
        ParentFont = False
        TabHeight = 16
        TabOrder = 6
        TabPosition = tpLeft
        Tabs.Strings = (
          'Visual'
          'List')
        TabIndex = 0
        TabStop = False
        OnChange = ColTblViewModeBarChange
      end
      object ColorTablesListBoxPanel: TPanel
        Left = 181
        Top = 0
        Width = 199
        Height = 89
        Align = alClient
        BevelInner = bvLowered
        BevelOuter = bvNone
        TabOrder = 7
        object ColorTablesListBox: TListBox
          Left = 1
          Top = 22
          Width = 197
          Height = 66
          Style = lbVirtualOwnerDraw
          Align = alClient
          ItemHeight = 13
          MultiSelect = True
          PopupMenu = ColTblsPopup
          TabOrder = 0
          Visible = False
          OnClick = ColorTablesListBoxClick
          OnContextPopup = ListBoxContextPopup
          OnData = ColorTablesListBoxData
          OnDataFind = ColorTablesListBoxDataFind
          OnDblClick = ColorTablesListBoxDblClick
          OnDrawItem = ListBoxDrawItem
          OnKeyDown = ColorTablesListBoxKeyDown
        end
        inline PaletteFrame: TPaletteFrame
          Left = 1
          Top = 22
          Width = 197
          Height = 66
          Align = alClient
          AutoScroll = False
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clWindowText
          Font.Height = -9
          Font.Name = 'Small Fonts'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
          inherited PaletteView: TTileMapView
            Width = 197
            Height = 52
            MapWidth = 16
            TileWidth = 11
            TileHeight = 11
            OnCustomMouseActionCheck = PaletteFramePaletteViewCustomMouseActionCheck
            OnContentsChanged = PaletteFramePaletteViewContentsChanged
            OnBeforeContentChange = PaletteFramePaletteViewBeforeContentChange
            OnSysKeyDown = MagicWandSysKeyDown
            OnSysKeyUp = MagicWandSysKeyUp
            OnMouseEnter = MagicWandMouseEnter
            OnMouseLeave = MagicWandMouseLeave
            PopupMenu = ColorsPopup
            OnEnter = PaletteFramePaletteViewEnter
            OnExit = PaletteFramePaletteViewExit
            OnMouseMove = PaletteFramePaletteViewMouseMove
          end
          inherited PaletteStatusBar: TTntStatusBar
            Width = 197
          end
        end
        object ColTblComboBox: TTntComboBox
          Left = 1
          Top = 1
          Width = 197
          Height = 21
          Align = alTop
          Style = csDropDownList
          ItemHeight = 13
          TabOrder = 2
          OnChange = ColTblComboBoxChange
          OnKeyDown = ColorTablesListBoxKeyDown
        end
      end
    end
    object FileEditToolBar: TTntToolBar
      Left = 0
      Top = 0
      Width = 75
      Height = 89
      Align = alLeft
      EdgeInner = esNone
      EdgeOuter = esNone
      Flat = True
      Images = ImgList
      Indent = 3
      TabOrder = 1
      Wrapable = False
      object FileNewButton: TTntToolButton
        Left = 3
        Top = 0
        Action = NewAction
      end
      object FileOpenButton: TTntToolButton
        Left = 26
        Top = 0
        Action = OpenAction
      end
      object LoadWithPluginBtn: TTntToolButton
        Left = 49
        Top = 0
        Action = LoadWithPluginAction
        Wrap = True
      end
      object FileSaveButton: TTntToolButton
        Left = 3
        Top = 22
        Action = SaveAction
      end
      object FileSaveAsButton: TTntToolButton
        Left = 26
        Top = 22
        Action = SaveAsAction
      end
      object SaveWithPluginBtn: TTntToolButton
        Left = 49
        Top = 22
        Action = SaveWithPluginAction
        Wrap = True
      end
      object EditCutButton: TTntToolButton
        Left = 3
        Top = 44
        Action = EditCutAction
      end
      object EditCopyButton: TTntToolButton
        Left = 26
        Top = 44
        Action = EditCopyAction
      end
      object EditPasteButton: TTntToolButton
        Left = 49
        Top = 44
        Action = EditPasteAction
        Wrap = True
      end
      object EditClearSelectionButton: TTntToolButton
        Left = 3
        Top = 66
        Action = EditDeleteAction
      end
      object ViewZoomInButton: TTntToolButton
        Left = 26
        Top = 66
        Action = ViewZoomInAction
      end
      object ViewZoomOutButton: TTntToolButton
        Left = 49
        Top = 66
        Action = ViewZoomOutAction
        Wrap = True
      end
    end
    object HistoryPages: TTntPageControl
      Left = 75
      Top = 0
      Width = 366
      Height = 89
      ActivePage = History
      Align = alClient
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -9
      Font.Name = 'Small Fonts'
      Font.Style = []
      MultiLine = True
      ParentFont = False
      TabHeight = 14
      TabOrder = 2
      TabPosition = tpLeft
      TabStop = False
      object History: TTntTabSheet
        Caption = 'History'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        object HistoryListBox: TListBox
          Left = 0
          Top = 0
          Width = 344
          Height = 81
          Style = lbVirtualOwnerDraw
          Align = alClient
          ItemHeight = 13
          TabOrder = 0
          OnClick = HistoryListBoxClick
          OnDrawItem = HistoryListBoxDrawItem
        end
      end
      object Macro: TTntTabSheet
        Caption = 'Macro'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabVisible = False
        object Panel1: TPanel
          Left = 0
          Top = 0
          Width = 105
          Height = 81
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 0
          object MacroToolBar: TTntToolBar
            Left = 0
            Top = 0
            Width = 105
            Height = 24
            EdgeInner = esNone
            EdgeOuter = esNone
            Flat = True
            Images = ImgList
            TabOrder = 0
            Wrapable = False
            object AddMacroButton: TTntToolButton
              Left = 0
              Top = 0
              Caption = 'AddMacroButton'
              ImageIndex = 48
            end
            object TntToolButton7: TTntToolButton
              Left = 23
              Top = 0
              Width = 8
              Caption = 'TntToolButton7'
              Style = tbsSeparator
            end
            object MacroUpBtn: TTntToolButton
              Left = 31
              Top = 0
              Caption = 'MacroUpBtn'
              ImageIndex = 1
            end
            object MacroDownBtn: TTntToolButton
              Left = 54
              Top = 0
              Caption = 'MacroDownBtn'
              ImageIndex = 2
            end
            object MacroDeleteButton: TTntToolButton
              Left = 77
              Top = 0
              Caption = 'MacroDeleteButton'
              ImageIndex = 0
            end
          end
          object MacroListBox: TListBox
            Left = 0
            Top = 24
            Width = 105
            Height = 57
            Style = lbVirtualOwnerDraw
            Align = alClient
            ItemHeight = 16
            TabOrder = 1
          end
        end
        object Panel2: TPanel
          Left = 105
          Top = 0
          Width = 239
          Height = 81
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 1
          object MacroActionsListBox: TListBox
            Left = 24
            Top = 0
            Width = 215
            Height = 81
            Style = lbVirtualOwnerDraw
            Align = alClient
            ItemHeight = 16
            TabOrder = 0
          end
          object MacroPlayToolBar: TTntToolBar
            Left = 0
            Top = 0
            Width = 24
            Height = 81
            Align = alLeft
            EdgeInner = esNone
            EdgeOuter = esNone
            Flat = True
            Images = ImgList
            TabOrder = 1
            Wrapable = False
            object TntToolButton4: TTntToolButton
              Left = 0
              Top = 0
              Caption = 'AddMacroButton'
              Grouped = True
              ImageIndex = 18
              Wrap = True
              Style = tbsCheck
            end
            object TntToolButton8: TTntToolButton
              Left = 0
              Top = 22
              Caption = 'MacroDownBtn'
              Grouped = True
              ImageIndex = 58
              Wrap = True
              Style = tbsCheck
            end
            object TntToolButton9: TTntToolButton
              Left = 0
              Top = 44
              Caption = 'MacroDeleteButton'
              Enabled = False
              ImageIndex = 11
            end
          end
        end
      end
    end
  end
  object LeftPanel: TPanel
    Left = 0
    Top = 89
    Width = 200
    Height = 593
    Align = alLeft
    BevelOuter = bvLowered
    TabOrder = 2
    object Splitter3: TSplitter
      Left = 1
      Top = 393
      Width = 198
      Height = 3
      Cursor = crVSplit
      Align = alBottom
      AutoSnap = False
      ResizeStyle = rsUpdate
    end
    inline MapsFrm: TMapsFrame
      Left = 1
      Top = 1
      Width = 198
      Height = 392
      Align = alClient
      TabOrder = 0
      inherited PageControl: TTntPageControl
        Width = 198
        Height = 392
        inherited PropsPage: TTntTabSheet
          inherited Splitter3: TSplitter
            Width = 190
          end
          inherited MapsListPanel: TPanel
            Width = 190
            inherited Splitter1: TSplitter
              Width = 190
            end
            inherited MapsListBox: TListBox
              Width = 190
              PopupMenu = MapsPopup
              OnContextPopup = MapsFrameMapsListBoxContextPopup
              OnEnter = MapsFrameListBoxEnter
              OnExit = MapsFrameListBoxExit
            end
            inherited MapsToolBar: TTntToolBar
              Width = 190
              Images = ImgList
            end
            inherited MapPropsPanel: TPanel
              Width = 190
              inherited MapPropertiesLabel: TTntLabel
                Width = 190
              end
              inherited MapPropEditor: TPropertyEditor
                Width = 190
                ValueWidth = 96
                Columns = <
                  item
                    Margin = 0
                    Options = [coEnabled, coParentBidiMode, coParentColor, coResizable, coVisible, coFixed]
                    Position = 0
                    Width = 90
                  end
                  item
                    Margin = 0
                    Options = [coEnabled, coParentBidiMode, coParentColor, coResizable, coVisible, coFixed]
                    Position = 1
                    Width = 96
                  end>
              end
            end
          end
          inherited LayerListPanel: TPanel
            Width = 190
            Height = 219
            inherited Splitter2: TSplitter
              Top = 132
              Width = 190
            end
            inherited LayersListBox: TLayerListBox
              Width = 190
              Height = 109
              PopupMenu = LayersPopup
              OnContextPopup = MapsFrameLayersListBoxContextPopup
              OnEnter = MapsFrameListBoxEnter
              OnExit = MapsFrameListBoxExit
            end
            inherited LayersToolBar: TTntToolBar
              Width = 190
              Images = ImgList
            end
            inherited Panel1: TPanel
              Top = 135
              Width = 190
              inherited LayersLabel: TTntLabel
                Width = 190
              end
              inherited LayerPropEditor: TPropertyEditor
                Width = 190
                ValueWidth = 66
                Columns = <
                  item
                    Margin = 0
                    Options = [coEnabled, coParentBidiMode, coParentColor, coResizable, coVisible, coFixed]
                    Position = 0
                    Width = 120
                  end
                  item
                    Margin = 0
                    Options = [coEnabled, coParentBidiMode, coParentColor, coResizable, coVisible, coFixed]
                    Position = 1
                    Width = 66
                  end>
              end
            end
          end
        end
        inherited VisualPage: TTntTabSheet
          inherited MapFrame: TMapFrame
            inherited MapStatusBar: TTntStatusBar
              Width = 190
              Panels = <
                item
                  Text = 'Map'
                  Width = 40
                end
                item
                  Width = 50
                end
                item
                  Width = 40
                end
                item
                  Width = 40
                end
                item
                  Width = 65
                end
                item
                  Width = 50
                end>
            end
            inherited MapEditView: TTileMapView
              Width = 190
              Height = 333
              OnCustomMouseActionCheck = MapFrameMapEditViewCustomMouseActionCheck
              OnCustomMouseAction = MapFrameMapEditViewCustomMouseAction
              OnCustomMouseActionFinish = MapFrameMapEditViewCustomMouseActionFinish
              OnSelectionChanged = MapFrameMapEditViewSelectionChanged
              OnContentsChanged = MapFrameMapEditViewContentsChanged
              OnSysKeyDown = MagicWandSysKeyDown
              OnSysKeyUp = MagicWandSysKeyUp
              OnMouseEnter = MagicWandMouseEnter
              OnMouseLeave = MagicWandMouseLeave
              PopupMenu = MapPopup
              OnEnter = MapFrameMapEditViewEnter
              OnExit = MapFrameMapEditViewExit
              OnMouseMove = MapFrameMapEditViewMouseMove
              OnMouseUp = MapFrameMapEditViewMouseUp
            end
          end
        end
      end
      inherited ActionList: TTntActionList
        Images = ImgList
        Left = 80
        Top = 120
      end
      inherited ImageList: TImageList
        Left = 120
      end
    end
    object SelectedCellPanel: TPanel
      Left = 1
      Top = 396
      Width = 198
      Height = 196
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 1
      object SelCellPropLabel: TTntLabel
        Left = 0
        Top = 0
        Width = 198
        Height = 13
        Align = alTop
        Caption = 'Cell properties'
        ShowAccelChar = False
      end
      object SelCellPropEditor: TPropertyEditor
        Tag = 2
        Left = 0
        Top = 13
        Width = 198
        Height = 183
        BoldChangedValues = False
        ButtonFillMode = fmShaded
        Align = alClient
        ScrollBarOptions.ScrollBars = ssVertical
        TabOrder = 0
        PropertyWidth = 90
        ValueWidth = 104
        OnEllipsisClick = PropEditorEllipsisClick
        Columns = <
          item
            Margin = 0
            Options = [coEnabled, coParentBidiMode, coParentColor, coResizable, coVisible, coFixed]
            Position = 0
            Width = 90
          end
          item
            Margin = 0
            Options = [coEnabled, coParentBidiMode, coParentColor, coResizable, coVisible, coFixed]
            Position = 1
            Width = 104
          end>
      end
    end
  end
  object CentralPanel: TPanel
    Left = 203
    Top = 89
    Width = 619
    Height = 593
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    object Splitter8: TSplitter
      Left = 0
      Top = 390
      Width = 619
      Height = 3
      Cursor = crVSplit
      Align = alBottom
      AutoSnap = False
      Beveled = True
      ResizeStyle = rsUpdate
    end
    object TileSetsPanel: TPanel
      Left = 0
      Top = 393
      Width = 619
      Height = 200
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 0
      inline TilesFrm: TTileSetsFrame
        Left = 0
        Top = 0
        Width = 619
        Height = 200
        Align = alClient
        TabOrder = 0
        inherited PageControl: TTntPageControl
          Width = 619
          Height = 200
          ActivePage = TilesFrm.VisualPage
          MultiLine = True
          TabPosition = tpLeft
          inherited PropertiesPage: TTntTabSheet
            inherited PropsPanel: TPanel
              inherited TileSetPropLabel: TTntLabel
                Width = 86
              end
              inherited TileSetPropEditor: TPropertyEditor
                Width = 597
                Height = 101
                ValueWidth = 455
                Columns = <
                  item
                    Margin = 0
                    Options = [coEnabled, coParentBidiMode, coParentColor, coResizable, coVisible, coFixed]
                    Position = 0
                    Width = 142
                  end
                  item
                    Margin = 0
                    Options = [coEnabled, coParentBidiMode, coParentColor, coResizable, coVisible, coFixed]
                    Position = 1
                    Width = 455
                  end>
              end
              inherited AttrToolBar: TTntToolBar
                Top = 114
                Width = 597
                Images = ImgList
              end
            end
            inherited TileSetsListBox: TListBox
              PopupMenu = TileSetsPopup
              OnContextPopup = TilesFrameTileSetsListBoxContextPopup
              OnEnter = TilesFrameTileSetsListBoxEnter
              OnExit = TilesFrameTileSetsListBoxExit
            end
            inherited TileSetsToolBar: TTntToolBar
              Images = ImgList
              inherited TileSetLabel: TTntLabel
                Height = 13
              end
            end
          end
          inherited VisualPage: TTntTabSheet
            inherited TileSetsComboBox: TTntComboBox
              Width = 597
            end
            inherited TilesFrame: TTilesFrame
              Width = 597
              Height = 171
              inherited StatusBar: TTntStatusBar
                Width = 597
              end
              inherited TileSetView: TTileMapView
                Width = 597
                Height = 157
                OnCustomMouseActionCheck = TilesFrameTileSetViewCustomMouseActionCheck
                OnCustomMouseAction = TilesFrameTileSetViewCustomMouseAction
                OnSelectionChanged = TilesFrameTileSetViewSelectionChanged
                OnContentsChanged = TilesFrameTileSetViewContentsChanged
                OnBeforeContentChange = TilesFrameTileSetViewBeforeContentChange
                OnSysKeyDown = MagicWandSysKeyDown
                OnSysKeyUp = MagicWandSysKeyUp
                OnMouseEnter = MagicWandMouseEnter
                OnMouseLeave = MagicWandMouseLeave
                PopupMenu = TilesPopup
                OnContextPopup = TilesFrameTileSetViewContextPopup
                OnEnter = TilesFrameTileSetViewEnter
                OnExit = TilesFrameTileSetViewExit
                OnMouseMove = TilesFrameTileSetViewMouseMove
              end
            end
          end
        end
        inherited ActionList: TTntActionList
          Left = 192
          Top = 96
        end
        inherited ImageList: TImageList
          Left = 136
          Top = 104
        end
        inherited SaveTilesDialog: TTntSaveDialog
          OnClose = nil
          OnShow = nil
          OnTypeChange = nil
        end
      end
    end
    object MainEditPanel: TPanel
      Left = 0
      Top = 0
      Width = 619
      Height = 390
      Align = alClient
      BevelOuter = bvLowered
      TabOrder = 1
    end
  end
  object RightPanel: TPanel
    Left = 825
    Top = 89
    Width = 200
    Height = 593
    Align = alRight
    BevelOuter = bvLowered
    TabOrder = 4
    object Splitter2: TSplitter
      Left = 1
      Top = 393
      Width = 198
      Height = 3
      Cursor = crVSplit
      Align = alBottom
      AutoSnap = False
      ResizeStyle = rsUpdate
    end
    inline BrushesFrame: TMapsFrame
      Left = 1
      Top = 1
      Width = 198
      Height = 392
      Align = alClient
      TabOrder = 0
      inherited PageControl: TTntPageControl
        Width = 198
        Height = 392
        ActivePage = BrushesFrame.VisualPage
        TabHeight = 16
        inherited PropsPage: TTntTabSheet
          inherited Splitter3: TSplitter
            Width = 190
          end
          inherited MapsListPanel: TPanel
            Width = 190
            inherited Splitter1: TSplitter
              Width = 190
            end
            inherited MapsListBox: TListBox
              Width = 190
              PopupMenu = MapsPopup
              OnContextPopup = MapsFrameMapsListBoxContextPopup
              OnEnter = MapsFrameListBoxEnter
              OnExit = MapsFrameListBoxExit
            end
            inherited MapsToolBar: TTntToolBar
              Width = 190
              Images = ImgList
              inherited NewMapToolBtn: TTntToolButton
                Hint = 'New|Adds a new map to the project'
                Action = BrushesFrame.BrushNewAction
                Caption = 'New Map'
              end
              inherited MapUpToolBtn: TTntToolButton
                Hint = 'Move Up|Moves selected maps up the list'
                Action = BrushesFrame.BrushUpAction
              end
              inherited MapDownToolBtn: TTntToolButton
                Hint = 'Move Down|Moves selected maps down the list'
                Action = BrushesFrame.BrushDownAction
              end
              inherited MapDeleteToolBtn: TTntToolButton
                Hint = 'Delete|Deletes selected maps from the project'
                Action = BrushesFrame.BrushDeleteAction
              end
              inherited MapsListLabel: TTntLabel
                Height = 13
              end
            end
            inherited MapPropsPanel: TPanel
              Width = 190
              inherited MapPropertiesLabel: TTntLabel
                Width = 72
              end
              inherited MapPropEditor: TPropertyEditor
                Width = 190
                ValueWidth = 100
                Columns = <
                  item
                    Margin = 0
                    Options = [coEnabled, coParentBidiMode, coParentColor, coResizable, coVisible, coFixed]
                    Position = 0
                    Width = 90
                  end
                  item
                    Margin = 0
                    Options = [coEnabled, coParentBidiMode, coParentColor, coResizable, coVisible, coFixed]
                    Position = 1
                    Width = 100
                  end>
              end
            end
          end
          inherited LayerListPanel: TPanel
            Width = 190
            Height = 217
            inherited Splitter2: TSplitter
              Top = 130
              Width = 190
            end
            inherited LayersListBox: TLayerListBox
              Width = 190
              Height = 107
              PopupMenu = LayersPopup
              OnContextPopup = MapsFrameLayersListBoxContextPopup
              OnEnter = MapsFrameListBoxEnter
              OnExit = MapsFrameListBoxExit
            end
            inherited LayersToolBar: TTntToolBar
              Width = 190
              Images = ImgList
              inherited LayersListLabel: TTntLabel
                Width = 59
                Height = 13
                Caption = 'Brush layers'
              end
            end
            inherited Panel1: TPanel
              Top = 133
              Width = 190
              inherited LayersLabel: TTntLabel
                Width = 79
              end
              inherited LayerPropEditor: TPropertyEditor
                Width = 190
                ValueWidth = 70
                Columns = <
                  item
                    Margin = 0
                    Options = [coEnabled, coParentBidiMode, coParentColor, coResizable, coVisible, coFixed]
                    Position = 0
                    Width = 120
                  end
                  item
                    Margin = 0
                    Options = [coEnabled, coParentBidiMode, coParentColor, coResizable, coVisible, coFixed]
                    Position = 1
                    Width = 70
                  end>
              end
            end
          end
        end
        inherited VisualPage: TTntTabSheet
          inherited MapsComboBox: TTntComboBox
            Width = 190
          end
          inherited MapFrame: TMapFrame
            Width = 190
            Height = 345
            inherited MapStatusBar: TTntStatusBar
              Width = 190
              Panels = <
                item
                  Text = 'Brush'
                  Width = 40
                end
                item
                  Width = 50
                end
                item
                  Width = 40
                end
                item
                  Width = 40
                end
                item
                  Width = 65
                end
                item
                  Width = 50
                end>
            end
            inherited MapEditView: TTileMapView
              Width = 190
              Height = 331
              OnCustomMouseActionCheck = MapFrameMapEditViewCustomMouseActionCheck
              OnCustomMouseAction = MapFrameMapEditViewCustomMouseAction
              OnCustomMouseActionFinish = MapFrameMapEditViewCustomMouseActionFinish
              OnSelectionChanged = MapFrameMapEditViewSelectionChanged
              OnContentsChanged = MapFrameMapEditViewContentsChanged
              OnSysKeyDown = MagicWandSysKeyDown
              OnSysKeyUp = MagicWandSysKeyUp
              OnMouseEnter = MagicWandMouseEnter
              OnMouseLeave = MagicWandMouseLeave
              PopupMenu = MapPopup
              OnEnter = MapFrameMapEditViewEnter
              OnExit = MapFrameMapEditViewExit
              OnMouseMove = MapFrameMapEditViewMouseMove
              OnMouseUp = MapFrameMapEditViewMouseUp
            end
          end
        end
      end
      inherited ActionList: TTntActionList
        Images = ImgList
        Left = 24
        Top = 120
      end
      inherited ImageList: TImageList
        Left = 120
        Top = 88
      end
    end
    object SelectedTilePanel: TPanel
      Left = 1
      Top = 396
      Width = 198
      Height = 196
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 1
      inline TilePropsFrm: TTilePropsFrame
        Left = 0
        Top = 0
        Width = 198
        Height = 196
        Align = alClient
        TabOrder = 0
        inherited SelTilePropLabel: TTntLabel
          Width = 198
        end
        inherited SelTilePropEditor: TPropertyEditor
          Width = 198
          Height = 183
          ValueWidth = 104
          Columns = <
            item
              Margin = 0
              Options = [coEnabled, coParentBidiMode, coParentColor, coResizable, coVisible, coFixed]
              Position = 0
              Width = 90
            end
            item
              Margin = 0
              Options = [coEnabled, coParentBidiMode, coParentColor, coResizable, coVisible, coFixed]
              Position = 1
              Width = 104
            end>
        end
      end
    end
  end
  object MainMenu: TTntMainMenu
    Images = ImgList
    Left = 392
    Top = 192
    object MenuFile: TTntMenuItem
      Caption = '&File'
      object Newproject1: TTntMenuItem
        Action = NewAction
      end
      object N31: TTntMenuItem
        Caption = '-'
      end
      object Saveproject1: TTntMenuItem
        Action = OpenAction
      end
      object Saveproject2: TTntMenuItem
        Action = SaveAction
      end
      object Saveprojectas1: TTntMenuItem
        Action = SaveAsAction
      end
      object N1: TTntMenuItem
        Caption = '-'
      end
      object ImportfromTED5map1: TTntMenuItem
        Action = LoadWithPluginAction
      end
      object ExportmapstoSMDformat1: TTntMenuItem
        Action = SaveWithPluginAction
      end
      object MenuFileSeparator1: TTntMenuItem
        Caption = '-'
      end
      object MenuFileExitItem: TTntMenuItem
        Action = ExitAction
      end
    end
    object Project1: TTntMenuItem
      Caption = '&Project'
      object ColorTablesMenu: TTntMenuItem
        Caption = '&Color Tables'
        ImageIndex = 56
        object New1: TTntMenuItem
          Action = ColorTableNewAction
        end
        object N6: TTntMenuItem
          Caption = '-'
        end
        object AddFrom1: TTntMenuItem
          Action = ColTblAddFromFilesAction
        end
        object RemoveBrush2: TTntMenuItem
          Action = ColorTableSaveToAction
        end
        object N32: TTntMenuItem
          Caption = '-'
        end
        object ImportColorsFrom1: TTntMenuItem
          Action = ColorsImportAction
        end
        object ExportColorsTo1: TTntMenuItem
          Action = ColorsExportAction
        end
        object N39: TTntMenuItem
          Caption = '-'
        end
        object MakeGradient1: TTntMenuItem
          Action = ColorsMakeGradientAction
        end
        object N8: TTntMenuItem
          Caption = '-'
        end
        object MoveUp1: TTntMenuItem
          Action = ColorTableUpAction
        end
        object MoveDown1: TTntMenuItem
          Action = ColorTableDownAction
        end
        object N9: TTntMenuItem
          Caption = '-'
        end
        object Remove1: TTntMenuItem
          Action = ColorTableDeleteAction
        end
      end
      object TileSetsMenu: TTntMenuItem
        Caption = '&Tile Sets'
        ImageIndex = 55
        object NewTileSet1: TTntMenuItem
          Action = TilesFrm.TileSetNewAction
        end
        object N7: TTntMenuItem
          Caption = '-'
        end
        object AddFrom2: TTntMenuItem
          Action = TilesFrm.TileSetAddFromFilesAction
        end
        object SaveTo1: TTntMenuItem
          Action = TilesFrm.TileSetSaveToFileAction
        end
        object N33: TTntMenuItem
          Caption = '-'
        end
        object ImportTiles1: TTntMenuItem
          Action = TilesFrm.TilesImportAction
        end
        object ExportTiles1: TTntMenuItem
          Action = TilesFrm.TilesExportAction
        end
        object N12: TTntMenuItem
          Caption = '-'
        end
        object New3: TTntMenuItem
          Action = TilesFrm.TileSetUpAction
        end
        object MoveDown3: TTntMenuItem
          Action = TilesFrm.TileSetDownAction
        end
        object N13: TTntMenuItem
          Caption = '-'
        end
        object Delete2: TTntMenuItem
          Action = TilesFrm.TileSetDeleteAction
        end
      end
      object MapsMenu: TTntMenuItem
        Caption = '&Maps'
        ImageIndex = 54
        object NewMap1: TTntMenuItem
          Action = MapsFrm.MapNewAction
        end
        object N10: TTntMenuItem
          Caption = '-'
        end
        object NewMap2: TTntMenuItem
          Action = MapsFrm.MapAddFromFileAction
        end
        object NewMap3: TTntMenuItem
          Action = MapsFrm.MapSaveToFileAction
        end
        object N34: TTntMenuItem
          Caption = '-'
        end
        object New2: TTntMenuItem
          Action = MapsFrm.MapUpAction
        end
        object MoveDown2: TTntMenuItem
          Action = MapsFrm.MapDownAction
        end
        object N11: TTntMenuItem
          Caption = '-'
        end
        object Delete1: TTntMenuItem
          Action = MapsFrm.MapDeleteAction
        end
        object N20: TTntMenuItem
          Caption = '-'
        end
        object Layer1: TTntMenuItem
          Caption = '&Layers'
          ImageIndex = 53
          object New4: TTntMenuItem
            Action = MapsFrm.LayerNewAction
          end
          object N14: TTntMenuItem
            Caption = '-'
          end
          object MoveUp2: TTntMenuItem
            Action = MapsFrm.LayerUpAction
          end
          object MoveDown4: TTntMenuItem
            Action = MapsFrm.LayerDownAction
          end
          object N15: TTntMenuItem
            Caption = '-'
          end
          object Delete3: TTntMenuItem
            Action = MapsFrm.LayerDeleteAction
          end
        end
      end
      object BrushesMenu: TTntMenuItem
        Caption = '&Brushes'
        ImageIndex = 20
        object NewBrush1: TTntMenuItem
          Action = BrushesFrame.BrushNewAction
        end
        object N18: TTntMenuItem
          Caption = '-'
        end
        object AddFrom3: TTntMenuItem
          Action = MapsFrm.BrushAddFromFileAction
        end
        object SaveTo2: TTntMenuItem
          Action = BrushesFrame.BrushSaveToFileAction
        end
        object N35: TTntMenuItem
          Caption = '-'
        end
        object MoveUp4: TTntMenuItem
          Action = BrushesFrame.BrushUpAction
        end
        object MoveDown6: TTntMenuItem
          Action = BrushesFrame.BrushDownAction
        end
        object N19: TTntMenuItem
          Caption = '-'
        end
        object Delete5: TTntMenuItem
          Action = BrushesFrame.BrushDeleteAction
        end
        object N21: TTntMenuItem
          Caption = '-'
        end
        object Layer2: TTntMenuItem
          Caption = '&Layers'
          ImageIndex = 53
          object New5: TTntMenuItem
            Action = BrushesFrame.LayerNewAction
          end
          object N16: TTntMenuItem
            Caption = '-'
          end
          object MoveUp3: TTntMenuItem
            Action = BrushesFrame.LayerUpAction
          end
          object MoveDown5: TTntMenuItem
            Action = BrushesFrame.LayerDownAction
          end
          object N17: TTntMenuItem
            Caption = '-'
          end
          object Delete4: TTntMenuItem
            Action = BrushesFrame.LayerDeleteAction
          end
        end
      end
    end
    object Edit1: TTntMenuItem
      Caption = '&Edit'
      object Undo1: TTntMenuItem
        Action = EditUndoAction
      end
      object Redo1: TTntMenuItem
        Action = EditRedoAction
      end
      object N25: TTntMenuItem
        Caption = '-'
      end
      object Cut2: TTntMenuItem
        Action = EditCutAction
      end
      object Copy3: TTntMenuItem
        Action = EditCopyAction
      end
      object Paste3: TTntMenuItem
        Action = EditPasteAction
      end
      object N27: TTntMenuItem
        Caption = '-'
      end
      object All3: TTntMenuItem
        Action = SelectAllAction
      end
      object Deselect3: TTntMenuItem
        Action = SelectDeselectAction
      end
      object Inverse3: TTntMenuItem
        Action = SelectInverseAction
      end
      object N43: TTntMenuItem
        Caption = '-'
      end
      object FillSelection3: TTntMenuItem
        Action = SelectFillAction
      end
      object N2: TTntMenuItem
        Caption = '-'
      end
      object ClearSelection2: TTntMenuItem
        Action = EditDeleteAction
      end
    end
  end
  object ActionList: TTntActionList
    Images = ImgList
    Left = 240
    Top = 149
    object ToolSingleSelectAction: TTntAction
      Category = 'Draw tools'
      AutoCheck = True
      Caption = 'Single Select'
      Checked = True
      GroupIndex = 1
      Hint = 'Single Select'
      ImageIndex = 41
      OnExecute = DrawToolActionExecute
      OnUpdate = SimpleActionUpdate
    end
    object BrushSmallAction: TTntAction
      Category = 'Brush sizes'
      AutoCheck = True
      Caption = 'Small'
      Checked = True
      GroupIndex = 4
      Hint = 'Small'
      ImageIndex = 49
      OnExecute = SizeToolActionExecute
      OnUpdate = SimpleActionUpdate
    end
    object ToolMultiSelectAction: TTntAction
      Tag = 1
      Category = 'Draw tools'
      AutoCheck = True
      Caption = 'Rectangular Select'
      GroupIndex = 1
      Hint = 'Rectangular Select'
      ImageIndex = 13
      OnExecute = DrawToolActionExecute
      OnUpdate = SimpleActionUpdate
    end
    object ToolMagicWandAction: TTntAction
      Tag = 2
      Category = 'Draw tools'
      AutoCheck = True
      Caption = 'Magic Wand'
      GroupIndex = 1
      Hint = 'Magic Wand'
      ImageIndex = 12
      OnExecute = DrawToolActionExecute
      OnUpdate = SimpleActionUpdate
    end
    object SelectAllAction: TTntAction
      Category = 'Select'
      Caption = 'Select &All'
      Hint = 'All|Selects all contents of the focused window'
      ShortCut = 16449
      OnExecute = SelectAllActionExecute
      OnUpdate = SelectAllActionUpdate
    end
    object ToolDropperAction: TTntAction
      Tag = 3
      Category = 'Draw tools'
      AutoCheck = True
      Caption = 'Picker'
      GroupIndex = 1
      Hint = 'Picker'
      ImageIndex = 17
      OnExecute = DrawToolActionExecute
      OnUpdate = SimpleActionUpdate
    end
    object SelectInverseAction: TTntAction
      Category = 'Select'
      Caption = '&Inverse'
      Hint = 'Inverse|Inverses selection in a focused window'
      ShortCut = 49225
      OnExecute = SelectInverseActionExecute
      OnUpdate = SelectDeselectActionUpdate
    end
    object ExitAction: TTntAction
      Category = 'File'
      Caption = 'E&xit'
      Hint = 'Quits the program'
      OnExecute = ExitActionExecute
    end
    object NewAction: TTntAction
      Category = 'File'
      Caption = '&New Project'
      Hint = 'New Project|Creates a new project'
      ImageIndex = 35
      OnExecute = NewActionExecute
      OnUpdate = SimpleActionUpdate
    end
    object OpenAction: TTntAction
      Category = 'File'
      Caption = '&Open Project...'
      Hint = 'Open Project|Loads a project from an existing file'
      ImageIndex = 36
      ShortCut = 16463
      SecondaryShortCuts.Strings = (
        'F3')
      OnExecute = OpenActionExecute
      OnUpdate = SimpleActionUpdate
    end
    object SaveAction: TTntAction
      Category = 'File'
      Caption = '&Save Project'
      Hint = 'Save Project|Saves the project to a new file'
      ImageIndex = 37
      ShortCut = 16467
      SecondaryShortCuts.Strings = (
        'F2')
      OnExecute = SaveActionExecute
      OnUpdate = SimpleActionUpdate
    end
    object SaveAsAction: TTntAction
      Category = 'File'
      Caption = 'Save Project &As...'
      Hint = 'Save Project As|Saves the project to a file with a new name'
      ImageIndex = 38
      ShortCut = 49235
      OnExecute = SaveAsActionExecute
      OnUpdate = SimpleActionUpdate
    end
    object ToolPencilAction: TTntAction
      Tag = 4
      Category = 'Draw tools'
      AutoCheck = True
      Caption = 'Pencil'
      GroupIndex = 1
      Hint = 'Pencil'
      ImageIndex = 19
      OnExecute = DrawToolActionExecute
      OnUpdate = SimpleActionUpdate
    end
    object ToolBrushAction: TTntAction
      Tag = 5
      Category = 'Draw tools'
      AutoCheck = True
      Caption = 'Brush'
      GroupIndex = 1
      Hint = 'Brush'
      ImageIndex = 20
      OnExecute = DrawToolActionExecute
      OnUpdate = SimpleActionUpdate
    end
    object ToolEraserAction: TTntAction
      Tag = 6
      Category = 'Draw tools'
      AutoCheck = True
      Caption = 'Eraser'
      GroupIndex = 1
      Hint = 'Eraser'
      ImageIndex = 14
      OnExecute = DrawToolActionExecute
      OnUpdate = SimpleActionUpdate
    end
    object ToolDrawLineAction: TTntAction
      Category = 'Line draw tools'
      AutoCheck = True
      Caption = 'Draw Line'
      Checked = True
      GroupIndex = 3
      Hint = 'Draw Line'
      ImageIndex = 49
      OnExecute = ToolDrawLineActionExecute
      OnUpdate = SimpleActionUpdate
    end
    object ToolFloodFillAction: TTntAction
      Tag = 7
      Category = 'Draw tools'
      AutoCheck = True
      Caption = 'Flood Fill'
      GroupIndex = 1
      Hint = 'Flood Fill'
      ImageIndex = 16
      OnExecute = DrawToolActionExecute
      OnUpdate = SimpleActionUpdate
    end
    object MapNewAction: TTntAction
      Category = 'Maps'
      Caption = 'New'
      Hint = 'New|Adds a new empty map to the project'
      ImageIndex = 5
      OnExecute = MapNewActionExecute
      OnUpdate = SimpleActionUpdate
    end
    object MapUpAction: TTntAction
      Category = 'Maps'
      Caption = 'Move Up'
      Hint = 'Move Up|Moves map up the list'
      ImageIndex = 1
    end
    object MapDownAction: TTntAction
      Category = 'Maps'
      Caption = 'Move Map Down'
      Hint = 'Move Map Down|Moves map down the list'
      ImageIndex = 2
    end
    object MapDeleteAction: TTntAction
      Category = 'Maps'
      Caption = 'Remove Map'
      Hint = 'Remove Map|Removes map from the project'
      ImageIndex = 0
    end
    object LayerNewAction: TTntAction
      Category = 'Layers'
      Caption = 'New Layer'
      Hint = 'New Layer|Adds a new layer to the map'
      ImageIndex = 6
    end
    object LayerUpAction: TTntAction
      Category = 'Layers'
      Caption = 'Layer Up'
      Hint = 'Layer Up|Moves layer up the list'
      ImageIndex = 1
    end
    object LayerDownAction: TTntAction
      Category = 'Layers'
      Caption = 'Layer Down'
      Hint = 'Layer Down|Moves layer down the list'
      ImageIndex = 2
    end
    object LayerDeleteAction: TTntAction
      Category = 'Layers'
      Caption = 'Remove Layer'
      Hint = 'Remove Layer|Removes layer from the map'
      ImageIndex = 0
    end
    object ColorTableNewAction: TTntAction
      Category = 'Color tables'
      Caption = '&New Color Table'
      Hint = 'New Color Table|Adds a new color table to the project'
      ImageIndex = 7
      OnExecute = ColorTableNewActionExecute
      OnUpdate = SimpleActionUpdate
    end
    object ColTblAddFromFilesAction: TTntAction
      Category = 'Color tables'
      Caption = '&Add From...'
      Hint = 'Add From|Loads color tables from files and adds them to the list'
      ImageIndex = 62
      OnExecute = ColTblAddFromFilesActionExecute
      OnUpdate = SimpleActionUpdate
    end
    object ColorTableSaveToAction: TTntAction
      Category = 'Color tables'
      Caption = '&Save To...'
      Hint = 'Save To|Saves all selected color tables to disk'
      ImageIndex = 38
      OnExecute = ColorTableSaveToActionExecute
      OnUpdate = ColorTableSaveToActionUpdate
    end
    object ColorTableUpAction: TTntAction
      Category = 'Color tables'
      Caption = 'Move &Up'
      Hint = 'Move Up|Moves all selected color tables up the list'
      ImageIndex = 1
      OnExecute = ColorTableUpActionExecute
      OnUpdate = ColorTableUpActionUpdate
    end
    object ColorTableDownAction: TTntAction
      Category = 'Color tables'
      Caption = 'Move &Down'
      Hint = 'Move Down|Moves all selected color tables down the list'
      ImageIndex = 2
      OnExecute = ColorTableDownActionExecute
      OnUpdate = ColorTableDownActionUpdate
    end
    object ColorTableDeleteAction: TTntAction
      Category = 'Color tables'
      Caption = 'D&elete'
      Hint = 'Delete|Deletes all selected color tables from the project'
      ImageIndex = 0
      OnExecute = ColorTableDeleteActionExecute
      OnUpdate = ColorTableDeleteActionUpdate
    end
    object TileSetNewAction: TTntAction
      Category = 'Tile sets'
      Caption = 'New'
      Hint = 'New|Adds a new tile set to the project'
      ImageIndex = 46
    end
    object TileSetUpAction: TTntAction
      Category = 'Tile sets'
      Caption = 'Move Tile Set Up'
      Hint = 'Move Tile Set Up|Moves tile set up the list'
      ImageIndex = 1
    end
    object TileSetDownAction: TTntAction
      Category = 'Tile sets'
      Caption = 'Move Tile Set Down'
      Hint = 'Move Tile Set Down|Moves tile set down the list'
      ImageIndex = 2
    end
    object TileSetDeleteAction: TTntAction
      Category = 'Tile sets'
      Caption = 'Remove Tile Set'
      Hint = 'Remove Tile Set|Removes tile set from the project'
      ImageIndex = 0
    end
    object BrushNewAction: TTntAction
      Category = 'Brushes'
      Caption = 'New'
      Hint = 'New|Adds a new brush to the map'
      ImageIndex = 9
    end
    object BrushUpAction: TTntAction
      Category = 'Brushes'
      Caption = 'Move Brush Up'
      Hint = 'Move Brush Up|Moves brush up the list'
      ImageIndex = 1
    end
    object BrushDownAction: TTntAction
      Category = 'Brushes'
      Caption = 'Move Brush Down'
      Hint = 'Move Brush Down|Moves brush down the list'
      ImageIndex = 2
    end
    object BrushDeleteAction: TTntAction
      Category = 'Brushes'
      Caption = 'Remove Brush'
      Hint = 'Remove Brush|Removes brush from the project'
      ImageIndex = 0
    end
    object BrushEditModeAction: TTntAction
      Tag = 1
      Category = 'Brushes'
      AutoCheck = True
      Caption = 'Brush Edit Mode'
      GroupIndex = 2
      Hint = 'Brush Edit Mode|Activates brush edit mode'
      ImageIndex = 45
      OnExecute = EditModeActionExecute
      OnUpdate = SimpleActionUpdate
    end
    object ColorTableActiveModeAction: TTntAction
      Category = 'Color tables'
      AutoCheck = True
      Caption = 'Show Selected Color Table'
      Hint = 
        'Show Selected Color Table|If not checked, shows color table of a' +
        'ctive map '
      ImageIndex = 43
      OnUpdate = SimpleActionUpdate
    end
    object ColorsImportAction: TTntAction
      Category = 'Color tables'
      Caption = 'Import Colors...'
      Hint = 
        'Import Colors|Imports colors into selected color tables from fil' +
        'es'
      ImageIndex = 36
      OnExecute = ColorsImportActionExecute
      OnUpdate = ColorsImportExportActionUpdate
    end
    object TileSetEditModeAction: TTntAction
      Tag = 2
      Category = 'Tile sets'
      AutoCheck = True
      Caption = 'Tile Set Edit Mode'
      GroupIndex = 2
      Hint = 'Tile Set Edit Mode|Activates tile set edit mode'
      ImageIndex = 47
      OnExecute = EditModeActionExecute
      OnUpdate = SimpleActionUpdate
    end
    object TileEditModeAction: TTntAction
      Tag = 3
      Category = 'Tile sets'
      AutoCheck = True
      Caption = 'Tile Edit Mode'
      GroupIndex = 2
      Hint = 'Tile Edit Mode|Activates tile edit mode'
      ImageIndex = 44
      OnExecute = EditModeActionExecute
      OnUpdate = SimpleActionUpdate
    end
    object MapEditModeAction: TTntAction
      Category = 'Maps'
      AutoCheck = True
      Caption = 'Map Edit Mode'
      Checked = True
      GroupIndex = 2
      Hint = 'Map Edit Mode|Activates map edit mode'
      ImageIndex = 42
      OnExecute = EditModeActionExecute
      OnUpdate = SimpleActionUpdate
    end
    object EditUndoAction: TTntAction
      Tag = -1
      Category = 'History'
      Caption = '&Undo'
      Hint = 'Undo|Reverts the last action'
      ImageIndex = 39
      ShortCut = 16474
      SecondaryShortCuts.Strings = (
        'Alt+BkSp')
      OnExecute = EditUndoRedoActionExecute
      OnUpdate = EditUndoActionUpdate
    end
    object EditRedoAction: TTntAction
      Tag = 1
      Category = 'History'
      Caption = '&Redo'
      Hint = 'Redo|Redoes a reverted action'
      ImageIndex = 40
      ShortCut = 16473
      SecondaryShortCuts.Strings = (
        'Shift+Alt+BkSp')
      OnExecute = EditUndoRedoActionExecute
      OnUpdate = EditRedoActionUpdate
    end
    object EditCutAction: TTntAction
      Tag = 1
      Category = 'Edit'
      Caption = 'Cu&t'
      Hint = 'Cut|Cuts the selection and puts it on the Clipboard'
      ImageIndex = 33
      ShortCut = 16472
      OnExecute = EditCopyActionExecute
      OnUpdate = SelectionActionUpdate
    end
    object EditCopyAction: TTntAction
      Category = 'Edit'
      Caption = '&Copy'
      Hint = 'Copy|Copies the selection and puts it on the Clipboard'
      ImageIndex = 30
      ShortCut = 16451
      OnExecute = EditCopyActionExecute
      OnUpdate = SelectionActionUpdate
    end
    object EditPasteAction: TTntAction
      Category = 'Edit'
      Caption = '&Paste'
      Hint = 'Paste|Inserts Clipboard contents'
      ImageIndex = 31
      ShortCut = 16470
      OnExecute = EditPasteActionExecute
      OnUpdate = EditPasteActionUpdate
    end
    object SelectDeselectAction: TTntAction
      Category = 'Select'
      Caption = 'Dese&lect'
      Hint = 'Deselect|Removes selection from the focused window'
      ShortCut = 16452
      OnExecute = SelectDeselectActionExecute
      OnUpdate = SelectDeselectActionUpdate
    end
    object EditDeleteAction: TTntAction
      Category = 'Edit'
      Caption = 'D&elete'
      Hint = 'Delete|Erases all selected contents of the focused window'
      ImageIndex = 34
      ShortCut = 46
      OnExecute = EditDeleteActionExecute
      OnUpdate = SelectionActionUpdate
    end
    object ViewZoomInAction: TTntAction
      Tag = 1
      Category = 'View'
      Caption = 'Zoom In'
      Hint = 
        'Zoom In (Ctrl+Plus, Shift+Scroll Up)|Zooms tiles in focused wind' +
        'ow in'
      ImageIndex = 28
      OnExecute = ToolsZoomActionExecute
      OnUpdate = ToolsZoomActionUpdate
    end
    object ViewZoomOutAction: TTntAction
      Tag = -1
      Category = 'View'
      Caption = 'Zoom Out'
      Hint = 
        'Zoom Out (Ctrl+Minus, Shift+Scroll Down)|Zooms tiles in focused ' +
        'window out'
      ImageIndex = 29
      OnExecute = ToolsZoomActionExecute
      OnUpdate = ToolsZoomActionUpdate
    end
    object PropAddAction: TTntAction
      Category = 'Properties'
      Caption = 'Add Property'
      Hint = 'Add Property|Adds new composite or tile attribute definition'
      ImageIndex = 48
    end
    object PropUpAction: TTntAction
      Category = 'Properties'
      Caption = 'Move Property Up'
      Hint = 'Move Property Up|Moves selected property up'
      ImageIndex = 1
    end
    object PropDownAction: TTntAction
      Category = 'Properties'
      Caption = 'Move Property Down'
      Hint = 'Move Property Down|Moves selected property down'
      ImageIndex = 2
    end
    object PropDeleteAction: TTntAction
      Category = 'Properties'
      Caption = 'Remove Property'
      Hint = 'Remove Property|Removes selected property'
      ImageIndex = 0
    end
    object ToolDrawRectangleAction: TTntAction
      Tag = 1
      Category = 'Line draw tools'
      AutoCheck = True
      Caption = 'Draw Rectangle'
      GroupIndex = 3
      Hint = 'Draw Rectangle'
      ImageIndex = 23
      OnExecute = ToolDrawLineActionExecute
      OnUpdate = SimpleActionUpdate
    end
    object ToolDrawPolygonAction: TTntAction
      Tag = 2
      Category = 'Line draw tools'
      AutoCheck = True
      Caption = 'Draw Polygon'
      GroupIndex = 3
      Hint = 'Draw Polygon'
      ImageIndex = 25
      OnExecute = ToolDrawLineActionExecute
      OnUpdate = SimpleActionUpdate
    end
    object ToolDrawEllipseAction: TTntAction
      Tag = 3
      Category = 'Line draw tools'
      AutoCheck = True
      Caption = 'Draw Ellipse'
      GroupIndex = 3
      Hint = 'Draw Ellipse'
      ImageIndex = 15
      OnExecute = ToolDrawLineActionExecute
      OnUpdate = SimpleActionUpdate
    end
    object BrushMediumAction: TTntAction
      Tag = 1
      Category = 'Brush sizes'
      AutoCheck = True
      Caption = 'Medium'
      GroupIndex = 4
      Hint = 'Medium'
      ImageIndex = 50
      OnExecute = SizeToolActionExecute
      OnUpdate = SimpleActionUpdate
    end
    object BrushLargeAction: TTntAction
      Tag = 2
      Category = 'Brush sizes'
      AutoCheck = True
      Caption = 'Large'
      GroupIndex = 4
      Hint = 'Large'
      ImageIndex = 51
      OnExecute = SizeToolActionExecute
      OnUpdate = SimpleActionUpdate
    end
    object BrushVeryLargeAction: TTntAction
      Tag = 3
      Category = 'Brush sizes'
      AutoCheck = True
      Caption = 'Very Large'
      GroupIndex = 4
      Hint = 'Very Large'
      ImageIndex = 52
      OnExecute = SizeToolActionExecute
      OnUpdate = SimpleActionUpdate
    end
    object ColorsExportAction: TTntAction
      Category = 'Color tables'
      Caption = 'Export Colors...'
      Hint = 'Export Colors|Saves all or selected color table colors to a file'
      ImageIndex = 38
      OnExecute = ColorsExportActionExecute
      OnUpdate = ColorsImportExportActionUpdate
    end
    object ColorsMakeGradientAction: TTntAction
      Category = 'Color tables'
      Caption = 'Make Gradient'
      Hint = 
        'Make Gradient|Shows dialog to choose color to form gradient and ' +
        'fill selected area with it'
      OnExecute = ColorsMakeGradientActionExecute
      OnUpdate = ColorsMakeGradientActionUpdate
    end
    object ColorReplaceAction: TTntAction
      Category = 'Color tables'
      Caption = 'Replace Color Under Cursor'
      Hint = 
        'Replace Color Under Cursor|Shows dialog to choose a color for on' +
        'e under cursor to be replaced with'
      OnExecute = ColorReplaceActionExecute
      OnUpdate = ColorReplaceActionUpdate
    end
    object TilesTileEditAction: TTntAction
      Category = 'Tile sets'
      Caption = 'Tile Edit'
      Hint = 'Tile Edit|Opens the tile under cursor in tile editor'
      OnExecute = TilesTileEditActionExecute
      OnUpdate = TilesTileEditActionUpdate
    end
    object LoadWithPluginAction: TTntAction
      Category = 'File'
      Caption = 'Load With Plugin...'
      Hint = 
        'Load With Plugin|Shows plugins dialog to use one to load a proje' +
        'ct in custom format'
      ImageIndex = 60
      SecondaryShortCuts.Strings = (
        'F3')
      OnExecute = PluginActionExecute
      OnUpdate = SimpleActionUpdate
    end
    object SaveWithPluginAction: TTntAction
      Tag = 1
      Category = 'File'
      Caption = 'Save With Plugin...'
      Hint = 
        'Save With Plugin|Shows plugins dialog to use one to save current' +
        ' project in custom format'
      ImageIndex = 61
      SecondaryShortCuts.Strings = (
        'F2')
      OnExecute = PluginActionExecute
      OnUpdate = SimpleActionUpdate
    end
    object SelectInTileSetAction: TTntAction
      Category = 'Tile sets'
      Caption = 'Select In Tile Set'
      Hint = 'Select In Tile Set|Selects the tile in tile set window'
      OnExecute = SelectInTileSetActionExecute
      OnUpdate = SelectInTileSetActionUpdate
    end
    object SelectFillAction: TTntAction
      Category = 'Select'
      Caption = 'Fill Selection'
      Hint = 'Fill Selection|Fills selected cells with something'
      ShortCut = 16392
      OnExecute = SelectFillActionExecute
      OnUpdate = SelectFillActionUpdate
    end
  end
  object OpenActDialog: TTntOpenDialog
    Filter = 
      'All Compatible Color Table Files (*.cct;*.act)|*.act;*.cct|Custo' +
      'mizable Color Tables (*.cct)|*.cct|Adobe Color Table (*.act)|*.a' +
      'ct|All Files (*.*)|*.*'
    Options = [ofHideReadOnly, ofFileMustExist, ofEnableSizing]
    Title = 'Load Colors'
    Left = 325
    Top = 189
  end
  object OpenProjectDialog: TTntOpenDialog
    DefaultExt = 'med'
    Filter = 'Map Editor of Dreams Project (*.med)|*.med'
    Options = [ofFileMustExist, ofEnableSizing]
    Title = 'Open Project'
    Left = 224
    Top = 101
  end
  object SaveProjectDialog: TTntSaveDialog
    DefaultExt = 'med'
    Filter = 'Map Editor of Dreams Project (*.med)|*.med'
    Options = [ofOverwritePrompt, ofHideReadOnly, ofEnableSizing]
    Title = 'Save Project'
    Left = 280
    Top = 253
  end
  object ImgList: TImageList
    Left = 427
    Top = 237
    Bitmap = {
      494C01013F004100040010001000FFFFFFFFFF00FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000000001000001002000000000000000
      0100000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000009B00000000000000000000000000
      0000000000000000000000000000000000009B00000000000000000000000000
      00000000000000000000000000009B0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000002CC020002CC02000068
      000002CC020002CC0200000000009B0000009B0000009B0000009B0000009B00
      0000000000000000000000000000000000000000000002CC020002CC02000068
      000002CC020002CC020000000000000000000000000000000000000000000000
      000000000000000000009B00000000000000000000009B0000000000000000FF
      0000000000009B0000009B0000009B0000009B0000009B0000009B0000009B00
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000996666000057000002CC020000D1
      000002CC02000E710E0048C2FF0048C2FF0048C2FF0048C2FF0048C2FF00B8B8
      B8009B320000000000000000000000000000000000000057000002CC020000D1
      000002CC02000E710E009B0000009B0000009B0000009B0000009B0000009B00
      00009B00000000000000000000000000000000000000000000000000000000FF
      000000000000000000000000000048C2FF0048C2FF0048C2FF0048C2FF00B8B8
      B8009B3200000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000D3000002CC0200004B
      000002CC020018E718000000000048C2FF0048C2FF0048C2FF0048C2FF0048C2
      FF00B8B8B8009B32000000000000000000000000000000D3000002CC0200004B
      000002CC020018E7180000000000FFFFFF00FFFFFF00FFFFFF009B000000B8B8
      B800F0CAA6009B00000000000000000000000000000000FF000000FF000000FF
      000000FF000000FF00000000000048C2FF0048C2FF0048C2FF0048C2FF0048C2
      FF00B8B8B8009B32000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000099666600004C000002CC020003DA
      030002CC02001E701E0048C2FF0048C2FF0048C2FF0048C2FF0048C2FF0048C2
      FF0048C2FF00B8B8B8009B3200000000000000000000004C000002CC020003DA
      030002CC02001E701E00FFFFFF009B00000099666600FFFFFF009B000000B8B8
      B800F0CAA6009B000000000000000000000000000000000000000000000000FF
      000000000000000000000000000048C2FF0048C2FF0048C2FF0048C2FF0048C2
      FF0048C2FF00B8B8B8009B320000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000002CC020002CC02000C54
      0C0002CC020002CC020000000000B8B8B800B8B8B800B8B8B800B8B8B800B8B8
      B800B8B8B800B8B8B800B8B8B8009B0000000000000002CC020002CC02000C54
      0C0002CC020002CC0200000000009B0000009B000000FFFFFF009B000000B8B8
      B800F0CAA6009B000000000000000000000099666600FFFFFF000000000000FF
      000000000000B8B8B800B8B8B800B8B8B800B8B8B800B8B8B800B8B8B800B8B8
      B800B8B8B800B8B8B800B8B8B8009B0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000099666600000000000000000000FF
      FF00000000000000000099666600996666009966660099666600996666009966
      660099666600996666009966660099666600000000000000000000000000B8B8
      B80000000000000000009966660099666600996666009966660099666600B8B8
      B800F0CAA6009B000000000000009B00000099666600FFFFFF00000000000000
      0000000000009966660099666600996666009966660099666600996666009966
      6600996666009966660099666600996666000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000099666600FFFFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF00B8B8B8009B00000000000000000000009B0000000000000099666600B8B8
      B800F0CAA600F0CAA600F0CAA600F0CAA600F0CAA600F0CAA600F0CAA600B8B8
      B800F0CAA6009B000000000000000000000099666600FFFFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF00B8B8B8009B00000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000099666600FFFFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF00B8B8B8009B0000000000000000000000000000000000000099666600B8B8
      B800F0CAA600996666009966660099666600996666009966660099666600B8B8
      B800F0CAA6009B000000000000000000000099666600FFFFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF00B8B8B8009B00000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000099666600FFFFFF0000FFFF0000FF
      FF0000FFFF0000FFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00B8B8B8009B0000000000000000000000000000000000000099666600B8B8
      B800F0CAA60099666600B8B8B800B8B8B800B8B8B800B8B8B80099666600B8B8
      B800FFFFFF009B000000000000000000000099666600FFFFFF0000FFFF0000FF
      FF0000FFFF0000FFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00B8B8B8009B00000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000099666600FFFFFF0000FFFF0000FF
      FF0000FFFF00FFFFFF0099666600996666009966660099666600996666009966
      660099666600000000000000000000000000000000000000000099666600B8B8
      B800F0CAA60099666600B8B8B800F0CAA600F0CAA600B8B8B80099666600B8B8
      B8009B0000009B000000000000000000000099666600FFFFFF0000FFFF0000FF
      FF0000FFFF00FFFFFF0099666600996666009966660099666600996666009966
      6600996666000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000099666600FFFFFF00FFFF
      FF00FFFFFF009966660000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000099666600B8B8
      B800F0CAA60099666600B8B8B800F0CAA600F0CAA600B8B8B80099666600B8B8
      B800F0CAA6009B00000000000000000000000000000099666600FFFFFF00FFFF
      FF00FFFFFF009966660000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000996666009966
      6600996666000000000000000000000000000000000000000000000000000000
      00009B0000009B0000009B000000000000000000000000000000000000009966
      6600996666009966660099666600996666009966660099666600996666009966
      6600996666000000000000000000000000000000000000000000996666009966
      6600996666000000000000000000000000000000000000000000000000000000
      00009B0000009B0000009B000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000009B0000009B00000000000000000000009B000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000009B000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000009B0000009B000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000009B00000000000000000000000000
      00009B000000000000009B000000000000009B00000000000000000000000000
      00000000000000000000000000009B0000000000000000000000000000000000
      00000000000000000000000000009B0000000000000000000000000000000000
      0000000000000000000000000000000000009B00000000000000000000000000
      00009B000000000000009B000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000009B0000009B0000009B00
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000009B0000009B0000009B00
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000840000008400000084000000840000008400000084000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000007D0000007D0000007D0000007D0000007D0000007D00000000
      0000000000000000000000000000000000000000000000000000000000000000
      8400000084000000840000008400000084000000840000008400000084000000
      8400000084000000000000000000000000000000000000000000000000000000
      0000FFFF0000FFFF000000000000316594003165940000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000084000000C6000000C6000000C6000000C6000000C6000000C6000000
      8400000000000000000000000000000000000000000000000000000000000000
      0000007D000000DF000000DF000000DF000000DF000000DF000000DF0000007D
      0000000000000000000000000000000000000000000000000000000000000000
      84000000C6000000C6000000C6000000C6000000C6000000C6000000C6000000
      C600000084000000000000000000000000000000000000000000000000000000
      0000FFFF0000FFFF000000000000316594003165940000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      84006B6DFF000000FF000000FF000000FF000000FF000000FF000000FF000000
      C60000008400000000000000000000000000000000000000000000000000007D
      000052FFAD0000FF080000FF080000FF080000FF080000FF080000FF080000DF
      0000007D00000000000000000000000000000000000000000000000000000000
      84006B6DFF000000FF000000FF000000FF000000FF000000FF000000FF000000
      C600000084000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      84006B6DFF000000FF000000FF000000FF000000FF000000FF000000FF000000
      C60000008400000000000000000000000000000000000000000000000000007D
      000052FFAD0000FF080000FF080000FF080000FF080000FF080000FF080000DF
      0000007D00000000000000000000000000000000000000000000000000000000
      84006B6DFF000000FF000000FF000000FF000000FF000000FF000000FF000000
      C600000084000000000000000000000000000000000000000000000000000000
      000000FFFF0000FFFF000000000000FF000000FF000000000000EF8AAD00EF8A
      AD00000000000000000000000000000000000000000000000000000000000000
      84006B6DFF000000FF000000FF000000FF000000FF000000FF000000FF000000
      C60000008400000000000000000000000000000000000000000000000000007D
      000052FFAD0000FF080000FF080000FF080000FF080000FF080000FF080000DF
      0000007D00000000000000000000000000000000000000000000000000000000
      84006B6DFF000000FF000000FF000000FF000000FF000000FF000000FF000000
      C600000084000000000000000000000000000000000000000000000000000000
      000000FFFF0000FFFF000000000000FF000000FF000000000000EF8AAD00EF8A
      AD00000000000000000000000000000000000000000000000000000000000000
      84006B6DFF000000FF000000FF000000FF000000FF000000FF000000FF000000
      C60000008400000000000000000000000000000000000000000000000000007D
      000052FFAD0000FF080000FF080000FF080000FF080000FF080000FF080000DF
      0000007D00000000000000000000000000000000000000000000000000000000
      84006B6DFF000000FF000000FF000000FF000000FF000000FF000000FF000000
      C600000084000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      84006B6DFF000000FF000000FF000000FF000000FF000000FF000000FF000000
      C60000008400000000000000000000000000000000000000000000000000007D
      000052FFAD0000FF080000FF080000FF080000FF080000FF080000FF080000DF
      0000007D00000000000000000000000000000000000000000000000000000000
      84006B6DFF000000FF000000FF000000FF000000FF000000FF000000FF000000
      C600000084000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF0000000000FF000000FF000000000000000000FF000000
      FF00000000000000000000000000000000000000000000000000000000000000
      84006B6DFF000000FF000000FF000000FF000000FF000000FF000000FF000000
      C60000008400000000000000000000000000000000000000000000000000007D
      000052FFAD0000FF080000FF080000FF080000FF080000FF080000FF080000DF
      0000007D00000000000000000000000000000000000000000000000000000000
      84006B6DFF000000FF000000FF000000FF000000FF000000FF000000FF000000
      C600000084000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF0000000000FF000000FF000000000000000000FF000000
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000084006B6DFF006B6DFF006B6DFF006B6DFF006B6DFF006B6DFF000000
      8400000000000000000000000000000000000000000000000000000000000000
      0000007D000052FFAD0052FFAD0052FFAD0052FFAD0052FFAD0052FFAD00007D
      0000000000000000000000000000000000000000000000000000000000000000
      84006B6DFF006B6DFF006B6DFF006B6DFF006B6DFF006B6DFF006B6DFF000000
      C600000084000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000840000008400000084000000840000008400000084000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000007D0000007D0000007D0000007D0000007D0000007D00000000
      0000000000000000000000000000000000000000000000000000000000000000
      8400000084000000840000008400000084000000840000008400000084000000
      8400000084000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000DEDBDE00DEDB
      DE00DEDBDE000000000000000000000000000000000000000000DEDBDE00DEDB
      DE00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000DEDBDE00FFFFFF00FFFF
      FF00FFFFFF00DEDBDE00DEDBDE00DEDBDE00DEDBDE00DEDBDE00FFFFFF00FFFF
      FF00DEDBDE00DEDBDE00DEDBDE0000000000000000005251520094CFDE007BAE
      BD0073A6B500000000001010100031303100393C3900313431009CD3E7007BB6
      C600000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000DEDBDE00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00DEDBDE000000000000000000393839008CC3
      D60073A6B50073AABD0084B6C6009CD3E700B5DBE700BDDFEF00B5DFEF00A5D7
      E70084BACE00638E9C007BAEBD00101010000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000DEDBDE00FFFF
      FF00FFFFFF0021414A00FFFFFF00FFFFFF006B8E9400849AA5007B969C00FFFF
      FF00FFFFFF00FFFFFF00DEDBDE000000000000000000000000001014100094CB
      DE0084B6C60029515A0073A2AD0094CFDE006B8A9400849AA500849EA500B5DB
      E70094CFDE00638E9C0000000000000000000000000000000000000000000000
      000031B6940031A28400216D8C00299AC600298ABD00425542006B6D39006B6D
      4200000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000DEDBDE00FFFFFF00FFFF
      FF0042697300FFFFFF00213C4200FFFFFF00FFFFFF00FFFFFF00FFFFFF005271
      7B00FFFFFF00DEDBDE000000000000000000000000000000000094CFDE0094CF
      DE004269730084B6C60021414A0073A6B50094CFDE009CD3E700A5D3E7004A71
      7B0084BACE000000000000000000000000000000000000000000000000000000
      000031B69C0029A28400216D8C002992CE00298EBD00425542006B6D4200636D
      4200000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000DEDBDE00FFFFFF00FFFF
      FF005A7D8400FFFFFF002949520021384200FFFFFF00294D5200FFFFFF00FFFF
      FF00FFFFFF00DEDBDE00000000000000000000000000000400009CCFDE009CD3
      E70063828C0094CFDE0021414A0021414A0073A6B500315D6B009CCFDE00A5D3
      E7008CCBDE000000000000000000000000000000000000000000000000000000
      000031866B0031796300215D630031798C0031718400313C2900524D29004A4D
      3100000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000DEDBDE00FFFFFF005A7D
      8C00FFFFFF00FFFFFF00FFFFFF0029495200294D52003155630039657300FFFF
      FF00FFFFFF00DEDBDE00000000000000000000000000181C18009CD3E7005A79
      8400ADDBE7009CD3E70073A2AD0021414A0021414A0031515A004A717B00ADDB
      E700A5D7E7000000000000000000000000000000000000000000000000000000
      0000A5B294008C9E84007B867B00B5BAB500A5B2AD0063513100845931008461
      3100000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000DEDBDE00FFFFFF004265
      7300FFFFFF004A717B00FFFFFF00FFFFFF00FFFFFF00FFFFFF004A6D7B004A6D
      7B00FFFFFF00DEDBDE000000000000000000000000000000000084C3D600395D
      6B00A5D7E700527984008CC7DE008CC3D6008CC7DE008CC3D60039616B005A79
      8400A5D3E7000000000000000000000000000000000000000000000000000000
      0000ADBA9C0094A68C00848E8400BDC7BD00B5BEB5006B5539008C6531008C65
      3100000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000DEDBDE00FFFFFF00FFFF
      FF0042697300FFFFFF00FFFFFF00FFFFFF004A6D7B00FFFFFF00FFFFFF004265
      7300FFFFFF00DEDBDE00000000000000000000000000000000006B9EAD007BAA
      BD00426973009CD3E70094CFDE009CD3E70063828C0094CFDE008CC7D600315D
      6B0094CFDE000000000000000000000000000000000000000000000000000000
      000073867B0063756B00525963007B828C00737984004A3C2900634D2900634D
      2900000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DEDBDE00FFFFFF00FFFFFF00FFFF
      FF0052757B0031596300FFFFFF003155630039616B0039616B00FFFFFF00FFFF
      FF00FFFFFF00DEDBDE0000000000000000000808080084BECE008CCBDE0094CF
      DE0042697300396573007BAEBD003159630042697300426973008CCBDE0073AA
      BD0073A2AD000000000000000000000000000000000000000000000000000000
      0000429AE7003986CE002934AD00393CEF003938E70042595200638A4A005A86
      5200000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000DEDBDE00FFFFFF00FFFF
      FF00FFFFFF005A7984003159630031596300FFFFFF0052798400396573002949
      5200FFFFFF00DEDBDE0000000000000000000000000000000000A5D3E700A5D7
      E700A5D3E7004269730029515A00294D5A0084BACE004265730039616B00294D
      5200638A94000000000000000000000000000000000000000000000000000000
      00004AAAFF004296DE00313CC600423CFF003938FF0042615A006B9A52006396
      5A00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000DEDBDE00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00DEDBDE00000000000000000000000000181C1800ADDB
      E700B5DFE700B5DBE700A5D7E7009CD3DE0094CFDE00A5D3E700A5D7E70094CF
      DE007BAEBD006B96A50000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000DEDB
      DE00DEDBDE00DEDBDE00DEDBDE00DEDBDE00DEDBDE00DEDBDE00DEDBDE00DEDB
      DE00FFFFFF00FFFFFF00DEDBDE00000000000000000000000000000000003938
      3900525552005A5D5A004A494A00292829001010100021242100292C2900181C
      180094CFDE008CCBDE0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000DEDBDE00DEDBDE00DEDBDE00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000181C18003130310000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000002400000028000000280000002C0000083408000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000040000104110000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000D7000000EB000000FF000010101000184518000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000DF000000F3000000FF0000080C0800103C10000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000002400000024
      0000002800000000000000EB000000F7000000FF000008080800003400000030
      0000002C0000002C000008380800000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000F3000000FB000000FF000000000000000000000000
      0000000000000808080018491800000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000D7000000E7
      000000F3000000F7000000FB000000FF000000FF000000FF000000FF000000FF
      000008FF08001818180029512900000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000E3000000F3
      000000FF000000FF000000FF000000FF000000FF000000FF000000FF000008FF
      080010FF10002124210031593100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000EF000000FF
      000000FF000000FF000000FB000000FF000000FF000008FF080010FF100018FF
      180021FF2100292C2900315D3100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000F7000000FF000008FF080010141000212021002124
      2100292829003130310000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000EF000000FB000008FF0800181C1800295129000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000EB000000FF000010FF100021242100315931000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000F7000008FF080018FF180029282900315931000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000001014100021202100292C2900000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000C6284A00C6284A00C6284A00C628
      4A00C6284A00C6284A00C6284A00C6284A00C6284A00C6284A00C6284A00C628
      4A00C6284A00C6284A00C6284A00C6284A00C6284A00C6284A00C6284A000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000C6284A00C6284A00C6284A00C6284A000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000C6284A00C6284A00C6284A00C628
      4A00C6284A00C6284A00C6284A00C6284A00C6284A00C6284A00C6284A00C628
      4A00C6284A00C6284A00C6284A00C6284A00C6284A00FF5D8C00FF5D8C00FF5D
      8C00FF5D8C00FF5D8C00FF5D8C00FF5D8C00FF5D8C00FF5D8C00FF5D8C00FF5D
      8C00FF5D8C00FF5D8C00FF5D8C00C6284A00C6284A00FF5D8C00FF5D8C00FF5D
      8C0000000000FFFFFF0084828400FFFFFF0084828400FFFFFF00FFFFFF000000
      0000FF5D8C00FF5D8C00FF5D8C00C6284A0000000000000000000000000000FF
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000C6284A00FF5D8C00FF5D8C00FF5D
      8C00FF5D8C00FF5D8C00FF5D8C00FF5D8C00FF5D8C00FF5D8C00FF5D8C00FF5D
      8C00FF5D8C00FF5D8C00FF5D8C00C6284A00C6284A00FF5D8C006B3C52000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000006B3C5200FF5D8C00C6284A00C6284A00FF5D8C00FF9ACE00FF9A
      CE0000000000FFFFFF0084828400FFFFFF0084828400FFFFFF00FFFFFF000000
      0000FF9ACE00FF9ACE00FF5D8C00C6284A0000000000000000000000000000FF
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000C6284A00FF5D8C00FF9ACE00FF9A
      CE00FF9ACE00FF9ACE00FF9ACE00FF9ACE00FF9ACE00FF9ACE00FF9ACE00FF9A
      CE00FF9ACE00FF9ACE00FF5D8C00C6284A00C6284A00FF5D8C00000000003996
      DE003996D6003996D6003996D600399AD600399AD6003996D600398ED6004296
      DE003996D60000000000FF5D8C00C6284A00C6284A00FF5D8C00FF9ACE00FFAA
      DE0000000000FFFFFF0084828400FFFFFF0084828400FFFFFF00FFFFFF000000
      0000FFAADE00FF9ACE00FF5D8C00C6284A000000000000FF000000FF000000FF
      000000FF000000FF000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000C6284A00FF5D8C00FF9ACE000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FF9ACE00FF5D8C00C6284A00C6284A00FF5D8C0000000000398A
      D600398ED600398AD600398AD6004292D600398ED600398AD600398AD600428E
      D600398ED60000000000FF5D8C00C6284A00C6284A00FF5D8C00FF9ACE00FFAA
      DE0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000FFAADE00FF9ACE00FF5D8C00C6284A0000000000000000000000000000FF
      0000000000000000000000000000299AC600298ABD00425542006B6D39006B6D
      420000000000000000000000000000000000C6284A00FF5D8C00FF9ACE000000
      000031B6940031A28400216D8C00299AC600298ABD00425542006B6D39006B6D
      420000000000FF9ACE00FF5D8C00C6284A00C6284A00FF5D8C00000000003986
      CE00398ECE003986CE003986CE00428ACE003986CE003986CE003986CE003986
      CE003986CE0000000000FF5D8C00C6284A00C6284A00FF5D8C00FF9ACE00FFAA
      DE00000000000000000000000000000000000000000000000000000000000000
      0000FFAADE00FF9ACE00FF5D8C00C6284A0000000000000000000000000000FF
      00000000000029A28400216D8C002992CE00298EBD00425542006B6D4200636D
      420000000000000000000000000000000000C6284A00FF5D8C00FF9ACE000000
      000031B69C0029A28400216D8C002992CE00298EBD00425542006B6D4200636D
      420000000000FF9ACE00FF5D8C00C6284A00C6284A00FF5D8C00000000003992
      D6004296D6003992D6003996D6003996D600398ED600428ED600398ECE00398E
      D6003992D60000000000FF5D8C00C6284A00C6284A00FF5D8C00FF9ACE00FFAA
      DE00000000000000000000000000000000000000000000000000000000000000
      0000FFAADE00FF9ACE00FF5D8C00C6284A000000000000000000000000000000
      00000000000031796300215D630031798C0031718400313C2900524D29004A4D
      310000000000000000000000000000000000C6284A00FF5D8C00FF9ACE000000
      000031866B0031796300215D630031798C0031718400313C2900524D29004A4D
      310000000000FF9ACE00FF5D8C00C6284A00C6284A00FF5D8C0000000000398A
      D600398AD600398AD6003992D600398ED600428ED600428ED600398ACE00428E
      D600398ED60000000000FF5D8C00C6284A00C6284A00FF5D8C00FF9ACE00FFAA
      DE000000000000BEBD0000BEBD0000BEBD0000BEBD0000BEBD0000BEBD000000
      0000FFAADE00FF9ACE00FF5D8C00C6284A000000000000000000000000000000
      0000A5B294008C9E84007B867B00B5BAB500A5B2AD0063513100845931008461
      310000000000000000000000000000000000C6284A00FF5D8C00FF9ACE000000
      0000A5B294008C9E84007B867B00B5BAB500A5B2AD0063513100845931008461
      310000000000FF9ACE00FF5D8C00C6284A00C6284A00FF5D8C0000000000398A
      CE00398AD600398ED600398ED600398AD600428ED600428ACE00398ACE00398A
      CE00398ACE0000000000FF5D8C00C6284A00C6284A00FF5D8C00FF9ACE00FFAA
      DE00FFC3F7000000000000BEBD0000BEBD0000BEBD0000BEBD0000000000FFC3
      F700FFAADE00FF9ACE00FF5D8C00C6284A000000000000000000000000000000
      0000ADBA9C0094A68C00848E8400BDC7BD00B5BEB5006B5539008C6531008C65
      310000000000000000000000000000000000C6284A00FF5D8C00FF9ACE000000
      0000ADBA9C0094A68C00848E8400BDC7BD00B5BEB5006B5539008C6531008C65
      310000000000FF9ACE00FF5D8C00C6284A00C6284A00FF5D8C0000000000398E
      D600398ED600428ED600398ED600398AD600428ED6004292D600398ED600398E
      D6003992D60000000000FF5D8C00C6284A00C6284A00FF5D8C00FF9ACE00FFAA
      DE00FFC3F700FFCBFF000000000000BEBD0000BEBD0000000000FFCBFF00FFC3
      F700FFAADE00FF9ACE00FF5D8C00C6284A000000000000000000000000000000
      000073867B0063756B00525963007B828C00737984004A3C2900634D2900634D
      290000000000000000000000000000000000C6284A00FF5D8C00FF9ACE000000
      000073867B0063756B00525963007B828C00737984004A3C2900634D2900634D
      290000000000FF9ACE00FF5D8C00C6284A00C6284A00FF5D8C00000000003986
      CE003986CE004286CE003986CE003982CE003982CE003986CE004286CE003982
      CE003986CE0000000000FF5D8C00C6284A00C6284A00FF5D8C00FF9ACE00FFAA
      DE00FFC3F700FFCBFF000000000000BEBD0000BEBD0000000000FFCBFF00FFC3
      F700FFAADE00FF9ACE00FF5D8C00C6284A000000000000000000000000000000
      0000429AE7003986CE002934AD00393CEF003938E70042595200638A4A005A86
      520000000000000000000000000000000000C6284A00FF5D8C00FF9ACE000000
      0000429AE7003986CE002934AD00393CEF003938E70042595200638A4A005A86
      520000000000FF9ACE00FF5D8C00C6284A00C6284A00FF5D8C00000000003992
      D6003996D6003992D6003992D600398ED6003992D600398ED600398AD600398E
      D600398ED60000000000FF5D8C00C6284A00C6284A00FF5D8C00FF9ACE00FFAA
      DE00FFC3F700FFC3F7000000000000BEBD0000BEBD0000000000FFC3F700FFC3
      F700FFAADE00FF9ACE00FF5D8C00C6284A000000000000000000000000000000
      00004AAAFF004296DE00313CC600423CFF003938FF0042615A006B9A52006396
      5A0000000000000000000000000000000000C6284A00FF5D8C00FF9ACE000000
      00004AAAFF004296DE00313CC600423CFF003938FF0042615A006B9A52006396
      5A0000000000FF9ACE00FF5D8C00C6284A00C6284A00FF5D8C00000000003996
      D6003996D6003992D6003992D6003992D6003996D6003996D6003996D6004296
      D6003992D60000000000FF5D8C00C6284A00C6284A00FF5D8C00FF9ACE00FFAA
      DE00FFAADE00FFAADE000000000000BEBD0000BEBD0000000000FFAADE00FFAA
      DE00FFAADE00FF9ACE00FF5D8C00C6284A000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000C6284A00FF5D8C00FF9ACE000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FF9ACE00FF5D8C00C6284A00C6284A00FF5D8C006B3C52000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000006B3C5200FF5D8C00C6284A00C6284A00FF5D8C00FF9ACE00FF9A
      CE00FF9ACE00FF9ACE000000000000BEBD0000BEBD0000000000FF9ACE00FF9A
      CE00FF9ACE00FF9ACE00FF5D8C00C6284A000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000C6284A00FF5D8C00FF9ACE00FF9A
      CE00FF9ACE00FF9ACE00FF9ACE00FF9ACE00FF9ACE00FF9ACE00FF9ACE00FF9A
      CE00FF9ACE00FF9ACE00FF5D8C00C6284A00C6284A00FF5D8C00FF5D8C00FF5D
      8C00FF5D8C00FF5D8C00FF5D8C00FF5D8C00FF5D8C00FF5D8C00FF5D8C00FF5D
      8C00FF5D8C00FF5D8C00FF5D8C00C6284A00C6284A00FF5D8C00FF5D8C00FF5D
      8C00FF5D8C00FF5D8C000000000000BEBD0000BEBD0000000000FF5D8C00FF5D
      8C00FF5D8C00FF5D8C00FF5D8C00C6284A000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000C6284A00FF5D8C00FF5D8C00FF5D
      8C00FF5D8C00FF5D8C00FF5D8C00FF5D8C00FF5D8C00FF5D8C00FF5D8C00FF5D
      8C00FF5D8C00FF5D8C00FF5D8C00C6284A00C6284A00C6284A00C6284A00C628
      4A00C6284A00C6284A00C6284A00C6284A00C6284A00C6284A00C6284A00C628
      4A00C6284A00C6284A00C6284A00C6284A00C6284A00C6284A00C6284A00C628
      4A00C6284A00C6284A00000000000000000000000000C6284A00C6284A00C628
      4A00C6284A00C6284A00C6284A00C6284A000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000C6284A00C6284A00C6284A00C628
      4A00C6284A00C6284A00C6284A00C6284A00C6284A00C6284A00C6284A00C628
      4A00C6284A00C6284A00C6284A00C6284A000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000C6284A00C6284A00C6284A00C628
      4A00C6284A00C6284A00C6284A00C6284A00C6284A00C6284A00C6284A00C628
      4A00C6284A00C6284A00C6284A00C6284A00C6284A00C6284A00C6284A00C628
      4A00C6284A00C6284A00C6284A00C6284A00C6284A00C6284A00C6284A00C628
      4A00C6284A00C6284A00C6284A00C6284A000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000063616300E7E3E7003938
      390000000000000000000000000000000000C6284A00FF5D8C00000000000000
      000000000000FF5D8C00FF5D8C00FF5D8C00FF5D8C00FF5D8C00000000000000
      0000FF5D8C00FF5D8C00FF5D8C00C6284A00C6284A00FF5D8C00FF5D8C00FF5D
      8C00FF5D8C00FF5D8C00FF5D8C00FF5D8C00FF5D8C00FF5D8C00FF5D8C00FF5D
      8C00FF5D8C00FF5D8C00FF5D8C00C6284A000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000009C9A9C00FFFFFF005A5D
      5A0000000000000000000000000000000000C6284A00000000008CC3D60094CF
      DE008CC3D6000000000000000000000000000000000000000000638E9C0094CF
      DE00313431004A4D4A0042414200C6284A00C6284A00FF5D8C00FF9ACE00FF9A
      CE00FF9ACE00FF9ACE00FF9ACE00FF9ACE00FF9ACE00FF9ACE00FF9ACE00FF9A
      CE00FF9ACE00FF9ACE00FF5D8C00C6284A000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000042454200DEDBDE00424542000000
      000000000000000000000000000000000000C6284A00FF5D8C00000000008CC7
      D6008CC3D6008CCBDE00A5D3E70094CFDE0073A6B5006B9EAD0073AABD0094CF
      DE00ADDBE700BDE3EF00B5DFEF0021202100C6284A00FF5D8C00FF9ACE000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FF9ACE00FF5D8C00C6284A000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000424142000000000000000000000000009C9A9C00FFFFFF006B696B000000
      000000000000000000000000000000000000C6284A00FF5D8C00000000007BAE
      BD008CC3D60042697300ADD7E7009CD3DE0031515A0029515A0029515A0084B6
      C600A5D7E700C6E3EF0063656300C6284A00C6284A00FF5D8C00FF9ACE000000
      0000FFFF0000FFFF000000000000316594003165940000000000FF00FF00FF00
      FF0000000000FF9ACE00FF5D8C00C6284A000000000000000000000000008400
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000042414200424142000000000052555200EFEBEF004A4D4A00000000000000
      000000000000000000000000000000000000C6284A000000000063929C0084B6
      C600315D6B0094CFDE0063828C009CCFDE007BB2C6006B9AA5005A8694002141
      4A009CCFDE006B696B00FF5D8C00C6284A00C6284A00FF5D8C00FF9ACE000000
      0000FFFF0000FFFF000000000000316594003165940000000000FF00FF00FF00
      FF0000000000FF9ACE00FF5D8C00C6284A000000000000000000000000008400
      0000000000000000000000000000000000000000000094000000840000008400
      0000840000009400000000000000000000000000000000000000000000000000
      0000424142004241420042414200ADAAAD00FFFFFF00737173008C8A8C009492
      940094929400000000000000000000000000C6284A00000000008CC3D60094CF
      DE00315963008CC7D60052717B0039616B007BAEBD0029495200638A94006B96
      A5009CD3E70063656300FF5D8C00C6284A00C6284A00FF5D8C00FF9ACE000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FF9ACE00FF5D8C00C6284A000000000000000000940000000000
      0000000000000000000000000000000000000000000000000000940000008400
      0000940000008400000000000000000000000000000000000000000000000000
      0000424142004241420063616300FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00ADAAAD00000000000000000000000000C6284A00000000009CD3E7005A79
      840073A6B50073A2AD0094CFDE003159630029515A0031515A003155630084BE
      CE00A5D7E70063656300FF5D8C00C6284A00C6284A00FF5D8C00FF9ACE000000
      000000FFFF0000FFFF000000000000FF000000FF000000000000EF8AAD00EF8A
      AD0000000000FF9ACE00FF5D8C00C6284A000000000000000000840000000000
      0000000000000000000000000000000000000000000000000000000000009400
      0000840000008400000000000000000000000000000000000000000000000000
      00004241420042414200BDBABD00FFFFFF00FFFFFF00FFFFFF00FFFFFF00BDBA
      BD0000000000000000000000000000000000C6284A00000000009CD3E7004A71
      7B00638A940029454A0094CFDE0094CFDE008CC7DE008CCBDE00426573003965
      7300A5D3E70042414200FF5D8C00C6284A00C6284A00FF5D8C00FF9ACE000000
      000000FFFF0000FFFF000000000000FF000000FF000000000000EF8AAD00EF8A
      AD0000000000FF9ACE00FF5D8C00C6284A000000000000000000940000000000
      0000000000000000000000000000000000000000000000000000940000000000
      0000940000008400000000000000000000000000000000000000000000000000
      00004241420052515200FFFFFF00FFFFFF00FFFFFF00FFFFFF00BDBEBD000000
      000000000000000000000000000000000000C6284A000000000094CFDE008CCB
      DE002138420063929C00A5D7E700A5D7E7004265730094CFDE0094CFDE004A6D
      7B009CD3E700080C0800FF5D8C00C6284A00C6284A00FF5D8C00FF9ACE000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FF9ACE00FF5D8C00C6284A000000000000000000000000008400
      0000000000000000000000000000000000008400000094000000000000000000
      0000000000009400000000000000000000000000000000000000000000000000
      0000424142009C9E9C00FFFFFF00FFFFFF00FFFFFF00BDBABD00000000000000
      00000000000000000000000000000000000000000000A5D7E7009CD3E7008CC7
      D600213C4200294952009CD3E7004A717B00315D6B00315963008CC3D60094CF
      DE0094CFDE0000000000FF5D8C00C6284A00C6284A00FF5D8C00FF9ACE000000
      0000FFFFFF00FFFFFF0000000000FF000000FF000000000000000000FF000000
      FF0000000000FF9ACE00FF5D8C00C6284A000000000000000000000000000000
      0000940000008400000094000000840000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000042414200EFEFEF00FFFFFF00FFFFFF00B5B6B50000000000000000000000
      000000000000000000000000000000000000C6284A0063616300A5D7E7007BB6
      C600638E9C00294D5A0042697300426973008CCBDE0031555A00294D5A003159
      630084BACE0000000000FF5D8C00C6284A00C6284A00FF5D8C00FF9ACE000000
      0000FFFFFF00FFFFFF0000000000FF000000FF000000000000000000FF000000
      FF0000000000FF9ACE00FF5D8C00C6284A000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00008C8E8C00FFFFFF00FFFFFF00ADAEAD000000000000000000000000000000
      000000000000000000000000000000000000C6284A00FF5D8C00181C18007BAA
      BD007BAABD008CC7D6009CD3E700A5D7E7009CD3E70084BECE0073A6B5007BB6
      C60073AABD00739EAD0029282900C6284A00C6284A00FF5D8C00FF9ACE000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FF9ACE00FF5D8C00C6284A000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000D6D3D600FFFFFF0094929400000000000000000000000000000000000000
      000000000000000000000000000000000000C6284A00FF5D8C00FF5D8C000000
      0000000000000000000010101000181C1800181C180000000000000000000000
      00007BAEBD0084BECE004A4D4A00C6284A00C6284A00FF5D8C00FF9ACE00FF9A
      CE00FF9ACE00FF9ACE00FF9ACE00FF9ACE00FF9ACE00FF9ACE00FF9ACE00FF9A
      CE00FF9ACE00FF9ACE00FF5D8C00C6284A000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF006361630000000000000000000000000000000000000000000000
      000000000000000000000000000000000000C6284A00FF5D8C00FF5D8C00FF5D
      8C00FF5D8C00FF5D8C00FF5D8C00FF5D8C00FF5D8C00FF5D8C00FF5D8C00FF5D
      8C00000000002124210063656300C6284A00C6284A00FF5D8C00FF5D8C00FF5D
      8C00FF5D8C00FF5D8C00FF5D8C00FF5D8C00FF5D8C00FF5D8C00FF5D8C00FF5D
      8C00FF5D8C00FF5D8C00FF5D8C00C6284A000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000212421000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000C6284A00C6284A00C6284A00C628
      4A00C6284A00C6284A00C6284A00C6284A00C6284A00C6284A00C6284A00C628
      4A00C6284A00C6284A00C6284A00C6284A00C6284A00C6284A00C6284A00C628
      4A00C6284A00C6284A00C6284A00C6284A00C6284A00C6284A00C6284A00C628
      4A00C6284A00C6284A00C6284A00C6284A000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000009C00000000000000000000000000
      0000000000000000000000000000000000009C00000000000000000000000000
      00000000000000000000000000009C0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000009C0000009C0000009C00
      00009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C00
      000000000000000000000000000000000000000000009C0000009C0000009C00
      00009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C00
      00009C0000009C0000000000000000000000000000009C000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000009C000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000009C6563009C656300BDBABD004AC3
      FF004AC3FF004AC3FF004AC3FF004AC3FF004AC3FF004AC3FF004AC3FF00BDBA
      BD009C3000000000000000000000000000009C656300BDBABD009C656300BDBA
      BD00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF009C00
      0000BDBABD00F7CBA5009C000000000000000000000000000000000000009C00
      00009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C00
      00009C0000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000009C656300FFFFFF009C656300BDBA
      BD004AC3FF004AC3FF004AC3FF004AC3FF004AC3FF004AC3FF004AC3FF004AC3
      FF00BDBABD009C30000000000000000000009C656300BDBABD009C656300BDBA
      BD00FFFFFF00FFFFFF00FFFFFF00FFFFFF009C0000009C656300FFFFFF009C00
      0000BDBABD00F7CBA5009C0000000000000000000000000000009C656300BDBA
      BD009C656300BDBABD00FFFFFF00FFFFFF00FFFFFF00FFFFFF009C000000BDBA
      BD00F7CBA5009C00000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000009C656300FFFFFF0000FFFF009C65
      6300BDBABD004AC3FF004AC3FF004AC3FF004AC3FF004AC3FF004AC3FF004AC3
      FF004AC3FF00BDBABD009C300000000000009C656300BDBABD009C656300BDBA
      BD00FFFFFF00FFFFFF00FFFFFF00FFFFFF009C0000009C656300FFFFFF009C00
      0000BDBABD00F7CBA5009C0000000000000000000000000000009C656300BDBA
      BD009C656300BDBABD00FFFFFF009C0000009C656300FFFFFF009C000000BDBA
      BD00F7CBA5009C00000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000009C656300FFFFFF0000FFFF0000FF
      FF009C656300BDBABD00BDBABD00BDBABD00BDBABD00BDBABD00BDBABD00BDBA
      BD00BDBABD00BDBABD00BDBABD009C0000009C656300BDBABD009C656300BDBA
      BD00FFFFFF00FFFFFF00FFFFFF00FFFFFF009C0000009C000000FFFFFF009C00
      0000BDBABD00F7CBA5009C0000000000000000000000000000009C656300BDBA
      BD009C656300BDBABD00FFFFFF009C0000009C000000FFFFFF009C000000BDBA
      BD00F7CBA5009C00000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000940000000000000000000000000000009C656300FFFFFF0000FFFF0000FF
      FF0000FFFF009C6563009C6563009C6563009C6563009C6563009C6563009C65
      63009C6563009C6563009C6563009C6563009C656300BDBABD009C6563009C65
      63009C6563009C6563009C6563009C6563009C6563009C6563009C6563009C65
      6300BDBABD00F7CBA5009C0000000000000000000000000000009C656300BDBA
      BD009C6563009C6563009C6563009C6563009C6563009C6563009C656300BDBA
      BD00F7CBA5009C000000000000009C0000000000000000000000840000008400
      0000840000009400000084000000000000000000000000000000000000000000
      0000840000000000000000000000000000009C656300FFFFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF00BDBABD009C00000000000000000000009C656300BDBABD00F7CBA500F7CB
      A500F7CBA500F7CBA500F7CBA500F7CBA500F7CBA500F7CBA500F7CBA500F7CB
      A500BDBABD00F7CBA5009C000000000000009C000000000000009C656300BDBA
      BD00F7CBA500F7CBA500F7CBA500F7CBA500F7CBA500F7CBA500F7CBA500BDBA
      BD00F7CBA5009C00000000000000000000000000000000000000940000008400
      0000940000008400000000000000000000000000000000000000000000000000
      0000000000008400000000000000000000009C656300FFFFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF00BDBABD009C00000000000000000000009C656300BDBABD00F7CBA5009C65
      63009C6563009C6563009C6563009C6563009C6563009C6563009C6563009C65
      6300BDBABD00F7CBA5009C0000000000000000000000000000009C656300BDBA
      BD00F7CBA5009C6563009C6563009C6563009C6563009C6563009C656300BDBA
      BD00F7CBA5009C00000000000000000000000000000000000000840000009400
      0000840000000000000000000000000000000000000000000000000000000000
      0000000000008400000000000000000000009C656300FFFFFF0000FFFF0000FF
      FF0000FFFF0000FFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00BDBABD009C00000000000000000000009C656300BDBABD00F7CBA5009C65
      6300BDBABD00BDBABD00BDBABD00BDBABD00BDBABD00BDBABD00BDBABD009C65
      6300BDBABD00F7CBA5009C0000000000000000000000000000009C656300BDBA
      BD00F7CBA5009C656300BDBABD00BDBABD00BDBABD00BDBABD009C656300BDBA
      BD00FFFFFF009C00000000000000000000000000000000000000940000008400
      0000000000008400000000000000000000000000000000000000000000000000
      0000000000008400000000000000000000009C656300FFFFFF0000FFFF0000FF
      FF0000FFFF00FFFFFF009C6563009C6563009C6563009C6563009C6563009C65
      63009C6563000000000000000000000000009C656300BDBABD00F7CBA5009C65
      6300BDBABD00F7CBA500F7CBA500F7CBA500F7CBA500F7CBA500BDBABD009C65
      6300BDBABD00FFFFFF009C0000000000000000000000000000009C656300BDBA
      BD00F7CBA5009C656300BDBABD00F7CBA500F7CBA500BDBABD009C656300BDBA
      BD009C0000009C00000000000000000000000000000000000000840000000000
      0000000000000000000084000000840000000000000000000000000000000000
      000084000000000000000000000000000000000000009C656300FFFFFF00FFFF
      FF00FFFFFF009C65630000000000000000000000000000000000000000000000
      0000000000000000000000000000000000009C656300BDBABD00F7CBA5009C65
      6300BDBABD00F7CBA500F7CBA500F7CBA500F7CBA500F7CBA500BDBABD009C65
      6300BDBABD009C0000009C0000000000000000000000000000009C656300BDBA
      BD00F7CBA5009C656300BDBABD00F7CBA500F7CBA500BDBABD009C656300BDBA
      BD00F7CBA5009C00000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000009400000084000000940000008400
      00000000000000000000000000000000000000000000000000009C6563009C65
      63009C6563000000000000000000000000000000000000000000000000000000
      00009C0000009C0000009C000000000000009C656300BDBABD00F7CBA5009C65
      6300BDBABD00F7CBA500F7CBA500F7CBA500F7CBA500F7CBA500BDBABD009C65
      6300BDBABD00F7CBA5009C000000000000000000000000000000000000009C65
      63009C6563009C6563009C6563009C6563009C6563009C6563009C6563009C65
      63009C6563000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000009C0000009C00000000000000000000009C6563009C6563009C65
      63009C6563009C6563009C6563009C6563009C6563009C6563009C6563009C65
      63009C6563009C6563000000000000000000000000009C000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000009C000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000009C00000000000000000000000000
      00009C000000000000009C000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000009C00000000000000000000000000
      00000000000000000000000000009C0000000000000000000000000000000000
      00000000000000000000000000009C0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000009C0000009C0000009C00
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000C6284A00C6284A00C6284A000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000C6284A00C6284A00C6284A00C6284A000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000C6284A00FF5D8C00FF5D8C00FF5D
      8C0000000000FFFFFF0084828400FFFFFF0084828400FFFFFF00FFFFFF000000
      0000FF5D8C00FF5D8C00FF5D8C00C6284A000000000000000000000000000000
      0000000000008400000094000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000C6284A00FF5D8C00FF9ACE00FF9A
      CE0000000000FFFFFF0084828400FFFFFF0084828400FFFFFF00FFFFFF000000
      0000FF9ACE00FF9ACE00FF5D8C00C6284A000000000000000000000000000000
      0000840000000000000000000000840000000000000000000000840000008400
      0000000000000000000000000000000000000000000000000000000000009C20
      3900000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C62C4A000000000000000000000000009C0000009C00
      00009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C00
      00009C0000009C0000000000000000000000C6284A00FF5D8C00FF9ACE00FFAA
      DE0000000000FFFFFF0084828400FFFFFF0084828400FFFFFF00FFFFFF000000
      0000FFAADE00FF9ACE00FF5D8C00C6284A000000000000000000000000000000
      0000940000000000000000000000840000000000000084000000000000000000
      0000940000000000000000000000000000000000000000000000941C39009420
      39009C2039000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009C656300FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF009C0000000000000000000000C6284A00FF5D8C00FF9ACE00FFAA
      DE0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000FFAADE00FF9ACE00FF5D8C00C6284A000000000000000000000000000000
      0000840000000000000000000000940000000000000084000000000000000000
      0000840000000000000000000000000000000000000000000000941C3100941C
      39009C203900A520390000000000000000000000000000000000000000000000
      000000000000C6345200000000000000000000000000000000009C656300FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF009C0000000000000000000000C6284A00FF5D8C00FF9ACE00FFAA
      DE00000000000000000000000000000000000000000000000000000000000000
      0000FFAADE00FF9ACE00FF5D8C00C6284A000000000000000000000000000000
      0000000000008400000094000000840000000000000084000000000000000000
      0000940000000000000000000000000000000000000000000000000000008C1C
      3100942039009C20390000000000000000000000000000000000000000000000
      0000C638520000000000000000000000000000000000000000009C656300FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF009C0000000000000000000000C6284A00FF5D8C00FF9ACE00FFAA
      DE00000000000000000000000000000000000000000000000000000000000000
      0000FFAADE00FF9ACE00FF5D8C00C6284A000000000000000000000000000000
      0000000000000000000000000000840000000000000094000000840000008400
      0000000000000000000000000000000000000000000000000000000000000000
      0000942039009C203900AD24420000000000000000000000000000000000C638
      5A00CE415A0000000000000000000000000000000000000000009C656300FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF009C0000000000000000000000C6284A00FF5D8C00FF9ACE00FFAA
      DE000000000000BEBD0000BEBD0000BEBD0000BEBD0000BEBD0000BEBD000000
      0000FFAADE00FF9ACE00FF5D8C00C6284A000000000000000000000000000000
      0000000000000000000000000000840000000000000084000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000A5203900AD244200BD2842000000000000000000C63C5A00CE45
      63000000000000000000000000000000000000000000000000009C656300FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF009C0000000000000000000000C6284A00FF5D8C00FF9ACE00FFAA
      DE00FFC3F7000000000000BEBD0000BEBD0000BEBD0000BEBD0000000000FFC3
      F700FFAADE00FF9ACE00FF5D8C00C6284A000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000B5244200C6284A00C6345200C63C5A00CE4563000000
      00000000000000000000000000000000000000000000000000009C656300FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF009C0000000000000000000000C6284A00FF5D8C00FF9ACE00FFAA
      DE00FFC3F700FFCBFF000000000000BEBD0000BEBD0000000000FFCBFF00FFC3
      F700FFAADE00FF9ACE00FF5D8C00C6284A000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000C62C4A00C6385A00CE415A00000000000000
      00000000000000000000000000000000000000000000000000009C656300FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF009C000000FFFFFF00FFFF
      FF009C0000009C0000000000000000000000C6284A00FF5D8C00FF9ACE00FFAA
      DE00FFC3F700FFCBFF000000000000BEBD0000BEBD0000000000FFCBFF00FFC3
      F700FFAADE00FF9ACE00FF5D8C00C6284A000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000BD284A00C6345200C6415A00CE496300CE516B000000
      00000000000000000000000000000000000000000000000000009C656300FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF009C6563009C0000009C00
      0000FFFFFF009C6563000000000000000000C6284A00FF5D8C00FF9ACE00FFAA
      DE00FFC3F700FFC3F7000000000000BEBD0000BEBD0000000000FFC3F700FFC3
      F700FFAADE00FF9ACE00FF5D8C00C6284A000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000B5244200C62C4A00C63C5A000000000000000000CE557300CE5D
      73000000000000000000000000000000000000000000000000009C656300FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF009C656300FFFFFF00FFFF
      FF009C656300000000000000000000000000C6284A00FF5D8C00FF9ACE00FFAA
      DE00FFAADE00FFAADE000000000000BEBD0000BEBD0000000000FFAADE00FFAA
      DE00FFAADE00FF9ACE00FF5D8C00C6284A000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000A520
      3900B5244200BD284A00C634520000000000000000000000000000000000CE5D
      7300D65D730000000000000000000000000000000000000000009C656300FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF009C656300FFFFFF009C65
      630000000000000000000000000000000000C6284A00FF5D8C00FF9ACE00FF9A
      CE00FF9ACE00FF9ACE000000000000BEBD0000BEBD0000000000FF9ACE00FF9A
      CE00FF9ACE00FF9ACE00FF5D8C00C6284A000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000A5244200AD24
      4200BD284200C62C4A0000000000000000000000000000000000000000000000
      0000CE5D7300CE597300000000000000000000000000000000009C656300FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF009C6563009C6563000000
      000000000000000000000000000000000000C6284A00FF5D8C00FF5D8C00FF5D
      8C00FF5D8C00FF5D8C000000000000BEBD0000BEBD0000000000FF5D8C00FF5D
      8C00FF5D8C00FF5D8C00FF5D8C00C6284A000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000B5244200BD24
      4200C6284A000000000000000000000000000000000000000000000000000000
      00000000000000000000CE516B000000000000000000000000009C6563009C65
      63009C6563009C6563009C6563009C6563009C6563009C656300000000000000
      000000000000000000000000000000000000C6284A00C6284A00C6284A00C628
      4A00C6284A00C6284A00000000000000000000000000C6284A00C6284A00C628
      4A00C6284A00C6284A00C6284A00C6284A000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000BD000000BD00000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000BD000000BD00000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000BD00
      0000C6C3C600BD000000BD000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000BD00
      0000C6C3C600BD000000BD000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000094000000840000009400000084000000940000008400
      0000940000008400000094000000840000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000BD000000C6C3
      C600BD000000BD000000BD000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000BD000000C6C3
      C600BD000000BD000000BD000000000000000000000000000000000000000000
      0000000000000000000084000000840000009400000084000000840000008400
      0000940000008400000084000000000000000000000000000000000000000000
      0000000000000000000084000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00840000000000000000000000000000000000
      00000000000000000000000000000000000000000000BD000000C6C3C600BD00
      0000BD000000BD00000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000BD000000C6C3C600BD00
      0000BD000000BD00000000000000000000000000000000000000000000000000
      0000000000000000000084000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0084000000000000000000000084828400008A94008482
      8400008A94008482840094000000FFFFFF000000000000000000000000000000
      00000000000000000000FFFFFF00840000000000000000000000000000000000
      000000000000000000000000000000000000BD000000C6C3C600BD000000BD00
      0000BD0000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000BD000000C6C3C600BD000000BD00
      0000BD0000000000000000000000000000000000000000000000000000000000
      0000000000000000000094000000FFFFFF000000000000000000000000000000
      000000000000FFFFFF0094000000000000000000000000828400848284000082
      9400848284000082840084000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00940000000000000000000000000000000000
      0000C6C3C600C6C3C600C6C3C600FFFFFF0084828400BD000000BD000000BD00
      0000000000000000000000000000000000000000000000000000000000000000
      0000C6C3C600C6C3C600C6C3C600FFFFFF0084828400BD000000BD000000BD00
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000084000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0084000000000000000000000084828400008A94008482
      8400008A94008482840094000000FFFFFF00000000000000000000000000FFFF
      FF0094000000840000009400000084000000000000000000000084828400C6C3
      C600C6C3C600C6C3C600C6C3C600C6C3C600FFFFFF0084828400000000008482
      840000000000000000000000000000000000000000000000000084828400C6C3
      C600C6C3C600C6C3C600C6C3C600C6C3C600FFFFFF0084828400000000008482
      84000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0084000000FFFFFF000000000000000000000000000000
      000000000000FFFFFF0084000000000000000000000000829400848284000082
      8400848284000082940084000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0084000000FFFFFF0084000000000000000000000000000000C6C3C600C6C3
      C600C6C3C6000000000000000000C6C3C600C6C3C600FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000C6C3C600C6C3
      C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600FFFFFF00000000000000
      00000000000000000000000000000000000000000000FFFFFF00000000000000
      0000000000000000000084000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0084000000000000000000000084828400008A94008482
      8400008A94008482840094000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF009400000084000000000000000000000000000000C6C3C600C6C3C600C6C3
      C600C6C3C6000000000000000000C6C3C600C6C3C600C6C3C600C6C3C6000000
      00000000000000000000000000000000000000000000C6C3C600C6C3C600C6C3
      C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C6000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0094000000FFFFFF000000000000000000FFFFFF008400
      0000840000008400000094000000000000000000000000828400848284000082
      9400848284000082840084000000940000008400000084000000840000009400
      00008400000000000000000000000000000000000000C6C3C600C6C3C6000000
      00000000000000000000000000000000000000000000C6C3C600C6C3C6000000
      00000000000000000000000000000000000000000000C6C3C600C6C3C6000000
      00000000000000000000000000000000000000000000C6C3C600C6C3C6000000
      00000000000000000000000000000000000000000000FFFFFF00000000000000
      0000000000000000000084000000FFFFFF00FFFFFF00FFFFFF00FFFFFF009400
      0000FFFFFF009400000000000000000000000000000084828400008A94008482
      8400008A940084828400008A940084828400008A940084828400008A94008482
      8400008A940000000000000000000000000000000000C6C3C600FFFFFF000000
      00000000000000000000000000000000000000000000C6C3C600C6C3C6000000
      00000000000000000000000000000000000000000000C6C3C600FFFFFF000000
      00000000000000000000000000000000000000000000C6C3C600C6C3C6000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0084000000FFFFFF00FFFFFF00FFFFFF00FFFFFF008400
      0000940000000000000000000000000000000000000000829400848284000000
      0000000000000000000000000000000000000000000000000000000000008482
      84008482840000000000000000000000000000000000C6C3C600FFFFFF00C6C3
      C600C6C3C6000000000000000000C6C3C600C6C3C600C6C3C600C6C3C6000000
      00000000000000000000000000000000000000000000C6C3C600FFFFFF00C6C3
      C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C6000000
      00000000000000000000000000000000000000000000FFFFFF00000000000000
      0000FFFFFF000000000084000000940000008400000094000000840000009400
      0000000000000000000000000000000000000000000084828400948A94000000
      0000D6CBD600D6C3D600D6CBD600C6C3C600D6CBD600D6C3D600000000008482
      8400008A94000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00C6C3C6000000000000000000C6C3C600C6C3C600C6C3C600000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000828400848284000082
      94000000000000FFFF00000000000000000000FFFF0000000000848284000082
      940084828400000000000000000000000000000000000000000084828400FFFF
      FF00FFFFFF00FFFFFF00C6C3C600C6C3C600C6C3C60084828400000000000000
      000000000000000000000000000000000000000000000000000084828400FFFF
      FF00FFFFFF00FFFFFF00C6C3C600C6C3C600C6C3C60084828400000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000FFFF0000FFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000C6C3C600C6C3C600C6C3C600C6C3C6000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000C6C3C600C6C3C600C6C3C600C6C3C6000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000C6C3C600C6C3
      C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3
      C600C6C3C600C6C3C60000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6C3C600C6C3C600C6C3
      C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3
      C600C6C3C600C6C3C60000000000000000000000000000000000C6C3C600C6C3
      C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3
      C600C6C3C600C6C3C60000000000000000000000000000000000C6C3C600C6C3
      C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3
      C600C6C3C6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6C3C600C6C3C600C6C3
      C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3
      C600C6C3C600C6C3C6000000000000000000000000000000000000000000C6C3
      C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3
      C600C6C3C600C6C3C600000000000000000000000000C6C3C600C6C3C600C6C3
      C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3
      C600C6C3C600C6C3C60000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6C3C600C6C3C600C6C3
      C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3
      C600C6C3C600C6C3C6000000000000000000000000000000000000000000C6C3
      C600C6C3C600C6C3C600C6C3C600C6C3C6000000000000000000000000000000
      00000000000000000000000000000000000000000000C6C3C600C6C3C600C6C3
      C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3
      C600C6C3C600C6C3C60000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6C3C600C6C3C600C6C3
      C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3
      C600C6C3C600C6C3C6000000000000000000000000000000000000000000C6C3
      C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C60000000000000000000000
      00000000000000000000000000000000000000000000C6C3C600C6C3C600C6C3
      C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3
      C600C6C3C600C6C3C60000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6C3C600C6C3C600C6C3
      C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3
      C600C6C3C600C6C3C60000000000000000000000000000000000000000000000
      0000C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600000000000000
      00000000000000000000000000000000000000000000C6C3C600C6C3C600C6C3
      C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3
      C600C6C3C600C6C3C60000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6C3C600C6C3C600C6C3
      C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3
      C600C6C3C600C6C3C60000000000000000000000000000000000000000000000
      0000C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600000000000000
      00000000000000000000000000000000000000000000C6C3C600C6C3C600C6C3
      C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3
      C600C6C3C600C6C3C60000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6C3C600C6C3C600C6C3
      C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3
      C600C6C3C600C6C3C60000000000000000000000000000000000000000000000
      000000000000C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C6000000
      00000000000000000000000000000000000000000000C6C3C600C6C3C600C6C3
      C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3
      C600C6C3C600C6C3C60000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6C3C600C6C3C600C6C3
      C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3
      C600C6C3C600C6C3C60000000000000000000000000000000000000000000000
      000000000000C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C6000000
      00000000000000000000000000000000000000000000C6C3C600C6C3C600C6C3
      C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3
      C600C6C3C600C6C3C60000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6C3C600C6C3C600C6C3
      C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3
      C600C6C3C600C6C3C60000000000000000000000000000000000000000000000
      00000000000000000000C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3
      C600000000000000000000000000000000000000000000000000C6C3C600C6C3
      C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3
      C600C6C3C6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3
      C600000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF0084828400FFFFFF0084828400FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF0084828400FFFFFF0084828400FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF0084828400FFFFFF0084828400FFFFFF00FFFFFF000000
      000000000000000000000000000000000000BD00000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      00000000000000000000000000000000000000000000BD000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000BD00000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000BD000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000BEBD0000BEBD0000BEBD0000BEBD0000BEBD0000BEBD000000
      000000000000000000000000000000000000BD000000C6C3C600BD0000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FF0000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000BEBD0000BEBD0000BEBD0000BEBD00000000000000
      000000000000000000000000000000000000E7F3F700BD000000000000000000
      000000000000000000000000000000000000000000000000000000000000FF00
      000000000000FF000000FF000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000BEBD0000BEBD0000000000000000000000
      000000000000000000000000000000000000BD000000C6C3C600BD0000000000
      0000000000000000000000000000000000000000000000000000FF0000000000
      0000FF000000FF000000FF000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000BEBD0000BEBD0000000000000000000000
      00000000000000000000000000000000000000000000BD000000C6C3C600BD00
      00000000000000000000000000000000000000000000FF00000000000000FF00
      0000FF000000FF00000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000BEBD0000BEBD0000000000000000000000
      000000000000000000000000000000000000BD000000C6C3C600BD0000000000
      0000000000000000000000000000FFFFFF000000000000000000FF000000FF00
      0000FF0000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000BEBD0000BEBD0000000000000000000000
      00000000000000000000000000000000000000000000BD000000C6C3C600BD00
      0000000000000000000000000000FFFFFF00FFFFFF000000000000000000FF00
      0000FF0000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000BEBD0000BEBD0000000000000000000000
      000000000000000000000000000000000000BD000000C6C3C600BD000000C6C3
      C600BD000000000000000000000000000000FFFFFF00FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000BEBD0000BEBD0000000000000000000000
      00000000000000000000000000000000000000000000BD000000C6C3C600BD00
      0000C6C3C600BD000000BD000000BD0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000BD0000000000
      0000BD0000000000000000000000BD0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000BDBE0000BDBE0000BDBE
      0000BDBE0000BDBE0000BDBE0000BDBE00000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000004A494A004A494A00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000BDBE0000BDBE0000000000000000
      000000000000BDBE0000BDBE0000BDBE0000BDBE0000BDBE0000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000007D000000DF000000BE29004A494A000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000BDBE000000000000BDBE
      0000BDBE000000000000BDBE0000BDBE0000BDBE000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000007D000000DF000000DF000000BE29004A494A0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000BDBE0000BDBE00000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00BDBE000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000007D000000DF000000DF000000DF000000BE29004A494A00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000BDBE0000BDBE
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00BDBE0000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000007D000000DF000000FF080000DF000000DF000000BE29004A494A000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000FFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000BDBE
      0000BDBE00000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00BDBE00000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000007D000000FF290000DF000000FF080000DF000000DF000000BE29004A49
      4A00000000000000000000000000000000000000000000000000000000000000
      000000FFFF00FFFFFF00C6C3C600000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000BDBE0000000000000000000000000000000000000000
      0000BDBE0000BDBE000000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00FFFFFF008482840000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000007D000052FF000000FF290000DF000000FF080000DF000000DF000000BE
      29004A494A000000000000000000000000000000000000000000000000000000
      00000000000000FFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000BDBE0000BDBE00000000000000000000000000000000
      000000000000BDBE0000BDBE0000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF00FFFFFF0084828400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000007D000052FFAD0000FF080000FF290000DF000000FF080000DF000000DF
      0000007D00000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF0000FFFF00C6C3C6000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000C6C3C6000000
      000000000000BDBE000000000000BDBE0000BDBE000000000000000000000000
      00000000000000000000BDBE0000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00848284000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000007D000052FFAD0000FF080000FF080000FF290000DF000052FFAD00007D
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF0000FFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000C6C3C6000000
      0000BDBE0000BDBE0000BDBE0000BDBE0000BDBE0000BDBE0000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000007D0000ADFF290000FF080000FF080000FF080000FF2900007D00000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000FFFF00FFFFFF00C6C3C60000000000000000000000
      000000000000000000000000000000000000000000000000000000000000C6C3
      C60000000000BDBE0000BDBE0000BDBE0000BDBE0000BDBE0000BDBE00000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000007D0000ADFF290000FF080000FF0800ADFF2900007D0000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000FFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000C6C3C60000000000FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000007D0000ADFFAD0000FF0800ADFF2900007D000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000BD00FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000007D0000ADFFAD00ADFFAD00007D00000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000BD00FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000C6C3C6000000000000000000000000000000000000000000000000000000
      0000007D0000ADFFAD00007D0000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000BD000000BD000000BD000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000007D000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000BD000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000BD000000C6C3C600BD0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000BD000000C6C3C600BD000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000BD00
      0000C6C3C600BD00000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000C6C3C600C6C3
      C600C6C3C600C6C3C600C6C3C600C6C3C6000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C6000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000094FFFF00000000000000000000000000BD000000C6C3
      C600BD0000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000C6C3C600C6C3
      C600C6C3C600C6C3C600C6C3C600C6C3C6000000000000BEBD00000000000000
      0000000000000000000000000000000000000000000000000000C6C3C600C6C3
      C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3
      C600C6C3C6000000000000000000000000000000000000000000A5FFFF000000
      000000000000000000004A8EFF000000000000000000BD000000C6C3C600BD00
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000C6C3C600C6C3
      C600C6C3C600C6C3C600C6C3C600C6C3C6000000000000BEBD0000BEBD000000
      00000000000000000000000000000000000000000000C6C3C600C6C3C600C6C3
      C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3
      C600C6C3C600C6C3C600000000000000000000000000000000000000000094FF
      FF009CFFFF00BDFFFF0000000000BDFFFF0094FFFF0084F3FF00BD0000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF000000000000BEBD0000BE
      BD000000000000000000000000000000000000000000C6C3C600C6C3C600C6C3
      C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3
      C600C6C3C600C6C3C6000000000000000000000000000000000000000000A5FF
      FF0073BAFF00CEFFFF00BDFFFF00CEFFFF0073BEFF009CFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF000000000000BE
      BD0000BEBD0000000000000000000000000000000000C6C3C600C6C3C600C6C3
      C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3
      C600C6C3C600C6C3C6000000000000000000000000000000000000000000BDFF
      FF00CEFFFF002134840094F3FF0021348400CEFFFF00BDFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF000000
      000000BEBD0000BEBD00000000000000000000000000C6C3C600C6C3C600C6C3
      C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3
      C600C6C3C600C6C3C6000000000000000000A5FFFF007BDBFF004275FF000000
      0000CEFFFF0094F3FF0021388C0094F3FF00BDFFFF00000000003971FF0073D3
      FF009CFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FF
      FF000000000000BEBD0000000000000000000000000000000000C6C3C600C6C3
      C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3
      C600C6C3C600000000000000000000000000000000000000000000000000BDFF
      FF00CEFFFF002134840094F3FF0021348400CEFFFF00BDFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF0000FFFF00FFFFFF0000FFFF00FFFF
      FF0000FFFF000000000000000000000000000000000000000000000000000000
      0000C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C6000000
      000000000000000000000000000000000000000000000000000000000000ADFF
      FF0073BAFF00CEFFFF00D6FFFF00CEFFFF0073BAFF00A5FFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000FFFF00FFFFFF0000FF
      FF00FFFFFF0000FFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000009CFF
      FF00ADFFFF00C6FFFF0000000000BDFFFF00ADFFFF0094FFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000ADFFFF000000
      000000000000000000005AA6FF00000000000000000000000000ADFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000A5FFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      0000000000000000000000000000FFFFFF0084828400FFFFFF0084828400FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      0000000000000000000000000000FFFFFF0084828400FFFFFF0084828400FFFF
      FF00FFFFFF0000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000FF000000FF000000FF
      000000FF000000FF000000000000399AD600399AD6003996D600398ED6004296
      DE003996D6000000000000000000000000000000000000FF000000FF000000FF
      000000FF000000FF000000000000FFFFFF0084828400FFFFFF0084828400FFFF
      FF00FFFFFF0000000000000000000000000000000000FFFFFF00000000000000
      0000FFFFFF000000000000000000000000000000000000000000FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000008400
      0000840000008400000084000000840000008400000084000000840000008400
      00008400000000000000000000000000000000000000000000000000000000FF
      00000000000000000000000000004292D600398ED600398AD600398AD600428E
      D600398ED60000000000000000000000000000000000000000000000000000FF
      0000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000008400
      0000844100008441000084410000844100008441000084410000844100008441
      00008400000000000000000000000000000000000000000000000000000000FF
      0000000000003986CE003986CE00428ACE003986CE003986CE003986CE003986
      CE003986CE0000000000000000000000000000000000000000000000000000FF
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00000000000000
      0000FFFFFF000000000000000000000000000000000000000000FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000008400
      0000FF820000FF000000FF000000FF000000FF000000FF000000FF0000008441
      0000840000000000000000000000000000000000000000000000000000000000
      0000000000003992D6003996D6003996D600398ED600428ED600398ECE00398E
      D6003992D6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000008400
      0000FF820000FF000000FF000000FF000000FF000000FF000000FF0000008441
      000084000000000000000000000000000000000000000000000000000000398A
      D600398AD600398AD6003992D600398ED600428ED600428ED600398ACE00428E
      D600398ED6000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000BEBD0000BEBD0000BEBD0000BEBD0000BE
      BD0000BEBD0000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000008400
      0000FF820000FF000000FF000000FF000000FF000000FF000000FF0000008441
      000084000000000000000000000000000000000000000000000000000000398A
      CE00398AD600398ED600398ED600398AD600428ED600428ACE00398ACE00398A
      CE00398ACE000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000BEBD0000BEBD0000BEBD0000BE
      BD000000000000000000000000000000000000000000FFFFFF00000000000000
      0000FFFFFF00FFFFFF00FFFFFF0000000000C6C3C60000000000FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000008400
      0000FF820000FF000000FF000000FF000000FF000000FF000000FF0000008441
      000084000000000000000000000000000000000000000000000000000000398E
      D600398ED600428ED600398ED600398AD600428ED6004292D600398ED600398E
      D6003992D6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000BEBD0000BEBD000000
      00000000000000000000000000000000000000000000FFFFFF0000000000D6C3
      D60000000000FFFFFF0000000000D6C3D60000000000C6C3C600000000000000
      0000000000000000000094000000840000000000000000000000000000008400
      0000FF820000FF000000FF000000FF000000FF000000FF000000FF0000008441
      0000840000000000000000000000000000000000000000000000000000003986
      CE003986CE004286CE003986CE003982CE003982CE003986CE004286CE003982
      CE003986CE000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000BEBD0000BEBD000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF000000
      0000C6C3C60000000000C6C3C60000000000C6C3C60000000000C6C3C600D6CB
      D600C6C3C6000000000084000000840000000000000000000000000000008400
      0000FF820000FF000000FF000000FF000000FF000000FF000000FF0000008441
      0000840000000000000000000000000000000000000000000000000000003992
      D6003996D6003992D6003992D600398ED6003992D600398ED600398AD600398E
      D600398ED6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000BEBD0000BEBD000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000D6C3D60000000000C6C3C60000000000D6C3D600D6CBD600C6C3
      C600D6CBD600D6C3D60094000000840000000000000000000000000000008400
      0000FF820000FF820000FF820000FF820000FF820000FF820000FF8200008441
      0000840000000000000000000000000000000000000000000000000000003996
      D6003996D6003992D6003992D6003992D6003996D6003996D6003996D6004296
      D6003992D6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000BEBD0000BEBD000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C6C3C60000000000C6C3C600D6CBD600C6C3C600D6CB
      D600C6C3C600D6CBD60084000000940000000000000000000000000000008400
      0000840000008400000084000000840000008400000084000000840000008400
      0000840000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000BEBD0000BEBD000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000D6C3D600D6CBD600C6C3C600D6CBD600D6C3
      D600D6CBD6000000000094000000840000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000BEBD0000BEBD000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000084000000840000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      0000000000000000000000000000000000000000000000000000DEDBDE00DEDB
      DE000000000000000000000000000000000000000000000000000000000000FF
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      0000000000000000000000000000000000000000000000000000638E9C0094CF
      DE00313431004A4D4A00424142000000000000000000000000000000000000FF
      0000000000000000000000000000DEDBDE00DEDBDE00DEDBDE00FFFFFF00FFFF
      FF00DEDBDE00DEDBDE00DEDBDE000000000000000000000000000000000000FF
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000FF000000FF000000FF
      000000FF000000FF00000000000094CFDE0073A6B5006B9EAD0073AABD0094CF
      DE00ADDBE700BDE3EF00B5DFEF00212021000000000000FF000000FF000000FF
      000000FF000000FF000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00DEDBDE000000000000FF000000FF000000FF
      000000FF000000FF000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      00000000000000000000000000009CD3DE0031515A0029515A0029515A0084B6
      C600A5D7E700C6E3EF00636563000000000000000000000000000000000000FF
      0000000000000000000000000000FFFFFF002949520029515A0029515A00FFFF
      FF00FFFFFF00FFFFFF00DEDBDE000000000000000000000000000000000000FF
      0000000000000000000000000000316594003165940000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      00000000000094CFDE0063828C009CCFDE007BB2C6006B9AA5005A8694002141
      4A009CCFDE006B696B00000000000000000000000000DEDBDE000000000000FF
      000000000000FFFFFF005A798400FFFFFF00FFFFFF00FFFFFF00FFFFFF00294D
      5200FFFFFF00DEDBDE00000000000000000000000000000000000000000000FF
      000000000000FFFF000000000000316594003165940000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008CC7D60052717B0039616B007BAEBD0029495200638A94006B96
      A5009CD3E70063656300000000000000000000000000DEDBDE00000000000000
      000000000000FFFFFF004265730039657300FFFFFF00213C4200FFFFFF00FFFF
      FF00FFFFFF00DEDBDE0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009CD3E7005A79
      840073A6B50073A2AD0094CFDE003159630029515A0031515A003155630084BE
      CE00A5D7E70063656300000000000000000000000000DEDBDE00FFFFFF004A6D
      7B00FFFFFF00FFFFFF00FFFFFF0039616B0031556300294D520031556300FFFF
      FF00FFFFFF00DEDBDE0000000000000000000000000000000000000000000000
      000000FFFF0000FFFF000000000000FF000000FF000000000000EF8AAD00EF8A
      AD00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009CD3E7004A71
      7B00638A940029454A0094CFDE0094CFDE008CC7DE008CCBDE00426573003965
      7300A5D3E70042414200000000000000000000000000DEDBDE00FFFFFF004265
      7300FFFFFF0029495200FFFFFF00FFFFFF00FFFFFF00FFFFFF0039616B005275
      8400FFFFFF00DEDBDE0000000000000000000000000000000000000000000000
      000000FFFF0000FFFF000000000000FF000000FF000000000000EF8AAD00EF8A
      AD00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000094CFDE008CCB
      DE002138420063929C00A5D7E700A5D7E7004265730094CFDE0094CFDE004A6D
      7B009CD3E700080C0800000000000000000000000000DEDBDE00FFFFFF00FFFF
      FF0018343900FFFFFF00FFFFFF00FFFFFF00315D6B00FFFFFF00FFFFFF005A7D
      8C00FFFFFF00DEDBDE0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A5D7E7009CD3E7008CC7
      D600213C4200294952009CD3E7004A717B00315D6B00315963008CC3D60094CF
      DE0094CFDE00000000000000000000000000DEDBDE00FFFFFF00FFFFFF00FFFF
      FF0018343900294D5200FFFFFF0039616B0031596300315D6B00FFFFFF00FFFF
      FF00FFFFFF00DEDBDE0000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF0000000000FF000000FF000000000000000000FF000000
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000063616300A5D7E7007BB6
      C600638E9C00294D5A0042697300426973008CCBDE0031555A00294D5A003159
      630084BACE0000000000000000000000000000000000DEDBDE00FFFFFF00FFFF
      FF00FFFFFF0031515A0052757B0052758400FFFFFF0031596300315563003961
      6B00FFFFFF00DEDBDE0000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF0000000000FF000000FF000000000000000000FF000000
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000181C18007BAA
      BD007BAABD008CC7D6009CD3E700A5D7E7009CD3E70084BECE0073A6B5007BB6
      C60073AABD00739EAD0029282900000000000000000000000000DEDBDE00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00DEDBDE00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000010101000181C1800181C180000000000000000000000
      00007BAEBD0084BECE004A4D4A0000000000000000000000000000000000DEDB
      DE00DEDBDE00DEDBDE00DEDBDE00DEDBDE00DEDBDE00DEDBDE00DEDBDE00DEDB
      DE00FFFFFF00FFFFFF00DEDBDE00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000002124210063656300000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000DEDBDE00DEDBDE00DEDBDE00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000000100000100010000000000000800000000000000000000
      000000000000000000000000FFFFFF0093FF137EC7FF0000000F01FD800F0000
      0007800700070000000300030003000000018003000100000000000300000000
      000080020000000000034003000300000003C003000300000003C00300030000
      0007C0030007000083FFC00383FF0000C7F1E007C7F10000FFF9BFFDFFF90000
      FF757EFEFF750000FF8FFFFFFF8F0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFE007F81FF81FE007E037F00FF00FE007E037E007E007E007
      E007E007E007E007E007E007E007E007E007E007E007E007E007E007E007E007
      E007E007E007E007E007F00FF00FE007E007F81FF81FE007FFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC3C7FFFFFFFFF81FC001C7CFFFFF
      E00780008001FFFFC003C000C000E007C003C000C001E007800180018003E007
      800180018003E007800180018003E007800180018003E007800180018003E007
      800100010003E007C00380008003E007C003C000C001E007E007E000E001FFFF
      F81FFFF1FFF1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC1FFFFFFFFFFFFF
      F81FFFFFFFFFFFFFF81FFFFFFFFFFC3FF81FFFFFFFFFF00FC001FFFFFC3FF00F
      8001FE7FF81FE0078001FC3FF81FE0078001FC3FF81FE0078001FE7FF81FE007
      8003FFFFFC3FF00FF81FFFFFFFFFF00FF81FFFFFFFFFFC3FF81FFFFFFFFFFFFF
      F83FFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000C7FF000000000000C7FF0000
      0000000001FF00000000000000070000000000000007000000000000C0070000
      00000000C007000000000000E007000000000000E007000000000000E0070000
      00000000E007000000000000E007000000000000E007000000000000FFFF0000
      00000000FFFF000000000000FFFF0000FFFFFF0F00000000FFFFFF0F00000000
      FFFFFF0F00000000FFFFE61F00000000FFFFE21F00000000EFFFE03F00000000
      EF83E00700000000DFC3E00700000000DFE3E00F00000000DFD3E01F00000000
      EF3BE03F00000000F0FFE07F00000000FFFFE0FF00000000FFFFE1FF00000000
      FFFFE3FF00000000FFFFE7FF00000000FFFFFFFF7F7EFFFF800F8003BFFDFFFF
      00070001E007FFFF00030001C003FFFF00010001C003FFFF00000001C003FFF7
      00000001C002C1F7000300014003C3FB00030001C003C7FB00030001C003CBFB
      00070001C003DCF783FF0001C003FF0FC7F10001E007FFFFFFF98003BFFDFFFF
      FF75FFFF7EFEFFFFFF8FFFFFFFFFFFFF0000FFFFFFFFFFFF0000F9FFFFFFFFFF
      0000F6CF4925C0030000F6B747FDC0030000F6B7C3FBC0030000F8B763F5C003
      0000FE8F71E5C0030000FE3FF8CFC0030000FF7F7C1DC0030000FE3F7E3DC003
      0000FEBFFC1FC0030000FC9F78CDC0070000FDDF4125C00F0000FDDFC3F3C01F
      0000FDDFC7FDC03F0000FFFFFFFFFFFFFFF3FFF3FFFFFFFFFFE1FFE1FFFFFC00
      FFC1FFC1FC018000FF83FF83FC010000F007F007FC010000C00FC00F00010000
      800F800F00010001801F801F00010003000F000F00010003000F000F00030003
      000F000F00070003000F000F000F0003801F801F00FF0003801F801F01FF8007
      C03FC03F03FFF87FF0FFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFE7FFFFFFFFFFFFF
      FCFFFFFF8001FFFFF9FF00018001C007FBFF000180018003FBFF0001C0010001
      F9FF0001C0010001FCFF0001C03F0001FE7F0001E01F0001FF3F0001E01F0001
      FF9F0001F00F0001FFDF0001F00F0001FFDF0001F8078003FF9F0001F807C007
      FF3FFFFFF803FFFFFE7FFFFFFFFFFFFFE00FFFFFFFFFFFFFF00FFFFFFFFFFFFF
      F00FFFFF8701FFF9F00F7FFFEFC7FFF3F00FBFFFEF87FFE7F00F7FE7F00FFFCF
      F00FBFC3F70FFF9FF00F1F81F71FFF3FF81F3F00FA1FFE7FFC3F1E00FA3FFCFF
      FC3F8C01FC3FF9FFFC3F1C03FC7FF3FFFC3F8C07FC7FE7FFFC3F040FFEFFCFFF
      FC3F801FFFFF9FFFFC7FD6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF80FFF9FFEFFF
      DFFF003FF0FFE7FF9F9F807FF07FE3FF9F0FC1FFF03FE1FF1E07E0FFF01FE0FF
      1C03F07FF00FE0FF1801F83FF007F07F1800FC17F007F07F0000FE07F00FF83F
      0001FF03F01FF83F0003FF81F03FFC1F8007FF00F07FFC1FC40FFFC0F0FFFE0F
      FE3FFFE0F1FFFE0FFF3FFFF1FBFFFF1FFFFFFFFFFFFFFFFFFFFDFFFFFFFFFFFF
      FFF84925FFFFFFFFFFF17FFDC07FF01FFFE3FFFF803FC007FDC77FFD801F8003
      DD8F7FFD800F0001E21FFFFFC0070001E03F7FFDE0030001E03F7FFDF0010001
      1047FFFFF8018003E03F7FFDFC01C007E03F4925FE01F01FE23FFFFFFF03FFFF
      DDDFFFFFFFFFFFFFFDFFFFFFFFFFFFFFC7FFC003FFFFFFFFC7FFC403000FFFFF
      00030003000FFFFF00030003000FE00700030003000FE007C003C403000FE007
      C003C403000FE007C003FC03000FE007C003FE07000FE007C003FF0F0004E007
      C003FF0F0000E007C003FF0F0000E007C003FF0FF800E007C003FF0FFC00FFFF
      FFFFFF0FFE04FFFFFFFFFF9FFFFFFFFFFFFFC7FFC3C7C7FFFFFFC7CFC001C7FF
      FFFF0001000001FFFFBF000000000007FF9F000100000037FF8F80038001C037
      D20780038001C007D20380038001E007D20780038001E007FF8F80038001E007
      FF9F00030001E007FFBF80038000E007FFFFC001C000E007FFFFE001E000FFFF
      FFFFFFF1FFF1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFFBFFFFFFFFFFFF
      8FFFFC7FFEFFFFFF87F7FFFFFC7FFBFFC7EFFC7FF83FF3FFE3CFFC7FF01FE3FF
      F19FFFFFE00FC097F83FFC7FFC7F8097FC7FFC7FFC7FC097F83FE00FFFFFE3FF
      F19FF01FFC7FF3FFC3CFF83FFC7FFBFF87E7FC7FFFFFFFFF8FFBFEFFFC7FFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
  end
  object ColTblsPopup: TTntPopupMenu
    Images = ImgList
    Left = 499
    Top = 145
    object NewColorTable1: TTntMenuItem
      Action = ColorTableNewAction
    end
    object N3: TTntMenuItem
      Caption = '-'
    end
    object AddColorTablesFrom1: TTntMenuItem
      Action = ColTblAddFromFilesAction
    end
    object Replacepalette1: TTntMenuItem
      Action = ColorTableSaveToAction
    end
    object N4: TTntMenuItem
      Caption = '-'
    end
    object MoveColorTableUp1: TTntMenuItem
      Action = ColorTableUpAction
    end
    object MoveColorTableDown1: TTntMenuItem
      Action = ColorTableDownAction
    end
    object N24: TTntMenuItem
      Caption = '-'
    end
    object Copy1: TTntMenuItem
      Action = EditCutAction
    end
    object Copy2: TTntMenuItem
      Action = EditCopyAction
    end
    object Paste2: TTntMenuItem
      Action = EditPasteAction
    end
    object N5: TTntMenuItem
      Caption = '-'
    end
    object All2: TTntMenuItem
      Action = SelectAllAction
    end
    object Deselect2: TTntMenuItem
      Action = SelectDeselectAction
    end
    object Inverse2: TTntMenuItem
      Action = SelectInverseAction
    end
    object N23: TTntMenuItem
      Caption = '-'
    end
    object RemoveColorTable1: TTntMenuItem
      Action = ColorTableDeleteAction
    end
    object N22: TTntMenuItem
      Caption = '-'
    end
  end
  object SaveActDialog: TTntSaveDialog
    OnClose = SaveActDialogTypeChange
    OnShow = SaveActDialogTypeChange
    Filter = 
      'Adobe Color Table (*.act)|*.act|Customizable Color Tables (*.cct' +
      ')|*.cct|Binary File (*.*)|*.*'
    Options = [ofOverwritePrompt, ofHideReadOnly, ofEnableSizing]
    Title = 'Save Colors'
    OnTypeChange = SaveActDialogTypeChange
    Left = 379
    Top = 305
  end
  object ColorsPopup: TTntPopupMenu
    Images = ImgList
    Left = 499
    Top = 201
    object ReplaceColor1: TTntMenuItem
      Action = ColorReplaceAction
    end
    object ReplaceSelected1: TTntMenuItem
      Action = ColorsMakeGradientAction
    end
    object N30: TTntMenuItem
      Caption = '-'
    end
    object ReplaceColorUnderCursor1: TTntMenuItem
      Action = ColorsImportAction
    end
    object RemoveBrush1: TTntMenuItem
      Action = ColorsExportAction
    end
    object N29: TTntMenuItem
      Caption = '-'
    end
    object Cut1: TTntMenuItem
      Action = EditCutAction
    end
    object TntMenuItem10: TTntMenuItem
      Action = EditCopyAction
    end
    object Paste1: TTntMenuItem
      Action = EditPasteAction
    end
    object N26: TTntMenuItem
      Caption = '-'
    end
    object SelectAll1: TTntMenuItem
      Action = SelectAllAction
    end
    object Deselect1: TTntMenuItem
      Action = SelectDeselectAction
    end
    object Inverse1: TTntMenuItem
      Action = SelectInverseAction
    end
    object N28: TTntMenuItem
      Caption = '-'
    end
    object Delete6: TTntMenuItem
      Action = EditDeleteAction
    end
  end
  object MapsPopup: TTntPopupMenu
    Images = ImgList
    Left = 563
    Top = 145
    object MapNewMenuItem: TTntMenuItem
      Action = MapsFrm.MapNewAction
    end
    object TntMenuItem6: TTntMenuItem
      Caption = '-'
    end
    object MapAddFromFileMenuItem: TTntMenuItem
      Action = MapsFrm.BrushAddFromFileAction
    end
    object MapSaveToFileMenuItem: TTntMenuItem
      Action = MapsFrm.MapSaveToFileAction
    end
    object N36: TTntMenuItem
      Caption = '-'
    end
    object MapMoveUpMenuItem: TTntMenuItem
      Action = MapsFrm.MapUpAction
    end
    object MapMoveDownMenuItem: TTntMenuItem
      Action = MapsFrm.MapDownAction
    end
    object TntMenuItem9: TTntMenuItem
      Caption = '-'
    end
    object TntMenuItem11: TTntMenuItem
      Action = EditCutAction
    end
    object TntMenuItem12: TTntMenuItem
      Action = EditCopyAction
    end
    object TntMenuItem13: TTntMenuItem
      Action = EditPasteAction
    end
    object TntMenuItem14: TTntMenuItem
      Caption = '-'
    end
    object TntMenuItem15: TTntMenuItem
      Action = SelectAllAction
    end
    object TntMenuItem16: TTntMenuItem
      Action = SelectDeselectAction
    end
    object TntMenuItem17: TTntMenuItem
      Action = SelectInverseAction
    end
    object TntMenuItem18: TTntMenuItem
      Caption = '-'
    end
    object MapDeleteMenuItem: TTntMenuItem
      Action = MapsFrm.MapDeleteAction
    end
  end
  object LayersPopup: TTntPopupMenu
    Images = ImgList
    Left = 563
    Top = 201
    object TntMenuItem32: TTntMenuItem
      Action = MapsFrm.LayerNewAction
    end
    object TntMenuItem33: TTntMenuItem
      Caption = '-'
    end
    object TntMenuItem34: TTntMenuItem
      Action = MapsFrm.LayerUpAction
    end
    object TntMenuItem35: TTntMenuItem
      Action = MapsFrm.LayerDownAction
    end
    object TntMenuItem36: TTntMenuItem
      Caption = '-'
    end
    object TntMenuItem37: TTntMenuItem
      Action = EditCutAction
    end
    object TntMenuItem38: TTntMenuItem
      Action = EditCopyAction
    end
    object TntMenuItem39: TTntMenuItem
      Action = EditPasteAction
    end
    object TntMenuItem40: TTntMenuItem
      Caption = '-'
    end
    object TntMenuItem41: TTntMenuItem
      Action = SelectAllAction
    end
    object TntMenuItem42: TTntMenuItem
      Action = SelectDeselectAction
    end
    object TntMenuItem43: TTntMenuItem
      Action = SelectInverseAction
    end
    object TntMenuItem44: TTntMenuItem
      Caption = '-'
    end
    object TntMenuItem45: TTntMenuItem
      Action = MapsFrm.LayerDeleteAction
    end
  end
  object TileSetsPopup: TTntPopupMenu
    Images = ImgList
    Left = 619
    Top = 145
    object TileSetNewMenuItem: TTntMenuItem
      Caption = '&New Tile Set'
      Hint = 'New Tile Set|Adds a new tile set to the project'
      ImageIndex = 46
    end
    object TntMenuItem3: TTntMenuItem
      Caption = '-'
    end
    object TileSetAddFromFilesMenuItem: TTntMenuItem
      Action = TilesFrm.TileSetAddFromFilesAction
    end
    object TileSetSaveToFileMenuItem: TTntMenuItem
      Action = TilesFrm.TileSetSaveToFileAction
    end
    object N38: TTntMenuItem
      Caption = '-'
    end
    object TileSetMoveUpMenuItem: TTntMenuItem
      Action = TilesFrm.TileSetUpAction
    end
    object TileSetMoveDownMenuItem: TTntMenuItem
      Action = TilesFrm.TileSetDownAction
    end
    object TntMenuItem20: TTntMenuItem
      Caption = '-'
    end
    object TntMenuItem21: TTntMenuItem
      Action = EditCutAction
    end
    object TntMenuItem22: TTntMenuItem
      Action = EditCopyAction
    end
    object TntMenuItem23: TTntMenuItem
      Action = EditPasteAction
    end
    object TntMenuItem24: TTntMenuItem
      Caption = '-'
    end
    object TntMenuItem25: TTntMenuItem
      Action = SelectAllAction
    end
    object TntMenuItem26: TTntMenuItem
      Action = SelectDeselectAction
    end
    object TntMenuItem27: TTntMenuItem
      Action = SelectInverseAction
    end
    object TntMenuItem28: TTntMenuItem
      Caption = '-'
    end
    object TileSetDeleteMenuItem: TTntMenuItem
      Action = TilesFrm.TileSetDeleteAction
    end
  end
  object TilesPopup: TTntPopupMenu
    Images = ImgList
    Left = 619
    Top = 201
    object TntMenuItem30: TTntMenuItem
      Action = TilesTileEditAction
    end
    object N37: TTntMenuItem
      Caption = '-'
    end
    object ImportTiles2: TTntMenuItem
      Action = TilesFrm.TilesImportAction
    end
    object ExportTiles2: TTntMenuItem
      Action = TilesFrm.TilesExportAction
    end
    object TntMenuItem46: TTntMenuItem
      Caption = '-'
    end
    object TntMenuItem47: TTntMenuItem
      Action = EditCutAction
    end
    object TntMenuItem48: TTntMenuItem
      Action = EditCopyAction
    end
    object TntMenuItem49: TTntMenuItem
      Action = EditPasteAction
    end
    object TntMenuItem50: TTntMenuItem
      Caption = '-'
    end
    object TntMenuItem51: TTntMenuItem
      Action = SelectAllAction
    end
    object TntMenuItem52: TTntMenuItem
      Action = SelectDeselectAction
    end
    object TntMenuItem53: TTntMenuItem
      Action = SelectInverseAction
    end
    object TntMenuItem54: TTntMenuItem
      Caption = '-'
    end
    object TntMenuItem55: TTntMenuItem
      Action = EditDeleteAction
    end
  end
  object MapPopup: TTntPopupMenu
    Images = ImgList
    Left = 563
    Top = 249
    object TntMenuItem59: TTntMenuItem
      Action = EditCutAction
    end
    object TntMenuItem60: TTntMenuItem
      Action = EditCopyAction
    end
    object TntMenuItem61: TTntMenuItem
      Action = EditPasteAction
    end
    object TntMenuItem62: TTntMenuItem
      Caption = '-'
    end
    object TntMenuItem63: TTntMenuItem
      Action = SelectAllAction
    end
    object TntMenuItem64: TTntMenuItem
      Action = SelectDeselectAction
    end
    object TntMenuItem65: TTntMenuItem
      Action = SelectInverseAction
    end
    object N42: TTntMenuItem
      Caption = '-'
    end
    object FillSelection2: TTntMenuItem
      Action = SelectFillAction
    end
    object TntMenuItem66: TTntMenuItem
      Caption = '-'
    end
    object TntMenuItem67: TTntMenuItem
      Action = EditDeleteAction
    end
  end
  object TileEditPopup: TTntPopupMenu
    Images = ImgList
    Left = 659
    Top = 273
    object Selectintileset1: TTntMenuItem
      Action = SelectInTileSetAction
    end
    object N40: TTntMenuItem
      Caption = '-'
    end
    object TntMenuItem8: TTntMenuItem
      Action = EditCutAction
    end
    object TntMenuItem19: TTntMenuItem
      Action = EditCopyAction
    end
    object TntMenuItem29: TTntMenuItem
      Action = EditPasteAction
    end
    object TntMenuItem31: TTntMenuItem
      Caption = '-'
    end
    object TntMenuItem56: TTntMenuItem
      Action = SelectAllAction
    end
    object TntMenuItem57: TTntMenuItem
      Action = SelectDeselectAction
    end
    object TntMenuItem58: TTntMenuItem
      Action = SelectInverseAction
    end
    object N41: TTntMenuItem
      Caption = '-'
    end
    object FillSelection1: TTntMenuItem
      Action = SelectFillAction
    end
    object TntMenuItem68: TTntMenuItem
      Caption = '-'
    end
    object TntMenuItem69: TTntMenuItem
      Action = EditDeleteAction
    end
  end
end
