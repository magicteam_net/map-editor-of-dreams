object ToolsFrame: TToolsFrame
  Left = 0
  Top = 0
  Width = 1001
  Height = 89
  TabOrder = 0
  object ToolsPanel: TPanel
    Left = 0
    Top = 0
    Width = 1001
    Height = 89
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object Splitter1: TSplitter
      Left = 429
      Top = 0
      Height = 89
      Align = alRight
      AutoSnap = False
      ResizeStyle = rsUpdate
    end
    object HistoryPanel: TPanel
      Left = 69
      Top = 0
      Width = 360
      Height = 89
      Align = alClient
      TabOrder = 0
      object HistoryLabel: TTntLabel
        Left = 1
        Top = 1
        Width = 358
        Height = 13
        Align = alTop
        Caption = 'History'
      end
      object HistoryListBox: TListBox
        Left = 1
        Top = 14
        Width = 358
        Height = 74
        Align = alClient
        ItemHeight = 13
        TabOrder = 0
      end
    end
    object FileEditToolBar: TTntToolBar
      Left = 0
      Top = 0
      Width = 69
      Height = 89
      Align = alLeft
      EdgeInner = esNone
      EdgeOuter = esNone
      Flat = True
      TabOrder = 1
      Wrapable = False
      object FileNewButton: TTntToolButton
        Left = 0
        Top = 0
        ImageIndex = 27
      end
      object FileSaveButton: TTntToolButton
        Left = 23
        Top = 0
        Caption = 'FileSaveButton'
        ImageIndex = 29
      end
      object EditUndoButton: TTntToolButton
        Left = 46
        Top = 0
        Caption = 'EditUndoButton'
        ImageIndex = 26
        Wrap = True
      end
      object FileOpenButton: TTntToolButton
        Left = 0
        Top = 22
        Caption = 'FileOpenButton'
        ImageIndex = 28
      end
      object FileSaveAsButton: TTntToolButton
        Left = 23
        Top = 22
        Caption = 'FileSaveAsButton'
        ImageIndex = 30
      end
      object EditRedoButton: TTntToolButton
        Left = 46
        Top = 22
        Caption = 'EditRedoButton'
        ImageIndex = 25
        Wrap = True
      end
      object EditCutButton: TTntToolButton
        Left = 0
        Top = 44
        Caption = 'EditCutButton'
        ImageIndex = 23
      end
      object EditCopyButton: TTntToolButton
        Left = 23
        Top = 44
        Caption = 'EditCopyButton'
        ImageIndex = 22
      end
      object EditPasteButton: TTntToolButton
        Left = 46
        Top = 44
        Caption = 'EditPasteButton'
        ImageIndex = 24
        Wrap = True
      end
      object EditClearSelectionButton: TTntToolButton
        Left = 0
        Top = 66
        Caption = 'EditClearSelectionButton'
        ImageIndex = 21
      end
      object ViewZoomInButton: TTntToolButton
        Left = 23
        Top = 66
        Caption = 'ViewZoomInButton'
        ImageIndex = 11
      end
      object ViewZoomOutButton: TTntToolButton
        Left = 46
        Top = 66
        Caption = 'ViewZoomOutButton'
        ImageIndex = 20
      end
    end
    object DrawingToolsBar: TTntToolBar
      Left = 432
      Top = 0
      Width = 69
      Height = 89
      Align = alRight
      EdgeInner = esNone
      EdgeOuter = esNone
      Flat = True
      TabOrder = 2
      Wrapable = False
      object ToolSingleSelectButton: TTntToolButton
        Left = 0
        Top = 0
        Grouped = True
        ImageIndex = 19
        Style = tbsCheck
      end
      object ToolMultiSelectButton: TTntToolButton
        Left = 23
        Top = 0
        Grouped = True
        ImageIndex = 6
        Style = tbsCheck
      end
      object ToolMagicWandButton: TTntToolButton
        Left = 46
        Top = 0
        Grouped = True
        ImageIndex = 5
        Wrap = True
        Style = tbsCheck
      end
      object ToolPencilButton: TTntToolButton
        Left = 0
        Top = 22
        Grouped = True
        ImageIndex = 12
        Style = tbsCheck
      end
      object ToolBrushButton: TTntToolButton
        Left = 23
        Top = 22
        Grouped = True
        ImageIndex = 13
        Style = tbsCheck
      end
      object ToolEraserButton: TTntToolButton
        Left = 46
        Top = 22
        Grouped = True
        ImageIndex = 7
        Wrap = True
        Style = tbsCheck
      end
      object ToolLineButton: TTntToolButton
        Left = 0
        Top = 44
        Grouped = True
        ImageIndex = 14
        Style = tbsCheck
      end
      object ToolFloodFillButton: TTntToolButton
        Left = 23
        Top = 44
        Grouped = True
        ImageIndex = 9
        Style = tbsCheck
      end
      object ToolPickerButton: TTntToolButton
        Left = 46
        Top = 44
        Grouped = True
        ImageIndex = 10
        Wrap = True
        Style = tbsCheck
      end
      object ToolRectangleButton: TTntToolButton
        Left = 0
        Top = 66
        Grouped = True
        ImageIndex = 16
        Style = tbsCheck
      end
      object ToolPolygon: TTntToolButton
        Left = 23
        Top = 66
        Grouped = True
        ImageIndex = 17
        Style = tbsCheck
      end
      object ToolEllipseButton: TTntToolButton
        Left = 46
        Top = 66
        Grouped = True
        ImageIndex = 8
        Style = tbsCheck
      end
    end
    object ColorTablePanel: TPanel
      Left = 501
      Top = 0
      Width = 500
      Height = 89
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 3
      object Splitter2: TSplitter
        Left = 301
        Top = 0
        Height = 89
        Align = alRight
        AutoSnap = False
        ResizeStyle = rsUpdate
      end
      object Splitter3: TSplitter
        Left = 102
        Top = 0
        Height = 89
        AutoSnap = False
        ResizeStyle = rsUpdate
      end
      object ColorTablesListBoxPanel: TPanel
        Left = 0
        Top = 0
        Width = 102
        Height = 89
        Align = alLeft
        TabOrder = 0
        object ColorTablesLabel: TTntLabel
          Left = 1
          Top = 1
          Width = 100
          Height = 13
          Align = alTop
          Caption = 'Color tables'
        end
        object ColorTablesListBox: TListBox
          Left = 1
          Top = 36
          Width = 100
          Height = 52
          Align = alClient
          ItemHeight = 13
          TabOrder = 0
        end
        object ColorTablesToolBar: TTntToolBar
          Left = 1
          Top = 14
          Width = 100
          Height = 22
          EdgeInner = esNone
          EdgeOuter = esNone
          Flat = True
          TabOrder = 1
          Wrapable = False
          object TileSetNewToolBtn: TTntToolButton
            Left = 0
            Top = 0
            ImageIndex = 0
          end
          object sep1: TTntToolButton
            Left = 23
            Top = 0
            Width = 8
            Caption = 'sep1'
            Style = tbsSeparator
          end
          object TileSetUpToolBtn: TTntToolButton
            Left = 31
            Top = 0
            ImageIndex = 2
          end
          object TileSetDownToolBtn: TTntToolButton
            Left = 54
            Top = 0
            ImageIndex = 3
          end
          object TileSetDeleteToolBtn: TTntToolButton
            Left = 77
            Top = 0
            ImageIndex = 1
          end
        end
      end
      inline PaletteFrame: TPaletteFrame
        Left = 304
        Top = 0
        Width = 196
        Height = 89
        Align = alRight
        AutoScroll = False
        TabOrder = 1
        inherited PaletteView: TTileMapView
          Width = 196
          Height = 70
        end
        inherited PaletteStatusBar: TTntStatusBar
          Width = 196
        end
      end
      object PalPropsPanel: TPanel
        Left = 105
        Top = 0
        Width = 196
        Height = 89
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 2
        Visible = False
        object PalPropsLabel: TTntLabel
          Left = 0
          Top = 0
          Width = 196
          Height = 13
          Align = alTop
          Caption = 'Color table properties'
          ShowAccelChar = False
          Layout = tlCenter
        end
        object PalPropEditor: TPropertyEditor
          Tag = 4
          Left = 0
          Top = 13
          Width = 196
          Height = 76
          BoldChangedValues = False
          ButtonFillMode = fmShaded
          Align = alClient
          ScrollBarOptions.ScrollBars = ssVertical
          TabOrder = 0
          PropertyWidth = 90
          ValueWidth = 102
          Columns = <
            item
              Margin = 0
              Options = [coEnabled, coParentBidiMode, coParentColor, coResizable, coVisible, coFixed]
              Position = 0
              Width = 90
            end
            item
              Margin = 0
              Options = [coEnabled, coParentBidiMode, coParentColor, coResizable, coVisible, coFixed]
              Position = 1
              Width = 102
            end>
        end
      end
    end
  end
end
