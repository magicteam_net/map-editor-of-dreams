program med32;

uses
  Forms,
  me_main in 'me_main.pas' {MainForm},
  me_mapfrm in 'me_mapfrm.pas' {MapFrame: TFrame},
  me_lib in 'me_lib.pas',
  me_tilesfrm in 'me_tilesfrm.pas' {TilesFrame: TFrame},
  me_palfrm in 'me_palfrm.pas' {PaletteFrame: TFrame},
  me_tilesloaddlg in 'me_tilesloaddlg.pas' {LoadTilesDialog},
  me_mappropsfrm in 'me_mappropsfrm.pas' {MapsFrame: TFrame},
  me_tilesetpropsfrm in 'me_tilesetpropsfrm.pas' {TileSetsFrame: TFrame},
  me_project in 'me_project.pas',
  me_importcolorsdlg in 'me_importcolorsdlg.pas' {ImportColorsDlg},
  me_tileprops in 'me_tileprops.pas' {TilePropsFrame: TFrame},
  me_tileeditframe in 'me_tileeditframe.pas' {TileEditFrame: TFrame},
  me_plugdlg in 'me_plugdlg.pas' {PluginDialog: TTntForm},
  me_plugin in 'me_plugin.pas',
  me_plugadapters in 'me_plugadapters.pas',
  me_plugutils in 'me_plugutils.pas',
  me_process in 'me_process.pas' {ProcessDialog: TTntForm},
  me_importtilesdlg in 'me_importtilesdlg.pas' {ImportTilesDialog: TTntForm};

{$R *.res}

begin
  Application.Initialize;
  Application.Title := 'Map Editor of Dreams';
  Application.CreateForm(TMainForm, MainForm);
  Application.Run;
end.
