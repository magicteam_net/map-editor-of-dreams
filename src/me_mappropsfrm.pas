unit me_mappropsfrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ActnList, ImgList, TntActnList, StdCtrls, LayerListBox, TileMapView,
  ComCtrls, TntDialogs, TntComCtrls, TntClasses, ToolWin, TntStdCtrls, VirtualTrees, PropEditor,
  ExtCtrls, me_mapfrm, me_project, PropUtils, PropContainer, NodeLst, MyClasses,
  me_lib, BmpImg, BitmapEx, TilesEx;

const
 WM_REFRESHMAPPROPS =  WM_USER + 992;
 WM_REFRESHLAYERPROPS =  WM_USER + 991;
 TAG_MAPNAME = 0;
 TAG_MAPWDTH = 1;
 TAG_MAPHGHT = 2;
 TAG_BRSHEXT = 3;
 TAG_MAPCOLR = 4;
 TAG_MAPTLWD = 5;
 TAG_MAPTLHT = 6;

type
  TMapsFrame = class;
  TLayerDblClickEvent = procedure(Sender: TMapsFrame;
                                  Layer: TMapLayer;
                                  TileClicked: Boolean) of object;
  TLayerEvent = procedure(Sender: TMapsFrame; Layer: TMapLayer) of object;
  TRefreshMapContentEvent = procedure(Sender: TMapsFrame; Layers: Boolean) of object;

  TMapsFrame = class(TFrame)
    MapsListPanel: TPanel;
    MapPropertiesLabel: TTntLabel;
    MapPropEditor: TPropertyEditor;
    MapsToolBar: TTntToolBar;
    NewMapToolBtn: TTntToolButton;
    sep11: TTntToolButton;
    MapUpToolBtn: TTntToolButton;
    MapDownToolBtn: TTntToolButton;
    MapDeleteToolBtn: TTntToolButton;
    LayerListPanel: TPanel;
    LayersListLabel: TTntLabel;
    LayersListBox: TLayerListBox;
    LayersToolBar: TTntToolBar;
    NewLayerToolBtn: TTntToolButton;
    LayersToolSep: TTntToolButton;
    LayerUpToolBtn: TTntToolButton;
    LayerDownToolBtn: TTntToolButton;
    LayerDeleteToolBtn: TTntToolButton;
    LayersLabel: TTntLabel;
    LayerPropEditor: TPropertyEditor;
    ActionList: TTntActionList;
    MapNewAction: TTntAction;
    MapUpAction: TTntAction;
    MapDownAction: TTntAction;
    MapDeleteAction: TTntAction;
    BrushNewAction: TTntAction;
    BrushUpAction: TTntAction;
    BrushDownAction: TTntAction;
    BrushDeleteAction: TTntAction;
    LayerNewAction: TTntAction;
    LayerUpAction: TTntAction;
    LayerDownAction: TTntAction;
    LayerDeleteAction: TTntAction;
    MapsListBox: TListBox;
    MapsListLabel: TTntLabel;
    PageControl: TTntPageControl;
    PropsPage: TTntTabSheet;
    VisualPage: TTntTabSheet;
    MapsComboBox: TTntComboBox;
    Splitter1: TSplitter;
    Splitter2: TSplitter;
    Splitter3: TSplitter;
    MapFrame: TMapFrame;
    sep25: TTntToolButton;
    sep99: TTntToolButton;
    ImageList: TImageList;
    MapPropsPanel: TPanel;
    Panel1: TPanel;
    BrushAddFromFileAction: TTntAction;
    BrushSaveToFileAction: TTntAction;
    MapAddFromFileAction: TTntAction;
    MapSaveToFileAction: TTntAction;
    OpenDialog: TTntOpenDialog;
    SaveDialog: TTntSaveDialog;
    procedure LayerNewActionUpdate(Sender: TObject);
    procedure LayerNewActionExecute(Sender: TObject);
    procedure LayerUpActionUpdate(Sender: TObject);
    procedure LayerUpActionExecute(Sender: TObject);
    procedure LayerDownActionUpdate(Sender: TObject);
    procedure LayerDownActionExecute(Sender: TObject);
    procedure LayerDeleteActionUpdate(Sender: TObject);
    procedure LayerDeleteActionExecute(Sender: TObject);
    procedure MapNewActionUpdate(Sender: TObject);
    procedure MapNewActionExecute(Sender: TObject);
    procedure MapUpActionExecute(Sender: TObject);
    procedure MapUpActionUpdate(Sender: TObject);
    procedure MapDownActionExecute(Sender: TObject);
    procedure MapDownActionUpdate(Sender: TObject);
    procedure MapDeleteActionExecute(Sender: TObject);
    procedure MapDeleteActionUpdate(Sender: TObject);
    procedure MapsListBoxClick(Sender: TObject);
    function MapsListBoxDataFind(Control: TWinControl;
      FindString: String): Integer;
    procedure ScaleChange(Sender: TObject; var Value: Integer);
    procedure MapPropertiesValueChange(Sender: TPropertyListItem);
    procedure LayerPropertiesValueChange(Sender: TPropertyListItem);
    procedure MapFrameGetLayerActive(Sender: TMapFrame;
              Layer: TMapLayer; var Active: Boolean);
    procedure LayersListBoxClick(Sender: TObject);
    function LayersListBoxDataFind(Control: TWinControl;
      FindString: String): Integer;
    procedure LayersListBoxDrawPic(Sender: TObject; AIndex: Integer;
      Rect: TRect; State: TOwnerDrawState; var Handled: Boolean);
    procedure LayersListBoxGetItemClickMode(Sender: TObject;
      AIndex: Integer; var AModes: TItemClickModes);
    procedure LayersListBoxItemClick(Sender: TObject; AIndex: Integer;
      ChangedMode: TItemClickMode);
    procedure MapsComboBoxChange(Sender: TObject);
    procedure MapsListBoxDblClick(Sender: TObject);
    procedure VisualPageShow(Sender: TObject);
    procedure MapsListBoxDrawItem(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    procedure PageControlChange(Sender: TObject);
    procedure LayersListBoxDblClick(Sender: TObject);
    procedure MapsListBoxKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure LayersListBoxKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MapAddFromFileActionExecute(Sender: TObject);
    procedure MapSaveToFileActionExecute(Sender: TObject);
    procedure MapSaveToFileActionUpdate(Sender: TObject);
    procedure MapFrameMapEditViewBeforeContentChange(
      Sender: TCustomTileMapView; ChangeType: TChangeType);
    procedure LayersListBoxUnicodeGetData(Control: TWinControl;
      Index: Integer; var Data: WideString);
  private
    FDlgMapFileName: WideString;
    FDlgBrushFileName: WideString;  
    FProject: TMapEditorProject;
    FBrushMode: Boolean;
    FDrawTileHeader: TPicHeader;
    FDrawTileBmp: TBitmapContainer;
    FOnMapSelected: TNotifyEvent;
    FOnMapModified: TNotifyEvent;
    FOnMapPropsModified: TNotifyEvent;
    FOnMapsModified: TNotifyEvent;
    FOnMapDeselect: TNotifyEvent;
    FOnMapNameModified: TNotifyEvent;
    FOnRefreshMapContent: TRefreshMapContentEvent;
    FOnLayerSelected: TLayerEvent;
    FOnLayersModified: TNotifyEvent;
    FOnLayerDblClick: TLayerDblClickEvent;
    FOnOpen: TNotifyEvent;
    FOnPageControlChange: TNotifyEvent;
    FOnMapSelectionChanged: TNotifyEvent;
    FOnMapComboSelected: TNotifyEvent;
    procedure WMRefreshLayerProps(var Message: TMessage); message WM_REFRESHLAYERPROPS;
    procedure WMRefreshMapProps(var Message: TMessage); message WM_REFRESHMAPPROPS;
    function CanAct: Boolean;
    function GetMapIndex: Integer;
    function GetMapList: TBaseMapList;
    function GetMapScale: Integer;
    function GetLayerIndex: Integer;
    procedure SetLayerIndex(Value: Integer);
    procedure SetMapIndex(Value: Integer);
    procedure SetMapScale(Value: Integer);
  public
    property BrushMode: Boolean read FBrushMode;
    property MapList: TBaseMapList read GetMapList;
    property MapIndex: Integer read GetMapIndex write SetMapIndex;
    property MapScale: Integer read GetMapScale write SetMapScale;
    property LayerIndex: Integer read GetLayerIndex write SetLayerIndex;

    procedure SetProject(Value: TMapEditorProject; BrushMode: Boolean; ATag: Integer);

    procedure RefreshMapProps;
    procedure RefreshLayerProps;
    procedure RefreshColorTable(DoRepaint: Boolean = True);
    procedure RefreshMapContent(Layers: Boolean = False);
    procedure RefreshLists;
    procedure MapModified;
    procedure MapsModified(ProjectUpdate: Boolean);
    procedure MapPropsModified;
    procedure MapDeselect;
    procedure FillComboBox;
    procedure MapNameModified;
    procedure MapSelected;
    procedure MapComboSelected;
    procedure LayerSelected;
    procedure LayersModified;
    procedure RefreshLayersList(ClearSel: Boolean = True);
    procedure SelectMap(Index: Integer);
    procedure MapSelectionChanged;

    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    procedure CopyLayers(Cut: Boolean);
    procedure CopyMaps(Cut: Boolean);
    procedure PasteLayers;
    procedure PasteMaps;
    procedure AddMapsFromStream(Stream: TStream);
    function NewMoveLayersEvent(Map: TMap; Shift: Integer): THistoryEvent;
    function NewMoveMapsEvent(Shift: Integer): THistoryEvent;
    function AddListEvent(const Text: WideString): THistoryEvent;
    function AddMapEvent(Map: TMap; const Text: WideString): THistoryEvent;
    procedure SetListEventData(Event: THistoryEvent; Mode: TRedoUndo);
    procedure SetMapEventData(Event: THistoryEvent; Map: TMap; Mode: TRedoUndo);
  published
    property OnOpen: TNotifyEvent read FOnOpen write FOnOpen;
    property OnMapSelected: TNotifyEvent read FOnMapSelected
                                        write FOnMapSelected;
    property OnMapModified: TNotifyEvent read FOnMapModified
                                        write FOnMapModified;
    property OnMapsModified: TNotifyEvent read FOnMapsModified
                                         write FOnMapsModified;
    property OnMapDeselect: TNotifyEvent read FOnMapDeselect
                                        write FOnMapDeselect;
    property OnMapNameModified: TNotifyEvent read FOnMapNameModified
                                            write FOnMapNameModified;
    property OnRefreshMapContent: TRefreshMapContentEvent
                     read FOnRefreshMapContent write
                     FOnRefreshMapContent;
    property OnLayerSelected: TLayerEvent read FOnLayerSelected
                                          write FOnLayerSelected;
    property OnLayersModified: TNotifyEvent
                                  read FOnLayersModified
                                 write FOnLayersModified;
    property OnLayerDblClick: TLayerDblClickEvent
                                   read FOnLayerDblClick
                                  write FOnLayerDblClick;
    property OnMapPropsModified: TNotifyEvent read FOnMapPropsModified
                                             write FOnMapPropsModified;
    property OnPageControlChange: TNotifyEvent
                                          read FOnPageControlChange
                                         write FOnPageControlChange;
    property OnMapSelectionChanged: TNotifyEvent
                                    read FOnMapSelectionChanged
                                   write FOnMapSelectionChanged;
    property OnMapComboSelected: TNotifyEvent read FOnMapComboSelected
                                             write FOnMapComboSelected;
  end;

const
 ldNames: array [TLinkDirection] of WideString =
  ('Left',
   'Right',
   'Up',
   'Down',
   'Left/up',
   'Right/up',
   'Left/down',
   'Right/down');

 ldNamesLC: array [TLinkDirection] of WideString =
  ('left',
   'right',
   'up',
   'down',
   'left/up',
   'right/up',
   'left/down',
   'right/down');

 ChangeTypeStr: array[TChangeType] of WideString =
 ('Clear selected area',
  'Floating content apply');        

var
 CF_LAYERSCONTAINER: Word;
 CF_MAPSCONTAINER: Word;

  
implementation

uses HexUnit, MyUtils;

{$R *.dfm}

procedure TMapsFrame.LayerNewActionUpdate(Sender: TObject);
begin
 LayerNewAction.Enabled := CanAct and (MapFrame.MapData <> nil);
end;

procedure TMapsFrame.LayerNewActionExecute(Sender: TObject);
var
 Map: TMap;
 I: Integer;
 Event: THistoryEvent;
begin
 Map := MapFrame.MapData;
 if (Map <> nil) and CanAct then
 begin
  MapDeselect;

  //Event := FProject.History.AddEvent(Map, '');
  Event := AddMapEvent(Map, '');

  with Map.AddChild('Unnamed 1') as TMapLayer do
  begin
   if Map.Count > 1 then AssignLayerFormat(Map.LastNode.Prev as TMapLayer) else
   begin
    CellSize := 2;
    IndexMask := $FFFF;
   end;

   Event.Text := WideFormat('%s ''%s'': New layer ''%s''',
                                [MapTypeStr[BrushMode],
                                Map.Name, Name]);
  end;

  LayersListBox.Count := 0;
  LayersListBox.Count := Map.Count;
  I := Map.Count - 1;

  FProject.Update;

  LayerIndex := I;

  LayersModified;

  SetMapEventData(Event, Map, ruRedo);
  //Event.RedoData := Map;
 end;
end;

procedure TMapsFrame.LayerUpActionExecute(Sender: TObject);
var
 Map: TMap;
 I, NewII, J, L: Integer;
 Detached: array of TNode;
 Node, Prv: TNode;
 Event: THistoryEvent;
begin
 Map := MapFrame.MapData;
 if (Map <> nil) and CanAct then
  with Map do
   if Count > 1 then
   begin
    if LayersListBox.SelCount = 0 then
     LayersListBox.Selected[LayerIndex] := True;

    if LayersListBox.Selected[0] then Exit;

    MapDeselect;
      
    Event := NewMoveLayersEvent(Map, -1);

    LayerPropEditor.AcceptEdit;
    NewII := -1;
    Node := Map.LastNode;
    Map.FInternalFlags := [secLoading];
    try
     for I := Map.Count - 1 downto 0 do
     begin
      Prv := Node.Prev;
      if LayersListBox.Selected[I] then
      begin
       NewII := I - 1;
       L := Length(Detached);
       SetLength(Detached, L + 1);
       Detached[L] := Node;
       J := Node.Index;
       Map.DetachNode(Node);
       Node.Index := J - 1;
      end;
      Node := Prv;
     end;

     LayersListBox.ClearSelection;
    
     for I := Length(Detached) - 1 downto 0  do
     begin
      Node := Detached[I];
      J := Node.Index;
      Map.AttachNode(Node, J);
      LayersListBox.Selected[J] := True;
     end;
    finally
     Map.FInternalFlags := [];
    end;

    Map.United := True;

{    LayersListBox.ItemIndex := NewII;
    LayersListBox.Invalidate;}

    LayerIndex := NewII;

    LayersModified;

    Event.RedoData := Map;
   end;
end;

procedure TMapsFrame.LayerUpActionUpdate(Sender: TObject);
var
 Enable: Boolean;
 Map: TMap;
begin
 Map := MapFrame.MapData;
 Enable := (Map <> nil) and CanAct and (Map.Count > 1);
 if Enable then
 begin
  if LayersListBox.SelCount > 0 then
   Enable := not LayersListBox.Selected[0] else
   Enable := LayerIndex > 0;
 end;
 
 LayerUpAction.Enabled := Enable;
end;

procedure TMapsFrame.LayerDownActionUpdate(Sender: TObject);
var
 Enable: Boolean;
 Map: TMap;
begin
 Map := MapFrame.MapData;
 Enable := (Map <> nil) and CanAct and (Map.Count > 1);
 if Enable then
 begin
  if LayersListBox.SelCount > 0 then
   Enable := not LayersListBox.Selected[Map.Count - 1] else
   Enable := LayerIndex < Map.Count - 1;
 end;

 LayerDownAction.Enabled := Enable;
end;

procedure TMapsFrame.LayerDownActionExecute(Sender: TObject);
var
 Map: TMap;
 I, NewII, J, L: Integer;
 Detached: array of TNode;
 Node, Prv: TNode;
 Event: THistoryEvent;
begin
 Map := MapFrame.MapData;
 if (Map <> nil) and CanAct then
  with Map do
   if Count > 1 then
   begin
    if LayersListBox.SelCount = 0 then
     LayersListBox.Selected[LayerIndex] := True;

    if LayersListBox.Selected[Count - 1] then Exit;

    MapDeselect;

    Event := NewMoveLayersEvent(Map, +1);

    LayerPropEditor.AcceptEdit;

    Map.FInternalFlags := [secLoading];
    try
     NewII := -1;
     Node := Map.LastNode;
     for I := Map.Count - 1 downto 0 do
     begin
      Prv := Node.Prev;
      if LayersListBox.Selected[I] then
      begin
       if NewII < 0 then NewII := I + 1;
       L := Length(Detached);
       SetLength(Detached, L + 1);
       Detached[L] := Node;
       J := Node.Index;
       Map.DetachNode(Node);
       Node.Index := J + 1;
      end;
      Node := Prv;
     end;

     LayersListBox.ClearSelection;

     for I := Length(Detached) - 1 downto 0  do
     begin
      Node := Detached[I];
      J := Node.Index;
      Map.AttachNode(Node, J);
      LayersListBox.Selected[J] := True;
     end;
    finally
     Map.FInternalFlags := [];
    end;

    United := True;

  {  LayersListBox.Invalidate;

    LayersListBox.ItemIndex := NewII;}

    LayerIndex := NewII;

    LayersModified;

    Event.RedoData := Map;
   end;
end;

procedure TMapsFrame.LayerDeleteActionUpdate(Sender: TObject);
var
 Map: TMap;
begin
 Map := MapFrame.MapData;
 LayerDeleteAction.Enabled := (Map <> nil) and CanAct and
                           (Map.Count > 0);
end;

procedure TMapsFrame.LayerDeleteActionExecute(Sender: TObject);
var
 Map: TMap;
 I, Top: Integer;
 Node, Prv: TNode;
 Event: THistoryEvent;
 Str: WideString;
begin
 Map := MapFrame.MapData;
 if (Map <> nil) and CanAct then
  with Map do
   if Count > 0 then
   begin
    if LayersListBox.SelCount = 0 then
     LayersListBox.Selected[LayerIndex] := True;

    MapDeselect;

    if LayersListBox.SelCount = 1 then
     Str := WideFormat('%s ''%s'': Delete layer ''%s''',
                           [MapTypeStr[BrushMode], Map.Name,
                           Map.Layers[LayerIndex].Name]) else
     Str := WideFormat('%s ''%s'': Delete layers',
                           [MapTypeStr[BrushMode], Map.Name]);

//    Event := FProject.History.AddEvent(Map, Str);
    Event := AddMapEvent(Map, Str);  

    LayerPropEditor.AcceptEdit;
    Top := -1;
    Node := Map.LastNode;
    Map.FInternalFlags := [secLoading];
    try
     for I := Map.Count - 1 downto 0 do
     begin
      Prv := Node.Prev;
      if LayersListBox.Selected[I] then
      begin
       if Top < 0 then Top := I;
       Map.Remove(Node);
      end;
      Node := Prv;
     end;
    finally
     Map.FInternalFlags := [];
    end;

    MapList.Changed;

    LayersListBox.Count := Map.Count;
    if Top >= Map.Count then
     Dec(Top);

    if (Top < 0) and (Map.Count > 0) then
     Top := 0;

    LayerIndex := Top;

    LayersModified;

    //Event.RedoData := Map;
    SetMapEventData(Event, Map, ruRedo);
   end;
end;

procedure TMapsFrame.MapNewActionUpdate(Sender: TObject);
begin
 (Sender as TAction).Enabled := CanAct;
end;

procedure TMapsFrame.MapNewActionExecute(Sender: TObject);
var
 Last: TMap;
 Event: THistoryEvent;
 List: TBaseMapList;
begin
 if CanAct then
 begin
  List := MapList;
  Event := AddListEvent('');
  with List do
  begin
   Last := LastNode as TMap;

   with AddChild('Unnamed 1') as TMap do
   begin
    if Last = nil then
    begin
     TileWidth := 16;
     TileHeight := 16;
     if BrushMode then
      SetMapSize(1, 1) else
      SetMapSize(32, 32);
    end else
     AssignMapInfo(Last);

    Event.Text := WideFormat('New %s ''%s''', [MapTypeStrLC[BrushMode], Name]);
   end;

   MapIndex := Count - 1;

   FProject.Update;
   if Last <> nil then
   begin
    if FBrushMode then
     FProject.FBrushLayerFocus[LastNode.Index] :=
        FProject.FBrushLayerFocus[Last.Index] else
     FProject.FMapLayerFocus[LastNode.Index] :=
        FProject.FMapLayerFocus[Last.Index];
   end;

   MapsModified(False);
  end;

  SetListEventData(Event, ruRedo);
 end;
end;

procedure TMapsFrame.MapUpActionExecute(Sender: TObject);
var
 I, NewII, J, L: Integer;
 Detached: array of TNode;
 Node, Prv: TNode;
 Src, Dst: PBoolean;
 Temp: Boolean;
 Event: THistoryEvent;
 List: TBaseMapList;
begin
 if CanAct then
 begin
  List := MapList;
  with List do
   if Count > 1 then
   begin
    if MapsListBox.SelCount = 0 then
     MapsListBox.Selected[MapIndex] := True;

    if MapsListBox.Selected[0] then
     Exit;

    Event := NewMoveMapsEvent(-1);

    NewII := -1;
    Node := LastNode;
    for I := Count - 1 downto 0 do
    begin
     Prv := Node.Prev;
     if MapsListBox.Selected[I] then
     begin
      NewII := I - 1;
      L := Length(Detached);
      SetLength(Detached, L + 1);
      Detached[L] := Node;
      DetachNode(Node);
      Node.Index := NewII;

      if FBrushMode then
      begin
       Src := Addr(FProject.FBrushLayerFocus[I]);
       Dst := Addr(FProject.FBrushLayerFocus[NewII]);
      end else
      begin
       Src := Addr(FProject.FMapLayerFocus[I]);
       Dst := Addr(FProject.FMapLayerFocus[NewII]);
      end;
      Temp := Dst^;
      Dst^ := Src^;
      Src^ := Temp;
     end;
     Node := Prv;
    end;

   MapsListBox.ClearSelection;

    for I := Length(Detached) - 1 downto 0  do
    begin
     Node := Detached[I];
     J := Node.Index;
     AttachNode(Node, J);
     MapsListBox.Selected[J] := True;
    end;

    United := True;

    MapIndex := NewII;

    MapsModified(True);

    SetListEventData(Event, ruRedo);
   end;
 end;
end;

procedure TMapsFrame.MapUpActionUpdate(Sender: TObject);
var
 Enable: Boolean;
begin
 Enable := False;
 if CanAct and (MapList.Count > 1) then
 begin
  if MapsListBox.SelCount > 0 then
   Enable := not MapsListBox.Selected[0] else
   Enable := MapIndex > 0;
 end;
 
 (Sender as TAction).Enabled := Enable;
end;

procedure TMapsFrame.MapDownActionExecute(Sender: TObject);
var
 I, NewII, J, L: Integer;
 Detached: array of TNode;
 Node, Prv: TNode;
 Src, Dst: PBoolean;
 Temp: Boolean;
 List: TBaseMapList;
 Event: THistoryEvent;
begin
 if CanAct then
 begin
  List := MapList;
  with List do
   if Count > 1 then
   begin
    if MapsListBox.SelCount = 0 then
     MapsListBox.Selected[MapIndex] := True;

    if MapsListBox.Selected[Count - 1] then
     Exit;

    Event := NewMoveMapsEvent(+1);

    NewII := -1;
    Node := LastNode;
    for I := Count - 1 downto 0 do
    begin
     Prv := Node.Prev;
     if MapsListBox.Selected[I] then
     begin
      if NewII < 0 then NewII := I + 1;
      L := Length(Detached);
      SetLength(Detached, L + 1);
      Detached[L] := Node;
      DetachNode(Node);
      Node.Index := I + 1;

      if FBrushMode then
      begin
       Src := Addr(FProject.FBrushLayerFocus[I]);
       Dst := Addr(FProject.FBrushLayerFocus[I + 1]);
      end else
      begin
       Src := Addr(FProject.FMapLayerFocus[I]);
       Dst := Addr(FProject.FMapLayerFocus[I + 1]);
      end;
      Temp := Dst^;
      Dst^ := Src^;
      Src^ := Temp;
     end;
     Node := Prv;
    end;

    MapsListBox.ClearSelection;

    for I := Length(Detached) - 1 downto 0  do
    begin
     Node := Detached[I];
     J := Node.Index;
     AttachNode(Node, J);
     MapsListBox.Selected[J] := True;
    end;

    MapIndex := NewII;

    MapsModified(True);
    
    SetListEventData(Event, ruRedo);
   end;
 end;
end;

procedure TMapsFrame.MapDownActionUpdate(Sender: TObject);
var
 Enable: Boolean;
begin
 Enable := False;
 if CanAct then
  with MapList do
   if Count > 1 then
   begin
    if MapsListBox.SelCount > 0 then
     Enable := not MapsListBox.Selected[Count - 1] else
     Enable := MapIndex < Count - 1;
   end;

 (Sender as TAction).Enabled := Enable;
end;

procedure TMapsFrame.MapDeleteActionExecute(Sender: TObject);
var
 I, Top: Integer;
 Node, Prv: TNode;
 Used: Boolean;
 Msg: WideString;
 List: TBaseMapList;
 Event: THistoryEvent;
 Str: WideString;
begin
 if CanAct then
 begin
  List := MapList;
  with List do
   if Count > 0 then
   begin
    if MapsListBox.SelCount = 0 then
     MapsListBox.Selected[MapIndex] := True;

    if FBrushMode then
    begin
     Used := False;
     for I := 0 to Count - 1 do
      if MapsListBox.Selected[I] then
      begin
       Used := FProject.IsBrushUsed(I);
       if Used then Break;
      end;

     if Used then
     begin
      if MapsListBox.SelCount = 1 then
       Msg := 'This brush is in use. Delete it anyway?' else
       Msg := 'One or more of the selected brushes are in use.'#13#10 +
                      'Delete them anyway?';
      if WideMessageDlg(Msg, mtConfirmation, [mbYes, mbNo], 0) <> mrYes then
       Exit;
     end;
    end;

    MapDeselect;

    if MapsListBox.SelCount = 1 then
     Str := WideFormat('Delete %s ''%s''', [MapTypeStrLC[BrushMode],
                           Maps[MapIndex].Name]) else
     Str := WideFormat('Delete %s', [MapTypeStr2[BrushMode]]);

    Event := AddListEvent(Str);

    Top := -1;
    Node := LastNode;
    FInternalFlags := [secLoading];
    try
     for I := Count - 1 downto 0 do
     begin
      Prv := Node.Prev;
      if MapsListBox.Selected[I] then
      begin
       if Node is TMapBrush then
        FProject.UnhookBrush(TMapBrush(Node));

       if Top < 0 then Top := I;

       if I < Count - 1 then
       begin
        if FBrushMode then
         Move(FProject.FBrushLayerFocus[I],
              FProject.FBrushLayerFocus[I + 1],
              Length(FProject.FBrushLayerFocus) - (I + 1)) else
         Move(FProject.FMapLayerFocus[I],
              FProject.FMapLayerFocus[I + 1],
              Length(FProject.FMapLayerFocus) - (I + 1));
       end;

       if MapFrame.MapData = Node then
        MapFrame.SetMap(nil);
       Remove(Node);
      end;
      Node := Prv;
     end;
    finally
     FInternalFlags := [];
    end;

    Changed;

    if Top >= Count then
     Dec(Top);

    if (Top < 0) and (Count > 0) then
     Top := 0;

    MapIndex := Top;

    MapsModified(True);

    SetListEventData(Event, ruRedo);
   end;
 end;
end;

procedure TMapsFrame.MapDeleteActionUpdate(Sender: TObject);
begin
 (Sender as TAction).Enabled := CanAct and (MapList.Count > 0);
end;

procedure TMapsFrame.SetProject(Value: TMapEditorProject; BrushMode: Boolean; ATag: Integer);
begin
 if BrushMode then
 begin
  OpenDialog.Filter := 'Customizable Brushes (*.cbr)|*.cbr';
  OpenDialog.DefaultExt := 'cbr';
  OpenDialog.FileName := FDlgBrushFileName;
 end else
 begin
  OpenDialog.Filter := 'Customizable Maps (*.cmp)|*.cmp';
  OpenDialog.DefaultExt := 'cmp';
  OpenDialog.FileName := FDlgMapFileName;
 end;
 OpenDialog.Title := WideFormat('Open %s', [MapTypeStr3[BrushMode]]);
 SaveDialog.Title := WideFormat('Save %s', [MapTypeStr3[BrushMode]]);
 SaveDialog.Filter := OpenDialog.Filter;
 SaveDialog.DefaultExt := OpenDialog.DefaultExt;
 Tag := ATag;
 FProject := Value;
 FBrushMode := BrushMode;
 MapFrame.MapData := nil; 
 if FBrushMode then
 begin
  NewMapToolBtn.Action := BrushNewAction;
  MapUpToolBtn.Action := BrushUpAction;
  MapDownToolBtn.Action := BrushDownAction;
  MapDeleteToolBtn.Action := BrushDeleteAction;
 end else
 begin
  NewMapToolBtn.Action := MapNewAction;
  MapUpToolBtn.Action := MapUpAction;
  MapDownToolBtn.Action := MapDownAction;
  MapDeleteToolBtn.Action := MapDeleteAction;
 end;
 if FProject <> nil then
 begin
  if FBrushMode then
   MapFrame.Scale := FProject.FInfo.Scales.scBrushFrame[ATag]else
   MapFrame.Scale := FProject.FInfo.Scales.scMapFrame[ATag];
 end;
 MapsListLabel.Caption := MapTypeStr3[FBrushMode];
 LayersListLabel.Caption := WideFormat('%s layers', [MapTypeStr[FBrushMode]]);
 MapsListBox.Count := 0;
 RefreshLists;
end;

procedure TMapsFrame.MapsListBoxClick(Sender: TObject);
var
 I, Idx: Integer;
begin
 with MapsListBox do
 begin
  Idx := ItemIndex;
  if (Count > 0) and not Selected[Idx] then
   for I := 0 to Count - 1 do
    if Selected[I] then
    begin
     Idx := I;
     ItemIndex := I;     
     Break;;
    end;
 end;
 MapsComboBox.ItemIndex := Idx;
 MapIndex := Idx;
 MapSelected;
end;

function TMapsFrame.MapsListBoxDataFind(Control: TWinControl;
  FindString: String): Integer;
begin
 if FProject <> nil then
  Result := MapList.FindIndexByFirstLetters(FindString) else
  Result := -1;
end;

function TMapsFrame.CanAct: Boolean;
begin
 Result := (FProject <> nil) and not (secLoading in FProject.FInternalFlags)
                             and not (secSaving in FProject.FInternalFlags);
end;

constructor TMapsFrame.Create(AOwner: TComponent);
begin
 inherited;

 MapsListPanel.DoubleBuffered := True;
 PageControl.DoubleBuffered := True;
 MapsToolBar.DoubleBuffered := True;
 MapsListBox.DoubleBuffered := True;
 LayersToolBar.DoubleBuffered := True;
 LayersListBox.DoubleBuffered := True;
 FDrawTileBmp := TBitmapContainer.Create;
 FDrawTileBmp.ImageFlags := (8 - 1) or IMG_Y_FLIP; 
 MapFrame.OnScaleChange := ScaleChange;
 MapFrame.OnGetLayerActive := MapFrameGetLayerActive;
 MapPropEditor.PropertyList.OnValueChange := MapPropertiesValueChange;
 LayerPropEditor.PropertyList.OnValueChange := LayerPropertiesValueChange;
end;

procedure TMapsFrame.FillComboBox;
var
 I: Integer;
begin
 MapsComboBox.Clear;
 if FProject <> nil then
  with MapList do
  begin
   for I := 0 to Count - 1 do
    with Nodes[I] as TMap do
     MapsComboBox.Items.Add(WideFormat('%d. %s', [I + 1, Name]));

   MapsComboBox.ItemIndex := MapIndex;
  end;
end;

procedure TMapsFrame.MapDeselect;
var
 I: Integer;
begin
 if Assigned(FOnMapDeselect) then
  FOnMapDeselect(Self);

 MapPropEditor.AcceptEdit;
  
 with MapFrame do
 begin
  for I := 0 to MapsListBox.Count - 1 do
   if (MapsListBox.Selected[I] and
      (MapData.Index = I)) then
   begin
    MapEditView.Deselect;
    Exit;
   end;

  if MapData <> nil then
   MapEditView.Deselect;
 end;
end;

procedure TMapsFrame.MapModified;
begin
 MapFrame.MapModified := True;
 if Assigned(FOnMapModified) then
  FOnMapModified(Self);
end;

procedure TMapsFrame.MapNameModified;
begin
 RefreshMapProps;
 MapsListBox.Invalidate;
 FillComboBox;

 if Assigned(FOnMapNameModified) then
  FOnMapNameModified(Self);
end;

procedure TMapsFrame.MapSelected;
var
 Map: TMap;
 MStr: ^WideString;
 I: Integer;
begin
 if FProject <> nil then
 begin
  with MapList do
  begin
   MapFrame.SetMap(Maps[MapIndex], MapScale, FBrushMode);

   Map := MapFrame.MapData;

   MStr := Addr(MapTypeStr[FBrushMode]);
   if Map <> nil then
   begin
    MapPropertiesLabel.Caption := WideFormat('%s #%d properties',
                                [MStr^, Map.Index + 1]);
    LayersListLabel.Caption := WideFormat('%s #%d layers',
                                [MStr^, Map.Index + 1]);
    LayersListBox.Count := Map.Count;

    if Map.Count > 0 then
    begin
     I := LayerIndex;
     if I < 0 then
     begin
      I := 0;
      LayerIndex := 0;
     end;
     LayersListBox.ItemIndex := I;     
     LayersListBox.Selected[I] := True;
    end;
   end else
   begin
    LayersListBox.Count := 0;   
    MapPropertiesLabel.Caption := WideFormat('%s properties', [MStr^]);
    LayersListLabel.Caption := WideFormat('%s layers', [MStr^]);
   end;
    
   RefreshMapProps;
   RefreshColorTable;
   LayerSelected;

   if Assigned(FOnMapSelected) then
    FOnMapSelected(Self);
  end;
 end else
 begin
  MapFrame.SetMap(nil, 0, FBrushMode);
  RefreshMapProps;
  LayerSelected;
 end;
end;

procedure TMapsFrame.MapsModified(ProjectUpdate: Boolean);
begin
 if ProjectUpdate then
  FProject.Update;

 if Assigned(FOnMapsModified) then
  FOnMapsModified(Self);  

 RefreshLists;
end;

procedure TMapsFrame.WMRefreshMapProps(var Message: TMessage);
var
 List: TTntStringList;
 I: Integer;
 VFlags: Integer;
 UseMap, Map: TMap;
begin
 MapPropertiesLabel.Caption := WideFormat('%s properties', [MapTypeStr[FBrushMode]]);
 with MapPropEditor, PropertyList do
 begin
  ClearList;

  if (FProject <> nil) and
     (PageControl.ActivePageIndex = 0) then
  begin
   with MapList do
    if Count > 0 then
    begin
     if MapsListBox.SelCount = 0 then
      MapsListBox.Selected[MapIndex] := True;

     VFlags := 0;
     UseMap := nil;
     Map := nil;

     for I := 0 to Count - 1 do
      if MapsListBox.Selected[I] then
      begin
       Map := Nodes[I] as TMap;
       if UseMap <> nil then
       begin
        if Map.Name <> UseMap.Name then
         VFlags := VFlags or (1 shl TAG_MAPNAME);
        if Map.ColorTable <> UseMap.ColorTable then
         VFlags := VFlags or (1 shl TAG_MAPCOLR);
        if Map.Width <> UseMap.Width then
         VFlags := VFlags or (1 shl TAG_MAPWDTH);
        if Map.Height <> UseMap.Height then
         VFlags := VFlags or (1 shl TAG_MAPHGHT);
        if Map.TileWidth <> UseMap.TileWidth then
         VFlags := VFlags or (1 shl TAG_MAPTLWD);
        if Map.TileHeight <> UseMap.TileHeight then
         VFlags := VFlags or (1 shl TAG_MAPTLHT);
        if Map is TMapBrush then
        if TMapBrush(Map).Extends <> TMapBrush(UseMap).Extends then
         VFlags := VFlags or (1 shl TAG_BRSHEXT);
       end else
        UseMap := Map;
       end;

     if UseMap <> nil then
      with UseMap do
      begin
       if UseMap = Map then
        MapPropertiesLabel.Caption := WideFormat('%s #%d properties',
           [MapTypeStr[FBrushMode], Index + 1]) else
        MapPropertiesLabel.Caption :=
          WideFormat('Selected %s'' properties', [MapTypeStr2[FBrushMode]]);

       with AddString('Name', Name) do
       begin                                      
        if VFlags and (1 shl TAG_MAPNAME) <> 0 then
         Parameters := PL_MULTIPLE_VALUE;         
        UserTag := TAG_MAPNAME;
       end;
       with AddDecimal('Width', Width, 1, 32767 div TileWidth) do
       begin
        if VFlags and (1 shl TAG_MAPWDTH) <> 0 then
          Parameters := PL_MULTIPLE_VALUE;
        UserTag := TAG_MAPWDTH;
       end;
       with AddDecimal('Height', Height, 1, 32767 div TileHeight) do
       begin
        if VFlags and (1 shl TAG_MAPHGHT) <> 0 then
          Parameters := PL_MULTIPLE_VALUE;        
        UserTag := TAG_MAPHGHT;
       end;                                       
       if Map is TMapBrush then                     
       begin                                        
        List := TTntStringList.Create;              
        try                                         
         with FProject.FMaps.BrushList do           
          for I := 0 to Count - 1 do                
//           if (Map <> UseMap) or (I <> Map.Index) then
            List.Add(Brushes[I].Name);

         List.CaseSensitive := True;
     //    I := ;
  //       if I > Map.Index then
    //      Dec(I);
         with AddPickList('Extends brush', List, False, TMapBrush(Map).ExtendsIndex, False) do
         begin
          if VFlags and (1 shl TAG_BRSHEXT) <> 0 then
           Parameters := Parameters or PL_MULTIPLE_VALUE;
          UserTag := TAG_BRSHEXT;
         end;
        finally                                       
         List.Free;                                   
        end;                                          
       end;                                           
       List := TTntStringList.Create;                 
       try
        with FProject.FMaps.ColorTableList do
         for I := 0 to Count - 1 do
          List.Add(Items[I].Name);

        List.CaseSensitive := True;
        with AddPickList('Color table', List, False, ColorTableIndex, False) do
        begin
         if VFlags and (1 shl TAG_MAPCOLR) <> 0 then
          Parameters := Parameters or PL_MULTIPLE_VALUE;      
         UserTag := TAG_MAPCOLR;
        end;                                                  
       finally
        List.Free;
       end;
       with AddDecimal('Tile width', TileWidth, 1, 256) do     
       begin                                                   
        if VFlags and (1 shl TAG_MAPTLWD) <> 0 then
         Parameters := PL_MULTIPLE_VALUE;                      
        UserTag := TAG_MAPTLWD;
       end;                                                    
       with AddDecimal('Tile height', TileHeight, 1, 256) do
       begin                                                  
        if VFlags and (1 shl TAG_MAPTLHT) <> 0 then
         Parameters := PL_MULTIPLE_VALUE;
        UserTag := TAG_MAPTLHT;
       end;
      end;
    end;
  end;

  RootNodeCount := Count;
 end;
end;

procedure TMapsFrame.ScaleChange(Sender: TObject; var Value: Integer);
begin
 if FProject <> nil then
 begin
  if FBrushMode then
   FProject.FInfo.Scales.scBrushFrame[Tag] := Value else
   FProject.FInfo.Scales.scMapFrame[Tag] := Value;
 end;
end;

procedure TMapsFrame.LayerPropertiesValueChange(Sender: TPropertyListItem);

 procedure CheckLinkValue(Layer: TBrushLayer; Direction: TLinkDirection);
 var
  Brush: TMapBrush;
  Seek: TBaseSectionedList;
  I: Integer;
  Str: WideString;
  Event: THistoryEvent;
  Link: TBrushLayer;
 begin
  Brush := Layer.Owner as TMapBrush;

  Str := Sender.ValueStr;
  I := Pos(' -> ', Str);
  if I > 0 then
  begin
   Delete(Str, 1, I + 3);
   Seek := Brush.Extends.FindByName(Str);
  end else
   Seek := Brush.FindByName(Str);
  if (Sender.ValueStr <> '') and (Seek = nil) then
  begin
   WideMessageDlg(WideFormat('Layer ''%s'' not found', [Sender.ValueStr]), mtError, [mbOk], 0);
   Sender.Changing := True;
   I := Layer.LinkIndex[Direction];
   if I >= 0 then
    Sender.ValueStr := Sender.PickList[I] else
    Sender.ValueStr := '';
   Sender.Changing := False;
   Exit;
  end;

//  Event := FProject.History.AddEvent(Layer, '');

  Event := AddMapEvent(Brush, '');

  Layer.LinkIndex[Direction] := Sender.PickListIndex;

  Link := Layer.Links[Direction];
  if Link = nil then
   Str := 'Nothing' else
   Str := '''' + Link.Name + '''';

  Event.Text :=
    WideFormat('Brush ''%s'', layer ''%s'': %s link = %s',
                    [Brush.Name, Layer.Name, ldNames[Direction], Str]);

  //Event.RedoData := Layer;
  SetMapEventData(Event, Brush, ruRedo);
 end;

 procedure FixBrushDistances(Brush: TMapBrush; FixLayer: TMapLayer; ChangeX, ChangeY: Integer);
 var
  BrLayer: TBrushLayer;
  ld: TLinkDirection;
 begin
  BrLayer := Brush.RootNode as TBrushLayer;
  while BrLayer <> nil do
  begin
   if BrLayer <> FixLayer then
    for ld := Low(TLinkDirection) to High(TLinkDirection) do
     if BrLayer.Links[ld] = FixLayer then
      with BrLayer.DrawSpots[ld] do
      begin
       Dec(x, ChangeX);
       Dec(y, ChangeY);
      end;
   BrLayer := BrLayer.Next as TBrushLayer;
  end;
 end;

 procedure FixLayerDistances(Layer: TBrushLayer; ChangeX, ChangeY: Integer);
 var
  ld: TLinkDirection;
 begin
  for ld := Low(TLinkDirection) to High(TLinkDirection) do
   if Layer.Links[ld] <> nil then
    with Layer.DrawSpots[ld] do
    begin
     Inc(x, ChangeX);
     Inc(y, ChangeY);
    end;
 end;

var
 Map: TMap;
 Brush: TMapBrush;
 Layer: TMapLayer;
 FixDistancesResult: Integer;
 TileSetNotFound: Boolean;
 I, J, ChangeX, ChangeY, NewX, NewY: Integer;
 Str, Head: WideString;
 Event: THistoryEvent;
 Dir: TlinkDirection;
begin
 if CanAct then
 begin
  Map := MapFrame.MapData;
  if Map <> nil then
  begin
   TileSetNotFound := (Sender.UserTag = 3) and
                      (Sender.ValueStr <> '') and
                      (FProject.FMaps.TileSetList.FindByName(Sender.ValueStr) = nil);
   case Sender.UserTag of
    19, 20: FixDistancesResult := WideMessageDlg('Fix distance values?',
                                  mtConfirmation, [mbYes, mbNo, mbCancel], 0);
    else   FixDistancesResult := 0;
   end;
    
   for I := 0 to Map.Count - 1 do
    if LayersListBox.Selected[I] then
    begin
     Layer := Map.Layers[I];
     Head := WideFormat('%s ''%s'', layer ''%s'':',
            [MapTypeStr[Map is TMapBrush], Map.Name, Layer.Name]);
     case Sender.UserTag of
      1:
      with Layer do
       Flags := (Flags and not LF_VISIBLE) or
                  ((not Sender.PickListIndex + 1) and LF_VISIBLE);
      2:
      with Layer do
       Flags := (Flags and not LF_LAYER_ACTIVE) or
                  ((not Sender.PickListIndex + 1) and LF_LAYER_ACTIVE);
      3:
      with Layer do
      begin
       if TileSetNotFound then
       begin
        WideMessageDlg(WideFormat('No tile set named ''%s''',
                       [Sender.ValueStr]), mtError, [mbOk], 0);
        RefreshLayerProps;
        Exit;
       end;

       Event := AddMapEvent(Map, '');

       TileSetIndex := Sender.PickListIndex;

       if TileSet = nil then
        Str := 'Nothing' else
        Str := '''' + TileSet.Name + '''';

       Event.Text := WideFormat('%s Tile set = %s', [Head, Str]);

       SetMapEventData(Event, Map, ruRedo);
      end;
      19, 20:
      with Layer as TBrushLayer do
      begin
       if Sender.UserTag = 19 then
       begin
        NewX := Sender.Value;
        NewY := HotSpotY;
       end else
       begin
        NewX := HotSpotX;
        NewY := Sender.Value;
       end;
       case FixDistancesResult of
        mrYes:
        begin
         Event := AddMapEvent(Map,
                WideFormat('%s Hot spot X = %d, Hot spot Y = %d, fix distances',
                    [Head, NewX, NewY]));

         ChangeX := HotSpotX - NewX;
         ChangeY := HotSpotY - NewY;
         HotSpotX := NewX;
         HotSpotY := NewY;
         FixLayerDistances(TBrushLayer(Layer), ChangeX, ChangeY);
         FixBrushDistances(Owner as TMapBrush, Layer, ChangeX, ChangeY);
         Brush := FProject.FMaps.BrushList.RootNode as TMapBrush;
         while Brush <> nil do
         begin
          if Owner = Brush.Extends then
           FixBrushDistances(Brush, Layer, ChangeX, ChangeY);
          Brush := Brush.Next as TMapBrush;
         end;
         
         SetMapEventData(Event, Map, ruRedo);
        end;
        mrCancel:
        begin
         RefreshLayerProps;
         Exit;
        end;
        else
        begin
         Event := AddMapEvent(Map,
          WideFormat('%s Hot spot X = %d, Hot spot Y = %d', [Head, NewX, NewY]));
         HotSpotX := NewX;
         HotSpotY := NewY;
         SetMapEventData(Event, Map, ruRedo);
        end;
       end;
      end;
      21: CheckLinkValue(Layer as TBrushLayer, ldLeft);
      22: CheckLinkValue(Layer as TBrushLayer, ldRight);
      23: CheckLinkValue(Layer as TBrushLayer, ldUp);
      24: CheckLinkValue(Layer as TBrushLayer, ldDown);
      25: CheckLinkValue(Layer as TBrushLayer, ldLeftUp);
      26: CheckLinkValue(Layer as TBrushLayer, ldRightUp);
      27: CheckLinkValue(Layer as TBrushLayer, ldLeftDown);
      28: CheckLinkValue(Layer as TBrushLayer, ldRightDown);
      else
      begin
       Event := AddMapEvent(Map, '');

       with Layer do
        case Sender.UserTag of
         0:
         begin
          Str := Name;
          Name := Sender.ValueStr;
          Event.Text := WideFormat('%s Name = %s', [Head, Name]);
         end;
         4:
         begin
          FirstColor := Sender.Value;

          Event.Text := WideFormat('%s 1st color index = %d',
            [Head, FirstColor]);
         end;
         5:
         begin
          CellSize := Sender.Value;
          Event.Text := WideFormat('%s Cell size = %d', [Head, CellSize]);
         end;
         6:
         begin
          IndexShift := Sender.Value;
          Event.Text :=
            WideFormat('%s Tile index shift = %d',
            [Head, IndexShift]);
         end;
         7:
         begin
          IndexMask := Sender.Value;
          Event.Text :=
            WideFormat('%s Tile index mask = 0x%8.8x',
            [Head, IndexMask]);
         end;
         8:
         begin
          PaletteIndexShift := Sender.Value;
          Event.Text :=
            WideFormat('%s Palette index shift = %d',
            [Head, PaletteIndexShift]);
         end;
         9:
         begin
          PaletteIndexMask := Sender.Value;
          Event.Text :=
            WideFormat('%s Palette index mask = 0x%8.8x',
            [Head, PaletteIndexMask]);
         end;
         10:
         begin
          PriorityShift := Sender.Value;
          Event.Text :=
            WideFormat('%s Priority shift = %d',
            [Head, PriorityShift]);
         end;
         11:
         begin
          PriorityMask := Sender.Value;
          Event.Text :=
            WideFormat('%s Priority mask = 0x%8.8x',
            [Head, PriorityMask]);
         end;
         12:
         begin
          XFlipShift := Sender.Value;
          Event.Text :=
            WideFormat('%s Horizontal flip shift = %d',
            [Head, XFlipShift]);
         end;
         13:
         begin
          YFlipShift := Sender.Value;
          Event.Text :=
            WideFormat('%s Vertical flip shift = %d',
            [Head, YFlipShift]);
         end;
         14:
         begin
          DrawInfo.TileIndex := Sender.Value;
          Event.Text :=
            WideFormat('%s Draw info, Tile index = %d',
            [Head, DrawInfo.TileIndex]);
         end;
         15:
         begin
          DrawInfo.PaletteIndex := Sender.Value;
          Event.Text :=
            WideFormat('%s Draw info, Palette index = %d',
            [Head, DrawInfo.PaletteIndex]);
         end;
         16:
         begin
          DrawInfo.Priority := Sender.Value;
          Event.Text :=
            WideFormat('%s Draw info, Priority = %d',
            [Head, DrawInfo.Priority]);
         end;
         17:
         begin
          DrawInfo.XFlip := Sender.PickListIndex <> 0;
          Event.Text :=
            WideFormat('%s Draw info, Horisontal flip = %s',
            [Head, BoolToStr(DrawInfo.XFlip, True)]);
         end;
         18:
         begin
          DrawInfo.YFlip := Sender.PickListIndex <> 0;
          Event.Text :=
            WideFormat('%s Draw info, Vertical flip = %s',
            [Head, BoolToStr(DrawInfo.YFlip, True)]);
         end;
         29:
         begin
          with Layer as TBrushLayer do
           if Sender.PickListIndex = 0 then
            Unfinished := Unfinished - [ldLeft] else
            Unfinished := Unfinished + [ldLeft];

          Event.Text := WideFormat('%s Left unfinished = %s',
            [Head, BoolToStr(Sender.PickListIndex <> 0, True)]);
         end;
         30:
         begin
          with Layer as TBrushLayer do
           if Sender.PickListIndex = 0 then
            Unfinished := Unfinished - [ldRight] else
            Unfinished := Unfinished + [ldRight];

          Event.Text := WideFormat('%s Right unfinished = %s',
             [Head, BoolToStr(Sender.PickListIndex <> 0, True)]);
         end;
         31:
         begin
          with Layer as TBrushLayer do
           if Sender.PickListIndex = 0 then
            Unfinished := Unfinished - [ldUp] else
            Unfinished := Unfinished + [ldUp];

          Event.Text := WideFormat('%s Up unfinished = %s',
            [Head, BoolToStr(Sender.PickListIndex <> 0, True)]);
         end;
         32:
         begin
          with Layer as TBrushLayer do
           if Sender.PickListIndex = 0 then
            Unfinished := Unfinished - [ldDown] else
            Unfinished := Unfinished + [ldDown];

          Event.Text := WideFormat('%s Down unfinished = %s',
            [Head, BoolToStr(Sender.PickListIndex <> 0, True)]);
         end;
         33:
         begin
          with Layer as TBrushLayer do
           if Sender.PickListIndex = 0 then
            Unfinished := Unfinished - [ldLeftUp] else
            Unfinished := Unfinished + [ldLeftUp];

          Event.Text := WideFormat('%s Left/up unfinished = %s',
            [Head, BoolToStr(Sender.PickListIndex <> 0, True)]);
         end;
         34:
         begin
          with Layer as TBrushLayer do
           if Sender.PickListIndex = 0 then
            Unfinished := Unfinished - [ldRightUp] else
            Unfinished := Unfinished + [ldRightUp];

          Event.Text := WideFormat('%s Right/up unfinished = %s',
            [Head, BoolToStr(Sender.PickListIndex <> 0, True)]);
         end;
         35:
         begin
          with Layer as TBrushLayer do
           if Sender.PickListIndex = 0 then
            Unfinished := Unfinished - [ldLeftDown] else
            Unfinished := Unfinished + [ldLeftDown];

          Event.Text := WideFormat('%s Left/down unfinished = %s',
            [Head, BoolToStr(Sender.PickListIndex <> 0, True)]);
         end;
         36:
         begin
          with Layer as TBrushLayer do
           if Sender.PickListIndex = 0 then
            Unfinished := Unfinished - [ldRightDown] else
            Unfinished := Unfinished + [ldRightDown];

          Event.Text := WideFormat('%s Right/down unfinished = %s',
            [Head, BoolToStr(Sender.PickListIndex <> 0, True)]);
         end;
         37..(37 + Ord(High(TLinkDirection))):
         with Layer as TBrushLayer do
         begin
          Dir := TLinkDirection(Sender.UserTag - 37);
          with DrawSpots[Dir] do
          begin
           x := Sender.Value;

           Event.Text := WideFormat('%s %s distance x = %d',
            [Head, ldNames[Dir], x]);
          end;
         end;
         (38 + Ord(High(TLinkDirection)))..
         (38 + Ord(High(TLinkDirection)) * 2):
         with Layer as TBrushLayer do
         begin
          Dir := TLinkDirection(Sender.UserTag -
              (38 + Ord(High(TLinkDirection))));
          with DrawSpots[Dir] do
          begin
           y := Sender.Value;

           Event.Text := WideFormat('%s %s distance y = %d',
            [Head, ldNames[Dir], y]);
          end;
         end;
         54:
         begin
          with Layer as TBrushLayer do
           if Sender.PickListIndex = 0 then
            Extended := Extended - [ldLeft] else
            Extended := Extended + [ldLeft];

          Event.Text := WideFormat('%s Left extended = %s',
            [Head, BoolToStr(Sender.PickListIndex <> 0, True)]);
         end;
         55:
         begin
          with Layer as TBrushLayer do
           if Sender.PickListIndex = 0 then
            Extended := Extended - [ldRight] else
            Extended := Extended + [ldRight];

          Event.Text := WideFormat('%s Right extended = %s',
            [Head, BoolToStr(Sender.PickListIndex <> 0, True)]);
         end;
         56:
         begin
          with Layer as TBrushLayer do
           if Sender.PickListIndex = 0 then
            Extended := Extended - [ldUp] else
            Extended := Extended + [ldUp];

          Event.Text := WideFormat('%s Up extended = %s',
            [Head, BoolToStr(Sender.PickListIndex <> 0, True)]);
         end;
         57:
         begin
          with Layer as TBrushLayer do
           if Sender.PickListIndex = 0 then
            Extended := Extended - [ldDown] else
            Extended := Extended + [ldDown];

          Event.Text := WideFormat('%s Down extended = %s',
            [Head, BoolToStr(Sender.PickListIndex <> 0, True)]);
         end;
         58:
         begin
          with Layer as TBrushLayer do
           if Sender.PickListIndex = 0 then
            Extended := Extended - [ldLeftUp] else
            Extended := Extended + [ldLeftUp];

          Event.Text := WideFormat('%s Left/up extended = %s',
            [Head, BoolToStr(Sender.PickListIndex <> 0, True)]);
         end;
         59:
         begin
          with Layer as TBrushLayer do
           if Sender.PickListIndex = 0 then
            Extended := Extended - [ldRightUp] else
            Extended := Extended + [ldRightUp];

          Event.Text := WideFormat('%s Right/up extended = %s',
            [Head, BoolToStr(Sender.PickListIndex <> 0, True)]);
         end;
         60:
         begin
          with Layer as TBrushLayer do
           if Sender.PickListIndex = 0 then
            Extended := Extended - [ldLeftDown] else
            Extended := Extended + [ldLeftDown];

          Event.Text := WideFormat('%s Left/down extended = %s',
            [Head, BoolToStr(Sender.PickListIndex <> 0, True)]);
         end;
         61:
         begin
          with Layer as TBrushLayer do
           if Sender.PickListIndex = 0 then
            Extended := Extended - [ldRightDown] else
            Extended := Extended + [ldRightDown];

          Event.Text := WideFormat('%s Right/down extended = %s',
            [Head, BoolToStr(Sender.PickListIndex <> 0, True)]);
         end;
         62:
         with Layer as TBrushLayer do
         begin
          IdentityTag := Sender.Value;

          Event.Text := WideFormat('%s Identity tag = %d', [Head, IdentityTag]);
         end;
         63:
         with Layer as TBrushLayer do
         begin
          Randomize := Sender.PickListIndex <> 0;

          Event.Text := WideFormat('%s Randomize = %s', [Head,
          BoolToStr(Randomize, True)]);
         end;
        end;

       SetMapEventData(Event, Map, ruRedo);
      end;
     end;
    end;

   case Sender.UserTag of
    0..2: RefreshMapContent(True);
    4..13:
    begin
     MapModified;
     RefreshMapContent(True);     
    end;
    else LayersModified;
   end;
  end;
 end;
end;

procedure TMapsFrame.MapPropertiesValueChange(Sender: TPropertyListItem);
var
 I: Integer;
 Map: TMap;
 Brush: TMapBrush;
 Event: THistoryEvent;
 Str: WideString;
begin
 if CanAct then
  with MapList do
   case Sender.UserTag of
    TAG_MAPNAME:
    begin
     for I := 0 to Count - 1 do
      if MapsListBox.Selected[I] then
      begin
       Map := Nodes[I] as TMap;

       Str := Map.Name;
//       Event := FProject.History.AddEvent(Map, '');
       Event := AddMapEvent(Map, '');

       Map.Name := Sender.ValueStr;

       Event.Text := WideFormat('%s ''%s'': Name = ''%s''',
                      [MapTypeStr[BrushMode], Str, Map.Name]);
       //Event.RedoData := Map;
       SetMapEventData(Event, Map, ruRedo);
      end;

     MapNameModified;
    end; // TAG_MAPNAME

    TAG_BRSHEXT:
    if BrushMode then
    begin
     if (Sender.ValueStr <> '') and
        (FProject.FMaps.BrushList.FindByName(Sender.ValueStr) = nil) then
     begin
      WideMessageDlg(WideFormat('No brush named ''%s''', [Sender.ValueStr]), mtError, [mbOk], 0);
      RefreshMapProps;
      Exit;
     end;
     
     for I := 0 to Count - 1 do
      if MapsListBox.Selected[I] then
      begin
       Brush := Nodes[I] as TMapBrush;

       Event := AddMapEvent(Brush, '');
       //FProject.History.AddEvent(Brush, '');

       Brush.ExtendsIndex := Sender.PickListIndex;

       with Brush do
        if Extends = nil then
         Str := 'Nothing' else
         Str := 'brush ''' + Extends.Name + '''';       

       Event.Text := WideFormat('Brush ''%s'': Extends brush = %s', [Brush.Name, Str]);
       //Event.RedoData := Brush;
       SetMapEventData(Event, Brush, ruRedo);
      end;
        
     RefreshMapProps;

     LayerSelected;
    end;  // TAG_BRSHEXT
    else   //////////////
    begin
     case Sender.UserTag of
      TAG_MAPCOLR:
      begin
       if (Sender.ValueStr <> '') and
          (FProject.FMaps.ColorTableList.FindByName(Sender.ValueStr) = nil) then
       begin
        WideMessageDlg(WideFormat('No color table named ''%s''',
                        [Sender.ValueStr]), mtError, [mbOk], 0);
        RefreshMapProps;
        Exit;
       end;
       for I := 0 to Count - 1 do
        if MapsListBox.Selected[I] then
        begin
         Map := Nodes[I] as TMap;

         //Event := FProject.History.AddEvent(Map, '');

         Event := AddMapEvent(Map, '');

         Map.ColorTableIndex := Sender.PickListIndex;

         with Map do
          if ColorTable = nil then
           Str := 'Nothing' else
           Str := '''' + ColorTable.Name + '''';         

         Event.Text := WideFormat('%s ''%s'': Color table = %s',
                     [MapTypeStr[BrushMode], Map.Name, Str]);
         //Event.RedoData := Map;
         SetMapEventData(Event, Map, ruRedo);
        end;

       RefreshColorTable;
      end; // TAG_MAPCOLR

      TAG_MAPWDTH:
      for I := 0 to Count - 1 do
       if MapsListBox.Selected[I] then
       begin
        Map := Nodes[I] as TMap;

        Event := AddMapEvent(Map, '');
        //FProject.History.AddEvent(Map, '');

        Map.Width := Sender.Value;
        Event.Text := WideFormat('%s ''%s'': Width = %d',
                                     [MapTypeStr[BrushMode],
                                     Map.Name, Map.Width]);
        //Event.RedoData := Map;
        SetMapEventData(Event, Map, ruRedo);
       end;

      TAG_MAPHGHT:
      for I := 0 to Count - 1 do
       if MapsListBox.Selected[I] then
       begin
        Map := Nodes[I] as TMap;

        Event := AddMapEvent(Map, '');
        //FProject.History.AddEvent(Map, '');
        Map.Height := Sender.Value;
        Event.Text := WideFormat('%s ''%s'': Height = %d',
                                     [MapTypeStr[BrushMode],
                                     Map.Name, Map.Height]);
        //Event.RedoData := Map;
        SetMapEventData(Event, Map, ruRedo);
       end;

      TAG_MAPTLWD:
      begin
       for I := 0 to Count - 1 do
        if MapsListBox.Selected[I] then
        begin
         Map := Nodes[I] as TMap;

         Event := AddMapEvent(Map, '');
         //FProject.History.AddEvent(Map, '');

         with Map do
         begin
          TileWidth := Sender.Value;
          Width := Min(Width, 32767 div TileWidth);

          (Sender.Owner as TPropertyList).Properties[1].Value := Width;
         end;

         Event.Text := WideFormat('%s ''%s'': Tile width = %d',
                                     [MapTypeStr[BrushMode],
                                     Map.Name, Map.TileWidth]);

         //Event.RedoData := Map;
         SetMapEventData(Event, Map, ruRedo);
        end;

       MapPropEditor.Invalidate;
      end;
      
      TAG_MAPTLHT:
      begin
       for I := 0 to Count - 1 do
        if MapsListBox.Selected[I] then
        begin
         Map := Nodes[I] as TMap;

         Event := AddMapEvent(Map, '');
         //FProject.History.AddEvent(Map, '');

         with Map do
         begin
          TileHeight := Sender.Value;
          Height := Min(Height, 32767 div TileHeight);

          (Sender.Owner as TPropertyList).Properties[2].Value := Height;
         end;

         Event.Text := WideFormat('%s ''%s'': Tile height = %d',
                                     [MapTypeStr[BrushMode],
                                     Map.Name, Map.TileHeight]);

         //Event.RedoData := Map;
         SetMapEventData(Event, Map, ruRedo);
        end;

       MapPropEditor.Invalidate;
      end;
     end;
     MapModified;
     MapPropsModified;
    end;
   end;
end;

procedure TMapsFrame.MapFrameGetLayerActive(Sender: TMapFrame;
  Layer: TMapLayer; var Active: Boolean);
var
 I: Integer;
begin
 if not Active then
  with Sender.MapData do
  begin
   for I := 0 to Count - 1 do
    if Layers[I].Flags and LF_LAYER_ACTIVE <> 0 then Exit;

   if Sender.BrushMode then
    Active := Layer = Layers[FProject.FBrushLayerFocus[Index]] else
    Active := Layer = Layers[FProject.FMapLayerFocus[Index]]
  end;
end;

function TMapsFrame.GetMapIndex: Integer;
begin
 if FProject <> nil then
 begin
  if FBrushMode then
   Result := FProject.FInfo.SelectedBrushIndex[Tag] else
   Result := FProject.FInfo.SelectedMapIndex[Tag];
  with MapList do
   if Result >= Count then
    Result := Count - 1;
 end else
  Result := -1;
end;

function TMapsFrame.GetMapList: TBaseMapList;
begin
 if FProject <> nil then
 begin
  Result := FProject.FMaps;
  if FBrushMode then
   Result := TMapList(Result).BrushList;
 end else
  Result := nil;
end;

function TMapsFrame.GetMapScale: Integer;
begin
 if FProject <> nil then
 begin
  if FBrushMode then
   Result := FProject.FInfo.Scales.scBrushFrame[Tag] else
   Result := FProject.FInfo.Scales.scMapFrame[Tag]
 end else
  Result := 0;
end;

procedure TMapsFrame.RefreshColorTable(DoRepaint: Boolean);
var
 Map: TMap;
begin
 MapFrame.RefreshColorTable(DoRepaint);
 Map := MapFrame.MapData;
 if (Map <> nil) and (Map.ColorTable <> nil) then
  with Map.ColorTable do
   ColorFormat.ConvertToRGBQuad(ColorData, Addr(FDrawTileHeader.bhPalette), ColorsCount) else
   FillChar(FDrawTileHeader.bhPalette, SizeOf(TRGBQuads), 0);
 if DoRepaint then
  LayersListBox.Invalidate;
end;

procedure TMapsFrame.RefreshMapProps;
begin
 PostMessage(Handle, WM_REFRESHMAPPROPS, 0, 0);
end;

procedure TMapsFrame.RefreshLayerProps;
begin
 PostMessage(Handle, WM_REFRESHLAYERPROPS, 0, 0);
end;

procedure TMapsFrame.WMRefreshLayerProps(var Message: TMessage);
var
 ld: TLinkDirection;
 I: Integer;
 UseLayer, Layer: TMapLayer;
 BLayer: TBrushLayer absolute Layer;
 BUseLayer: TBrushLayer absolute UseLayer;
 VFlags: packed record Lo, Hi: LongInt end;
 VFlags64: Int64 absolute VFlags;

 ln: WideString;
 List: TTntStringList;
 ptp: ^TSmallPoint;

 Map: TMap;

begin
 LayersLabel.Caption := 'Layer properties';

 with LayerPropEditor, PropertyList do
 begin
  ClearList;
  if (FProject <> nil) and
     (PageControl.ActivePageIndex = 0) then
  begin
   Map := MapFrame.MapData;
   if (Map <> nil) and (Map.Count > 0) then
   begin
    if LayersListBox.SelCount = 0 then
     LayersListBox.Selected[LayerIndex] := True;

    VFlags64 := 0;
    UseLayer := nil;
    Layer := nil;
    for I := 0 to Map.Count - 1 do
     if LayersListBox.Selected[I] then
     begin
      Layer := Map.Layers[I];
      if UseLayer <> nil then
      begin
       if Layer.Name <> UseLayer.Name then
        VFlags.Lo := VFlags.Lo or (1 shl 0);
       if Layer.Flags and LF_VISIBLE <> UseLayer.Flags and LF_VISIBLE then
        VFlags.Lo := VFlags.Lo or (1 shl 1);
       if Layer.Flags and LF_LAYER_ACTIVE <> UseLayer.Flags and LF_LAYER_ACTIVE then
        VFlags.Lo := VFlags.Lo or (1 shl 2);
       if Layer.TileSet <> UseLayer.TileSet then
        VFlags.Lo := VFlags.Lo or (1 shl 3);
       if Layer.FirstColor <> UseLayer.FirstColor then
        VFlags.Lo := VFlags.Lo or (1 shl 4);
       if Layer.CellSize <> UseLayer.CellSize then
        VFlags.Lo := VFlags.Lo or (1 shl 5);
       if Layer.IndexShift <> UseLayer.IndexShift then
        VFlags.Lo := VFlags.Lo or (1 shl 6);
       if Layer.IndexMask <> UseLayer.IndexMask then
        VFlags.Lo := VFlags.Lo or (1 shl 7);
       if Layer.PaletteIndexShift <> UseLayer.PaletteIndexShift then
        VFlags.Lo := VFlags.Lo or (1 shl 8);
       if Layer.PaletteIndexMask <> UseLayer.PaletteIndexMask then
        VFlags.Lo := VFlags.Lo or (1 shl 9);
       if Layer.PriorityShift <> UseLayer.PriorityShift then
        VFlags.Lo := VFlags.Lo or (1 shl 10);
       if Layer.PriorityMask <> UseLayer.PriorityMask then
        VFlags.Lo := VFlags.Lo or (1 shl 11);
       if Layer.XFlipShift <> UseLayer.XFlipShift then
        VFlags.Lo := VFlags.Lo or (1 shl 12);
       if Layer.YFlipShift <> UseLayer.YFlipShift then
        VFlags.Lo := VFlags.Lo or (1 shl 13);
       if Layer.Flags and LF_TILE_SELECTED = 0 then
        Layer.DrawInfo.TileIndex := -1;
       if UseLayer.Flags and LF_TILE_SELECTED = 0 then
        UseLayer.DrawInfo.TileIndex := -1;
       if (Layer.DrawInfo.TileIndex <> UseLayer.DrawInfo.TileIndex) then
        VFlags.Lo := VFlags.Lo or (1 shl 14);
       if Layer.DrawInfo.PaletteIndex <> UseLayer.DrawInfo.PaletteIndex then
        VFlags.Lo := VFlags.Lo or (1 shl 15);
       if Layer.DrawInfo.Priority <> UseLayer.DrawInfo.Priority then
        VFlags.Lo := VFlags.Lo or (1 shl 16);
       if Layer.DrawInfo.XFlip <> UseLayer.DrawInfo.XFlip then
        VFlags.Lo := VFlags.Lo or (1 shl 17);
       if Layer.DrawInfo.YFlip <> UseLayer.DrawInfo.YFlip then
        VFlags.Lo := VFlags.Lo or (1 shl 18);
       if Map is TMapBrush then
       begin
        if BLayer.IdentityTag <> BUseLayer.IdentityTag then
         VFlags.Hi := VFlags.Hi or (1 shl (62 - 32));

        if BLayer.Randomize <> BUseLayer.Randomize then
         VFlags.Hi := VFlags.Hi or (1 shl (63 - 32));

        if BLayer.HotSpotX <> BUseLayer.HotSpotX then
         VFlags.Lo := VFlags.Lo or (1 shl 19);
        if BLayer.HotSpotY <> BUseLayer.HotSpotY then
         VFlags.Lo := VFlags.Lo or (1 shl 20);
        if (BLayer.LinkLeft <> BUseLayer.LinkLeft) then
         VFlags.Lo := VFlags.Lo or (1 shl 21);
        if (BLayer.LinkRight <> BUseLayer.LinkRight) then
         VFlags.Lo := VFlags.Lo or (1 shl 22);
        if (BLayer.LinkUp <> BUseLayer.LinkUp) then
         VFlags.Lo := VFlags.Lo or (1 shl 23);
        if (BLayer.LinkDown <> BUseLayer.LinkDown) then
         VFlags.Lo := VFlags.Lo or (1 shl 24);
        if (BLayer.LinkLeftUp <> BUseLayer.LinkLeftUp) then
         VFlags.Lo := VFlags.Lo or (1 shl 25);
        if (BLayer.LinkRightUp <> BUseLayer.LinkRightUp) then
         VFlags.Lo := VFlags.Lo or (1 shl 26);
        if (BLayer.LinkLeftDown <> BUseLayer.LinkLeftDown) then
         VFlags.Lo := VFlags.Lo or (1 shl 27);
        if (BLayer.LinkRightDown <> BUseLayer.LinkRightDown) then
         VFlags.Lo := VFlags.Lo or (1 shl 28);

        if (ldLeft in BLayer.Unfinished) <>
           (ldLeft in BUseLayer.Unfinished) then
         VFlags.Lo := VFlags.Lo or (1 shl 29);
        if (ldRight in BLayer.Unfinished) <>
           (ldRight in BUseLayer.Unfinished) then
         VFlags.Lo := VFlags.Lo or (1 shl 30);
        if (ldUp in BLayer.Unfinished) <>
           (ldUp in BUseLayer.Unfinished) then
         VFlags.Lo := VFlags.Lo or (1 shl 31);
        if (ldDown in BLayer.Unfinished) <>
           (ldDown in BUseLayer.Unfinished) then
         VFlags.Hi := VFlags.Hi or (1 shl 0);
        if (ldLeftUp in BLayer.Unfinished) <>
           (ldLeftUp in BUseLayer.Unfinished) then
         VFlags.Hi := VFlags.Hi or (1 shl 1);
        if (ldRightUp in BLayer.Unfinished) <>
           (ldRightUp in BUseLayer.Unfinished) then
         VFlags.Hi := VFlags.Hi or (1 shl 2);
        if (ldLeftDown in BLayer.Unfinished) <>
           (ldLeftDown in BUseLayer.Unfinished) then
         VFlags.Hi := VFlags.Hi or (1 shl 3);
        if (ldRightDown in BLayer.Unfinished) <>
           (ldRightDown in BUseLayer.Unfinished) then
         VFlags.Hi := VFlags.Hi or (1 shl 4);

         if (ldLeft in BLayer.Extended) <>
           (ldLeft in BUseLayer.Extended) then
         VFlags.Hi := VFlags.Hi or (1 shl (54 - 32));
        if (ldRight in BLayer.Extended) <>
           (ldRight in BUseLayer.Extended) then
         VFlags.Hi := VFlags.Hi or (1 shl (55 - 32));
        if (ldUp in BLayer.Extended) <>
           (ldUp in BUseLayer.Extended) then
         VFlags.Hi := VFlags.Hi or (1 shl (56 - 32));
        if (ldDown in BLayer.Extended) <>
           (ldDown in BUseLayer.Extended) then
         VFlags.Hi := VFlags.Hi or (1 shl (57 - 32));
        if (ldLeftUp in BLayer.Extended) <>
           (ldLeftUp in BUseLayer.Extended) then
         VFlags.Hi := VFlags.Hi or (1 shl (58 - 32));
        if (ldRightUp in BLayer.Extended) <>
           (ldRightUp in BUseLayer.Extended) then
         VFlags.Hi := VFlags.Hi or (1 shl (59 - 32));
        if (ldLeftDown in BLayer.Extended) <>
           (ldLeftDown in BUseLayer.Extended) then
         VFlags.Hi := VFlags.Hi or (1 shl (60 - 32));
        if (ldRightDown in BLayer.Extended) <>
           (ldRightDown in BUseLayer.Extended) then
         VFlags.Hi := VFlags.Hi or (1 shl (61 - 32));
        for ld := Low(TLinkDirection) to High(TLinkDirection) do
        begin
         ptp := Addr(BUseLayer.DrawSpots[ld]);
         with BLayer.DrawSpots[ld] do
         begin
          if x <> ptp.x then
           VFlags.Hi := VFlags.Hi or (1 shl (5 + Ord(ld)));
          if y <> ptp.y then
           VFlags.Hi := VFlags.Hi or (1 shl (6 + Ord(High(TLinkDirection)) + Ord(ld)));
         end;
        end;
       end;
      end else
       UseLayer := Layer;
     end;

    if UseLayer <> nil then
     with UseLayer do
     begin

      if UseLayer = Layer then
       LayersLabel.Caption := WideFormat('%s #%d, layer #%d',
       [MapTypeStr[BrushMode], Map.Index + 1, UseLayer.Index + 1]) else
       LayersLabel.Caption := WideFormat('%s #%d selected layers',
       [MapTypeStr[BrushMode], Map.Index + 1]);

      with AddString('Name', Name) do
      begin
       if VFlags.Lo and 1 <> 0 then
        Parameters := PL_MULTIPLE_VALUE;
       UserTag := 0;
      end;

      List := TTntStringList.Create;
      try
       with FProject.FMaps.TileSetList do
        for I := 0 to Count - 1 do
         List.Add(Sets[I].Name);

       List.CaseSensitive := True;
       with AddPickList('Tile set', List, False, TileSetIndex, False) do
       begin
        if VFlags.Lo and (1 shl 3) <> 0 then
         Parameters := PL_MULTIPLE_VALUE;
        UserTag := 3;
       end;
      finally
       List.Free;
      end;

      if Map is TMapBrush then
      begin
       with AddString('Brush data', '(...)', False).SubProperties do
       begin
        with AddDecimal('Hot spot X', BUseLayer.HotSpotX, Low(SmallInt), High(SmallInt)) do
        begin
         if VFlags.Lo and (1 shl 19) <> 0 then
          Parameters := PL_MULTIPLE_VALUE;
         UserTag := 19;
        end;
        with AddDecimal('Hot spot Y', BUseLayer.HotSpotY, Low(SmallInt), High(SmallInt)) do
        begin
         if VFlags.Lo and (1 shl 20) <> 0 then
          Parameters := PL_MULTIPLE_VALUE;
         UserTag := 20;
        end;

        with AddDecimal('Identity tag', BUseLayer.IdentityTag, Low(LongInt), MaxInt) do
        begin
         if VFlags.Hi and (1 shl (62 - 32)) <> 0 then
          Parameters := PL_MULTIPLE_VALUE;
         UserTag := 62;
        end;

        with AddBoolean('Randomize', BUseLayer.Randomize) do
        begin
         if VFlags.Hi and (1 shl (63 - 32)) <> 0 then
          Parameters := Parameters or PL_MULTIPLE_VALUE;
         UserTag := 63;
        end;

        List := TTntStringList.Create;
        try
         for I := 0 to Map.Count - 1 do
           List.Add(Map.Layers[I].Name);

         with Map as TMapBrush do
          if Extends <> nil then
           for I := 0 to Extends.Count - 1 do
            List.Add(WideFormat('%s -> %s', [Extends.Name, Extends.Layers[I].Name]));

         List.CaseSensitive := True;
         for ld := Low(TLinkDirection) to High(TLinkDirection) do
         begin
          with AddBoolean(WideFormat('%s unfinished', [ldNames[ld]]), ld in BUseLayer.Unfinished) do
          begin
           UserTag := 29 + Ord(ld);
           if VFlags64 and (Int64(1) shl UserTag) <> 0 then
            Parameters := Parameters or PL_MULTIPLE_VALUE;
          end;
          with AddPickList(WideFormat('%s link', [ldNames[ld]]), List, False,
               BUseLayer.LinkIndex[ld], False) do
          begin
           UserTag := 21 + Ord(ld);
           if VFlags.Lo and (1 shl UserTag) <> 0 then
            Parameters := Parameters or PL_MULTIPLE_VALUE;
          end;

          with AddBoolean('  Extended', ld in BUseLayer.Extended) do
          begin
           UserTag := 54 + Ord(ld);
           if VFlags64 and (Int64(1) shl UserTag) <> 0 then
            Parameters := Parameters or PL_MULTIPLE_VALUE;
          end;

          ptp := Addr(BUseLayer.DrawSpots[ld]);
          with AddDecimal('  Distance X', ptp.x, Low(SmallInt), High(SmallInt)) do
          begin
           I := 5 + Ord(ld);
           if VFlags.Hi and (1 shl I) <> 0 then
            Parameters := PL_MULTIPLE_VALUE;
           UserTag := 32 + I;
          end;
          with AddDecimal('  Distance Y', ptp.y, Low(SmallInt), High(SmallInt)) do
          begin
           I := 6 + Ord(High(TLinkDirection)) + Ord(ld);
           if VFlags.Hi and (1 shl I) <> 0 then
            Parameters := PL_MULTIPLE_VALUE;
           UserTag := 32 + I;
          end;
         end;
        finally
         List.Free;
        end;
       end;
      end;

      with AddBoolean('Visible', Flags and LF_VISIBLE <> 0) do
      begin
       if VFlags.Lo and (1 shl 1) <> 0 then
        Parameters := Parameters or PL_MULTIPLE_VALUE;
       UserTag := 1;
      end;

      with AddBoolean('Drawing enabled', Flags and LF_LAYER_ACTIVE <> 0) do
      begin
       if VFlags.Lo and (1 shl 2) <> 0 then
           Parameters := Parameters or PL_MULTIPLE_VALUE;
       UserTag := 2;
       with SubProperties do
       begin

        with AddDecimal('Tile index', DrawInfo.TileIndex, -1, MaxInt) do
        begin
         if VFlags.Lo and (1 shl 14) <> 0 then
          Parameters := PL_MULTIPLE_VALUE;
         UserTag := 14;
        end;

        with AddDecimal('Palette index', DrawInfo.PaletteIndex, -1, MaxInt) do
        begin
         if VFlags.Lo and (1 shl 15) <> 0 then
          Parameters := PL_MULTIPLE_VALUE;
         UserTag := 15;
        end;

        with AddDecimal('Priority', DrawInfo.Priority, -1, MaxInt) do
        begin
         if VFlags.Lo and (1 shl 16) <> 0 then
          Parameters := PL_MULTIPLE_VALUE;
         UserTag := 16;
        end;

        with AddBoolean('Horizontal flip', DrawInfo.XFlip) do
        begin
         if VFlags.Lo and (1 shl 17) <> 0 then
          Parameters := Parameters or PL_MULTIPLE_VALUE;
         UserTag := 17;
        end;

        with AddBoolean('Vertical flip', DrawInfo.YFlip) do
        begin
         if VFlags.Lo and (1 shl 18) <> 0 then
          Parameters := Parameters or PL_MULTIPLE_VALUE;
         UserTag := 18;
        end;
       end;
      end;

      with AddString('Layer format', '(...)') do
      begin
       ReadOnly := True;
       with SubProperties do
       begin

        with AddDecimal('Cell size', CellSize, 0, 8) do
        begin
         if VFlags.Lo and (1 shl 5) <> 0 then
          Parameters := PL_MULTIPLE_VALUE;
         UserTag := 5;
        end;

        with AddDecimal('Tile index shift', IndexShift, 0, 63) do
        begin
         if VFlags.Lo and (1 shl 6) <> 0 then
          Parameters := PL_MULTIPLE_VALUE;
         UserTag := 6;
        end;

        with AddHexadecimal('Tile index mask', IndexMask, 0, High(LongWord), 8) do
        begin
         if VFlags.Lo and (1 shl 7) <> 0 then
          Parameters := PL_MULTIPLE_VALUE;
         UserTag := 7;
        end;

        with AddDecimal('Palette index shift', PaletteIndexShift, -1, 63) do
        begin
         if VFlags.Lo and (1 shl 8) <> 0 then
          Parameters := PL_MULTIPLE_VALUE;
         UserTag := 8;
        end;

        with AddHexadecimal('Palette index mask', PaletteIndexMask, 0, High(LongWord), 8) do
        begin
         if VFlags.Lo and (1 shl 9) <> 0 then
          Parameters := PL_MULTIPLE_VALUE;
         UserTag := 9;
        end;

        with AddDecimal('Priority shift', PriorityShift, -1, 63) do
        begin
         if VFlags.Lo and (1 shl 10) <> 0 then
          Parameters := PL_MULTIPLE_VALUE;
         UserTag := 10;
        end;

        with AddHexadecimal('Priority mask', PriorityMask, 0, High(LongWord), 8) do
        begin
         if VFlags.Lo and (1 shl 11) <> 0 then
          Parameters := PL_MULTIPLE_VALUE;
         UserTag := 11;
        end;

        with AddDecimal('Horizontal flip shift', XFlipShift, -1, 63) do
        begin
         if VFlags.Lo and (1 shl 12) <> 0 then
          Parameters := PL_MULTIPLE_VALUE;
         UserTag := 12;
        end;

        with AddDecimal('Vertical flip shift', YFlipShift, -1, 63) do
        begin
         if VFlags.Lo and (1 shl 13) <> 0 then
          Parameters := PL_MULTIPLE_VALUE;
         UserTag := 13;
        end;
       end;
      end;

      with AddDecimal('1st color index', FirstColor, 0, 255) do
      begin
       if VFlags.Lo and (1 shl 4) <> 0 then
         Parameters := PL_MULTIPLE_VALUE;
       UserTag := 4;
      end;
     end;
   end;
  end;
  RootNodeCount := Count;
 end;
end;

procedure TMapsFrame.LayersListBoxClick(Sender: TObject);
var
 I, Idx: Integer;
begin
 with LayersListBox do
 begin
  Idx := ItemIndex;
  if (Count > 0) and not Selected[Idx] then
   for I := 0 to Count - 1 do
    if Selected[I] then
    begin
     Idx := I;
     ItemIndex := I;     
     Break;;
    end;
 end;
 LayerIndex := Idx;
 LayerSelected;
end;

function TMapsFrame.LayersListBoxDataFind(Control: TWinControl;
  FindString: String): Integer;
var
 Map: TMap;
begin
 Map := MapFrame.MapData;
 if Map <> nil then
  Result := Map.FindIndexByFirstLetters(FindString) else
  Result := -1;
end;

procedure TMapsFrame.LayersListBoxDrawPic(Sender: TObject; AIndex: Integer;
  Rect: TRect; State: TOwnerDrawState; var Handled: Boolean);
var
 Map: TMap;
 Layer: TMapLayer;
 Tile: TTileItem;
 Cvt: TBitmapConverter;
begin
 Map := MapFrame.MapData;
 if Map <> nil then
  with Sender as TLayerListBox do
  begin
   Layer := Map.Layers[AIndex];
   if (Layer <> nil) and (Layer.TileSet <> nil) and
      (Layer.Flags and LF_TILE_SELECTED <> 0) then
   begin
    with Layer.DrawInfo do
    begin
     Tile := Layer.TileSet.Indexed[TileIndex];
     if Tile <> nil then
      with Layer.TileSet do
      begin
       FillPicHeader(TileWidth, TileHeight, FDrawTileHeader, FDrawTileBmp);

       if MapFrame.Converters = nil then
        MapFrame.FillConverters(MapList);

       Cvt := MapFrame.Converters[Index].tcvtDraw[XFlip, YFlip];
       if Cvt <> nil then
       begin
        Cvt.SrcInfo.PixelModifier := Layer.FirstColor + Tile.FirstColor;

        FillChar(FDrawTileBmp.ImageData^,
                 FDrawTileHeader.bhHead.bhInfoHeader.biSizeImage,
                 Integer(TransparentColor) + Cvt.SrcInfo.PixelModifier);

        TileDraw(Tile.TileData^, FDrawTileBmp, 0, 0, Cvt);

        Inc(Rect.Left);
        Dec(Rect.Right);
        Inc(Rect.Top);
        Dec(Rect.Bottom);
        DrawPic(Canvas.Handle, Rect, FDrawTileHeader, FDrawTileBmp.ImageData);
        Handled := True;
       end;
      end;
    end;
   end;
  end;
end;

procedure TMapsFrame.LayersListBoxGetItemClickMode(Sender: TObject;
  AIndex: Integer; var AModes: TItemClickModes);
var
 Map: TMap;
 Layer: TMapLayer;
begin
 Map := MapFrame.MapData;
 if Map <> nil then
 begin
  Layer := Map.Layers[AIndex];
  if Layer <> nil then
  begin
   if Layer.Flags and LF_LAYER_ACTIVE <> 0 then
    Include(AModes, llbChecked);
   if Layer.Flags and LF_VISIBLE <> 0 then
    Include(AModes, llbVisible);
   if (Layer.XFlipShift < 0) and (Layer.YFlipShift < 0) then
    Include(AModes, llbFlipsDisabled) else
   begin
    if Layer.DrawInfo.XFlip then
     Include(AModes, llbXFlip);
    if Layer.DrawInfo.YFlip then
     Include(AModes, llbYFlip);
   end;
  end;
 end else
  Include(AModes, llbFlipsDisabled);
end;

procedure TMapsFrame.LayersListBoxItemClick(Sender: TObject;
  AIndex: Integer; ChangedMode: TItemClickMode);
var
 Map: TMap;
 Layer: TMapLayer;
begin
 Map := MapFrame.MapData;
 if Map <> nil then
 begin
  Layer := Map.Layers[AIndex];
  if Layer <> nil then
  begin
   case ChangedMode of
    llbChecked: Layer.Flags := Layer.Flags xor LF_LAYER_ACTIVE;
    llbVisible: Layer.Flags := Layer.Flags xor LF_VISIBLE;
    llbXFlip:   Layer.DrawInfo.XFlip := not Layer.DrawInfo.XFlip;
    llbYFlip:   Layer.DrawInfo.YFlip := not Layer.DrawInfo.YFlip;
    llbTile:
    if ssDouble in GlobalShiftState then
    begin
     if Assigned(FOnLayerDblClick) then
      FOnLayerDblClick(Self, Layer, True);
     Exit;
    end;
   end;
   if LayersListBox.Selected[AIndex] then
    RefreshLayerProps;
   RefreshMapContent(True);
  end;
 end;
end;

function TMapsFrame.GetLayerIndex: Integer;
var
 Map: TMap;
begin
 if FProject <> nil then
 begin
  Map := MapFrame.MapData;
  if Map <> nil then
  begin
   if FBrushMode then
    Result := FProject.FBrushLayerFocus[Map.Index] else
    Result := FProject.FMapLayerFocus[Map.Index];
   if Result >= Map.Count then
    Result := Map.Count - 1;
   Exit; 
  end;
 end;
 Result := -1;
end;

destructor TMapsFrame.Destroy;
var
 P: Pointer;
begin
 P := FDrawTileBmp.ImageData;
 FreeMem(P);
 FDrawTileBmp.Free;
 inherited;
end;

procedure TMapsFrame.SetLayerIndex(Value: Integer);
var
 Map: TMap;
begin
 if FProject <> nil then
 begin
  Map := MapFrame.MapData;
  if Map <> nil then
  begin
   if FBrushMode then
    FProject.FBrushLayerFocus[Map.Index] := Value else
    FProject.FMapLayerFocus[Map.Index] := Value;
  end;
 end;
end;

procedure TMapsFrame.SetMapIndex(Value: Integer);
begin
 if FProject <> nil then
 begin
  if FBrushMode then
   FProject.FInfo.SelectedBrushIndex[Tag] := Value else
   FProject.FInfo.SelectedMapIndex[Tag] := Value;
 end;
end;

procedure TMapsFrame.SetMapScale(Value: Integer);
begin
 if FProject <> nil then
 begin
  if FBrushMode then
   FProject.FInfo.Scales.scBrushFrame[Tag] := Value else
   FProject.FInfo.Scales.scMapFrame[Tag] := Value;
 end;
end;

procedure TMapsFrame.MapsComboBoxChange(Sender: TObject);
var
 I: Integer;
begin
 I := MapsComboBox.ItemIndex;
 MapIndex := I;

 MapsListBox.ClearSelection;
 MapsListBox.ItemIndex := I;
 MapsListBox.Selected[I] := True;
 MapSelected;
 MapComboSelected;
end;

procedure TMapsFrame.RefreshMapContent(Layers: Boolean);
begin
 MapFrame.MapEditView.Invalidate;

 if Layers then
  LayersListBox.Invalidate;

 if Assigned(FOnRefreshMapContent) then
  FOnRefreshMapContent(Self, Layers);
end;

procedure TMapsFrame.LayerSelected;
var
 Map: TMap;
begin
 RefreshLayerProps;
 Map := MapFrame.MapData;
 if (Map <> nil) and Assigned(FOnLayerSelected) then
  FOnLayerSelected(Self, Map.Nodes[LayerIndex] as TMapLayer);
end;

procedure TMapsFrame.LayersModified;
begin
 FProject.FMaps.BrushList.CleanUpTails;
 
 if Assigned(FOnLayersModified) then
  FOnLayersModified(Self);

 RefreshLayersList(False);

 MapFrame.MapEditView.Invalidate;
 MapSelectionChanged;
end;

procedure TMapsFrame.RefreshLists;
var
 I: Integer;
begin
 if FProject <> nil then
 begin
  with MapList do
   if Count > 0 then
   begin
    if MapsListBox.Count <> Count then
     MapsListBox.Count := Count else
     MapsListBox.Invalidate;
    I := MapIndex;
    if I < 0 then
    begin
     I := 0;
     MapIndex := 0;
    end;
    FillComboBox;    
    MapsListBox.ItemIndex := I;
    MapsListBox.Selected[I] := True;
    MapSelected;
    Exit;
   end;
 end;
 MapsComboBox.Clear;
 MapsListBox.Count := 0;
 MapSelected;
end;

procedure TMapsFrame.MapsListBoxDblClick(Sender: TObject);
begin
 if Assigned(FOnOpen) then
  FOnOpen(Self);
end;

procedure TMapsFrame.VisualPageShow(Sender: TObject);
begin
 MapFrame.MapEditView.SetFocus;
end;

procedure TMapsFrame.CopyLayers(Cut: Boolean);
var
 Map, Temp: TMap;
 CRC: array of Int64Rec;
 AssignedIndex: array of Integer;
 I: TLinkDirection;
 X, Y: Integer;
 Link, Layer: TBrushLayer;
 Stream: TMemoryStream;
begin
 if CanAct then
 begin
  Map := MapFrame.MapData;
  if Map <> nil then
  begin
   if LayersListBox.SelCount > 0 then
   begin
    SetLength(AssignedIndex, Map.Count);
    FillChar(AssignedIndex[0], Map.Count * SizeOf(Integer), 255);
    Temp := TNodeClass(Map.ClassType).Create as TMap;
    try
     Temp.Owner := Map.Owner;
     Temp.Width := Map.Width;
     Temp.Height := Map.Height;
     Temp.TileWidth := Map.TileWidth;
     Temp.TileHeight := Map.TileHeight;
     Temp.ColorTableIndex := Map.ColorTableIndex;
     Temp.FInternalFlags := [secLoading];
     for X := 0 to Map.Count - 1 do
      if LayersListBox.Selected[X] then
      begin
       Temp.AddNode.Assign(Map.Layers[X]);
       AssignedIndex[X] := Temp.LastNode.Index;
      end;
     if Temp is TMapBrush then
     begin
      with TMapBrush(Temp) do
      begin
       ExtendsIndex := TMapBrush(Map).ExtendsIndex;
       if Extends = nil then
        ExtendsIndex := TMapBrush(Map).Index;
      end;
      for X := 0 to Map.Count - 1 do
      begin
       Layer := Temp.Nodes[AssignedIndex[X]] as TBrushLayer;
       if Layer <> nil then
        with Map.Nodes[X] as TBrushLayer do
         for I := Low(TLinkDirection) to High(TLinkDirection) do
         begin
          Link := Links[I];
          if Map.IsChild(Link) then
          begin
           Y := AssignedIndex[Link.Index];
           if (Y < 0) and (Map = TMapBrush(Temp).Extends) then
            Layer.LinkIndex[I] := Temp.Count + Link.Index else
            Layer.LinkIndex[I] := Y;
          end;
         end;
      end;
     end;

     if OpenClipboard(0) then
     try
      Stream := TMemoryStream.Create;
      try
       Temp.SaveToStream(Stream);

       with Temp do
       begin
        if ColorTable <> nil then
         X := ColorTable.Checksum else
         X := 0;

        Stream.WriteBuffer(X, 4);

        if ColorTable <> nil then
         X := ColorTable.DataSize;

        Stream.WriteBuffer(X, 4);
       end;

       if Temp is TMapBrush then
        with TMapBrush(Temp) do
        begin
         if Extends <> nil then
          X := Extends.Checksum else
          X := 0;

         Stream.WriteBuffer(X, 4);

         if Extends <> nil then
          X := Extends.DataSize;

         Stream.WriteBuffer(X, 4);
        end;             
       
       SetLength(CRC, Temp.Count);
       for X := 0 to Temp.Count - 1 do
        with Temp.Layers[X] do
         if TileSet <> nil then
          with TileSet, CRC[X] do
          begin
           Lo := Checksum;
           Hi := DataSize;
          end;
       Stream.WriteBuffer(CRC[0], Temp.Count * SizeOf(Int64Rec));

       SetClipboardData(CF_LAYERSCONTAINER, GlobalHandle(Stream.Memory));
      finally
       Stream.Free;
      end;
     finally
      CloseClipboard;
     end;
    finally
     Temp.Free;
    end;
    if Cut then LayerDeleteActionExecute(LayerDeleteAction);
   end;
  end;
 end;
end;

procedure SaveMapsToStream(Stream: TStream; Maps: TBaseMapList);
var
 CRC: array of Int64Rec;
 X, Y, Z: Integer;
 Map: TMap;
begin
 Maps.SaveToStream(Stream);
 for X := 0 to Maps.Count - 1 do
 begin
  Map := Maps.Nodes[X] as TMap;
  with Map do
  begin
   if ColorTable <> nil then
    Z := ColorTable.Checksum else
    Z := 0;

   Stream.WriteBuffer(Z, 4);

   if ColorTable <> nil then
    Z := ColorTable.DataSize;

   Stream.WriteBuffer(Z, 4);
  end;

  if Maps is TBrushList then
   with TMapBrush(Map) do
   begin
    if Extends <> nil then
     Z := Extends.Checksum else
     Z := 0;

    Stream.WriteBuffer(Z, 4);

    if Extends <> nil then
     Z := Extends.DataSize;

    Stream.WriteBuffer(Z, 4);
   end;             

  with Map do
  begin
   SetLength(CRC, Count);
   for Y := 0 to Count - 1 do
    with Nodes[Y] as TMapLayer do
     if TileSet <> nil then
      with TileSet, CRC[Y] do
      begin
       Lo := Checksum;
       Hi := DataSize;
      end;

   Stream.WriteBuffer(CRC[0], Count * SizeOf(Int64Rec));
  end;
 end;
end;

procedure TMapsFrame.CopyMaps(Cut: Boolean);
var
 Temp: TBaseMapList;
 X: Integer;
 Stream: TMemoryStream;
begin
 if CanAct and (MapsListBox.SelCount > 0) then
 with MapList do
 begin
  if BrushMode then
   Temp := TBrushList.Create else
   Temp := TBaseMapList.Create;
  try
   Temp.FInternalFlags := [secLoading];
   for X := 0 to Count - 1 do
    if MapsListBox.Selected[X] then
     Temp.AddNode.Assign(Nodes[X]);

   if OpenClipboard(0) then
   try
    Stream := TMemoryStream.Create;
    try
     SaveMapsToStream(Stream, Temp);

     SetClipboardData(CF_MAPSCONTAINER, GlobalHandle(Stream.Memory));
    finally
     Stream.Free;
    end;
   finally
    CloseClipboard;
   end;
  finally
   Temp.Free;
  end;
  if Cut then MapDeleteActionExecute(MapDeleteAction);
 end;
end;

procedure TMapsFrame.PasteLayers;
var
 Stream: TBufferStream;
 ClpHandle: THandle;
 Map, PasteMap: TMap;
 Signature: LongWord;
 I, NewIndex: Integer;
 CheckData: Int64Rec;
 Node: TBaseSectionedList;
 ld: TLinkDirection;
 Link: TBrushLayer;
 Event: THistoryEvent;
begin
 if CanAct then
 begin
  Event := nil;
  Map := MapFrame.MapData;
  if Map <> nil then
  begin
   try
    if OpenClipboard(0) then
    try
     ClpHandle := GetClipboardData(CF_LAYERSCONTAINER);
     if ClpHandle <> 0 then
     begin
      Stream := TBufferStream.Create(GlobalLock(ClpHandle), GlobalSize(ClpHandle));
      try
       Stream.ReadBuffer(Signature, 4);
       if Signature = BRUSH_SIGN then
        PasteMap := TMapBrush.Create else
       if Signature = MAP_SIGN then
        PasteMap := TMap.Create else
        PasteMap := nil;
       if PasteMap <> nil then
       try
        PasteMap.Owner := Map.Owner;
        Stream.Seek(-4, soFromCurrent);
        PasteMap.LoadFromStream(Stream);

        if PasteMap.Count > 0 then
        begin
         Event := AddMapEvent(Map, 'Paste map layers from clipboard');
         NewIndex := Map.Count;

         Stream.ReadBuffer(CheckData, SizeOf(Int64Rec));
         if CheckData.Hi <> 0 then
          Node := FProject.FMaps.ColorTableList.FindByContent(CheckData.Lo, CheckData.Hi) else
          Node := nil;

         if (Node <> nil) and (Map.ColorTable = nil) then
          Map.ColorTableIndex := Node.Index;

         if (Map.Width < PasteMap.Width) or
            (Map.Height < PasteMap.Height) then
          Map.SetMapSize(PasteMap.Width, PasteMap.Height);

         if PasteMap is TMapBrush then
         begin
          Stream.ReadBuffer(CheckData, SizeOf(Int64Rec));
          if CheckData.Hi <> 0 then
           Node := FProject.FMaps.BrushList.FindByContent(CheckData.Lo, CheckData.Hi) else
           Node := nil;
          if Node <> nil then
           TMapBrush(PasteMap).ExtendsIndex := Node.Index else
           TMapBrush(PasteMap).ExtendsIndex := -1;

          TMapBrush(PasteMap).RepointLinks;
         end;

         Map.FInternalFlags := [secLoading];
         try
          for I := 0 to PasteMap.Count - 1 do
          begin
           Map.AddNode.Assign(PasteMap.Layers[I]);
           Stream.ReadBuffer(CheckData, SizeOf(Int64Rec));
           if CheckData.Hi <> 0 then
            Node := FProject.FMaps.TileSetList.FindByContent(CheckData.Lo, CheckData.Hi) else
            Node := nil;
           if Node <> nil then
            (Map.LastNode as TMapLayer).TileSetIndex := Node.Index;
          end;
         finally
          Map.FInternalFlags := [];
         end;

         if (Map is TMapBrush) and (PasteMap is TMapBrush) then
         begin
          for I := 0 to PasteMap.Count - 1 do
           with Map.Nodes[NewIndex + I] as TBrushLayer do
            for ld := Low(TLinkDirection) to High(TLinkDirection) do
            begin
             Link := (PasteMap.Nodes[I] as TBrushLayer).Links[ld];
             if PasteMap.IsChild(Link) then
              LinkIndex[ld] := NewIndex + Link.Index;
            end;
         end;

         LayersListBox.Count := Map.Count;

         for I := NewIndex to Map.Count - 1 do
          LayersListBox.Selected[I] := True;

         LayerIndex := NewIndex;
         LayersListBox.ItemIndex := NewIndex;

         LayersModified;

         SetMapEventData(Event, Map, ruRedo);         
        end;
       finally
        PasteMap.Free;
       end;
      finally
       GlobalUnlock(ClpHandle);
       Stream.Free;
      end;
     end;
    finally
     CloseClipboard;
    end;
   except
    WideMessageDlg('Clipboard data is damaged', mtError, [mbOk], 0);
    if Event <> nil then
     with FProject.History do
     begin
      Position := Position - 1;
      Count := Count - 1;
     end;
   end;
  end;
 end;
end;

procedure TMapsFrame.PasteMaps;
var
 Stream: TBufferStream;
 ClpHandle: THandle;
 Event: THistoryEvent;
 Cnt: Integer;
begin
 if CanAct then
 begin
  Event := nil;
  Cnt := MapList.Count;
  try
   if OpenClipboard(0) then
   try
    ClpHandle := GetClipboardData(CF_MAPSCONTAINER);
    if ClpHandle <> 0 then
    begin
     Stream := TBufferStream.Create(GlobalLock(ClpHandle), GlobalSize(ClpHandle));
     try
      Event := AddListEvent('Paste maps from clipboard');

      AddMapsFromStream(Stream);

      if MapList.Count > Cnt then
       SetListEventData(Event, ruRedo) else
      with FProject.History do
       Count := Count - 1;
     finally
      GlobalUnlock(ClpHandle);
      Stream.Free;
     end;
    end;
   finally
    CloseClipboard;
   end;
  except
   WideMessageDlg('Clipboard data is damaged', mtError, [mbOk], 0);
   if Event <> nil then
    with FProject.History do
    begin
     Position := Position - 1;
     Count := Count - 1;
    end;
  end;
 end;
end;

procedure TMapsFrame.MapsListBoxDrawItem(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
var
 Flags: Integer;
 Map: TMap;
 WStr: WideString;
begin
 if Index >= 0 then
  with Control as TListBox do
  begin
   Canvas.FillRect(Rect);

   Flags := DrawTextBiDiModeFlags(DT_SINGLELINE or DT_VCENTER or DT_NOPREFIX);
   if not UseRightToLeftAlignment then
     Inc(Rect.Left, 2) else
     Dec(Rect.Right, 2);
   Map := MapList.Nodes[Index] as TMap;
   if Map <> nil then
   begin
    WStr := WideFormat('%d. %s', [Map.Index + 1, Map.Name]);
    Windows.DrawTextW(Canvas.Handle, Pointer(WStr), Length(WStr), Rect, Flags);
   end;
  end;
end;

procedure TMapsFrame.SelectMap(Index: Integer);
begin
 if Index >= 0 then
 begin
  with MapsListBox do
  begin
   ClearSelection;
   ItemIndex := Index;
   Selected[Index] := True;
  end;
  MapsListBoxClick(MapsListBox);
 end;
end;

procedure TMapsFrame.MapPropsModified;
begin
 if Assigned(FOnMapPropsModified) then
  FOnMapPropsModified(Self);
end;

procedure TMapsFrame.PageControlChange(Sender: TObject);
var
 I: Integer;
begin
 MapsListBox.Count := MapsListBox.Count;
 I := MapIndex;
 MapsListBox.ItemIndex := I;
 MapsListBox.Selected[I] := True;

 LayersListBox.Count := LayersListBox.Count;
 I := LayerIndex;
 LayersListBox.ItemIndex := I;
 LayersListBox.Selected[I] := True;

 RefreshMapProps;
 RefreshLayerProps;
 if Assigned(FOnPageControlChange) then
  FOnPageControlChange(Self); 
end;

procedure TMapsFrame.RefreshLayersList(ClearSel: Boolean);
var
 Map: TMap;
 I: Integer;
begin
 Map := MapFrame.MapData;
 if Map <> nil then
  with Map do
   if Count > 0 then
   begin
    if ClearSel or (LayersListBox.Count <> Count) then
     LayersListBox.Count := Count else
     LayersListBox.Invalidate;
    I := LayerIndex;
    if I < 0 then
    begin
     I := 0;
     LayerIndex := 0;
    end;

    LayersListBox.ItemIndex := I;
    LayersListBox.Selected[I] := True;
    LayerSelected;
    Exit;
   end;
 LayersListBox.Count := 0;
 RefreshLayerProps;
end;

procedure TMapsFrame.MapSelectionChanged;
begin
 MapFrame.MapEditView.SelectionChanged;
 if Assigned(FOnMapSelectionChanged) then
  FOnMapSelectionChanged(Self);
end;

procedure TMapsFrame.MapComboSelected;
begin
 if Assigned(FOnMapComboSelected) then
  FOnMapComboSelected(Self);
end;

procedure TMapsFrame.LayersListBoxDblClick(Sender: TObject);
begin
 with MapFrame do
  if (MapData <> nil) and Assigned(FOnLayerDblClick) then
   FOnLayerDblClick(Self, MapData.Layers[LayerIndex], False);
end;               

procedure TMapsFrame.MapsListBoxKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 if ssCtrl in Shift then
 case Key of
  VK_UP:
  begin
   MapUpActionExecute(MapUpAction);
   Key := 0;
  end;
  VK_DOWN:
  begin
   MapDownActionExecute(MapDownAction);
   Key := 0;
  end;
 end;
end;

procedure TMapsFrame.LayersListBoxKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 if ssCtrl in Shift then
 case Key of
  VK_UP:
  begin
   LayerUpActionExecute(LayerUpAction);
   Key := 0;
  end;
  VK_DOWN:
  begin
   LayerDownActionExecute(LayerDownAction);
   Key := 0;
  end;
 end;
end;

procedure TMapsFrame.MapAddFromFileActionExecute(Sender: TObject);
var
 Stream: TStream;
 Event: THistoryEvent;
 Cnt: Integer;
begin
 if CanAct then
  with MapList, OpenDialog do
  begin
   if BrushMode then
    FileName := FDlgBrushFileName;
    FileName := FDlgMapFileName;
   Options := Options - [ofAllowMultiSelect];
   Cnt := Count;
   Event := nil;
   if Execute then
   try
    if BrushMode then
     FDlgBrushFileName := FileName else
     FDlgMapFileName := FileName;
    Stream := TTntFileStream.Create(FileName, fmOpenRead or fmShareDenyWrite);
    try
     Event := AddListEvent(WideFormat('Load maps from ''%s''', [FileName]));

     AddMapsFromStream(Stream);

     if Count > Cnt then
      SetListEventData(Event, ruRedo) else
     with FProject.History do
      Count := Count - 1;
    finally
     Stream.Free;
    end;
   except
    WideMessageDlg('Error loading file', mtError, [mbOk], 0);
    if Event <> nil then
    with FProject.History do
    begin
     Position := Position - 1;
     Count := Count - 1;
    end;
   end;
  end;
end;

procedure TMapsFrame.MapSaveToFileActionExecute(Sender: TObject);
var
 I: Integer;
 Stream: TStream;
 List: TBaseMapList;
begin
 if CanAct then
  with MapList do
   if Count > 0 then
   begin
    if MapsListBox.SelCount = 0 then
     MapsListBox.Selected[MapIndex] := True;

    if MapsListBox.SelCount = 1 then
     SaveDialog.FileName := FixedFileName(Maps[MapIndex].Name) else
     SaveDialog.FileName := '';

    if SaveDialog.Execute then
    begin
     if BrushMode then
      List := TBrushList.Create else
      List := TBaseMapList.Create;
     try

      for I := 0 to Count - 1 do
       if MapsListBox.Selected[I] then
        List.AddNode.Assign(Nodes[I]);

      Stream := TTntFileStream.Create(SaveDialog.FileName, fmCreate);
      try
       SaveMapsToStream(Stream, List);
      finally
       Stream.Free;
      end;
     finally
      List.Free;
     end;
    end;
   end;
end;

procedure TMapsFrame.MapSaveToFileActionUpdate(Sender: TObject);
begin
 MapSaveToFileAction.Enabled := CanAct and (MapList.Count > 0);
end;

procedure TMapsFrame.AddMapsFromStream(Stream: TStream);
var
 List: TBaseMapList;
 NewIndex, I: Integer;
begin
 List := FProject.FMaps;
 if BrushMode then
  List := TMapList(List).BrushList;

 NewIndex := List.Count;
 FProject.AddMapsFromStream(Stream, BrushMode);

 MapsListBox.Count := List.Count;

 for I := NewIndex to List.Count - 1 do
  MapsListBox.Selected[I] := True;

 MapIndex := NewIndex;
 MapsListBox.ItemIndex := NewIndex;

 MapsModified(True);
end;

function TMapsFrame.NewMoveLayersEvent(Map: TMap; Shift: Integer): THistoryEvent;
var
 Str: WideString;
 NewII: Integer;
begin
 if LayersListBox.SelCount = 1 then
 begin
  NewII := LayerIndex;
  Str := WideFormat('%s ''%s'': Change layer ''%s'' index to #%d',
  [MapTypeStr[BrushMode], Map.Name, Map.Layers[NewII].Name, NewII + Shift + 1]);
 end else
  Str := WideFormat('%s ''%s'': Move layers', [MapTypeStr[BrushMode], Map.Name]);

 Result := AddMapEvent(Map, Str);
 //FProject.History.AddEvent(Map, Str);
end;

function TMapsFrame.NewMoveMapsEvent(Shift: Integer): THistoryEvent;
var
 Str: WideString;
 NewII: Integer;
begin
 if MapsListBox.SelCount = 1 then
 begin
  NewII := MapIndex;
  Str := WideFormat('Change %s ''%s'' index to #%d',
        [MapTypeStrLC[BrushMode], MapList[NewII].Name, NewII + Shift + 1]);
 end else
  Str := WideFormat('Move %s', [MapTypeStr2[BrushMode]]);

 Result := AddListEvent(Str);
end;

function TMapsFrame.AddListEvent(const Text: WideString): THistoryEvent;
begin
 Result := FProject.History.AddEvent(nil, Text);
 Result.EventDataClass := TStreamNode;
 SetListEventData(Result, ruUndo);
 Result.OnApply := FProject.MapListApply;
end;

function TMapsFrame.AddMapEvent(Map: TMap; const Text: WideString): THistoryEvent;
begin
 Result := FProject.History.AddEvent(nil, Text);
 Result.EventDataClass := TStreamNode;
 SetMapEventData(Result, Map, ruUndo);
 Result.OnApply := FProject.MapApply;
end;

procedure TMapsFrame.SetListEventData(Event: THistoryEvent; Mode: TRedoUndo);
var
 Temp: TBaseMapList;
 Node: TStreamNode;
 X: Integer;
begin
 if BrushMode then
  Temp := TBrushList.Create else
  Temp := TBaseMapList.Create;
 try
  with MapList do
  begin
   Temp.FInternalFlags := [secLoading];
   for X := 0 to Count - 1 do
    Temp.AddNode.Assign(Nodes[X]);
  end;

  Node := TStreamNode.Create;
  try
   SaveMapsToStream(Node.Stream, Temp);
   Node.UserTag := Ord(BrushMode);
   Event.SetData(Mode, Node);
  finally
   Node.Free;
  end;
 finally
  Temp.Free;
 end;
end;

procedure TMapsFrame.SetMapEventData(Event: THistoryEvent; Map: TMap; Mode: TRedoUndo);
var
 Temp: TBaseMapList;
 Node: TStreamNode;
begin
 if BrushMode then
  Temp := TBrushList.Create else
  Temp := TBaseMapList.Create;
 try
  Temp.FInternalFlags := [secLoading];
  Temp.AddNode.Assign(Map);

  Node := TStreamNode.Create;
  try
   SaveMapsToStream(Node.Stream, Temp);
   Node.Index := Map.Index;
   Node.UserTag := Ord(BrushMode);
   Event.SetData(Mode, Node);
  finally
   Node.Free;
  end;
 finally
  Temp.Free;
 end;
end;

procedure TMapsFrame.MapFrameMapEditViewBeforeContentChange(
  Sender: TCustomTileMapView; ChangeType: TChangeType);
var
 Frm: TMapFrame;
begin
 Frm := Sender.Owner as TMapFrame;
 AddMapEvent(Frm.MapData,
              WideFormat('%s ''%s'': %s', [MapTypeStr[Frm.BrushMode],
                                       Frm.MapData.Name,
              ChangeTypeStr[ChangeType]]));
end;

procedure TMapsFrame.LayersListBoxUnicodeGetData(Control: TWinControl;
  Index: Integer; var Data: WideString);
var
 Map: TMap;
 Layer: TMapLayer;
begin
 Map := MapFrame.MapData;
 if Map <> nil then
 begin
  Layer := Map.Layers[Index];
  if Layer <> nil then
   Data := WideFormat('%d. %s', [Layer.Index + 1, Layer.Name]);
 end;
end;

initialization
 CF_LAYERSCONTAINER := RegisterClipboardFormat('Kusharami Layers Container');
 CF_MAPSCONTAINER := RegisterClipboardFormat('Kusharami Maps Container');
end.
