object TilePropsFrame: TTilePropsFrame
  Left = 0
  Top = 0
  Width = 288
  Height = 310
  TabOrder = 0
  object SelTilePropLabel: TTntLabel
    Left = 0
    Top = 0
    Width = 288
    Height = 13
    Align = alTop
    Caption = 'Tile properties'
    ShowAccelChar = False
    Layout = tlCenter
  end
  object SelTilePropEditor: TPropertyEditor
    Tag = 4
    Left = 0
    Top = 13
    Width = 288
    Height = 297
    BoldChangedValues = False
    ButtonFillMode = fmShaded
    Align = alClient
    ScrollBarOptions.ScrollBars = ssVertical
    TabOrder = 0
    PropertyWidth = 90
    ValueWidth = 194
    Columns = <
      item
        Margin = 0
        Options = [coEnabled, coParentBidiMode, coParentColor, coResizable, coVisible, coFixed]
        Position = 0
        Width = 90
      end
      item
        Margin = 0
        Options = [coEnabled, coParentBidiMode, coParentColor, coResizable, coVisible, coFixed]
        Position = 1
        Width = 194
      end>
  end
end
