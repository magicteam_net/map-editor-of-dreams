unit me_project;

interface

uses SysUtils, Classes, NodeLst, MyClasses, me_lib, ZLibEx, TilesEx, BitmapEx;

type
 TEditMode = (emMap, emBrush, emTileSet, emTile);
 TPageSet = (psMainFrame, psMap, psBrush, psTileSet);
 TPageSetup = set of TPageSet;
 TEditModeSet = set of TEditMode;
 TSetMode = (smActive, smSelected);

 TProjectInfoV1 = packed record
   SelectedMapIndex:        SmallInt;  // 2
   SelectedBrushIndex:      SmallInt;  // 4
   SelectedPaletteIndex:    SmallInt;  // 6
   SelectedTileSetIndex:    SmallInt;  // 8
   Reserved:                Word;      // 10
   EditMode:                TEditMode; // 11
   TileSetMode:             TSetMode;  // 12
   ColorTblMode:            TSetMode;  // 13
   MapFrameScale:           Byte;      // 14
   BrushFrameScale:         Byte;      // 15
   TilesFrameScale:         Byte;      // 16
  end;

 TScales = packed record
  scMapFrame:            array[0..1] of Byte;      // 1, 2
  scBrushFrame:          array[0..1] of Byte;      // 3, 4
  scTilesFrame:          array[0..2] of Byte;      // 5, 6
  scTileFrame:           Byte;                     // 7, 8
 end;

 TProjectInfoV2 = packed record
   SelectedMapIndex:     array[0..1] of SmallInt;  // 2, 4
   SelectedBrushIndex:   array[0..1] of SmallInt;  // 6, 8
   SelectedTileSetIndex: array[0..2] of SmallInt;  // 10, 12, 14
   SelectedPaletteIndex: SmallInt;                 // 16
   SelectedTileIndex:    LongInt;                  // 20
   SelectedTileTileSet:  SmallInt;                 // 22
   EditMode:             TEditMode;                // 23
   PageSetup:            TPageSetup;               // 24
   Scales:               TScales;
  end;

 TProjectInfoV3 = packed record
   SelectedMapIndex:     array[0..1] of SmallInt;  // 2, 4
   SelectedBrushIndex:   array[0..1] of SmallInt;  // 6, 8
   SelectedTileSetIndex: array[0..2] of SmallInt;  // 10, 12, 14
   SelectedPaletteIndex: SmallInt;                 // 16
   SelectedTileIndex:    LongInt;                  // 20
   SelectedTileTileSet:  SmallInt;                 // 22
   EditMode:             TEditMode;                // 23
   PageSetup:            TPageSetup;               // 24
   Scales:               TScales;

   DrawColors: array[Boolean] of Byte;
   Reserved: Word;
  end;

  TProjectInfo = TProjectInfoV3;

  TTileSetInfo = packed record
   Width: Word;
   Reserved: Word;
   SelectedTile: LongInt;
   OffsetX: LongInt;
   OffsetY: LongInt;
  end;

  THistoryEvent = class;
  THistorySet = class;

  TMapEditorProject = class(TBaseSectionedList)
   private
    FHistory: THistorySet;
   protected
    procedure ClearData; override;
    procedure Initialize; override;

    function CheckHeaderSize(var Size: LongInt): Boolean; override;
    function ReadHeader(Stream: TStream; var Header: TSectionHeader): Boolean; override;
    procedure ReadData(Stream: TStream; var Header: TSectionHeader); override;
    procedure WriteData(Stream: TStream; var Header: TSectionHeader); override;
    procedure FillHeader(var Header: TSectionHeader); override;
    function CalculateChecksum: TChecksum; override;
    function CalculateDataSize: LongInt; override;
   public
    FInfo: TProjectInfo;
    FMaps: TMapList;
    FMapLayerFocus: array of Byte;
    FBrushLayerFocus: array of Byte;
    FTileSetInfo: array of TTileSetInfo;

    property History: THistorySet read FHistory;

    procedure Update;


    function IsColorTableUsed(AIndex: Integer): Boolean; overload;
    function IsColorTableUsed(AColorTable: TColorTable): Boolean; overload;

    function IsTileSetUsed(AIndex: Integer): Boolean; overload;
    function IsTileSetUsed(ATileSet: TTileSet): Boolean; overload;

    function IsBrushUsed(AIndex: Integer): Boolean; overload;
    function IsBrushUsed(ABrush: TMapBrush): Boolean; overload;

    procedure UnhookColorTable(AIndex: Integer); overload;
    procedure UnhookColorTable(AColorTable: TColorTable); overload;

    procedure UnhookTileSet(AIndex: Integer); overload;
    procedure UnhookTileSet(ATileSet: TTileSet); overload;

    procedure UnhookBrush(AIndex: Integer); overload;
    procedure UnhookBrush(ABrush: TMapBrush); overload;

    procedure AddMapsFromStream(Stream: TStream; BrushMode: Boolean);
    procedure MapListApply(Sender: THistoryEvent; Data: TNode);
    procedure MapApply(Sender: THistoryEvent; Data: TNode);
    procedure TileSetListApply(Sender: THistoryEvent; Data: TNode);
    procedure ColorTableListApply(Sender: THistoryEvent; Data: TNode);
    procedure ProjectApply(Sender: THistoryEvent; Data: TNode);

    destructor Destroy; override;
  end;

 PProjectEventInfo = ^TProjectEventInfo;
 TProjectEventInfo = record
    FInfo: TProjectInfo;
    FMapLayerFocus: array of Byte;
    FBrushLayerFocus: array of Byte;
    FTileSetInfo: array of TTileSetInfo;
 end;

 TRedoUndo = (ruUndo, ruRedo);

 TIndexArray = array of Integer;

 TApplyEvent = procedure (Sender: THistoryEvent; Data: TNode) of object;
 TGetDestOwnerEvent = procedure (Sender: THistoryEvent;
                                const OwnerIndexList: TIndexArray;
                                Data: TNode;
                                var Result: TNodeList) of object;

 THistoryEvent = class(TNode)
  private
    FText: WideString;
    FRedoData: TNode;
    FUndoData: TNode;
    FRedoInfo: TProjectEventInfo;
    //FUndoInfo: TProjectEventInfo;
    FOnApply: TApplyEvent;
    FOnGetDestOwner: TGetDestOwnerEvent;
    FEventDataClass: TNodeClass;
    procedure SetRedoData(Value: TNode);
    procedure SetUndoData(Value: TNode);
    function GetRedoInfo: PProjectEventInfo;
//    function GetUndoInfo: PProjectEventInfo;
  protected
    procedure RestoreProjectInfo;

    procedure JustAdded; override;

    procedure DoApply(const OwnerIndexList: TIndexArray;
                      Data: TNode); virtual;
    function GetDestOwner(const OwnerIndexList: TIndexArray;
                                      Data: TNode): TNodeList;
  public
    OwnerIndexList: array[TRedoUndo] of TIndexArray;
    property Text: WideString read FText write FText;
    property EventDataClass: TNodeClass read FEventDataClass
                                       write FEventDataClass;

    property RedoData: TNode read FRedoData write SetRedoData;
    property UndoData: TNode read FUndoData write SetUndoData;
    property RedoInfo: PProjectEventInfo read GetRedoInfo;
 //   property UndoInfo: PProjectEventInfo read GetUndoInfo;

    property OnApply: TApplyEvent read FOnApply write FOnApply;
    property OnGetDestOwner: TGetDestOwnerEvent read FOnGetDestOwner
                                               write FOnGetDestOwner;

    procedure DataApply(Mode: TRedoUndo);
    procedure SetData(Mode: TRedoUndo; Data: TNode);
    procedure SetProjectInfo;    
    destructor Destroy; override;
 end;

 THistorySet = class(TNodeList)
   private
    FPosition: Integer;
    FSavedPosition: Integer;
    FOnPositionChanged: TNotifyEvent;
    FOnGetDestOwner: TGetDestOwnerEvent;
    function GetEvent(Index: Integer): THistoryEvent;
    procedure SetPosition(Value: Integer);
    function GetPosition: Integer;
   protected
    procedure ClearData; override;
    procedure Initialize; override;
    procedure DoPositionChanged; virtual;
   public
    property Position: Integer read GetPosition write SetPosition;
    property SavedPosition: Integer read FSavedPosition write FSavedPosition;

    property OnPositionChanged: TNotifyEvent read FOnPositionChanged write FOnPositionChanged;

    property OnGetDestOwner: TGetDestOwnerEvent read FOnGetDestOwner
                                               write FOnGetDestOwner;

    property Events[Index: Integer]: THistoryEvent read GetEvent;

    function AddEvent(Data: TNode; const AText: WideString): THistoryEvent;

    procedure UpdateSavedPosition;
 end;

 TStreamNode = class(TNode)
  private
    FStream: TMemStream;
  protected
    procedure Initialize; override;
  public
    property Stream: TMemStream read FStream;

    procedure Assign(Source: TNode); override;
    destructor Destroy; override;
 end; 

const
  MEDP_SIGN = Ord('M') or
             (Ord('E') shl 8) or
             (Ord('D') shl 16) or
             (Ord('P') shl 24);

 HistoryMax = 100;            

implementation

{ THistorySet }

function THistorySet.AddEvent(Data: TNode; const AText: WideString): THistoryEvent;
begin
 if Count = HistoryMax then
 begin
  Remove(RootNode);
  Dec(FPosition);
  Dec(FSavedPosition);
 end;
 Count := FPosition + 1;

 Result := FNodeClass.Create as THistoryEvent;

 if Data <> nil then
  Result.FEventDataClass := TNodeClass(Data.ClassType);

 Result.FText := AText;
 AddCreated(Result, True);
 Result.SetUndoData(Data);
end;

procedure THistorySet.ClearData;
begin
 FPosition := -1;
 FSavedPosition := 0;
end;

procedure THistorySet.DoPositionChanged;
begin
 if Assigned(FOnPositionChanged) then
  FOnPositionChanged(Self);
end;

function THistorySet.GetEvent(Index: Integer): THistoryEvent;
begin
 Result := Nodes[Index] as THistoryEvent;
end;

function THistorySet.GetPosition: Integer;
begin
 if FPosition >= Count then
  FPosition := Count - 1;

 Result := FPosition;
end;

procedure THistorySet.Initialize;
begin
 FNodeClass := THistoryEvent;
 FAssignableClass := THistorySet;
 FNodeClassAssignable := True;
 FPosition := -1;
end;

procedure THistorySet.SetPosition(Value: Integer);
var
 Event: THistoryEvent;
 I: Integer;
 Redo: Boolean;
begin
 if FPosition < Value then
 begin
  for I := FPosition + 1 to Value do
  begin
   Event := Events[I];
   if Event <> nil then
    Event.DataApply(ruRedo);
  end;
  Redo := True;
 end else
 if FPosition > Value then
 begin
  for I := FPosition downto Value + 1 do
  begin
   Event := Events[I];
   if Event <> nil then
    Event.DataApply(ruUndo);
  end;
  Redo := False;
 end else
  Exit;

 FPosition := Value;
 Event := Events[Value];
 if Event <> nil then
 begin
  if not Redo then
   Event.DataApply(ruRedo);
  Event.RestoreProjectInfo;
 end;
 DoPositionChanged;
end;

procedure THistorySet.UpdateSavedPosition;
begin
 FSavedPosition := FPosition;
end;

{ THistoryEvent }

procedure THistoryEvent.DataApply(Mode: TRedoUndo);
var
 Data: TNode;
begin
 if Mode = ruUndo then
  Data := FUndoData
 else
  Data := FRedoData;

 DoApply(OwnerIndexList[Mode], Data);
end;

destructor THistoryEvent.Destroy;
begin
 FRedoData.Free;
 FUndoData.Free;
 inherited;
end;

procedure THistoryEvent.DoApply(const OwnerIndexList: TIndexArray;
                                      Data: TNode);
var
 Dest: TNodeList;
 Node: TNode;
begin
 if Assigned(FOnApply) then
  FOnApply(Self, Data) else
 if Data <> nil then
 begin
  Dest := GetDestOwner(OwnerIndexList, Data);
  Data.Owner := Dest;  
  if Dest <> nil then
  begin
   Node := Dest.Nodes[Data.Index];
   if Node <> nil then
    Node.Assign(Data);
  end;  
 end;
end;

function THistoryEvent.GetDestOwner(const OwnerIndexList: TIndexArray;
                                      Data: TNode): TNodeList;
begin
 if Assigned(FOnGetDestOwner) then
  FOnGetDestOwner(Self, OwnerIndexList, Data, Result) else
 if Assigned(THistorySet(Owner).FOnGetDestOwner) then
  THistorySet(Owner).FOnGetDestOwner(Self, OwnerIndexList, Data, Result) else
  Result := Data.Owner as TNodeList;
end;

function THistoryEvent.GetRedoInfo: PProjectEventInfo;
begin
 Result := @FRedoInfo;
end;
            {
function THistoryEvent.GetUndoInfo: PProjectEventInfo;
begin
 Result := @FUndoInfo;
end;
                     }
procedure THistoryEvent.JustAdded;
begin
 (FOwner as THistorySet).FPosition := Index;
end;

procedure THistoryEvent.RestoreProjectInfo;
var
 L: Integer;
 Project: TMapEditorProject;
begin
 with THistorySet(Owner) do
  if Owner is TMapEditorProject then
  begin
   Project := TMapEditorProject(Owner);

   Project.FInfo := FRedoInfo.FInfo;

   L := Length(FRedoInfo.FMapLayerFocus);
   SetLength(Project.FMapLayerFocus, L);
   Move(FRedoInfo.FMapLayerFocus[0], Project.FMapLayerFocus[0], L);

   L := Length(FRedoInfo.FBrushLayerFocus);
   SetLength(Project.FBrushLayerFocus, L);
   Move(FRedoInfo.FBrushLayerFocus[0], Project.FBrushLayerFocus[0], L);

   L := Length(FRedoInfo.FTileSetInfo);
   SetLength(Project.FTileSetInfo, L);
   Move(FRedoInfo.FTileSetInfo[0], Project.FTileSetInfo[0], L * SizeOf(TTileSetInfo));

   Project.Update;
  end;
end;

procedure THistoryEvent.SetData(Mode: TRedoUndo; Data: TNode);
var
 NewData: TNode;
 Own: TNode;
 Ptr: ^TIndexArray;
 L: Integer;
begin
 if Mode = ruUndo then
  FUndoData.Free else
  FRedoData.Free;

 if Data <> nil then
 begin
  Ptr := Addr(OwnerIndexList[Mode]);
  L := 0;
  Own := Data.Owner;
  while Own <> nil do
  begin
   Inc(L);
   SetLength(Ptr^, L);
   Ptr^[L - 1] := Own.Index;
   Own := Own.Owner;
  end;
  NewData := FEventDataClass.Create;
  NewData.Owner := Data.Owner;

  if NewData is TBaseSectionedList then
   TBaseSectionedList(NewData).NoRename := True;
  NewData.Assign(Data);

  NewData.Index := Data.Index;
  NewData.UserTag := Data.UserTag;
 end else
  NewData := nil;

 if Mode = ruUndo then
  FUndoData := NewData else
  FRedoData := NewData;

 SetProjectInfo;  
end;

procedure THistoryEvent.SetProjectInfo;
var
 L: Integer;
 Project: TMapEditorProject;
begin
 with THistorySet(Owner) do
  if Owner is TMapEditorProject then
  begin
   Project := TMapEditorProject(Owner);
     
   FRedoInfo.FInfo := Project.FInfo;

   L := Length(Project.FMapLayerFocus);
   SetLength(FRedoInfo.FMapLayerFocus, L);
   Move(Project.FMapLayerFocus[0], FRedoInfo.FMapLayerFocus[0], L);

   L := Length(Project.FBrushLayerFocus);
   SetLength(FRedoInfo.FBrushLayerFocus, L);
   Move(Project.FBrushLayerFocus[0], FRedoInfo.FBrushLayerFocus[0], L);

   L := Length(Project.FTileSetInfo);
   SetLength(FRedoInfo.FTileSetInfo, L);
   Move(Project.FTileSetInfo[0], FRedoInfo.FTileSetInfo[0], L * SizeOf(TTileSetInfo));
  end;
end;

procedure THistoryEvent.SetRedoData(Value: TNode);
begin
 SetData(ruRedo, Value);
end;

procedure THistoryEvent.SetUndoData(Value: TNode);
begin
 SetData(ruUndo, Value);
end;

{ TMapEditorProject }

procedure TMapEditorProject.AddMapsFromStream(Stream: TStream; BrushMode: Boolean);
var
 PasteMaps, List: TBaseMapList;
 Signature: LongWord;
 I, J, NewIndex, FinalIndex: Integer;
 CheckData: Int64Rec;
 Node: TBaseSectionedList;
 Map, ANode: TMap;
 Layer, Link: TBrushLayer;
 Old: TNode;
 Dir: TLinkDirection;
begin
 Stream.ReadBuffer(Signature, 4);
 case Signature of
  BLST_SIGN: PasteMaps := TBrushList.Create;
  MAPS_SIGN: PasteMaps := TBaseMapList.Create;
  else       PasteMaps := nil;
 end;
 if PasteMaps <> nil then
 begin
  try
   Stream.Seek(-4, soFromCurrent);
   PasteMaps.LoadFromStream(Stream);

   List := FMaps;
   if BrushMode then
    List := TMapList(List).BrushList;

   NewIndex := List.Count;
   ANode := PasteMaps.RootNode as TMap;
   while ANode <> nil do
   begin
    List.AddNode.Assign(ANode);
    ANode := ANode.Next as TMap;
   end;
   FinalIndex := NewIndex + (PasteMaps.Count - 1);

   ANode := PasteMaps.RootNode as TMap;
   for J := NewIndex to FinalIndex do
   begin
    Map := List.Nodes[J] as TMap;

    Stream.ReadBuffer(CheckData, SizeOf(Int64Rec));
    if CheckData.Hi <> 0 then
     Node := FMaps.ColorTableList.FindByContent(CheckData.Lo,
                            CheckData.Hi) else
     Node := nil;

    if (Node <> nil) and (Map.ColorTable = nil) then
     Map.ColorTableIndex := Node.Index;

    if PasteMaps is TBrushList then //Map is TMapBrush then
    begin
     Stream.ReadBuffer(CheckData, SizeOf(Int64Rec));
     if Map is TMapBrush then
     begin
      if CheckData.Hi <> 0 then
       Node := FMaps.BrushList.FindByContent(CheckData.Lo, CheckData.Hi) else
       Node := nil;
      if Node <> nil then
       TMapBrush(Map).ExtendsIndex := Node.Index else
       TMapBrush(Map).ExtendsIndex := -1;

      TMapBrush(ANode).Extends := TMapBrush(Map).Extends;
      for I := 0 to Map.Count - 1 do
      begin
       Layer := ANode.Nodes[I] as TBrushLayer;
       with Map.Nodes[I] as TBrushLayer do
        for Dir := Low(TLinkDirection) to High(TLinkDirection) do
        begin
         Link := Layer.Links[Dir];
         if Link <> nil then
         begin
          Old := Link.Owner;

          if Link.Owner <> ANode then
           Link.Owner := TMapBrush(ANode).Extends;

          LinkIndex[Dir] := Layer.LinkIndex[Dir];
          Link.Owner := Old;
         end else
          LinkIndex[Dir] := -1;
        end;
      end;
     end;
    end;

    for I := 0 to Map.Count - 1 do
    begin
     Stream.ReadBuffer(CheckData, SizeOf(Int64Rec));
     if CheckData.Hi <> 0 then
      Node := FMaps.TileSetList.FindByContent(CheckData.Lo, CheckData.Hi) else
      Node := nil;
     if Node <> nil then
      with Map.Nodes[I] as TMapLayer do
       TileSetIndex := Node.Index;
    end;

    ANode := ANode.Next as TMap;
   end;
  finally
   PasteMaps.Free;
  end;
 end else
  raise Exception.Create('Bad signature');
end;

function TMapEditorProject.CalculateChecksum: TChecksum;
begin
 Result := CalcCheckSum(FMapLayerFocus[0], FMaps.Count) +
           CalcCheckSum(FBrushLayerFocus[0], FMaps.BrushList.Count) +
           CalcChecksum(FTileSetInfo[0], Length(FTileSetInfo) * SizeOf(TTileSetInfo)) +
           FMaps.Checksum;
end;

function TMapEditorProject.CalculateDataSize: LongInt;
begin
 Result := FMaps.Count +
           FMaps.BrushList.Count + 
           FMaps.TileSetList.Count * SizeOf(TTileSetInfo) +
           FMaps.TotalSize;
end;

function TMapEditorProject.CheckHeaderSize(var Size: Integer): Boolean;
begin
 Result := (Size = SizeOf(TProjectInfoV1)) or
           (Size = SizeOf(TProjectInfoV2)) or
           inherited CheckHeaderSize(Size);
end;

procedure TMapEditorProject.ClearData;
begin
 FHistory.OnRemove := nil;
 FHistory.Clear;
 FMaps.Clear;
 Finalize(FTileSetInfo);
 Finalize(FMapLayerFocus);
 Finalize(FBrushLayerFocus);
end;

procedure TMapEditorProject.ColorTableListApply(Sender: THistoryEvent;
  Data: TNode);
begin
 FMaps.ColorTablesLock;
 FMaps.BrushList.ColorTablesLock;
 try
  FMaps.ColorTableList.Assign(Data);
 finally
  FMaps.BrushList.ColorTablesUnlock;
  FMaps.ColorTablesUnlock;
 end;
end;

destructor TMapEditorProject.Destroy;
begin
 inherited;
 FHistory.Free; 
 FMaps.Free;
end;

procedure TMapEditorProject.FillHeader(var Header: TSectionHeader);
begin
 Move(FInfo, Header.dh, Header.shHeaderSize);
end;

procedure TMapEditorProject.Initialize;
begin
 inherited;
 FHistory := THistorySet.Create;
 FHistory.Owner := Self;
 FAssignableClass := TMapEditorProject;
 FMaps := TMapList.Create;
 FMaps.Owner := Self;
 FSignature := MEDP_SIGN;
 FHeaderSize := SizeOf(TProjectInfo);
 FillChar(FInfo.Scales, SizeOf(TScales), 2);
 FInfo.Scales.scTileFrame := 16;
 FInfo.PageSetup := [psTileSet, psBrush, psMainFrame];
end;

function TMapEditorProject.IsBrushUsed(ABrush: TMapBrush): Boolean;
begin
 Result := FMaps.BrushList.IsBrushUsed(ABrush);
end;

function TMapEditorProject.IsBrushUsed(AIndex: Integer): Boolean;
begin
 with FMaps.BrushList do
  Result := IsBrushUsed(Nodes[AIndex] as TMapBrush);
end;

function TMapEditorProject.IsColorTableUsed(AColorTable: TColorTable): Boolean;
begin
 Result := FMaps.IsColorTableUsed(AColorTable);
 if Result then Exit;
 Result := FMaps.BrushList.IsColorTableUsed(AColorTable);
end;

function TMapEditorProject.IsColorTableUsed(AIndex: Integer): Boolean;
var
 Item: TColorTable;
begin
 Item := FMaps.ColorTableList.Nodes[AIndex] as TColorTable;
 Result := FMaps.IsColorTableUsed(Item);
 if Result then Exit;
 Result := FMaps.BrushList.IsColorTableUsed(Item);
end;

function TMapEditorProject.IsTileSetUsed(ATileSet: TTileSet): Boolean;
begin
 Result := FMaps.IsTileSetUsed(ATileSet);
 if Result then Exit;
 Result := FMaps.BrushList.IsTileSetUsed(ATileSet);
end;

function TMapEditorProject.IsTileSetUsed(AIndex: Integer): Boolean;
var
 Item: TTileSet;
begin
 Item := FMaps.TileSetList.Nodes[AIndex] as TTileSet;
 Result := FMaps.IsTileSetUsed(Item);
 if Result then Exit;
 Result := FMaps.BrushList.IsTileSetUsed(Item);
end;


procedure TMapEditorProject.MapApply(Sender: THistoryEvent; Data: TNode);
var
 PasteMaps, List: TBaseMapList;
 Signature: LongWord;
 I: Integer;
 CheckData: Int64Rec;
 Node: TBaseSectionedList;
 Map, ANode: TMap;
 Layer, Link: TBrushLayer;
 Old: TNode;
 Dir: TLinkDirection;
begin
 if Data is TStreamNode then
  with TStreamNode(Data) do
  begin
   Stream.Seek(0, soFromBeginning);
   Stream.ReadBuffer(Signature, 4);
   case Signature of
    BLST_SIGN: PasteMaps := TBrushList.Create;
    MAPS_SIGN: PasteMaps := TBaseMapList.Create;
    else       PasteMaps := nil;
   end;
   if PasteMaps <> nil then
   begin
    try
     Stream.Seek(-4, soFromCurrent);
     PasteMaps.LoadFromStream(Stream);

     if PasteMaps is TBrushList then
      List := FMaps.BrushList else
      List := FMaps;

     ANode := PasteMaps.RootNode as TMap;
     Map := List.Nodes[Index] as TMap;
     Map.Assign(ANode);

     Stream.ReadBuffer(CheckData, SizeOf(Int64Rec));
     if CheckData.Hi <> 0 then
      Node := FMaps.ColorTableList.FindByContent(CheckData.Lo,
                             CheckData.Hi) else
      Node := nil;

     if (Node <> nil) and (Map.ColorTable = nil) then
      Map.ColorTableIndex := Node.Index;

     if Map is TMapBrush then
     begin
      Stream.ReadBuffer(CheckData, SizeOf(Int64Rec));
      if CheckData.Hi <> 0 then
       Node := FMaps.BrushList.FindByContent(CheckData.Lo, CheckData.Hi) else
       Node := nil;
      if Node <> nil then
       TMapBrush(Map).ExtendsIndex := Node.Index else
       TMapBrush(Map).ExtendsIndex := -1;

      TMapBrush(ANode).Extends := TMapBrush(Map).Extends;
      for I := 0 to Map.Count - 1 do
      begin
       Layer := ANode.Nodes[I] as TBrushLayer;
       with Map.Nodes[I] as TBrushLayer do
        for Dir := Low(TLinkDirection) to High(TLinkDirection) do
        begin
         Link := Layer.Links[Dir];
         if Link <> nil then
         begin
          Old := Link.Owner;

          if Link.Owner <> ANode then
           Link.Owner := TMapBrush(ANode).Extends;

          LinkIndex[Dir] := Layer.LinkIndex[Dir];
          Link.Owner := Old;
         end else
          LinkIndex[Dir] := -1;
        end;
      end;
     end;

     for I := 0 to Map.Count - 1 do
     begin
      Stream.ReadBuffer(CheckData, SizeOf(Int64Rec));
      if CheckData.Hi <> 0 then
       Node := FMaps.TileSetList.FindByContent(CheckData.Lo, CheckData.Hi) else
       Node := nil;
      if Node <> nil then
       with Map.Nodes[I] as TMapLayer do
        TileSetIndex := Node.Index;
     end;
    finally
     PasteMaps.Free;
    end;
   end;
  end;
end;

procedure TMapEditorProject.MapListApply(Sender: THistoryEvent; Data: TNode);
var
 List: TBaseMapList;
begin
 if Data is TStreamNode then
  with TStreamNode(Data) do
  begin
   Stream.Seek(0, soFromBeginning);
   if UserTag <> 0 then
    List := FMaps.BrushList else
    List := FMaps;
   List.Count := 0;
   AddMapsFromStream(Stream, UserTag <> 0);
  end;
end;

procedure TMapEditorProject.ProjectApply(Sender: THistoryEvent;
  Data: TNode);
begin
 with Data as TStreamNode do
 begin
  Stream.Seek(0, soFromBeginning);
  FMaps.LoadFromStream(Stream);
 end;
// FMaps.Assign(Data);
end;

procedure TMapEditorProject.ReadData(Stream: TStream;
  var Header: TSectionHeader);
var
 Rec: TProgressRec;
begin
 Rec.Header := @Header;
 Rec.ProgressID := SPID_READ_DATA;
 Include(FInternalFlags, secSkipDataSizeTest);
 Stream := TZDecompressionStream.Create(Stream);
 try
  Rec.Stream := Stream;
  FMaps.LoadFromStream(Stream);
  Update;
  Read(Rec, FMapLayerFocus[0], FMaps.Count);
  Read(Rec, FBrushLayerFocus[0], FMaps.BrushList.Count);
  Read(Rec, FTileSetInfo[0], FMaps.TileSetList.Count * SizeOf(TTileSetInfo));
 finally
  Stream.Free;
 end;
end;

function TMapEditorProject.ReadHeader(Stream: TStream;
  var Header: TSectionHeader): Boolean;
var
 V1: ^TProjectInfoV1;
begin
 Result := inherited ReadHeader(Stream, Header);
 case Header.shHeaderSize of
  SizeOf(TProjectInfoV1):
  begin
   V1 := Addr(Header.dh);
   FInfo.EditMode := V1.EditMode;
   FInfo.SelectedMapIndex[0] := V1.SelectedMapIndex;
   FInfo.SelectedMapIndex[1] := V1.SelectedMapIndex;
   FInfo.SelectedBrushIndex[0] := V1.SelectedBrushIndex;
   FInfo.SelectedBrushIndex[1] := V1.SelectedBrushIndex;
   FInfo.SelectedPaletteIndex := V1.SelectedPaletteIndex;
   FInfo.SelectedTileSetIndex[0] := V1.SelectedTileSetIndex;
   FInfo.SelectedTileSetIndex[1] := V1.SelectedTileSetIndex;
   FInfo.SelectedTileSetIndex[2] := V1.SelectedTileSetIndex;
   FInfo.SelectedTileTileSet := V1.SelectedTileSetIndex;
   FInfo.SelectedTileIndex := 0;
   FInfo.Scales.scMapFrame[0] := V1.MapFrameScale;
   FInfo.Scales.scMapFrame[1] := V1.MapFrameScale;
   FInfo.Scales.scBrushFrame[0] := V1.BrushFrameScale;
   FInfo.Scales.scBrushFrame[1] := V1.BrushFrameScale;
   FInfo.Scales.scTilesFrame[0] := V1.TilesFrameScale;
   FInfo.Scales.scTilesFrame[1] := V1.TilesFrameScale;
   FInfo.Scales.scTilesFrame[2] := V1.TilesFrameScale;
   FInfo.Scales.scTileFrame := 8;
   FInfo.PageSetup := [psMainFrame, psBrush, psTileSet];
   case FInfo.EditMode of
    emBrush: Exclude(FInfo.PageSetup, psBrush);
    emTileSet: Exclude(FInfo.PageSetup, psTileSet);
   end;
  end;
  SizeOf(TProjectInfoV2):
  begin
   Move(Header.dh, FInfo, SizeOf(TProjectInfoV2));
   FInfo.DrawColors[False] := 0;
   FInfo.DrawColors[True] := 0;
   FInfo.Reserved := 0;
  end;
  SizeOf(TProjectInfoV3):
   Move(Header.dh, FInfo, SizeOf(TProjectInfoV3));
 end;
end;

procedure TMapEditorProject.TileSetListApply(Sender: THistoryEvent;
  Data: TNode);
begin
 FMaps.TileSetsLock;
 FMaps.BrushList.TileSetsLock;
 try
  FMaps.TileSetList.Assign(Data);
 finally
  FMaps.BrushList.TileSetsUnlock;
  FMaps.TileSetsUnlock;
 end;
end;

procedure TMapEditorProject.UnhookBrush(ABrush: TMapBrush);
begin
 FMaps.BrushList.UnhookBrush(ABrush);
end;

procedure TMapEditorProject.UnhookBrush(AIndex: Integer);
begin
 with FMaps.BrushList do
  UnhookBrush(Nodes[AIndex] as TMapBrush);
end;

procedure TMapEditorProject.UnhookColorTable(AColorTable: TColorTable);
begin
 FMaps.UnhookColorTable(AColorTable);
 FMaps.BrushList.UnhookColorTable(AColorTable);
end;

procedure TMapEditorProject.UnhookColorTable(AIndex: Integer);
var
 Item: TColorTable;
begin
 Item := FMaps.ColorTableList.Nodes[AIndex] as TColorTable;
 FMaps.UnhookColorTable(Item);
 FMaps.BrushList.UnhookColorTable(Item);
end;

procedure TMapEditorProject.UnhookTileSet(ATileSet: TTileSet);
begin
 FMaps.UnhookTileSet(ATileSet);
 FMaps.BrushList.UnhookTileSet(ATileSet);
end;

procedure TMapEditorProject.UnhookTileSet(AIndex: Integer);
var
 Item: TTileSet;
begin
 Item := FMaps.TileSetList.Nodes[AIndex] as TTileSet;
 FMaps.UnhookTileSet(Item);
 FMaps.BrushList.UnhookTileSet(Item);
end;

procedure TMapEditorProject.Update;
var
 I, OldLength: Integer;
begin
 SetLength(FMapLayerFocus, FMaps.Count);
 SetLength(FBrushLayerFocus, FMaps.BrushList.Count);
 OldLength := Length(FTileSetInfo);
 SetLength(FTileSetInfo, FMaps.TileSetList.Count);
 for I := OldLength to Length(FTileSetInfo) - 1 do
  with FTileSetInfo[I] do
  begin
   Width := 18;
   Reserved := 0;
   SelectedTile := -1;
   OffsetX := 0;
   OffsetY := 0;
  end;
end;

procedure TMapEditorProject.WriteData(Stream: TStream;
  var Header: TSectionHeader);
var
 Rec: TProgressRec;
begin
 Rec.Header := @Header;
 Rec.ProgressID := SPID_WRITE_DATA;
 Stream := TZCompressionStream.Create(Stream);
 try
  Rec.Stream := Stream;
  FMaps.SaveToStream(Stream);
  Write(Rec, FMapLayerFocus[0], FMaps.Count);
  Write(Rec, FBrushLayerFocus[0], FMaps.BrushList.Count);
  Write(Rec, FTileSetInfo[0], FMaps.TileSetList.Count * SizeOf(TTileSetInfo));
 finally
  Stream.Free;
 end;
end;

{ TStreamNode }

procedure TStreamNode.Assign(Source: TNode);
begin
 inherited;
 with TStreamNode(Source).FStream do
 begin
  FStream.SetSize(Size);
  Move(Memory^, FStream.Memory^, Size);
  FStream.Seek(Position, soFromBeginning);
 end;
end;

destructor TStreamNode.Destroy;
begin
 inherited;
 FStream.Free;
end;

procedure TStreamNode.Initialize;
begin
 inherited;
 FStream := TMemStream.Create;
end;


end.
