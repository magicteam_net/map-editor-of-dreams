object MapFrame: TMapFrame
  Left = 0
  Top = 0
  Width = 443
  Height = 269
  Align = alClient
  AutoScroll = False
  Color = clBtnFace
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -9
  Font.Name = 'Small Fonts'
  Font.Style = []
  ParentBackground = False
  ParentColor = False
  ParentFont = False
  TabOrder = 0
  object MapStatusBar: TTntStatusBar
    Left = 0
    Top = 0
    Width = 443
    Height = 14
    Align = alTop
    Panels = <
      item
        Width = 40
      end
      item
        Width = 50
      end
      item
        Width = 40
      end
      item
        Width = 40
      end
      item
        Width = 70
      end
      item
        Width = 50
      end>
    ParentColor = True
    ParentFont = True
    UseSystemFont = False
  end
  object MapEditView: TTileMapView
    Left = 0
    Top = 14
    Width = 443
    Height = 255
    MapWidth = 0
    MapHeight = 0
    OffsetX = 0
    OffsetY = 0
    ShowGrid = False
    GridStyle = psSolid
    SelectStyle = psSolid
    OnDrawCell = MapEditViewDrawCell
    OnSelectionChanged = MapEditViewSelectionChanged
    OnGetCellState = MapEditViewGetCellState
    OnGetBufCellState = MapEditViewGetBufCellState
    OnCopyMapData = MapEditViewCopyMapData
    OnPasteBufferApply = MapEditViewPasteBufferApply
    OnClearCell = MapEditViewClearCell
    OnBufClearCell = MapEditViewBufClearCell
    OnReleasePasteBuffer = MapEditViewReleasePasteBuffer
    OnCheckCompatibility = MapEditViewCheckCompatibility
    OnSetPosition = MapEditViewSetPosition
    OnMakePasteBufferCopy = MapEditViewMakePasteBufferCopy
    Align = alClient
    Constraints.MinHeight = 32
    DragCursor = 983
    TabOrder = 1
    OnEnter = MapEditViewEnter
    OnMouseMove = MapEditViewMouseMove
    OnMouseWheelDown = MapEditViewMouseWheelDown
    OnMouseWheelUp = MapEditViewMouseWheelUp
  end
end
