unit me_tileeditframe;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, TileMapView, ComCtrls, TntComCtrls, ExtCtrls, me_tileprops,
  BmpImg, BitmapEx, TilesEx, mbColorPreview, StdCtrls, TntStdCtrls;

type
  TTileEditFrame = class;
  TScaleChangeEvent = procedure(Sender: TObject; var Value: Integer) of object;
  TDrawColorChangeEvent = procedure(Sender: TTileEditFrame; Mode: Boolean) of object;

  TTileEditFrame = class(TFrame)
    PropsFrame: TTilePropsFrame;
    Panel: TPanel;
    StatusBar: TTntStatusBar;
    TileEditView: TTileMapView;
    Splitter: TSplitter;
    LeftPanel: TPanel;
    ColorPanel: TPanel;
    Color1Preview: TmbColorPreview;
    Color2Preview: TmbColorPreview;
    Color1Label: TTntLabel;
    Color2Label: TTntLabel;
    procedure TileEditViewReleasePasteBuffer(Sender: TCustomTileMapView;
      Data: Pointer);
    procedure TileEditViewPasteBufferApply(Sender: TCustomTileMapView;
      CellX, CellY: Integer; Data: Pointer);
    procedure TileEditViewMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure TileEditViewMakePasteBufferCopy(Sender: TCustomTileMapView;
      Source: Pointer; var Dest: Pointer);
    procedure TileEditViewGetCellState(Sender: TCustomTileMapView; CellX,
      CellY: Integer; var Dirty: Boolean);
    procedure TileEditViewGetBufCellState(Sender: TCustomTileMapView;
      Data: Pointer; CellX, CellY: Integer; var Dirty: Boolean);
    procedure TileEditViewDrawCell(Sender: TCustomTileMapView;
      const Rect: TRect; CellX, CellY: Integer);
    procedure TileEditViewCopyMapData(Sender: TCustomTileMapView;
      var Data: Pointer; const Rect: TRect; SelectionBuffer: Pointer);
    procedure TileEditViewClearCell(Sender: TCustomTileMapView; CellX,
      CellY: Integer);
    procedure TileEditViewCheckCompatibility(Sender: TCustomTileMapView;
      Data: Pointer; var Result: Boolean);
    procedure TileEditViewBufClearCell(Sender: TCustomTileMapView; CellX,
      CellY: Integer; Data: Pointer);
    procedure TileEditViewMouseWheelDown(Sender: TObject;
      Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
    procedure TileEditViewMouseWheelUp(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure TileEditViewEnter(Sender: TObject);
    procedure ColorPreviewMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure TileEditViewContentsChanged(Sender: TObject);
    procedure TileEditViewSelectionChanged(Sender: TCustomTileMapView);
  private
    FTile: TTileItem;
    FCvtTo: TBitmapConverter;
    FCvtFrom: TBitmapConverter;
    FPicData: TBitmapContainer;
    FPicHeader: TPicHeader;
    FColorTable: TColorTable;
    FScale: Integer;
    FDrawColors: array[Boolean] of Byte;
    FOnScaleChange: TScaleChangeEvent;
    FOnImageModified: TNotifyEvent;
    FOnDrawColorChange: TDrawColorChangeEvent;
    procedure SetColorTable(Value: TColorTable);
    procedure SetScale(Value: Integer);
    procedure SetTile(Value: TTileItem);
    procedure UpdateCoordinates;
    function GetRGBQuadsPtr: Pointer;
    function GetColorIndex(Mode: Boolean): Integer;
    procedure SetColorIndex(Mode: Boolean; Value: Integer);
    procedure UpdateColorInfo(Mode: Boolean);
    function GetIndexValue(Mode: Boolean): Integer;
  protected
    property ColorIndexValue[Mode: Boolean]: Integer read GetIndexValue;
  public
    property Scale: Integer read FScale write SetScale;
    property Tile: TTileItem read FTile write SetTile;
    property ColorTable: TColorTable read FColorTable write SetColorTable;

    property OnScaleChange: TScaleChangeEvent read FOnScaleChange
                                             write FOnScaleChange;
    property OnImageModified: TNotifyEvent
                              read FOnImageModified
                             write FOnImageModified;
    property OnDrawColorChange: TDrawColorChangeEvent
                            read FOnDrawColorChange
                           write FOnDrawColorChange;

    property RGBQuadsPtr: Pointer read GetRGBQuadsPtr;
    property ColorIndex[Mode: Boolean]: Integer read GetColorIndex write SetColorIndex;

    procedure RefreshColorTable(DoRepaint: Boolean = True);
    procedure DoSetScale(Value: Integer; DoRepaint: Boolean = True);

    function TileImageBackup: Pointer;
    function TileImageData: Pointer;
    function TileImageSize: Integer;

    procedure RefreshImage(DoRepaint: Boolean);

    procedure ContinguousSelect(X, Y: Integer; Shift: TShiftState);
    procedure MagicWand(X, Y: Integer; Shift: TShiftState);
    procedure FillSelected;
    
    procedure FloodFill(X, Y: Integer; Mode: Boolean);
    procedure PickColor(X, Y: Integer; Mode: Boolean);

    procedure SetPixel(X, Y: Integer; Mode: Boolean);
    procedure ErasePixel(X, Y: Integer);

    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  end;

  TBitmapBuffer = class
  public
   FPicData: TBitmapContainer;
   FPicHeader: TPicHeader;
   FVisibility: array of Boolean;
   constructor Create(AWidth, AHeight: Integer);
   destructor Destroy; override;
  end;


implementation

uses HexUnit;

{$R *.dfm}

{ TBitmapBuffer }

constructor TBitmapBuffer.Create(AWidth, AHeight: Integer);
begin
 FPicData := TBitmapContainer.Create;

 FillPicHeader(Min(65535, AWidth),
               Min(65535, AHeight), FPicHeader, FPicData);
 FPicData.ImageFlags := (8 - 1) or IMG_Y_FLIP;
 with FPicData do
  SetLength(FVisibility, (Width + WidthRemainder) * Height);
end;

destructor TBitmapBuffer.Destroy;
var
 P: Pointer;
begin
 P := FPicData.ImageData;
 FreeMem(P);
 FPicData.Free;
 inherited;
end;

procedure TTileEditFrame.TileEditViewReleasePasteBuffer(
  Sender: TCustomTileMapView; Data: Pointer);
begin
 TObject(Data).Free;
end;

procedure TTileEditFrame.TileEditViewPasteBufferApply(
  Sender: TCustomTileMapView; CellX, CellY: Integer; Data: Pointer);
var
 X, Y, SrcStride, DstStride, W: Integer;
 Src, Dst: PByte;
 Flg: PBoolean;
begin
 with FPicData do
 begin
  DstStride := Width + WidthRemainder;
  Dst := ImageData;
  CellY := (Height - 1) - CellY;
 end;
 with TObject(Data) as TBitmapBuffer, FPicData do
 begin
  SrcStride := Width + WidthRemainder;

  Dec(CellY, Height - 1);

  W := Width;

  Src := Pointer(ImageData);
  Flg := Pointer(FVisibility);

  if CellX < 0 then
  begin
   Dec(Src, CellX);
   Dec(Flg, CellX);
   Inc(W, CellX);
  end else
   Inc(Dst, CellX);

  X := CellX + Width;
  if X > Self.FPicData.Width then
   Dec(W, X - Self.FPicData.Width);

  if W > 0 then
  begin
   if CellY < 0 then
   begin
    Y := CellY * SrcStride;
    Dec(Src, Y);
    Dec(Flg, Y);
   end;

   Y := Max(0, CellY);
   Inc(Dst, Y * DstStride);
   Dec(DstStride, W);
   Dec(SrcStride, W);

   for Y := Y to Min(Self.FPicData.Height, CellY + Height) - 1 do
   begin
    for X := 0 to W - 1 do
    begin
     if Flg^ then
      Dst^ := Src^;
     Inc(Src);
     Inc(Dst);
     Inc(Flg);
    end;
    Inc(Src, SrcStride);
    Inc(Flg, SrcStride);
    Inc(Dst, DstStride);
   end;
  end;
 end;
end;

procedure TTileEditFrame.TileEditViewMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
 UpdateCoordinates;
end;

procedure TTileEditFrame.TileEditViewMakePasteBufferCopy(
  Sender: TCustomTileMapView; Source: Pointer; var Dest: Pointer);
var
 Src: TBitmapBuffer absolute Source;
 Dst: TBitmapBuffer;
 Size: Integer;
begin
 if TObject(Source) is TBitmapBuffer then
  with Src.FPicData do
  begin
   Dst := TBitmapBuffer.Create(Width, Height);
   try
    Size := (Width + WidthRemainder) * Height;
    Move(ImageData^, Dst.FPicData.ImageData^, Size);
    Move(Src.FVisibility[0], Dst.FVisibility[0], Size);
    Dest := Pointer(Dst);
   except
    Dst.Free;
    raise;
   end;
  end;
end;

procedure TTileEditFrame.TileEditViewGetCellState(
  Sender: TCustomTileMapView; CellX, CellY: Integer; var Dirty: Boolean);
begin
 with FPicData do
  Dirty := PByteArray(ImageData)[((Height - 1) - CellY) *
          (Width + WidthRemainder) + CellX] - FTile.FirstColor <>
          Byte(TTileSet(FTile.Owner).TransparentColor);
end;

procedure TTileEditFrame.TileEditViewGetBufCellState(
  Sender: TCustomTileMapView; Data: Pointer; CellX, CellY: Integer;
  var Dirty: Boolean);
begin
 with TObject(Data) as TBitmapBuffer, FPicData do
  Dirty := FVisibility[((Height - 1) - CellY) * (Width + WidthRemainder) + CellX];
end;

procedure TTileEditFrame.TileEditViewDrawCell(Sender: TCustomTileMapView;
  const Rect: TRect; CellX, CellY: Integer);
var
 Offset, XX, YY: Integer;
begin
 with Sender, Canvas do
 begin
  Brush.Style := bsSolid;

  if TObject(PasteBuffer) is TBitmapBuffer then
  begin
   XX := CellX - PasteBufRect.Left;
   YY := CellY - PasteBufRect.Top;
   with TBitmapBuffer(PasteBuffer), FPicData do
   if (XX >= 0) and
      (YY >= 0) and
      (XX < Width) and
      (YY < Height) then
   begin
    Offset := ((Height - 1) - YY) * (Width + WidthRemainder) + XX;

    if FVisibility[Offset] then
    begin
     if FColorTable <> nil then
      Brush.Color := FColorTable.RGBZ[PByteArray(ImageData)[Offset]] else
      Brush.Color := clBlack;
      
     FillRect(Rect);
     Exit;
    end;
   end;
  end;

  if (FColorTable <> nil) and
     (FPicData.ImageData <> nil) then
   with FPicData do
    Brush.Color := FColorTable.RGBZ[PByteArray(ImageData)[((Height - 1) - CellY)
            * (Width + WidthRemainder) + CellX]] else
   Brush.Color := clBlack;
  FillRect(Rect);
 end;
end;

procedure TTileEditFrame.TileEditViewCopyMapData(
  Sender: TCustomTileMapView; var Data: Pointer; const Rect: TRect;
  SelectionBuffer: Pointer);
var
 Buf: TBitmapBuffer;
 Src, Dst: PByte;
 X, Y: Integer;
 SrcStride, DstStride, SelStride: Integer;
 SB, Flg: PBoolean;
 Transparent: Byte;
begin
 Buf := TBitmapBuffer.Create(Rect.Right - Rect.Left, Rect.Bottom - Rect.Top);
 try
  with FPicData do
  begin
   Src := Addr(PByteArray(ImageData)[((Height - 1) - Rect.Top) *
             (Width + WidthRemainder) + Rect.Left]);
   SB := Addr(PByteArray(SelectionBuffer)[Rect.Top * Width + Rect.Left]);
   SrcStride := -((Width + WidthRemainder) + Buf.FPicData.Width);
   SelStride := Width;
  end;
  Transparent := TTileSet(FTile.Owner).TransparentColor;
  with Buf, FPicData do
  begin
   DstStride := -(Width * 2 + WidthRemainder);
   Dec(SelStride, Width);

   X := (Height - 1) * (Width + WidthRemainder);
   Dst := Pointer(ImageData);
   Inc(Dst, X);
   Flg := Pointer(FVisibility);
   Inc(Flg, X);

   for Y := 0 to Height - 1 do
   begin
    for X := 0 to Width - 1 do
    begin
     Flg^ := SB^;
     if SB^ then
      Dst^ := Src^ else
      Dst^ := Transparent;
     Inc(SB);
     Inc(Flg);
     Inc(Src);
     Inc(Dst);
    end;

    Inc(SB, SelStride);
    Inc(Src, SrcStride);
    Inc(Dst, DstStride);
    Inc(Flg, DstStride);
   end;
   Data := Pointer(Buf);
  end;
 except
  Buf.Free;
  raise;
 end;
end;

procedure TTileEditFrame.TileEditViewClearCell(Sender: TCustomTileMapView;
  CellX, CellY: Integer);
begin
 with FPicData do
  PByteArray(ImageData)[((Height - 1) - CellY) *
       (Width + WidthRemainder) + CellX] :=
          TTileSet(FTile.Owner).TransparentColor + FTile.FirstColor;
end;

procedure TTileEditFrame.TileEditViewCheckCompatibility(
  Sender: TCustomTileMapView; Data: Pointer; var Result: Boolean);
begin
 Result := TObject(Data) is TBitmapBuffer;
end;

procedure TTileEditFrame.TileEditViewBufClearCell(
  Sender: TCustomTileMapView; CellX, CellY: Integer; Data: Pointer);
begin
 with TObject(Data) as TBitmapBuffer, FPicData do
  FVisibility[((Height - 1) - CellY) *
          (Width + WidthRemainder) + CellX] := False;
end;

procedure TTileEditFrame.SetTile(Value: TTileItem);
var
 Flg: TCompositeFlags;
begin
 if Value = FTile then Exit;
 TileEditView.Deselect; 
 FTile := nil;
 if Value <> nil then
  with Value.Owner as TTileSet do
  begin
   if Value <> EmptyTile then
   begin
    FTile := Value;
    FillPicHeader(TileWidth, TileHeight, FPicHeader, FPicData);
    FillConverter(FCvtTo, [(8 - 1) or IMG_Y_FLIP]);
    Flg := TileBitmap.FlagsList;
    FCvtFrom.Reset([(8 - 1) or IMG_Y_FLIP], Flg, DrawFlags and DRAW_PIXEL_MODIFY);

    RefreshImage(False);

    TileEditView.SetMapSize(TileWidth, TileHeight, True);

    PropsFrame.ShowTileProps(TTileSet(Value.Owner), Value.TileIndex);
   end;
  end;
 if FTile = nil then
 begin
  PropsFrame.ShowTileProps(nil, 0);
  TileEditView.SetMapSize(0, 0, True);
 end;
 TileEditView.SelectionChanged;
 UpdateColorInfo(False);
 UpdateColorInfo(True); 
 UpdateCoordinates;
end;

constructor TTileEditFrame.Create(AOwner: TComponent);
begin
 inherited;
 FCvtTo := TBitmapConverter.Create;
 FCvtFrom := TBitmapConverter.Create;
 FPicData := TBitmapContainer.Create;
 FPicData.ImageFlags := (8 - 1) or IMG_Y_FLIP;
 Scale := 8;
 UpdateColorInfo(False);
 UpdateColorInfo(True); 
end;

destructor TTileEditFrame.Destroy;
begin
 FPicData.Free;
 FCvtTo.Free;
 FCvtFrom.Free;
 inherited;
end;

procedure TTileEditFrame.SetColorTable(Value: TColorTable);
begin
 FColorTable := Value;
 RefreshColorTable;
end;

procedure TTileEditFrame.SetScale(Value: Integer);
begin
 DoSetScale(Value);
end;

procedure TTileEditFrame.DoSetScale(Value: Integer; DoRepaint: Boolean);
var
 OldScale, X, Y: Integer;
begin
 if Value < 1 then Value := 1;
 if Value > 32 then Value := 32;
 if Assigned(FOnScaleChange) then
  FOnScaleChange(Self, Value);
 OldScale := FScale;
 if OldScale <= 0 then
  OldScale := 1;
 FScale := Value;
 StatusBar.Panels[1].Text := WideFormat('Scale: x%d', [FScale]);
 if FTile <> nil then
 begin
  if TileEditView.ClientLeft > 0 then
   X := ((TileEditView.MapWidth * TileEditView.TileWidth) shr 1) div OldScale else
   X := (TileEditView.OffsetX + TileEditView.ClientWidth shr 1) div OldScale;

  if TileEditView.ClientTop > 0 then
   Y := ((TileEditView.MapHeight * TileEditView.TileHeight)  shr 1) div OldScale else
   Y := (TileEditView.OffsetY + TileEditView.ClientHeight shr 1) div OldScale;

  TileEditView.SetTileSize(FScale, FScale, False);
  TileEditView.SetPosition(X * FScale - TileEditView.ClientWidth shr 1,
                           Y * FScale - TileEditView.ClientHeight shr 1,
                           DoRepaint);
 end else
 begin
  TileEditView.SetTileSize(FScale, FScale, False);
  if DoRepaint then
   TileEditView.Invalidate;
 end;
end;

procedure TTileEditFrame.RefreshColorTable(DoRepaint: Boolean);
begin
 if FColorTable <> nil then
  FColorTable.ColorFormat.ConvertToRGBQuad(FColorTable.ColorData,
                                    Addr(FPicHeader.bhPalette),
                                    FColorTable.ColorsCount) else
  FillChar(FPicHeader.bhPalette, SizeOf(TRGBQuads), 0);

 UpdateColorInfo(False);
 UpdateColorInfo(True); 

 if DoRepaint then
  TileEditView.Invalidate;
end;

procedure TTileEditFrame.UpdateCoordinates;
var
 I: Integer;
begin
 if FTile <> nil then
 begin
  with TileEditView do
   if (TileX >= 0) and (TileX < MapWidth) and
      (TileY >= 0) and (TileY < MapHeight) then
    I := TileY * MapWidth + TileX else
    I := -1;
  if I >= 0 then
  begin
   with TileEditView do
   begin
    StatusBar.Panels[2].Text := WideFormat('X: %d', [TileX]);
    StatusBar.Panels[3].Text := WideFormat('Y: %d', [TileY]);
   end;
   StatusBar.Panels[4].Text := WideFormat('Offset: %d', [I]);
   Exit;
  end;
 end;
 StatusBar.Panels[2].Text := '';
 StatusBar.Panels[3].Text := '';
 StatusBar.Panels[4].Text := '';
end;

procedure TTileEditFrame.TileEditViewMouseWheelDown(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
 if ssShift in Shift then
 begin
  SetScale(FScale - 1);
  Handled := True;
 end;
end;

procedure TTileEditFrame.TileEditViewMouseWheelUp(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
 if ssShift in Shift then
 begin
  SetScale(FScale + 1);
  Handled := True;
 end;
end;

procedure TTileEditFrame.TileEditViewEnter(Sender: TObject);
begin
 if FTile <> nil then
  with PropsFrame do
   if PaletteFrame <> nil then
    PaletteFrame.SelectColorRange(FTile.FirstColor,
          1 shl TTileSet(FTile.Owner).TileBitmap.BitsCount);
end;

procedure TTileEditFrame.RefreshImage(DoRepaint: Boolean);
begin
 if FTile <> nil then
 begin
  FCvtTo.SrcInfo.PixelModifier := FTile.FirstColor;
  FTile.Draw(FPicData, 0, 0, FCvtTo);
  if DoRepaint then
   TileEditView.Invalidate;
 end;
end;

function TTileEditFrame.GetRGBQuadsPtr: Pointer;
begin
 Result := Addr(FPicHeader.bhPalette);
end;

function TTileEditFrame.GetColorIndex(Mode: Boolean): Integer;
begin
 Result := FDrawColors[Mode];
 if FTile <> nil then
  with TTileSet(FTile.Owner) do
   Result := Result and ((1 shl BitCount) - 1);
end;

procedure TTileEditFrame.SetColorIndex(Mode: Boolean;
  Value: Integer);
begin
 FDrawColors[Mode] := Value;
 UpdateColorInfo(Mode);
end;

procedure TTileEditFrame.UpdateColorInfo(Mode: Boolean);
var
 L: TTntLabel;
 P: TmbColorPreview;
 C, FC: Byte;
 RGB: LongWord;
begin
 if Mode then
 begin
  P := Color2Preview;
  L := Color2Label;
 end else
 begin
  P := Color1Preview;
  L := Color1Label;
 end;

 C := ColorIndex[Mode];
 FC := ColorIndexValue[Mode];

 RGB := LongWord(FPicHeader.bhPalette[FC]) and $FFFFFF;
 L.Caption := WideFormat('Index: %d/%d; RGB: %6.6x', [C, FC, RGB]);

 if FColorTable <> nil then
  P.Color := FColorTable.RGBZ[FC] else
  P.Color := clBlack;
end;

procedure TTileEditFrame.ColorPreviewMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
 Mode: Boolean;
begin
 if FTile <> nil then
 begin
  Mode := TControl(Sender).Tag <> 0;
  with TTileSet(FTile.Owner) do
  begin
   if Button = mbLeft then
    Inc(FDrawColors[Mode]) else
   if Button = mbRight then
    Dec(FDrawColors[Mode]) else
    Exit;
   UpdateColorInfo(Mode);
   if Assigned(FOnDrawColorChange) then
    FOnDrawColorChange(Self, Mode);
  end;
 end;
end;

procedure TTileEditFrame.TileEditViewContentsChanged(Sender: TObject);
begin
 if FTile <> nil then
 begin
  FCvtFrom.SrcInfo.PixelModifier := -FTile.FirstColor;
  FTile.Load(FPicData, 0, 0, FCvtFrom);
  if Assigned(FOnImageModified) then
   FOnImageModified(Self);
 end;
end;

procedure TTileEditFrame.TileEditViewSelectionChanged(
  Sender: TCustomTileMapView);
begin
 if Sender.MultiSelected then
 begin
  with Sender.SelectionRect do
   StatusBar.Panels[5].Text := WideFormat('Selection: (%d, %d) %dx%d',
                [Left, Top, Right - Left, Bottom - Top]);
 end else
  StatusBar.Panels[5].Text := '';
end;

function TTileEditFrame.TileImageData: Pointer;
begin
 Result := FPicData.ImageData;
end;

function TTileEditFrame.TileImageBackup: Pointer;
var
 Sz: Integer;
begin
 with FPicData do
 begin
  Sz := ImageSize;
  GetMem(Result, Sz);
  Move(ImageData^, Result^, Sz);
 end;
end;

procedure TTileEditFrame.ContinguousSelect(X, Y: Integer;
  Shift: TShiftState);
var
 SelBuf: PBoolean;
 c: Byte;
 SelInc: Boolean;
 W, R, H: Integer;
 Buf: PByteArray;

 procedure Beam(XInc, YInc, X, Y: Integer);
 var
  SP: PBoolean;
  Incr: Integer;
  PV: PInteger;
 begin
  if XInc <> 0 then
  begin
   Incr := XInc;
   PV := @X;
  end else
  begin
   Incr := YInc;
   PV := @Y;
  end;
  repeat
   Inc(PV^, Incr);
   if (X < 0) or
      (Y < 0) or
      (X >= W) or
      (Y >= H) then Break;

   SP := SelBuf;
   Inc(SP, Y * W + X);
   if (SP^ <> SelInc) and
      (Buf[((H - 1) - Y) * R + X] = c) then
   begin
    SP^ := SelInc;
    if YInc >= 0 then
     Beam(0, -1, X, Y);
    if YInc <= 0 then
     Beam(0, +1, X, Y);
    if XInc >= 0 then
     Beam(-1, 0, X, Y);
    if XInc <= 0 then
     Beam(+1, 0, X, Y);
   end else
    Break;
  until False;
 end;

begin
 if FTile <> nil then
 begin
  Buf := FPicData.ImageData;
  if Buf <> nil then
   with TileEditView, FPicData do
   begin
    SelBuf := SelectionBuffer;
    W := Width;
    R := Width + WidthRemainder;
    H := Height;

    c := Buf[((Height - 1) - Y) * R + X];

    SelInc := Shift <> SelectExcludeShiftState;
    if Shift = SelectNewShiftState then
     FillChar(SelBuf^, W * H, 0);

    PBoolean(Integer(SelBuf) + (Y * W + X))^ := SelInc;

    Beam(0, -1, X, Y);
    Beam(0, +1, X, Y);
    Beam(-1, 0, X, Y);
    Beam(+1, 0, X, Y);

    UpdateSelection;
   end;
 end;
end;

procedure TTileEditFrame.FloodFill(X, Y: Integer; Mode: Boolean);
var
 SelBuf: PBoolean;
 c, cc: Byte;
 W, R, H: Integer;
 Buf: PByteArray;

 procedure Beam(XInc, YInc, X, Y: Integer);
 var
  Incr: Integer;
  PV: PInteger;
  PB: PByte;
 begin
  if XInc <> 0 then
  begin
   Incr := XInc;
   PV := @X;
  end else
  begin
   Incr := YInc;
   PV := @Y;
  end;
  repeat
   Inc(PV^, Incr);
   if (X < 0) or
      (Y < 0) or
      (X >= W) or
      (Y >= H) then Break;

   PB := Addr(Buf[((H - 1) - Y) * R + X]);
   if ((SelBuf = nil) or
      (PByteArray(SelBuf)[Y * W + X] <> 0)) and
      (PB^ = cc) then
   begin
    PB^ := c;
    if YInc >= 0 then
     Beam(0, -1, X, Y);
    if YInc <= 0 then
     Beam(0, +1, X, Y);
    if XInc >= 0 then
     Beam(-1, 0, X, Y);
    if XInc <= 0 then
     Beam(+1, 0, X, Y);
   end else
    Break;
  until False;
 end;

var
 PB: PByte;
begin
 if FTile <> nil then
 begin
  Buf := FPicData.ImageData;
  if Buf <> nil then
   with TileEditView, FPicData do
   begin
    if SomethingSelected then
     SelBuf := SelectionBuffer else
     SelBuf := nil;
    W := Width;
    R := Width + WidthRemainder;
    H := Height;

    if (SelBuf = nil) or PBoolean(Integer(SelBuf) + (Y * W + X))^ then
    begin
     c := ColorIndexValue[Mode];

     PB := Addr(Buf[((Height - 1) - Y) * R + X]);
     cc := PB^;
     if cc <> c then
     begin
      PB^ := c;

      Beam(0, -1, X, Y);
      Beam(0, +1, X, Y);
      Beam(-1, 0, X, Y);
      Beam(+1, 0, X, Y);

      ContentsChanged;
      Invalidate;
     end;
    end;
   end;
 end;
end;

procedure TTileEditFrame.MagicWand(X, Y: Integer; Shift: TShiftState);
var
 c: Byte;
 Contiguous, NewValue: Boolean;
 XX, YY: Integer;
 RowStride: Integer;
 PB: PByte;
 SP: PBoolean;
begin
 if FTile <> nil then
 begin
  TileEditView.PasteBufferApply;
  Contiguous := ssCtrl in Shift;
  if Contiguous then
   Shift := Shift - [ssCtrl];
  with TileEditView do
  if (Shift = SelectNewShiftState) or
     (Shift = SelectIncludeShiftState) or
     (Shift = SelectExcludeShiftState) then
  begin
   if not Contiguous then
   begin
    with FPicData do
    begin
     c := PByteArray(ImageData)[((Height - 1) - Y) * (Width + WidthRemainder) + X];
     SP := SelectionBuffer;
     NewValue := Shift <> SelectExcludeShiftState;
     if Shift = SelectNewShiftState then
      FillChar(SP^, Width * Height, 0);
     RowStride := -(Width * 2 + WidthRemainder);
     PB := ImageData;
     Inc(PB, (Height - 1) * (Width + WidthRemainder));
     for YY := 0 to Height - 1 do
     begin
      for XX := 0 to Width - 1 do
      begin
       if PB^ = c then
        SP^ := NewValue;
       Inc(SP);
       Inc(PB);
      end;
      Inc(PB, RowStride);
     end;
     UpdateSelection;
    end
   end else
    ContinguousSelect(X, Y, Shift);
  end;
 end;
end;

procedure TTileEditFrame.SetPixel(X, Y: Integer; Mode: Boolean);
var
 PB: PByte;
begin
 if (FTile <> nil) and (FPicData.ImageData <> nil) then
  with FPicData do
  if (X >= 0) and (Y >= 0) and
     (X < Width) and (Y < Height) then
  begin
   PB := ImageData;
   Inc(PB, ((Height - 1) - Y) * (Width + WidthRemainder) + X);
   PB^ := ColorIndexValue[Mode];
  end;
end;

function TTileEditFrame.GetIndexValue(Mode: Boolean): Integer;
begin
 Result := ColorIndex[Mode];
 if FTile <> nil then
 begin
  if TTileSet(FTile.Owner).DrawFlags and DRAW_PIXEL_MODIFY <> 0 then
   Inc(Result, FTile.FirstColor);
 end
end;

procedure TTileEditFrame.ErasePixel(X, Y: Integer);
var
 PB: PByte;
begin
 if (FTile <> nil) and (FPicData.ImageData <> nil) then
  with FPicData do
  if (X >= 0) and (Y >= 0) and
     (X < Width) and (Y < Height) then
  begin
   PB := ImageData;
   Inc(PB, ((Height - 1) - Y) * (Width + WidthRemainder) + X);
   with TTileSet(FTile.Owner) do
   begin
    PB^ := TransparentColor;
    if DrawFlags and DRAW_PIXEL_MODIFY <> 0 then
     Inc(PB^, FTile.FirstColor);
   end;
  end;
end;

function TTileEditFrame.TileImageSize: Integer;
begin
 Result := FPicData.ImageSize;
end;

procedure TTileEditFrame.PickColor(X, Y: Integer; Mode: Boolean);
var
 PB: PByte;
begin
 if (FTile <> nil) and (FPicData.ImageData <> nil) then
  with FPicData do
  if (X >= 0) and (Y >= 0) and
     (X < Width) and (Y < Height) then
  begin
   PB := ImageData;
   Inc(PB, ((Height - 1) - Y) * (Width + WidthRemainder) + X);
   with TTileSet(FTile.Owner) do
   begin
    if DrawFlags and DRAW_PIXEL_MODIFY <> 0 then
     ColorIndex[Mode] := PB^ - FTile.FirstColor else
     ColorIndex[Mode] := PB^;
   end;
  end; 
end;

procedure TTileEditFrame.FillSelected;
var
 SelBuf: PBoolean;
 c: Byte;
 W, R, H, X, Y: Integer;
 Buf: PByteArray;

begin
 if FTile <> nil then
 begin
  Buf := FPicData.ImageData;
  if Buf <> nil then
   with TileEditView, FPicData do
    if SomethingSelected then
    begin
     SelBuf := SelectionBuffer;

     W := Width;
     R := Width + WidthRemainder;
     H := Height;
     c := ColorIndexValue[True];

     for Y := 0 to H - 1 do
      for X := 0 to W - 1 do
      begin
       if SelBuf^ then
        Buf[((Height - 1) - Y) * R + X] := c;
       Inc(SelBuf);
      end;

     ContentsChanged;
     Invalidate;
    end;
 end;
end;

end.
