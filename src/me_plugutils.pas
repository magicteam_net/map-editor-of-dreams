unit me_plugutils;

interface

uses
 Windows, SysUtils, Classes, TntClasses, NodeLst, me_plugin, MyUtils;

type
  TLoadingParametersProc = procedure(Data: PParametersData); stdcall;
  TSavingParametersProc = procedure(Data: PParametersData); stdcall;
  TLoadProc = function(Data: PProcessData): LongInt; stdcall;
  TSaveProc = function(Data: PProcessData): LongInt; stdcall;
  TDescriptionProc = function(var Str: PWideChar): LongInt; stdcall;

  TPlugin = class(TNode)
   private
    FFileName: WideString;
    FFullFileName: WideString;
    FHandle: THandle;
    FDescription: TDescriptionProc;
    FLoadingParameters: TLoadingParametersProc;
    FSavingParameters: TSavingParametersProc;
    FLoad: TLoadProc;
    FSave: TSaveProc;
   public
    property Description: TDescriptionProc read FDescription;
    property LoadingParameters: TLoadingParametersProc read FLoadingParameters;
    property SavingParameters: TSavingParametersProc read FSavingParameters;
    property Load: TLoadProc read FLoad;
    property Save: TSaveProc read FSave;
    
    property FileName: WideString read FFileName;
    property FullFileName: WideString read FFullFileName;

    function LoadPlugin(const FileName: WideString): Boolean;
    destructor Destroy; override;
  end;

  TPluginList = class(TNodeList)
   private
    function GetPlugin(Index: Integer): TPlugin;
   protected
    procedure Initialize; override;
   public
    function FindByName(const FName: WideString): TPlugin;
    function FindIndexByName(const FName: WideString): Integer;    

    property Items[Index: Integer]: TPlugin read GetPlugin;
    procedure Load(Path: WideString);
  end;

implementation

{ TPluginList }

function TPluginList.FindByName(const FName: WideString): TPlugin;
begin
 Result := RootNode as TPlugin;
 while Result <> nil do
 begin
  if Result.FFileName = FName then
   Exit;
  Result := Result.Next as TPlugin;
 end;
end;

function TPluginList.FindIndexByName(const FName: WideString): Integer;
var
 Plg: TPlugin;
begin
 Plg := FindByName(FName);
 if Plg <> nil then
  Result := Plg.Index else
  Result := -1;
end;

function TPluginList.GetPlugin(Index: Integer): TPlugin;
begin
 Result := Nodes[Index] as TPlugin;
end;

procedure TPluginList.Initialize;
begin
 inherited;
 FNodeClassAssignable := False;
 FNodeClass := TPlugin;
end;

procedure TPluginList.Load(Path: WideString);
var
 SR: TSearchRecW;
 Temp: TPlugin;
begin
 Clear;
 Path := WideIncludeTrailingPathDelimiter(Path);
 if WideFindFirst(Path + '*.dll', faAnyTrueFile, SR) = 0 then
 begin
  repeat
   Temp := TPlugin.Create;
   if Temp.LoadPlugin(Path + SR.Name) then
    AddCreated(Temp, True) else
    Temp.Free;
  until WideFindNext(SR) <> 0;
  WideFindClose(SR);
 end;
end;

{ TPlugin }

destructor TPlugin.Destroy;
begin
 if FHandle <> 0 then
  FreeLibrary(FHandle);
 inherited;
end;

function TPlugin.LoadPlugin(const FileName: WideString): Boolean;
var
 H: THandle;
begin
 if FHandle <> 0 then
 begin
  FreeLibrary(FHandle);
  FHandle := 0;
 end;
 Result := False;
 H := LoadLibraryW(Pointer(FileName));
 if H <> 0 then
 begin
  @FDescription := GetProcAddress(H, 'Description');
  @FLoadingParameters := GetProcAddress(H, 'LoadingParameters');
  @FSavingParameters := GetProcAddress(H, 'SavingParameters');
  @FLoad := GetProcAddress(H, 'Load');
  @FSave := GetProcAddress(H, 'Save');

  if (Addr(FDescription) = nil) or
     not (((Addr(FLoadingParameters) <> nil) and
           (Addr(FLoad) <> nil)) or
     ((Addr(FSavingParameters) <> nil) and
      (Addr(FSave) <> nil))) then
  begin
   FreeLibrary(H);
   FHandle := 0;
   Exit;
  end;
  FFullFileName := FileName;
  FFileName := WideExtractFileName(FileName);
  FHandle := H;
  Result := True;
 end;
end;

end.
