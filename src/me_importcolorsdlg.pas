unit me_importcolorsdlg;

interface

uses Windows, SysUtils, TntForms, Dialogs, TntDialogs, TntClasses, Classes,
  Graphics, Forms, Controls, StdCtrls,
  Buttons, ExtCtrls, BitmapEx, TileMapView, VirtualTrees, PropEditor,
  ComCtrls, TntComCtrls, PropContainer, TntStdCtrls;

type
  TImportColorsDlg = class(TTntForm)
    OKBtn: TTntButton;
    CancelBtn: TTntButton;
    Bevel: TBevel;
    PalPropEditor: TPropertyEditor;
    VScrollBar: TScrollBar;
    PaletteView: TTileMapView;
    ViewPanel: TPanel;
    StatusBar: TTntStatusBar;
    HScrollBar: TScrollBar;
    HScrollBarPanel: TPanel;
    OpenDialog: TTntOpenDialog;
    procedure TntFormCreate(Sender: TObject);
    procedure TntFormDestroy(Sender: TObject);
    procedure PaletteViewDrawCell(Sender: TCustomTileMapView;
      const Rect: TRect; CellX, CellY: Integer);
    procedure PaletteViewSelectionChanged(Sender: TCustomTileMapView);
    procedure ValueChange(Sender: TPropertyListItem);
    procedure PaletteViewMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure ScrollBarChange(Sender: TObject);
    procedure HScrollBarScroll(Sender: TObject; ScrollCode: TScrollCode;
      var ScrollPos: Integer);
    procedure VScrollBarScroll(Sender: TObject; ScrollCode: TScrollCode;
      var ScrollPos: Integer);
    procedure PalPropEditorEllipsisClick(Sender: TCustomPropertyEditor;
      Node: PVirtualNode; var Accepted: Boolean);
    procedure PalPropEditorExit(Sender: TObject);
    procedure PalPropEditorEnter(Sender: TObject);
  private
    FColFmt: TColorFormat;
    FSaveHeight: Integer;
    procedure AdjustScrollBars;
    procedure SetFileName(const AValue: WideString);
    function GetFileName: WideString;
    function GetColorFormat: AnsiString;
    function GetColorsCount: Integer;
    function GetSelectedIndex: Integer;
    function GetSelectedOffset: Int64;
    function GetTransparentIndex: Integer;
    procedure SetColorFormat(const Value: AnsiString);
    procedure SetColorsCount(Value: Integer);
    procedure SetSelectedIndex(Value: Integer);
    procedure SetSelectedOffset(const Value: Int64);
    procedure SetTransparentIndex(Value: Integer);
    function GetOffset: Int64;
    procedure SetOffset(const Value: Int64);
    procedure UpdateColorBuffer;
    procedure UpdateRGB;
    procedure UpdateSelectedOffset;
  public
    ColorBuffer: array[0..256 * 4 - 1] of Byte;
    Stream: TStream;
    property FileName: WideString read GetFileName write SetFileName;
    property ColorFormat: AnsiString read GetColorFormat write SetColorFormat;
    property ColorsCount: Integer read GetColorsCount write SetColorsCount;
    property TransparentIndex: Integer read GetTransparentIndex write SetTransparentIndex;
    property SelectedIndex: Integer read GetSelectedIndex write SetSelectedIndex;
    property SelectedOffset: Int64 read GetSelectedOffset write SetSelectedOffset;
    property Offset: Int64 read GetOffset write SetOffset;
  end;

var
  ImportColorsDlg: TImportColorsDlg;
  LastColFmtStr: AnsiString = 'R8G8B8';
  LastTrnsIdx: Integer = 0;
  LastColCnt: Integer = 256;

function ImportColorsDialog(const AFileName: WideString): TColorTable;

implementation

uses HexUnit;

const
 TAG_FNAME = 1;
 TAG_CLFMT = 2;
 TAG_CLCNT = 3;
 TAG_TRIDX = 4;
 TAG_OFFST = 5;
 TAG_SOFFS = 6;
 TAG_S_IDX = 7;

 IDX_FNAME = 0;
 IDX_FSIZE = 1;
 IDX_CLFMT = 2;
 IDX_CLCNT = 3;
 IDX_TRIDX = 4;
 IDX_OFFST = 5;
 IDX_SOFFS = 6;
 IDX_S_IDX = 7;
 IDX_VALUE = 8;
 IDX_S_RGB = 9;


{$R *.dfm}

function ImportColorsDialog(const AFileName: WideString): TColorTable;
begin
 try
  Application.CreateForm(TImportColorsDlg, ImportColorsDlg);
  ImportColorsDlg.FileName := AFileName;

  if ImportColorsDlg.ShowModal = mrOk then
  begin
   Result := TColorTable.Create;
   Result.ColorFormat.SetFormat(LastColFmtStr);
   Result.ColorsCount := LastColCnt;
   Result.TransparentIndex := LastTrnsIdx;
   Move(ImportColorsDlg.ColorBuffer, Result.ColorData^, Result.TableSize);
  end else
   Result := nil;
 finally
  FreeAndNil(ImportColorsDlg);
 end;
end;

procedure TImportColorsDlg.SetFileName(const AValue: WideString);
begin
 PalPropEditor.PropertyList.Properties[IDX_FNAME].ValueStr := AValue;
end;

procedure TImportColorsDlg.TntFormCreate(Sender: TObject);
begin
 FSaveHeight := ViewPanel.Height - 12;
 FColFmt := TColorFormat.Create(LastColFmtStr);
 with PalPropEditor, PropertyList do
 begin
  OnValueChange := Self.ValueChange;
  AddFilePick('File name', '', 'All Files (*.*)|*.*', '', '').UserTag := TAG_FNAME;
  AddDecimal('File size', 0, 0, High(Int64)).ReadOnly := True;
  AddString('Color format', '').UserTag := TAG_CLFMT;
  AddDecimal('Colors count', 2, 2, 256).UserTag := TAG_CLCNT;
  AddDecimal('Transparent index', 0, 0, 255).UserTag := TAG_TRIDX;
  AddHexadecimal('Offset', 0, 0, High(Int64), 8).UserTag := TAG_OFFST;
  AddHexadecimal('Selected offset', 0, 0, 0, 8).UserTag := TAG_SOFFS;
  AddDecimal('Selected index', 0, 0, 255).UserTag := TAG_S_IDX;
  AddString('Selected value', '', False);
  AddString('', '', False);

  RootNodeCount := Count;
 end;
 Offset := 0;
 ColorFormat := LastColFmtStr;
 ColorsCount := LastColCnt;
 TransparentIndex := LastTrnsIdx;
 SelectedIndex := 0;
 SelectedOffset := 0;
 Top := (Screen.Height - Height) shr 1;
end;

procedure TImportColorsDlg.TntFormDestroy(Sender: TObject);
begin
 Stream.Free;
 FColFmt.Free;
end;

procedure TImportColorsDlg.PaletteViewDrawCell(Sender: TCustomTileMapView;
  const Rect: TRect; CellX, CellY: Integer);
var
 Color: TColor;
 RGBQ: LongWord;
begin
 with Sender, Canvas do
 begin
  if (Stream = nil) or (InternalIndex >= LastColCnt) then
  begin
   Brush.Style := bsDiagCross;
   Color := clWhite;
  end else
  begin
   Brush.Style := bsSolid;
   RGBQ := FColFmt.ToRGBQuad(PByteArray(@ColorBuffer)[InternalIndex * FColFmt.ColorSize]);
   Color := RGBZ_ColorFormat.FromRGBQuad(RGBQ);
  end;
  Brush.Color := Color;
  FillRect(Rect);
 end;
end;

procedure TImportColorsDlg.PaletteViewSelectionChanged(Sender: TCustomTileMapView);
var
 I, Diff: Integer;
begin
 if Stream <> nil then
 begin
  with PalPropEditor.PropertyList.Properties[IDX_S_IDX], Sender.SelectedCell do
  begin
   I := Y * 16 + X;
   Diff := (LastColCnt - 1) - I;
   Changing := True;
   if Diff >= 0 then
   begin
    Value := I;
    Changing := False;
   end else
   begin
    I := LastColCnt - 1;
    Value := not I;
    Changing := False;
    Value := I;
   end;

   UpdateRGB;
   if Diff >= 0 then
    UpdateSelectedOffset;
  end;
 end;
end;

function TImportColorsDlg.GetFileName: WideString;
begin
 Result := PalPropEditor.PropertyList.Properties[IDX_FNAME].ValueStr;
end;

function TImportColorsDlg.GetColorFormat: AnsiString;
begin
 Result := PalPropEditor.PropertyList.Properties[IDX_CLFMT].ValueStr;
end;

function TImportColorsDlg.GetColorsCount: Integer;
begin
 Result := PalPropEditor.PropertyList.Properties[IDX_CLCNT].Value;
end;

function TImportColorsDlg.GetSelectedIndex: Integer;
begin
 Result := PalPropEditor.PropertyList.Properties[IDX_S_IDX].Value;
end;

function TImportColorsDlg.GetSelectedOffset: Int64;
begin
 Result := PalPropEditor.PropertyList.Properties[IDX_SOFFS].Value;
end;

function TImportColorsDlg.GetTransparentIndex: Integer;
begin
 Result := PalPropEditor.PropertyList.Properties[IDX_TRIDX].Value;
end;

procedure TImportColorsDlg.SetColorFormat(const Value: AnsiString);
begin
 PalPropEditor.PropertyList.Properties[IDX_CLFMT].ValueStr := Value;
end;

procedure TImportColorsDlg.SetColorsCount(Value: Integer);
begin
 PalPropEditor.PropertyList.Properties[IDX_CLCNT].Value := Value;
end;

procedure TImportColorsDlg.SetSelectedIndex(Value: Integer);
begin
 PalPropEditor.PropertyList.Properties[IDX_S_IDX].Value := Value;
end;

procedure TImportColorsDlg.SetSelectedOffset(const Value: Int64);
begin
 PalPropEditor.PropertyList.Properties[IDX_SOFFS].Value := Value;
end;

procedure TImportColorsDlg.SetTransparentIndex(Value: Integer);
begin
 PalPropEditor.PropertyList.Properties[IDX_TRIDX].Value := Value;
end;

procedure TImportColorsDlg.ValueChange(Sender: TPropertyListItem);
var
 X, H: Integer;
 X64: Int64;
begin
 case Sender.UserTag of
  TAG_FNAME:
  begin
   Stream.Free;
   try
    Stream := TTntFileStream.Create(Sender.ValueStr, fmOpenRead or fmShareDenyWrite);
   except
    WideMessageDlg('Error loading file', mtError, [mbOk], 0);
    OkBtn.Enabled := False;
    Stream := nil;
    Sender.Changing := True;
    Sender.ValueStr := '';
    Sender.Changing := False;
   end;

   OkBtn.Enabled := Stream <> nil;

   AdjustScrollBars;

   with PalPropEditor.PropertyList do
   begin
    with Properties[IDX_FSIZE] do
    begin
     Changing := True;
     if Stream <> nil then
      Value := Stream.Size else
      Value := 0;
     Changing := False;
    end;

    Properties[IDX_SOFFS].ValueMax := Stream.Size - 1;
   end;

   UpdateColorBuffer;
   UpdateRGB;
  end;
  TAG_CLFMT:
  begin
   try
    FColFmt.FormatString := Sender.ValueStr;

    Sender.Changing := True;
    Sender.ValueStr := FColFmt.FormatString;
    Sender.Changing := False;    

    AdjustScrollBars;
        
    if Stream <> nil then
    begin
     X := LastColCnt * FColFmt.ColorSize;
     X64 := Stream.Size;
     if Offset + X <= X64 then
     begin
      UpdateSelectedOffset;
      UpdateRGB;
     end else
      Offset := X64 - X;
    end else
    begin
     UpdateSelectedOffset;
     UpdateRGB;
     AdjustScrollBars;
    end;

    PaletteView.Invalidate;
   except
    WideMessageDlg('Unknown color format', mtError, [mbOk], 0);
    Sender.Changing := True;
    Sender.ValueStr := LastColFmtStr;
    Sender.Changing := False;
    FColFmt.FormatString := LastColFmtStr;
   end;
   LastColFmtStr := Sender.ValueStr;
  end;
  TAG_CLCNT:
  begin
   LastColCnt := Sender.Value;
   if Stream <> nil then
   begin
    X64 := Stream.Size div FColFmt.ColorSize;
    if LastColCnt > X64 then
    begin
     LastColCnt := X64;
     Sender.Changing := True;
     Sender.Value := LastColCnt;
     Sender.Changing := False;
    end;

    with PalPropEditor.PropertyList.Properties[IDX_S_IDX] do
    begin
     ValueMax := LastColCnt - 1;
     if SelectedIndex > ValueMax then
      SelectedIndex := ValueMax;
    end;
   end;
   H := (LastColCnt + 15) shr 4;
   X := 12 * H;
   ViewPanel.Height := FSaveHeight + X;
//   Bevel.Height := (ViewPanel.Top - Bevel.Top) + ViewPanel.Height + 8;
//   Self.ClientHeight := Bevel.Top + Bevel.Height + 8;
   PaletteView.SetMapSize(16, H);
   AdjustScrollBars;

   if Stream <> nil then
   begin
    X := LastColCnt * FColFmt.ColorSize;
    X64 := Stream.Size;
    if Offset + X > X64 then
     Offset := X64 - X;
   end;
  end;
  TAG_TRIDX: LastTrnsIdx := Sender.Value;
  TAG_S_IDX:
  begin
   with Sender do
    PaletteView.Selected[Value and 15, Value shr 4] := True;
  end;
  TAG_SOFFS:
  begin
   X64 := Sender.Value - SelectedIndex * FColFmt.ColorSize;
   if Offset <> X64 then
    Offset := X64;
  end;
  TAG_OFFST:
  if Stream <> nil then
  begin
   X := LastColCnt * FColFmt.ColorSize;
   X64 := Stream.Size;
   if Sender.Value + X > X64 then
   begin
    Sender.Changing := True;
    Sender.Value := X64 - X;
    Sender.Changing := False;
   end;
   if X64 > MaxInt then
   begin
    if Sender.Value = 0 then
    begin
     HScrollBar.Position := 0;
     VScrollBar.Position := 0;
    end else
    if Sender.Value = X64 then
    begin
     HScrollBar.Position := 2;
     VScrollBar.Position := 2;
    end else
    begin
     HScrollBar.Position := 1;
     VScrollBar.Position := 1;
    end;
   end else
   if X64 > X then
   begin
    X := Sender.Value;
    HScrollBar.Position := X;
    VScrollBar.Position := X;
   end;
   Stream.Position := Sender.Value;
   UpdateColorBuffer;
   UpdateRGB;
   UpdateSelectedOffset;
  end;
 end;
 PalPropEditor.InvalidateChildren(nil, False);
 PaletteView.Invalidate;
end;

function TImportColorsDlg.GetOffset: Int64;
begin
 if Stream <> nil then
  Result := Stream.Position else
  Result := 0;
end;

procedure TImportColorsDlg.SetOffset(const Value: Int64);
begin
 PalPropEditor.PropertyList.Properties[IDX_OFFST].Value := Value;
end;

type
 TABGR = packed record
  A: Byte;
  B: Byte;
  G: Byte;
  R: Byte;
 end;

procedure TImportColorsDlg.PaletteViewMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
var
 I, Sz: Integer;
 XX: LongWord;
 WFmt: WideString;
 Ptr: PByte;
 ABGR: TABGR absolute XX;
begin
 if Stream <> nil then
 begin
  with PaletteView do
   if (TileX >= 0) and (TileX < MapWidth) and
      (TileY >= 0) and (TileY < MapHeight) then
    I := TileY * MapWidth + TileX else
    I := -1;
  if (I >= 0) and (I < LastColCnt) then
  begin
   StatusBar.Panels[0].Text := WideFormat('I: %d', [I]);
   if Stream <> nil then
   begin
    Sz := FColFmt.ColorSize shl 1;
    WFmt := WideFormat('Value: %%%d.%dx', [Sz, Sz]);
    XX := 0;
    Ptr := Addr(ColorBuffer[I * FColFmt.ColorSize]);
    Move(Ptr^, XX, FColFmt.ColorSize);
    if FColFmt.ByteSwap then
     case FColFmt.ColorSize of
      2: XX := SwapWord(XX);
      3: XX := SwapLong(XX) shr 8;
      4: XX := SwapLong(XX);
     end;
    StatusBar.Panels[1].Text := WideFormat(WFmt, [XX]);
    with TRGBQuad(FColFmt.ToRGBQuad(Ptr^)) do
    begin
     ABGR.R := rgbRed;
     ABGR.G := rgbGreen;
     ABGR.B := rgbBlue;
     ABGR.A := rgbReserved;
    end;
    if not FColFmt.Alpha then
    begin
     WFmt := 'RGB: %6.6x';
     XX := XX shr 8;
    end else
     WFmt := 'RGBA: %8.8x';
    StatusBar.Panels[2].Text := WideFormat(WFmt, [XX]);
   end else
   begin
    StatusBar.Panels[1].Text := '';
    StatusBar.Panels[2].Text := '';
   end;
   Exit;
  end;
 end;
 StatusBar.Panels[0].Text := '';
 StatusBar.Panels[1].Text := '';
 StatusBar.Panels[2].Text := '';
end;

procedure TImportColorsDlg.ScrollBarChange(Sender: TObject);
begin
 PaletteView.SetFocus;
 if Stream.Size - LastColCnt * FColFmt.ColorSize <= MaxInt then
 begin
  with Sender as TScrollBar do
   Offset := Position;
 end;
end;

procedure TImportColorsDlg.AdjustScrollBars;
var
 X64: Int64;
begin
 if Stream <> nil then
 begin
  X64 := Stream.Size - LastColCnt * FColFmt.ColorSize;
  if X64 > 0 then
  begin
   with HScrollBar do
   begin
    if X64 > MaxInt then
    begin
     Min := 0;
     Max := 2;
     Position := 0;
     LargeChange := 1;
     SmallChange := 1;
    end else
    begin
     Min := 0;
     Max := X64;
     Position := 0;
     SmallChange := FColFmt.ColorSize;
     LargeChange := HexUnit.Min(LastColCnt, 16) * FColFmt.ColorSize;
    end;
    Enabled := True;
   end;

   with VScrollBar do
   begin
    if X64 > MaxInt then
    begin
     Min := 0;
     Max := 2;
     Position := 0;
     LargeChange := 1;
     SmallChange := 1;
    end else
    begin
     Min := 0;
     Max := HScrollBar.Max;
     Position := 0;
     SmallChange := HScrollBar.LargeChange;
     LargeChange := LastColCnt * FColFmt.ColorSize;
    end;
    Enabled := True;
   end;
   Exit;
  end;
 end;
 HScrollBar.Enabled := False;
 VScrollBar.Enabled := False;
end;

procedure TImportColorsDlg.HScrollBarScroll(Sender: TObject;
  ScrollCode: TScrollCode; var ScrollPos: Integer);
var
 X64: Int64;
begin
 X64 := Stream.Size - LastColCnt * FColFmt.ColorSize;
 if X64 > MaxInt then
 begin
  case ScrollCode of
   scLineUp:   Offset := Offset - FColFmt.ColorSize;
   scLineDown: Offset := Offset + FColFmt.ColorSize;
   scPageUp:   Offset := Offset - Min(LastColCnt, 16) * FColFmt.ColorSize;
   scPageDown: Offset := Offset + Min(LastColCnt, 16) * FColFmt.ColorSize;
   scPosition,
   scTrack,
   scTop,
   scBottom,
   scEndScroll: { do nothing };
  end;
  if Offset = 0 then
   ScrollPos := 0 else
  if Offset = X64 then
   ScrollPos := 2 else
   ScrollPos := 1;
 end;
end;

procedure TImportColorsDlg.VScrollBarScroll(Sender: TObject;
  ScrollCode: TScrollCode; var ScrollPos: Integer);
var
 X64: Int64;
 CSZ: Integer;
begin
 CSZ := LastColCnt * FColFmt.ColorSize;
 X64 := Stream.Size - CSZ;
 if X64 > MaxInt then
 begin
  case ScrollCode of
   scLineUp:   Offset := Offset - Min(LastColCnt, 16) * FColFmt.ColorSize;
   scLineDown: Offset := Offset + Min(LastColCnt, 16) * FColFmt.ColorSize;
   scPageUp:   Offset := Offset - CSZ;
   scPageDown: Offset := Offset + CSZ;
   scPosition,
   scTrack,
   scTop,
   scBottom,
   scEndScroll: { do nothing };
  end;
  if Offset = 0 then
   ScrollPos := 0 else
  if Offset = X64 then
   ScrollPos := 2 else
   ScrollPos := 1;
 end;
end;

procedure TImportColorsDlg.PalPropEditorEllipsisClick(
  Sender: TCustomPropertyEditor; Node: PVirtualNode;
  var Accepted: Boolean);
begin
 OpenDialog.FileName := FileName;
 Accepted := OpenDialog.Execute;
 if Accepted then
  FileName := OpenDialog.FileName;
end;

procedure TImportColorsDlg.UpdateColorBuffer;
var
 SavePos: Int64;
begin
 SavePos := Stream.Position;
 Stream.Read(ColorBuffer, LastColCnt * FColFmt.ColorSize);
 Stream.Position := SavePos;
end;

procedure TImportColorsDlg.UpdateRGB;
var
 Sz: Integer;
 XX: LongWord;
 ABGR: TABGR absolute XX;
 WFmt: WideString;
 Ptr: PByte;
begin
 Ptr := Addr(ColorBuffer[SelectedIndex * FColFmt.ColorSize]);
 with PalPropEditor.PropertyList.Properties[IDX_VALUE] do
 begin
  Sz := FColFmt.ColorSize shl 1;
  WFmt := WideFormat('%%%d.%dx', [Sz, Sz]);
  XX := 0;
  Move(Ptr^, XX, FColFmt.ColorSize);
  if FColFmt.ByteSwap then
   case FColFmt.ColorSize of
    2: XX := SwapWord(XX);
    3: XX := SwapLong(XX) shr 8;
    4: XX := SwapLong(XX);
   end;
  Changing := True;
  ValueStr := WideFormat(WFmt, [XX]);
  Changing := False;
 end;
 with PalPropEditor.PropertyList.Properties[IDX_S_RGB] do
 begin
  with TRGBQuad(FColFmt.ToRGBQuad(Ptr^)) do
  begin
   ABGR.R := rgbRed;
   ABGR.G := rgbGreen;
   ABGR.B := rgbBlue;
   ABGR.A := rgbReserved;
  end;
  if not FColFmt.Alpha then
  begin
   WFmt := '%6.6x';
   XX := XX shr 8;
   Name := 'Selected RGB';
  end else
  begin
   WFmt := '%8.8x';
   Name := 'Selected RGBA';
  end;

  Changing := True;
  ValueStr := WideFormat(WFmt, [XX]);
  Changing := False;
 end;
 PalPropEditor.InvalidateChildren(nil, False);
end;

procedure TImportColorsDlg.UpdateSelectedOffset;
begin
 with PalPropEditor.PropertyList.Properties[IDX_SOFFS] do
 begin
  Changing := True;
  Value := Offset + SelectedIndex * FColFmt.ColorSize;
  Changing := False;
 end;
end;

procedure TImportColorsDlg.PalPropEditorExit(Sender: TObject);
begin
 (*with Sender as TPropertyEditor do
{ begin
  UserSelect := True;
  FocusedNode := nil; }
  CancelEditNode;
 {end;    }       *)
end;

procedure TImportColorsDlg.PalPropEditorEnter(Sender: TObject);
begin
{ with Sender as TPropertyEditor do
  if FocusedNode <> nil then
   EditNode(FocusedNode, 1) else
   FocusedNode := GetFirst;     }
end;

end.
