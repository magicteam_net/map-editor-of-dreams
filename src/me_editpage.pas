unit me_editpage;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, me_tilesfrm, me_mapfrm, ComCtrls, TntComCtrls;

type
  TMainEditPageFrame = class(TFrame)
    PageControl: TTntPageControl;
    MapPage: TTntTabSheet;
    BrushPage: TTntTabSheet;
    TileSetPage: TTntTabSheet;
    TilePage: TTntTabSheet;
    MapFrame: TMapFrame;
    BrushFrame: TMapFrame;
    TilesFrame: TTilesFrame;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

end.
