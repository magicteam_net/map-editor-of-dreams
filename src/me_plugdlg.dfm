object PluginDialog: TPluginDialog
  Left = 329
  Top = 239
  Width = 640
  Height = 458
  BorderIcons = [biSystemMenu]
  Color = clBtnFace
  Constraints.MinHeight = 280
  Constraints.MinWidth = 400
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = TntFormCreate
  OnDestroy = TntFormDestroy
  DesignSize = (
    624
    420)
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel: TBevel
    Left = 8
    Top = 8
    Width = 617
    Height = 408
    Anchors = [akLeft, akTop, akRight, akBottom]
    Shape = bsFrame
  end
  object RefreshButton: TSpeedButton
    Left = 16
    Top = 383
    Width = 25
    Height = 25
    Anchors = [akLeft, akBottom]
    Glyph.Data = {
      36030000424D3603000000000000360000002800000010000000100000000100
      18000000000000030000120B0000120B00000000000000000000FF00FFFF00FF
      FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
      FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
      00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
      FF00FFFF00FFFF00FFFF00FF43E93F39DE3335D92E3DE33845EB41FF00FFFF00
      FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF36DA2E35D92D35
      D92D35D92D35D92D35D92D36DB2FFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
      FF00FFFF00FF37DB3035D92D40E53AFF00FFFF00FFFF00FF3FE43A39DE32FF00
      FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF44EA3F36DA2E40E63BFF00FFFF
      00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
      FF00FF3BDF3337DB2EFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
      FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF38DC3038DC30FF00FFFF00FFFF
      00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
      FF00FF3CE03439DC30FF00FFFF00FFFF00FF47ED4239DC3039DC3039DC3039DC
      3039DC30FF00FFFF00FFFF00FFFF00FFFF00FF45EB4039DD3144E93EFF00FFFF
      00FFFF00FFFF00FF39DD3139DD3139DD3139DD31FF00FFFF00FFFF00FFFF00FF
      FF00FFFF00FF3CE0353ADE3144EA3EFF00FFFF00FFFF00FF3EE2373ADE313ADE
      313ADE31FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF3DE1353BDF323B
      DF323BDF323BDF323BDF323BDF333BDF323BDF32FF00FFFF00FFFF00FFFF00FF
      FF00FFFF00FFFF00FFFF00FF46EC413DE1353CDF3340E43847EC41FF00FFFF00
      FF3CDF33FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
      00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
      FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
      FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
      00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
    Margin = 1
    Spacing = 0
    OnClick = RefreshButtonClick
  end
  object PlgListBox: TListBox
    Left = 16
    Top = 160
    Width = 169
    Height = 216
    Style = lbVirtualOwnerDraw
    Anchors = [akLeft, akTop, akBottom]
    ItemHeight = 13
    TabOrder = 0
    OnClick = PlgListBoxClick
    OnDataFind = PlgListBoxDataFind
    OnDrawItem = PlgListBoxDrawItem
  end
  object PropEditor: TPropertyEditor
    Left = 192
    Top = 16
    Width = 425
    Height = 392
    BoldChangedValues = False
    Anchors = [akLeft, akTop, akRight, akBottom]
    ScrollBarOptions.ScrollBars = ssVertical
    TabOrder = 1
    PropertyWidth = 180
    ValueWidth = 241
    OnEllipsisClick = PropEditorEllipsisClick
    Columns = <
      item
        Margin = 0
        Options = [coEnabled, coParentBidiMode, coParentColor, coResizable, coVisible, coFixed]
        Position = 0
        Width = 180
      end
      item
        Margin = 0
        Options = [coEnabled, coParentBidiMode, coParentColor, coResizable, coVisible, coFixed]
        Position = 1
        Width = 241
      end>
  end
  object OkButton: TTntButton
    Left = 48
    Top = 383
    Width = 65
    Height = 25
    Anchors = [akLeft, akBottom]
    ModalResult = 1
    TabOrder = 2
  end
  object CancelButton: TTntButton
    Left = 120
    Top = 383
    Width = 65
    Height = 25
    Anchors = [akLeft, akBottom]
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 3
  end
  object DescriptionMemo: TTntMemo
    Left = 16
    Top = 16
    Width = 169
    Height = 137
    BevelKind = bkTile
    Color = clBtnFace
    ReadOnly = True
    TabOrder = 4
  end
  object OpenDialog: TTntOpenDialog
    Left = 184
    Top = 88
  end
  object SaveDialog: TTntSaveDialog
    Left = 184
    Top = 136
  end
end
