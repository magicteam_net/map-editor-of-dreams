unit me_tilesloaddlg;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, TntDialogs, StdCtrls, TntStdCtrls, Buttons, TntButtons, Spin,
  TntExtCtrls;

type
  TLoadTilesDialog = class(TForm)
    OKBtn: TTntButton;
    CancelBtn: TTntButton;
    OpenDialog: TTntOpenDialog;
    L1: TTntLabel;
    CustomTilesFileEdit: TTntEdit;
    CustomTilesFileButton: TTntSpeedButton;
    L2: TTntLabel;
    ColorInfoFileEdit: TTntEdit;
    ColorInfoFileButton: TTntSpeedButton;
    LoadingModeGroup: TTntRadioGroup;
    TilesCountEdit: TSpinEdit;
    TilesCountLabel: TTntLabel;
    procedure BrowseButtonClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

function LoadTilesDialogExecute(var F1, F2: WideString; var Mode, Cnt: Integer): Boolean;

implementation

{$R *.dfm}

procedure TLoadTilesDialog.BrowseButtonClick(Sender: TObject);
var
 Edit: TTntEdit;
begin
 if (Sender as TTntSpeedButton).Tag = 0 then
  Edit := CustomTilesFileEdit else
  Edit := ColorInfoFileEdit;
 OpenDialog.FileName := Edit.Text;
 if OpenDialog.Execute then
 begin
  Edit.Text := OpenDialog.FileName;
  Edit.SelectAll;
  Edit.SetFocus;
 end;
end;

function LoadTilesDialogExecute(var F1, F2: WideString; var Mode, Cnt: Integer): Boolean;
var
 Dlg: TLoadTilesDialog;
begin
 Application.CreateForm(TLoadTilesDialog, Dlg);
 try
  Result := Dlg.ShowModal = mrOk;
  if Result then
  begin
   F1 := Dlg.CustomTilesFileEdit.Text;
   F2 := Dlg.ColorInfoFileEdit.Text;
   Mode := Dlg.LoadingModeGroup.ItemIndex;
   Cnt := Dlg.TilesCountEdit.Value;
  end;
 finally
  Dlg.Free;
 end;
end;

end.
