object PaletteFrame: TPaletteFrame
  Left = 0
  Top = 0
  Width = 557
  Height = 348
  AutoScroll = False
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -9
  Font.Name = 'Small Fonts'
  Font.Style = []
  ParentFont = False
  TabOrder = 0
  object PaletteView: TTileMapView
    Left = 0
    Top = 14
    Width = 557
    Height = 334
    MapWidth = 0
    MapHeight = 0
    OffsetX = 0
    OffsetY = 0
    SelectionMode = selMulti
    GridStyle = psSolid
    GridMode = pmCopy
    SelectStyle = psSolid
    ScrollBarsAllwaysVisible = False
    OnDrawCell = PaletteViewDrawCell
    OnGetCellState = PaletteViewGetCellState
    OnGetBufCellState = PaletteViewGetBufCellState
    OnCopyMapData = PaletteViewCopyMapData
    OnPasteBufferApply = PaletteViewPasteBufferApply
    OnClearCell = PaletteViewClearCell
    OnBufClearCell = PaletteViewBufClearCell
    OnReleasePasteBuffer = PaletteViewReleasePasteBuffer
    OnCheckCompatibility = PaletteViewCheckCompatibility
    OnMakePasteBufferCopy = PaletteViewMakePasteBufferCopy
    Align = alClient
    Constraints.MinHeight = 32
    DragCursor = 983
    TabOrder = 0
    OnMouseMove = PaletteViewMouseMove
  end
  object PaletteStatusBar: TTntStatusBar
    Left = 0
    Top = 0
    Width = 557
    Height = 14
    Align = alTop
    Panels = <
      item
        Text = 'Color table'
        Width = 55
      end
      item
        Width = 55
      end
      item
        Width = 50
      end
      item
        Width = 50
      end>
    ParentColor = True
    ParentFont = True
    UseSystemFont = False
  end
end
