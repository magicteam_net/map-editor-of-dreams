object MainEditPageFrame: TMainEditPageFrame
  Left = 0
  Top = 0
  Width = 493
  Height = 314
  TabOrder = 0
  object PageControl: TTntPageControl
    Left = 0
    Top = 0
    Width = 493
    Height = 314
    ActivePage = MapPage
    Align = alClient
    TabOrder = 0
    object MapPage: TTntTabSheet
      Caption = 'Map'
      inline MapFrame: TMapFrame
        Left = 0
        Top = 0
        Width = 485
        Height = 286
        Align = alClient
        AutoScroll = False
        Color = clBtnFace
        ParentBackground = False
        ParentColor = False
        TabOrder = 0
        inherited MapStatusBar: TTntStatusBar
          Width = 485
          Panels = <
            item
              Text = 'Map'
              Width = 45
            end
            item
              Width = 55
            end
            item
              Width = 40
            end
            item
              Width = 40
            end
            item
              Width = 50
            end>
        end
        inherited MapEditView: TTileMapView
          Width = 485
          Height = 267
        end
      end
    end
    object BrushPage: TTntTabSheet
      Caption = 'Brush'
      inline BrushFrame: TMapFrame
        Left = 0
        Top = 0
        Width = 485
        Height = 283
        Align = alClient
        AutoScroll = False
        Color = clBtnFace
        ParentBackground = False
        ParentColor = False
        TabOrder = 0
        inherited MapStatusBar: TTntStatusBar
          Width = 485
          Panels = <
            item
              Text = 'Brush'
              Width = 45
            end
            item
              Width = 55
            end
            item
              Width = 40
            end
            item
              Width = 40
            end
            item
              Width = 50
            end>
        end
        inherited MapEditView: TTileMapView
          Width = 485
          Height = 264
        end
      end
    end
    object TileSetPage: TTntTabSheet
      Caption = 'Tile set'
      inline TilesFrame: TTilesFrame
        Left = 0
        Top = 0
        Width = 485
        Height = 286
        Align = alClient
        AutoScroll = False
        Color = clBtnFace
        ParentBackground = False
        ParentColor = False
        TabOrder = 0
        inherited StatusBar: TTntStatusBar
          Width = 485
        end
        inherited TileSetView: TTileMapView
          Width = 485
          Height = 267
        end
      end
    end
    object TilePage: TTntTabSheet
      Caption = 'Tile'
    end
  end
end
