unit me_tilesetpropsfrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, TntForms, TntComCtrls, ToolWin, VirtualTrees,
  PropEditor, TntStdCtrls, ExtCtrls, ActnList, TntActnList, ImgList, TilesEx,
  me_tilesfrm, me_project, PropUtils, PropContainer, BitmapEx, TntSysUtils,
  TntClasses, TntDialogs, NodeLst, MyClasses, TileMapView;

const
 WM_REFRESHTILESETPROPS =  WM_USER + 993;
 TAG_DEFTYP = -1;
 TAG_ARRLEN = -2;
 TAG_PRECIS = -3;
 TAG_STRLEN = -4;
 TAG_ENUMER = -5;
 TAG_INTMIN = -6;
 TAG_INTMAX = -7;
 TAG_NUMSYS = -8;
 TAG_TLCFMT = -9;

 TAG_SETNAME = 0;
 TAG_TILECNT = 1;
 TAG_ROWSIZE = 2;
 TAG_USE1CLR = 3;
 TAG_TLWIDTH = 4;
 TAG_THEIGHT = 5;
 TAG_TILEBPP = 6;
 TAG_TRCOLOR = 7;
 TAG_COMPCNT = 8;
 TAG_ATTRCNT = 9;
 TAG_FRMNEXT = 10;
 TAG_RELATIV = 11;
 TAG_FRDELAY = 12;

 TAG_COMPDEF = 13;
 TAG_ATTRDEF = 14;

 varTileIndex = $8000; 

type
  TTileSetsFrame = class(TFrame)
    TileSetPropLabel: TTntLabel;
    TileSetPropEditor: TPropertyEditor;
    TileSetsToolBar: TTntToolBar;
    TileSetNewToolBtn: TTntToolButton;
    sep10: TTntToolButton;
    TileSetUpToolBtn: TTntToolButton;
    TileSetDownToolBtn: TTntToolButton;
    TileSetDeleteToolBtn: TTntToolButton;
    AttrToolBar: TTntToolBar;
    PropAddToolBtn: TTntToolButton;
    sep14: TTntToolButton;
    PropUpToolBtn: TTntToolButton;
    PropDownToolBtn: TTntToolButton;
    PropDeleteToolBtn: TTntToolButton;
    TileSetsListBox: TListBox;
    TileSetLabel: TTntLabel;
    ActionList: TTntActionList;
    TileSetNewAction: TTntAction;
    TileSetUpAction: TTntAction;
    TileSetDownAction: TTntAction;
    TileSetDeleteAction: TTntAction;
    PropAddAction: TTntAction;
    PropUpAction: TTntAction;
    PropDownAction: TTntAction;
    PropertyDeleteAction: TTntAction;
    sep999: TTntToolButton;
    Splitter2: TSplitter;
    PageControl: TTntPageControl;
    PropertiesPage: TTntTabSheet;
    VisualPage: TTntTabSheet;
    TileSetsComboBox: TTntComboBox;
    TilesFrame: TTilesFrame;
    ImageList: TImageList;
    PropsPanel: TPanel;
    TileSetAddFromFilesAction: TTntAction;
    TilesImportAction: TTntAction;
    TilesExportAction: TTntAction;
    TileSetSaveToFileAction: TTntAction;
    OpenTilesDialog: TTntOpenDialog;
    SaveTilesDialog: TTntSaveDialog;
    procedure TileSetNewActionExecute(Sender: TObject);
    procedure TileSetNewActionUpdate(Sender: TObject);
    procedure TileSetDownActionExecute(Sender: TObject);
    procedure TileSetDownActionUpdate(Sender: TObject);
    procedure TileSetUpActionUpdate(Sender: TObject);
    procedure TileSetUpActionExecute(Sender: TObject);
    procedure TileSetDeleteActionExecute(Sender: TObject);
    procedure TileSetDeleteActionUpdate(Sender: TObject);
    procedure PropAddActionExecute(Sender: TObject);
    procedure PropMoveActionExecute(Sender: TObject);
    procedure PropertyDeleteActionUpdate(Sender: TObject);
    procedure PropDownActionUpdate(Sender: TObject);
    procedure PropUpActionUpdate(Sender: TObject);
    procedure PropAddActionUpdate(Sender: TObject);
    procedure TileSetsListBoxClick(Sender: TObject);
    function TileSetsListBoxDataFind(Control: TWinControl;
      FindString: String): Integer;
    procedure TileSetValueChange(Sender: TPropertyListItem);
    procedure TileSetImageFlagsChange(Sender: TPropertyListItem);
    procedure ScaleChange(Sender: TObject; var Value: Integer);
    procedure TileSetsComboBoxChange(Sender: TObject);
    procedure TileSetPropEditorPropertyRemove(
      Sender: TCustomPropertyEditor; Node: PVirtualNode; var Yes: Boolean);
    procedure TileSetPropEditorPropertyUp(Sender: TCustomPropertyEditor;
      Node: PVirtualNode; var Yes: Boolean);
    procedure TileSetPropEditorPropertyDown(Sender: TCustomPropertyEditor;
      Node: PVirtualNode; var Yes: Boolean);
    procedure TileSetsListBoxDblClick(Sender: TObject);
    procedure VisualPageShow(Sender: TObject);
    procedure TilesFrameTileSetViewSetPosition(Sender: TCustomTileMapView;
      var NewX, NewY: Integer);
    procedure TileSetsListBoxDrawItem(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    procedure PageControlChange(Sender: TObject);
    procedure TileSetPropEditorGetPropertyName(
      Sender: TCustomPropertyEditor; Node: PVirtualNode;
      var Text: WideString);
    procedure TileSetsListBoxKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure TileSetAddFromFilesActionExecute(Sender: TObject);
    procedure TilesImportActionExecute(Sender: TObject);
    procedure TilesExportActionExecute(Sender: TObject);
    procedure SaveTilesDialogTypeChange(Sender: TObject);
    procedure TilesImportExportActionUpdate(Sender: TObject);
    procedure TileSetSaveToFileActionExecute(Sender: TObject);
    procedure TileSetSaveToFileActionUpdate(Sender: TObject);
  private
    FProject: TMapEditorProject;
    FImportTileSet: TTileSet;
    FImportWidth: Integer;
    FOnTileSetModified: TNotifyEvent;
    FOnTileSetsModified: TNotifyEvent;
    FOnTileSetDeselect: TNotifyEvent;
    FOnTileSetNameModified: TNotifyEvent;
    FOnTileSetPropsModified: TNotifyEvent;
    FOnTileSetSelected: TNotifyEvent;
    FOnTileSetSelectionChanged: TNotifyEvent;
    FOnPageControlChange: TNotifyEvent;
    FOnOpen: TNotifyEvent;
    FOnTileSetComboSelected: TNotifyEvent;
    FOnStartDialog: TNotifyEvent;
    FOnEndDialog: TNotifyEvent;
    FOnTilesModified: TNotifyEvent;
    FOnBeforeUnhook: TNotifyEvent;
    FOnAfterUnhook: TNotifyEvent;
    procedure WMRefreshTileSetProps(var Message: TMessage); message WM_REFRESHTILESETPROPS;
    function GetTileSetIndex: Integer;
    procedure SetTileSetIndex(Value: Integer);
  protected
    procedure StartDialog;
    procedure EndDialog;
  public
    property TileSetIndex: Integer read GetTileSetIndex write SetTileSetIndex;

    procedure SetProject(Value: TMapEditorProject; ATag: Integer);

    function CanAct: Boolean;

    procedure RefreshTileSetProps(UserSelect: Boolean);
    procedure TileSetModified;
    procedure TileSetsModified(ProjectUpdate: Boolean);
    procedure TileSetDeselect;
    procedure FillComboBox;
    procedure TileSetNameModified;
    procedure TileSetPropsModified(All: Boolean = False);
    procedure TileSetSelectionChanged;
    procedure TileSetSelected;
    procedure TileSetComboSelected;
    procedure TileSetChanged;
    procedure RefreshLists;
    procedure CopyTileSets(Cut: Boolean);
    procedure PasteTileSets;
    procedure SelectTileSet(Index: Integer);
    procedure TilesModified;
    procedure BeforeUnhook;
    procedure AfterUnhook;

    function NewMoveTileSetsEvent(Shift: Integer): THistoryEvent;
    function AddListEvent(const Text: WideString): THistoryEvent;

    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;            
  published
    property OnOpen: TNotifyEvent read FOnOpen write FOnOpen;
    property OnTileSetModified: TNotifyEvent read FOnTileSetModified
                                            write FOnTileSetModified;
    property OnTileSetsModified: TNotifyEvent read FOnTileSetsModified
                                         write FOnTileSetsModified;
    property OnTileSetDeselect: TNotifyEvent read FOnTileSetDeselect
                                            write FOnTileSetDeselect;
    property OnTileSetNameModified: TNotifyEvent read FOnTileSetNameModified
                                                write FOnTileSetNameModified;
    property OnTileSetPropsModified: TNotifyEvent read FOnTileSetPropsModified
                                                write FOnTileSetPropsModified;
    property OnTileSetSelected: TNotifyEvent read FOnTileSetSelected
                                            write FOnTileSetSelected;
    property OnTileSetSelectionChanged: TNotifyEvent
                                        read FOnTileSetSelectionChanged
                                       write FOnTileSetSelectionChanged;
    property OnPageControlChange: TNotifyEvent
                                          read FOnPageControlChange
                                         write FOnPageControlChange;
    property OnTileSetComboSelected: TNotifyEvent
                                         read FOnTileSetComboSelected
                                        write FOnTileSetComboSelected;
    property OnStartDialog: TNotifyEvent read FOnStartDialog write FOnStartDialog;
    property OnEndDialog: TNotifyEvent read FOnEndDialog write FOnEndDialog;
    property OnTilesModified: TNotifyEvent read FOnTilesModified write FOnTilesModified;
    property OnBeforeUnhook: TNotifyEvent read FOnBeforeUnhook
                                         write FOnBeforeUnhook;
    property OnAfterUnhook: TNotifyEvent read FOnAfterUnhook
                                        write FOnAfterUnhook;
  end;

var
 VarTypesList: TTntStringList;
 CF_TILESETS: Word;

procedure FillImageFormatProperties(List: TPropertyList; ImageFlags: Word);
function CheckTileSetFlags(Sender: TPropertyListItem; TileSet: TTileSet): Boolean;
function FillTileSetFlags(Sender: TPropertyListItem;
          TileSet: TTileSet; Realloc: Boolean): Word;

implementation

uses HexUnit, me_importtilesdlg, MyUtils;

{$R *.dfm}


const
 OrderList: array[0..1] of WideString = ('Left to right', 'Right to left');

procedure FillImageFormatProperties(List: TPropertyList; ImageFlags: Word);
begin
 with List do
 begin
  AddDecimal('Bits per unit', (ImageFlags and 31) + 1, 1, 32).UserTag := TAG_TLCFMT;
  AddPickList('Pixel bits order', OrderList,
             Integer(ImageFlags and IMG_BITS_REVERSE <> 0)).UserTag := TAG_TLCFMT;
  AddBoolean('Swap pixel bytes', ImageFlags and IMG_PIXEL_BSWAP <> 0).UserTag := TAG_TLCFMT;
  AddBoolean('Vertical flip', ImageFlags and IMG_Y_FLIP <> 0).UserTag := TAG_TLCFMT;
  AddBoolean('Horizontal flip', ImageFlags and IMG_X_FLIP <> 0).UserTag := TAG_TLCFMT;
  AddBoolean('Horizontal bits flip', ImageFlags and IMG_X_FLIP4 = IMG_X_FLIP4).UserTag := TAG_TLCFMT;
  AddBoolean('Planar', ImageFlags and IMG_PLANAR <> 0).UserTag := TAG_TLCFMT;
  AddPickList('Plane bit capacity', ['1-bit', '2-bit', '4-bit', '8-bit'],
              ImageFlags shr PLANE_FORMAT_SHIFT).UserTag := TAG_TLCFMT;
  AddPickList('Plane order', OrderList,
              Integer(ImageFlags and IMG_PLANE_REVERSE <> 0)).UserTag := TAG_TLCFMT;
  AddPickList('Plane format', ['Matrix', 'Vector'],
             Integer(ImageFlags and IMG_PLANE_VECTOR <> 0)).UserTag := TAG_TLCFMT;
 end;
end;

function CheckTileSetFlags(Sender: TPropertyListItem; TileSet: TTileSet): Boolean;
var
 I, BCnt, Idx: Integer;
 Value: Word;
 Flags: TCompositeFlags;
begin
 with TileSet.TileBitmap do
 begin
  Idx := Sender.ItemOwner.Index;
  if Idx = 0 then
   Value := ImageFlags else
   Value := CompositeFlags[Idx - 1];

  Flags := FlagsList;
  case Sender.Index of
   0:
   begin
    Flags[Idx] := Sender.Value - 1;
    
    BCnt := Length(Flags);
    for I := 0 to Length(Flags) - 1 do
     Inc(BCnt, Flags[I] and 31);

    if BCnt > 8 then
    begin
     Result := False;
     Exit;
    end;
   end;
   7:
   begin
    BCnt := 1 shl Sender.PickListIndex;
    if BCnt > Value and 31 + 1 then
    begin
     Result := False;
     Exit;
    end;
   end;
  end;
 end;
 Result := True;
end;

function FillTileSetFlags(Sender: TPropertyListItem;
                           TileSet: TTileSet; Realloc: Boolean): Word;
var
 BCnt, Idx: Integer;
 Flags: TCompositeFlags;
begin
 with TileSet.TileBitmap do
 begin
  Idx := Sender.ItemOwner.Index;
  if Idx = 0 then
   Result := ImageFlags else
   Result := CompositeFlags[Idx - 1];

  Flags := FlagsList;
  case Sender.Index of
   0:
   begin
    Flags[Idx] := Sender.Value - 1;

    Result := (Result and not 31) or ((Sender.Value - 1) and 31);
    BCnt := Result shr PLANE_FORMAT_SHIFT;
    while 1 shl BCnt > Sender.Value do
     Dec(BCnt);
    Result := (Result and IMG_FORMAT_MASK) or
             (BCnt shl PLANE_FORMAT_SHIFT);
   end;
   1: if Sender.PickListIndex = 0 then
       Result := Result and not IMG_BITS_REVERSE else
       Result := Result or IMG_BITS_REVERSE;
   2: if Sender.PickListIndex = 0 then
       Result := Result and not IMG_PIXEL_BSWAP else
       Result := Result or IMG_PIXEL_BSWAP;
   3: if Sender.PickListIndex = 0 then
       Result := Result and not IMG_Y_FLIP else
       Result := Result or IMG_Y_FLIP;
   4: if Sender.PickListIndex = 0 then
       Result := Result and not IMG_X_FLIP else
       Result := Result or IMG_X_FLIP;
   5: if Sender.PickListIndex = 0 then
       Result := Result and not IMG_X_FLIP4 else
       Result := Result or IMG_X_FLIP4;
   6: if Sender.PickListIndex = 0 then
       Result := Result and not IMG_PLANAR else
       Result := Result or IMG_PLANAR;
   7:
   begin
    Result := (Result and IMG_FORMAT_MASK) or
             (Sender.PickListIndex shl PLANE_FORMAT_SHIFT);
   end;
   8: if Sender.PickListIndex = 0 then
       Result := Result and not IMG_PLANE_REVERSE else
       Result := Result or IMG_PLANE_REVERSE;
   9: if Sender.PickListIndex = 0 then
       Result := Result and not IMG_PLANE_VECTOR else
       Result := Result or IMG_PLANE_VECTOR;
  end;
  Flags[Idx] := Result;
  if Realloc then
   TileSet.Reallocate(Flags) else
   TileSet.SwitchFormat(Flags);
 end;
end;

procedure TTileSetsFrame.TileSetNewActionExecute(Sender: TObject);
var
 Last, ts: TTileSet;
 Event: THistoryEvent;
 List: TTileSetList;
begin
 if CanAct then
 begin
  List := FProject.FMaps.TileSetList;
  Event := AddListEvent('');
  with List do
  begin
   Last := LastNode as TTileSet;

   ts := AddChild('Unnamed 1') as TTileSet;
   with ts do
    if Last <> nil then
     AssignTileFormat(Last) else
     SwitchFormat([8 - 1], 16, 16);

   Event.Text := WideFormat('New tile set ''%s''', [ts.Name]);

   TileSetIndex := Count - 1;

   FProject.Update;
   with FProject.FTileSetInfo[LastNode.Index] do
   begin
    if Last <> nil then
     Width := FProject.FTileSetInfo[Last.Index].Width;

    TTileSet(LastNode).TilesCount := Width * 4;
   end;

   TileSetsModified(False);
  end;
  Event.RedoData := List;
 end;
end;

procedure TTileSetsFrame.TileSetNewActionUpdate(Sender: TObject);
begin
 (Sender as TAction).Enabled := CanAct;
end;

procedure TTileSetsFrame.TileSetUpActionUpdate(Sender: TObject);
var
 Enable: Boolean;
begin
 Enable := False;
 if CanAct and (FProject.FMaps.TileSetList.Count > 1) then
 begin
  if TileSetsListBox.SelCount > 0 then
   Enable := not TileSetsListBox.Selected[0] else
   Enable := TileSetIndex > 0;
 end;

 TileSetUpAction.Enabled := Enable;
end;

procedure TTileSetsFrame.TileSetUpActionExecute(Sender: TObject);
var
 I, NewII, J, L: Integer;
 Detached: array of TNode;
 Node, Prv: TNode;
 Src, Dst: ^TTileSetInfo;
 Temp: TTileSetInfo;
 List: TTileSetList;
 Event: THistoryEvent;
begin
 if CanAct and (TileSetsListBox.SelCount > 0) then
 begin
  List := FProject.FMaps.TileSetList;
  with List do
   if Count > 1 then
   begin
    if TileSetsListBox.SelCount = 0 then
     TileSetsListBox.Selected[TileSetIndex] := True;

    if TileSetsListBox.Selected[0] then Exit;

    TileSetDeselect;

    Event := NewMoveTileSetsEvent(-1);

    NewII := -1;
    Node := LastNode;
    for I := Count - 1 downto 0 do
    begin
     Prv := Node.Prev;
     if TileSetsListBox.Selected[I] then
     begin
      NewII := I - 1;
      L := Length(Detached);
      SetLength(Detached, L + 1);
      Detached[L] := Node;
      DetachNode(Node);
      Node.Index := NewII;

      Src := Addr(FProject.FTileSetInfo[I]);
      Dst := Addr(FProject.FTileSetInfo[NewII]);
      Temp := Dst^;
      Dst^ := Src^;
      Src^ := Temp;
     end;
     Node := Prv;
    end;

    TileSetsListBox.ClearSelection;

    for I := Length(Detached) - 1 downto 0  do
    begin
     Node := Detached[I];
     J := Node.Index;
     AttachNode(Node, J);
     TileSetsListBox.Selected[J] := True;
    end;

    United := True;

    TileSetsListBox.Invalidate;

    TileSetIndex := NewII;

    TileSetsModified(True);

    Event.RedoData := List;
   end;
 end;
end;

procedure TTileSetsFrame.TileSetDownActionExecute(Sender: TObject);
var
 I, NewII, J, L: Integer;
 Detached: array of TNode;
 Node, Prv: TNode;
 Src, Dst: ^TTileSetInfo;
 Temp: TTileSetInfo;
 List: TTileSetList;
 Event: THistoryEvent;
begin
 if CanAct and (TileSetsListBox.SelCount > 0) then
 begin
  List := FProject.FMaps.TileSetList;
  with List do
   if Count > 1 then
   begin
    if TileSetsListBox.SelCount = 0 then
     TileSetsListBox.Selected[TileSetIndex] := True;

    if TileSetsListBox.Selected[Count - 1] then Exit;

    TileSetDeselect;

    Event := NewMoveTileSetsEvent(+1);

    NewII := -1;
    Node := LastNode;
    for I := Count - 1 downto 0 do
    begin
     Prv := Node.Prev;
     if TileSetsListBox.Selected[I] then
     begin
      if NewII < 0 then NewII := I + 1;
      L := Length(Detached);
      SetLength(Detached, L + 1);
      Detached[L] := Node;
      DetachNode(Node);
      Node.Index := I + 1;

      Src := Addr(FProject.FTileSetInfo[I]);
      Dst := Addr(FProject.FTileSetInfo[I + 1]);
      Temp := Dst^;
      Dst^ := Src^;
      Src^ := Temp;
     end;
     Node := Prv;
    end;

    TileSetsListBox.ClearSelection;

    for I := Length(Detached) - 1 downto 0  do
    begin
     Node := Detached[I];
     J := Node.Index;
     AttachNode(Node, J);
     TileSetsListBox.Selected[J] := True;
    end;

    United := True;

    TileSetsListBox.Invalidate;

    TileSetIndex := NewII;

    TileSetsModified(True);

    Event.RedoData := List;
   end;
 end;
end;

procedure TTileSetsFrame.TileSetDownActionUpdate(Sender: TObject);
var
 Enable: Boolean;
begin
 Enable := False;
 if CanAct then
  with FProject.FMaps.TileSetList do
   if Count > 1 then
   begin
    if TileSetsListBox.SelCount > 0 then
     Enable := not TileSetsListBox.Selected[Count - 1] else
     Enable := TileSetsListBox.ItemIndex < Count - 1;
   end;

 TileSetDownAction.Enabled := Enable;
end;

procedure TTileSetsFrame.TileSetDeleteActionExecute(Sender: TObject);
var
 I, Top: Integer;
 Node, Prv: TNode;
 Used: Boolean;
 Msg: WideString;
 List: TTileSetList;
 Event: THistoryEvent;
begin
 if CanAct and (TileSetsListBox.SelCount > 0) then
 begin
  List := FProject.FMaps.TileSetList;
  with List do
   if Count > 0 then
   begin
    if TileSetsListBox.SelCount = 0 then
     TileSetsListBox.Selected[TileSetIndex] := True;

    Used := False;
    for I := 0 to Count - 1 do
     if TileSetsListBox.Selected[I] then
     begin
      Used := FProject.IsTileSetUsed(I);
      if Used then Break;
     end;

    if Used then
    begin
     if TileSetsListBox.SelCount = 1 then
      Msg := 'This tile set is in use. Delete it anyway?' else
      Msg := 'One or more of the selected tile sets are in use.'#13#10 +
                     'Delete them anyway?';
     if WideMessageDlg(Msg, mtConfirmation, [mbYes, mbNo], 0) <> mrYes then
      Exit;
    end;

    TileSetDeselect;

    if TileSetsListBox.SelCount = 1 then
     Msg := WideFormat('Delete tile set ''%s''', [Sets[TileSetIndex].Name]) else
     Msg := 'Delete tile sets';

    if Used then
     BeforeUnhook;
    Event := AddListEvent(Msg);
    try
     Top := -1;
     Node := LastNode;
     for I := Count - 1 downto 0 do
     begin
      Prv := Node.Prev;
      if TileSetsListBox.Selected[I] then
      begin
       FProject.UnhookTileSet(Node as TTileSet);

       if Top < 0 then Top := I;

       if I < Count - 1 then
       begin
        Move(FProject.FTileSetInfo[I],
             FProject.FTileSetInfo[I + 1],
             (Length(FProject.FTileSetInfo) - (I + 1)) * SizeOf(TTileSetInfo));
       end;

       if TilesFrame.TileSet = Node then
        TilesFrame.TileSet := nil;
       Remove(Node);
      end;
      Node := Prv;
     end;
     if Top >= Count then
      Dec(Top);

     if (Top < 0) and (Count > 0) then
      Top := 0;

     TileSetIndex := Top;

     TileSetsModified(True);
    finally
     Event.RedoData := List;
     if Used then
      AfterUnhook;
    end;
   end;
 end;
end;

procedure TTileSetsFrame.TileSetDeleteActionUpdate(Sender: TObject);
begin
 TileSetDeleteAction.Enabled := CanAct and
                             (FProject.FMaps.TileSetList.Count > 0);
end;

procedure TTileSetsFrame.SetProject(Value: TMapEditorProject; ATag: Integer);
begin
 Tag := ATag;
 FProject := Value;
 TileSetsListBox.Count := 0;
 TilesFrame.TileSet := nil;
 if FProject <> nil then
  TilesFrame.Scale := FProject.FInfo.Scales.scTilesFrame[ATag];                   
 RefreshLists;
end;

procedure TTileSetsFrame.PropAddActionUpdate(Sender: TObject);
var
 Data: PPropertyData;
 Enable: Boolean;
begin
 Enable := CanAct and (TilesFrame.TileSet <> nil) and
           (TileSetPropEditor.FocusedNode <> nil);
 if Enable then
 begin
  Data := TileSetPropEditor.GetNodeData(TileSetPropEditor.FocusedNode);
  Enable := (Data.Owner <> nil) and
           ((Data.Item.UserTag in [TAG_COMPCNT, TAG_COMPDEF,
                                  TAG_ATTRCNT, TAG_ATTRDEF]) or
            (Data.Item.UserTag < 0));
 end;
 PropAddAction.Enabled := Enable;
 if Enable then
 begin
  case Data.Item.UserTag of
   TAG_COMPCNT,
   TAG_COMPDEF,
   TAG_TLCFMT:
   begin
    PropAddAction.Caption := '&New Composite';
    PropAddAction.Hint := 'New Composite|Adds a new graphic composite';
   end;
   else
   begin
    PropAddAction.Caption := '&New Attribute';
    PropAddAction.Hint := 'New Attribute|Adds a new tile attribute definition';
   end;
  end; 
 end else
 begin
  PropAddAction.Caption := '&New Property';
  PropAddAction.Hint := 'New Property|Adds a new graphic composite or tile attribute definition';
 end;
end;

procedure TTileSetsFrame.PropAddActionExecute(Sender: TObject);
var
 Data: PPropertyData;
 L: Integer;
 Flg: TCompositeFlags;
 Event: THistoryEvent;
begin
 if TilesFrame.TileSet <> nil then
  with TileSetPropEditor do
  begin
   Data := GetNodeData(FocusedNode);
   if (Data <> nil) and (Data.Owner <> nil) then
   with TilesFrame.TileSet do
   case Data.Item.UserTag of
    TAG_TLCFMT, TAG_COMPCNT, TAG_COMPDEF:
    begin
     if TileBitmap.BitsCount < 8 then
     begin
      Flg := TileBitmap.FlagsList;
      SetLength(Flg, Length(Flg) + 1);
      GetRoute(FocusedNode, LastRoute);

      L := Length(LastRoute);
      case Data.Item.UserTag of
       TAG_COMPCNT:
        SetLength(LastRoute, L + 1);
       TAG_COMPDEF:
        Dec(L);
       else
       begin
        Dec(L);
        SetLength(LastRoute, L);
        Dec(L);
       end;
      end;

      LastRoute[L] := Length(Flg) - 1;
      with TilesFrame do
      begin
       Event := FProject.History.AddEvent(TileSet,
        WideFormat('Tile set ''%s'': Composite count = %d',
         [TileSet.Name, Length(Flg)]));

       Reallocate(Flg);
       Event.RedoData := TileSet;
      end;
      RefreshTileSetProps(True);
      TileSetModified;
      TileSetPropsModified;
     end else
      WideMessageDlg('Cannot add more composites because direct color is unsupported.', mtError, [mbOk], 0);
    end;
    TAG_ATTRCNT, TAG_ATTRDEF, TAG_NUMSYS..TAG_DEFTYP:
    if AttrCount < 64 then
    begin
     L := AttrCount + 1;
     with TilesFrame do
     begin
      Event := FProject.History.AddEvent(TileSet,
       WideFormat('Tile set ''%s'': Tile attributes = %d',
        [TileSet.Name, L]));
      AttrCount := L;
      Event.RedoData := TileSet;
     end;
     GetRoute(FocusedNode, LastRoute);

     L := Length(LastRoute);
     case Data.Item.UserTag of
      TAG_ATTRCNT:
       SetLength(LastRoute, L + 1);
      TAG_ATTRDEF:
       Dec(L);
      else
      begin
       Dec(L);
       SetLength(LastRoute, L);
       Dec(L);
      end;
     end;

     LastRoute[L] := AttrCount - 1;

     RefreshTileSetProps(True);
     TileSetSelectionChanged;
     TileSetPropsModified;
    end else
     WideMessageDlg('Reached the limit of 64 tile attributes per tile set.',
                    mtError, [mbOk], 0);
   end;
  end;
end;

procedure TTileSetsFrame.PropMoveActionExecute(Sender: TObject);
var
 Data: PPropertyData;
 Node: PVirtualNode;
begin
 Node := TileSetPropEditor.FocusedNode;
 if CanAct and (Node <> nil) then
 begin
  Data := TileSetPropEditor.GetNodeData(Node);
  if Data.Owner <> nil then
  case Data.Item.UserTag of
   TAG_COMPDEF,
   TAG_ATTRDEF: ;
   else
   if Data.Item.UserTag < 0 then
    Node := Node.Parent else
    Exit;
  end;
  case (Sender as TAction).Tag of
   0: TileSetPropEditor.PropertyUp(Node);
   1: TileSetPropEditor.PropertyDown(Node);
   2: TileSetPropEditor.PropertyRemove(Node);
  end;
 end;
end;

procedure TTileSetsFrame.PropertyDeleteActionUpdate(Sender: TObject);
var
 Data: PPropertyData;
 Enable: Boolean;
begin
 Enable := CanAct and (TileSetPropEditor.FocusedNode <> nil);
 if Enable then
 begin
  Data := TileSetPropEditor.GetNodeData(TileSetPropEditor.FocusedNode);
  if Data.Owner <> nil then
  case Data.Item.UserTag of
   TAG_COMPDEF: Enable := Data.Owner.Count > 1;
   TAG_ATTRDEF: ;
   TAG_TLCFMT:
   with Data.Item.ItemOwner do
    Enable := (Owner as TPropertyList).Count > 1;
   else
    Enable := Data.Item.UserTag < 0;
  end;
 end;
 PropertyDeleteAction.Enabled := Enable;
 if Enable then
 begin
  case Data.Item.UserTag of
   TAG_COMPCNT,
   TAG_COMPDEF,
   TAG_TLCFMT:
    PropertyDeleteAction.Hint := 'Delete|Deletes a selected graphic composite';
   else
    PropertyDeleteAction.Hint := 'Delete|Deletes a selected tile attribute definition';
  end; 
 end else
  PropertyDeleteAction.Hint := 'Delete|Deletes a selected property';
end;

procedure TTileSetsFrame.PropDownActionUpdate(Sender: TObject);
var
 Data: PPropertyData;
 Enable: Boolean;
begin
 Enable := CanAct and (TileSetPropEditor.FocusedNode <> nil);
 if Enable then
 begin
  Data := TileSetPropEditor.GetNodeData(TileSetPropEditor.FocusedNode);
  if Data.Owner <> nil then
  case Data.Item.UserTag of
   TAG_COMPDEF,
   TAG_ATTRDEF: Enable := Data.Item.Index < Data.Owner.Count - 1;
   else
   with Data.Item do
    Enable := (UserTag < 0) and
    (ItemOwner.Index < (ItemOwner.Owner as TPropertyList).Count - 1);
  end;
 end;
 PropDownAction.Enabled := Enable;
 if Enable then
 begin
  case Data.Item.UserTag of
   TAG_COMPCNT,
   TAG_COMPDEF,
   TAG_TLCFMT:
    PropDownAction.Hint := 'Move Down|Moves a selected graphic composite down';
   else
    PropDownAction.Hint := 'Move Down|Moves a selected tile attribute definition down';
  end; 
 end else
  PropDownAction.Hint := 'Move Down|Moves a selected property down';
end;

procedure TTileSetsFrame.PropUpActionUpdate(Sender: TObject);
var
 Data: PPropertyData;
 Enable: Boolean;
begin
 Enable := CanAct and (TileSetPropEditor.FocusedNode <> nil);
 if Enable then
 begin
  Data := TileSetPropEditor.GetNodeData(TileSetPropEditor.FocusedNode);
  if Data.Owner <> nil then
  case Data.Item.UserTag of
   TAG_COMPDEF,
   TAG_ATTRDEF: Enable := Data.Item.Index > 0;
   else
    Enable := (Data.Item.UserTag < 0) and (Data.Item.ItemOwner.Index > 0);
  end;
 end;
 PropUpAction.Enabled := Enable;
 if Enable then
 begin
  case Data.Item.UserTag of
   TAG_COMPCNT,
   TAG_COMPDEF,
   TAG_TLCFMT:
    PropUpAction.Hint := 'Move Up|Moves a selected graphic composite up';
   else
    PropUpAction.Hint := 'Move Up|Moves a selected tile attribute definition up';
  end; 
 end else
  PropUpAction.Hint := 'Move Up|Moves a selected property up';
end;

procedure TTileSetsFrame.TileSetsListBoxClick(Sender: TObject);
var
 I, Idx: Integer;
begin
 with TileSetsListBox do
 begin
  Idx := ItemIndex;
  if (Count > 0) and not Selected[Idx] then
   for I := 0 to Count - 1 do
    if Selected[I] then
    begin
     Idx := I;
     ItemIndex := I;     
     Break;;
    end;
 end;
 TileSetsComboBox.ItemIndex := Idx;
 TileSetIndex := Idx;
 TileSetSelected;
end;

function TTileSetsFrame.TileSetsListBoxDataFind(Control: TWinControl;
  FindString: String): Integer;
begin
 if FProject <> nil then
  Result := FProject.FMaps.TileSetList.FindIndexByFirstLetters(FindString) else
  Result := -1;
end;

constructor TTileSetsFrame.Create(AOwner: TComponent);
begin
 inherited;
 FImportTileSet := TTileSet.Create;
 FImportTileSet.SwitchFormat([7], 16, 16);
 FImportWidth := 18;
 FImportTileSet.TilesCount := 18 * 4;
 PropsPanel.DoubleBuffered := True;
 AttrToolBar.DoubleBuffered := True;
 TileSetsToolBar.DoubleBuffered := True;
 PageControl.DoubleBuffered := True;
 TileSetsListBox.DoubleBuffered := True;
 TilesFrame.OnScaleChange := ScaleChange;
end;

procedure TTileSetsFrame.TileSetImageFlagsChange(Sender: TPropertyListItem);
var
 TileSet: TTileSet;
 I: Integer;
 Event: THistoryEvent;
begin
 if CanAct then
 begin
  with FProject.FMaps.TileSetList do
  begin
   for I := 0 to Count - 1 do
    if TileSetsListBox.Selected[I] then
    begin
     TileSet := Nodes[I] as TTileSet;
     if not CheckTileSetFlags(Sender, TileSet) then
     begin
      case Sender.Index of
       0: WideMessageDlg('Direct color is unsupported.', mtError, [mbOk], 0);
       7: WideMessageDlg('Cannot be higher than bits per unit.', mtError, [mbOk], 0);
      end;
      RefreshTileSetProps(False);
      Exit;
     end;
    end;

   for I := 0 to Count - 1 do
    if TileSetsListBox.Selected[I] then
    begin
     TileSet := Nodes[I] as TTileSet;
     Event := FProject.History.AddEvent(TileSet, '');
     Event.Text := WideFormat('Tile set ''%s'': Composite #%d = 0x%4.4x',
                 [TileSet.Name, Sender.ItemOwner.Index + 1,
                 FillTileSetFlags(Sender, TileSet, True)]);
     Event.RedoData := TileSet;
    end;
  end;
  TileSetModified;
  TileSetPropsModified(True);
 end;
end;

procedure TTileSetsFrame.TileSetModified;
begin
 TileSetChanged;
 if Assigned(FOnTileSetModified) then
  FOnTileSetModified(Self);
end;

procedure TTileSetsFrame.ScaleChange(Sender: TObject; var Value: Integer);
begin
 if FProject <> nil then
  FProject.FInfo.Scales.scTilesFrame[Tag] := Value;
end;

procedure TTileSetsFrame.TileSetsComboBoxChange(Sender: TObject);
var
 I: Integer;
begin
 I := TileSetsComboBox.ItemIndex;
 TileSetIndex := I;
 TileSetsListBox.ClearSelection;
 TileSetsListBox.ItemIndex := I;
 TileSetsListBox.Selected[I] := True;
 TileSetSelected;
 TileSetComboSelected;
end;

procedure TTileSetsFrame.TileSetsModified(ProjectUpdate: Boolean);
begin
 if ProjectUpdate then
  FProject.Update;

 if Assigned(FOnTileSetsModified) then
  FOnTileSetsModified(Self);
    
 RefreshLists;
end;

procedure TTileSetsFrame.TileSetPropEditorPropertyRemove(
  Sender: TCustomPropertyEditor; Node: PVirtualNode; var Yes: Boolean);
var
 Data: PPropertyData;
 Idx, I, L: Integer;
 Flg: TCompositeFlags;
 ts: TTileSet;
 Event: THistoryEvent;
begin
 Data := Sender.GetNodeData(Node);
 if CanAct and (Data <> nil) and (Data.Owner <> nil) then
  with FProject.FMaps.TileSetList do
   case Data.Item.UserTag of
    TAG_ATTRDEF:
    begin
     for I := 0 to Count - 1 do
      if TileSetsListBox.Selected[I] then
      begin
       ts := Nodes[I] as TTileSet;
       with Data.Item do
       begin
        Event := FProject.History.AddEvent(ts,
         WideFormat('Tile set ''%s'': Delete attribute #%d ''%s''',
                          [ts.Name, Index + 1,
                          ts.AttrDefs[Index].adName]));
        ts.DeleteAttr(Index);
       end;
       Event.RedoData := ts;
      end;

     Data := Sender.GetNodeData(Node.Parent);
     if (Data <> nil) and (Data.Owner <> nil) then
     with Data.Item do
     begin
      Changing := True;
      Value := Value - 1;
      Changing := False;
     end;
     Sender.InvalidateNode(Node.Parent);

     TileSetSelectionChanged;
     TileSetPropsModified(True);
     Exit;     
    end;
    TAG_COMPDEF:
    begin
     Idx := Data.Item.Index;
     for I := 0 to Count - 1 do
      if TileSetsListBox.Selected[I] then
      begin
       ts := Nodes[I] as TTileSet;
       Flg := ts.TileBitmap.FlagsList;
       L := Length(Flg) - 1;
       if Idx < L then
        Move(Flg[Idx + 1], Flg[Idx], (L - Idx) shl 1);
       SetLength(Flg, L);

       Event := FProject.History.AddEvent(ts,
        WideFormat('Tile set ''%s'': Delete composite #%d',
                                    [ts.Name, Idx + 1]));
       ts.Reallocate(Flg);
       Event.RedoData := ts;
      end;
     Sender.InvalidateNode(Node.Parent);

     TileSetModified;
     TileSetPropsModified(True);
     Exit;
    end;
   end;
 Yes := False;
end;

procedure TTileSetsFrame.TileSetPropEditorPropertyUp(
  Sender: TCustomPropertyEditor; Node: PVirtualNode; var Yes: Boolean);
var
 Data: PPropertyData;
 Idx, I: Integer;
 Value: Word;
 Flg: TCompositeFlags;
 ts: TTileSet;
 Event: THistoryEvent;
begin
 Data := Sender.GetNodeData(Node);
 if CanAct and (Data <> nil) and (Data.Owner <> nil) then
  with FProject.FMaps.TileSetList do
   case Data.Item.UserTag of
    TAG_ATTRDEF:
    begin
     for I := 0 to Count - 1 do
      if TileSetsListBox.Selected[I] then
      begin
       ts := Nodes[I] as TTileSet;
       with Data.Item do
       begin
        Event := FProject.History.AddEvent(ts,
         WideFormat('Tile set ''%s'': Exchange attribute #%d ''%s'' with #%d ''%s''',
                          [ts.Name, Index + 1,
                          ts.AttrDefs[Index].adName, Index,
                          ts.AttrDefs[Index - 1].adName]));
        ts.MoveAttr(Index, Index - 1);
       end;
       Event.RedoData := ts;
      end;

     TileSetSelectionChanged;
     TileSetPropsModified(True);
     Exit;
    end;
    TAG_COMPDEF:
    begin
     Idx := Data.Item.Index;
     if Idx > 0 then
     begin
      for I := 0 to Count - 1 do
       if TileSetsListBox.Selected[I] then
       begin
        ts := Nodes[I] as TTileSet;

        Flg := ts.TileBitmap.FlagsList;
        Value := Flg[Idx];
        Flg[Idx] := Flg[Idx - 1];
        Flg[Idx - 1] := Value;

        Event := FProject.History.AddEvent(ts,
         WideFormat('Tile set ''%s'': Exchange composite #%d with #%d',
                                     [ts.Name, Idx + 1, Idx]));
        ts.Reallocate(Flg);
        Event.RedoData := ts;
       end;
      Sender.InvalidateNode(Node.Parent);

      TileSetModified;
      TileSetPropsModified;
      Exit;
     end;
    end;
   end;
 Yes := False;
end;


procedure TTileSetsFrame.TileSetPropEditorPropertyDown(
  Sender: TCustomPropertyEditor; Node: PVirtualNode; var Yes: Boolean);
var
 Data: PPropertyData;
 Idx, I: Integer;
 Value: Word;
 Flg: TCompositeFlags;
 Event: THistoryEvent;
 ts: TTileSet;
begin
 Data := Sender.GetNodeData(Node);
 if CanAct and (Data <> nil) and (Data.Owner <> nil) then
  with FProject.FMaps.TileSetList do
   case Data.Item.UserTag of
    TAG_ATTRDEF:
    begin
     for I := 0 to Count - 1 do
      if TileSetsListBox.Selected[I] then
      begin
       ts := Nodes[I] as TTileSet;
       with Data.Item do
       begin
        Event := FProject.History.AddEvent(ts,
         WideFormat('Tile set ''%s'': Exchange attribute #%d ''%s'' with #%d ''%s''',
                          [ts.Name, Index + 1,
                          ts.AttrDefs[Index].adName, Index + 2,
                          ts.AttrDefs[Index + 1].adName]));
        ts.MoveAttr(Index, Index + 1);
       end;
       Event.RedoData := ts;
      end;

     TileSetSelectionChanged;
     TileSetPropsModified(True);
     Exit;
    end;
    TAG_COMPDEF:
    begin
     Idx := Data.Item.Index;
     if Idx < Data.Owner.Count - 1 then
     begin
      for I := 0 to Count - 1 do
       if TileSetsListBox.Selected[I] then
       begin
        ts := Nodes[I] as TTileSet;
        Flg := ts.TileBitmap.FlagsList;
        Value := Flg[Idx];
        Flg[Idx] := Flg[Idx + 1];
        Flg[Idx + 1] := Value;

        Event := FProject.History.AddEvent(ts,
         WideFormat('Tile set ''%s'': Exchange composite #%d with #%d',
                                     [ts.Name, Idx + 1, Idx + 2]));
        ts.Reallocate(Flg);
        Event.RedoData := ts;
       end;
      Sender.InvalidateNode(Node.Parent);

      TileSetModified;
      TileSetPropsModified;
      Exit;
     end;
    end;
   end;
 Yes := False;
end;

function TTileSetsFrame.CanAct: Boolean;
begin
 Result := (FProject <> nil) and not (secLoading in FProject.FInternalFlags)
                             and not (secSaving in FProject.FInternalFlags);
end;

procedure TTileSetsFrame.TileSetDeselect;
var
 I: Integer;
begin
 if Assigned(FOnTileSetDeselect) then
  FOnTileSetDeselect(Self);

 TileSetPropEditor.AcceptEdit;
 with TilesFrame do
 begin
  for I := 0 to TileSetsListBox.Count - 1 do
   if (TileSetsListBox.Selected[I] and
      (TileSet.Index = I)) then
   begin
    TileSetView.Deselect;
    Exit;
   end;

  if TileSet <> nil then
   TileSetView.Deselect; 
 end;
end;

procedure TTileSetsFrame.FillComboBox;
var
 I: Integer;
begin
 TileSetsComboBox.Clear;
 if FProject <> nil then
  with FProject.FMaps.TileSetList do
  begin
   for I := 0 to Count - 1 do
    with Nodes[I] as TTileSet do
     TileSetsComboBox.Items.Add(WideFormat('%d. %s', [I + 1, Name]));

   TileSetsComboBox.ItemIndex := TileSetIndex;
  end;
end;

procedure TTileSetsFrame.TileSetValueChange(Sender: TPropertyListItem);
const
 SNothing: WideString = 'Nothing'; 
var
 Idx, L, J, I, BCnt: Integer;
 Sz: Int64;
 Diff: Integer;
 Flg: TCompositeFlags;
 TempEnum: TWideStringArray;
 RefreshAll, NoAdd: Boolean;
 Event: THistoryEvent;
 ts: TTileSet;
 Str: WideString;
 Def: PAttrDef;
begin
 if CanAct then
  with FProject.FMaps.TileSetList do
  begin
   case Sender.UserTag of
    TAG_SETNAME:
    begin
     for I := 0 to Count - 1 do
      if TileSetsListBox.Selected[I] then
      begin
       ts := Nodes[I] as TTileSet;
       Event := FProject.History.AddEvent(ts, '');
       Str := ts.Name;
       ts.Name := Sender.ValueStr;
       Event.Text := WideFormat('Tile set ''%s'': Name = ''%s''',
                                [Str, ts.Name]);
       Event.RedoData := ts;
      end;

     TileSetNameModified;
    end;
    TAG_FRMNEXT:
    begin
     for I := 0 to Count - 1 do
      if TileSetsListBox.Selected[I] then
      begin
       ts := Nodes[I] as TTileSet;
       Event := FProject.History.AddEvent(ts, '');
       if TileSetsListBox.SelCount > 1 then
        ts.NextFrameAttr := Sender.Value else // ts.FindAttrByName(Sender.ValueStr) else
       if Sender.ValueStr <> '' then
        ts.NextFrameAttr := Sender.PickListIndex else
        ts.NextFrameAttr := -1;

       if ts.NextFrameAttr >= 0 then
        Str := ts.AttrDefs[ts.NextFrameAttr].adName else
        Str := SNothing;

       Event.Text := WideFormat('Tile set ''%s'': Next frame attribute = %d (''%s'')',
                                [ts.Name, ts.NextFrameAttr, Str]);
       Event.RedoData := ts;
      end;

     TileSetPropsModified;
    end;
    TAG_FRDELAY:
    begin
     for I := 0 to Count - 1 do
      if TileSetsListBox.Selected[I] then
      begin
       ts := Nodes[I] as TTileSet;
       Event := FProject.History.AddEvent(ts, '');
       if TileSetsListBox.SelCount > 1 then
        ts.FrameDelayAttr := Sender.Value else // ts.FindAttrByName(Sender.ValueStr) else
       if Sender.ValueStr <> '' then
        ts.FrameDelayAttr := Sender.PickListIndex else
        ts.FrameDelayAttr := -1;

       if ts.FrameDelayAttr >= 0 then
        Str := ts.AttrDefs[ts.FrameDelayAttr].adName else
        Str := SNothing;

       Event.Text := WideFormat('Tile set ''%s'': Frame delay attribute = %d (''%s'')',
                                [ts.Name, ts.FrameDelayAttr, Str]);
       Event.RedoData := ts;
      end;

     TileSetPropsModified;
    end;
    TAG_RELATIV:
    begin
     for I := 0 to Count - 1 do
      if TileSetsListBox.Selected[I] then
      begin
       ts := Nodes[I] as TTileSet;
       Event := FProject.History.AddEvent(ts, '');
       ts.FrameRelative := Sender.PickListIndex <> 0;
       Event.Text := WideFormat('Tile set ''%s'': Relative frame index = %s', [ts.Name,
         BoolToStr(ts.FrameRelative, True)]);

       Event.RedoData := ts;
      end;
    end;
    TAG_TILECNT:
    begin
     Sz := 0;
     for I := 0 to Count - 1 do
      if TileSetsListBox.Selected[I] then
       with Nodes[I] as TTileSet do
        Inc(Sz, Sender.Value * TileSize);

     if (Sz < 16 * 1024 * 1024) or
      (WideMessageDlg(
       WideFormat('It can take up to %g MB of memory. Proceed?',
                  [(Sz + Sender.Value * 64) / (1024 * 1024)]),
                  mtConfirmation, [mbYes, mbNo], 0) = mrYes) then
     begin
      for I := 0 to Count - 1 do
       if TileSetsListBox.Selected[I] then
       begin
        ts := Nodes[I] as TTileSet;
        Event := FProject.History.AddEvent(ts, '');
        ts.TilesCount := Sender.Value;
        Event.Text := WideFormat('Tile set ''%s'': Tiles count = %d',
                                [ts.Name, ts.TilesCount]);
        with FProject.FTileSetInfo[ts.Index] do
        begin
         SelectedTile := ts.LastIndex;
         OffsetX := -2;
         OffsetY := -2;
        end;
        Event.RedoData := ts;
       end;

      TileSetModified;
      TileSetPropsModified;
     end else
      RefreshTileSetProps(False);
    end;
    TAG_ROWSIZE,
    TAG_TLWIDTH,
    TAG_THEIGHT,
    TAG_USE1CLR,
    TAG_TILEBPP,
    TAG_TRCOLOR,
    TAG_COMPCNT:
    begin
     RefreshAll := False;
     case Sender.UserTag of
      TAG_ROWSIZE:
      begin
       for I := 0 to Count - 1 do
        if TileSetsListBox.Selected[I] then
        begin
         Event := FProject.History.AddEvent(nil,
               WideFormat('Tile set ''%s'': Tiles in a row = %d',
               [(Nodes[I] as TTileSet).Name, Sender.Value]));
         FProject.FTileSetInfo[I].Width := Sender.Value;
         Event.RedoData := nil;
        end;
      end;
      TAG_TLWIDTH:
      begin
       for I := 0 to Count - 1 do
        if TileSetsListBox.Selected[I] then
        begin
         ts := Nodes[I] as TTileSet;
         Event := FProject.History.AddEvent(ts,
          WideFormat('Tile set ''%s'': Width = %d', [ts.Name, Sender.Value]));
         ts.Reallocate([], Sender.Value);
         Event.RedoData := ts;
        end;
      end;
      TAG_THEIGHT:
      begin
       for I := 0 to Count - 1 do
        if TileSetsListBox.Selected[I] then
        begin
         ts := Nodes[I] as TTileSet;
         Event := FProject.History.AddEvent(ts,
          WideFormat('Tile set ''%s'': Height = %d', [ts.Name, Sender.Value]));
         ts.Reallocate([], -1, Sender.Value);
         Event.RedoData := ts;
        end;
      end;       
      TAG_USE1CLR:
      begin
       for I := 0 to Count - 1 do
        if TileSetsListBox.Selected[I] then
        begin
         ts := Nodes[I] as TTileSet;
         Event := FProject.History.AddEvent(ts, '');
         ts.DrawFlags := (ts.DrawFlags and not DRAW_PIXEL_MODIFY) or
                  ((-Sender.PickListIndex) and DRAW_PIXEL_MODIFY);
         Event.Text :=
          WideFormat('Tile set ''%s'': Use 1st color index = %s',
          [ts.Name, BoolToStr(ts.DrawFlags and DRAW_PIXEL_MODIFY <> 0, True)]);
         Event.RedoData := ts;
        end;
      end;
      TAG_TILEBPP:
      begin
       case Sender.Value of
        1, 2, 4, 8:
        begin
         for I := 0 to Count - 1 do
          if TileSetsListBox.Selected[I] then
          begin
           ts := Nodes[I] as TTileSet;
           Event := FProject.History.AddEvent(ts,
            WideFormat('Tile set ''%s'': Bits per pixel = %d',
             [ts.Name, Sender.Value]));
           ts.Reallocate([Sender.Value - 1]);
           Event.RedoData := ts;
          end;

         RefreshAll := True;          
        end;
        3, 5, 6, 7:
        begin
         for I := 0 to Count - 1 do
          if TileSetsListBox.Selected[I] then
          begin
           ts := Nodes[I] as TTileSet;
           Event := FProject.History.AddEvent(ts,
            WideFormat('Tile set ''%s'': Bits per pixel = %d',
             [ts.Name, Sender.Value]));
           ts.Reallocate([(Sender.Value - 1) or IMG_PLANAR]);
           Event.RedoData := ts;
          end;

         RefreshAll := True;
        end;
        else
        begin
         WideMessageDlg('Direct color is unsupported.', mtError, [mbOk], 0);
         RefreshTileSetProps(False);
         Exit;
        end;
       end;
      end;
      TAG_TRCOLOR:
      begin
       for I := 0 to Count - 1 do
        if TileSetsListBox.Selected[I] then
        begin
         ts := Nodes[I] as TTileSet;
         Event := FProject.History.AddEvent(ts, '');
         ts.TransparentColor := Sender.Value;
         Event.Text := WideFormat('Tile set ''%s'': Transparent color index = %d',
             [ts.Name, ts.TransparentColor]);
         Event.RedoData := ts;
        end;
      end;
      TAG_COMPCNT:
      begin
       NoAdd := False;
       for I := 0 to Count - 1 do
        if TileSetsListBox.Selected[I] then
         with Nodes[I] as TTileSet do
         begin
          Diff := Sender.Value - (Length(TileBitmap.CompositeFlags) + 1);
          NoAdd := (Diff > 0) and (TileBitmap.BitsCount + Diff > 8);
          if NoAdd then Break;
         end;
       if NoAdd then
       begin
        WideMessageDlg('Cannot add more composites because direct color is unsupported.', mtError, [mbOk], 0);
        RefreshTileSetProps(False);
        Exit;
       end else
       begin
        for I := 0 to Count - 1 do
         if TileSetsListBox.Selected[I] then
         begin
          ts := Nodes[I] as TTileSet;
          with ts do
           if Sender.Value - (Length(TileBitmap.CompositeFlags) + 1) <> 0 then
           begin
            Flg := TileBitmap.FlagsList;
            SetLength(Flg, Sender.Value);
            Event := FProject.History.AddEvent(ts,
                WideFormat('Tile set ''%s'': Composite count = %d',
             [ts.Name, Length(Flg)]));
            Reallocate(Flg);
            Event.RedoData := ts;
            RefreshAll := True;
           end;
         end;
       end;
      end;
     end;

     TileSetModified;
     TileSetPropsModified(RefreshAll);
    end;
    TAG_ATTRCNT:
    begin
     for I := 0 to Count - 1 do
      if TileSetsListBox.Selected[I] then
      begin
       ts := Nodes[I] as TTileSet;
       Event := FProject.History.AddEvent(ts, '');
       ts.AttrCount := Sender.Value;
       Event.Text := WideFormat('Tile set ''%s'': Tile attributes = %d',
             [ts.Name, ts.AttrCount]);
       Event.RedoData := ts;
      end;

     TileSetSelectionChanged;
     TileSetPropsModified(True);
    end;
    TAG_COMPDEF:
    begin
     for I := 0 to Count - 1 do
      if TileSetsListBox.Selected[I] then
       with Nodes[I] as TTileSet do
       begin
        Flg := TileBitmap.FlagsList;
        Flg[Sender.Index] := Sender.Value;
        BCnt := Length(Flg);
        for J := 0 to BCnt - 1 do
         Inc(BCnt, Flg[J] and 31);
        if BCnt > 8 then
        begin
         WideMessageDlg(WideFormat('Direct color is unsupported.', [Sender.Value]),
                      mtError, [mbOk], 0);
         RefreshTileSetProps(False);
         Exit;
        end;
       end;

     BCnt := Sender.Value and 31 + 1;
     Diff := (Sender.Value shr PLANE_FORMAT_SHIFT) and 3;
     while 1 shl Diff > BCnt do
      Dec(Diff);

     for I := 0 to Count - 1 do
      if TileSetsListBox.Selected[I] then
      begin
       ts := Nodes[I] as TTileSet;
       Flg := ts.TileBitmap.FlagsList;
       J := (Sender.Value and IMG_FORMAT_MASK) or (Diff shl PLANE_FORMAT_SHIFT);
       Flg[Sender.Index] := J;
       Event := FProject.History.AddEvent(ts,
           WideFormat('Tile set ''%s'': Composite #%d = 0x%4.4x',
             [ts.Name, Sender.Index + 1, J]));
       ts.Reallocate(Flg);
       Event.RedoData := ts;
      end;

     TileSetModified;
     TileSetPropsModified(True);
    end;
    TAG_ATTRDEF:
    begin
     for I := 0 to Count - 1 do
      if TileSetsListBox.Selected[I] then
      begin
       ts := Nodes[I] as TTileSet;
       Event := FProject.History.AddEvent(ts,
          WideFormat('Tile set ''%s'': Attribute #%d = ''%s''',
             [ts.Name, Sender.Index + 1, Sender.ValueStr]));
       ts.AttrDefs[Sender.Index].adName := Sender.ValueStr;
       Event.RedoData := ts;
      end;

     TileSetSelectionChanged;
     TileSetPropsModified(True);
    end;
    else
    if (Sender.ItemOwner <> nil) and
       (Sender.ItemOwner.UserTag = TAG_ATTRDEF) then
    begin
     Idx := Sender.ItemOwner.Index;
     case Sender.UserTag of
      TAG_DEFTYP:
      begin
       for I := 0 to Count - 1 do
        if TileSetsListBox.Selected[I] then
        begin
         ts := Nodes[I] as TTileSet;
         Event := FProject.History.AddEvent(ts,
             WideFormat('Tile set ''%s'': Attribute #%d, Type = %s',
             [ts.Name, Idx + 1, Sender.ValueStr]));
         with ts.AttrDefs[Idx]^ do
         begin
          case Sender.PickListIndex of
           0..3:
           begin
            Finalize(adEnum);
            case Sender.PickListIndex of
             0:
             begin
              adType := varInt64 or (adType and not varTypeMask);
              adMin := Low(Int64);
              adMax := High(Int64);
              adNumSys := nsDecimal;
             end;
             1:
             begin
              adType := varDouble or (adType and not varTypeMask);
              adPrecision := 15;
             end;
             2:
             begin
              adType := varOleStr or (adType and not varTypeMask);
              adMaxStrLen := 0;
             end;
             else
              adType := varBoolean or (adType and not varTypeMask);
            end;
           end;
           4:
           if adEnum = nil then
           begin
            SetLength(adEnum, 2);
            adEnum[0] := 'Enum1';
            adEnum[1] := 'Enum2';
           end;
          end;
         end;
         Event.RedoData := ts;
        end;
      end;
      TAG_ARRLEN:
      begin
       for I := 0 to Count - 1 do
        if TileSetsListBox.Selected[I] then
        begin
         ts := Nodes[I] as TTileSet;
         Event := FProject.History.AddEvent(ts,
           WideFormat('Tile set ''%s'': Attribute #%d, Max elements count = %d',
             [ts.Name, Idx + 1, Sender.Value]));
         with ts.AttrDefs[Idx]^ do
         begin
          adMaxArrayLen := Sender.Value;
          if adMaxArrayLen <= 1 then
           adType := adType and not varArray else
           adType := adType or varArray;
         end;
         Event.RedoData := ts;
        end;
      end;
      TAG_PRECIS:
      begin
       for I := 0 to Count - 1 do
        if TileSetsListBox.Selected[I] then
        begin
         ts := Nodes[I] as TTileSet;
         Event := FProject.History.AddEvent(ts,
           WideFormat('Tile set ''%s'': Attribute #%d, Precision = %d',
             [ts.Name, Idx + 1, Sender.Value]));
         ts.AttrDefs[Idx].adPrecision := Sender.Value;
         Event.RedoData := ts;
        end;
      end;
      TAG_STRLEN:
      begin
       for I := 0 to Count - 1 do
        if TileSetsListBox.Selected[I] then
        begin
         ts := Nodes[I] as TTileSet;
         Event := FProject.History.AddEvent(ts,
           WideFormat('Tile set ''%s'': Attribute #%d, Max string length = %d',
             [ts.Name, Idx + 1, Sender.Value]));
         ts.AttrDefs[Idx].adMaxStrLen := Sender.Value;
         Event.RedoData := ts;
        end;
      end;
      TAG_ENUMER:
      try
       GetEnumListFromString(Sender.ValueStr, TempEnum);
       L := Length(TempEnum);
       if L < 2 then
        raise Exception.Create('');

       for I := 0 to Count - 1 do
        if TileSetsListBox.Selected[I] then
        begin
         ts := Nodes[I] as TTileSet;
         Event := FProject.History.AddEvent(ts,
           WideFormat('Tile set ''%s'': Attribute #%d, Enumeration = %s',
             [ts.Name, Idx + 1, Sender.ValueStr]));
         with ts.AttrDefs[Idx]^ do
         begin
          SetLength(adEnum, L);
          for J := 0 to L - 1 do
           adEnum[J] := TempEnum[J];
         end;
         Event.RedoData := ts;
        end; 
      except
       WideMessageDlg('Invalid enumeration string', mtError, [mbOk], 0);
       RefreshTileSetProps(False);
       Exit;
      end;
      TAG_INTMIN:
      begin
       for I := 0 to Count - 1 do
        if TileSetsListBox.Selected[I] then
        begin
         ts := Nodes[I] as TTileSet;
         Event := FProject.History.AddEvent(ts,
           WideFormat('Tile set ''%s'': Attribute #%d, Min value = %d',
             [ts.Name, Idx + 1, Sender.Value]));
         Def := ts.AttrDefs[Idx];
         L := Def.adType and varArray;
         InitIntegerAttr(Def^, Def.adName, Sender.Value, Def.adMax, Def.adNumSys);
         Def.adType := Def.adType or L;
//         ts.AttrDefs[Idx].adMin := Sender.Value;
         Event.RedoData := ts;
        end;
      end;
      TAG_INTMAX:
      begin
       for I := 0 to Count - 1 do
        if TileSetsListBox.Selected[I] then
        begin
         ts := Nodes[I] as TTileSet;
         Event := FProject.History.AddEvent(ts,
           WideFormat('Tile set ''%s'': Attribute #%d, Max value = %d',
             [ts.Name, Idx + 1, Sender.Value]));

         Def := ts.AttrDefs[Idx];
         L := Def.adType and varArray;
         InitIntegerAttr(Def^, Def.adName, Def.adMin, Sender.Value, Def.adNumSys);
         Def.adType := Def.adType or L;
//         ts.AttrDefs[Idx].adMax := Sender.Value;
         Event.RedoData := ts;
        end;
      end;
      TAG_NUMSYS:
      begin
       for I := 0 to Count - 1 do
        if TileSetsListBox.Selected[I] then
        begin
         ts := Nodes[I] as TTileSet;
         Event := FProject.History.AddEvent(ts,
           WideFormat('Tile set ''%s'': Attribute #%d, Notation = %s',
             [ts.Name, Idx + 1, Sender.ValueStr]));
         ts.AttrDefs[Idx].adNumSys :=
               TNumerationSystem(Sender.PickListIndex);
         Event.RedoData := ts;
        end;
      end;
     end;
     TileSetSelectionChanged;
     TileSetPropsModified(True);
    end;
   end;
  end;
end;

procedure TTileSetsFrame.TileSetNameModified;
begin
 RefreshTileSetProps(False);
 TileSetsListBox.Invalidate;
 FillComboBox;

 if Assigned(FOnTileSetNameModified) then
  FOnTileSetNameModified(Self);
end;

procedure TTileSetsFrame.WMRefreshTileSetProps(var Message: TMessage);
const
 SNextFrameAttr: WideString = 'Next frame attribute';
 SFrameDelayAttr: WideString = 'Frame delay attribute';
 SFrameRelative: WideString = 'Relative frame index';
var
 I, J, Cnt, L1, L2: Integer;
 OldUserSelect: Boolean;
 UseSet, TileSet: TTileSet;
 VFlags: Integer;
 CFlags: Integer;
 Mask: Integer;
 AFlags: Int64;
 NFlags: Int64;
 EFlags: Int64;
 TypFlg: Int64;
 CntFlg: Int64;
 MinFlg: Int64;
 MaxFlg: Int64;
 SysFlg: Int64;
 Mask64: Int64;
 P1, P2: PWord;
 A1, A2: PAttrDef;
 List: TTntStringList;
 Item: TPropertyListItem;
begin
 TileSetPropLabel.Caption := 'Tile set properties';
 OldUserSelect := TileSetPropEditor.UserSelect;
 TileSetPropEditor.UserSelect := Message.WParam <> 0;
 with TileSetPropEditor, PropertyList do
 begin
  ClearList;
  if (FProject <> nil) and
     (PageControl.ActivePageIndex = 0) then
  begin
   with FProject.FMaps.TileSetList do
    if Count > 0 then
    begin
     if TileSetsListBox.SelCount = 0 then
      TileSetsListBox.Selected[TileSetIndex] := True;

     VFlags := 0;
     CFlags := 0;
     AFlags := 0;
     NFlags := 0;
     EFlags := 0;
     TypFlg := 0;
     CntFlg := 0;
     MinFlg := 0;
     MaxFlg := 0;
     SysFlg := 0;

     UseSet := nil;
     TileSet := nil;

     for I := 0 to Count - 1 do
      if TileSetsListBox.Selected[I] then
      begin
       TileSet := Nodes[I] as TTileSet;
       if UseSet <> nil then
       begin
        if TileSet.Name <> UseSet.Name then
         VFlags := VFlags or (1 shl TAG_SETNAME);
        if TileSet.LastIndex <> UseSet.LastIndex then
         VFlags := VFlags or (1 shl TAG_TILECNT);           
        if FProject.FTileSetInfo[I].Width <>                
           FProject.FTileSetInfo[UseSet.Index].Width then   
         VFlags := VFlags or (1 shl TAG_ROWSIZE);           
        if TileSet.DrawFlags and DRAW_PIXEL_MODIFY <>
            UseSet.DrawFlags and DRAW_PIXEL_MODIFY then
         VFlags := VFlags or (1 shl TAG_USE1CLR);
        if TileSet.TileWidth <> UseSet.TileWidth then
         VFlags := VFlags or (1 shl TAG_TLWIDTH);
        if TileSet.TileHeight <> UseSet.TileHeight then
         VFlags := VFlags or (1 shl TAG_THEIGHT);
        if TileSet.BitCount <> UseSet.BitCount then
         VFlags := VFlags or (1 shl TAG_TILEBPP);
        if TileSet.TransparentColor <> UseSet.TransparentColor then
         VFlags := VFlags or (1 shl TAG_TRCOLOR);
        if TileSet.NextFrameAttr <> UseSet.NextFrameAttr then
         VFlags := VFlags or (1 shl TAG_FRMNEXT);
        if TileSet.FrameRelative <> UseSet.FrameRelative then
         VFlags := VFlags or (1 shl TAG_RELATIV);
        if TileSet.FrameDelayAttr <> UseSet.FrameDelayAttr then
         VFlags := VFlags or (1 shl TAG_FRDELAY);

        if VFlags and (1 shl TAG_COMPCNT) = 0 then
        begin
         L1 := Length(TileSet.TileBitmap.CompositeFlags);
         L2 := Length(UseSet.TileBitmap.CompositeFlags);
         if L1 = L2 then
         begin
          if TileSet.TileBitmap.ImageFlags <> UseSet.TileBitmap.ImageFlags then
           CFlags := CFlags or 1;
          P1 := Pointer(TileSet.TileBitmap.CompositeFlags);
          P2 := Pointer(UseSet.TileBitmap.CompositeFlags);
          Mask := 1 shl 1;
          for J := 0 to L1 - 1 do
          begin
           if P1^ <> P2^ then
            CFlags := CFlags or Mask;
           Inc(P1);
           Inc(P2);
           Mask := Mask shl 1;
          end;
         end else
          VFlags := VFlags or (1 shl TAG_COMPCNT);
        end;

        if VFlags and (1 shl TAG_ATTRCNT) = 0 then
        begin
         if TileSet.AttrCount = UseSet.AttrCount then
         begin
          A1 := TileSet.AttrDefs[0];
          A2 := UseSet.AttrDefs[0];
          Mask64 := 1;
          for J := 0 to TileSet.AttrCount - 1 do
          begin
           if A1.adName <> A2.adName then
            NFlags := NFlags or Mask64;

           if A1.adMaxArrayLen <> A2.adMaxArrayLen then
            CntFlg := CntFlg or Mask64;

           if A1.adEnum <> nil then
           begin
            if (A1.adEnum <> nil) = (A2.adEnum <> nil) then
            begin
             if not CompareStrArrays(A1.adEnum, A2.adEnum) then
              EFlags := EFlags or Mask64;
            end else
             TypFlg := TypFlg or Mask64;
           end else
           begin
            if A1.adType and varTypeMask = A2.adType and varTypeMask then
            begin
             case A1.adType and varTypeMask of
              varBoolean: ;
              varOleStr,
              varString:
              if A1.adMaxStrLen <> A2.adMaxStrLen then
               SysFlg := SysFlg or Mask64;
              varSingle,
              varDouble,
              varCurrency:
              begin
               if A1.adPrecision <> A2.adPrecision then
                SysFlg := SysFlg or Mask64;
              end;
              else
              begin
               if A1.adMin <> A2.adMin then
                MinFlg := MinFlg or Mask64;
               if A1.adMax <> A2.adMax then
                MaxFlg := MaxFlg or Mask64;
               if A1.adNumSys <> A2.adNumSys then
                SysFlg := SysFlg or Mask64;
              end;
             end;
            end else
             TypFlg := TypFlg or Mask64;
           end;

           Inc(A1);
           Inc(A2);
           Mask64 := Mask64 shl 1;
          end;
         end else
          VFlags := VFlags or (1 shl TAG_ATTRCNT);
        end;
       end else
        UseSet := TileSet;
      end;

     if UseSet <> nil then
      with UseSet do
      begin
       if UseSet = TileSet then
        TileSetPropLabel.Caption := WideFormat('Tile set #%d properties', [TileSet.Index + 1]) else
        TileSetPropLabel.Caption := 'Selected tile sets'' properties';

       with AddString('Name', Name) do
       begin
        if VFlags and (1 shl TAG_SETNAME) <> 0 then
         Parameters := PL_MULTIPLE_VALUE;
        UserTag := TAG_SETNAME;
        OnValueChange := TileSetValueChange;
       end;

       with AddDecimal('Tiles count', TilesCount, 0, 1000000) do
       begin
        if VFlags and (1 shl TAG_TILECNT) <> 0 then
         Parameters := PL_MULTIPLE_VALUE;
        UserTag := TAG_TILECNT;
        OnValueChange := TileSetValueChange;
       end;

       with AddDecimal('Tiles in a row', FProject.FTileSetInfo[Index].Width, 1, 256) do
       begin
        if VFlags and (1 shl TAG_ROWSIZE) <> 0 then
         Parameters := PL_MULTIPLE_VALUE;
        UserTag := TAG_ROWSIZE;
        OnValueChange := TileSetValueChange;
       end;

       with AddBoolean('Use 1st color index', DrawFlags and DRAW_PIXEL_MODIFY <> 0) do
       begin
        if VFlags and (1 shl TAG_USE1CLR) <> 0 then
         Parameters := Parameters or PL_MULTIPLE_VALUE;
        UserTag := TAG_USE1CLR;
        OnValueChange := TileSetValueChange;
       end;

    //   with AddData('Tile format', '(Convert...)').SubProperties do
      // begin
       with AddDecimal('Tile width', TileWidth, 1, 256) do
       begin
        if VFlags and (1 shl TAG_TLWIDTH) <> 0 then
         Parameters := PL_MULTIPLE_VALUE;
        UserTag := TAG_TLWIDTH;
        OnValueChange := TileSetValueChange;
       end;

       with AddDecimal('Tile height', TileHeight, 1, 256) do
       begin
        if VFlags and (1 shl TAG_THEIGHT) <> 0 then
         Parameters := PL_MULTIPLE_VALUE;
        UserTag := TAG_THEIGHT;
        OnValueChange := TileSetValueChange;
       end;

       Cnt := BitCount;

       with AddDecimal('Bits per pixel', Cnt, 1, 32) do
       begin
        if VFlags and (1 shl TAG_TILEBPP) <> 0 then
         Parameters := PL_MULTIPLE_VALUE;
        UserTag := TAG_TILEBPP;
        OnValueChange := TileSetValueChange;
       end;

       with AddDecimal('Transparent color index', TransparentColor, 0, High(LongWord)) do
       begin
        if VFlags and (1 shl TAG_TRCOLOR) <> 0 then
         Parameters := PL_MULTIPLE_VALUE;
        UserTag := TAG_TRCOLOR;
        OnValueChange := TileSetValueChange;
       end;

       with TileBitmap, AddDecimal('Composite count',
                       Length(CompositeFlags) + 1, 1, 32) do
       begin
        if VFlags and (1 shl TAG_COMPCNT) <> 0 then
         Parameters := PL_MULTIPLE_VALUE;
        UserTag := TAG_COMPCNT;
        OnValueChange := TileSetValueChange;

        with SubProperties.AddHexadecimal('', ImageFlags, 0, 65535, 4) do
        begin
         UserTag := TAG_COMPDEF;
         if CFlags and 1 = 0 then
         begin
          SubProperties.OnValueChange := TileSetImageFlagsChange;
          FillImageFormatProperties(SubProperties, ImageFlags);
         end else
          Parameters := PL_MULTIPLE_VALUE;
        end;

        Mask := 1 shl 1;
        for I := 0 to Length(CompositeFlags) - 1 do
        begin
         with SubProperties.AddHexadecimal('',
                             CompositeFlags[I], 0, 65535, 4) do
         begin
          UserTag := TAG_COMPDEF;
          if CFlags and Mask = 0 then
          begin
           SubProperties.OnValueChange := TileSetImageFlagsChange;
           FillImageFormatProperties(SubProperties, CompositeFlags[I]);
          end else
           Parameters := PL_MULTIPLE_VALUE;
         end;
         Mask := Mask shl 1;               
        end;
       end;
       //end;

       with AddDecimal('Tile attributes', AttrCount, 0, 64) do
       begin
        UserTag := TAG_ATTRCNT;
        OnValueChange := TileSetValueChange;
        if VFlags and (1 shl TAG_ATTRCNT) = 0 then
        begin
         Mask64 := 1;
         for I := 0 to AttrCount - 1 do
         begin
          with AttrDefs[I]^, SubProperties.AddString('', adName) do
          begin
           if NFlags and Mask64 <> 0 then
            Parameters := PL_MULTIPLE_VALUE;

           UserTag := TAG_ATTRDEF;

           if adEnum <> nil then
                         J := 4 else
           case adType and varTypeMask of
            varBoolean:  J := 3;
            varOleStr,
            varString:   J := 2;
            varSingle,
            varDouble,
            varCurrency: J := 1;
            else         J := 0;
           end;

           with SubProperties.AddPickList('Type', VarTypesList, True, J) do
           begin
            if TypFlg and Mask64 <> 0 then
             Parameters := Parameters or PL_MULTIPLE_VALUE;

            UserTag := TAG_DEFTYP;
           end;
           if adType and varArray <> 0 then
            Cnt := adMaxArrayLen else
            Cnt := 1;
           with SubProperties.AddDecimal('Max elements count', Cnt, 1, 1024) do
           begin
            UserTag := TAG_ARRLEN;

            if CntFlg and Mask64 <> 0 then
             Parameters := PL_MULTIPLE_VALUE;
           end;
           case J of
            1:
            begin
             with SubProperties.AddDecimal('Precision',
                                adPrecision, 2, 15) do
             begin
              if SysFlg and Mask64 <> 0 then
               Parameters := PL_MULTIPLE_VALUE;
              UserTag := TAG_PRECIS;
             end;
            end;
            2:
            with SubProperties.AddDecimal('Max string length', adMaxStrLen,
                               0, High(Word)) do
            begin
             if SysFlg and Mask64 <> 0 then
              Parameters := PL_MULTIPLE_VALUE;
             UserTag := TAG_STRLEN;
            end;
            3: ;
            4:
            with SubProperties.AddString('Enumeration', GetEnumString(adEnum)) do
            begin
             if EFlags and Mask64 = 0 then
              Parameters := PL_BUFFER else
              Parameters := PL_BUFFER or PL_MULTIPLE_VALUE;
             UserTag := TAG_ENUMER;
            end;
            else
            begin
             with SubProperties.AddDecimal('Min value', adMin,
                                     Low(Int64), High(Int64)) do
             begin
              if MinFlg and Mask64 <> 0 then
               Parameters := PL_MULTIPLE_VALUE;
              UserTag := TAG_INTMIN;
             end;
             with SubProperties.AddDecimal('Max value', adMax,
               Low(Int64), High(Int64)) do
             begin
              if MaxFlg and Mask64 <> 0 then
               Parameters := PL_MULTIPLE_VALUE;
              UserTag := TAG_INTMAX;
             end;
             with SubProperties.AddPickList('Notation',
                 ['Decimal', 'Hexadecimal'], Ord(adNumSys)) do
             begin
              if SysFlg and Mask64 <> 0 then
               Parameters := Parameters or PL_MULTIPLE_VALUE;
              UserTag := TAG_NUMSYS;
             end; 
            end;
           end;
          end;
          Mask64 := Mask64 shl 1;
         end;
        end else
         Parameters := PL_MULTIPLE_VALUE;
       end;

       with AddData('Animation', '(...)') do
       begin
        if UseSet = TileSet then
         List := TTntStringList.Create else
         List := nil;
        try
         if UseSet = TileSet then
         begin
          for I := 0 to AttrCount - 1 do
           List.Add(AttrDefs[I].adName);

          List.CaseSensitive := True;
          Item := Subproperties.AddPickList(SNextFrameAttr, List, False, NextFrameAttr, False);
         end else
          Item := SubProperties.AddDecimal(SNextFrameAttr, NextFrameAttr, -1, 63);
         with Item do
         begin
          if VFlags and (1 shl TAG_FRMNEXT) <> 0 then
           Parameters := Parameters or PL_MULTIPLE_VALUE;
          UserTag := TAG_FRMNEXT;
          OnValueChange := TileSetValueChange;
         end;

         with SubProperties.AddBoolean(SFrameRelative, FrameRelative) do
         begin
          if VFlags and (1 shl TAG_RELATIV) <> 0 then
           Parameters := Parameters or PL_MULTIPLE_VALUE;
          UserTag := TAG_RELATIV;
          OnValueChange := TileSetValueChange;
         end;

         if UseSet = TileSet then
          Item := Subproperties.AddPickList(SFrameDelayAttr, List, False, FrameDelayAttr, False) else
          Item := SubProperties.AddDecimal(SFrameDelayAttr, FrameDelayAttr, -1, 63);
         with Item do
         begin
          if VFlags and (1 shl TAG_FRDELAY) <> 0 then
           Parameters := Parameters or PL_MULTIPLE_VALUE;
          UserTag := TAG_FRDELAY;
          OnValueChange := TileSetValueChange;
         end;
        finally
         List.Free;
        end;
       end;
(*
     with AddData('Tile format', '(Convert...)').SubProperties do
     begin
      AddDecimal('Tile width', TileWidth, 1, 256).UserTag := 5;
      AddDecimal('Tile height', TileHeight, 1, 256).UserTag := 6;;
      Cnt := BitCount;
      AddDecimal('Transparent index', TransparentColor, 0, 1 shl Cnt - 1).UserTag := 255;
      AddDecimal('Bits per pixel', Cnt, 1, 32).ReadOnly := True;
      with TileBitmap do
      begin
       AddDecimal('Composite count', Length(CompositeFlags) + 1, 1, 32).UserTag := 7;

       with AddHexadecimal('Composite 1', ImageFlags, 0, 65535, 4) do
       begin
        UserTag := 10;
        ReadOnly := False;
        OnValueChange := TileSetImageFlagsValueChange;
        SubProperties.OnValueChange := TileSetImageFlagsChange;
        FillImageFormatProperties(SubProperties, ImageFlags, False);
       end;

       for I := 0 to Length(CompositeFlags) - 1 do
       begin
        with AddHexadecimal(WideFormat('Composite %d', [I + 2]),
                            CompositeFlags[I], 0, 65535, 4) do
        begin
         ReadOnly := False;
         UserTag := I + 11;
         OnValueChange := TileSetImageFlagsValueChange;
         SubProperties.OnValueChange := TileSetImageFlagsChange;
         FillImageFormatProperties(SubProperties, CompositeFlags[I], False);
        end;
       end;
      end;
     end;
     with AddDecimal('Tile attributes', AttrCount, 0, 64) do
     begin
      UserTag := 8;
      Parameters := PL_BUFFER or PL_EDIT_PROP_CHILDREN;

      for I := 0 to AttrCount - 1 do with AttrDefs[I]^ do
      begin
       if adEnum <> nil then
                     J := 4 else
       case adType and varTypeMask of
        varBoolean:  J := 3;
        varOleStr,
        varString:   J := 2;
        varSingle,
        varDouble,
        varCurrency: J := 1;
        else         J := 0;
       end;
       with SubProperties.AddPickList(adName, VarTypesList, True, J) do
       begin
        UserTag := 9;
        if adType and varArray <> 0 then
         Cnt := adMaxArrayLen else
         Cnt := 1;
        SubProperties.AddDecimal('Max elements count', Cnt, 1, 1024).UserTag := TAG_ARRLEN;
        case J of
         1:
         begin
          SubProperties.AddFloat('Min value', adFloatMin,
            MinDouble, MaxDouble, 15).UserTag := TAG_FLTMIN;
          SubProperties.AddFloat('Max value', adFloatMax,
            MinDouble, MaxDouble, 15).UserTag := TAG_FLTMAX;
          SubProperties.AddDecimal('Precision',
                  adPrecision, 2, 15).UserTag := TAG_PRECIS;
         end;
         2:
          SubProperties.AddDecimal('Max string length', adMaxStrLen,
            0, High(Word)).UserTag := TAG_STRLEN;
         3: ;
         4:
         with SubProperties.AddString('Enumeration', GetEnumString(adEnum)) do
         begin
          Parameters := Parameters or PL_BUFFER;
          UserTag := TAG_ENUMER;
         end;
         else
         begin
          SubProperties.AddDecimal('Min value', adMin,
            Low(Int64), High(Int64)).UserTag := TAG_INTMIN;
          SubProperties.AddDecimal('Max value', adMax,
            Low(Int64), High(Int64)).UserTag := TAG_INTMAX;
          SubProperties.AddPickList('Notation',
           ['Decimal', 'Hexadecimal'], Ord(adNumSys)).UserTag := TAG_NUMSYS;
         end;
        end;
       end;
      end;
     end;       *)
     end;
    end;
  end;
  RootNodeCount := Count;
 end;
 TileSetPropEditor.UserSelect := OldUserSelect;
end;

procedure TTileSetsFrame.TileSetSelected;
begin
 TileSetChanged;

 RefreshTileSetProps(False);

 if (FProject <> nil) and Assigned(FOnTileSetSelected) then
  FOnTileSetSelected(Self);
end;

procedure TTileSetsFrame.RefreshTileSetProps(UserSelect: Boolean);
begin
 PostMessage(Handle, WM_REFRESHTILESETPROPS, Ord(UserSelect), 0);
end;

procedure TTileSetsFrame.RefreshLists;
var
 I: Integer;
begin
 if FProject <> nil then
 begin
  with FProject.FMaps.TileSetList do
   if Count > 0 then
   begin
    if TileSetsListBox.Count <> Count then
     TileSetsListBox.Count := Count else
     TileSetsListBox.Invalidate;
    I := TileSetIndex;
    if I < 0 then
    begin
     I := 0;
     TileSetIndex := 0;
    end;
    FillComboBox;    
    TileSetsListBox.Selected[I] := True;
    TileSetsListBox.ItemIndex := I;
    TileSetSelected;
    Exit;
   end;
 end;
 TileSetsComboBox.Clear;
 TileSetsListBox.Count := 0;
 TileSetSelected;
end;

procedure TTileSetsFrame.TileSetsListBoxDblClick(Sender: TObject);
begin
 if Assigned(FOnOpen) then
  FOnOpen(Self);
end;

procedure TTileSetsFrame.VisualPageShow(Sender: TObject);
begin
 TilesFrame.TileSetView.SetFocus;
end;

procedure TTileSetsFrame.TilesFrameTileSetViewSetPosition(
  Sender: TCustomTileMapView; var NewX, NewY: Integer);
begin
 if FProject <> nil then
  with TilesFrame do
  begin
   if TileSet <> nil then
    with FProject.FTileSetInfo[TileSet.Index] do
    begin
     OffsetX := NewX div Scale;
     OffsetY := NewY div Scale;
    end;
  end;
end;

procedure TTileSetsFrame.TileSetChanged;
var
 I: Integer;
 ts: TTileSet;
begin
 if FProject <> nil then
 begin
  I := TileSetIndex;
  ts := FProject.FMaps.TileSetList.Nodes[I] as TTileSet;
  if ts <> nil then
  begin
   with FProject.FTileSetInfo[I] do
    TilesFrame.DoSetTileSet(ts, True,
     FProject.FMaps.ColorTableList[FProject.FInfo.SelectedPaletteIndex],
      Width, SelectedTile, OffsetX, OffsetY,
       FProject.FInfo.Scales.scTilesFrame[Self.Tag]);
   Exit;
  end;
 end;
 TilesFrame.TileSet := nil;
end;

procedure TTileSetsFrame.CopyTileSets(Cut: Boolean);
var
 Temp: TTileSetList;
 I, X: Integer;
 Stream: TMemoryStream;
 Inf: array of TTileSetInfo;
begin
 if CanAct and (TileSetsListBox.SelCount > 0) then
 begin
  Temp := TTileSetList.Create;
  try
   Temp.FInternalFlags := [secLoading];
   SetLength(Inf, TileSetsListBox.SelCount);
   I := 0;
   with FProject.FMaps.TileSetList do
    for X := 0 to Count - 1 do
     if TileSetsListBox.Selected[X] then
     begin
      Inf[I] := FProject.FTileSetInfo[X];
      Temp.AddNode.Assign(Nodes[X]);
      Inc(I);
     end;

   if OpenClipboard(0) then
   try
    Stream := TMemoryStream.Create;
    try
     Temp.SaveToStream(Stream);
     Stream.WriteBuffer(Inf[0], SizeOf(TTileSetInfo) * Length(Inf));

     SetClipboardData(CF_TILESETS, GlobalHandle(Stream.Memory));
    finally
     Stream.Free;
    end;
   finally
    CloseClipboard;
   end;
  finally
   Temp.Free;
  end;
  if Cut then TileSetDeleteActionExecute(TileSetDeleteAction);
 end;
end;

procedure TTileSetsFrame.PasteTileSets;
var
 Stream: TBufferStream;
 ClpHandle: THandle;
 Paste, List: TTileSetList;
 I, J, Cnt, NewIndex: Integer;
 Event: THistoryEvent;
begin
 if CanAct then
 begin
  Event := nil;
  List := FProject.FMaps.TileSetList;
  Cnt := List.Count; 
  try
   if OpenClipboard(0) then
   try
    ClpHandle := GetClipboardData(CF_TILESETS);
    if ClpHandle <> 0 then
    begin
     Stream := TBufferStream.Create(GlobalLock(ClpHandle), GlobalSize(ClpHandle));
     try
      Paste := TTileSetList.Create;
      try
       Paste.LoadFromStream(Stream);

       Event := AddListEvent('Paste tile sets from clipboard');

       NewIndex := Cnt;
       for J := 0 to Paste.Count - 1 do
        List.AddNode.Assign(Paste.Nodes[J]);

       FProject.Update;

       Stream.ReadBuffer(FProject.FTileSetInfo[NewIndex], SizeOf(TTileSetInfo) * Paste.Count);
      finally
       Paste.Free;
      end;

      TileSetsListBox.Count := List.Count;

      for I := NewIndex to List.Count - 1 do
       TileSetsListBox.Selected[I] := True;

      TileSetIndex := NewIndex;

      TileSetsModified(False);

      if List.Count > Cnt then
       Event.RedoData := List else
      with FProject.History do
       Count := Count - 1;
     finally
      GlobalUnlock(ClpHandle);
      Stream.Free;
     end;
    end;
   finally
    CloseClipboard; 
   end;
  except
   WideMessageDlg('Clipboard data is damaged', mtError, [mbOk], 0);
   if Event <> nil then
    with FProject.History do
    begin
     Position := Position - 1;
     Count := Count - 1;
    end;   
  end;
 end;
end;

function TTileSetsFrame.GetTileSetIndex: Integer;
begin
 if FProject <> nil then
 begin
  Result := FProject.FInfo.SelectedTileSetIndex[Tag];
  with FProject.FMaps.TileSetList do
   if Result >= Count then
    Result := Count - 1;
 end else
  Result := -1;
end;

procedure TTileSetsFrame.SetTileSetIndex(Value: Integer);
begin
 if FProject <> nil then
  FProject.FInfo.SelectedTileSetIndex[Tag] := Value;
end;

procedure TTileSetsFrame.TileSetsListBoxDrawItem(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
var
 Flags: Integer;
 TileSet: TTileSet;
 WStr: WideString;
begin
 if Index >= 0 then
  with Control as TListBox do
  begin
   Canvas.FillRect(Rect);

   Flags := DrawTextBiDiModeFlags(DT_SINGLELINE or DT_VCENTER or DT_NOPREFIX);
   if not UseRightToLeftAlignment then
     Inc(Rect.Left, 2) else
     Dec(Rect.Right, 2);

   TileSet := FProject.FMaps.TileSetList.Nodes[Index] as TTileSet;
   if TileSet <> nil then
   begin
    WStr := WideFormat('%d. %s', [TileSet.Index + 1, TileSet.Name]);
    Windows.DrawTextW(Canvas.Handle, Pointer(WStr), Length(WStr), Rect, Flags);
   end;
  end;
end;

procedure TTileSetsFrame.SelectTileSet(Index: Integer);
begin
 if Index >= 0 then
 begin
  with TileSetsListBox do
  begin
   ClearSelection;
   ItemIndex := Index;
   Selected[Index] := True;
  end;
  TileSetsListBoxClick(TileSetsListBox);
 end;
end;

procedure TTileSetsFrame.PageControlChange(Sender: TObject);
var
 I: Integer;
begin
 TileSetsListBox.Count := TileSetsListBox.Count;
 I := TileSetIndex;
 TileSetsListBox.ItemIndex := I;
 TileSetsListBox.Selected[I] := True;

 RefreshTileSetProps(False);
 if Assigned(FOnPageControlChange) then
  FOnPageControlChange(Self);
end;

procedure TTileSetsFrame.TileSetPropsModified(All: Boolean);
begin
 if All then
  RefreshTileSetProps(False);
  
 if Assigned(FOnTileSetPropsModified) then
  FOnTileSetPropsModified(Self);
end;

procedure TTileSetsFrame.TileSetSelectionChanged;
begin
 TilesFrame.TileSetView.SelectionChanged;
 if Assigned(FOnTileSetSelectionChanged) then
  FOnTileSetSelectionChanged(Self);
end;

procedure TTileSetsFrame.TileSetPropEditorGetPropertyName(
  Sender: TCustomPropertyEditor; Node: PVirtualNode; var Text: WideString);
var
 Data: PPropertyData;
begin
 Data := Sender.GetNodeData(Node);
 if Data.Owner <> nil then
 begin
  case Data.Item.UserTag of
   TAG_COMPDEF:
    Text := WideFormat('Composite #%d', [Data.Item.Index + 1]);
   TAG_ATTRDEF:
    Text := WideFormat('Attribute #%d', [Data.Item.Index + 1]);
  end;
 end;
end;

procedure TTileSetsFrame.TileSetComboSelected;
begin
 if Assigned(FOnTileSetComboSelected) then
  FOnTileSetComboSelected(Self);
end;

procedure TTileSetsFrame.TileSetsListBoxKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
 if ssCtrl in Shift then
 case Key of
  VK_UP:
  begin
   TileSetUpActionExecute(TileSetUpAction);
   Key := 0;
  end;
  VK_DOWN:
  begin
   TileSetDownActionExecute(TileSetDownAction);
   Key := 0;
  end;
 end;
end;

procedure TTileSetsFrame.TileSetAddFromFilesActionExecute(Sender: TObject);
var
 I, NewIndex, SaveCnt: Integer;
 S, Ext: WideString;
 Stream: TStream;
 List, Lst: TTileSetList;
 Inf: array of TTileSetInfo;
 Rec: TTileSetRec;
 Event: THistoryEvent;
begin
 if CanAct then
 begin
  Lst := FProject.FMaps.TileSetList;
  with Lst, OpenTilesDialog do
  begin
   Options := Options + [ofAllowMultiSelect];
   if Execute then
   begin
    NewIndex := Count;
    I := 0;
    while I < Files.Count do
    begin
     SaveCnt := Count;
     S := Files[I];
     try
      Stream := TTntFileStream.Create(S, fmOpenRead or fmShareDenyWrite);
      try
       Ext := WideUpperCase(WideExtractFileExt(S));
       if Ext = '.CTS' then
       begin
        List := TTileSetList.Create;
        try
         List.LoadFromStream(Stream);
         if Stream.Position < Stream.Size then
         begin
          SetLength(Inf, List.Count);
          Stream.ReadBuffer(Inf[0], List.Count * SizeOf(TTileSetInfo));
         end else
          Inf := nil;
         Event := AddListEvent(WideFormat('Add tile sets from ''%s''', [S]));
         InsertList(List, SaveCnt);
         FProject.Update;
         if Inf <> nil then
          Move(Inf[0], FProject.FTileSetInfo[SaveCnt],
                         List.Count * SizeOf(TTileSetInfo));
         TileSetIndex := SaveCnt;
         Event.RedoData := Lst; 
        finally
         List.Free;
        end;
       end
      finally
       Stream.Free;
      end;
      if SaveCnt = Count then
      begin
       Rec := ImportTilesDialog(S, FImportTileSet,
       FProject.FMaps.ColorTableList[FProject.FInfo.SelectedPaletteIndex],
                                   FImportTileSet.TilesCount, FImportWidth);
       with Rec do
        if TileSet <> nil then
        begin
         FImportWidth := SetWidth;
         FImportTileSet.AssignTileFormat(TileSet);
         FImportTileSet.TilesCount := TileSet.TilesCount;
         Event := AddListEvent('');
         TileSet.ClearTileCache;
         AttachNode(TileSet, SaveCnt);
         TileSet.Name := WideChangeFileExt(WideExtractFileName(S), '');
         Event.Text := WideFormat('Add tile set ''%s'' from file',
                                   [TileSet.Name]);
         United := True;
         FProject.Update;
         TileSetIndex := SaveCnt;         
         Event.RedoData := Lst;
        end;
      end;
     except
      on E: Exception do
       case MessageDlg(E.Message, mtError, [mbOk, mbRetry, mbAbort], 0) of
        mrRetry:
        begin
         Count := SaveCnt;
         Continue;
        end;
        mrAbort:
        begin
         Count := SaveCnt;
         if I = 0 then
          Exit else
          Break;
        end;
       end;
     end;
     Inc(I);
    end;
    TileSetsListBox.Count := Count;

    for I := NewIndex to Count - 1 do
     TileSetsListBox.Selected[I] := True;
     
    TileSetIndex := NewIndex;
    TileSetsModified(False);
   end;
  end;
 end;
end;

const
 LoadingErrorMsg: PWideChar = 'Error loading file.';

procedure TTileSetsFrame.TilesImportActionExecute(Sender: TObject);

 function AssignTiles(Source, Dest: TTileSet; WithAttrs: Boolean): Boolean;
 var
  I, SI, X, Y: Integer;
  Tile: TTileItem;
  Event: THistoryEvent;
 begin
  Result := (Source <> nil) and (Source.Count > 0);
  if Result then
  begin
   if TilesFrame.TileSetView.MultiSelected then
    TilesFrame.TileSetView.PasteBufferApply;
    
   Event := FProject.History.AddEvent(Dest,
    WideFormat('Tile set ''%s'': Import tiles from file', [Dest.Name]));
   if TilesFrame.TileSetView.MultiSelected then
   begin
    with TilesFrame.TileSetView do
    begin
     I := 0;
     SI := 0;
     for Y := 0 to MapHeight - 1 do
      for X := 0 to MapWidth - 1 do
      begin
       if Selected[X, Y] then
       begin
        Tile := Source.Indexed[I];
        if Tile = Source.EmptyTile then
        begin
         Tile := Dest.Indexed[SI];
         if Tile <> Dest.EmptyTile then
          Dest.Remove(Tile);
        end else
        with Dest.AddFixed(SI) do
        begin
         if WithAttrs then
          Assign(Tile) else
          AssignTileData(Tile);
         TileIndex := SI; 
        end;
        Inc(I);
       end;
       Inc(SI);
      end;
    end;
   end else
   begin
    if WithAttrs then
     Dest.AssignAttrDefs(Source);

    for I := 0 to Source.TilesCount - 1 do
    begin
     Tile := Source.Indexed[I];
     if Tile = Source.EmptyTile then
     begin
      Tile := Dest.Indexed[I];
      if Tile <> Dest.EmptyTile then
       Dest.Remove(Tile);
     end else
     with Dest.AddFixed(I) do
     begin
      if WithAttrs then
       Assign(Tile) else
       AssignTileData(Tile);
      TileIndex := I; 
     end;
    end;
    Dest.ClearTileCache;
    Dest.LastIndex := Source.LastIndex;
   end;
   Event.RedoData := Dest;
  end;
 end;

var
 ts: TTileSet;
 Stream: TStream;
 S, Msg, Ext: WideString;
 I: Integer;
 TempList: TTileSetList;
 Dlg: TTntForm;
 Combo: TTntComboBox;
 OKBtn, CancelBtn: TTntButton;
 CheckBox: TTntCheckBox;
 Imported, FileLoaded: Boolean;
 Rec: TTileSetRec;
 Inf: array of TTileSetInfo;
begin
 ts := TilesFrame.TileSet;
 if CanAct and (ts <> nil) then
 begin
  with OpenTilesDialog do
   Options := Options - [ofAllowMultiSelect];
  StartDialog;
  if OpenTilesDialog.Execute then
  begin
   Msg := '';
   S := OpenTilesDialog.FileName;
   Imported := False;
   FileLoaded := False;
   try
    Stream := TTntFileStream.Create(S, fmOpenRead);
    try
     Ext := WideUpperCase(WideExtractFileExt(S));
     if Ext = '.CTS' then
     begin
      TempList := TTileSetList.Create;
      try
       TempList.LoadFromStream(Stream);
       if Stream.Position < Stream.Size then
       begin
        SetLength(Inf, TempList.Count);
        Stream.ReadBuffer(Inf[0], SizeOf(TTileSetInfo) * Length(Inf));
       end else
        Inf := nil;

       FileLoaded := True;
       if TempList.Count > 0 then
       begin
        Dlg := TTntForm.CreateNew(Application);
        try
         Dlg.BorderStyle := bsDialog;
         Dlg.ClientWidth := 168;
         Dlg.ClientHeight := 88;
         Dlg.Caption := 'Select Tile Set';
         Dlg.Position := poDesktopCenter;
         
         Combo := TTntComboBox.Create(Dlg);
         Combo.Style := csDropDownList;
         Combo.Width := 153;
         Combo.Height := 21;
         Combo.Left := 8;
         Combo.Top := 8;
         Combo.Parent := Dlg;         

         with TempList do
          for I := 0 to Count - 1 do
           with Nodes[I] as TTileSet do
            Combo.Items.Add(Name);

         Combo.ItemIndex := 0;

         OkBtn := TTntButton.Create(Dlg);
         OkBtn.Default := True;
         OkBtn.ModalResult := mrOk;
         OkBtn.Width := 75;
         OkBtn.Height := 25;
         OkBtn.Left := 7;
         OkBtn.Top := 36 + 22;
         OkBtn.Parent := Dlg;
         OkBtn.Caption := 'Ok';

         CancelBtn := TTntButton.Create(Dlg);
         CancelBtn.Cancel := True;
         CancelBtn.ModalResult := mrCancel;
         CancelBtn.Width := 75;
         CancelBtn.Height := 25;
         CancelBtn.Left := 87;
         CancelBtn.Top := 36 + 22;
         CancelBtn.Parent := Dlg;
         CancelBtn.Caption := 'Cancel';         

         CheckBox := TTntCheckBox.Create(Dlg);
         CheckBox.Checked := False;
         CheckBox.Caption := 'Assign attributes';
         CheckBox.Left := 8;
         CheckBox.Top := 36;
         CheckBox.Parent := Dlg;

         if Dlg.ShowModal = mrOk then
         begin
          I := Combo.ItemIndex;
          Imported := AssignTiles(TempList[I], ts, CheckBox.Checked);
          if (Inf <> nil) and CheckBox.Checked then
           FProject.FTileSetInfo[TileSetIndex] := Inf[I];
         end;

         Rec.SetWidth := 0; 
        finally
         Dlg.Free;
        end;
       end;
      finally
       TempList.Free;
      end;
     end;
    finally
     Stream.Free;
    end;
    if not FileLoaded then
    begin
     Rec := ImportTilesDialog(S, FImportTileSet,
      FProject.FMaps.ColorTableList[FProject.FInfo.SelectedPaletteIndex],
                                 FImportTileSet.TilesCount, FImportWidth);
     with Rec do
      if TileSet <> nil then
      try
       FImportWidth := SetWidth;
       FImportTileSet.AssignTileFormat(TileSet);
       FImportTileSet.TilesCount := TileSet.TilesCount;

       Imported := AssignTiles(TileSet, ts, False);
      finally
       TileSet.Free;
      end;
    end;
    if Imported then
    begin
     if (Rec.SetWidth > 0) and
        not TilesFrame.TileSetView.MultiSelected then
     begin
      FProject.FTileSetInfo[TileSetIndex].Width := Rec.SetWidth;
      TileSetModified;
      TileSetPropsModified(True);
     end else
      TilesModified;

     (FProject.History.LastNode as THistoryEvent).SetProjectInfo;      
    end;
   except
    on EWideFOpenError do Msg := LoadingErrorMsg;
    on EReadError do      Msg := LoadingErrorMsg;
    else                  Msg := 'File format error';
   end;
   if Msg <> '' then
    WideMessageDlg(Msg, mtError, [mbOk], 0);
  end;
  EndDialog;
 end;
end;

procedure TTileSetsFrame.TilesExportActionExecute(Sender: TObject);
var
 Stream: TStream;
 X, Y, I, Cnt: Integer;
 List: TTileSetList;
 ts: TTileSet;
 Tile: TTileItem;
begin
 if CanAct then
  with TilesFrame do
   if TileSet <> nil then
   begin
    StartDialog;
    try
     SaveTilesDialog.FileName := FixedFileName(TileSet.Name);
     if SaveTilesDialog.Execute then
     begin
      if TileSetView.MultiSelected then
       ts := TTileSet.Create else
       ts := TileSet;
      try
       if ts <> TileSet then
       begin
        TileSetView.PasteBufferApply;
        ts.AssignTileFormat(TileSet);
        I := 0;
        Cnt := 0;
        with TileSetView do
         for Y := 0 to MapHeight - 1 do
          for X := 0 to MapWidth - 1 do
          begin
           if Selected[X, Y] then
           begin
            Tile := TileSet.Indexed[I];
            with ts.AddTile do
            begin
             if Tile <> TileSet.EmptyTile then
              Assign(Tile);
             TileIndex := Cnt;
            end;
            Inc(Cnt);
           end;
           Inc(I);
          end;
        ts.TilesCount := Cnt;
       end;

       Stream := TTntFileStream.Create(SaveTilesDialog.FileName, fmCreate);
       try
        case SaveTilesDialog.FilterIndex of
         1:
         begin
          List := TTileSetList.Create;
          try
           List.AddNode.Assign(ts);
           List.SaveToStream(Stream);
           if ts = TileSet then
            Stream.WriteBuffer(FProject.FTileSetInfo[TileSetIndex],
                               SizeOf(TTileSetInfo));
          finally
           List.Free;
          end;
         end;
         2: ts.WriteToStream(Stream, 0, ts.TilesCount, True);
        end;
       finally
        Stream.Free;
       end;
      finally
       if ts <> TileSet then
        ts.Free;
      end;
     end;
    except
     WideMessageDlg('Error saving file', mtError, [mbOk], 0);
    end;
    EndDialog;
   end;
end;

procedure TTileSetsFrame.SaveTilesDialogTypeChange(Sender: TObject);
begin
 case SaveTilesDialog.FilterIndex of
  1: SaveTilesDialog.DefaultExt := 'cts';
  2: SaveTilesDialog.DefaultExt := 'bin';
 end;
end;

procedure TTileSetsFrame.TilesImportExportActionUpdate(Sender: TObject);
begin
 (Sender as TAction).Enabled := CanAct and
                                (TilesFrame.TileSet <> nil);
end;

procedure TTileSetsFrame.EndDialog;
begin
 if Assigned(FOnEndDialog) then
  FOnEndDialog(Self);
end;

procedure TTileSetsFrame.StartDialog;
begin
 if Assigned(FOnStartDialog) then
  FOnStartDialog(Self);
end;

destructor TTileSetsFrame.Destroy;
begin
 FImportTileSet.Free;
 inherited;
end;

procedure TTileSetsFrame.TileSetSaveToFileActionExecute(Sender: TObject);
var
 I: Integer;
 Stream: TStream;
 List: TTileSetList;
 FName, Ext: WideString;
 Inf: array of TTileSetInfo;
begin
 if CanAct then
  with FProject.FMaps.TileSetList do
   if Count > 0 then
   begin
    if TileSetsListBox.SelCount = 0 then
     TileSetsListBox.Selected[TileSetIndex] := True;

    SaveTilesDialog.FilterIndex := 1;
    SaveTilesDialog.DefaultExt := 'cts';

    if TileSetsListBox.SelCount = 1 then
     SaveTilesDialog.FileName := FixedFileName(Sets[TileSetIndex].Name) else
     SaveTilesDialog.FileName := '';

    if SaveTilesDialog.Execute then
    begin
     case SaveTilesDialog.FilterIndex of
      1:
      begin
       List := TTileSetList.Create;
       try
        SetLength(Inf, Count);
        for I := 0 to Count - 1 do
         if TileSetsListBox.Selected[I] then
          with List.AddNode do
          begin
           Assign(Nodes[I]);
           Inf[Index] := FProject.FTileSetInfo[I];
          end;

        Stream := TTntFileStream.Create(SaveTilesDialog.FileName, fmCreate);
        try
         List.SaveToStream(Stream);
         Stream.WriteBuffer(Inf[0], List.Count * SizeOf(TTileSetInfo));
        finally
         Stream.Free;
        end;
       finally
        List.Free;
       end;
      end;
      2:
      if TileSetsListBox.SelCount = 1 then
      begin
       Stream := TTntFileStream.Create(SaveTilesDialog.FileName, fmCreate);
       try
        with Sets[TileSetIndex] do
         WriteToStream(Stream, 0, TilesCount, True);
       finally
        Stream.Free;
       end;
      end else
      begin
       Ext := WideExtractFileExt(SaveTilesDialog.FileName);
       FName := WideChangeFileExt(SaveTilesDialog.FileName, '');
       for I := 0 to Count - 1 do
        if TileSetsListBox.Selected[I] then
         with Nodes[I] as TTileSet do
         begin
          Stream := TTntFileStream.Create(WideFormat('%s %s%s',
                            [FName, FixedFileName(Name), Ext]), fmCreate);
          try
           WriteToStream(Stream, 0, TilesCount, True);
          finally
           Stream.Free;
          end;
         end;
      end;
     end;
    end;
   end;
end;

procedure TTileSetsFrame.TileSetSaveToFileActionUpdate(Sender: TObject);
begin
 TileSetSaveToFileAction.Enabled := CanAct and
                 (FProject.FMaps.TileSetList.Count > 0);
end;

procedure TTileSetsFrame.TilesModified;
begin
 TilesFrame.TileSetView.Invalidate;
 if Assigned(FOnTilesModified) then
  FOnTilesModified(Self);
end;

function TTileSetsFrame.AddListEvent(
  const Text: WideString): THistoryEvent;
begin
 Result := FProject.History.AddEvent(FProject.FMaps.TileSetList, Text);
 Result.OnApply := FProject.TileSetListApply;
end;

function TTileSetsFrame.NewMoveTileSetsEvent(
  Shift: Integer): THistoryEvent;
var
 Str: WideString;
 NewII: Integer;
begin
 if TileSetsListBox.SelCount = 1 then
 begin
  NewII := TileSetIndex;
  Str := WideFormat('Change tile set ''%s'' index to #%d',
        [FProject.FMaps.TileSetList[NewII].Name, NewII + Shift + 1]);
 end else
  Str := 'Move tile sets';

 Result := AddListEvent(Str);
 Result.OnApply := FProject.TileSetListApply;
end;

procedure TTileSetsFrame.AfterUnhook;
begin
 if Assigned(FOnAfterUnhook) then
  FOnAfterUnhook(Self);
end;

procedure TTileSetsFrame.BeforeUnhook;
begin
 if Assigned(FOnBeforeUnhook) then
  FOnBeforeUnhook(Self);
end;

initialization
 CF_TILESETS := RegisterClipboardFormat('Kusharami Tile Sets Container');
 VarTypesList := TTntStringList.Create;
 VarTypesList.Add('Integer value');
 VarTypesList.Add('Floating point value');
 VarTypesList.Add('String');
 VarTypesList.Add('Boolean');
 VarTypesList.Add('Enumerated value');
finalization
 VarTypesList.Free;
end.
