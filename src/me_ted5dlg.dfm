object ImportFromTED5_Dialog: TImportFromTED5_Dialog
  Left = 495
  Top = 293
  BorderStyle = bsDialog
  Caption = 'Import TED5 maps'
  ClientHeight = 370
  ClientWidth = 301
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = TntFormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object L1: TTntLabel
    Left = 8
    Top = 8
    Width = 69
    Height = 13
    Caption = 'Maps data file:'
  end
  object MapsDataFileButton: TTntSpeedButton
    Left = 272
    Top = 24
    Width = 23
    Height = 22
    Caption = '...'
    OnClick = FileBrowseButtonClick
  end
  object L2: TTntLabel
    Left = 8
    Top = 48
    Width = 189
    Height = 13
    Caption = 'Huffman dictionary file for the maps data'
  end
  object MapsDictFileButton: TTntSpeedButton
    Tag = 1
    Left = 272
    Top = 64
    Width = 23
    Height = 22
    Caption = '...'
    OnClick = FileBrowseButtonClick
  end
  object L3: TTntLabel
    Left = 8
    Top = 88
    Width = 81
    Height = 13
    Caption = 'Maps header file:'
  end
  object MapsHeadFileButton: TTntSpeedButton
    Tag = 2
    Left = 272
    Top = 104
    Width = 23
    Height = 22
    Caption = '...'
    OnClick = FileBrowseButtonClick
  end
  object L4: TTntLabel
    Left = 8
    Top = 128
    Width = 85
    Height = 13
    Caption = 'Graphics data file:'
  end
  object GFX_DataFileButton: TTntSpeedButton
    Tag = 3
    Left = 272
    Top = 144
    Width = 23
    Height = 22
    Caption = '...'
    OnClick = FileBrowseButtonClick
  end
  object L5: TTntLabel
    Left = 8
    Top = 168
    Width = 207
    Height = 13
    Caption = 'Huffman dictionary file for the graphics data:'
  end
  object GFX_DictFileButton: TTntSpeedButton
    Tag = 4
    Left = 272
    Top = 184
    Width = 23
    Height = 22
    Caption = '...'
    OnClick = FileBrowseButtonClick
  end
  object L6: TTntLabel
    Left = 8
    Top = 208
    Width = 94
    Height = 13
    Caption = 'Graphics header file'
  end
  object GFX_HeadFileButton: TTntSpeedButton
    Tag = 5
    Left = 272
    Top = 224
    Width = 23
    Height = 22
    Caption = '...'
    OnClick = FileBrowseButtonClick
  end
  object TntLabel1: TTntLabel
    Left = 8
    Top = 248
    Width = 81
    Height = 13
    Caption = '16x16 tiles index:'
  end
  object TntLabel2: TTntLabel
    Left = 104
    Top = 248
    Width = 83
    Height = 13
    Caption = '16x16 tiles count:'
  end
  object TntLabel3: TTntLabel
    Left = 8
    Top = 288
    Width = 90
    Height = 13
    Caption = 'Masked tiles index:'
  end
  object TntLabel4: TTntLabel
    Left = 104
    Top = 288
    Width = 59
    Height = 13
    Caption = 'Icons count:'
  end
  object TntLabel5: TTntLabel
    Left = 200
    Top = 288
    Width = 92
    Height = 13
    Caption = 'Masked tiles count:'
  end
  object MapsDataFileEdit: TTntEdit
    Left = 8
    Top = 24
    Width = 257
    Height = 21
    TabOrder = 0
  end
  object MapsDictFileEdit: TTntEdit
    Left = 8
    Top = 64
    Width = 257
    Height = 21
    TabOrder = 1
  end
  object MapsHeadFileEdit: TTntEdit
    Left = 8
    Top = 104
    Width = 257
    Height = 21
    TabOrder = 2
  end
  object GFX_DataFileEdit: TTntEdit
    Left = 8
    Top = 144
    Width = 257
    Height = 21
    TabOrder = 3
  end
  object GFX_DictFileEdit: TTntEdit
    Left = 8
    Top = 184
    Width = 257
    Height = 21
    TabOrder = 4
  end
  object GFX_HeadFileEdit: TTntEdit
    Left = 8
    Top = 224
    Width = 257
    Height = 21
    TabOrder = 5
  end
  object OKButton: TTntButton
    Left = 8
    Top = 336
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 6
  end
  object CancelButton: TTntButton
    Left = 216
    Top = 336
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 7
  end
  object TilesIndexEdit: TSpinEdit
    Left = 8
    Top = 264
    Width = 89
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 8
    Value = 370
  end
  object TilesCountEdit: TSpinEdit
    Left = 104
    Top = 264
    Width = 89
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 9
    Value = 1440
  end
  object MaskedIndexEdit: TSpinEdit
    Left = 8
    Top = 304
    Width = 89
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 10
    Value = 1810
  end
  object IconsCountEdit: TSpinEdit
    Left = 104
    Top = 304
    Width = 89
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 11
    Value = 72
  end
  object MaskedCountEdit: TSpinEdit
    Left = 200
    Top = 304
    Width = 89
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 12
    Value = 1134
  end
  object OpenDialog: TTntOpenDialog
    Filter = 
      'Keen Dreams Data Files (*.kdr)|*.kdr|Object Files (*.obj)|*.obj|' +
      'All files (*.*)|*.*'
    Left = 120
    Top = 328
  end
end
