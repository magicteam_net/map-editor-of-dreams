unit me_history;

interface

uses
 SysUtils, Classes, NodeLst, TilesEx, me_lib, MyClasses;

type
 PProjectEventInfo = ^TProjectEventInfo;
 TProjectEventInfo = record
    FInfo: TProjectInfo;
    FMapLayerFocus: array of Byte;
    FBrushLayerFocus: array of Byte;
    FTileSetInfo: array of TTileSetInfo;
 end;

 TRedoUndo = (ruUndo, ruRedo);

 THistoryEvent = class(TNode)
  private
    FText: WideString;
    FRedoData: TNode;
    FUndoData: TNode;
    FRedoInfo: TProjectEventInfo;
    //FUndoInfo: TProjectEventInfo;
    FOnApply: TNotifyEvent;
    FSaved: Boolean;
    procedure SetRedoData(Value: TNode);
    procedure SetUndoData(Value: TNode);
    function GetRedoInfo: PProjectEventInfo;
//    function GetUndoInfo: PProjectEventInfo;
  protected
    FEventDataClass: TNodeClass;
    procedure SetProjectInfo;
    procedure RestoreProjectInfo;

    procedure JustAdded; override;
    procedure DataApply(Mode: TRedoUndo); virtual;
    procedure DoApply; virtual;
    procedure DataChanged(NewData: TNode); virtual;    
  public
    property Text: WideString read FText write FText;
    property Saved: Boolean read FSaved write FSaved;

    property RedoData: TNode read FRedoData write SetRedoData;
    property UndoData: TNode read FUndoData write SetUndoData;
    property RedoInfo: PProjectEventInfo read GetRedoInfo;
 //   property UndoInfo: PProjectEventInfo read GetUndoInfo;
    property OnApply: TNotifyEvent read FOnApply write FOnApply;

    procedure Redo;
    procedure Undo;
    destructor Destroy; override;
 end;

 THistoryEventClass = class of THistoryEvent;

 THistorySet = class(TNodeList)
   private
    FPosition: Integer;
    FOnApply: TNotifyEvent;
    function GetEvent(Index: Integer): THistoryEvent;
    procedure SetPosition(Value: Integer);
   protected
    procedure ClearData; override;
    procedure Initialize; override;
    procedure DoApply; virtual;
   public
    property Position: Integer read FPosition write SetPosition;

    property OnApply: TNotifyEvent read FOnApply write FOnApply;

    property Events[Index: Integer]: THistoryEvent read GetEvent;

    function AddEvent(AClass: THistoryEventClass; Data: TNode;
                      const AText: WideString): THistoryEvent;
 end;

 TMapChangeEvent = class(THistoryEvent)
  protected
    procedure Initialize; override;
 end;

 TTileSetChangeEvent = class(THistoryEvent)
  protected
    procedure Initialize; override;
 end;

 TTileChangeEvent = class(THistoryEvent)
  protected
    procedure Initialize; override;
  end;

implementation

uses me_project;

{ THistorySet }

function THistorySet.AddEvent(AClass: THistoryEventClass; Data: TNode;
                              const AText: WideString): THistoryEvent;
begin
 Count := FPosition + 1;
 if AClass = nil then
  Result := THistoryEvent.Create else
  Result := AClass.Create;
 Result.FText := AText;
 AddCreated(Result, True); 
 Result.SetUndoData(Data);
end;

procedure THistorySet.ClearData;
begin
 FPosition := -1;
end;

procedure THistorySet.DoApply;
begin
 if Assigned(FOnApply) then
  FOnApply(Self);
end;

function THistorySet.GetEvent(Index: Integer): THistoryEvent;
begin
 Result := Nodes[Index] as THistoryEvent;
end;

procedure THistorySet.Initialize;
begin
 FNodeClass := THistoryEvent;
 FAssignableClass := THistorySet;
 FNodeClassAssignable := True;
 FPosition := -1;
end;

procedure THistorySet.SetPosition(Value: Integer);
var
 Event: THistoryEvent;
 I: Integer;
 Redo: Boolean;
begin
 if FPosition < Value then
 begin
  for I := FPosition + 1 to Value do
  begin
   Event := Events[I];
   if Event <> nil then
    Event.Redo;
  end;
  Redo := True;
 end else
 if FPosition > Value then
 begin
  for I := FPosition downto Value + 1 do
  begin
   Event := Events[I];
   if Event <> nil then
    Event.Undo;
  end;
  Redo := False;
 end else
  Exit;

 FPosition := Value;
 Event := Events[Value];
 if Event <> nil then
 begin
  if not Redo then
   Event.Redo;
  Event.RestoreProjectInfo;
 end;
 DoApply;
end;

{ THistoryEvent }

procedure THistoryEvent.DataApply(Mode: TRedoUndo);
var
 Data: TNode;
begin
 if Mode = ruUndo then
  Data := FUndoData
 else
  Data := FRedoData;

 if Data <> nil then
  (Data.Owner as TNodeList).Nodes[Data.Index].Assign(Data);
end;

procedure THistoryEvent.DataChanged(NewData: TNode);
begin
//
end;

destructor THistoryEvent.Destroy;
begin
 FRedoData.Free;
 FUndoData.Free;
 inherited;
end;

procedure THistoryEvent.DoApply;
begin
 if Assigned(FOnApply) then
  FOnApply(Self) else
  THistorySet(Owner).DoApply;
end;

function THistoryEvent.GetRedoInfo: PProjectEventInfo;
begin
 Result := @FRedoInfo;
end;
            {
function THistoryEvent.GetUndoInfo: PProjectEventInfo;
begin
 Result := @FUndoInfo;
end;
                     }
procedure THistoryEvent.JustAdded;
begin
 (FOwner as THistorySet).FPosition := Index;
end;

procedure THistoryEvent.Redo;
begin
 DataApply(ruRedo);
end;

procedure THistoryEvent.RestoreProjectInfo;
var
 L: Integer;
 Project: TMapEditorProject;
begin
 with THistorySet(Owner) do
  if Owner is TMapEditorProject then
  begin
   Project := TMapEditorProject(Owner);

   Project.FInfo := FRedoInfo.FInfo;

   L := Length(FRedoInfo.FMapLayerFocus);
   SetLength(Project.FMapLayerFocus, L);
   Move(FRedoInfo.FMapLayerFocus[0], Project.FMapLayerFocus[0], L);

   L := Length(FRedoInfo.FBrushLayerFocus);
   SetLength(Project.FBrushLayerFocus, L);
   Move(FRedoInfo.FBrushLayerFocus[0], Project.FBrushLayerFocus[0], L);

   L := Length(FRedoInfo.FTileSetInfo);
   SetLength(Project.FTileSetInfo, L);
   Move(FRedoInfo.FTileSetInfo[0], Project.FTileSetInfo[0], L * SizeOf(TTileSetInfo));

   Project.Update;
  end;
end;

procedure THistoryEvent.SetProjectInfo;
var
 L: Integer;
 Project: TMapEditorProject;
begin
 with THistorySet(Owner) do
  if Owner is TMapEditorProject then
  begin
   Project := TMapEditorProject(Owner);
     
   FRedoInfo.FInfo := Project.FInfo;

   L := Length(Project.FMapLayerFocus);
   SetLength(FRedoInfo.FMapLayerFocus, L);
   Move(Project.FMapLayerFocus[0], FRedoInfo.FMapLayerFocus[0], L);

   L := Length(Project.FBrushLayerFocus);
   SetLength(FRedoInfo.FBrushLayerFocus, L);
   Move(Project.FBrushLayerFocus[0], FRedoInfo.FBrushLayerFocus[0], L);

   L := Length(Project.FTileSetInfo);
   SetLength(FRedoInfo.FTileSetInfo, L);
   Move(Project.FTileSetInfo[0], FRedoInfo.FTileSetInfo[0], L * SizeOf(TTileSetInfo));
  end;
end;

procedure THistoryEvent.SetRedoData(Value: TNode);
begin
 FRedoData.Free;
 if Value <> nil then
 begin
  FRedoData := FEventDataClass.Create;
  FRedoData.Owner := Value.Owner;
  if FRedoData is TBaseSectionedList then
   TBaseSectionedList(FRedoData).NoRename := True;
  FRedoData.Assign(Value);
  FRedoData.Index := Value.Index;
 end else
  FRedoData := nil;

 SetProjectInfo;

 DataChanged(Value);  
end;

procedure THistoryEvent.SetUndoData(Value: TNode);
begin
 FUndoData.Free;
 if Value <> nil then
 begin
  FUndoData := FEventDataClass.Create;
  FUndoData.Owner := Value.Owner;
  if FUndoData is TBaseSectionedList then
   TBaseSectionedList(FUndoData).NoRename := True;
  FUndoData.Assign(Value);
  FUndoData.Index := Value.Index;
 end else
  FUndoData := nil;

 SetProjectInfo;
end;

procedure THistoryEvent.Undo;
begin
 DataApply(ruUndo);
end;

{ TTileChangeEvent }

procedure TTileChangeEvent.Initialize;
begin
 FEventDataClass := TTileItem;
end;

{ TTileSetChangeEvent }

procedure TTileSetChangeEvent.Initialize;
begin
 FEventDataClass := TTileSet;
end;

{ TMapChangeEvent }

procedure TMapChangeEvent.Initialize;
begin
 FEventDataClass := TMap;
end;

end.
