unit me_ted5dlg;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, TntForms, TntDialogs, StdCtrls, TntStdCtrls, Buttons, TntButtons,
  Spin;

type
  TImportFromTED5_Dialog = class(TTntForm)
    MapsDataFileEdit: TTntEdit;
    L1: TTntLabel;
    MapsDataFileButton: TTntSpeedButton;
    L2: TTntLabel;
    MapsDictFileEdit: TTntEdit;
    MapsDictFileButton: TTntSpeedButton;
    L3: TTntLabel;
    MapsHeadFileEdit: TTntEdit;
    MapsHeadFileButton: TTntSpeedButton;
    L4: TTntLabel;
    GFX_DataFileEdit: TTntEdit;
    GFX_DataFileButton: TTntSpeedButton;
    L5: TTntLabel;
    GFX_DictFileEdit: TTntEdit;
    GFX_DictFileButton: TTntSpeedButton;
    L6: TTntLabel;
    GFX_HeadFileEdit: TTntEdit;
    GFX_HeadFileButton: TTntSpeedButton;
    OKButton: TTntButton;
    CancelButton: TTntButton;
    OpenDialog: TTntOpenDialog;
    TilesIndexEdit: TSpinEdit;
    TilesCountEdit: TSpinEdit;
    TntLabel1: TTntLabel;
    TntLabel2: TTntLabel;
    TntLabel3: TTntLabel;
    MaskedIndexEdit: TSpinEdit;
    TntLabel4: TTntLabel;
    IconsCountEdit: TSpinEdit;
    MaskedCountEdit: TSpinEdit;
    TntLabel5: TTntLabel;
    procedure FileBrowseButtonClick(Sender: TObject);
    procedure TntFormCreate(Sender: TObject);
  private
    FFieldsList: array[0..5] of TTntEdit;
  public
    { Public declarations }
  end;

 TTed5FileNamesRec = record
  MapsDataFileName: WideString;
  MapsDictFileName: WideString;
  MapsHeadFileName: WideString;
  GFX_DataFileName: WideString;
  GFX_DictFileName: WideString;
  GFX_HeadFileName: WideString;
  TilesIndex: Integer;
  TilesCount: Integer;
  MaskedIndex: Integer;
  IconsCount: Integer;
  MaskedCount: Integer;
 end;

function ImportFromTED5_Dialog(var OutRec: TTed5FileNamesRec): Boolean;

implementation

{$R *.DFM}

procedure TImportFromTED5_Dialog.FileBrowseButtonClick(Sender: TObject);
begin
 with Sender as TTntSpeedButton, FFieldsList[Tag] do
 begin
  OpenDialog.FileName := Text;
  if OpenDialog.Execute then
  begin
   Text := OpenDialog.FileName;
   SelectAll;
   SetFocus;
  end;
 end;
end;

function ImportFromTED5_Dialog(var OutRec: TTed5FileNamesRec): Boolean;
var
 Dlg: TImportFromTED5_Dialog;
begin
 Application.CreateForm(TImportFromTED5_Dialog, Dlg);
 try
  Result := Dlg.ShowModal = mrOk;
  if Result then with OutRec do
  begin
   MapsDataFileName := Dlg.MapsDataFileEdit.Text;
   MapsDictFileName := Dlg.MapsDictFileEdit.Text;
   MapsHeadFileName := Dlg.MapsHeadFileEdit.Text;
   GFX_DataFileName := Dlg.GFX_DataFileEdit.Text;
   GFX_DictFileName := Dlg.GFX_DictFileEdit.Text;
   GFX_HeadFileName := Dlg.GFX_HeadFileEdit.Text;
   TilesIndex := Dlg.TilesIndexEdit.Value;
   TilesCount := Dlg.TilesCountEdit.Value;
   MaskedIndex := Dlg.MaskedIndexEdit.Value;
   IconsCount := Dlg.IconsCountEdit.Value;
   MaskedCount := Dlg.MaskedCountEdit.Value;
  end;
 finally
  Dlg.Free;
 end;
end;

procedure TImportFromTED5_Dialog.TntFormCreate(Sender: TObject);
begin
 FFieldsList[0] := MapsDataFileEdit;
 FFieldsList[1] := MapsDictFileEdit;
 FFieldsList[2] := MapsHeadFileEdit;
 FFieldsList[3] := GFX_DataFileEdit;
 FFieldsList[4] := GFX_DictFileEdit;
 FFieldsList[5] := GFX_HeadFileEdit;
end;

end.
