unit me_main;

interface

uses
  Windows, Messages, me_plugin, me_plugutils, Controls, Forms, Graphics, Menus,
  ColorReduction, NodeLst, HexUnit, SysUtils, Variants, Classes, TntComCtrls,
  Dialogs, TntForms, ExtCtrls, ToolWin, ComCtrls, TntSysUtils, TntClasses,
  TntMenus, me_mapfrm, ActnList, TntActnList, TntDialogs, TilesEx, me_plugdlg,
  StdCtrls, TntStdCtrls, me_lib, CheckLst, TntCheckLst, MyClasses, MyClassesEx,
  me_tilesfrm, VirtualTrees, PropEditor, me_palfrm, TntFileCtrl, TileMapView,
  PlugInterface, PropContainer, PropDialog, ImgList, CustomizeDlg, StdActns,
  LayerListBox, me_mappropsfrm, me_editpage, me_tilesetpropsfrm, me_project,
  BitmapEx, PickColorDlg, me_tileprops, me_tileeditframe, me_process;

const
 SInitialEvent: WideString = 'Initial event';
 PROP_LIST_CNT = 5;
 MAP_PROP = 0;
 LAYER_PROP = 1;
 SELCELL_PROP = 2;
 TILESET_PROP = 3;
 SELTILE_PROP = 4;
 WM_CONTENT_REDRAW = WM_USER + 999;
 WM_FILLCELLPROPS =  WM_USER + 998;
 WM_TILEEDIT = WM_USER + 999;
// WM_FILLTILEPROPS =  WM_USER + 997;
 WM_REFRESHCTBLPROPS = WM_USER + 996;
(*
 WM_FILLSETPROPS =   WM_USER + 997;
 WM_FILLMAPPROPS =   WM_USER + 996;

 WM_FILLLAYERPROPS = WM_USER + 994;*)

type
  TRefreshMapContent = (rmcView, rmcLayersList, rmcLayerProps);
  TRefreshMapContentSet = set of TRefreshMapContent;
  TDrawTool = (dtSingleSelect,
               dtMultiSelect,
               dtMagicWand,
               dtPicker,
               dtPencil,
               dtBrush,
               dtEraser,
               dtFloodFill);
  TSpecialDrawTool = (sdtLine, sdtRect, sdtPoly, sdtEllipse);

  PBrushPattern = ^TBrushPattern;
  TBrushPattern = packed record
   W: Byte;
   H: Byte;
   X: ShortInt;
   Y: ShortInt;
   P: Pointer;
  end;

  TDrawUnitFunction = function (Control: TCustomTileMapView; DrawX, DrawY: Integer): Boolean of object; 

  TMainForm = class(TTntForm)
    MainMenu: TTntMainMenu;
    MenuFile: TTntMenuItem;
    MenuFileExitItem: TTntMenuItem;
    ActionList: TTntActionList;
    ExitAction: TTntAction;
    MainStatusBar: TTntStatusBar;
    MenuFileSeparator1: TTntMenuItem;
    ImportfromTED5map1: TTntMenuItem;
    LeftSplitter: TSplitter;
    RightSplitter: TSplitter;
    ColorsImportAction: TTntAction;
    Edit1: TTntMenuItem;
    OpenActDialog: TTntOpenDialog;
    OpenAction: TTntAction;
    SaveAction: TTntAction;
    SaveAsAction: TTntAction;
    Saveproject1: TTntMenuItem;
    Saveproject2: TTntMenuItem;
    Saveprojectas1: TTntMenuItem;
    N1: TTntMenuItem;
    NewAction: TTntAction;
    Newproject1: TTntMenuItem;
    OpenProjectDialog: TTntOpenDialog;
    SaveProjectDialog: TTntSaveDialog;
    SelCellPropLabel: TTntLabel;
    SelCellPropEditor: TPropertyEditor;
    ExportmapstoSMDformat1: TTntMenuItem;
    ImgList: TImageList;
    MapNewAction: TTntAction;
    MapUpAction: TTntAction;
    MapDownAction: TTntAction;
    MapDeleteAction: TTntAction;
    LayerNewAction: TTntAction;
    LayerUpAction: TTntAction;
    LayerDownAction: TTntAction;
    LayerDeleteAction: TTntAction;
    ColorTableNewAction: TTntAction;
    ColorTableUpAction: TTntAction;
    ColorTableDownAction: TTntAction;
    ColorTableDeleteAction: TTntAction;
    TileSetNewAction: TTntAction;
    TileSetUpAction: TTntAction;
    TileSetDownAction: TTntAction;
    TileSetDeleteAction: TTntAction;
    BrushNewAction: TTntAction;
    BrushUpAction: TTntAction;
    BrushDownAction: TTntAction;
    BrushDeleteAction: TTntAction;
    Splitter8: TSplitter;
    BrushEditModeAction: TTntAction;
    ColorTableActiveModeAction: TTntAction;
    TileSetEditModeAction: TTntAction;
    TileEditModeAction: TTntAction;
    MapEditModeAction: TTntAction;
    EditUndoAction: TTntAction;
    EditRedoAction: TTntAction;
    EditCutAction: TTntAction;
    EditCopyAction: TTntAction;
    EditPasteAction: TTntAction;
    EditDeleteAction: TTntAction;
    SelectAllAction: TTntAction;
    ToolMultiSelectAction: TTntAction;
    ToolPencilAction: TTntAction;
    ToolBrushAction: TTntAction;
    ToolEraserAction: TTntAction;
    ToolSingleSelectAction: TTntAction;
    ViewZoomInAction: TTntAction;
    ViewZoomOutAction: TTntAction;
    SelectedCellPanel: TPanel;
    SelectedTilePanel: TPanel;
    ToolFloodFillAction: TTntAction;
    ToolDropperAction: TTntAction;
    SelectDeselectAction: TTntAction;
    SelectInverseAction: TTntAction;
    PropUpAction: TTntAction;
    PropDownAction: TTntAction;
    PropDeleteAction: TTntAction;
    PropAddAction: TTntAction;
    ToolsPanel: TPanel;
    Splitter7: TSplitter;
    FileEditToolBar: TTntToolBar;
    FileNewButton: TTntToolButton;
    SaveWithPluginBtn: TTntToolButton;
    LoadWithPluginBtn: TTntToolButton;
    FileOpenButton: TTntToolButton;
    FileSaveButton: TTntToolButton;
    FileSaveAsButton: TTntToolButton;
    EditCutButton: TTntToolButton;
    EditCopyButton: TTntToolButton;
    EditPasteButton: TTntToolButton;
    EditClearSelectionButton: TTntToolButton;
    ViewZoomInButton: TTntToolButton;
    ViewZoomOutButton: TTntToolButton;
    DrawingToolsBar: TTntToolBar;
    ToolSingleSelectButton: TTntToolButton;
    ToolMultiSelectButton: TTntToolButton;
    ToolMagicWandButton: TTntToolButton;
    ToolPencilButton: TTntToolButton;
    ToolBrushButton: TTntToolButton;
    ToolEraserButton: TTntToolButton;
    ToolFloodFillButton: TTntToolButton;
    ToolPickerButton: TTntToolButton;
    ColTblPanel: TPanel;
    Splitter10: TSplitter;
    ColorTablesListBoxPanel: TPanel;
    ColorTablesListBox: TListBox;
    ColTblToolBar: TTntToolBar;
    ColTblNewToolBtn: TTntToolButton;
    ColTblUpToolBtn: TTntToolButton;
    ColTblDonToolBtn: TTntToolButton;
    ColTblDelToolBtn: TTntToolButton;
    PaletteFrame: TPaletteFrame;
    ColTblPropsPanel: TPanel;
    ColTblPropsLabel: TTntLabel;
    PalPropEditor: TPropertyEditor;
    ToolMagicWandAction: TTntAction;
    ToolDrawLineAction: TTntAction;
    ToolDrawRectangleAction: TTntAction;
    ToolDrawPolygonAction: TTntAction;
    ToolDrawEllipseAction: TTntAction;
    BrushesFrame: TMapsFrame;
    LeftPanel: TPanel;
    TileSetsPanel: TPanel;
    MapsFrm: TMapsFrame;
    CentralPanel: TPanel;
    RightPanel: TPanel;
    Splitter3: TSplitter;
    Splitter2: TSplitter;
    MainEditPanel: TPanel;
    DrawModeToolBar: TTntToolBar;
    ToolButton1: TTntToolButton;
    ToolButton2: TTntToolButton;
    ToolButton3: TTntToolButton;
    ToolButton4: TTntToolButton;
    EditModeToolBar: TTntToolBar;
    MapEditModeToolButton: TTntToolButton;
    BrushEditModeToolButton: TTntToolButton;
    TileSetEditModeToolButton: TTntToolButton;
    TileEditModeToolButton: TTntToolButton;
    DrawSizesToolBar: TTntToolBar;
    ToolSizeBtn1: TTntToolButton;
    ToolSizeBtn2: TTntToolButton;
    ToolSizeBtn3: TTntToolButton;
    ToolSizeBtn4: TTntToolButton;
    BrushSmallAction: TTntAction;
    BrushMediumAction: TTntAction;
    BrushLargeAction: TTntAction;
    BrushVeryLargeAction: TTntAction;
    ColTblsPopup: TTntPopupMenu;
    Replacepalette1: TTntMenuItem;
    ColorsExportAction: TTntAction;
    SaveActDialog: TTntSaveDialog;
    ColTblAddFromFilesAction: TTntAction;
    AddColorTablesFrom1: TTntMenuItem;
    NewColorTable1: TTntMenuItem;
    N4: TTntMenuItem;
    MoveColorTableUp1: TTntMenuItem;
    MoveColorTableDown1: TTntMenuItem;
    RemoveColorTable1: TTntMenuItem;
    N5: TTntMenuItem;
    Project1: TTntMenuItem;
    TileSetsMenu: TTntMenuItem;
    ColorTablesMenu: TTntMenuItem;
    BrushesMenu: TTntMenuItem;
    NewTileSet1: TTntMenuItem;
    New1: TTntMenuItem;
    AddFrom1: TTntMenuItem;
    N6: TTntMenuItem;
    ImportColorsFrom1: TTntMenuItem;
    ExportColorsTo1: TTntMenuItem;
    NewBrush1: TTntMenuItem;
    N7: TTntMenuItem;
    MapsMenu: TTntMenuItem;
    NewMap1: TTntMenuItem;
    N8: TTntMenuItem;
    MoveUp1: TTntMenuItem;
    MoveDown1: TTntMenuItem;
    N9: TTntMenuItem;
    Remove1: TTntMenuItem;
    N10: TTntMenuItem;
    New2: TTntMenuItem;
    MoveDown2: TTntMenuItem;
    N11: TTntMenuItem;
    Delete1: TTntMenuItem;
    N12: TTntMenuItem;
    New3: TTntMenuItem;
    MoveDown3: TTntMenuItem;
    N13: TTntMenuItem;
    Delete2: TTntMenuItem;
    Layer1: TTntMenuItem;
    New4: TTntMenuItem;
    N14: TTntMenuItem;
    MoveUp2: TTntMenuItem;
    MoveDown4: TTntMenuItem;
    N15: TTntMenuItem;
    Delete3: TTntMenuItem;
    Layer2: TTntMenuItem;
    New5: TTntMenuItem;
    N16: TTntMenuItem;
    MoveUp3: TTntMenuItem;
    MoveDown5: TTntMenuItem;
    N17: TTntMenuItem;
    Delete4: TTntMenuItem;
    N18: TTntMenuItem;
    MoveUp4: TTntMenuItem;
    MoveDown6: TTntMenuItem;
    N19: TTntMenuItem;
    Delete5: TTntMenuItem;
    N20: TTntMenuItem;
    N21: TTntMenuItem;
    ColorsPopup: TTntPopupMenu;
    TntMenuItem10: TTntMenuItem;
    Paste1: TTntMenuItem;
    Cut1: TTntMenuItem;
    N22: TTntMenuItem;
    Copy1: TTntMenuItem;
    Copy2: TTntMenuItem;
    Paste2: TTntMenuItem;
    N23: TTntMenuItem;
    N24: TTntMenuItem;
    All2: TTntMenuItem;
    Deselect2: TTntMenuItem;
    Inverse2: TTntMenuItem;
    Undo1: TTntMenuItem;
    Redo1: TTntMenuItem;
    N25: TTntMenuItem;
    Cut2: TTntMenuItem;
    Copy3: TTntMenuItem;
    Paste3: TTntMenuItem;
    ClearSelection2: TTntMenuItem;
    N27: TTntMenuItem;
    All3: TTntMenuItem;
    Deselect3: TTntMenuItem;
    Inverse3: TTntMenuItem;
    N2: TTntMenuItem;
    N26: TTntMenuItem;
    SelectAll1: TTntMenuItem;
    Deselect1: TTntMenuItem;
    Inverse1: TTntMenuItem;
    N28: TTntMenuItem;
    Delete6: TTntMenuItem;
    MapsPopup: TTntPopupMenu;
    MapNewMenuItem: TTntMenuItem;
    TntMenuItem6: TTntMenuItem;
    MapMoveUpMenuItem: TTntMenuItem;
    MapMoveDownMenuItem: TTntMenuItem;
    TntMenuItem9: TTntMenuItem;
    TntMenuItem11: TTntMenuItem;
    TntMenuItem12: TTntMenuItem;
    TntMenuItem13: TTntMenuItem;
    TntMenuItem14: TTntMenuItem;
    TntMenuItem15: TTntMenuItem;
    TntMenuItem16: TTntMenuItem;
    TntMenuItem17: TTntMenuItem;
    TntMenuItem18: TTntMenuItem;
    MapDeleteMenuItem: TTntMenuItem;
    LayersPopup: TTntPopupMenu;
    TntMenuItem32: TTntMenuItem;
    TntMenuItem33: TTntMenuItem;
    TntMenuItem34: TTntMenuItem;
    TntMenuItem35: TTntMenuItem;
    TntMenuItem36: TTntMenuItem;
    TntMenuItem37: TTntMenuItem;
    TntMenuItem38: TTntMenuItem;
    TntMenuItem39: TTntMenuItem;
    TntMenuItem40: TTntMenuItem;
    TntMenuItem41: TTntMenuItem;
    TntMenuItem42: TTntMenuItem;
    TntMenuItem43: TTntMenuItem;
    TntMenuItem44: TTntMenuItem;
    TntMenuItem45: TTntMenuItem;
    ColorsMakeGradientAction: TTntAction;
    N29: TTntMenuItem;
    ReplaceSelected1: TTntMenuItem;
    ColorReplaceAction: TTntAction;
    ReplaceColor1: TTntMenuItem;
    TileSetsPopup: TTntPopupMenu;
    TileSetNewMenuItem: TTntMenuItem;
    TntMenuItem3: TTntMenuItem;
    TileSetMoveUpMenuItem: TTntMenuItem;
    TileSetMoveDownMenuItem: TTntMenuItem;
    TntMenuItem20: TTntMenuItem;
    TntMenuItem21: TTntMenuItem;
    TntMenuItem22: TTntMenuItem;
    TntMenuItem23: TTntMenuItem;
    TntMenuItem24: TTntMenuItem;
    TntMenuItem25: TTntMenuItem;
    TntMenuItem26: TTntMenuItem;
    TntMenuItem27: TTntMenuItem;
    TntMenuItem28: TTntMenuItem;
    TileSetDeleteMenuItem: TTntMenuItem;
    TilesPopup: TTntPopupMenu;
    TntMenuItem30: TTntMenuItem;
    TntMenuItem46: TTntMenuItem;
    TntMenuItem47: TTntMenuItem;
    TntMenuItem48: TTntMenuItem;
    TntMenuItem49: TTntMenuItem;
    TntMenuItem50: TTntMenuItem;
    TntMenuItem51: TTntMenuItem;
    TntMenuItem52: TTntMenuItem;
    TntMenuItem53: TTntMenuItem;
    TntMenuItem54: TTntMenuItem;
    TntMenuItem55: TTntMenuItem;
    TilesTileEditAction: TTntAction;
    MapPopup: TTntPopupMenu;
    TntMenuItem59: TTntMenuItem;
    TntMenuItem60: TTntMenuItem;
    TntMenuItem61: TTntMenuItem;
    TntMenuItem62: TTntMenuItem;
    TntMenuItem63: TTntMenuItem;
    TntMenuItem64: TTntMenuItem;
    TntMenuItem65: TTntMenuItem;
    TntMenuItem66: TTntMenuItem;
    TntMenuItem67: TTntMenuItem;
    TilesFrm: TTileSetsFrame;
    ColTblComboBox: TTntComboBox;
    ColTblViewModeBar: TTntTabControl;
    HistoryPages: TTntPageControl;
    History: TTntTabSheet;
    Macro: TTntTabSheet;
    Panel1: TPanel;
    MacroToolBar: TTntToolBar;
    AddMacroButton: TTntToolButton;
    MacroUpBtn: TTntToolButton;
    MacroDownBtn: TTntToolButton;
    MacroDeleteButton: TTntToolButton;
    MacroListBox: TListBox;
    MacroActionsListBox: TListBox;
    TntToolButton7: TTntToolButton;
    Panel2: TPanel;
    MacroPlayToolBar: TTntToolBar;
    TntToolButton4: TTntToolButton;
    TntToolButton8: TTntToolButton;
    TntToolButton9: TTntToolButton;
    N30: TTntMenuItem;
    ReplaceColorUnderCursor1: TTntMenuItem;
    RemoveBrush1: TTntMenuItem;
    ColorTableSaveToAction: TTntAction;
    N3: TTntMenuItem;
    N31: TTntMenuItem;
    TilePropsFrm: TTilePropsFrame;
    LoadWithPluginAction: TTntAction;
    SaveWithPluginAction: TTntAction;
    N32: TTntMenuItem;
    RemoveBrush2: TTntMenuItem;
    AddFrom2: TTntMenuItem;
    SaveTo1: TTntMenuItem;
    N33: TTntMenuItem;
    ImportTiles1: TTntMenuItem;
    ExportTiles1: TTntMenuItem;
    N34: TTntMenuItem;
    NewMap2: TTntMenuItem;
    NewMap3: TTntMenuItem;
    N35: TTntMenuItem;
    AddFrom3: TTntMenuItem;
    SaveTo2: TTntMenuItem;
    N36: TTntMenuItem;
    MapAddFromFileMenuItem: TTntMenuItem;
    MapSaveToFileMenuItem: TTntMenuItem;
    N37: TTntMenuItem;
    ImportTiles2: TTntMenuItem;
    ExportTiles2: TTntMenuItem;
    N38: TTntMenuItem;
    TileSetAddFromFilesMenuItem: TTntMenuItem;
    TileSetSaveToFileMenuItem: TTntMenuItem;
    N39: TTntMenuItem;
    MakeGradient1: TTntMenuItem;
    TileEditPopup: TTntPopupMenu;
    TntMenuItem8: TTntMenuItem;
    TntMenuItem19: TTntMenuItem;
    TntMenuItem29: TTntMenuItem;
    TntMenuItem31: TTntMenuItem;
    TntMenuItem56: TTntMenuItem;
    TntMenuItem57: TTntMenuItem;
    TntMenuItem58: TTntMenuItem;
    TntMenuItem68: TTntMenuItem;
    TntMenuItem69: TTntMenuItem;
    N40: TTntMenuItem;
    Selectintileset1: TTntMenuItem;
    SelectInTileSetAction: TTntAction;
    SelectFillAction: TTntAction;
    N41: TTntMenuItem;
    FillSelection1: TTntMenuItem;
    N42: TTntMenuItem;
    FillSelection2: TTntMenuItem;
    N43: TTntMenuItem;
    FillSelection3: TTntMenuItem;
    HistoryListBox: TListBox;
    procedure ExitActionExecute(Sender: TObject);
    procedure MainFormDestroy(Sender: TObject);
    procedure HistoryAdded(Sender: TNodeList; Node: TNode);
    procedure HistoryRemoveEvent(Sender: TNodeList; Node: TNode);    
    procedure MainFormCreate(Sender: TObject);
    procedure TileSetsStructureChanged(Sender: TObject);
    procedure MapsListBoxChange(Sender: TObject);
    procedure ColorsImportActionExecute(Sender: TObject);
    procedure ColorsImportExportActionUpdate(Sender: TObject);
    procedure OpenActionExecute(Sender: TObject);
    procedure SaveActionExecute(Sender: TObject);
    procedure SaveAsActionExecute(Sender: TObject);
    procedure NewActionExecute(Sender: TObject);
    procedure MainFormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure ColorTablesListBoxClick(Sender: TObject);
    procedure PropEditorEllipsisClick(Sender: TCustomPropertyEditor;
      Node: PVirtualNode; var Accepted: Boolean);
    procedure MapNewActionExecute(Sender: TObject);
    procedure EditModeActionExecute(Sender: TObject);
    procedure ColorTableDeleteActionUpdate(Sender: TObject);
    procedure ColorTableDeleteActionExecute(Sender: TObject);
    procedure MapFrameMapEditViewCustomMouseAction(
      Sender: TCustomTileMapView; var Done: Boolean);
    procedure MapFrameMapEditViewCustomMouseActionCheck(
      Sender: TCustomTileMapView; Shift: TShiftState; var Act: Boolean);
    procedure DrawToolActionExecute(Sender: TObject);
    procedure ToolsZoomActionExecute(Sender: TObject);
    procedure ToolsZoomActionUpdate(Sender: TObject);
    procedure LoadSaveProgress(Owner: TBaseSectionedList; Sender: TObject;
                        const Rec: TProgressRec);
    procedure MainFormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SimpleActionUpdate(Sender: TObject);
    procedure EditDeleteActionExecute(Sender: TObject);
    procedure SelectAllActionExecute(Sender: TObject);
    procedure SelectAllActionUpdate(Sender: TObject);
    procedure SelectDeselectActionExecute(Sender: TObject);
    procedure SelectionActionUpdate(Sender: TObject);
    procedure TilesFrameTileSetViewContentsChanged(Sender: TObject);
    procedure TilesFrameTileSetViewCustomMouseAction(
      Sender: TCustomTileMapView; var Done: Boolean);
    procedure TilesFrameTileSetViewCustomMouseActionCheck(
      Sender: TCustomTileMapView; Shift: TShiftState; var Act: Boolean);
    procedure RefreshColorTableProps;
    procedure MapFrameMapEditViewSelectionChanged(
      Sender: TCustomTileMapView);
    procedure TilesFrameTileSetViewSelectionChanged(
      Sender: TCustomTileMapView);
    procedure EditCopyActionExecute(Sender: TObject);
    procedure EditPasteActionExecute(Sender: TObject);
    procedure EditPasteActionUpdate(Sender: TObject);
    procedure SelectInverseActionExecute(Sender: TObject);
    procedure MapFrameMapEditViewMouseUp(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure MainFormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SelectDeselectActionUpdate(Sender: TObject);
    function ColorTablesListBoxDataFind(Control: TWinControl;
      FindString: String): Integer;
    procedure HistoryListBoxClick(Sender: TObject);
    procedure HistoryGetDestOwner(Sender: THistoryEvent;
                                const OwnerIndexList: TIndexArray;
                                Data: TNode;
                                var Result: TNodeList);
    procedure ColorTableNewActionExecute(Sender: TObject);
    procedure ColorTableUpActionExecute(Sender: TObject);
    procedure ColorTableUpActionUpdate(Sender: TObject);
    procedure ColorTableDownActionExecute(Sender: TObject);
    procedure ColorTableDownActionUpdate(Sender: TObject);
    procedure ColorTablesListBoxDblClick(Sender: TObject);
    procedure ColorsExportActionExecute(Sender: TObject);
    procedure ColTblAddFromFilesActionExecute(Sender: TObject);
    procedure MapFrameMapEditViewEnter(Sender: TObject);
    procedure MapFrameMapEditViewExit(Sender: TObject);
    procedure TilesFrameTileSetViewEnter(Sender: TObject);
    procedure TilesFrameTileSetViewExit(Sender: TObject);
    procedure PaletteFramePaletteViewEnter(Sender: TObject);
    procedure PaletteFramePaletteViewExit(Sender: TObject);
    procedure MapsFrameListBoxEnter(Sender: TObject);
    procedure MapsFrameListBoxExit(Sender: TObject);
    procedure TilesFrameTileSetsListBoxEnter(Sender: TObject);
    procedure TilesFrameTileSetsListBoxExit(Sender: TObject);
    procedure MapsFrameLayersListBoxContextPopup(Sender: TObject;
      MousePos: TPoint; var Handled: Boolean);
    procedure MapsFrameMapsListBoxContextPopup(Sender: TObject;
      MousePos: TPoint; var Handled: Boolean);
    procedure ColorsMakeGradientActionExecute(Sender: TObject);
    procedure ColorsMakeGradientActionUpdate(Sender: TObject);
    procedure ColorReplaceActionExecute(Sender: TObject);
    procedure ColorReplaceActionUpdate(Sender: TObject);
    procedure ListBoxContextPopup(Sender: TObject;
      MousePos: TPoint; var Handled: Boolean);
    procedure TilesFrameTileSetsListBoxContextPopup(Sender: TObject;
      MousePos: TPoint; var Handled: Boolean);
    procedure PaletteFramePaletteViewCustomMouseActionCheck(
      Sender: TCustomTileMapView; Shift: TShiftState; var Act: Boolean);
    procedure HistoryListBoxData(Control: TWinControl; Index: Integer;
      var Data: String);
    procedure ColorTablesListBoxData(Control: TWinControl; Index: Integer;
      var Data: String);
    procedure ListBoxDrawItem(Control: TWinControl;
      Index: Integer; Rect: TRect; State: TOwnerDrawState);

    procedure MapsFrameMapSelectionChanged(Sender: TObject);
    procedure MapsFrameMapOpen(Sender: TObject);
    procedure MapsFrameMapSelected(Sender: TObject);
    procedure MapsFrameMapComboSelected(Sender: TObject);
    procedure MapsFrameMapModified(Sender: TObject);
    procedure MapsFrameMapsModified(Sender: TObject);
    procedure MapsFrameMapDeselect(Sender: TObject);
    procedure MapsFrameMapNameModified(Sender: TObject);
    procedure MapsFrameRefreshMapContent(Sender: TMapsFrame; Layers: Boolean);
    procedure MapsFrameLayersModified(Sender: TObject);
    procedure MapsFrameLayerDblClick(Sender: TMapsFrame;
                                     Layer: TMapLayer;
                                     TileClicked: Boolean);
    procedure MapsFrameMapPropsModified(Sender: TObject);
    procedure MapsFramePageControlChange(Sender: TObject);

    procedure ColTblViewModeBarChange(Sender: TObject);
    procedure ColTblComboBoxChange(Sender: TObject);

    procedure TilesFrameTileSetOpen(Sender: TObject);
    procedure TilesFrameTileSetSelectionChanged(Sender: TObject);
    procedure TilesFrameTileSetModified(Sender: TObject);
    procedure TilesFrameTilesModified(Sender: TObject);
    procedure TilesFrameTileSetPropsModified(Sender: TObject);
    procedure TilesFrameTileSetsModified(Sender: TObject);
    procedure TilesFrameTileSetDeselect(Sender: TObject);
    procedure TilesFrameTileSetNameModified(Sender: TObject);
    procedure TilesFrameTileSetSelected(Sender: TObject);
    procedure TilesFrameTileSetComboSelected(Sender: TObject);
    procedure TilesFramePageControlChange(Sender: TObject);

    procedure SaveActDialogTypeChange(Sender: TObject);

    procedure ColorTableSaveToActionUpdate(Sender: TObject);
    procedure ColorTableSaveToActionExecute(Sender: TObject);
    procedure MapFrameMapEditViewContentsChanged(Sender: TObject);
    procedure PaletteFramePaletteViewContentsChanged(Sender: TObject);
    procedure SizeToolActionExecute(Sender: TObject);
    procedure MagicWandMouseEnter(Sender: TObject);
    procedure MagicWandMouseLeave(Sender: TObject);
    procedure MagicWandSysKeyDown(Sender: TCustomTileMapView;
      Key: Word; State: TShiftState);
    procedure MagicWandSysKeyUp(Sender: TCustomTileMapView;
      Key: Word; State: TShiftState);
    procedure PaletteFramePaletteViewMouseMove(Sender: TObject;
      Shift: TShiftState; X, Y: Integer);
    procedure TilesFrameTileSetViewMouseMove(Sender: TObject;
      Shift: TShiftState; X, Y: Integer);
    procedure MapFrameMapEditViewMouseMove(Sender: TObject;
      Shift: TShiftState; X, Y: Integer);
    procedure ToolDrawLineActionExecute(Sender: TObject);
    procedure MapFrameMapEditViewCustomMouseActionFinish(
      Sender: TCustomTileMapView);
    procedure TileScaleChange(Sender: TObject; var Value: Integer);
    procedure TileDrawColorChange(Sender: TTileEditFrame; Mode: Boolean);
    procedure TileEditViewCustomMouseActionFinish(
      Sender: TCustomTileMapView);
    procedure TileEditViewCustomMouseActionCheck(
      Sender: TCustomTileMapView; Shift: TShiftState; var Act: Boolean);
    procedure TileEditViewCustomMouseAction(Sender: TCustomTileMapView;
      var Done: Boolean);
    procedure TileEditViewMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure TilesTileEditActionExecute(Sender: TObject);
    procedure TilesTileEditActionUpdate(Sender: TObject);
    procedure TilesFrameTileSetViewContextPopup(Sender: TObject;
      MousePos: TPoint; var Handled: Boolean);
    procedure ColorTablesListBoxKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure PluginActionExecute(Sender: TObject);
    procedure SelectInTileSetActionUpdate(Sender: TObject);
    procedure SelectInTileSetActionExecute(Sender: TObject);
    procedure SelectFillActionUpdate(Sender: TObject);
    procedure SelectFillActionExecute(Sender: TObject);
    procedure HistoryListBoxDrawItem(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    procedure TileEditViewBeforeContentChange(
            Sender: TCustomTileMapView; ChangeType: TChangeType);
    procedure TilesFrameTileSetViewBeforeContentChange(
      Sender: TCustomTileMapView; ChangeType: TChangeType);
    procedure HistoryEventApply(Sender: TObject);
    procedure EditUndoActionUpdate(Sender: TObject);
    procedure EditRedoActionUpdate(Sender: TObject);
    procedure EditUndoRedoActionExecute(Sender: TObject);
    procedure TilesFrameBeforeUnhook(Sender: TObject);
    procedure TilesFrameAfterUnhook(Sender: TObject);
    procedure PaletteFramePaletteViewBeforeContentChange(
      Sender: TCustomTileMapView; ChangeType: TChangeType);
  private
//    FPostStates: set of (medFillCellPropsPosted, medFillTilePropsPosted);
    FExeDir: WideString;
    FPluginsPath: WideString;
    FPluginsIniPath: WideString;
    FSelectedPlugin: WideString;
    FProjectFileName: WideString;
    FProject: TMapEditorProject;
    FClickPos: TPoint;
    FBackup: Pointer;
    FLastTile: TTileItem;
    FLastFocus: TWinControl;
 //   FMaps: TMapList;
//    FVarTypesList: TTntStringList;
    FSaved: Boolean;
    FCellPropsFillPosted: Boolean;
  {  FEditMode: TEditMode;
    FTileSetMode: TSetMode;
    FColorTblMode: TSetMode;  }
    FDrawTool: TDrawTool;
    FDrawLine: Integer;
//    FOldDrawX: Integer;
  //  FOldDrawY: Integer;
    FProgressLabel: TTntLabel;
    FProgressBar: TProgressBar;
    FFocusedMapFrame: TMapFrame;
    FFocusedTilesFrame: TTilesFrame;
    FFocusedPaletteFrame: TPaletteFrame;
    FFocusedMapsFrame: TMapsFrame;
    FFocusedTileSetsFrame: TTileSetsFrame;
    FMainEditActivePage: Integer;
    FColorsCollection: array[0..15] of TColor;
//    FDottedBrush: HBRUSH;    
  //  FView: TCustomTileMapView;
 //   FPropListArray: array[0..PROP_LIST_CNT - 1] of TMedPropertyList;
    function NewMoveColorTablesEvent(Shift: Integer): THistoryEvent;
    function AddColorTableListEvent(const Text: WideString): THistoryEvent;
    procedure ColorTableBeforeUnhook;
    procedure ColorTableAfterUnhook; 
    procedure FrameStartDialog(Sender: TObject);
    procedure FrameEndDialog(Sender: TObject);    
    procedure ContinguousPaletteSelect(X, Y: Integer; Shift: TShiftState);
    procedure SelectColorTable(Index: Integer);
    procedure FillColorTablesCombo;
    procedure InitMapsFrameEvents(Frame: TMapsFrame);
    procedure InitTileSetsFrameEvents(Frame: TTileSetsFrame);
    procedure ColorTableColorsModified(Item: TColorTable);
    procedure ColorTableDeselect;
    procedure ColorTableSelected;
    procedure ColorTablesModified(Deleted: Boolean = False);
    procedure StartDialog;
    procedure EndDialog;
    function GetNamed: Boolean;
    procedure SetSaved(Value: Boolean);
    procedure SetProjFName(const Value: WideString);
    function DrawTileWithPencil(Control: TCustomTileMapView; DrawX, DrawY: Integer): Boolean;
    function DrawTileUseEraser(Control: TCustomTileMapView; DrawX, DrawY: Integer): Boolean;        
    function DrawWithPencil(Control: TCustomTileMapView; DrawX, DrawY: Integer): Boolean;
    function DrawMapLine(Control: TCustomTileMapView;
             DrawUnit: TDrawUnitFunction;
             x1, y1, x2, y2: Integer): Boolean;
    function DrawMapRect(Control: TCustomTileMapView;
             DrawUnit: TDrawUnitFunction;
             x1, y1, x2, y2: Integer): Boolean;
    function UseEraser(Control: TCustomTileMapView;
     BrushErase: Boolean; DrawX, DrawY: Integer): Boolean;
    function UseTileEraser(Control: TCustomTileMapView;
     DrawX, DrawY: Integer): Boolean;
    function UseBrushEraser(Control: TCustomTileMapView;
     DrawX, DrawY: Integer): Boolean;
    function DrawWithBrush(Control: TCustomTileMapView; DrawX, DrawY: Integer): Boolean;
    function DrawWithBrushEx(Control: TCustomTileMapView;
     ABrush: TMapBrush; DrawX, DrawY: Integer): Boolean;
//    procedure FillMapProps(Map: TMap);
//    procedure TilesFrameTileSetChange(Sender: TTilesFrame);
//    procedure TileValueChange(Sender: TPropertyListItem);
    procedure ColorTableValueChange(Sender: TPropertyListItem);
    procedure SelCellValueChange(Sender: TPropertyListItem);
//    procedure TileSetBaseValuesChange(Sender: TPropertyListItem);
//    procedure MapPropertiesValueChange(Sender: TPropertyListItem);
 //   procedure LayerPropertiesValueChange(Sender: TPropertyListItem);
 //   procedure TileSetImageFlagsChange(Sender: TPropertyListItem);
 //   procedure TileSetImageFlagsValueChange(Sender: TPropertyListItem);
//    procedure ShowTileProperties(TileSet: TTileSet;
 //    TileItem: TTileItem; const VFlags: Int64);
 (*   procedure WMFillLayerProps(var Message: TMessage); message WM_FILLLAYERPROPS;*)
    procedure WMFillCellProps(var Message: TMessage); message WM_FILLCELLPROPS;
    procedure WMTileEdit(var Message: TMessage); message WM_TILEEDIT;
  //  procedure WMFillTileProps(var Message: TMessage); message WM_FILLTILEPROPS;
    procedure WMRefreshCTblProps(var Message: TMessage); message WM_REFRESHCTBLPROPS;

    procedure SetEditMode(Value: TEditMode);
    function GetEditMode: TEditMode;
//    function GetSelectedMap: TMap;
   // function GetSelectedBrush: TMap;
    function CheckSaved: Boolean;
    procedure SetDrawTool(Value: TDrawTool);
    procedure RefreshMapContent(Map: TMap;
     ContentSet: TRefreshMapContentSet = [rmcView, rmcLayersList, rmcLayerProps]);
    procedure ColorContentModified;
    function GetColorTableIndex: Integer;
    procedure SetColorTableIndex(Value: Integer);
    procedure SetMainEditActivePage(Value: Integer);
//    procedure RefreshColorTable;
    procedure TileRemoved(Sender: TNodeList; Node: TNode);
  public
    BrushPattern: PBrushPattern;
    MainFrame: TFrame;
    MapFramesList: array of TMapsFrame;
    TileSetFramesList: array of TTileSetsFrame;
    property ExeDir: WideString read FExeDir;
    property PluginsPath: WideString read FPluginsPath;
    property PluginsIniPath: WideString read FPluginsIniPath;
    property MainEditActivePage: Integer read FMainEditActivePage write SetMainEditActivePage;
    property ColorTableIndex: Integer read GetColorTableIndex write SetColorTableIndex;
    property ProjectFileName: WideString read FProjectFileName write SetProjFName;
    property Saved: Boolean read FSaved write SetSaved;
    property Named: Boolean read GetNamed;
    property EditMode: TEditMode read GetEditMode write SetEditMode;
    procedure RefreshLists(AEditMode: TEditMode;
     PageSetup: TPageSetup = [psMainFrame, psTileSet, psBrush]);
//    procedure TilesBoxUpdate(Allways: Boolean = False);
    procedure ProgramTitleUpdate;
//    function SelTileIsEmpty: Boolean;
    procedure CreateProgressBar;
    procedure DestroyProgressBar;
    procedure TilePropsFrameRefreshGraphicContent(Sender: TObject);
    procedure TilePropsUpdated(Sender: TObject);
    procedure TileImageModified(Sender: TObject);
    procedure RefreshGraphicContent(TileSetRefresh: Boolean = True);
//    procedure MapChanged;
    procedure MapModified;
    procedure TileFormatModified;
    procedure TileFocusChanged(TileSet: TTileSet);
    procedure CopyColorTables(Cut: Boolean);

  //  property SelectedMap: TMap read GetSelectedMap;
 //   property SelectedBrush: TMap read GetSelectedBrush;
    property DrawTool: TDrawTool read FDrawTool write SetDrawTool;
//    procedure RefreshColorTable;
//    procedure UpdateMapName;
    procedure PasteColorTables;
//    procedure PasteLayers;
  end;


type
 TLoadSaveThread = class(TThread)
  private
    FMainForm: TMainForm;
    FPlugin: TPlugin;
    FData: TProcessData;
    FIniFile: TStreamIniFileW;
    FConfigIndex: Integer;
    FConfigSection: WideString;
    FMapList: TMapList;
    FThreadMode: TPluginDialogMode;
  protected
    procedure Execute; override;
    procedure LoadedSuccessfully;
  public
    constructor Create(Mode: TPluginDialogMode;
                       MainForm: TMainForm;
                       Plugin: TPlugin;
                       MapList: TMapList);
    destructor Destroy; override;
 end;


var
 MainForm: TMainForm;
 CF_COLORTABLES: Word;
 CF_TRANSP_MASK: Word;

implementation

uses
  MyUtils, me_ted5dlg, me_tilesloaddlg, ALZSS, BmpImg,
  me_importcolorsdlg, me_plugadapters;

{$R *.DFM}

const
 DrawToolNames: array[TDrawTool] of WideString =
 ('Single select',
  'Multi select',
  'Magic wand',
  'Picker',
  'Pencil',
  'Brush',
  'Eraser',
  'Flood fill');


 MediumBrush: array[0..(2 * 2) - 1] of Byte =
 (1, 1,
  1, 1);
 BigBrush: array[0..(4 * 4) - 1] of Byte =
 (0, 1, 1, 0,
  1, 1, 1, 1,
  1, 1, 1, 1,
  0, 1, 1, 0);
 LargeBrush: array[0..(7 * 7) - 1] of Byte =
 (0, 0, 1, 1, 1, 0, 0,
  0, 1, 1, 1, 1, 1, 0,
  1, 1, 1, 1, 1, 1, 1,
  1, 1, 1, 1, 1, 1, 1,
  1, 1, 1, 1, 1, 1, 1,
  0, 1, 1, 1, 1, 1, 0,
  0, 0, 1, 1, 1, 0, 0);
 Patterns: array[0..3] of TBrushPattern =
 ((W: 1; H: 1; X: 0; Y: 0; P: @MediumBrush),
  (W: 2; H: 2; X: 1; Y: 1; P: @MediumBrush),
  (W: 4; H: 4; X: 2; Y: 2; P: @BigBrush),
  (W: 7; H: 7; X: 3; Y: 3; P: @LargeBrush));

type
 TClipboardFormat = (cfPalette, cfMapData, cfBitmap, cfLayers,
                     cfMaps, cfTileSets, cfColorTables, cfTransparencyMask);
 TClipboardFormats = set of TClipboardFormat;

function GetClipboardFormats: TClipboardFormats;
var
 Format: Cardinal;
begin
 Result := [];
 if OpenClipboard(0) then
 try
  Format := EnumClipboardFormats(0);
  while Format <> 0 do
  begin
   case Format of
    CF_DIB,
    CF_BITMAP: Include(Result, cfBitmap);
    else
    begin
     if Format = CF_MAPDATA then
      Include(Result, cfMapData) else
     if Format = CF_PALETTEDATA then
      Include(Result, cfPalette) else
     if Format = CF_LAYERSCONTAINER then
      Include(Result, cfLayers) else
     if Format = CF_MAPSCONTAINER then
      Include(Result, cfMaps);
     if Format = CF_TILESETS then
      Include(Result, cfTileSets);
     if Format = CF_COLORTABLES then
      Include(Result, cfColorTables) else
     if Format = CF_TRANSP_MASK then
      Include(Result, cfTransparencyMask);
    end;
   end;

   Format := EnumClipboardFormats(Format);
  end;
 finally
  CloseClipboard;
 end;
end;

{ TMainForm }

procedure TMainForm.ExitActionExecute(Sender: TObject);
begin
 Close;
end;
         {
procedure LoadTileSet(TileSet: TTileSet;
              HeadPtr: PInteger; Cnt: Integer; DictBuf, TileBuf: Pointer);
var
 I, J: Integer;
 TileItem: TTileItem;
begin
 with TileSet do
 begin
  for I := 0 to Cnt - 1 do
  begin
   J := HeadPtr^; Inc(HeadPtr);
   if J < 0 then Continue;
   TileItem := AddTile;
   TileItem.TileIndex := I;
   CAL_HuffExpand(DictBuf^, Pointer(Integer(TileBuf) + J)^,
                  TileItem.TileData^, TileSize);
  end;
  LastIndex := Cnt - 1;
 end;
end;

procedure LoadMaskedTileSet(TileSet: TTileSet;
              HeadPtr: PInteger; Cnt: Integer; DictBuf, TileBuf: Pointer;
              StartIdx: Integer = 0);
var
 I, J: Integer;
 TileItem: TTileItem;
 Mask: array [0..31] of Byte;
begin
 with TileSet do
 begin
  for I := 0 to Cnt - 1 do
  begin
   J := HeadPtr^; Inc(HeadPtr);
   if J < 0 then Continue;
   TileItem := AddTile;
   TileItem.TileIndex := StartIdx + I;
   with TileItem do
   begin
    CAL_HuffExpand(DictBuf^, Pointer(Integer(TileBuf) + J)^,
                   TileData^, TileSize);
    Move(TileData^, Mask, 32);
    Move(Pointer(Integer(TileData) + 32)^, TileData^, TileSize - 32);
    Move(Mask, Pointer(Integer(TileData) + TileSize - 32)^, 32);
   end;
  end;
  FillChar(Pointer(Integer(EmptyTile.TileData) + TileSize - 32)^, 32, 255);
  LastIndex := (StartIdx + Cnt) - 1;
 end;
end;
            }
procedure TMainForm.EndDialog;
{var
 I: Integer;  }
begin
 ToolsPanel.Enabled := True;
 CentralPanel.Enabled := True;
 LeftPanel.Enabled := True;
 RightPanel.Enabled := True;
 if FLastFocus <> nil then
  Windows.SetFocus(FLastFocus.Handle);

 {PaletteFrame.PaletteView.Enabled := True;
 for I := 0 to Length(MapFramesList) - 1 do
  MapFramesList[I].MapFrame.MapEditView.Enabled := True;
 for I := 0 to Length(TileSetFramesList) - 1 do
  TileSetFramesList[I].TilesFrame.TileSetView.Enabled := True;      }
end;

procedure TMainForm.StartDialog;
{var
 I: Integer; }
begin
 FLastFocus := FindControl(GetFocus);
 ToolsPanel.Enabled := False;
 CentralPanel.Enabled := False;
 LeftPanel.Enabled := False;
 RightPanel.Enabled := False;
{ PaletteFrame.PaletteView.Enabled := False;
 for I := 0 to Length(MapFramesList) - 1 do
  MapFramesList[I].MapFrame.MapEditView.Enabled := False;
 for I := 0 to Length(TileSetFramesList) - 1 do
  TileSetFramesList[I].TilesFrame.TileSetView.Enabled := False;   }
end;
             (*
procedure TMainForm.TED5ImportActionExecute(Sender: TObject);
var
 MapsData: Pointer; SZ: Integer;
 DictData: Pointer;
 HeadData: Pointer;
 Stream: TStream;
 I, J, PSZ: Integer;
 MapHeader: TMapStruct;
 OutRec: TTed5FileNamesRec;
 Buf: Pointer;
 PB: PByte;
 PS: PShortInt absolute PB; PW: PWord;
 Done: Boolean;
begin
 Done := False;
 StartDialog;
 try
  if CheckSaved and ImportFromTED5_Dialog(OutRec) then
  begin
   try
    FProjectFileName := '';
    Saved := False;
    FProject.Clear;
    DictData := nil;
    HeadData := nil;
    try
     Stream := TTntFileStream.Create(OutRec.GFX_DictFileName,
               fmOpenRead or fmShareDenyWrite);
     try
      if WideUpperCase(WideExtractFileExt(OutRec.GFX_DictFileName)) = '.OBJ' then
       LoadObjectData(Stream, 'EGAdict', DictData) else
      begin
       SZ := Stream.Size;
       GetMem(DictData, SZ);
       Stream.ReadBuffer(DictData^, SZ);
      end;
     finally
      Stream.Free;
     end;
     Stream := TTntFileStream.Create(OutRec.GfX_HeadFileName,
               fmOpenRead or fmShareDenyWrite);
     try
      if WideUpperCase(WideExtractFileExt(OutRec.GFX_HeadFileName)) = '.OBJ' then
       LoadObjectData(Stream, 'EGAhead', HeadData) else
      begin
       SZ := Stream.Size;
       GetMem(HeadData, SZ);
       Stream.ReadBuffer(HeadData^, SZ);
      end;
     finally
      Stream.Free;
     end;
     with TTntFileStream.Create(OutRec.GFX_DataFileName,
          fmOpenRead or fmShareDenyWrite) do
     try
      SZ := Size;
      GetMem(Buf, SZ);
      try
       ReadBuffer(Buf^, SZ);
       with FProject.FMaps do
       begin
        with TileSetList.AddChild(LayerNames[liBackground]) as TTileSet do
        begin
         SwitchFormat([Planar(4, 1, pfMatrix, False)], 16, 16);
         TransparentColor := 16;
         AttrCount := 2;
         InitIntegerAttr(AttrDefs[0]^, 'FRAME_DELAY', 0, 255);
         InitIntegerAttr(AttrDefs[1]^, 'NEXT_FRAME', -128, 127);
        end;
        with TileSetList.AddChild(LayerNames[liForeground]) as TTileSet do
        begin
         SwitchFormat([Planar(5, 1, pfMatrix, False)], 16, 16);
         TransparentColor := 16;
         AttrCount := 7;
         InitIntegerAttr(AttrDefs[0]^, 'NORTH_WALL', 0, 255);
         InitIntegerAttr(AttrDefs[1]^, 'EAST_WALL', 0, 255);
         InitIntegerAttr(AttrDefs[2]^, 'SOUTH_WALL', 0, 255);
         InitIntegerAttr(AttrDefs[3]^, 'WEST_WALL', 0, 255);
         InitIntegerAttr(AttrDefs[4]^, 'NEXT_FRAME', -128, 127);
         InitIntegerAttr(AttrDefs[5]^, 'BEHIND_SPR', 0, 255);
         InitIntegerAttr(AttrDefs[6]^, 'FRAME_DELAY', 0, 255);
        end;
        with TileSetList.AddChild(LayerNames[liObjects]) as TTileSet do
        begin
         SwitchFormat([Planar(5, 1, pfMatrix, False)], 16, 16);
         TransparentColor := 16;
        end;

        LoadTileSet(TileSetList[0],
                    Pointer(Integer(HeadData) + OutRec.TilesIndex shl 2),
                    OutRec.TilesCount, DictData, Buf);
        LoadMaskedTileSet(TileSetList[2],
                    Pointer(Integer(HeadData) + OutRec.MaskedIndex shl 2),
                    OutRec.IconsCount, DictData, Buf);
        LoadMaskedTileSet(TileSetList[1],
                    Pointer(Integer(HeadData) +
                    (OutRec.MaskedIndex + OutRec.IconsCount) shl 2),
                    OutRec.MaskedCount, DictData, Buf, OutRec.IconsCount);
       end;
      finally
       FreeMem(Buf);
      end;
     finally
      Free;
     end;
      ////////////////
     Stream := TTntFileStream.Create(OutRec.MapsDictFileName,
               fmOpenRead or fmShareDenyWrite);
     try
      if WideUpperCase(WideExtractFileExt(OutRec.MapsDictFileName)) = '.OBJ' then
       LoadObjectData(Stream, 'mapdict', DictData) else
      begin
       SZ := Stream.Size;
       ReallocMem(DictData, SZ);
       Stream.ReadBuffer(DictData^, SZ);
      end;
     finally
      Stream.Free;
     end;
     Stream := TTntFileStream.Create(OutRec.MapsHeadFileName,
               fmOpenRead or fmShareDenyWrite);
     try
      if WideUpperCase(WideExtractFileExt(OutRec.MapsHeadFileName)) = '.OBJ' then
       LoadObjectData(Stream, 'maphead', HeadData) else
      begin
       SZ := Stream.Size;
       ReallocMem(HeadData, SZ);
       Stream.ReadBuffer(HeadData^, SZ);
      end;
     finally
      Stream.Free;
     end;
     PB := Pointer(Integer(HeadData) + SizeOf(TMapFileStruct));
     with FProject.FMaps.TileSetList[0] do
     begin
      for I := 0 to TilesCount - 1 do
      begin // Fill speed values
       Indexed[I].Attributes[0] := PB^;
       Inc(PB);
      end;
      for I := 0 to TilesCount - 1 do
      begin // Fill animation values
       Indexed[I].Attributes[1] := PS^;
       Inc(PS);
      end;
     end;
     with FProject.FMaps.TileSetList[1] do
     begin
      for I := 0 to TilesCount - 1 do
      begin // Fill north wall values
       Indexed[I].Attributes[0] := PB^;
       Inc(PB);
      end;
      for I := 0 to TilesCount - 1 do
      begin // Fill east wall values
       Indexed[I].Attributes[1] := PB^;
       Inc(PB);
      end;
      for I := 0 to TilesCount - 1 do
      begin // Fill south wall values
       Indexed[I].Attributes[2] := PB^;
       Inc(PB);
      end;
      for I := 0 to TilesCount - 1 do
      begin // Fill west wall values
       Indexed[I].Attributes[3] := PB^;
       Inc(PB);
      end;
      for I := 0 to TilesCount - 1 do
      begin // Fill animation values
       Indexed[I].Attributes[4] := PS^;
       Inc(PS);
      end;
      for I := 0 to TilesCount - 1 do
      begin // Fill in-tile values
       Indexed[I].Attributes[5] := PB^;
       Inc(PB);
      end;
      for I := 0 to TilesCount - 1 do
      begin // Fill speed values
       Indexed[I].Attributes[6] := PB^;
       Inc(PB);
      end;
     end;
     with FProject.FMaps.ColorTableList.AddColorTable('EGA standard', RGB_ColorFormat, 17) do
     begin
      Move(Palette, ColorData^, SizeOf(Palette));
      ColorFormat.SetColor(ColorData, 16, $FF00FF);
      TransparentIndex := 16;
     end;
     with TTntFileStream.Create(OutRec.MapsDataFileName,
          fmOpenRead or fmShareDenyWrite) do
     try
      with TMapFileStruct(HeadData^) do
       for I := 0 to 99 do
       begin
        SZ := mfsHeaderSize[I];
        if SZ > 0 then
        begin
         GetMem(MapsData, SZ);
         try
          Seek(mfsHeaderOffsets[I], soFromBeginning);
          ReadBuffer(MapsData^, SZ);
          CAL_HuffExpand(DictData^, MapsData^, MapHeader, SizeOf(MapHeader));
         finally
          FreeMem(MapsData);
         end;
        end else Continue;
        with FProject.FMaps.AddMap(PAnsiChar(Addr(MapHeader.msName)),
                          MapHeader.msWidth, MapHeader.msHeight, 16, 16) do
        begin
         ColorTableIndex := 0;
         PSZ := Width * 2 * Height;
         for J := 0 to 2 do
         with AddChild(LayerNames[TLayerIndex(J)]) as TMapLayer do
         begin
          CellSize := 2;
          IndexMask := $FFFF;
          IndexShift := 0;
          TileSetIndex := J;
          SZ := MapHeader.msPlaneLength[J];
          GetMem(MapsData, SZ);
          try
           Seek(MapHeader.msPlaneStart[J], soFromBeginning);
           ReadBuffer(MapsData^, SZ);
           PW := MapsData;
           SZ := PW^;
           Inc(PW);
           GetMem(Buf, SZ);
           try
            CAL_HuffExpand(DictData^, PW^, Buf^, SZ);
            CA_RLEWexpand(Pointer(Integer(Buf) + 2)^, LayerData^, PSZ, mfsRLEWtag);
           finally
            FreeMem(Buf);
           end;
          finally
           FreeMem(MapsData);
          end;
         end;
        end;
       end;
     finally
      Free; // map data file stream
     end;
    finally
     FreeMem(HeadData);
     FreeMem(DictData);
    end;
   except
    on E: Exception do
     WideMessageDlg(E.Message, mtError, [mbOk], 0, mbOk);
   end;
   FProject.Update;
   Done := True;
  end;
 finally
  EndDialog;
 end;
 if Done then
  RefreshLists(emMap);
end;
                    *)
procedure TMainForm.MainFormDestroy(Sender: TObject);
//var
// I: Integer;
//var
// P: Pointer;
begin
// for I := 0 to PROP_LIST_CNT - 1 do
//  FPropListArray[I].Free;
// P := FDrawTileBmp.ImageData;
// FreeMem(P);
// FDrawTileBmp.Free;
 HistoryListBox.Count := 0;
 FProject.History.OnRemove := nil;
 FProject.Free;
// DeleteObject(FDottedBrush);
// FVarTypesList.Free;
end;

procedure TMainForm.MainFormCreate(Sender: TObject);
//const
 //Bits: array[0..1] of Word = ($55, $AA);
var
 Buffer: array[0..1023] of WideChar;
 //PatternBitmap: HBITMAP;
begin
{ PatternBitmap := CreateBitmap(8, 2, 1, 1, @Bits);
 try
  FDottedBrush := CreatePatternBrush(PatternBitmap);
 finally
  DeleteObject(PatternBitmap);
 end;  }

 SetString(FExeDir, PWideChar(@Buffer),
  GetModuleFileNameW(0, @Buffer, SizeOf(Buffer) shr 1));
 FExeDir := WideExtractFilePath(FExeDir);

 FPluginsPath := FExeDir + 'Plugins\';
 FPluginsIniPath := FPluginsPath + 'Ini\';

 BrushPattern := @Patterns;
 FillChar(FColorsCollection, SizeOf(FColorsCollection), 255);
 InitMapsFrameEvents(MapsFrm);
 InitMapsFrameEvents(BrushesFrame);
 InitTileSetsFrameEvents(TilesFrm);
 SetLength(MapFramesList, 2);
 MapFramesList[0] := MapsFrm;
 MapFramesList[1] := BrushesFrame;
 SetLength(TileSetFramesList, 1);
 TileSetFramesList[0] := TilesFrm;
 FProject := TMapEditorProject.Create;
 FProject.FMaps.TileSetList.OnChange := TileSetsStructureChanged;
 FProject.History.OnPositionChanged := HistoryEventApply;
 FProject.History.OnAdded := HistoryAdded;
 FProject.History.OnRemove := HistoryRemoveEvent;
 FProject.History.OnGetDestOwner := HistoryGetDestOwner;
 MainEditActivePage := 1;

 //FDrawTileBmp := TBitmapContainer.Create;
 //FDrawTileBmp.ImageFlags := (8 - 1) or IMG_Y_FLIP;
// for I := 0 to PROP_LIST_CNT - 1 do
//  FPropListArray[I] := TMedPropertyList.Create;
// FOldDrawX := Low(Integer);
// FOldDrawY := Low(Integer);
 ColorTablesListBox.DoubleBuffered := True;
  LeftPanel.DoubleBuffered := True;
 RightPanel.DoubleBuffered := True;
 //HistoryPanel.DoubleBuffered := True;
 CentralPanel.DoubleBuffered := True;
 PalPropEditor.PropertyList.OnValueChange := ColorTableValueChange;
 SelCellPropEditor.PropertyList.OnValueChange := SelCellValueChange;
 TilePropsFrm.OnRefreshGraphicContent := TilePropsFrameRefreshGraphicContent;
 TilePropsFrm.OnDataUpdated := TilePropsUpdated;
 TilePropsFrm.PaletteFrame := PaletteFrame;
 TilePropsFrm.Project := FProject;

// SelTilePropEditor.PropertyList.OnValueChange := TileValueChange;
// SelTilePropEditor.NodeDataSize := SizeOf(TPropertyData);
// TileSetPropEditor.NodeDataSize := SizeOf(TPropertyData);
// SelCellPropEditor.NodeDataSize := SizeOf(TPropertyData);
// LayerPropEditor.NodeDataSize := SizeOf(TPropertyData);
// MapPropEditor.NodeDataSize := SizeOf(TPropertyData);
// TileSetPropEditor.PropertyList.OnValueChange := TileSetBaseValuesChange;
// MapPropEditor.PropertyList.OnValueChange := MapPropertiesValueChange;
// LayerPropEditor.PropertyList.OnValueChange := LayerPropertiesValueChange;
// TilesFrame.OnTileSetChange := TilesFrameTileSetChange;
// MapFrame.OnGetLayerActive := MapFrameGetLayerActive;
{ FVarTypesList := TTntStringList.Create;
 FVarTypesList.Add('String');
 FVarTypesList.Add('Boolean');
 FVarTypesList.Add('64-bit signed');
 FVarTypesList.Add('32-bit signed');
 FVarTypesList.Add('32-bit unsigned');
 FVarTypesList.Add('16-bit signed');
 FVarTypesList.Add('16-bit unsigned');
 FVarTypesList.Add('8-bit signed');
 FVarTypesList.Add('8-bit unsigned');
 FVarTypesList.Add('64-bit float');
 FVarTypesList.Add('32-bit float');}
 FProject.History.AddEvent(nil, SInitialEvent);
 Saved := True;
{
 FView := TCustomTileMapView.Create(Self);
 FView.Parent := BottomPanel;
 FView.Align := alLeft;
 FView.Width := 256;
 FView.GridStyle := psSolid;
 FView.GridColor := clBlack;
 FView.GridWidth := 1;
 FView.ShowGrid := True;
 FView.SetMapSize(22, 22);
 FView.SelectStyle := psInsideFrame;
 FView.SelectMode := pmNot;
 FView.SelectWidth := 2;
 FView.SelectColor := $00FF00;
 FView.SelectionMode := selMulti;  }
// TilesFrame.OnScaleChange := TilesFrameScaleChange;
 WindowState := wsMaximized;
 RefreshLists(emMap);
end;

procedure TMainForm.RefreshLists(AEditMode: TEditMode; PageSetup: TPageSetup);
 procedure FillColorTables;
 var
  I: Integer;
 begin
  PaletteFrame.PaletteView.Deselect(False);
  with FProject.FMaps.ColorTableList do
  begin
   ColorTablesListBox.Count := Count;
   if Count > 0 then
   begin
    I := ColorTableIndex;
    if I < 0 then
    begin
     I := 0;
     ColorTableIndex := 0;
    end;
    FillColorTablesCombo;
    ColorTablesListBox.ItemIndex := I;
    ColorTablesListBox.Selected[I] := True;
    ColorTableSelected;
    Exit;
   end;
   ColTblComboBox.Clear;
   ColorTableSelected;
  end;
 end;
var
 ts: TTileSet;
begin
{ Perform(WM_SETREDRAW, 0, 0);
 try  }
  TilesFrm.TilesFrame.TileSetView.Deselect(False);
  MapsFrm.MapFrame.MapEditView.Deselect(False);
  BrushesFrame.MapFrame.MapEditView.Deselect(False);

  if MainFrame is TMapsFrame then
  begin
   with TMapsFrame(MainFrame) do
   begin
    MapFrame.MapEditView.Deselect(False);
    SetProject(nil, BrushMode, Tag);
   end;
  end else
  if MainFrame is TTileSetsFrame then
  begin
   with TTileSetsFrame(MainFrame) do
   begin
    TilesFrame.TileSetView.Deselect(False);
    SetProject(nil, Tag);
   end;
  end else
  if MainFrame is TTileEditFrame then
  begin
   with TTileEditFrame(MainFrame) do
   begin
    TileEditView.Deselect(False);
    Tile := nil;
   end;
  end;

  TilesFrm.SetProject(nil, 1);
  MapsFrm.SetProject(nil, False, 1);
  BrushesFrame.SetProject(nil, True, 1);

  TilesFrm.SetProject(FProject, 1);
  if AEditMode <> emTileSet then
   FillColorTables;

  MapsFrm.PageControl.ActivePageIndex := Ord(psMap in PageSetup);
  BrushesFrame.PageControl.ActivePageIndex := Ord(psBrush in PageSetup);
  TilesFrm.PageControl.ActivePageIndex := Ord(psTileSet in PageSetup);
  FProject.FInfo.PageSetup := PageSetup;
  MainEditActivePage := Ord(psMainFrame in PageSetup);

  with FProject do
  begin
   ts := FMaps.TileSetList.Sets[FInfo.SelectedTileTileSet];
   if ts <> nil then
    FLastTile := ts.Indexed[FInfo.SelectedTileIndex] else
    FLastTile := nil;
  end;
  SetEditMode(AEditMode);

  if AEditMode = emTileSet then
   FillColorTables;

  MapsFrm.SetProject(FProject, False, 1);
  BrushesFrame.SetProject(FProject, True, 1);
{ finally
  Perform(WM_SETREDRAW, 1, 0);
  Rect := ClientRect;
  RedrawWindow(Handle, @Rect, NULL, RDW_ALLCHILDREN or RDW_INVALIDATE); 
 end;    }

{ MapsListBox.Clear;
 for I := 0 to Maps.Count - 1 do
  with Maps[I] do
   MapsListBox.Items.Add(WideFormat('%d. %s', [I + 1, Name]));
 if Maps.Count > 0 then
  MapsListBox.ItemIndex := FProject.FInfo.SelectedMapIndex;

 with Maps do
 begin
  BrushesListBox.Clear;
  for I := 0 to BrushList.Count - 1 do
   with BrushList[I] do
    BrushesListBox.Items.Add(WideFormat('%d. %s', [I + 1, Name]));

  if BrushList.Count > 0 then
   BrushesListBox.ItemIndex := FProject.FInfo.SelectedBrushIndex;
  BrushFrame.SetMap(SelectedBrush, FProject.FInfo.BrushFrameScale, True);

  TileSetsListBox.Clear;
  for I := 0 to TileSetList.Count - 1 do
   with TileSetList[I] do
    TileSetsListBox.Items.Add(WideFormat('%d. %s', [I + 1, Name]));

  if TileSetList.Count > 0 then
   TileSetsListBox.ItemIndex := FProject.FInfo.SelectedTileSetIndex;

  ColorTableListBox.Clear;
  for I := 0 to ColorTableList.Count - 1 do
   with ColorTableList[I] do
    ColorTableListBox.Items.Add(WideFormat('%d. %s', [I + 1, Name]));

   if ColorTableList.Count > 0 then
    ColorTableListBox.ItemIndex := ColorTableIndex;

  MapsListBoxChange(nil);
 end;}
end;

procedure TMainForm.MapsListBoxChange(Sender: TObject);
{var
 Map: TMap;}
begin
(*Map := SelectedMap;
 if Map <> nil then
  FProject.FInfo.SelectedMapIndex := Map.Index else
  FProject.FInfo.SelectedMapIndex := 0;
 SetEditMode(emMap);
(* Map := Maps[MapsListBox.ItemIndex];
 if Map <> nil then
 begin
  LayersListBox.Count := Map.Count;
  {
  for I := Map.Count - 1 downto 0 do
  with Map.Layers[I] do
   LayersListBox.Checked[LayersListBox.Items.Add(LayerName)] := Visible;}
  if Map.Count > 0 then
  begin
   LayersListBox.ItemIndex := 0;
   TileSetsListBox.ItemIndex := Map.Layers[Map.Count - 1].TileSet.Index;
  end;
 end else
  LayersListBox.Count := 0;
 MapEditModeAction.Checked := True;
 EditModeActionExecute(MapEditModeAction);
 MapFrame.SetMap(Map);
 if Map <> nil then
  ColorTableListBox.ItemIndex := Map.ColorTableIndex else
  ColorTableListBox.ItemIndex := -1;
 ColorTableListBoxChange(nil);
 TilesBoxUpdate;  *)
end;
                  {
procedure TMainForm.LayersListBoxClickCheck(Sender: TObject);
var
 Map: TMap;
 I: Integer;
begin
 Map := Maps[MapsListBox.ItemIndex];
 if Map <> nil then
 begin
  I := LayersListBox.ItemIndex;
  Map.Layers[I].Visible := LayersListBox.Checked[I];
  MapFrame.Redraw;
 end;
end;    }
    {
procedure TMainForm.TilesBoxUpdate(Allways: Boolean);
var
 Map: TMap;
 I: Integer;
 TileSet: TTileSet;
 Ct: TColorTable;
begin
 TileSet := nil;
 Ct := Maps.ColorTableList[ColorTableIndex]; 
 if TileSetMode = smActive then
 begin
  Map := TObject(LayersListBox.Tag) as TMap;
  if Map <> nil then
  begin
   I := LayersListBox.ItemIndex;
   if I >= 0 then
   begin
    if Map.Count > 0 then
     TileSet := Map.Layers[I].TileSet else
     TileSet := nil;
    if TileSet <> nil then
    begin
     TileSetsListBox.ItemIndex := TileSet.Index;
     FProject.FInfo.SelectedTileSetIndex := TileSet.Index;
    end;
   end;
   Ct := Map.ColorTable;
  end;
 end;
 if TileSet = nil then
  TileSet := Maps.TileSetList[FProject.FInfo.SelectedTileSetIndex];

 if TileSet <> nil then
 begin
  with FProject.FTileSetInfo[TileSet.Index] do
    TilesFrame.DoSetTileSet(TileSet, Allways,
       Ct, Width, SelectedTile, OffsetX, OffsetY,
          FProject.FInfo.TilesFrameScale);
 end else
  TilesFrame.TileSet := nil;
end;      }

const
 LoadingErrorMsg: PWideChar = 'Error loading file.';

procedure TMainForm.ColorsImportActionExecute(Sender: TObject);

 function AssignColors(Source, Dest: TColorTable): Boolean;
 var
  I, SI, X, Y: Integer;
  Event: THistoryEvent;
 begin
  Result := (Source <> nil) and (Source.ColorsCount > 0);
  if Result then
  begin
   if PaletteFrame.PaletteView.MultiSelected then
    PaletteFrame.PaletteView.PasteBufferApply;

   Event := FProject.History.AddEvent(Dest,
    WideFormat('Color table ''%s'': Import colors from file', [Dest.Name]));
   if PaletteFrame.PaletteView.MultiSelected then
   begin
    with PaletteFrame.PaletteView do
    begin
     I := 0;
     SI := 0;
     for Y := 0 to MapHeight - 1 do
      for X := 0 to MapWidth - 1 do
      begin
       if Selected[X, Y] then
       begin
        Dest.BGRA[SI] := Source.BGRA[I];
        Inc(I);
       end;
       Inc(SI);
      end;
    end;
   end else
   with Source do
   begin
    Dest.ColorsCount := ColorsCount;
    ColorFormat.ConvertTo(Dest.ColorFormat, ColorData, Dest.ColorData,
                          ColorsCount);
   end;
   Event.RedoData := Dest;   
  end;
 end;

var
 ColorTable, Temp: TColorTable;
 Stream: TStream;
 S, Msg, Ext: WideString;
 I: Integer;
 TempColTbls: TColorTableList;
 Dlg: TTntForm;
 Combo: TTntComboBox;
 OKBtn, CancelBtn: TTntButton;
 Imported, FileLoaded: Boolean;
begin
 ColorTable := PaletteFrame.ColorTable;
 if (FProgressBar = nil) and (ColorTable <> nil) then
 begin
  with OpenActDialog do
   Options := Options - [ofAllowMultiSelect];
  StartDialog;
  if OpenActDialog.Execute then
  begin
   Msg := '';
   S := OpenActDialog.FileName;
   Imported := False;
   FileLoaded := False;
   try
    Stream := TTntFileStream.Create(S, fmOpenRead);
    try
     Ext := WideUpperCase(WideExtractFileExt(S));
     if Ext = '.CCT' then
     begin
      TempColTbls := TColorTableList.Create;
      try
       TempColTbls.LoadFromStream(Stream);
       FileLoaded := True;
       if TempColTbls.Count > 1 then
       begin
        Dlg := TTntForm.CreateNew(Application);
        try
         Dlg.BorderStyle := bsDialog;
         Dlg.ClientWidth := 168;
         Dlg.ClientHeight := 64;
         Dlg.Caption := 'Select Color Table';
         Dlg.Position := poDesktopCenter;
         
         Combo := TTntComboBox.Create(Dlg);
         Combo.Style := csDropDownList;
         Combo.Width := 153;
         Combo.Height := 21;
         Combo.Left := 8;
         Combo.Top := 8;
         Combo.Parent := Dlg;         

         with TempColTbls do
          for I := 0 to Count - 1 do
           with Nodes[I] as TColorTable do
            Combo.Items.Add(Name);

         Combo.ItemIndex := 0;


         OkBtn := TTntButton.Create(Dlg);
         OkBtn.Default := True;
         OkBtn.ModalResult := mrOk;
         OkBtn.Width := 75;
         OkBtn.Height := 25;
         OkBtn.Left := 7;
         OkBtn.Top := 36;
         OkBtn.Parent := Dlg;
         OkBtn.Caption := 'Ok';

         CancelBtn := TTntButton.Create(Dlg);
         CancelBtn.Cancel := True;
         CancelBtn.ModalResult := mrCancel;
         CancelBtn.Width := 75;
         CancelBtn.Height := 25;
         CancelBtn.Left := 87;
         CancelBtn.Top := 36;
         CancelBtn.Parent := Dlg;
         CancelBtn.Caption := 'Cancel';

         if Dlg.ShowModal = mrOk then
          Imported := AssignColors(TempColTbls.Items[Combo.ItemIndex], ColorTable);
        finally
         Dlg.Free;
        end;
       end else
       with TempColTbls do
        Imported := AssignColors(RootNode as TColorTable, ColorTable);
      finally
       TempColTbls.Free;
      end;
     end else
     if Ext = '.ACT' then
     begin
      Temp := TColorTable.Create;
      try
       Temp.LoadActFromStream(Stream);
       FileLoaded := True;
       Imported := AssignColors(Temp, ColorTable);
      finally
       Temp.Free;
      end;
     end;
    finally
     Stream.Free;
    end;
    if not FileLoaded then
    begin
     Temp := ImportColorsDialog(S);
     if Temp <> nil then
     try
      Imported := AssignColors(Temp, ColorTable);
     finally
      Temp.Free;
     end;
    end;
    if Imported then
    begin
     PaletteFrame.ColorTableModified;
     ColorTableSelected;
     ColorTableColorsModified(ColorTable);

     (FProject.History.LastNode as THistoryEvent).SetProjectInfo;
    end;
   except
    on EWideFOpenError do Msg := LoadingErrorMsg;
    on EReadError do      Msg := LoadingErrorMsg;
    else                  Msg := 'File format error';
   end;
   if Msg <> '' then
    WideMessageDlg(Msg, mtError, [mbOk], 0);
  end;
  EndDialog;
 end;
end;

procedure TMainForm.ColorsImportExportActionUpdate(Sender: TObject);
begin
 (Sender as TAction).Enabled := (FProgressBar = nil) and
                                (PaletteFrame.ColorTable <> nil);
end;

procedure TMainForm.OpenActionExecute(Sender: TObject);
var
 Msg: WideString;
 Sign: LongWord;
 Done: Boolean;
begin
 Done := False;
 StartDialog;
 try
  if CheckSaved and OpenProjectDialog.Execute then
  begin
   CreateProgressBar;
   try
    FProjectFileName := OpenProjectDialog.FileName;
    with TTntFileStream.Create(FProjectFileName, fmOpenRead or fmShareDenyWrite) do
    try
     ReadBuffer(Sign, 4);
    finally
     Free;
    end;
    if Sign = CLST_SIGN then
    begin
     Finalize(FProject.FMapLayerFocus);
     Finalize(FProject.FBrushLayerFocus);
     Finalize(FProject.FTileSetInfo);
     FillChar(FProject.FInfo, SizeOf(TProjectInfo), 0);
     FillChar(FProject.FInfo.Scales, SizeOf(TScales), 2);
     FProject.FInfo.Scales.scTileFrame := 16;
     FProject.FInfo.EditMode := emMap;
     FProject.FInfo.PageSetup := [psMainFrame, psBrush, psTileSet];
     FProject.FMaps.Owner := nil;
     FProject.FMaps.OnProgress := LoadSaveProgress;
     FProject.FMaps.LoadFromFile(FProjectFileName);
     FProject.FMaps.OnProgress := nil;     
     FProject.FMaps.Owner := FProject;
     FProject.Update;
    end else
    begin
     FProject.OnProgress := LoadSaveProgress;
     FProject.LoadFromFile(FProjectFileName);
     FProject.OnProgress := nil;
    end;
   except
    on E: Exception do
    begin
     FProject.Clear;
     FProjectFileName := '';
     if E is ESectionSignatureError then
      Msg := 'Unknown section format.' else
     if E is ESectionHeaderError then
      Msg := 'Bad section header.' else
     if E is ESectionDataSizeError then
      Msg := 'Data size test failed.' else
     if E is ESectionChecksumError then
      Msg := 'Checksum test failed.'
     else
      Msg := E.Message;
     WideMessageDlg(WideFormat('Unable to load. Reason: %s', [Msg]), mtError, [mbOk], 0, mbOk);
    end;
   end;
   DestroyProgressBar;
   FProject.History.OnRemove := nil;
   FProject.History.Clear;
   FProject.History.AddEvent(nil, SInitialEvent);
   FProject.History.OnRemove := HistoryRemoveEvent;
   Saved := True;
   Done := True;
   FLastTile := nil;
   FLastFocus := nil;
  end;
 finally
  EndDialog;
 end;
 if Done then
  RefreshLists(FProject.FInfo.EditMode, FProject.FInfo.PageSetup);
end;

procedure TMainForm.SaveActionExecute(Sender: TObject);
begin
 StartDialog;
 try
  if not Named then SaveAsActionExecute(Sender) else
  begin
   CreateProgressBar;
   try
    FProject.OnProgress := LoadSaveProgress;
    FProject.SaveToFile(FProjectFileName);
    FProject.OnProgress := nil;
    Saved := True;
   except
    on E: Exception do
    begin
     Saved := False;
     WideMessageDlg(WideFormat('Unable to save. Reason: %s.', [E.Message]), mtError, [mbOk], 0, mbOk);
    end;
   end;
   DestroyProgressBar;
  end;
 finally
  EndDialog;
 end;
end;

procedure TMainForm.SaveAsActionExecute(Sender: TObject);
begin
 StartDialog;
 try
  if SaveProjectDialog.Execute then
  begin
   FProjectFileName := SaveProjectDialog.FileName;
   SaveActionExecute(Sender);
  end;
 finally
  EndDialog;
 end;
end;

procedure TMainForm.NewActionExecute(Sender: TObject);
begin
 if CheckSaved then
 begin
  FProject.Clear;
  FProject.History.OnRemove := nil;
  FProject.History.Clear;
  FProject.History.AddEvent(nil, SInitialEvent);
  FProject.History.OnRemove := HistoryRemoveEvent;
  ProjectFileName := '';
  Saved := True;
  RefreshLists(emMap);
 end;
end;

function TMainForm.GetNamed: Boolean;
begin
 Result := FProjectFileName <> '';
end;

procedure TMainForm.SetSaved(Value: Boolean);
begin
 FSaved := Value;
 ProgramTitleUpdate;
 if Value then
  FProject.History.UpdateSavedPosition;

end;

procedure TMainForm.ProgramTitleUpdate;
const
 FileStats: array [Boolean] of String[1] = ('*', '');
begin
 if not Named then
  Caption := Application.Title else
  Caption := WideFormat('%s - [%s]%s', [Application.Title,
             WideExtractFileName(FProjectFileName), FileStats[Saved]]);
end;

procedure TMainForm.SetProjFName(const Value: WideString);
begin
 FProjectFileName := Value;
 if Value = '' then FSaved := True;
 ProgramTitleUpdate;
end;

procedure TMainForm.MainFormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
 CanClose := CheckSaved;
end;

procedure TMainForm.ColorTablesListBoxClick(Sender: TObject);
var
 I, Idx: Integer;
begin
 with ColorTablesListBox do
 begin
  Idx := ItemIndex;
  if (Count > 0) and not Selected[Idx] then
   for I := 0 to Count - 1 do
    if Selected[I] then
    begin
     Idx := I;
     ItemIndex := I;
     Break;;
    end;
 end;
 ColTblComboBox.ItemIndex := Idx;
 ColorTableIndex := Idx;
 ColorTableSelected;
end;

type
 TSmallTile = array[0..31] of Byte;

function AddSmallTile(Src, Dst: TTileSet; Item: TTileItem;
                      const Attr: array of Variant;
                      X, Y: Integer; Cvt: TBitmapConverter): Integer;
var
 XFlip, YFlip: Boolean;
 TileBuf: TSmallTile;
begin
 Src.TileBitmap.ImageData := Item.TileData;
 Dst.TileLoad(Src.TileBitmap, TileBuf, X, Y, Cvt);
 with Dst.AddOptimized(TileBuf, Attr, XFlip, YFlip) do
  Result := (Index and $7FF) or
            (Ord(XFlip) shl 11) or
            (Ord(YFlip) shl 12) or
            Item.FirstColor shl 13;
end;
       (*
type
 TColTbl16 = array[0..15] of Word;
 TColTblLst = array of TColTbl16;

var
 DstFmt: TColorFormat = nil;

function AddColorTable16(Src: Pointer; SrcFmt: TColorFormat;
                         var Dst: TColTblLst): Integer;
var
 Buf: TColTbl16;
 I, Cnt: Integer;
begin
 SrcFmt.ConvertTo(DstFmt, Src, @Buf, 16);
 Cnt := Length(Dst);
 for I := 0 to Cnt - 1 do
  if CompareMem(@Buf, Addr(Dst[I]), SizeOf(TColTbl16)) then
  begin
   Result := I;
   Exit;
  end;
 Result := Cnt;
 SetLength(Dst, Cnt + 1);
 Dst[Result] := Buf;
end;

function FindTileIndex(Dst: TTileSet; I: Integer): Integer;
var
 Node: TTileItem;
begin
 Node := TTileItem(Dst.RootNode);
 Result := 0;
 while Node <> nil do
 begin
  if Node.Attributes[1] = I then Exit;
  Inc(Result);
  Node := TTileItem(Node.Next);
 end;
 Result := -1;
end;

function MakeTileAnimation(Src, Dst: TTileSet; TileIndex, FlipFlags: Integer): Integer;
var
 I, J, K, FrameDelay: Integer;
 Cvt: TBitmapConverter;
 PB: ^TSmallTile;
 X, Y: array [0..3] of Integer;
 V: Variant;
begin
 Cvt := TBitmapConverter.Create;
 try
  Dst.Clear;
  FlipFlags := (4 - 1) or
        ((FlipFlags and 3) shl IMG_FLIPS_SHIFT) or ((FlipFlags and 1) shl 6);
  Dst.SwitchFormat([FlipFlags], 8, 8 * 4);
  Dst.AttrCount := 2;
  Src.FillConverter(Cvt, [FlipFlags]);
  if FlipFlags and IMG_X_FLIP <> 0 then
  begin
   X[0] := -8; X[1] := 0;
   X[2] := -8; X[3] := 0;
  end else
  begin
   X[0] := 0;  X[1] := -8;
   X[2] := 0;  X[3] := -8;
  end;
  if FlipFlags and IMG_Y_FLIP <> 0 then
  begin
   Y[0] := -8; Y[1] := -8;
   Y[2] := 0;  Y[3] := 0;
  end else
  begin
   Y[0] := 0;  Y[1] := 0;
   Y[2] := -8; Y[3] := -8;
  end;
  I := TileIndex;
  repeat
   with Src.Indexed[I] do
   begin
    V := NamedAttr['FRAME_DELAY'];
    if VarIsNumeric(V) then
     FrameDelay := V else
     FrameDelay := 1;
    Dst.TileBitmap.Height := 8 * 4;
    with Dst.AddTile do
    begin
     PB := TileData;
     Attributes[0] := FrameDelay;
     Attributes[1] := I;
    end;
    Dst.TileBitmap.Height := 8;
    for J := 0 to 3 do
    begin
     Dst.TileBitmap.ImageData := PB;
     Draw(Dst.TileBitmap, X[J], Y[J], Cvt);
     Inc(PB);
    end;
    V := NamedAttr['NEXT_FRAME'];
   end;
   if VarIsNumeric(V) then
    K := V else
    K := 0;
   Inc(I, K);
   Result := FindTileIndex(Dst, I);
  until Result >= 0;
  Dst.TileBitmap.Height := 8 * 4;
 finally
  Cvt.Free;
 end;
end;

procedure TileSetReindex(const IndexList: array of Integer; TileSet: TTileSet);
var
 I, OldCnt, TSZ: Integer;
 Item: TTileItem;
begin
 if Length(IndexList) > 0 then
 begin
  OldCnt := TileSet.Count;
  TSZ := TileSet.TileSize;
  for I := 0 to Length(IndexList) - 1 do
  with TileSet.Items[IndexList[I]] do
  begin
   TileIndex := -TileSet.Count;
   Move(TileSet.AddTile.TileData^, TileData^, TSZ);
  end;
  Item := TTileItem(TileSet.RootNode);
  for I := 0 to OldCnt - 1 do
  begin
   if Item.TileIndex >= 0 then
    Item.TileIndex := I;
   repeat
    Item := TTileItem(Item.Next);
   until (Item = nil) or (Item.TileIndex >= 0);
  end;
 end;
end;

procedure MapReindex(TileSet: TTileSet; TileMap: Pointer; CellCount: Integer);
var
 MP: PWord;
 Attr: Word;
 I, Idx: Integer;
begin
 MP := TileMap;
 for I := 0 to CellCount - 1 do
 begin
  Idx := SwapWord(MP^);
  Attr := Idx and not $7FF;
  Idx := TileSet.Items[Idx and $7FF].TileIndex;
  if Idx < 0 then
   MP^ := SwapWord((TileSet.Items[-Idx].TileIndex and $7FF) or Attr)  else
   MP^ := SwapWord((Idx and $7FF) or Attr);
  Inc(MP);
 end;
end;

procedure TMainForm.SMDExportActionExecute(Sender: TObject);
const
 KMAP_SIGN = Ord('K') or
          (Ord('M') shl 8){ or
          (Ord('A') shl 16) or
          (Ord('P') shl 24)};
 BACK_CELL_SIZE_2 = 1;
 FORE_CELL_SIZE_2 = 1 shl 1;
 B16M_COMPRESSED = 1 shl 2;
 F16M_COMPRESSED = 1 shl 3;
 ATTR_COMPRESSED = 1 shl 4;
 BACK_COMPRESSED = 1 shl 5;
 FORE_COMPRESSED = 1 shl 6;
 INFO_COMPRESSED = 1 shl 7;
 TILES_COMPRESSED = 1 shl 8;
 POLE_BIT = 1 shl 7;

type
 TMapHeader = packed record
  mhSignature: Word; // 'KM'
  mhWidth: Word;                  // 4
  mhHeight: Word;
  mhFlags: Word;                  // 8
  mhTileAnimationCount: Word;
  mhTilesCount: Word;             // 12
  mhBackBlockCount: Word;
  mhForeBlockCount: Word;         // 16
  mhBackBlockMapOffset: LongInt;  // 20
  mhForeBlockMapOffset: LongInt;  // 24
  mhAttributeMapOffset: LongInt;  // 28
  mhTagReferenceOffset: LongInt;
  mhTilesOffset: LongInt;         // 36
  mhBackMapOffset: LongInt;       // 40
  mhForeMapOffset: LongInt;       // 44
  mhInfoMapOffset: LongInt;       // 48
 end;
 TAnimRec = packed record
  arTileIndex: Word;
  arFrameCount: Word;
  arLoopFrame: Word;
  arTilesCount: Word;
  arFlags: Word;
 end;
 TAttrRec = packed record
  case Boolean of
   False: (NorthWall: Byte;
           EastWall: Byte;
           SouthWall: Byte;
           WestWall: Byte);
   True:  (Flags: LongWord);
 end;
 TAttrIndexedRec = packed record
  Idx: Integer;
  Rec: TAttrRec;
 end;
 TAnimSetRec = record
  FLoopFrame: Integer;
  FSetCount: Integer;
  FFrameCount: Integer;
  FJoinIndex: array of Word;
  FDelayList: Pointer;
  FAttrList: array of TAttrIndexedRec;
  FAnimSet: TTileSet;
 end;
 PCellRec = ^TCellRec;
 TCellRec = array[0..3] of Word;
 PSmallTileArray = ^TSmallTileArray;
 TSmallTileArray = array[0..3] of TSmallTile;
var
 Dir: WideString;
 BigTiles: TTileSet;
 SmallTiles: TTileSet;
 AnimSetList: array of TAnimSetRec;
 AnimSet: TTileSet;
// AnimTiles: TTileSet;
 I, J, K, X, TI, LayerIdx, Idx, LayerSize: Integer;
 CvtRecList: TCvtRecList;
 CvtPtr: PTileConvertRec;
 Cvt: TBitmapConverter;
 TileBuf: array[0..15, 0..7] of Byte;
 LayerBuf, Buf, BlockBuf: Pointer;
 FC, BC: Byte; XF, YF: Boolean;
 PB: PByte; Dst, BDst, SrcW: PWord; Src: PByte; SrcD: PDWord absolute Src;
 CR: PCellRec;
 AR: ^TAttrRec;
 //ST: ^TSmallTile;
 Item, OldItem: TTileItem;
 BufStream: TMemStream;
 Header: TMapHeader;
 AnimRec: TAnimRec;
 AAttrPtr, AAttrPtr2: ^TAttrIndexedRec;
 AnimAttrList: array of LongRec;
 AnimPtrList: array of Integer;
// ColTblLst: TColTblLst;
 AttrList: array of Variant;
 TagList: array of Word;
 Pack: TStream;
begin
 StartDialog;
 try
  if WideSelectDirectory('Select output directory', '', Dir) then
  begin
   try
    Dir := WideIncludeTrailingPathDelimiter(Dir);
    BigTiles := TTileSet.Create;
    SmallTiles := TTileSet.Create;
 //   AnimTiles := TTileSet.Create;
    BufStream := TMemStream.Create;
    FProject.FMaps.TileSetList.FillCvtRecList(CvtRecList, [4 - 1]);
    try
     BigTiles.SwitchFormat([4 - 1], 16, 16);
     BigTiles.OptimizationFlags := OPT_CHECK_COLORS;
     SmallTiles.SwitchFormat([4 - 1], 8, 8);
     SmallTiles.OptimizationFlags := OPT_FIND_MIRRORING;
     //AnimTiles.Reallocate(8, 8, [4 - 1], 0);
 //    AnimTiles.Reallocate(8, 8 * 4, [4 - 1], 0);
     for I := 0 to FProject.FMaps.Count - 1 do
     with FProject.FMaps[I] do
     if Count >= 3 then // must be not less than 3 layers
     begin
      SmallTiles.AttrCount := 3;
      with SmallTiles.AddTile do // add empty tile
      begin
       Move(SmallTiles.EmptyTile.TileData^, TileData^, TileSize);
       Attributes[0] := -1;
       Attributes[1] := 0;
       Attributes[2] := 0;
      end;
 //     SetLength(AnimPtrList, 0);
      SetLength(TagList, 0);
      SetLength(AnimSetList, 0);
      Header.mhSignature := KMAP_SIGN;
      Header.mhWidth := SwapWord(Width);
      Header.mhHeight := SwapWord(Height);
      Header.mhFlags := 0;
      Header.mhTileAnimationCount := 0;
      GetMem(LayerBuf, (Width * Height) shl 2);
      try
       Dst := LayerBuf;
       for LayerIdx := 0 to 1 do with Layers[LayerIdx] do if TileSet <> nil then
       begin
        if LayerIdx <> 0 then
        begin
         BigTiles.AttrCount := 10;
         FC := FirstColor; // of TMapLayer
         with BigTiles.AddTile do
         begin
          Move(BigTiles.EmptyTile.TileData^, TileData^, TileSize);
          FirstColor := BigTiles.EmptyTile.FirstColor + FC;
          Attributes[0] := -1; // TileIndex
          Attributes[1] := 0;
          Attributes[2] := 0;
          Attributes[3] := -1; // NextFrameIndex
          Attributes[4] := 0;
          Attributes[5] := 0;
          Attributes[6] := 0;
          Attributes[7] := 0;
          Attributes[8] := 0; // empty tile BEHIND_SPR
          Attributes[9] := 0;
         end;
        end else
         BigTiles.AttrCount := 4;
        SetLength(AttrList, BigTiles.AttrCount + 3);
        CvtPtr := Addr(CvtRecList[TileSet.Index]);
        BC := TileSet.BitCount;
        Src := LayerData;
        // filling big tile set
        BigTiles.TileBitmap.ImageData := @TileBuf;
        for K := 0 to Width * Height - 1 do
        begin
         with TileSet.Indexed[(SrcD^ shr IndexShift) and IndexMask] do
         begin
          XF := (XFlipShift >= 0) and ((SrcD^ shr XFlipShift) and 1 <> 0);
          YF := (YFlipShift >= 0) and ((SrcD^ shr YFlipShift) and 1 <> 0);
          Cvt := CvtPtr.tcvtDraw[YF, XF];
          try
           FC := NamedAttr['INVISIBLE'];
          except
           FC := 0;
          end;
          if FC <> 0 then
           TileSet.TileDraw(TileSet.EmptyTile.TileData^, BigTiles.TileBitmap, 0, 0, Cvt) else
           TileSet.TileDraw(TileData^, BigTiles.TileBitmap, 0, 0, Cvt);
          if (BC < 8) and (PaletteIndexMask <> 0) then
           FC := ((SrcD^ shr PaletteIndexShift) and PaletteIndexMask) shl BC else
           FC := FirstColor;  // of TTileItem
          AttrList[1] := TileIndex;
          AttrList[2] := Ord(XF) or (Ord(YF) shl 1);
          if LayerIdx <> 0 then
          begin
           AttrList[11] := 2; // skip original index and flips when searching
           AttrList[12] := 8; // last attribute index
          end else
          begin
           AttrList[5] := 2; // skip original index and flips when searching
           AttrList[6] := 3; // last attribute index
          end;
          if TileIndex < 0 then
          begin
           AttrList[3] := 0;
           AttrList[4] := TileIndex;
           if LayerIdx <> 0 then
           begin
            AttrList[5] := 0;
            AttrList[6] := 0;
            AttrList[7] := 0;
            AttrList[8] := 0;
            AttrList[9] := 0; // empty tile BEHIND_SPR
            AttrList[10] := 0;
           end;
          end else
          try
           AttrList[3] := VarAsType(NamedAttr['FRAME_DELAY'], varByte);
           AttrList[4] := TileIndex + VarAsType(NamedAttr['NEXT_FRAME'], varInteger);
           if LayerIdx <> 0 then
           begin
            AttrList[5] := VarAsType(NamedAttr['NORTH_WALL'], varByte);
            AttrList[6] := VarAsType(NamedAttr['EAST_WALL'], varByte);
            AttrList[7] := VarAsType(NamedAttr['SOUTH_WALL'], varByte);
            AttrList[8] := VarAsType(NamedAttr['WEST_WALL'], varByte);
            AttrList[9] := VarAsType(NamedAttr['BEHIND_SPR'], varByte);
            AttrList[10] := VarAsType(NamedAttr['TAG'], varByte);
            SetLength(TagList, Max(AttrList[10], Length(TagList)));
           end;
          except
           // Add warning if some of the properties are not numeric or not exist
          end;
         end;
         Inc(FC, FirstColor); // of TMapLayer
         AttrList[0] := FC;
         Inc(Src, CellSize);
         Item := BigTiles.AddOptimized(TileBuf, AttrList, XF, YF);
         Dst^ := SwapWord(Item.Index);
         Inc(Dst);
        end; // end of big tile set fill loop
        if BigTiles.Count > 255 then
        begin
         if LayerIdx = 0 then
          Header.mhFlags := BACK_CELL_SIZE_2 else
          Header.mhFlags := Header.mhFlags or FORE_CELL_SIZE_2;
        end;
        Cvt := TBitmapConverter.Create;
        GetMem(BlockBuf, BigTiles.Count * SizeOf(TCellRec));
        try
         BigTiles.FillConverter(Cvt, [4 - 1]);
         SetLength(AttrList, 3);
         CR := BlockBuf;
         for K := 0 to BigTiles.Count - 1 do
         begin
          Item := BigTiles.Items[K];
          if Item.Attributes[3] <> Item.Attributes[0] then
           X := Item.Attributes[0] else // original tile index
           X := -1; // don't use original tile index
          AttrList[0] := X;
          AttrList[1] := Item.Attributes[2]; // FRAME_DELAY
          AttrList[2] := Item.Attributes[3] - Item.Attributes[0]; // NEXT_FRAME
          Item.FirstColor := (Item.FirstColor shr 4) and 3;
        //  with ColorTable do
        //   Item.FirstColor := AddColorTable16(Pointer(Item.FirstColor *
 //                             ColorFormat.ColorSize +
   //                           Integer(ColorData)), ColorFormat, ColTblLst) and 3;
          if (LayerIdx <> 0) and (Item.Attributes[8] and $80 <> 0) then // BEHIND_SPR
           Item.FirstColor := Item.FirstColor or (1 shl 2); // set tile priority
          if X >= 0 then
          begin
           BigTiles.TileBitmap.ImageData := Item.TileData;
           with SmallTiles.AddTile(AttrList) do
           begin
            SmallTiles.TileLoad(BigTiles.TileBitmap, TileData^, 0, 0, Cvt);
            CR[0] := (Index and $7FF) or (Item.FirstColor shl 13);
           end;
           with SmallTiles.AddTile(AttrList) do
           begin
            SmallTiles.TileLoad(BigTiles.TileBitmap, TileData^, 8, 0, Cvt);
            CR[1] := (Index and $7FF) or (Item.FirstColor shl 13);
           end;
           with SmallTiles.AddTile(AttrList) do
           begin
            SmallTiles.TileLoad(BigTiles.TileBitmap, TileData^, 0, 8, Cvt);
            CR[2] := (Index and $7FF) or (Item.FirstColor shl 13);
           end;
           with SmallTiles.AddTile(AttrList) do
           begin
            SmallTiles.TileLoad(BigTiles.TileBitmap, TileData^, 8, 8, Cvt);
            CR[3] := (Index and $7FF) or (Item.FirstColor shl 13);
           end;
          end else
          begin
           CR[0] := AddSmallTile(BigTiles, SmallTiles, Item, AttrList, 0, 0, Cvt);
           CR[1] := AddSmallTile(BigTiles, SmallTiles, Item, AttrList, 8, 0, Cvt);
           CR[2] := AddSmallTile(BigTiles, SmallTiles, Item, AttrList, 0, 8, Cvt);
           CR[3] := AddSmallTile(BigTiles, SmallTiles, Item, AttrList, 8, 8, Cvt);
          end;
          if X >= 0 then
          begin
           AnimSet := TTileSet.Create;
           try
            TI := MakeTileAnimation(TileSet, AnimSet, X, Item.Attributes[1]);
            GetMem(Buf, AnimSet.Count);
            try
             PB := Buf;
             with AnimSet do
             for X := 0 to Count - 1 do
             begin
              PB^ := Items[X].Attributes[0];
              Inc(PB);
             end;
             Idx := CR[0] and $7FF;
             for X := 0 to Length(AnimSetList) - 1 do
             with AnimSetList[X] do
             begin
              if (TI = FLoopFrame) and
                 (AnimSet.Count = FFrameCount) and
                 CompareMem(Buf, FDelayList, FFrameCount) then
              begin
               for TI := 0 to Length(FJoinIndex) - 1 do
                if FJoinIndex[TI] = Idx then
                begin
                 PB := nil;
                 Break;
                end;
               if PB <> nil then
               begin
                FAnimSet.InsertList(AnimSet, FAnimSet.Count);
               // FAnimSet.Append(AnimSet);
                Inc(FSetCount);
                SetLength(FJoinIndex, FSetCount);
                FJoinIndex[FSetCount - 1] := Idx;
                Idx := X;
               end;
               PB := nil;
               Break;
              end;
             end;
             if PB <> nil then
             begin
              Idx := Length(AnimSetList);
              SetLength(AnimSetList, Idx + 1);
              with AnimSetList[Idx] do
              begin
               FLoopFrame := TI;
               FSetCount := 1;
               FFrameCount := AnimSet.Count;
               SetLength(FJoinIndex, 1);
               FJoinIndex[0] := CR[0] and $7FF;
               FDelayList := Buf;
               Buf := nil;
               FAnimSet := AnimSet;
               AnimSet := nil;
              end;
             end;
             with AnimSetList[Idx] do
             begin
              X := Length(FAttrList);
              SetLength(FAttrList, X + FFrameCount);
              if LayerIdx = 0 then
              begin
               for TI := X to X + FFrameCount - 1 do
                FAttrList[TI].Idx := -1;
              end else
              begin
               if FSetCount <= 1 then
                AnimSet := FAnimSet;
               for TI := 0 to FFrameCount - 1 do
                with TileSet.Indexed[AnimSet.Items[TI].Attributes[1]],
                     FAttrList[TI + X] do
                begin
                 Idx := Item.Index;
                 Rec.NorthWall := NamedAttr['NORTH_WALL'];
                 Rec.EastWall := NamedAttr['EAST_WALL'];
                 Rec.SouthWall := NamedAttr['SOUTH_WALL'];
                 Rec.WestWall := NamedAttr['WEST_WALL'];
                 if NamedAttr['BEHIND_SPR'] and $7F = 1 then
                  Rec.Flags := Rec.Flags or POLE_BIT;
                end;
               if FSetCount <= 1 then
                AnimSet := nil;
              end;
             end;
            finally
             FreeMem(Buf);
            end;
           finally
            AnimSet.Free;
           end;
          end;
          CR[0] := SwapWord(CR[0]);
          CR[1] := SwapWord(CR[1]);
          CR[2] := SwapWord(CR[2]);
          CR[3] := SwapWord(CR[3]);
          Inc(CR);
         end;
         if LayerIdx = 0 then
         begin
          Header.mhBackBlockCount := BigTiles.Count;
          Header.mhBackBlockMapOffset := BufStream.Position;
         end else
         begin
          Header.mhForeBlockCount := BigTiles.Count;
          Header.mhForeBlockMapOffset := BufStream.Position;
         end;
         BufStream.WriteBuffer(BlockBuf^, BigTiles.Count * SizeOf(TCellRec));
         if BufStream.Position and 1 <> 0 then
          BufStream.Seek(1, soFromCurrent);
        finally
         FreeMem(BlockBuf);
         Cvt.Free;
        end;
        if LayerIdx <> 0 then
        begin
         Header.mhAttributeMapOffset := BufStream.Position;
         Header.mhFlags := Header.mhFlags or ATTR_COMPRESSED;
         GetMem(BlockBuf, BigTiles.Count * 4);
         try
          AR := BlockBuf;
          for K := 0 to BigTiles.Count - 1 do
          with BigTiles.Items[K] do
          begin
           AR.NorthWall := Attributes[4];
           AR.EastWall := Attributes[5];
           AR.SouthWall := Attributes[6];
           AR.WestWall := Attributes[7];
           if Attributes[8] and $7F = 1 then
            AR.Flags := AR.Flags or POLE_BIT;
           X := Attributes[9];
           if X > 0 then
            TagList[X - 1] := SwapWord(Index);
           Inc(AR);
          end;
          Pack := TALZSS_PackStream.Create(BufStream, 10);
          try
           Pack.WriteBuffer(BlockBuf^, BigTiles.Count * 4);
          finally
           Pack.Free;
          end;
          if BufStream.Position and 1 <> 0 then
           BufStream.Seek(1, soFromCurrent);
          Header.mhTagReferenceOffset := BufStream.Position;
          BufStream.WriteBuffer(Pointer(TagList)^, Length(TagList) * 2);
         finally
          FreeMem(BlockBuf);
         end;
        end;
        BigTiles.Clear;
        Finalize(AttrList);
       end;
 //      Header.mhColorTableCount := Min(Length(ColTblLst), 4); // TODO: add palette
 //      Header.mhColorTableOffset := BufStream.Position;       //  overflow warning
 //      BufStream.WriteBuffer(Pointer(ColTblLst)^,
   //      Header.mhColorTableCount * SizeOf(TColTbl16));

       Header.mhTileAnimationCount := SwapWord(Length(AnimSetList));
       SetLength(AnimPtrList, 0);
       for X := 0 to Length(AnimSetList) - 1 do
       with AnimSetList[X] do
       begin
        Idx := Length(AnimPtrList);
        SetLength(AnimPtrList, Idx + FSetCount * 4);
        for J := 0 to FSetCount - 1 do
        begin
         K := Idx + J * 4;
         AnimPtrList[K + 0] := FJoinIndex[J] + 0;
         AnimPtrList[K + 1] := FJoinIndex[J] + 1;
         AnimPtrList[K + 2] := FJoinIndex[J] + 2;
         AnimPtrList[K + 3] := FJoinIndex[J] + 3;
        end;
       end;
       TileSetReindex(AnimPtrList, SmallTiles);
       MapReindex(SmallTiles, Pointer(Integer(BufStream.Memory) +
                  Header.mhBackBlockMapOffset), Header.mhBackBlockCount * 4);
       MapReindex(SmallTiles, Pointer(Integer(BufStream.Memory) +
                  Header.mhForeBlockMapOffset), Header.mhForeBlockCount * 4);

      ////////// Write animation data ////////////
       SetLength(AnimPtrList, Length(AnimSetList));
       for X := 0 to Length(AnimSetList) - 1 do
       with AnimSetList[X] do
       begin
        Item := TTileItem(FAnimSet.RootNode);
        for Idx := 0 to FSetCount - 1 do
         for J := 0 to FFrameCount - 1 do
         begin
          Item.TileIndex := J * FSetCount + Idx;
          Item := TTileItem(Item.Next);
         end;
        FAnimSet.FastList := False;
        AnimPtrList[X] := BufStream.Position;
        AnimRec.arTileIndex := SwapWord
                               (
                                   SmallTiles.Items
                                   [
                                        -SmallTiles.Items
                                        [
                                           FJoinIndex[0]
                                        ].TileIndex
                                   ].TileIndex
                               );
        AnimRec.arFrameCount := SwapWord(FFrameCount);
        AnimRec.arLoopFrame := SwapWord(FLoopFrame);
        AnimRec.arTilesCount := SwapWord(FSetCount);
        AnimRec.arFlags := 0;
        Idx := 0;
        for K := 0 to FSetCount - 1 do
        begin
         AAttrPtr := nil;
         for TI := 0 to FFrameCount - 1 do
         begin
          with FAttrList[Idx] do
          begin
           if (Idx >= 0) then
           begin
            if AAttrPtr = nil then
            begin
             AAttrPtr := @Idx;
            end else
            if Rec.Flags <> AAttrPtr.Rec.Flags then
            begin
             AnimRec.arFlags := 256;
             Break;
            end;
           end;
          end;
          Inc(Idx);
         end;
         if AnimRec.arFlags <> 0 then Break;
        end;
        BufStream.WriteBuffer(AnimRec, SizeOf(TAnimRec));
        FAnimSet.WriteToStream(BufStream, 0, FAnimSet.Count, False);
//        FAnimSet.AddToStream(BufStream);
 ///
        if AnimRec.arFlags <> 0 then
        begin
         SetLength(AnimAttrList, FSetCount);
         TI := (FSetCount * 4) + ((FFrameCount + 1) and not 1);
         AAttrPtr := Pointer(FAttrList);
         for K := 0 to FSetCount - 1 do
          with AnimAttrList[K] do
          begin
           Words[0] := SwapWord(TI);
           AAttrPtr2 := AAttrPtr;
           if AAttrPtr2.Idx >= 0 then
           begin
            Inc(AAttrPtr2);
            for Idx := 1 to FFrameCount - 1 do
            begin
             if AAttrPtr2.Rec.Flags <> AAttrPtr.Rec.Flags then
             begin
              AAttrPtr2 := nil;
              Break;
             end;
             Inc(AAttrPtr2);
            end;
           end;
           if AAttrPtr2 <> nil then
           begin
            Words[1] := $FFFF;
            for Idx := 0 to FFrameCount - 1 do
            begin
             AAttrPtr.Idx := -1;
             Inc(AAttrPtr);
            end;
           end else
           begin
            Words[1] := SwapWord(AAttrPtr.Idx);
            //if AAttrPtr.Idx >= 0 then
            Inc(TI, FFrameCount * SizeOf(TAttrRec));
            Inc(AAttrPtr, FFrameCount);
           end;
          end;
         BufStream.WriteBuffer(Pointer(AnimAttrList)^, FSetCount * 4);
         Finalize(AnimAttrList);
        end;
 //////
        BufStream.WriteBuffer(FDelayList^, FFrameCount);
        if BufStream.Position and 1 <> 0 then
         BufStream.Seek(1, soFromCurrent);
 //////
        if AnimRec.arFlags <> 0 then
        begin
         AAttrPtr := Pointer(FAttrList);
         for K := 0 to FSetCount - 1 do
          for TI := 0 to FFrameCount - 1 do
          begin
           if AAttrPtr.Idx >= 0 then
            BufStream.WriteBuffer(AAttrPtr.Rec, SizeOf(TAttrRec));
           Inc(AAttrPtr);
          end;
        end;  
 //////
       end; //// End of animation data write

       Item := TTileItem(SmallTiles.RootNode);
       Item.TileIndex := -1; // deleting the empty tile
       while Item <> nil do
       begin
        OldItem := Item;
        Item := TTileItem(Item.Next);
        if OldItem.TileIndex < 0 then
         SmallTiles.Remove(OldItem);
       end;
       Header.mhTilesOffset := BufStream.Position;
       Header.mhTilesCount := SmallTiles.Count; // TODO 1 -oAlicia: check for overflow
       Header.mhFlags := Header.mhFlags or TILES_COMPRESSED;
       Pack := TALZSS_PackStream.Create(BufStream, 10);
       try
      //  SmallTiles.AddToStream(Pack, True);
        SmallTiles.WriteToStream(Pack, 0, SmallTiles.Count, False);
       finally
        Pack.Free;
       end;
       if BufStream.Position and 1 <> 0 then
        BufStream.Seek(1, soFromCurrent);

 //      Header.mhAnimatedTilesOffset := BufStream.Position;
 //      AnimTiles.AddToStream(BufStream, True); // no need to align

       LayerSize := Width * Height;
       X := LayerSize;
       if X <= 32768 then
        Header.mhFlags := Header.mhFlags or INFO_COMPRESSED;
       Inc(X, LayerSize);
       if Header.mhFlags and FORE_CELL_SIZE_2 <> 0 then
        Inc(X, LayerSize);
       if X <= 32768 then
        Header.mhFlags := Header.mhFlags or FORE_COMPRESSED;
       Inc(X, LayerSize);
       if Header.mhFlags and BACK_CELL_SIZE_2 <> 0 then
        Inc(X, LayerSize);
       if X <= 32768 then
        Header.mhFlags := Header.mhFlags or BACK_COMPRESSED;
       Src := LayerBuf;
       if Header.mhFlags and BACK_CELL_SIZE_2 = 0 then
       begin
        Inc(Src);
        PB := LayerBuf;
        for X := 0 to LayerSize - 1 do
        begin
         PB^ := Src^;
         Inc(PB);
         Inc(Src, 2);
        end;
        Dec(Src);
        X := LayerSize;
       end else
       begin
        X := LayerSize shl 1;
        Inc(Src, X);      
       end;
       Header.mhBackMapOffset := BufStream.Position;
       if Header.mhFlags and BACK_COMPRESSED <> 0 then
       begin
        Pack := TALZSS_PackStream.Create(BufStream, 10);
        try
         Pack.WriteBuffer(LayerBuf^, X);
        finally
         Pack.Free;
        end;
       end else
        BufStream.WriteBuffer(LayerBuf^, X);
       if BufStream.Position and 1 <> 0 then
        BufStream.Seek(1, soFromCurrent);
       if Header.mhFlags and FORE_CELL_SIZE_2 = 0 then
       begin
        Inc(Src);
        PB := LayerBuf;
        for X := 0 to LayerSize - 1 do
        begin
         PB^ := Src^;
         Inc(PB);
         Inc(Src, 2);
        end;
        Src := LayerBuf;
        X := LayerSize;
       end else
        X := LayerSize shl 1;
       Header.mhForeMapOffset := BufStream.Position;
       if Header.mhFlags and FORE_COMPRESSED <> 0 then
       begin
        Pack := TALZSS_PackStream.Create(BufStream, 10);
        try
         Pack.WriteBuffer(Src^, X);
        finally
         Pack.Free;
        end;
       end else
        BufStream.WriteBuffer(Src^, X);
       if BufStream.Position and 1 <> 0 then
        BufStream.Seek(1, soFromCurrent);
       Src := Layers[2].LayerData;
       PB := LayerBuf;
       for X := 0 to LayerSize - 1 do
       begin
        PB^ := Src^;
        Inc(PB);
        Inc(Src, 2);
       end;
       Header.mhInfoMapOffset := BufStream.Position;
       if Header.mhFlags and INFO_COMPRESSED <> 0 then
       begin
        Pack := TALZSS_PackStream.Create(BufStream, 10);
        try
         Pack.WriteBuffer(LayerBuf^, LayerSize);
        finally
         Pack.Free;
        end;
       end else
        BufStream.WriteBuffer(LayerBuf^, LayerSize);
      finally
       FreeMem(LayerBuf);
      end;
 //     AnimTiles.Clear;

      SmallTiles.Clear;
      with TTntFileStream.Create(WideFormat('%s%s.map', [Dir,
          FixedFileName(Name, WideChar('_'), FNIC + ['.', ',', '-', ' ', ''''])]), fmCreate) do
      try
       with Header do
       begin
        X := SizeOf(TMapHeader) + Length(AnimPtrList) shl 2;
        mhFlags := SwapWord(mhFlags);
        mhTilesCount := SwapWord(mhTilesCount);
        mhBackBlockCount := SwapWord(mhBackBlockCount);
        mhForeBlockCount := SwapWord(mhForeBlockCount);
        mhBackBlockMapOffset := SwapInt(mhBackBlockMapOffset + X);
        mhForeBlockMapOffset := SwapInt(mhForeBlockMapOffset + X);
        mhAttributeMapOffset := SwapInt(mhAttributeMapOffset + X);
        mhTagReferenceOffset := SwapInt(mhTagReferenceOffset + X);
 //       mhColorTableOffset := SwapInt(mhColorTableOffset + X);
        mhTilesOffset := SwapInt(mhTilesOffset + X);
 //       mhAnimatedTilesOffset := SwapInt(mhAnimatedTilesOffset + X);
        mhBackMapOffset := SwapInt(mhBackMapOffset + X);
        mhForeMapOffset := SwapInt(mhForeMapOffset + X);
        mhInfoMapOffset := SwapInt(mhInfoMapOffset + X);
        for J := 0 to Length(AnimPtrList) - 1 do
         AnimPtrList[J] := SwapInt(AnimPtrList[J] + X);
       end;
       WriteBuffer(Header, SizeOf(TMapHeader));
       WriteBuffer(Pointer(AnimPtrList)^, Length(AnimPtrList) shl 2);
       WriteBuffer(BufStream.Memory^, BufStream.Size);
      finally
       Free;
      end;
      for X := 0 to Length(AnimSetList) - 1 do
      with AnimSetList[X] do
      begin
       FreeMem(FDelayList);
       FAnimSet.Free;      
       Finalize(FJoinIndex);
       Finalize(FAttrList);
      end;
      BufStream.Clear;
     end // else // end of one map cycle
         //TODO 1: Not enough layers;
    finally
     CvtRecListFree(CvtRecList);
     BufStream.Free;
 //    AnimTiles.Free;
     SmallTiles.Free;
     BigTiles.Free;
    end;
   except
    on E: Exception do
     WideMessageDlg(E.Message, mtError, [mbOk], 0, mbOk);
   end;
  end;
 finally
  EndDialog;
 end;
end;       *)

procedure TMainForm.ColTblAddFromFilesActionExecute(Sender: TObject);
var
 I, NewIndex, SaveCnt: Integer;
 S, Ext: WideString;
 Stream: TStream;
 TempColTbls, List: TColorTableList;
 Item: TColorTable;
 Event: THistoryEvent;
begin
 if FProgressBar = nil then
 begin
  List := FProject.FMaps.ColorTableList;
  with List, OpenActDialog do
  begin
   Options := Options + [ofAllowMultiSelect];
   if Execute then
   begin
    NewIndex := Count;
    I := 0;
    while I < Files.Count do
    begin
     SaveCnt := Count;
     S := Files[I];
     try
      Stream := TTntFileStream.Create(S, fmOpenRead or fmShareDenyWrite);
      try
       Ext := WideUpperCase(WideExtractFileExt(S));
       if Ext = '.CCT' then
       begin
        TempColTbls := TColorTableList.Create;
        try
         TempColTbls.LoadFromStream(Stream);
         Event := AddColorTableListEvent(WideFormat('Add color tables from ''%s''', [S]));
         InsertList(TempColTbls, SaveCnt);
         ColorTableIndex := SaveCnt;
         Event.RedoData := List;
        finally
         TempColTbls.Free;
        end;
       end else
       if Ext = '.ACT' then
       begin
        Event := AddColorTableListEvent('');
        try
         with AddChild(WideChangeFileExt(WideExtractFileName(S),
                                         '')) as TColorTable do
         begin
          ColorFormat.Assign(RGB_ColorFormat);
          LoadActFromStream(Stream);
          Event.Text := WideFormat('Add color table ''%s'' from file',
                                    [Name]);
         end;
        except
         with FProject.History do
          Count := Count - 1;
         raise;
        end;
        ColorTableIndex := SaveCnt;
        Event.RedoData := List;
       end
      finally
       Stream.Free;
      end;
      if SaveCnt = Count then
      begin
       Item := ImportColorsDialog(S);
       if Item <> nil then
       begin
        Event := AddColorTableListEvent('');
        AttachNode(Item, SaveCnt);
        Item.Name := WideChangeFileExt(WideExtractFileName(S), '');
        United := True;
        Event.Text :=  WideFormat('Add color table ''%s'' from file',
                                  [Item.Name]);
        ColorTableIndex := SaveCnt;
        Event.RedoData := List;
       end;
      end;
     except
      on E: Exception do
       case MessageDlg(E.Message, mtError, [mbOk, mbRetry, mbAbort], 0) of
        mrRetry:
        begin
         Count := SaveCnt;
         Continue;
        end;
        mrAbort:
        begin
         Count := SaveCnt;
         if I = 0 then
          Exit else
          Break;
        end;
       end;
     end;
     Inc(I);
    end;

    ColorTablesListBox.Count := Count;

    for I := NewIndex to Count - 1 do
     ColorTablesListBox.Selected[I] := True;

    ColorTableIndex := NewIndex;
    ColorTablesModified;
   end;
  end;
 end;
end;

function TMainForm.UseEraser(Control: TCustomTileMapView;
 BrushErase: Boolean; DrawX, DrawY: Integer): Boolean;
var
 Layer: TMapLayer;
 I, CX, XX, YY, CY: Integer;
 lb: TLayerListBox;
 MapsFrame: TMapsFrame;
 ms: Boolean;
 PB: PByte;
begin
 Result := False;
 with Control.Owner as TMapFrame do
  if MapData <> nil then
  begin
   lb := nil;
   if ssCtrl in GlobalShiftState then
    for I := 0 to Length(MapFramesList) - 1 do
    begin
     MapsFrame := MapFramesList[I];
     if MapsFrame <> Owner then
      with MapsFrame do
       if (MapFrame.MapData = MapData) and
          (PageControl.ActivePageIndex = 0) then
       begin
        lb := LayersListBox;
        Break;
       end;
    end;
                 // Mouse button: Erase content on checked layers
          // Ctrl + Mouse button: Erase content on selected layers
   BrushErase := BrushErase and not BrushMode;
   ms := Control.MultiSelected;
   Control.DefaultCursor := crEraser;

   if BrushErase then
   begin
    with BrushPattern^ do
    begin
     XX := DrawX - X;
     YY := DrawY - Y;
     PB := P;
     for CY := YY to YY + (H - 1) do
      for CX := XX to XX + (W - 1) do
      begin
       if (PB^ <> 0) and
          (not ms or Control.Selected[CX, CY]) then
        Result := FProject.FMaps.BrushList.EraseBrushContentAt(MapData,
                                           Bounds(CX, CY, 1, 1)) or Result;
       Inc(PB);
      end;
    end;
   end
   else
  // if not Result then
    for I := 0 to MapData.Count - 1 do
     if (lb = nil) or lb.Selected[I] then
     begin
      Layer := MapData.Nodes[I] as TMapLayer;
      with Layer do
       if (TileSet <> nil) and
          (Flags and LF_VISIBLE <> 0) and
          ((lb <> nil) or (Flags and LF_LAYER_ACTIVE <> 0)) then
       begin
        TileSet.MaxIndex := IndexMask;
        with BrushPattern^ do
        begin
         XX := DrawX - X;
         YY := DrawY - Y;
         PB := P;
         for CY := YY to YY + (H - 1) do
          for CX := XX to XX + (W - 1) do
          begin
           if (PB^ <> 0) and
              (not ms or Control.Selected[CX, CY]) then
           begin
            Result := True;
            TileIndex[CX, CY] := TileSet.EmptyTileIndex;
           end;
           Inc(PB);
          end;
        end;
       end;
     end;
  end;
end;

function TMainForm.DrawWithPencil(Control: TCustomTileMapView; DrawX, DrawY: Integer): Boolean;
var
 Layer: TMapLayer;
 I, XX, YY, CX, CY, J: Integer;
 lb: TLayerListBox;
 MapsFrame: TMapsFrame;
 PB: PByte;
 ms: Boolean;
begin
 Result := False;
 with Control.Owner as TMapFrame do
  if MapData <> nil then
  begin
   lb := nil;
   if ssCtrl in GlobalShiftState then
    for I := 0 to Length(MapFramesList) - 1 do
    begin
     MapsFrame := MapFramesList[I];
     if MapsFrame <> Owner then
      with MapsFrame do
       if (MapFrame.MapData = MapData) and
          (PageControl.ActivePageIndex = 0) then
       begin
        lb := LayersListBox;
        Break;
       end;
    end;
          // Mouse button: draw tile on checked layers
   // Ctrl + Mouse button: draw tile on selected layers
   ms := Control.MultiSelected;
   Control.DefaultCursor := crPencil;
   for I := 0 to MapData.Count - 1 do
    if (lb = nil) or lb.Selected[I] then
    begin
     Layer := MapData.Nodes[I] as TMapLayer;
     with Layer do
      if (TileSet <> nil) and
         (Flags and LF_VISIBLE <> 0) and
         ((lb <> nil) or (Flags and LF_LAYER_ACTIVE <> 0)) then
      with BrushPattern^ do
      begin
       XX := DrawX - X;
       YY := DrawY - Y;
       PB := P;
       for CY := YY to YY + (H - 1) do
        for CX := XX to XX + (W - 1) do
        begin
         if (PB^ <> 0) and
            (not ms or Control.Selected[CX, CY]) then
         begin
          if Flags and LF_TILE_SELECTED <> 0 then
          begin
           Cells[CX, CY] := DrawInfo;
           Result := True;
          end else
          for J := 0 to Length(TileSetFramesList) - 1 do
           with TileSetFramesList[J] do
            if (PageControl.ActivePageIndex <> 0) and
               (TileSet = TilesFrame.TileSet) then
            begin
             TileIndex[CX, CY] := TilesFrame.SelectedTile;
             Result := True;
             Break;
            end;
         end;
         Inc(PB);
        end;
      end;
    end;
  end;
end;

type
 TPickerList = array of record ts: TTileSet; idx, pri: Integer end;

procedure PickTile(const PickerList: TPickerList; TilesFrame: TTilesFrame);
var
 I, Index, PriMax: Integer;
begin
 PriMax := Low(Integer);
 Index := -1;
 with TilesFrame do
 begin
  for I := 0 to Length(PickerList) - 1 do
   with PickerList[I] do
    if (ts = TileSet) and (pri >= PriMax) then
    begin
     PriMax := pri;
     Index := idx;
    end;

  if Index >= 0 then
  begin
   TileSetView.SelectionMode := selSingle;
   SelectedTile := Index;
  end;
 end;
end;

procedure TMainForm.MapFrameMapEditViewCustomMouseAction(
  Sender: TCustomTileMapView; var Done: Boolean);
var
 Used: Boolean;
 I, PI, X, Y, J: Integer;
 Item: TTileItem;
 CellInfo: TCellInfo;
 dt: TDrawTool;
 PickerList: TPickerList;
 Pick, RightButton: Boolean;
 MapsFrame: TMapsFrame;
 lb: TLayerListBox;
 brsh: TMapBrush;
 Layer: TMapLayer;
 pt: TPoint;
 DrawUnit: TDrawUnitFunction;
begin
 if Sender.NewCursorPosition then
  with Sender.Owner as TMapFrame do
   if MapData <> nil then
   begin
    RightButton := ssRight in GlobalShiftState;

    case DrawTool of
     dtSingleSelect:
     begin
      if not (ssAlt in GlobalShiftState) then
      begin
       if Sender.SelectionMode = selSingle then
        Sender.Selected[Sender.TileX, Sender.TileY] := True;
       dt := dtSingleSelect;
      end else
       dt := dtPicker;
     end;
     dtPencil: if RightButton then
                dt := dtEraser else
               if ssAlt in GlobalShiftState then
                dt := dtPicker else
                dt := dtPencil;

     dtBrush: if RightButton then
               dt := dtEraser else
               dt := dtBrush;

     dtEraser: dt := dtEraser;

     else dt := FDrawTool;
    end;

    case dt of
     dtPencil,
     dtEraser,
     dtBrush:
     begin
      case dt of
       dtPencil:
        DrawUnit := DrawWithPencil;
       dtEraser:
       if (DrawTool = dtBrush) or not RightButton then
        DrawUnit := UseBrushEraser else
        DrawUnit := UseTileEraser;
       else
        DrawUnit := DrawWithBrush;
      end;

      if FDrawLine > 0 then
      begin
       for I := 0 to MapData.Count - 1 do
       begin
        Layer := MapData.Nodes[I] as TMapLayer;
        if Layer.Flags and LF_VISIBLE <> 0 then
        begin
         with TMap(FBackup).Nodes[I] as TMapLayer do
          Move(LayerData^, Layer.LayerData^, LayerDataSize);
        end;
       end;
       with FClickPos do
        case FDrawLine of
         1:
          Used := DrawMapLine(Sender, DrawUnit, X, Y, Sender.TileX, Sender.TileY);
         else
          Used := DrawMapRect(Sender, DrawUnit, X, Y, Sender.TileX, Sender.TileY);
        end;
      end else
       Used := DrawUnit(Sender, Sender.TileX, Sender.TileY);

      if Used then
      begin
       Saved := False;
       RefreshMapContent(MapData, [rmcView]);
      end;
     end;
   {  dtEraser:
     begin
      Sender.PasteBufferApply;
      Nothing := not UseEraser(Sender,
       (DrawTool = dtBrush) or not RightButton,
        Sender.TileX, Sender.TileY);
     end;
     dtBrush:
     begin
     if BrushMode then
      begin
       WideMessageDlg('You cannot use brush on brush', mtError, [mbOk], 0);
       Exit;
      end;
      Sender.PasteBufferApply;

      Nothing := not DrawWithBrushEx(Sender,
       BrushesFrame.MapFrame.MapData as TMapBrush,
       Sender.TileX, Sender.TileY);
     end;  }
     dtPicker:                    // Left button: pick tile for the checked layers
     begin                        // Right button: select tile in visible tile sets
                           // Ctrl + Left button: pick tile for the selected layers
      lb := nil;
      if not RightButton and (ssCtrl in GlobalShiftState) then
       for I := 0 to Length(MapFramesList) - 1 do
       begin
        MapsFrame := MapFramesList[I];
        if MapsFrame <> Owner then
         with MapsFrame do
          if (MapFrame.MapData = MapData) and
             (PageControl.ActivePageIndex = 0) then
          begin
           lb := LayersListBox;
           Break;
          end;
       end;

      X := Sender.TileX;
      if X < 0 then
       X := 0;
      if X >= MapData.Width then
       X := MapData.Width - 1;

      Y := Sender.TileY;
      if Y < 0 then
       Y := 0;
      if Y >= MapData.Height then
       Y := MapData.Height - 1;
       
      if (MapData.Width > 0) and
         (MapData.Height > 0) then
      begin
       if Sender.SelectionMode = selSingle then
        Sender.Selected[X, Y] := True;

       if RightButton then
       begin
        Used := False;
        with FProject.FMaps.BrushList do
         for I := 0 to Count - 1 do
         begin
          brsh := Nodes[I] as TMapBrush;
          for J := MapData.Count - 1 downto 0 do
          begin
           Layer := MapData.Nodes[J] as TMapLayer;
           if (Layer.TileSet <> nil) and
              (Layer.Flags and LF_VISIBLE <> 0) then
           begin
            Layer := brsh.FindExactBrushLayer(Layer, X, Y, pt);
            if Layer <> nil then
            begin
             FProject.FBrushLayerFocus[brsh.Index] := Layer.Index;
             BrushesFrame.SelectMap(brsh.Index);
             Application.ProcessMessages;
             Used := True;
             Break;
            end;
           end;
          end;
          if Used then
           Break;
         end;
       end;
      end;

      Used := False;    
      PI := 0;
      SetLength(PickerList, MapData.Count);
      for I := 0 to MapData.Count - 1 do
       with MapData.Nodes[I] as TMapLayer do
        if (TileSet <> nil) and
           (Flags and LF_VISIBLE <> 0) then
        begin
         Pick := not RightButton;
         if Pick then
         begin
          if lb <> nil then
           Pick := lb.Selected[I] else
           Pick := (Flags and LF_LAYER_ACTIVE <> 0);
         end;
         CellInfo := Cells[X, Y];
         Item := TileSet.Indexed[CellInfo.TileIndex];
         if (Item <> nil) and (Item <> TileSet.EmptyTile) then
         begin
          with PickerList[PI] do
          begin
           ts := TileSet;
           idx := CellInfo.TileIndex;
           pri := CellInfo.Priority;
          end;
          Inc(PI);
          if Pick then
           Flags := Flags or LF_TILE_SELECTED else
           Continue;
         end else
         if Pick then
          Flags := Flags and not LF_TILE_SELECTED else
          Continue;
         DrawInfo := CellInfo;
         Used := True;
        end;

      SetLength(PickerList, PI);
      for I := 0 to Length(TileSetFramesList) - 1 do
       with TileSetFramesList[I] do
        if PageControl.ActivePageIndex <> 0 then
         PickTile(PickerList, TilesFrame);

      if Used then
       RefreshMapContent(MapData, [rmcLayersList]);
     end;
    end;
   end;
end;

procedure UpdateMagicCursor(View: TCustomTileMapView; State: TShiftState);
var
 Cur: TCursor;
begin
 State := State - (MouseStates + [ssCtrl]);
 if State = View.SelectIncludeShiftState - MouseStates then
  Cur := crMagicInc else
 if State = View.SelectExcludeShiftState - MouseStates then
  Cur := crMagicDec else
  Cur := crMagicWand;
 View.DefaultCursor := Cur;
end;

procedure ContinguousMapSelect(Frame: TMapFrame; X, Y: Integer;
                  Shift: TShiftState);
var
 SelBuf: PBoolean;
 SelInc: Boolean;
 CellInfo: array of TCellInfo;
 ReadInfo: TCellInfo;
 ric: TCellInfoCmp absolute ReadInfo;
 Map: TMap;

 procedure Beam(XInc, YInc, X, Y: Integer);
 var
  SP: PBoolean;
  Offset: Integer;
  Incr, I: Integer;
  Equals: Boolean;
  PV: PInteger;
 begin
  if XInc <> 0 then
  begin
   Incr := XInc;
   PV := @X;
  end else
  begin
   Incr := YInc;
   PV := @Y;
  end;
  repeat
   Inc(PV^, Incr);
   if (X < 0) or
      (Y < 0) or
      (X >= Map.Width) or
      (Y >= Map.Height) then Break;

   Offset := Y * Map.Width + X;
   SP := SelBuf;
   Inc(SP, Offset);
   if SP^ <> SelInc then
   begin
    Equals := True;
    for I := 0 to Map.Count - 1 do
     with Map.Nodes[I] as TMapLayer do
      if (Flags and LF_LAYER_ACTIVE <> 0) and
         (Flags and LF_VISIBLE <> 0) then
      begin
       ReadCellInfo(PByteArray(LayerData)[Offset * CellSize], ReadInfo);
       with TCellInfoCmp(CellInfo[I]) do
        if (Test1 <> ric.Test1) or
           (Test2 <> ric.Test2) or
           (Test3 <> ric.Test3) then
        begin
         Equals := False;
         Break;
        end;
      end;

    if Equals then
    begin
     SP^ := SelInc;
     if YInc >= 0 then
      Beam(0, -1, X, Y);
     if YInc <= 0 then
      Beam(0, +1, X, Y);
     if XInc >= 0 then
      Beam(-1, 0, X, Y);
     if XInc <= 0 then
      Beam(+1, 0, X, Y);
     Continue;
    end;
   end;
   Break;
  until False;
 end;

var
 Offset, J: Integer;
begin
 Map := Frame.MapData;
 if Map <> nil then
  with Frame.MapEditView do
  begin
   SelBuf := SelectionBuffer;

   SelInc := Shift <> SelectExcludeShiftState;
   if Shift = SelectNewShiftState then
    FillChar(SelBuf^, MapWidth * MapHeight, 0);

   Offset := Y * MapWidth + X;

   PBoolean(Integer(SelBuf) + Offset)^ := SelInc;

   SetLength(CellInfo, Map.Count);

   for J := 0 to Map.Count - 1 do
   begin
    with Map.Nodes[J] as TMapLayer do
    if (Flags and LF_LAYER_ACTIVE <> 0) and
       (Flags and LF_VISIBLE <> 0) then
     ReadCellInfo(PByteArray(LayerData)[Offset * CellSize], CellInfo[J]);
   end;

   Beam(0, -1, X, Y);
   Beam(0, +1, X, Y);
   Beam(-1, 0, X, Y);
   Beam(+1, 0, X, Y);

   UpdateSelection;
  end;
end;

procedure MapMagicWand(Frame: TMapFrame; Shift: TShiftState);
var
 J, I, Size, Offset: Integer;
 SP: PBoolean;
 SelInc, Equals: Boolean;
 Contiguous: Boolean;
 CellInfo: array of TCellInfo;
 ReadInfo: TCellInfo;
 ric: TCellInfoCmp absolute ReadInfo;
begin
 Contiguous := ssCtrl in Shift;
 if Contiguous then
  Shift := Shift - [ssCtrl];
 with Frame, MapEditView do
 if (Shift = SelectNewShiftState) or
    (Shift = SelectIncludeShiftState) or
    (Shift = SelectExcludeShiftState) then
 begin
  if not Contiguous then
  begin
   SelInc := Shift <> SelectExcludeShiftState;

   Size := MapWidth * MapHeight;
   if Shift = SelectNewShiftState then
    FillChar(SelectionBuffer^, Size, 0);

   Offset := TileY * MapWidth + TileX;

   SetLength(CellInfo, MapData.Count);

   for J := 0 to MapData.Count - 1 do
   begin
    with MapData.Nodes[J] as TMapLayer do
    if (Flags and LF_LAYER_ACTIVE <> 0) and
       (Flags and LF_VISIBLE <> 0) then
     ReadCellInfo(PByteArray(LayerData)[Offset * CellSize], CellInfo[J]);
   end;

   SP := SelectionBuffer;

   for I := 0 to Size - 1 do
   begin
    Equals := True;
    for J := 0 to MapData.Count - 1 do
     with MapData.Nodes[J] as TMapLayer do
      if (Flags and LF_LAYER_ACTIVE <> 0) and
         (Flags and LF_VISIBLE <> 0) then
      begin
       ReadCellInfo(PByteArray(LayerData)[I * CellSize], ReadInfo);
       with TCellInfoCmp(CellInfo[J]) do
        if (Test1 <> ric.Test1) or
           (Test2 <> ric.Test2) or
           (Test3 <> ric.Test3) then
        begin
         Equals := False;
         Break;
        end;
      end;

    if Equals then
     SP^ := SelInc;
    Inc(SP);
   end;

   UpdateSelection;
  end else
   ContinguousMapSelect(Frame, TileX, TileY, Shift);
  UpdateMagicCursor(Frame.MapEditView, Shift);
 end;
end;

function MapFloodFill(Frame: TMapFrame): Boolean;
var
 SelBuf: PBoolean;
 Map: TMap;
 CellInfo: array of TCellInfo;
 ReadInfo: TCellInfo;
 ric: TCellInfoCmp absolute ReadInfo;

 procedure Beam(XInc, YInc, X, Y: Integer);
 var
  Offset: Integer;
  Incr, I: Integer;
  Equals: Boolean;
  PV: PInteger;
 begin
  if XInc <> 0 then
  begin
   Incr := XInc;
   PV := @X;
  end else
  begin
   Incr := YInc;
   PV := @Y;
  end;
  repeat
   Inc(PV^, Incr);
   if (X < 0) or
      (Y < 0) or
      (X >= Map.Width) or
      (Y >= Map.Height) then Break;

   Offset := Y * Map.Width + X;

   if (SelBuf = nil) or PBoolean(Integer(SelBuf) + Offset)^ then
   begin
    Equals := True;
    for I := 0 to Map.Count - 1 do
     with Map.Nodes[I] as TMapLayer do
      if (Flags and LF_LAYER_ACTIVE <> 0) and
         (Flags and LF_VISIBLE <> 0) then
      begin
       ReadCellInfo(PByteArray(LayerData)[Offset * CellSize], ReadInfo);
       with TCellInfoCmp(CellInfo[I]) do
        if (Test1 <> ric.Test1) or
           (Test2 <> ric.Test2) or
           (Test3 <> ric.Test3) then
        begin
         Equals := False;
         Break;
        end;
      end;

    if Equals then
    begin
     for I := 0 to Map.Count - 1 do
      with Map.Nodes[I] as TMapLayer do
       if (Flags and LF_LAYER_ACTIVE <> 0) and
          (Flags and LF_VISIBLE <> 0) then
       begin
        WriteCellInfo(PByteArray(LayerData)[Offset * CellSize], DrawInfo);
        Result := True;
       end;

     if YInc >= 0 then
      Beam(0, -1, X, Y);
     if YInc <= 0 then
      Beam(0, +1, X, Y);
     if XInc >= 0 then
      Beam(-1, 0, X, Y);
     if XInc <= 0 then
      Beam(+1, 0, X, Y);
     Continue;
    end;
   end;
   Break;
  until False;
 end;
var
 Offset, J: Integer;
 Equals: Boolean;
 Ptr: Pointer;
begin
 Map := Frame.MapData;
 if Map <> nil then
  with Frame.MapEditView do
  begin
   Offset := TileY * MapWidth + TileX;

   if MultiSelected then
    SelBuf := SelectionBuffer else
    SelBuf := nil;

   if (SelBuf = nil) or PBoolean(Integer(SelBuf) + Offset)^ then
   begin
    SetLength(CellInfo, Map.Count);

    Equals := True;
    for J := 0 to Map.Count - 1 do
    begin
     with Map.Nodes[J] as TMapLayer do
     if (Flags and LF_LAYER_ACTIVE <> 0) and
        (Flags and LF_VISIBLE <> 0) then
     begin
      Ptr := Addr(PByteArray(LayerData)[Offset * CellSize]);
      ReadCellInfo(Ptr^, CellInfo[J]);
      WriteCellInfo(Ptr^, DrawInfo);
      ReadCellInfo(Ptr^, ReadInfo);

      with TCellInfoCmp(CellInfo[J]) do
       if (Test1 <> ric.Test1) or
          (Test2 <> ric.Test2) or
          (Test3 <> ric.Test3) then
        Equals := False;
     end;
    end;

    Result := False;
    if not Equals then
    begin
     Beam(0, -1, TileX, TileY);
     Beam(0, +1, TileX, TileY);
     Beam(-1, 0, TileX, TileY);
     Beam(+1, 0, TileX, TileY);

     Invalidate;
    end;
   end;
  end;
end;

procedure TMainForm.MapFrameMapEditViewCustomMouseActionCheck(
  Sender: TCustomTileMapView; Shift: TShiftState; var Act: Boolean);
var
 Frame: TMapFrame;
 MFrm: TMapsFrame;
 Event: THistoryEvent;
 dt: TDrawTool;
 RightButton: Boolean;
begin
 Frame := Sender.Owner as TMapFrame;
 MFrm := Frame.Owner as TMapsFrame;
 if (DrawTool = dtMagicWand) and
    (ssLeft in Shift) then
 begin
  Sender.PasteBufferApply;
  MapMagicWand(Frame, Shift);
  Act := False;
 end else
 if (DrawTool = dtFloodFill) and
    (ssLeft in Shift) then
 begin
  with Frame do
   if MapData <> nil then
   begin
    Sender.PasteBufferApply;
    Event := MFrm.AddMapEvent(MapData,
     WideFormat('%s ''%s'': Flood fill', [MapTypeStr[BrushMode], MapData.Name]));
    if MapFloodFill(Frame) then
    begin
     MFrm.SetMapEventData(Event, MapData, ruRedo);
         
     Saved := False;
     RefreshMapContent(Frame.MapData, [rmcView]);
    end else
     with FProject.History do
      Count := Position;
   end;
  Act := False;
 end else
 begin
  Act := (Frame.MapData <> nil) and
       not (DrawTool in [dtMultiSelect]) and
       ((ssLeft in Shift) or (ssRight in Shift));
  if Act then
  begin
   Sender.NewCursorPosition := True;
   case DrawTool of
    dtPencil,
    dtEraser,
    dtBrush:
    begin
     RightButton := ssRight in Shift;
     case DrawTool of
      dtPencil:
      if RightButton then
       dt := dtEraser else
      if ssAlt in Shift then
       dt := dtPicker else
       dt := dtPencil;

      dtBrush: if RightButton then
               dt := dtEraser else
               dt := dtBrush;

      else dt := dtEraser;
     end;
     Sender.PasteBufferApply;
     with Frame do
      Event := MFrm.AddMapEvent(MapData, WideFormat('%s ''%s'': %s',
             [MapTypeStr[BrushMode], MapData.Name, DrawToolNames[dt]]));
     if FDrawLine > 0 then
     begin
      with FClickPos do
      begin
       X := Sender.TileX;
       Y := Sender.TileY;
      end;

      FBackup := TMap.Create;
      with TMap(FBackup) do
      begin
       Assign(Frame.MapData);
       UserTag := Integer(Event);
      end;
     end else
      FBackup := Event;
    end;
   end;
  end;
 end;
end;

procedure TMainForm.MapNewActionExecute(Sender: TObject);
begin
//
end;

procedure TMainForm.EditModeActionExecute(Sender: TObject);
begin
 with Sender as TAction do
  if TEditMode(Tag) <> EditMode then
   EditMode := TEditMode(Tag);
end;

procedure TMainForm.ColorTableDeleteActionUpdate(Sender: TObject);
begin
 ColorTableDeleteAction.Enabled := (FProgressBar = nil) and
                              (FProject.FMaps.ColorTableList.Count > 0);
end;

procedure TMainForm.ColorTableDeleteActionExecute(Sender: TObject);
var
 I, Top: Integer;
 Node, Prv: TNode;
 Used: Boolean;
 Msg: WideString;
 List: TColorTableList;
 Event: THistoryEvent;
begin
 if FProgressBar = nil then
 begin
  List := FProject.FMaps.ColorTableList;
  with List do
   if Count > 0 then
   begin
    if ColorTablesListBox.SelCount = 0 then
     ColorTablesListBox.Selected[ColorTableIndex] := True;

    Used := False;
    for I := 0 to Count - 1 do
     if ColorTablesListBox.Selected[I] then
     begin
      Used := FProject.IsColorTableUsed(I);
      if Used then Break;
     end;

    if Used then
    begin
     if ColorTablesListBox.SelCount = 1 then
      Msg := 'This color table is in use. Delete it anyway?' else
      Msg := 'One or more of the selected color tables are in use.'#13#10 +
                     'Delete them anyway?';
     if WideMessageDlg(Msg, mtConfirmation, [mbYes, mbNo], 0) <> mrYes then
      Exit;
    end;

    ColorTableDeselect;

    if ColorTablesListBox.SelCount = 1 then
     Msg := WideFormat('Delete color table ''%s''',
           [Items[ColorTableIndex].Name]) else
     Msg := 'Delete color tables';

    if Used then
     ColorTableBeforeUnhook;
    Event := AddColorTableListEvent(Msg);
    try
     Top := -1;
     Node := LastNode;
     for I := Count - 1 downto 0 do
     begin
      Prv := Node.Prev;
      if ColorTablesListBox.Selected[I] then
      begin
       FProject.UnhookColorTable(Node as TColorTable);

       if Top < 0 then Top := I;

       if PaletteFrame.ColorTable = Node then
        PaletteFrame.ColorTable := nil;

       Remove(Node);
      end;
      Node := Prv;
     end;

     if Top >= Count then
      Dec(Top);

     if (Top < 0) and (Count > 0) then
      Top := 0;

     ColorTableIndex := Top;

     ColorTablesModified(True);
    finally
     Event.RedoData := List;
     if Used then
      ColorTableAfterUnhook;
    end;
   end;
 end;
end;

procedure TMainForm.DrawToolActionExecute(Sender: TObject);
begin
 DrawTool := TDrawTool((Sender as TAction).Tag);
end;
                       (*
procedure TMainForm.ShowTileProperties(TileSet: TTileSet;
           TileItem: TTileItem; const VFlags: Int64);
var
 I: Integer;
 Prop: TPropertyListItem;
 AttrPtr: PVariant;
 V: Variant;
 Int: Int64;
 Float: Double;
 Bool: Boolean;
begin
 if TileSet <> nil then
  with SelTilePropEditor.PropertyList do
  begin
   if (TileItem <> TileSet.EmptyTile) or (VFlags <> 0) then
    for I := 0 to TileSet.AttrCount - 1 do
     with TileSet.AttrDefs[I]^ do
     begin
      Prop := nil;
      if adType and varArray <> 0 then
      begin
       Prop := AddString(adName, GetArrayString(Addr(adSelf), TileItem.Attributes[I]));
      end else
      begin
       AttrPtr := Addr(TileItem.Attributes[I]);
       if adEnum = nil then
       begin
        if VarIsArray(AttrPtr^) then
         V := AttrPtr^[VarArrayLowBound(AttrPtr^, 1)] else
         V := AttrPtr^;
        case adType and varTypeMask of
         varSmallInt,
         varInteger,
         varShortInt,
         varByte,
         varWord,
         varLongWord,
         varInt64:
         begin
          try
           Int := V;
          except
           Int := 0;
          end;
          if adNumSys = nsHexadecimal then
           Prop := AddHexadecimal(adName, Int, adMin, adMax, 2) else
           Prop := AddDecimal(adName, Int, adMin, adMax);
         end;
         varBoolean:
         begin
          try
           Bool := V;
          except
           Bool := False;
          end;
          Prop := AddBoolean(adName, Bool);
         end;
         varSingle,
         varDouble,
         varCurrency:
         begin
          try
           Float := V;
          except
           Float := 0;
          end;
          Prop := AddFloat(adName, Float, adPrecision);
         end;
         varOleStr,
         varString:
          Prop := AddString(adName, V);
        end;
       end else
        Prop := AddPickList(adName, adEnum,
             FindEnumMember(adEnum, AttrPtr^));
      end;

      if Prop <> nil then
      begin
       if VFlags and (Int64(1) shl I) <> 0 then
        with Prop do
         Parameters := Parameters or PL_MULTIPLE_VALUE;
       Prop.UserTag := I;
      end;
     end;
  end;
end;
                    *)
procedure TMainForm.PropEditorEllipsisClick(Sender: TCustomPropertyEditor;
  Node: PVirtualNode; var Accepted: Boolean);
var Data: PPropertyData;
begin
 Data := Sender.GetNodeData(Node);
 With Data^ do If Item.Parameters = PL_BUFFER then
 begin
//  FillOpenDialog(OpenDialog);
//  Accepted := OpenDialog.Execute;
//  If Accepted then ValueStr := OpenDialog.FileName;
 end;
end;                  (*

procedure TMainForm.TileValueChange(Sender: TPropertyListItem);
var
 Item: TTileItem;
 X, Y, I, L, Idx: Integer;
 Rect: TRect;
 TempEnum: TWideStringArray;
 AttrPtr: ^Variant;
 MemberIdx: array of Integer;
 vtype: Word;
begin
 with TObject(Sender.Owner.UserTag) as TTilesFrame do
 begin
  if (TileSet <> nil) and
    (TileSetView.SomethingSelected) and
    (TileSetView.PasteBuffer = nil) then
  begin
  case Sender.UserTag of
  {  -1:
    begin
     with FProject.FTileSetInfo[TileSet.Index] do
     begin
      SelectedTile := Sender.Value;
      OffsetX := -2;
      OffsetY := -2;
     end;
     TileFocusChanged(TileSet);
     Exit;
    end;    }
    -2:
    if TileSetView.SelectionMode = selSingle then
    begin
     if Sender.PickListIndex = 0 then
     begin
      with TileSet do
      begin
       Item := AddFixed(SelectedTile);
       Move(EmptyTile.TileData^, Item.TileData^, TileSize);
       Item.FirstColor := EmptyTile.FirstColor;
      end;
      RefreshGraphicContent;
     end else
     begin
      with TileSet do
      begin
       Item := Indexed[SelectedTile];
       if Item <> EmptyTile then
        Remove(Item);
      end;
      (Sender.Next as TPropertyListItem).Value := TileSet.EmptyTile.FirstColor; 
      SelTilePropEditor.RootNodeCount := 3;
      RefreshGraphicContent;      
     end;
     Saved := False;
     Exit;
    end;
    -3:
    if TileSetView.SelectionMode = selSingle then
    begin
     Item := TileSet.Indexed[SelectedTile];
     Item.FirstColor := Sender.Value;
     if TileSet.DrawFlags and DRAW_PIXEL_MODIFY <> 0 then
      PaletteFrame.SelectColorRange(Item.FirstColor,
                                  1 shl TileSet.TileBitmap.BitsCount) else
      PaletteFrame.SelectColorRange(-1, 0);
     TileSetView.ScrollToSelectedCell;
     RefreshGraphicContent;
     Saved := False;
     Exit;
    end;
    else
    if TileSet.AttrDefs[Sender.UserTag].adType and varArray <> 0 then
    try
     GetEnumListFromString(Sender.ValueStr, TempEnum);
    except
     WideMessageDlg('Invalid array string', mtError, [mbOk], 0);
     PostMessage(Self.Handle, WM_FILLTILEPROPS, Integer(Sender.Owner.UserTag), 0);
     Exit;
    end;
   end;
   Rect := TileSetView.SelectionRect;
   Rect.Left := Max(Rect.Left, 0);
   Rect.Right := Min(TileSetView.MapWidth, Rect.Right);
   Rect.Top := Max(Rect.Top, 0);
   Rect.Bottom := Min(TileSetView.MapHeight, Rect.Bottom);
   if (Rect.Right - Rect.Left > 0) and
      (Rect.Bottom - Rect.Top > 0) then
   begin
    for Y := Rect.Top to Rect.Bottom - 1 do
    begin
     Idx := Y * TileSetView.MapWidth + Rect.Left;
     for X := Rect.Left to Rect.Right - 1 do
     begin
      if TileSetView.Selected[X, Y] then
      begin
       case Sender.UserTag of
        -2:
        with TileSet do
        begin
         if Sender.PickListIndex = 0 then
         begin
          Item := AddFixed(Idx);
          Move(EmptyTile.TileData^, Item.TileData^, TileSize);
          Item.FirstColor := EmptyTile.FirstColor;
         { with Sender.Owner as TPropertyList do
           if Count > 3 then
            PostMessage(Self.Handle, WM_FILLTILEPROPS, Integer(UserTag), 0);}
           { for I := 0 to TileSet.AttrCount - 1 do
             with Properties[I + 3] do
              case ValueType of
               vtDecimal, vtHexadecimal:
                if Value <> Item.Attributes[I] then
                 Parameters := Parameters or PL_MULTIPLE_VALUE;
               vtString:
                if ValueStr <> Item.Attributes[I] then
                 Parameters := Parameters or PL_MULTIPLE_VALUE;
               vtPickString:
               if Parameters and PL_FIXEDPICK <> 0 then
               begin
                if PickListIndex <> Item.Attributes[I] then
                 Parameters := Parameters or PL_MULTIPLE_VALUE;
               end else
               begin
                if ValueStr <> Item.Attributes[I] then
                 Parameters := Parameters or PL_MULTIPLE_VALUE;
               end;
               vtFloat:
                if Float <> Item.Attributes[I] then
                 Parameters := Parameters or PL_MULTIPLE_VALUE;
              end;   }
         end else
         begin
          Item := TileSet.Indexed[Idx];
          if Item <> EmptyTile then
           Remove(Item);
         end;
        end;
        -3: TileSet.Indexed[Idx].FirstColor := Sender.Value;
        else
        begin
         Item := TileSet.Indexed[Idx];
         if Item <> TileSet.EmptyTile then
         case Sender.ValueType of
          vtDecimal, vtHexadecimal:
           Item.Attributes[Sender.UserTag] := Sender.Value;
          vtString:
          with TileSet.AttrDefs[Sender.UserTag]^ do
          begin
           if adType and varArray <> 0 then
           begin
            L := Min(adMaxArrayLen, Length(TempEnum));

            if adEnum <> nil then
            begin
             SetLength(MemberIdx, L);
             for I := 0 to L - 1 do
             begin
              Idx := FindEnumMember(TempEnum[I], adEnum);
              if Idx < 0 then
               Exit else
               MemberIdx[I] := Idx;
             end;
            end;

            AttrPtr := Addr(Item.Attributes[Sender.UserTag]);

            vtype := adType and varTypeMask;
            if not VarTypeIsValidArrayType(vtype) then
             vtype := varVariant;

            AttrPtr^ := VarArrayCreate([0, L - 1], vtype);

            if adEnum <> nil then
            begin
             for I := 0 to L - 1 do
              AttrPtr^[I] := MemberIdx[I];
            end else
            begin
             for I := 0 to L - 1 do
              AttrPtr^[I] := TempEnum[I];
            end;

            Sender.Changing := True;
            Sender.ValueStr := GetArrayString(Addr(adSelf), AttrPtr^);
            Sender.Changing := False;
           end else
            Item.Attributes[Sender.UserTag] := Sender.ValueStr;
          end;
          vtPickString:
          if Sender.Parameters and PL_FIXEDPICK <> 0 then
           Item.Attributes[Sender.UserTag] := Sender.PickListIndex else
           Item.Attributes[Sender.UserTag] := Sender.ValueStr;
          vtFloat:
           Item.Attributes[Sender.UserTag] := Sender.Float;
         end;
        end;
       end;
      end;
      Inc(Idx);
     end;
    end;
    case Sender.UserTag of
     -2, -3:
     begin
      RefreshGraphicContent;
      PostMessage(Self.Handle, WM_FILLTILEPROPS, Integer(Sender.Owner.UserTag), 0);
     end;
    { -3:
     begin
      if TileSet.DrawFlags and DRAW_PIXEL_MODIFY <> 0 then
       PaletteFrame.SelectColorRange(TileSet.Indexed[SelectedTile].FirstColor,
                                  1 shl TileSet.TileBitmap.BitsCount) else
       PaletteFrame.SelectColorRange(-1, 0);
                                         
      RefreshGraphicContent;
     end;   }
    end;
    Saved := False;
   end;
  end;
 end;
end;
                    *)
const
 OrderList: array[0..1] of WideString = ('Left to right', 'Right to left');

procedure FillImageFormatProperties(List: TPropertyList;
            ImageFlags: Integer; AReadOnly: Boolean);
begin
 with List do
 begin
  with AddDecimal('Bits per unit', (ImageFlags and 31) + 1, 1, 32) do
  begin
   UserTag := -1;
   ReadOnly := AReadOnly;
   if ImageFlags < 0 then
    Parameters := PL_MULTIPLE_VALUE;
  end;
  with AddPickList('Pixel bits order', OrderList,
             Integer(ImageFlags and IMG_BITS_REVERSE <> 0)) do
  begin
   UserTag := -1;
   ReadOnly := AReadOnly;
   if ImageFlags < 0 then
    Parameters := Parameters or PL_MULTIPLE_VALUE;
  end;
  with AddBoolean('Swap pixel bytes', ImageFlags and IMG_PIXEL_BSWAP <> 0) do
  begin
   UserTag := -1;
   ReadOnly := AReadOnly;
   if ImageFlags < 0 then
    Parameters := Parameters or PL_MULTIPLE_VALUE;
  end;
  with AddBoolean('Vertical flip', ImageFlags and IMG_Y_FLIP <> 0) do
  begin
   UserTag := -1;
   ReadOnly := AReadOnly;
   if ImageFlags < 0 then
    Parameters := Parameters or PL_MULTIPLE_VALUE;
  end;
  with AddBoolean('Horizontal flip', ImageFlags and IMG_X_FLIP <> 0) do
  begin
   UserTag := -1;
   ReadOnly := AReadOnly;
   if ImageFlags < 0 then
    Parameters := Parameters or PL_MULTIPLE_VALUE;
  end;
  with AddBoolean('Hor. flip 1..4', ImageFlags and IMG_X_FLIP4 = IMG_X_FLIP4) do
  begin
   UserTag := -1;
   ReadOnly := AReadOnly;
   if ImageFlags < 0 then
    Parameters := Parameters or PL_MULTIPLE_VALUE;
  end;
  with AddBoolean('Planar', ImageFlags and IMG_PLANAR <> 0) do
  begin
   UserTag := -1;
   ReadOnly := AReadOnly;
   if ImageFlags < 0 then
    Parameters := Parameters or PL_MULTIPLE_VALUE;
  end;
  with AddPickList('Plane bit capacity', ['1-bit', '2-bit', '4-bit', '8-bit'],
              ImageFlags shr PLANE_FORMAT_SHIFT) do
  begin
   UserTag := -1;
   ReadOnly := AReadOnly;
   if ImageFlags < 0 then
    Parameters := Parameters or PL_MULTIPLE_VALUE;
  end;
  with AddPickList('Plane order', OrderList,
              Integer(ImageFlags and IMG_PLANE_REVERSE <> 0)) do
  begin
   UserTag := -1;
   ReadOnly := AReadOnly;
   if ImageFlags < 0 then
    Parameters := Parameters or PL_MULTIPLE_VALUE;
  end;
  with AddPickList('Plane format', ['Matrix', 'Vector'],
             Integer(ImageFlags and IMG_PLANE_VECTOR <> 0)) do
  begin
   UserTag := -1;  
   ReadOnly := AReadOnly;
   if ImageFlags < 0 then
    Parameters := Parameters or PL_MULTIPLE_VALUE;
  end;
 end;
end;
           (*
procedure TMainForm.TileSetBaseValuesChange(Sender: TPropertyListItem);
var
 Sz: Int64;
 Diff: Integer;
// XX, YY: Integer;
 Flg: TCompositeFlags;
begin
 with TilesFrame.TileSet do
 begin
  case Sender.UserTag of
   1:
   begin
    Name := Sender.ValueStr;
    Sender.Changing := True;
    Sender.ValueStr := Name;
    Sender.Changing := False;
    TileSetsListBox.Items[Index] := WideFormat('%d. %s', [Index + 1, Name]);
    TileSetsListBox.ItemIndex := Index;
    FillLayerProps(TObject(MapPropEditor.PropertyList.UserTag) as TMap);
    Saved := False;
   end;
   2:
   with TilesFrame.TileSet do
   begin
    Sz := Sender.Value * TileSize;
    if (Sz <= 8 * 1024 * 1024) or
     (WideMessageDlg(
      WideFormat('It will take about %f MB of memory. Proceed?',
                 [(Sz + Sender.Value * 64) / (1024 * 1024)]),
                 mtConfirmation, [mbYes, mbNo], 0) = mrYes) then
    begin
     TilesCount := Sender.Value;
     with FProject.FTileSetInfo[Index] do
     begin
      SelectedTile := LastIndex;
      OffsetX := -2;
      OffsetY := -2;
     end;
     Saved := False;
     TileContentModified;
    end else
    begin
     Sender.Changing := True;
     Sender.Value := TilesCount;
     Sender.Changing := False;
    end;
   end;
   3:
   begin
    FProject.FTileSetInfo[Index].Width := Sender.Value;
    Saved := False;
    ContentRedraw(crTileFocus);
   end;
   4..7:
   with TilesFrame.TileSet do
   begin
    case Sender.UserTag of
     4: DrawFlags := (DrawFlags and not DRAW_PIXEL_MODIFY) or
                 ((not Sender.PickListIndex + 1) and DRAW_PIXEL_MODIFY);
     5: SwitchFormat([], Sender.Value);
     6: SwitchFormat([], -1, Sender.Value);
     7:
     begin
      Diff := Sender.Value - (Length(TileBitmap.CompositeFlags) + 1);
      if (Diff > 0) and (TileBitmap.BitsCount + Diff > 8) then
      begin
       WideMessageDlg('Cannot add another composite because direct color is unsupported.', mtError, [mbOk], 0);
       Sender.Changing := True;
       Sender.Value := Length(TileBitmap.CompositeFlags) + 1;
       Sender.Changing := False;
       Exit;
      end else
      if Diff <> 0 then
      begin
       Flg := TileBitmap.FlagsList;
       SetLength(Flg, Sender.Value);
       SwitchFormat(Flg);
      end;
     end;
    end;
    Saved := False;
    TileContentModified;
   end;
   8:
   with TilesFrame.TileSet do
   begin
    AttrCount := Sender.Value;
    TilesFrameTileSetViewSelectionChanged(TilesFrame.TileSetView);
    ContentRedraw(crTileFocus);
   end;
  end;
 end;
end;

procedure TMainForm.TilesFrameTileSetChange(Sender: TTilesFrame);
var
 I, J: Integer;
begin
 with TileSetPropEditor, PropertyList do
 begin
  ClearList;
  if Sender.TileSet <> nil then with Sender.TileSet do
  begin
   AddString('Name', Name).UserTag := 1;
   AddDecimal('Tiles count', TilesCount, 0, 1000000).UserTag := 2;
   AddDecimal('Tiles in a row', FProject.FTileSetInfo[Index].Width, 1, 256).UserTag := 3;
  { with AddHexadecimal('Draw flags', DrawFlags, 0, 65535, 4).SubProperties do
   begin
    ItemOwner.ReadOnly := True;}
   AddBoolean('Use 1st color index', DrawFlags and DRAW_PIXEL_MODIFY <> 0).UserTag := 4;
//   end;
   with AddData('Tile format', '(Convert)').SubProperties do
   begin
    AddDecimal('Tile width', TileWidth, 1, 256).UserTag := 5;
    AddDecimal('Tile height', TileHeight, 1, 256).UserTag := 6;;
    AddDecimal('Bits per pixel', BitCount, 1, 32).ReadOnly := True;
    with TileBitmap do
    begin
     AddDecimal('Composite count', Length(CompositeFlags) + 1, 1, 32).UserTag := 7;

     with AddHexadecimal('Composite 1', ImageFlags, 0, 65535, 4) do
     begin
      UserTag := 10;
      ReadOnly := False;
      OnValueChange := TileSetImageFlagsValueChange;
      SubProperties.OnValueChange := TileSetImageFlagsChange;
      FillImageFormatProperties(SubProperties, ImageFlags, False);
     end;

    // Parameters := PL_EDITABLE_LIST or PL_PROP_CHILD_MOVABLE;
     for I := 0 to Length(CompositeFlags) - 1 do
     begin
      with AddHexadecimal(WideFormat('Composite %d', [I + 2]),
                          CompositeFlags[I], 0, 65535, 4) do
      begin
       ReadOnly := False;
       UserTag := I + 11;
       OnValueChange := TileSetImageFlagsValueChange;
       SubProperties.OnValueChange := TileSetImageFlagsChange;
       FillImageFormatProperties(SubProperties, CompositeFlags[I], False);
      end;
     end;
    end;
   end;
   with AddDecimal('Tile attr. definitions', AttrCount, 0, 64) do
   begin
    UserTag := 8;
    Parameters := PL_BUFFER or PL_EDIT_PROP_CHILDREN;
  //  UserTag := 1;
    for I := 0 to AttrCount - 1 do with AttrDefs[I]^ do
    begin
     case adType of
      varSingle:   J := 10;
      varDouble:   J := 9;
      varByte:     J := 8;
      varShortInt: J := 7;
      varWord:     J := 6;
      varSmallInt: J := 5;
      varInteger:  J := 4;
      varLongWord: J := 3;
      varInt64:    J := 2;
      varBoolean:  J := 1;
      else         J := 0;
     end;
     SubProperties.AddPickList(adName, FVarTypesList, True, J).UserTag := 9;
    end;
    {for I := 0 to AttrCount - 1 do with AttrDefs[I]^ do
   begin
    Prop := nil;
    if adFlags and ATTR_LIST <> 0 then
    begin
  //    AddPickList(adName,
  //    if
   //  L := VarArrayLowBound(Attributes[I], 1);
   //  H := VarArrayHighBound(Attributes[I], 1);
   //  for J := 0 to H - L do
   //   List[J] := VarToWideStrDef(Attributes[I][J + L], 'NULL');
   //  AddPickList(adName, Attributes[I][L + adIndex], List);
    end else
    case adType of
     varSmallInt,
     varInteger,
     varShortInt,
     varByte,
     varWord,
     varLongWord,
     varInt64: if adNumSys = nsHexadecimal then
                Prop := AddHexadecimal(adName, Attributes[I], adMin, adMax, 0) else
                Prop := AddDecimal(adName, Attributes[I], adMin, adMax);
     varBoolean: Prop := AddBoolean(adName, Attributes[I]);
     varSingle,
     varDouble,
     varCurrency: Prop := AddFloat(adName, Attributes[I], adPrecision);
     varOleStr,
     varString: Prop := AddString(adName, Attributes[I]);
    end;}
   end;
  {    property TileWidth: Integer read GetTileWidth;
    property TileHeight: Integer read GetTileHeight;
    property TileSize: Integer read GetTileSize;
    property TileBitmap: TBitmapContainer read FTileBitmap;
    property BitCount: Integer read GetBitCount;}

{
   with AddHexadecimal('Optimization flags', OptimizationFlags, 0, 65535, 4).SubProperties do
   begin
    AddBoolean('Find mirroring', OptimizationFlags and OPT_FIND_MIRRORING <> 0);
    AddBoolean('Check colors', OptimizationFlags and OPT_CHECK_COLORS <> 0);
   end;}
  // SelectedItem := FindProperty(LastRoute);
  // if SelectedItem = nil then SelectedItem := Properties[0];
  end;
  RootNodeCount := Count;
//  ReinitChildren(nil, True);
//  Invalidate;
 end;
end;

function FillTileSetFlags(Sender: TPropertyListItem;
                 TileSet: TTileSet; var Flags: TCompositeFlags): Boolean;
var
 I, BCnt, Idx: Integer;
 Value: Word;
begin
 Result := False;
 with TileSet.TileBitmap do
 begin
  Idx := Sender.ItemOwner.UserTag - 10;
  case Sender.ItemOwner.UserTag of
   10:     Value := ImageFlags;
   11..41: Value := CompositeFlags[Idx - 1];
   else    Exit;
  end;
  Result := True;
  Flags := FlagsList;
  case Sender.Index of
   0:
   begin
    Flags[Idx] := Sender.Value - 1;
    
    BCnt := Length(Flags);
    for I := 0 to Length(Flags) - 1 do
     Inc(BCnt, Flags[I] and 31);
     
    if BCnt > 8 then
    begin
     WideMessageDlg('Direct color is unsupported.', mtError, [mbOk], 0);
     Result := False;
     Sender.Changing := True;
     Sender.Value := Value and 31 + 1;
     Sender.Changing := False;
     Exit;
    end;
    Value := (Value and not 31) or ((Sender.Value - 1) and 31);
    BCnt := Value shr PLANE_FORMAT_SHIFT;
    while 1 shl BCnt > Sender.Value do
     Dec(BCnt);
    Value := (Value and PLANE_FORMAT_MASK) or
             (BCnt shl PLANE_FORMAT_SHIFT);
   end;
   1: if Sender.PickListIndex = 0 then
       Value := Value and not IMG_BITS_REVERSE else
       Value := Value or IMG_BITS_REVERSE;
   2: if Sender.PickListIndex = 0 then
       Value := Value and not IMG_PIXEL_BSWAP else
       Value := Value or IMG_PIXEL_BSWAP;
   3: if Sender.PickListIndex = 0 then
       Value := Value and not IMG_Y_FLIP else
       Value := Value or IMG_Y_FLIP;
   4: if Sender.PickListIndex = 0 then
       Value := Value and not IMG_X_FLIP else
       Value := Value or IMG_X_FLIP;
   5: if Sender.PickListIndex = 0 then
       Value := Value and not IMG_X_FLIP4 else
       Value := Value or IMG_X_FLIP4;
   6: if Sender.PickListIndex = 0 then
       Value := Value and not IMG_PLANAR else
       Value := Value or IMG_PLANAR;
   7:
   begin
    BCnt := 1 shl Sender.PickListIndex;
    if BCnt > Value and 31 + 1 then
    begin
     WideMessageDlg('Cannot be higher than bits per unit.', mtError, [mbOk], 0);
     Result := False;
     Sender.PickListIndex := Value shr PLANE_FORMAT_SHIFT;
     Exit;
    end;
    Value := (Value and PLANE_FORMAT_MASK) or
             (Sender.PickListIndex shl PLANE_FORMAT_SHIFT);
   end;
   8: if Sender.PickListIndex = 0 then
       Value := Value and not IMG_PLANE_REVERSE else
       Value := Value or IMG_PLANE_REVERSE;
   9: if Sender.PickListIndex = 0 then
       Value := Value and not IMG_PLANE_VECTOR else
       Value := Value or IMG_PLANE_VECTOR;
  end;
  Flags[Idx] := Value;
 end;
end;

procedure TMainForm.TileSetImageFlagsChange(Sender: TPropertyListItem);
var
 Flags: TCompositeFlags;
begin
 if FillTileSetFlags(Sender, TilesFrame.TileSet, Flags) then
 begin
  TilesFrame.TileSet.SwitchFormat(Flags);
 // with TilesFrame.TileSet do Reallocate(Flags);
  Saved := False;
  TileContentModified;
 end;
end;     *)

procedure TMainForm.MapModified;
var
 I: Integer;
begin
 for I := 0 to Length(MapFramesList) - 1 do
  MapFramesList[I].MapFrame.MapModified := True;

 Saved := False;  
end;

procedure TMainForm.TileFormatModified;
var
 I: Integer;
 Save: TTileItem;
begin
 for I := 0 to Length(MapFramesList) - 1 do
  with MapFramesList[I] do
  begin
   MapFrame.MapModified := True;
   LayersListBox.Invalidate;
  end;

 if MainFrame is TTileEditFrame then
  with TTileEditFrame(MainFrame) do
  begin
   TileEditView.Deselect(False);
   Save := Tile;
   Tile := nil;
   Tile := Save;
  end;

 Saved := False;  
end;

procedure TMainForm.TileFocusChanged(TileSet: TTileSet);
var
 I: Integer;
begin
 for I := 0 to Length(TileSetFramesList) - 1 do
  with TileSetFramesList[I] do
   if TilesFrame.TileSet = TileSet then
    TileSetChanged;
end;
                    (*
procedure TMainForm.TileSetPropEditorPropertyRemove(
  Sender: TCustomPropertyEditor; Node: PVirtualNode; var Yes: Boolean);
var
 Data: PPropertyData;
 Idx, L: Integer;
 Flg: TCompositeFlags;
begin
 Data := Sender.GetNodeData(Node);
 if (Data <> nil) and (Data.Owner <> nil) then
 case Data.Item.UserTag of
  9:
  begin
   TilesFrame.TileSet.DeleteAttr(Data.Item.Index);
   TilesFrameTileSetViewSelectionChanged(TilesFrame.TileSetView);
   Data := Sender.GetNodeData(Node.Parent);
   if (Data <> nil) and (Data.Owner <> nil) then
   with Data.Item do
   begin
    Changing := True;
    Value := Value - 1;
    Changing := False;
   end;
   Sender.InvalidateNode(Node.Parent);
   Saved := False;
  end;
  10..41:
  with TilesFrame.TileSet do
  begin
   Flg := TileBitmap.FlagsList;
   Idx := Data.Item.UserTag - 10;
   L := Length(Flg) - 1; 
   if Idx < L then
    Move(Flg[Idx + 1], Flg[Idx], (L - Idx) shl 1);
   SetLength(Flg, L); 
   SwitchFormat(Flg);
   Sender.InvalidateNode(Node.Parent);
   Saved := False;
   TileContentModified;
  end;
  else Yes := False;
 end;

end;
           {
procedure TMainForm.TileSetPropEditorPlusClick(
  Sender: TCustomPropertyEditor; Node: PVirtualNode;
  var Accepted: Boolean);
var
 Data: PPropertyData;
begin
 Data := Sender.GetNodeData(Node);
 with Data.Item do
 begin
  Accepted := Value < ValueMax;
  if Accepted then
  begin
   Value := Value + 1;
   Sender.InvalidateNode(Node);
   Saved := False;
  end;
 end;
end;  }

procedure TMainForm.TileSetPropEditorSetPropertyName(
  Sender: TCustomPropertyEditor; Node: PVirtualNode;
  const Text: WideString);
var
 Data: PPropertyData;
begin
 Data := Sender.GetNodeData(Node);
 if Data.Item.UserTag = 9 then
 begin
  TilesFrame.TileSet.AttrDefs[Data.Item.Index].adName := Text;
  Saved := False;
  TilesFrameTileSetViewSelectionChanged(TilesFrame.TileSetView);
 end;
end;

function TMainForm.SelTileIsEmpty: Boolean;
var
 Tile: TTileItem;
begin
 Result := TilesFrame.TileSet <> nil;
 if Result then
 begin
  Tile := TilesFrame.TileSet.Indexed[TilesFrame.SelectedTile];
  Result := (Tile = nil) or (Tile = TilesFrame.TileSet.EmptyTile);
 end;
end;

procedure TMainForm.TileSetPropEditorSetValue(
  Sender: TCustomPropertyEditor; Node: PVirtualNode;
  const Text: WideString);
const
 TypeList: array[0..10] of packed
 record
  typ: Word;
  pre: Byte;
  sys: TNumerationSystem;
  len: LongInt;
  min: Int64;
  max: Int64;
 end =
 ((typ: varOleStr;   pre:  0; sys: nsDecimal; min: 0; max: 0), // 0
  (typ: varBoolean;  pre:  0; sys: nsDecimal; min: 0; max: 0), // 1
  (typ: varInt64;    pre:  0; sys: nsDecimal; min: Low(Int64); max: High(Int64)), // 2
  (typ: varLongWord; pre:  0; sys: nsDecimal; min: 0; max: High(LongWord)), // 3
  (typ: varInteger;  pre:  0; sys: nsDecimal; min: Low(Integer); max: MaxInt), // 4
  (typ: varSmallInt; pre:  0; sys: nsDecimal; min: Low(SmallInt); max: High(SmallInt)), // 5
  (typ: varWord;     pre:  0; sys: nsDecimal; min: 0; max: 65535), // 6
  (typ: varShortInt; pre:  0; sys: nsDecimal; min: -128; max: 127), // 7
  (typ: varByte;     pre:  0; sys: nsDecimal; min: 0; max: 255), // 8
  (typ: varDouble;   pre: 15; sys: nsDecimal; min: 0; max: 0), // 9
  (typ: varSingle;   pre:  7; sys: nsDecimal; min: 0; max: 0));// 10
var
 I: Integer;
 Data: PPropertyData;
begin
 Data := Sender.GetNodeData(Node);
 if Data.Item.UserTag = 9 then
 begin
  I := FVarTypesList.IndexOf(Text);
  if I >= 0 then
  begin

   with TilesFrame.TileSet.AttrDefs[Data.Item.Index]^, TypeList[I] do
   begin
    adType := typ;
    case typ of
     varInt64,
     varLongWord,
     varInteger,
     varSmallInt,
     varWord,
     varShortInt,
     varByte:
     begin
      adMin := min;
      adMax := max;
      adNumSys := sys;
     end;
     varDouble,
     varSingle: adPrecision := pre;
     else adMaxLength := 0;
    end;
   end;
   Saved := False;
   TilesFrameTileSetViewSelectionChanged(TilesFrame.TileSetView);
  end;
 end;
end;

procedure TMainForm.Splitter1CanResize(Sender: TObject;
  var NewSize: Integer; var Accept: Boolean);
begin
 NewSize := Max(NewSize, LayerPropEditor.Constraints.MinHeight + LayersLabel.Height);
end;

procedure TMainForm.TileSetsListBoxChange(Sender: TObject);
begin
 FProject.FInfo.SelectedTileSetIndex := TileSetsListBox.ItemIndex;
 TilesBoxUpdate;
end;      *)

procedure TMainForm.SetEditMode(Value: TEditMode);

 function CreateMapsFrame: TMapsFrame;
 begin
  Result := TMapsFrame.Create(Self);
  with Result do
  begin
   Parent := MainEditPanel;
   Align := alClient;
   PageControl.TabPosition := tpLeft;
   MapsToolBar.Images := ImgList;
   LayersToolBar.Images := ImgList;
   with MapsListBox do
   begin
    OnContextPopup := MapsFrameMapsListBoxContextPopup;
    OnEnter := MapsFrameListBoxEnter;
    OnExit := MapsFrameListBoxExit;
    PopupMenu := MapsPopup;
   end;
   with LayersListBox do
   begin
    OnContextPopup := MapsFrameLayersListBoxContextPopup;
    OnEnter := MapsFrameListBoxEnter;
    OnExit := MapsFrameListBoxExit;
    PopupMenu := LayersPopup;
   end;
   with MapFrame.MapEditView do
   begin
    OnContentsChanged := MapFrameMapEditViewContentsChanged;
    OnCustomMouseAction := MapFrameMapEditViewCustomMouseAction;
    OnCustomMouseActionCheck := MapFrameMapEditViewCustomMouseActionCheck;
    OnCustomMouseActionFinish := MapFrameMapEditViewCustomMouseActionFinish;
    OnEnter := MapFrameMapEditViewEnter;
    OnExit := MapFrameMapEditViewExit;
    OnMouseEnter := MagicWandMouseEnter;
    OnMouseLeave := MagicWandMouseLeave;
    OnMouseMove := MapFrameMapEditViewMouseMove;
    OnMouseUp := MapFrameMapEditViewMouseUp;
    OnSelectionChanged := MapFrameMapEditViewSelectionChanged;
    OnSysKeyDown := MagicWandSysKeyDown;
    OnSysKeyUp := MagicWandSysKeyUp;
    PopupMenu := MapPopup;
   end;
//   PageControl.ActivePageIndex := 1;
  end;
  SetLength(MapFramesList, 3);
  MapFramesList[2] := Result;
  SetLength(TileSetFramesList, 1);
  InitMapsFrameEvents(Result);
 end;

 procedure MainFrameClear;
 var
  I: Integer;
  TilesFrm: TTileSetsFrame;
  MapsFrm: TMapsFrame;
  Nothing: Boolean;
  Test: TFrame;
 begin
  Test := TObject(TilePropsFrm.SelTilePropEditor.PropertyList.UserTag) as TTilesFrame;
  if (Test <> nil) and
     (Test.Owner = MainFrame) then
  begin
   Nothing := True;
   for I := 0 to Length(TileSetFramesList) - 1 do
   begin
    TilesFrm := TileSetFramesList[I];
    if (TilesFrm <> MainFrame) and
       (TilesFrm.PageControl.ActivePageIndex <> 0) and
       (TilesFrm.TilesFrame.TileSet = TTilesFrame(Test).TileSet) then
    begin
     TilesFrm.TilesFrame.TileSetView.SelectionChanged;
     Nothing := False;
     Break;
    end;
   end;
   if Nothing then
    for I := 0 to Length(TileSetFramesList) - 1 do
    begin
     TilesFrm := TileSetFramesList[I];
     if (TilesFrm <> MainFrame) and
        (TilesFrm.PageControl.ActivePageIndex <> 0) then
     begin
      TilesFrm.TilesFrame.TileSetView.SelectionChanged;
      Nothing := False;      
      Break;
     end;
    end;
   if Nothing then
    PostMessage(TilePropsFrm.Handle, WM_FILLTILEPROPS, 0, 0);
  end;

  if (FFocusedTilesFrame <> nil) and
     (FFocusedTilesFrame.Owner = MainFrame) then
   FFocusedTilesFrame := nil;

  if FFocusedTileSetsFrame = MainFrame then
    FFocusedTileSetsFrame := nil;

  Test := TObject(SelCellPropEditor.PropertyList.UserTag) as TMapFrame;

  if (Test <> nil) and
     (Test.Owner = MainFrame) then
  begin
   Nothing := True;   // try to select the same map cell
   for I := 0 to Length(MapFramesList) - 1 do
   begin
    MapsFrm := MapFramesList[I];
    if (MapsFrm <> MainFrame) and
       (MapsFrm.PageControl.ActivePageIndex <> 0) and
       (MapsFrm.MapFrame.MapData = TMapFrame(Test).MapData) then
    begin
     MapsFrm.MapFrame.MapEditView.SelectionChanged;
     Nothing := False;
     Break;
    end;
   end;
   if Nothing then  // try to select the same type map cell
    for I := 0 to Length(MapFramesList) - 1 do
    begin
     MapsFrm := MapFramesList[I];
     if (MapsFrm <> MainFrame) and
        (MapsFrm.BrushMode = TMapsFrame(MainFrame).BrushMode) and
        (MapsFrm.PageControl.ActivePageIndex <> 0) then
     begin
      MapsFrm.MapFrame.MapEditView.SelectionChanged;
      Nothing := False;
      Break;
     end;
    end;
   if Nothing then // try to select any map cell
    for I := 0 to Length(MapFramesList) - 1 do
    begin
     MapsFrm := MapFramesList[I];
     if (MapsFrm <> MainFrame) and
        (MapsFrm.PageControl.ActivePageIndex <> 0) then
     begin
      MapsFrm.MapFrame.MapEditView.SelectionChanged;
      Nothing := False;
      Break;
     end;
    end;
   if Nothing then
    PostMessage(Self.Handle, WM_FILLCELLPROPS, 0, 0);     
  end;

  if (FFocusedMapFrame <> nil) and
     (FFocusedMapFrame.Owner = MainFrame) then
   FFocusedMapFrame := nil;

  if FFocusedMapsFrame = MainFrame then
   FFocusedMapsFrame := nil;

  if MainFrame is TMapsFrame then
   TMapsFrame(MainFrame).MapFrame.MapEditView.OnSelectionChanged := nil else
  if MainFrame is TTileSetsFrame then
   TTileSetsFrame(MainFrame).TilesFrame.TileSetView.OnSelectionChanged := nil;

  Application.ProcessMessages;
  FreeAndNil(MainFrame);
 end;

var
 RepaintControl: Boolean;
 SrcView: TCustomTileMapView;
begin
 RepaintControl := False;
 try
  FProject.FInfo.EditMode := Value;
  case Value of
   emMap:
   begin
    MapEditModeAction.Checked := True;
    if not (MainFrame is TMapsFrame) or
              TMapsFrame(MainFrame).BrushMode then
    begin
     MainEditPanel.Perform(WM_SETREDRAW, 0, 0);
     RepaintControl := True;     
     MainFrameClear;

     MainFrame := CreateMapsFrame;
    end;
    with TMapsFrame(MainFrame) do
    begin
     SetProject(FProject, False, 0);
     SrcView := MapsFrm.MapFrame.MapEditView;
     with MapFrame.MapEditView do
     begin
      SelectionMode := SrcView.SelectionMode;
      SelectionEnabled := SrcView.SelectionEnabled;
      DefaultCursor := SrcView.DefaultCursor;
      CanPopup := SrcView.CanPopup;
     end;
    end;
   end;
   emBrush:
   begin
    BrushEditModeAction.Checked := True;
    if not (MainFrame is TMapsFrame) or
                not TMapsFrame(MainFrame).BrushMode then
    begin
     MainEditPanel.Perform(WM_SETREDRAW, 0, 0);
     RepaintControl := True;
     MainFrameClear;

     MainFrame := CreateMapsFrame;
    end;
    with TMapsFrame(MainFrame) do
    begin
     SetProject(FProject, True, 0);
     SrcView := BrushesFrame.MapFrame.MapEditView;
     with MapFrame.MapEditView do
     begin
      SelectionMode := SrcView.SelectionMode;
      SelectionEnabled := SrcView.SelectionEnabled;
      DefaultCursor := SrcView.DefaultCursor;
      CanPopup := SrcView.CanPopup;
     end;
    end;
   end;
   emTileSet:
   begin
    TileSetEditModeAction.Checked := True;
    if not (MainFrame is TTileSetsFrame) then
    begin
     MainEditPanel.Perform(WM_SETREDRAW, 0, 0);
     RepaintControl := True;
     MainFrameClear;

     MainFrame := TTileSetsFrame.Create(Self);
     with TTileSetsFrame(MainFrame) do
     begin
      Parent := MainEditPanel;
      Align := alClient;
      PageControl.TabPosition := tpLeft;
      TileSetsToolBar.Images := ImgList;
      AttrToolBar.Images := ImgList;
      with TileSetsListBox do
      begin
       OnContextPopup := TilesFrameTileSetsListBoxContextPopup;
       OnEnter := TilesFrameTileSetsListBoxEnter;
       OnExit := TilesFrameTileSetsListBoxExit;
       PopupMenu := TileSetsPopup;
      end;
      with TilesFrame.TileSetView do
      begin
       OnBeforeContentChange := TilesFrameTileSetViewBeforeContentChange;
       OnContentsChanged := TilesFrameTileSetViewContentsChanged;
       OnContextPopup := TilesFrameTileSetViewContextPopup;
       OnCustomMouseAction := TilesFrameTileSetViewCustomMouseAction;
       OnCustomMouseActionCheck := TilesFrameTileSetViewCustomMouseActionCheck;
       OnEnter := TilesFrameTileSetViewEnter;
       OnExit := TilesFrameTileSetViewExit;
       OnSelectionChanged := TilesFrameTileSetViewSelectionChanged;
       OnMouseEnter := MagicWandMouseEnter;
       OnMouseLeave := MagicWandMouseLeave;
       OnMouseMove := TilesFrameTileSetViewMouseMove;
       OnSysKeyDown := MagicWandSysKeyDown;
       OnSysKeyUp := MagicWandSysKeyUp;
       PopupMenu := TilesPopup;
      end;
     end;

     SetLength(TileSetFramesList, 2);
     TileSetFramesList[1] := TTileSetsFrame(MainFrame);

     SetLength(MapFramesList, 2);

     InitTileSetsFrameEvents(TTileSetsFrame(MainFrame));
    end;
    with TTileSetsFrame(MainFrame) do
    begin
     SetProject(FProject, 0);
     SrcView := TilesFrm.TilesFrame.TileSetView;
     with TilesFrame.TileSetView do
     begin
      SelectionMode := SrcView.SelectionMode;
      SelectionEnabled := SrcView.SelectionEnabled;
      DefaultCursor := SrcView.DefaultCursor;
      CanPopup := SrcView.CanPopup;
     end;
    end;
   end;
   emTile:
   begin
    TileEditModeAction.Checked := True;
    if not (MainFrame is TTileEditFrame) then
    begin
     MainEditPanel.Perform(WM_SETREDRAW, 0, 0);
     RepaintControl := True;    
     MainFrameClear;

     MainFrame := TTileEditFrame.Create(Self);
     with TTileEditFrame(MainFrame) do
     begin
      Parent := MainEditPanel;
      Align := alClient;
      PropsFrame.OnRefreshGraphicContent := TilePropsFrameRefreshGraphicContent;
      PropsFrame.OnDataUpdated := TilePropsUpdated;
      PropsFrame.PaletteFrame := PaletteFrame;
      PropsFrame.Project := FProject;
      OnImageModified := TileImageModified;
      OnScaleChange := TileScaleChange;
      OnDrawColorChange := TileDrawColorChange;

      with TileEditView do
      begin
       PopupMenu := TileEditPopup;
       OnBeforeContentChange := TileEditViewBeforeContentChange;
       OnCustomMouseAction := TileEditViewCustomMouseAction;
       OnCustomMouseActionCheck := TileEditViewCustomMouseActionCheck;
       OnCustomMouseActionFinish := TileEditViewCustomMouseActionFinish;
       OnMouseEnter := MagicWandMouseEnter;
       OnMouseLeave := MagicWandMouseLeave;
       OnMouseMove := TileEditViewMouseMove;
       OnSysKeyDown := MagicWandSysKeyDown;
       OnSysKeyUp := MagicWandSysKeyUp;
      end;
     end;
     SetLength(TileSetFramesList, 1);
     SetLength(MapFramesList, 2);
    end;
    with TTileEditFrame(MainFrame) do
    begin
     DoSetScale(FProject.FInfo.Scales.scTileFrame, False);
     ColorTable := PaletteFrame.ColorTable;
     Tile := FLastTile;
     ColorIndex[False] := FProject.FInfo.DrawColors[False];
     ColorIndex[True] := FProject.FInfo.DrawColors[True];
    end;
    DrawTool := DrawTool;
   end;
  end;
 finally
  case FProject.FInfo.EditMode of
   emMap, emBrush:
   with TMapsFrame(MainFrame) do
   begin
    if RepaintControl then
    begin
     if MainEditActivePage = 0 then
      PageControl.ActivePageIndex := 1;

     MainEditPanel.Perform(WM_SETREDRAW, 1, 0);
    end;

    if PageControl.ActivePageIndex <> MainEditActivePage then
    begin
     PageControl.ActivePageIndex := MainEditActivePage;
     PageControlChange(PageControl);
    end;
    PageControl.Invalidate;
   end;
   emTileSet:
   with TTileSetsFrame(MainFrame) do
   begin
    if RepaintControl then
    begin
     if MainEditActivePage = 0 then
      PageControl.ActivePageIndex := 1;

     MainEditPanel.Perform(WM_SETREDRAW, 1, 0);
    end;
    
    if PageControl.ActivePageIndex <> MainEditActivePage then
    begin
     PageControl.ActivePageIndex := MainEditActivePage;
     PageControlChange(PageControl);
    end;
    PageControl.Invalidate;
   end;
   else
   begin
    if RepaintControl then
    begin
     MainFrame.Visible := False;
     MainEditPanel.Perform(WM_SETREDRAW, 1, 0);
     MainFrame.Visible := True;     
    end;
  {  with TTileEditFrame(MainFrame) do
    begin
     PropsFrame.Invalidate;
     Panel.Invalidate;
     StatusBar.Invalidate;
     TileEditView.Invalidate;
     TileEditView.UpdateScrollBars(True);
     Splitter.Invalidate;
     LeftPanel.Invalidate;
     ColorPanel.Invalidate;
     Color1Preview.Invalidate;
     Color2Preview.Invalidate;
     Color1Label.Invalidate;
     Color2Label.Invalidate;
    end; }
   end;
  end;
  MainEditPanel.Invalidate;
  MainFrame.Invalidate;
 end;
end;

procedure TMainForm.ToolsZoomActionExecute(Sender: TObject);
var
 Control: TWinControl;
 Owner: TComponent;
begin
 Control := FindControl(GetFocus);
 if Control is TTileMapView then
 begin
  Owner := Control.Owner;
  if Owner is TMapFrame then
  begin
   with TMapFrame(Owner) do
    Scale := Scale + TControl(Sender).Tag;
  end else
  if Owner is TTilesFrame then
  begin
   with TTilesFrame(Owner) do
    Scale := Scale + TControl(Sender).Tag;
  end else
  if Owner is TTileEditFrame then
  begin
   with TTileEditFrame(Owner) do
    Scale := Scale + TControl(Sender).Tag;   
  end;
 end;
end;

procedure TMainForm.MapFrameMapEditViewEnter(Sender: TObject);
var
 View: TTileMapView;
begin
 View := Sender as TTileMapView;
 FFocusedMapFrame := View.Owner as TMapFrame;
 with FFocusedMapFrame do
 begin
  MapEditViewEnter(Sender);
  if MapData <> nil then
   SelectColorTable(MapData.ColorTableIndex);
 end;
end;

procedure TMainForm.MapFrameMapEditViewExit(Sender: TObject);
begin
 FFocusedMapFrame := nil;
end;

procedure TMainForm.TilesFrameTileSetViewEnter(Sender: TObject);
var
 View: TTileMapView;
begin
 View := Sender as TTileMapView;
 FFocusedTilesFrame := View.Owner as TTilesFrame;
 FFocusedTilesFrame.TileSetViewEnter(Sender);
end;

procedure TMainForm.TilesFrameTileSetViewExit(Sender: TObject);
begin
 FFocusedTilesFrame := nil;
end;

procedure TMainForm.PaletteFramePaletteViewEnter(Sender: TObject);
var
 View: TTileMapView;
begin
 View := Sender as TTileMapView;
 FFocusedPaletteFrame := View.Owner as TPaletteFrame;
end;

procedure TMainForm.PaletteFramePaletteViewExit(Sender: TObject);
begin
 FFocusedPaletteFrame := nil;
end;

procedure TMainForm.ToolsZoomActionUpdate(Sender: TObject);
var
 Enable: Boolean;
 Value, Tag: Integer;
begin
 Enable := False;
 Tag := (Sender as TAction).Tag;
 if FProgressBar = nil then
 begin
  if FFocusedTilesFrame <> nil then
  begin
   Value := FFocusedTilesFrame.Scale + Tag;
   Enable := (Value >= 1) and (Value <= 16);
  end else 
  if FFocusedMapFrame <> nil then
  begin
   Value := FFocusedMapFrame.Scale + Tag;
   Enable := (Value >= 1) and (Value <= 16);
  end else
  if (MainFrame is TTileEditFrame) and
     TTileEditFrame(MainFrame).TileEditView.Focused then
  begin
   Value := TTileEditFrame(MainFrame).Scale + Tag;
   Enable := (Value >= 1) and (Value <= 32);
  end;
 end;
 TAction(Sender).Enabled := Enable;
end;

function TMainForm.GetEditMode: TEditMode;
begin
 Result := FProject.FInfo.EditMode;
end;

procedure TMainForm.LoadSaveProgress(Owner: TBaseSectionedList;
  Sender: TObject; const Rec: TProgressRec);
begin
 if (Sender = Owner) and
    ((Rec.ProgressID = SPID_READ_SECTION_HEADER) or
    (Rec.ProgressID = SPID_WRITE_SECTION_HEADER)) then
 begin
  FProgressBar.Max := Rec.Header.shDataSize + Rec.Header.shHeaderSize + Rec.Value;
  FProgressBar.Position := Rec.Value;
 end else
 case Rec.ProgressID of
  SPID_READ_START: if Sender = Owner then
                    FProgressLabel.Caption := 'Loading... ';
  SPID_WRITE_START: if Sender = Owner then
                    FProgressLabel.Caption := 'Saving... ';
   SPID_READ_SECTION_HEADER,  SPID_READ_DATA_HEADER,  SPID_READ_DATA,
  SPID_WRITE_SECTION_HEADER, SPID_WRITE_DATA_HEADER, SPID_WRITE_DATA:
   FProgressBar.Position := Rec.Value;
 end;
 Application.ProcessMessages;
end;

procedure TMainForm.CreateProgressBar;
var
 Panel: TPanel;
begin
 Panel := TPanel.Create(Self);
 Panel.Parent := MainStatusBar;
 Panel.Height := MainStatusBar.ClientHeight;
 Panel.Align := alBottom;
 Panel.BevelOuter := bvNone;
 FProgressLabel := TTntLabel.Create(Self);
 FProgressLabel.Parent := Panel;
 FProgressLabel.Layout := tlCenter;
 FProgressLabel.ShowAccelChar := False;
 FProgressLabel.AutoSize := True;
 FProgressLabel.Align := alLeft;
 FProgressBar := TProgressBar.Create(Self);
 FProgressBar.Parent := Panel;
 FProgressBar.Align := alClient;
 FProgressBar.Smooth := True;
end;

procedure TMainForm.DestroyProgressBar;
begin
 FProgressBar.Parent.Free;
 FProgressBar := nil;
// FreeAndNIL(FProgressBar);
end;
       (*
procedure TMainForm.TilesFrameScaleChange(Sender: TObject; var Value: Integer);
begin
 FProject.FInfo.Scales.scTilesFrame[Tag] := Value;
end;

function TMainForm.GetSelectedMap: TMap;
begin
 Result := FProject.FMaps[MapsListBox.ItemIndex];
end;

function TMainForm.GetSelectedBrush: TMap;
begin
 Result := FProject.FMaps.BrushList[BrushesListBox.ItemIndex];
end;

procedure TMainForm.TilesFrameTileSetViewSetPosition(
  Sender: TCustomTileMapView; var NewX, NewY: Integer);
begin
 with TilesFrame do
 begin
  if TileSet <> nil then
   with FProject.FTileSetInfo[TileSet.Index] do
   begin
    OffsetX := NewX div Scale;
    OffsetY := NewY div Scale;
   end;
 end;
end;         *)

procedure TMainForm.MainFormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
function MapVisible(Map: TMap): Boolean;
var
 I: Integer;
begin
 if Map <> nil then
 begin
  for I := 0 to Length(MapFramesList) - 1 do
   with MapFramesList[I] do
    if (PageControl.ActivePageIndex <> 0) and
       (MapFrame.MapData = Map) then
    begin
     Result := True;
     Exit;
    end;
 end;
 Result := False;
end;
var
 Change, I, J, K, DLen: Integer;
 Map: TMap;
 DoneList: array of TMap;
 Layer: TMapLayer;
 Flg: Byte;
 Nothing: Boolean;
 RefreshContent: TRefreshMapContentSet;
begin
 if Shift = [ssCtrl] then
 begin
  case Key of
   VK_ADD, $BB:
    Change := +1;
   VK_SUBTRACT, $BD:
    Change := -1;
   else
    Change := 0;
  end;
  if Change <> 0 then
  begin
   Key := 0;
   if FFocusedTilesFrame <> nil then
    with FFocusedTilesFrame do
     Scale := Scale + Change else
   if FFocusedMapFrame <> nil then
    with FFocusedMapFrame do
     Scale := Scale + Change else
   if (MainFrame is TTileEditFrame) and
      TTileEditFrame(MainFrame).TileEditView.Focused then
    with TTileEditFrame(MainFrame) do
     Scale := Scale + Change;
  end;
 end else
 if Key = VK_MENU then
 begin
  if DrawTool in [dtPencil, dtSingleSelect] then
  begin
   for I := 0 to Length(MapFramesList) - 1 do
    MapFramesList[I].MapFrame.MapEditView.DefaultCursor := crDropper;
  end;
  if not (DrawTool in [dtMultiSelect, dtPicker, dtMagicWand]) then
   for I := 0 to Length(TileSetFramesList) - 1 do
    TileSetFramesList[I].TilesFrame.TileSetView.DefaultCursor := crDropper;
 end else
 begin
  case Key of
   VK_ADD, $BB:
    Flg := LF_VISIBLE;
   VK_SUBTRACT, $BD:
    Flg := LF_LAYER_ACTIVE;
   else
    Flg := 0;
  end;

  if (Flg <> 0) and (Shift = [ssAlt]) then
  begin
   RefreshContent := [rmcLayersList, rmcLayerProps];
   if Flg = LF_VISIBLE then
    Include(RefreshContent, rmcView);

   SetLength(DoneList, Length(MapFramesList));
   DLen := 0;
   for I := 0 to Length(MapFramesList) - 1 do
    with MapFramesList[I] do
     if PageControl.ActivePageIndex = 0 then
     begin
      Map := MapFrame.MapData;
      if MapVisible(Map) then
      begin
       Nothing := False;
       for J := 0 to Dlen - 1 do
        if DoneList[J] = Map then
        begin
         Nothing := True;
         Break;
        end;

       if not Nothing then
       begin
        DoneList[DLen] := Map;
        Inc(DLen);
        Nothing := True;
        Change := -1;
        for K := 0 to Map.Count - 1 do
         if LayersListBox.Selected[K] then
         begin
          Layer := Map.Layers[K];
          if Layer <> nil then
          begin
           if Change = -1 then
           begin
            if Layer.Flags and Flg <> 0 then
             Change := 0 else
             Change := 1;
           end;
           if Change = 0 then
            Layer.Flags := Layer.Flags and not Flg else
            Layer.Flags := Layer.Flags or Flg;
           Nothing := False;
          end;
         end;
        if not Nothing then
         Self.RefreshMapContent(Map, RefreshContent);
       end;
      end;
     end;

   if DLen > 0 then
    Key := 0;
   Exit;
  end;

  if Shift = [ssAlt] then
   Flg := LF_LAYER_ACTIVE else
  if Shift = [ssCtrl, ssShift] then
   Flg := LF_VISIBLE;

  if Flg <> 0 then
  begin
   case Key of
    Word('0'):
     Change := 9;
    Word('1')..Word('9'):
     Change := Key - Word('1');
    Word('A')..Word('Z'):
     Change := 10 + (Key - Word('A'));
    else
     Exit;
   end;

   RefreshContent := [rmcLayersList, rmcLayerProps];
   if Flg = LF_VISIBLE then
    Include(RefreshContent, rmcView);

   SetLength(DoneList, Length(MapFramesList));
   DLen := 0;
   for I := 0 to Length(MapFramesList) - 1 do
    with MapFramesList[I] do
     if PageControl.ActivePageIndex = 0 then
     begin
      Map := MapFrame.MapData;
      if MapVisible(Map) then
      begin
       Layer := Map.Layers[Change];

       for J := 0 to Dlen - 1 do
        if DoneList[J] = Map then
        begin
         Layer := nil;
         Break;
        end;

       if Layer <> nil then
       begin
        DoneList[DLen] := Map;
        Inc(DLen);
        Layer.Flags := Layer.Flags xor Flg;
        Self.RefreshMapContent(Map, RefreshContent);
       end;
      end;
     end;

   if DLen > 0 then
    Key := 0;
  end;
 end;
end;

procedure TMainForm.SimpleActionUpdate(Sender: TObject);
begin
 (Sender as TAction).Enabled := FProgressBar = nil;
end;

function TMainForm.CheckSaved: Boolean;
begin
 Result := True;
 if not Saved then
 begin
  case WideMessageDlg('Save changes to the project?', mtConfirmation, [mbYes, mbNo, mbCancel], 0) of
   mrYes: SaveActionExecute(NIL);
   mrCancel: Result := False;
  end;
 end;
end;

procedure TMainForm.SelectAllActionUpdate(Sender: TObject);
var
 Enable: Boolean;
 View: TTileMapView;
begin
 Enable := False;
 if FProgressBar = nil then
 begin
  if ColorTablesListBox.Focused then
   Enable := ColorTablesListBox.Count > 0 else
  if FFocusedTileSetsFrame <> nil then
   Enable := FFocusedTileSetsFrame.TileSetsListBox.SelCount > 0 else
  if FFocusedMapsFrame <> nil then
  begin
   with FFocusedMapsFrame do
    Enable := (MapsListBox.Focused and (MapsListBox.Count > 0)) or
              (LayersListBox.Focused and (LayersListBox.Count > 0));
  end else
  if (MainFrame is TTileEditFrame) and
     TTileEditFrame(MainFrame).TileEditView.Focused then
  begin
   with TTileEditFrame(MainFrame).TileEditView do
    Enable := (MapWidth > 0) and (MapHeight > 0);
  end else
  begin
   if FFocusedTilesFrame <> nil then
    View := FFocusedTilesFrame.TileSetView else
   if FFocusedMapFrame <> nil then
    View := FFocusedMapFrame.MapEditView else
   if FFocusedPaletteFrame <> nil then
    View := FFocusedPaletteFrame.PaletteView else
    View := nil;

   Enable := (View <> nil) and (View.MapWidth > 0) and (View.MapHeight > 0);
  end;
 end; 

 SelectAllAction.Enabled := Enable;
end;

procedure TMainForm.SelectAllActionExecute(Sender: TObject);
var
 View: TCustomTileMapView;
begin
 if ColorTablesListBox.Focused then
 begin
  ColorTablesListBox.SelectAll;
  RefreshColorTableProps;
 end else
 if FFocusedTileSetsFrame <> nil then
 begin
  with FFocusedTileSetsFrame do
  begin
   TileSetsListBox.SelectAll;
   RefreshTileSetProps(False);
  end;
 end else
 if FFocusedMapsFrame <> nil then
 begin
  with FFocusedMapsFrame do
  begin
   if LayersListBox.Focused then
   begin
    LayersListBox.SelectAll;
    RefreshLayerProps;
   end else
   if MapsListBox.Focused then
   begin
    MapsListBox.SelectAll;
    RefreshMapProps;
   end;
  end;
 end else
 if (MainFrame is TTileEditFrame) and
    TTileEditFrame(MainFrame).TileEditView.Focused then
 begin
  DrawTool := dtMultiSelect;
  ToolMultiSelectAction.Checked := True;
  TTileEditFrame(MainFrame).TileEditView.SelectAll;
 end else
 begin
  if FFocusedTilesFrame <> nil then
   View := FFocusedTilesFrame.TileSetView else
  if FFocusedMapFrame <> nil then
   View := FFocusedMapFrame.MapEditView else
   View := nil;

  if View <> nil then
  begin
   if View.SelectionMode <> selMulti then
   begin
    DrawTool := dtMultiSelect;
    ToolMultiSelectAction.Checked := True;
   end;
   View.SelectAll;   
  end else
  if FFocusedPaletteFrame <> nil then
   FFocusedPaletteFrame.PaletteView.SelectAll;
 end;
end;

procedure TMainForm.SelectDeselectActionUpdate(Sender: TObject);
var
 Enable: Boolean;
begin
 Enable := False;
 if FProgressBar = nil then
 begin
  if ColorTablesListBox.Focused then
   Enable := ColorTablesListBox.SelCount > 0 else
  if FFocusedTilesFrame <> nil then
   Enable := FFocusedTilesFrame.TileSetView.MultiSelected else
  if FFocusedMapFrame <> nil then
   Enable := FFocusedMapFrame.MapEditView.MultiSelected else
  if FFocusedPaletteFrame <> nil then
   Enable := FFocusedPaletteFrame.PaletteView.MultiSelected else
  if FFocusedTileSetsFrame <> nil then
   Enable := FFocusedTileSetsFrame.TileSetsListBox.SelCount > 0 else
  if FFocusedMapsFrame <> nil then
  begin
   with FFocusedMapsFrame do
    Enable := (MapsListBox.Focused and (MapsListBox.SelCount > 0)) or
              (LayersListBox.Focused and (LayersListBox.SelCount > 0));
  end else
  if (MainFrame is TTileEditFrame) and
     TTileEditFrame(MainFrame).TileEditView.Focused then
   Enable := TTileEditFrame(MainFrame).TileEditView.SomethingSelected;
 end;
 (Sender as TAction).Enabled := Enable;
end;

procedure TMainForm.SelectDeselectActionExecute(Sender: TObject);
begin
 if ColorTablesListBox.Focused then
 begin
  ColorTablesListBox.ClearSelection;
  RefreshColorTableProps;
 end else
 if FFocusedTileSetsFrame <> nil then
 begin
  with FFocusedTileSetsFrame do
  begin
   TileSetsListBox.ClearSelection;
   RefreshTileSetProps(False);
  end;
 end else
 if FFocusedMapsFrame <> nil then
 begin
  with FFocusedMapsFrame do
   if MapsListBox.Focused then
   begin
    MapsListBox.ClearSelection;
    RefreshMapProps;
   end else
   if LayersListBox.Focused then
   begin
    LayersListBox.ClearSelection;
    RefreshLayerProps;
   end;
 end else
 if (MainFrame is TTileEditFrame) and
    TTileEditFrame(MainFrame).TileEditView.Focused then
 begin
  with TTileEditFrame(MainFrame).TileEditView do
   if SomethingSelected then
    Deselect;
 end else
 if FFocusedTilesFrame <> nil then
 begin
  with FFocusedTilesFrame.TileSetView do
   if MultiSelected then
    Deselect;
 end else
 if FFocusedMapFrame <> nil then
 begin
  with FFocusedMapFrame.MapEditView do
   if MultiSelected then
    Deselect;
 end else
 if FFocusedPaletteFrame <> nil then
 begin
  with FFocusedPaletteFrame.PaletteView do
   if MultiSelected then
    Deselect;
 end;
end;

procedure TMainForm.SelectInverseActionExecute(Sender: TObject);
var
 I: Integer;
begin
 if FProgressBar = nil then
 begin
  if ColorTablesListBox.Focused then
  begin
   with ColorTablesListBox do
    if SelCount > 0 then
    begin
     for I := 0 to Count - 1 do
      Selected[I] := not Selected[I];

     RefreshColorTableProps;
    end;
  end else
  if FFocusedTileSetsFrame <> nil then
  begin
   with FFocusedTileSetsFrame do
    if TileSetsListBox.SelCount > 0 then
    begin
     with TileSetsListBox do
      for I := 0 to Count - 1 do
       Selected[I] := not Selected[I];

     RefreshTileSetProps(False);
    end;
  end else
  if FFocusedMapsFrame <> nil then
  begin
   with FFocusedMapsFrame do
    if MapsListBox.Focused then
    begin
     if MapsListBox.SelCount > 0 then
     begin
      with MapsListBox do
       for I := 0 to Count - 1 do
        Selected[I] := not Selected[I];

      RefreshMapProps;  
     end
    end else
    if LayersListBox.Focused then
    begin
     if LayersListBox.SelCount > 0 then
     begin
      with LayersListBox do
       for I := 0 to Count - 1 do
        Selected[I] := not Selected[I];

      RefreshLayerProps;  
     end
    end

  end else
  if (MainFrame is TTileEditFrame) and
     TTileEditFrame(MainFrame).TileEditView.Focused then
  begin
   with TTileEditFrame(MainFrame).TileEditView do
    if SomethingSelected then
     InvertSelection;
  end else
  if FFocusedTilesFrame <> nil then
  begin
   with FFocusedTilesFrame.TileSetView do
    if MultiSelected then
     InvertSelection;
  end else
  if FFocusedMapFrame <> nil then
  begin
   with FFocusedMapFrame.MapEditView do
    if MultiSelected then
     InvertSelection;
  end else
  if FFocusedPaletteFrame <> nil then
  begin
   with FFocusedPaletteFrame.PaletteView do
    if MultiSelected then
     InvertSelection;
  end;
 end;
end;

procedure TMainForm.SelectionActionUpdate(Sender: TObject);
var
 Enable: Boolean;
begin
 Enable := False;
 if FProgressBar = nil then
 begin
  if ColorTablesListBox.Focused then
    Enable := ColorTablesListBox.SelCount > 0 else
  if (MainFrame is TTileEditFrame) and
      TTileEditFrame(MainFrame).TileEditView.Focused then
   Enable := TTileEditFrame(MainFrame).TileEditView.SomethingSelected else
  if FFocusedTilesFrame <> nil then
   Enable := FFocusedTilesFrame.TileSetView.SomethingSelected else
  if FFocusedMapFrame <> nil then
   Enable := FFocusedMapFrame.MapEditView.SomethingSelected else
  if FFocusedPaletteFrame <> nil then
   Enable := FFocusedPaletteFrame.PaletteView.SomethingSelected else
  if FFocusedTileSetsFrame <> nil then
   Enable := FFocusedTileSetsFrame.TileSetsListBox.SelCount > 0 else
  if FFocusedMapsFrame <> nil then
   with FFocusedMapsFrame do
    Enable := (MapsListBox.Focused and (MapsListBox.SelCount > 0)) or
              (LayersListBox.Focused and (LayersListBox.SelCount > 0));
 end;
 (Sender as TAction).Enabled := Enable;
end;

procedure TMainForm.EditDeleteActionExecute(Sender: TObject);
begin
 if ColorTablesListBox.Focused then
 begin
  ColorTableDeleteActionExecute(ColorTableDeleteAction);
 end else
 if FFocusedTilesFrame <> nil then
 begin
  with FFocusedTilesFrame.TileSetView do
   if SomethingSelected then
    ClearSelection;
 end else
 if FFocusedMapFrame <> nil then
 begin
  with FFocusedMapFrame.MapEditView do
   if SomethingSelected then
    ClearSelection;
 end else
 if FFocusedPaletteFrame <> nil then
 begin
  with FFocusedPaletteFrame.PaletteView do
   if SomethingSelected then
    ClearSelection;
 end else
 if FFocusedTileSetsFrame <> nil then
  with FFocusedTileSetsFrame do
   TileSetDeleteActionExecute(TileSetDeleteAction) else
 if FFocusedMapsFrame <> nil then
 begin
  with FFocusedMapsFrame do
   if MapsListBox.Focused then
    MapDeleteActionExecute(MapDeleteAction) else
   if LayersListBox.Focused then 
    LayerDeleteActionExecute(LayerDeleteAction);
 end else
 if MainFrame is TTileEditFrame then
  with TTileEditFrame(MainFrame).TileEditView do
   if Focused and SomethingSelected then
    ClearSelection;
end;

procedure TMainForm.SetDrawTool(Value: TDrawTool);
var
 CursorIndex, I: Integer;
 Mode: TSelectionMode;
begin
 FDrawTool := Value;
 case Value of
  dtSingleSelect,
  dtMultiSelect:
  begin
   if Value = dtSingleSelect then
    Mode := selSingle else
    Mode := selMulti;

   for I := 0 to Length(MapFramesList) - 1 do
    with MapFramesList[I].MapFrame.MapEditView do
     SelectionMode := Mode;

   for I := 0 to Length(TileSetFramesList) - 1 do
    with TileSetFramesList[I].TilesFrame.TileSetView do
     SelectionMode := Mode;

   if MainFrame is TTileEditFrame then
    TTileEditFrame(MainFrame).TileEditView.SelectionMode := selMulti;
  end;
  dtMagicWand:
  begin
   for I := 0 to Length(MapFramesList) - 1 do
    with MapFramesList[I].MapFrame.MapEditView do
    begin
     DefaultCursor := crMagicWand;
     SelectionEnabled := False;
     SelectionMode := selMulti;
     CanPopup := True;
    end;
   for I := 0 to Length(TileSetFramesList) - 1 do
    with TileSetFramesList[I].TilesFrame.TileSetView do
    begin
     DefaultCursor := crMagicWand;
     SelectionEnabled := False;
     SelectionMode := selMulti;
     CanPopup := True;
    end;

   with PaletteFrame.PaletteView do
   begin
    DefaultCursor := crMagicWand;
    SelectionEnabled := False;
   end;

   if MainFrame is TTileEditFrame then
    with TTileEditFrame(MainFrame).TileEditView do
    begin
     DefaultCursor := crMagicWand;
     SelectionEnabled := False;
     CanPopup := True;
    end;
   Exit;
  end;
  else
  begin
   with PaletteFrame.PaletteView do
   begin
    DefaultCursor := crDefault;
    SelectionEnabled := True;
   end;
   case Value of
    dtPencil:    CursorIndex := crPencil;
    dtBrush:     CursorIndex := crBrush;
    dtEraser:    CursorIndex := crEraser;
    dtFloodFill: CursorIndex := crFloodFill;
    dtPicker:    CursorIndex := crDropper;
    else         CursorIndex := crDefault;
   end;

   for I := 0 to Length(MapFramesList) - 1 do
    with MapFramesList[I].MapFrame.MapEditView do
    begin
     if SelectionMode = selSingle then
     begin
      if Value <> dtPicker then
       SelectionMode := selNone;
     end else
     begin
      if (Value = dtPicker) and
         (SelectionMode <> selMulti) then
       SelectionMode := selSingle else
       SelectionEnabled := False;
     end;

     DefaultCursor := CursorIndex;

     CanPopup := False;
    end;

   for I := 0 to Length(TileSetFramesList) - 1 do
    with TileSetFramesList[I].TilesFrame.TileSetView do
    begin
     SelectionEnabled := True;
     SelectionMode := selSingle;
     if Value = dtPicker then
     begin
      DefaultCursor := CursorIndex;
      CanPopup := False;
     end else
     begin
      DefaultCursor := crDefault;
      CanPopup := True;
     end;
    end;

   if MainFrame is TTileEditFrame then
    with TTileEditFrame(MainFrame).TileEditView do
    begin
     SelectionEnabled := False;
     DefaultCursor := CursorIndex;
     CanPopup := False;
    end;

   Exit;
  end;
 end;

 for I := 0 to Length(MapFramesList) - 1 do
  with MapFramesList[I].MapFrame.MapEditView do
  begin
   SelectionEnabled := True;
   DefaultCursor := crDefault;
   CanPopup := True;
  end;

 for I := 0 to Length(TileSetFramesList) - 1 do
  with TileSetFramesList[I].TilesFrame.TileSetView do
  begin
   SelectionEnabled := True;
   DefaultCursor := crDefault;
   CanPopup := True;
  end;

 if MainFrame is TTileEditFrame then
  with TTileEditFrame(MainFrame).TileEditView do
  begin
   SelectionEnabled := True;
   DefaultCursor := crDefault;
   CanPopup := True;
  end;

 with PaletteFrame.PaletteView do
 begin
  SelectionEnabled := True;
  DefaultCursor := crDefault;  
 end;  
end;

procedure TMainForm.TilesFrameTileSetViewContentsChanged(Sender: TObject);
var
 View: TCustomTileMapView;
 Frm: TTilesFrame;
 ts: TTileSet;
 I: Integer;
 Event: THistoryEvent;
begin
 View := Sender as TCustomTileMapView;
 Frm := View.Owner as TTilesFrame;
 ts := Frm.TileSet;
 for I := 0 to Length(TileSetFramesList) - 1 do
  with TileSetFramesList[I].TilesFrame do
   if ts = TileSet then
    TileSetView.Invalidate;

 for I := 0 to Length(MapFramesList) - 1 do
  with MapFramesList[I] do
  begin
   MapFrame.MapEditView.Invalidate;
   LayersListBox.Invalidate;
  end;

 if MainFrame is TTileEditFrame then
  with TTileEditFrame(MainFrame) do
  begin
   TileEditView.Deselect(False);
   RefreshImage(False);
   if Tile <> nil then
    PropsFrame.ShowTileProps(TTileSet(Tile.Owner), Tile.TileIndex);
   RefreshColorTable;
  end;
 Saved := False;
 Event := FProject.History.LastNode as THistoryEvent;
 if Event <> nil then
  Event.RedoData := ts;  
end;                      (*

procedure TMainForm.LayersListBoxDrawPic(Sender: TObject; AIndex: Integer;
  Rect: TRect; State: TOwnerDrawState; var Handled: Boolean);
var
 Map: TMap;
 Layer: TMapLayer;
 Tile: TTileItem;
 Cvt: TBitmapConverter;
begin
 with Sender as TLayerListBox do
 begin
  Map := TObject(Tag) as TMap;
  if Map <> nil then
  begin
   Layer := Map.Layers[AIndex];
   if (Layer <> nil) and (Layer.TileSet <> nil) and
      (Layer.Flags and LF_TILE_SELECTED <> 0) then
   begin
    with Layer.DrawInfo do
    begin
     Tile := Layer.TileSet.Indexed[TileIndex];
     if Tile <> nil then
      with Layer.TileSet do
      begin
       FillPicHeader(TileWidth, TileHeight, FDrawTileHeader, FDrawTileBmp);

       if MapFrame.Converters = nil then
        MapFrame.FillConverters(FProject.FMaps);

       Cvt := MapFrame.Converters[Index].tcvtDraw[XFlip, YFlip];
       if Cvt <> nil then
       begin
        Cvt.SrcInfo.PixelModifier := Layer.FirstColor + Tile.FirstColor;

        FillChar(FDrawTileBmp.ImageData^,
                 FDrawTileHeader.bhHead.bhInfoHeader.biSizeImage,
                 Integer(TransparentColor) + Cvt.SrcInfo.PixelModifier);

        TileDraw(Tile.TileData^, FDrawTileBmp, 0, 0, Cvt);

        Inc(Rect.Left);
        Dec(Rect.Right);
        Inc(Rect.Top);
        Dec(Rect.Bottom);
        DrawPic(Canvas.Handle, Rect, FDrawTileHeader, FDrawTileBmp.ImageData);
        Handled := True;
       end;
      end;
    end;
   end;
  end;
 end;
end;

procedure TMainForm.LayersListBoxGetItemClickMode(Sender: TObject;
  AIndex: Integer; var AModes: TItemClickModes);
var
 Map: TMap;
 Layer: TMapLayer;
begin
 Map := TObject(LayersListBox.Tag) as TMap;
 if Map <> nil then
 begin
  Layer := Map.Layers[AIndex];
  if Layer <> nil then
  begin
   if Layer.Flags and LF_LAYER_ACTIVE <> 0 then
    Include(AModes, llbChecked);
   if Layer.Flags and LF_VISIBLE <> 0 then
    Include(AModes, llbVisible);
   if (Layer.XFlipShift < 0) and (Layer.YFlipShift < 0) then
    Include(AModes, llbFlipsDisabled) else
   begin
    if Layer.DrawInfo.XFlip then
     Include(AModes, llbXFlip);
    if Layer.DrawInfo.YFlip then
     Include(AModes, llbYFlip);
   end;
  end;
 end;
end;

procedure TMainForm.LayersListBoxItemClick(Sender: TObject;
  AIndex: Integer; ChangedMode: TItemClickMode);
var
 Map: TMap; Layer: TMapLayer;
begin
 Map := TObject(LayersListBox.Tag) as TMap;
 if Map <> nil then
 begin
  Layer := Map.Layers[AIndex];
  if Layer <> nil then
  begin
   case ChangedMode of
    llbChecked: Layer.Flags := Layer.Flags xor LF_LAYER_ACTIVE;
    llbVisible:
    begin
     Layer.Flags := Layer.Flags xor LF_VISIBLE;
     MapFrame.MapEditView.Invalidate;
     BrushFrame.MapEditView.Invalidate;
    end;
    llbXFlip:   Layer.DrawInfo.XFlip := not Layer.DrawInfo.XFlip;
    llbYFlip:   Layer.DrawInfo.YFlip := not Layer.DrawInfo.YFlip;
    llbTile:  if (ssDouble in GlobalShiftState) and
                 (Layer.Flags and LF_TILE_SELECTED <> 0) and
                 (Layer.TileSet <> nil) then
    begin
     if Layer.TileSet <> TilesFrame.TileSet then
     begin
      TileSetsListBox.ItemIndex := Layer.TileSet.Index;
      TilesBoxUpdate;
     end;
     FProject.FTileSetInfo[Layer.TileSet.Index].SelectedTile := Layer.DrawInfo.TileIndex;
     TilesFrame.SelectedTile := Layer.DrawInfo.TileIndex;
     TilesFrame.TileSetView.ScrollToSelectedCell;
    end;
   end;
   if LayersListBox.Selected[AIndex] then
    FillLayerProps(TObject(LayersListBox.Tag) as TMap);
  end;
 end;
end;

procedure TMainForm.LayersListBoxData(Control: TWinControl; Index: Integer;
  var Data: String);
var
 Map: TMap;
 Layer: TMapLayer;
begin
 Map := TObject(LayersListBox.Tag) as TMap;
 if Map <> nil then
 begin
  Layer := Map.Layers[Index];
  if Layer <> nil then
   Data := Format('%d. %s', [Layer.Index + 1, UTF8Encode(Layer.Name)]);
 end;
end;
                  *)
procedure TMainForm.TilesFrameTileSetViewCustomMouseAction(
  Sender: TCustomTileMapView; var Done: Boolean);
var
 DoneList: array of TMap;
 DLen, I, J: Integer;
 Map: TMap;
 Layer: TMapLayer;
 Found, PickToSelected, Pick: Boolean;
begin
 if (DrawTool = dtPicker) or (ssAlt in GlobalShiftState) then
  with Sender.Owner as TTilesFrame do
   if TileSetView.NewCursorPosition then
   begin
    with TileSetView do
     if SelectionMode = selSingle then
      Selected[TileX, TileY] := True;

    PickToSelected := (ssRight in GlobalShiftState) or
                      (ssCtrl in GlobalShiftState);
    
    SetLength(DoneList, Length(MapFramesList));
    DLen := 0;
    for I := 0 to Length(MapFramesList) - 1 do
     with MapFramesList[I] do
      if PageControl.ActivePageIndex = 0 then
      begin
       Map := MapFrame.MapData;
       if Map <> nil then
       begin
        Found := False;

        for J := 0 to Dlen - 1 do
         if DoneList[J] = Map then
         begin
          Found := True;
          Break;
         end;

        if not Found then
        begin
         DoneList[DLen] := Map;
         Inc(DLen);

         for J := 0 to Map.Count - 1 do
         begin
          Layer := Map.Layers[J];
          Pick := (Layer.TileSet = TileSet);
          if Pick then
          begin
           if PickToSelected then
            Pick := LayersListBox.Selected[J] else
            Pick := (Layer.Flags and LF_LAYER_ACTIVE <> 0);
           if Pick then
           begin
            Layer.DrawInfo.TileIndex := SelectedTile;
            if (SelectedTile >= 0) and
               (TileSet.Indexed[SelectedTile] <> TileSet.EmptyTile) then
             Layer.Flags := Layer.Flags or LF_TILE_SELECTED else
             Layer.Flags := Layer.Flags and not LF_TILE_SELECTED;
            Found := True;
           end;
          end;
         end;

         if Found then
          Self.RefreshMapContent(Map, [rmcLayersList, rmcLayerProps]);
        end;
       end;
      end;
   end;
end;

procedure ContinguousTilesSelect(Frame: TTilesFrame; X, Y: Integer;
                  Shift: TShiftState);
var
 SelBuf: PBoolean;
 Tile: TTileItem;
 SelInc: Boolean;
 W, H, TSZ: Integer;
 ts: TTileSet;

 procedure Beam(XInc, YInc, X, Y: Integer);
 var
  SP: PBoolean;
  Index: Integer;
  Incr: Integer;
  PV: PInteger;
 begin
  if XInc <> 0 then
  begin
   Incr := XInc;
   PV := @X;
  end else
  begin
   Incr := YInc;
   PV := @Y;
  end;
  repeat
   Inc(PV^, Incr);
   if (X < 0) or
      (Y < 0) or
      (X >= W) or
      (Y >= H) then Break;

   Index := Y * W + X;
   SP := SelBuf;
   Inc(SP, Index);
   if (SP^ <> SelInc) and
      (CompareMem(ts.Indexed[Index].TileData, Tile.TileData, TSZ)) then
   begin
    SP^ := SelInc;
    if YInc >= 0 then
     Beam(0, -1, X, Y);
    if YInc <= 0 then
     Beam(0, +1, X, Y);
    if XInc >= 0 then
     Beam(-1, 0, X, Y);
    if XInc <= 0 then
     Beam(+1, 0, X, Y);
   end else
    Break;
  until False;
 end;

begin
 ts := Frame.TileSet;
 if ts <> nil then
  with Frame.TileSetView do
  begin
   SelBuf := SelectionBuffer;
   W := MapWidth;

   H := Y * W + X;
   Tile := ts.Indexed[H];
   TSZ := ts.TileSize;

   SelInc := Shift <> SelectExcludeShiftState;
   if Shift = SelectNewShiftState then
    FillChar(SelBuf^, W * MapHeight, 0);

   PBoolean(Integer(SelBuf) + H)^ := SelInc;

   H := MapHeight;

   Beam(0, -1, X, Y);
   Beam(0, +1, X, Y);
   Beam(-1, 0, X, Y);
   Beam(+1, 0, X, Y);

   UpdateSelection;
  end;
end;

procedure TilesMagicWand(Frame: TTilesFrame; Shift: TShiftState);
var
 I: Integer;
 SP: PBoolean;
 SelInc: Boolean;
 Contiguous: Boolean;
 Tile: TTileItem;
 TSZ: Integer;
begin
 Contiguous := ssCtrl in Shift;
 if Contiguous then
  Shift := Shift - [ssCtrl];
 with Frame, TileSetView do
 if (Shift = SelectNewShiftState) or
    (Shift = SelectIncludeShiftState) or
    (Shift = SelectExcludeShiftState) then
 begin
  if not Contiguous then
   with TileSet do
   begin
    Tile := Indexed[IndexUnderCursor];

    SP := SelectionBuffer;
    SelInc := Shift <> SelectExcludeShiftState;
    if Shift = SelectNewShiftState then
     FillChar(SP^, MapWidth * MapHeight, 0);

    if Tile = EmptyTile then
    begin
     for I := 0 to TilesCount - 1 do
     begin
      if Indexed[I] = Tile then
       SP^ := SelInc;
      Inc(SP);
     end;
    end else
    begin
     TSZ := TileSize;
     for I := 0 to TilesCount - 1 do
     begin
      if CompareMem(Indexed[I].TileData, Tile.TileData, TSZ) then
       SP^ := SelInc;
      Inc(SP);
     end;
    end;
   UpdateSelection;
  end else
   ContinguousTilesSelect(Frame, TileX, TileY, Shift);
  UpdateMagicCursor(Frame.TileSetView, Shift);
 end;
end;

procedure TMainForm.TilesFrameTileSetViewCustomMouseActionCheck(
  Sender: TCustomTileMapView; Shift: TShiftState; var Act: Boolean);
begin
 if Shift = [ssLeft, ssDouble] then
 begin
  TilesTileEditActionExecute(Sender.Owner);
 end else
 if (DrawTool = dtMagicWand) and
    (ssLeft in Shift) then
 begin
  Sender.PasteBufferApply;
  TilesMagicWand(Sender.Owner as TTilesFrame, Shift);
  Act := False;
 end else
 if ((DrawTool = dtPicker) or
    ((ssAlt in Shift) and (Sender.SelectionMode = selSingle))) and
    ((ssLeft in Shift) or (ssRight in Shift)) then
 begin
  Act := True;
  Sender.NewCursorPosition := True;
 end;
end;
                     (*
procedure TMainForm.RefreshColorTable;
var
 Map: TMap;
begin
 Map := TObject(LayersListBox.Tag) as TMap;
 if (Map <> nil) and (Map.ColorTable <> nil) then
  with Map.ColorTable do
   ColorFormat.ConvertToRGBQuad(ColorData, Addr(FDrawTileHeader.bhPalette), ColorsCount) else
   FillChar(FDrawTileHeader.bhPalette, SizeOf(TRGBQuads), 0);
end;

procedure TMainForm.LayersListBoxClick(Sender: TObject);
var
 Map: TMap;
begin
 Map := TObject(LayersListBox.Tag) as TMap;
 if Map <> nil then
 begin
  if Map.Owner = FProject.FMaps.BrushList then
   FProject.FBrushFocus[Map.Index] := LayersListBox.ItemIndex else
   FProject.FFocusedList[Map.Index] := LayersListBox.ItemIndex;

 end;
 FillLayerProps(Map);
 TilesBoxUpdate;
end;

procedure TMainForm.LayersListBoxDblClick(Sender: TObject);
var
 Map: TMap;
 Layer: TMapLayer;
begin
 Map := TObject(LayersListBox.Tag) as TMap;
 if Map <> nil then
 begin
  Layer := Map.Layers[LayersListBox.ItemIndex];
  if (Layer <> nil) and (Layer.TileSet <> nil) and
     (Layer.TileSet <> TilesFrame.TileSet) then
  begin
   TileSetsListBox.ItemIndex := Layer.TileSet.Index;
   TilesBoxUpdate;
  end;
 end;
end;

procedure TMainForm.FillMapProps(Map: TMap);
var
 List: TTntStringList;
 I: Integer;
begin
 with MapPropEditor, PropertyList do
 begin
  ClearList;

  UserTag := Integer(Map);
  if Map <> nil then
   with Map do
   begin
    AddString('Name', Name).UserTag := 0;
    AddDecimal('Width', Width, 1, 32767 div TileWidth).UserTag := 2;
    AddDecimal('Height', Height, 1, 32767 div TileHeight).UserTag := 3;
    if Map is TMapBrush then
    begin
     List := TTntStringList.Create;
     try
      with FProject.FMaps.BrushList do
       for I := 0 to Count - 1 do
        if I <> Map.Index then
         List.Add(Brushes[I].Name);
      List.CaseSensitive := True;
      I := TMapBrush(Map).ExtendsIndex;
      if I > Map.Index then
       Dec(I);
      AddPickList('Extends brush', List, False, I, False).UserTag := 11;
     finally
      List.Free;
     end;
    end;
    List := TTntStringList.Create;
    try
     with FProject.FMaps.ColorTableList do
      for I := 0 to Count - 1 do
       List.Add(Items[I].Name);

     List.CaseSensitive := True;
     AddPickList('Color table', List, False, ColorTableIndex, False).UserTag := 1;
    finally
     List.Free;
    end;
    AddDecimal('Tile width', TileWidth, 1, 256).UserTag := 4;
    AddDecimal('Tile height', TileHeight, 1, 256).UserTag := 5;    
   end;
  RootNodeCount := Count;

 end;
end;

procedure TMainForm.MapPropertiesValueChange(Sender: TPropertyListItem);
var
 Map: TMap;
begin
 Map := TObject(MapPropEditor.PropertyList.UserTag) as TMap;
 if Map <> nil then
 case Sender.UserTag of
  0:
  begin
   Map.Name := Sender.ValueStr;
   Sender.Changing := True;
   Sender.ValueStr := Map.Name;
   Sender.Changing := False;

   UpdateMapName;
  end;
  11:
  if Map is TMapBrush then
   with TMapBrush(Map) do
   begin
    if (Sender.ValueStr <> '') and
       (FProject.FMaps.BrushList.FindByName(Sender.ValueStr) = nil) then
    begin
     WideMessageDlg(WideFormat('No brush named ''%s''', [Sender.ValueStr]), mtError, [mbOk], 0);
     Sender.Changing := True;
     if Extends <> nil then
      Sender.ValueStr := Extends.Name else
      Sender.ValueStr := '';
     Sender.Changing := False;
     Exit;
    end;
    ExtendsIndex := Sender.PickListIndex;
    if LayersListBox.Tag = Integer(Map) then
     FillLayerProps(Map);
   end;
  else
  begin
   case Sender.UserTag of
    1:
    begin
     if (Sender.ValueStr <> '') and
        (FProject.FMaps.ColorTableList.FindByName(Sender.ValueStr) = nil) then
     begin
      WideMessageDlg(WideFormat('No color table named ''%s''', [Sender.ValueStr]), mtError, [mbOk], 0);
      Sender.Changing := True;
      with Map do
       if ColorTable <> nil then
        Sender.ValueStr := ColorTable.Name else
        Sender.ValueStr := '';
      Sender.Changing := False;
      Exit;
     end;
     Map.ColorTableIndex := Sender.PickListIndex;
    end;
    2: Map.Width := Sender.Value;
    3: Map.Height := Sender.Value;
    4:
    begin
     Map.TileWidth := Sender.Value;
     Map.Width := Min(Map.Width, 32767 div Map.TileWidth);
     (Sender.Prev.Prev as TPropertyListItem).Value := Map.Width;
     MapPropEditor.Invalidate;     
    end;
    5:
    begin
     Map.TileHeight := Sender.Value;    
     Map.Height := Min(Map.Height, 32767 div Map.TileHeight);
     (Sender.Prev.Prev as TPropertyListItem).Value := Map.Height;
     MapPropEditor.Invalidate;
    end;
   end;
   MapModified;
  end;
 end;
end;

procedure TMainForm.UpdateMapName;
var
 Map: TMap;
 SavedIndex: Integer;
begin
 Map := TObject(MapPropEditor.PropertyList.UserTag) as TMap;
 if Map <> nil then
 begin
  if Map.Owner = FProject.FMaps then
  begin
   SavedIndex := MapsListBox.ItemIndex;
   MapsListBox.Items[Map.Index] := WideFormat('%d. %s', [Map.Index + 1, Map.Name]);
   MapsListBox.ItemIndex := SavedIndex;
  end else
  if Map.Owner = FProject.FMaps.BrushList then
  begin
   SavedIndex := BrushesListBox.ItemIndex;
   BrushesListBox.Items[Map.Index] := WideFormat('%d. %s', [Map.Index + 1, Map.Name]);
   BrushesListBox.ItemIndex := SavedIndex;
  end;
 end;
end;


procedure TMainForm.LayerPropertiesValueChange(Sender: TPropertyListItem);

 procedure CheckLinkValue(Layer: TBrushLayer; Direction: TLinkDirection);
 var
  Brush: TMapBrush;
  Seek: TBaseSectionedList;
  I: Integer;
  Str: WideString;
 begin
  Brush := Layer.Owner as TMapBrush;
  Str := Sender.ValueStr;
  I := Pos(' -> ', Str);
  if I > 0 then
  begin
   Delete(Str, 1, I + 3);
   Seek := Brush.Extends.FindByName(Str);
  end else
   Seek := Brush.FindByName(Str);
  if (Sender.ValueStr <> '') and (Seek = nil) then
  begin
   WideMessageDlg(WideFormat('Layer ''%s'' not found', [Sender.ValueStr]), mtError, [mbOk], 0);
   Sender.Changing := True;
   I := Layer.LinkIndex[Direction];
   if I >= 0 then
    Sender.ValueStr := Sender.PickList[I] else
    Sender.ValueStr := '';
   Sender.Changing := False;
   Exit;
  end;
  Layer.LinkIndex[Direction] := Sender.PickListIndex;
 end;

 procedure FixBrushDistances(Brush: TMapBrush; FixLayer: TMapLayer; ChangeX, ChangeY: Integer);
 var
  BrLayer: TBrushLayer;
  ld: TLinkDirection;
 begin
  BrLayer := Brush.RootNode as TBrushLayer;
  while BrLayer <> nil do
  begin
   if BrLayer <> FixLayer then
    for ld := Low(TLinkDirection) to High(TLinkDirection) do
    begin
     if BrLayer.Links[ld] = FixLayer then
      with BrLayer.DrawSpots[ld] do
      begin
       Dec(x, ChangeX);
       Dec(y, ChangeY);
      end;
    end;
   BrLayer := BrLayer.Next as TBrushLayer;
  end;
 end;

 procedure FixLayerDistances(Layer: TBrushLayer; ChangeX, ChangeY: Integer);
 var
  ld: TLinkDirection;
 begin
  for ld := Low(TLinkDirection) to High(TLinkDirection) do
   if Layer.Links[ld] <> nil then
    with Layer.DrawSpots[ld] do
    begin
     Inc(x, ChangeX);
     Inc(y, ChangeY);
    end;
 end;

var
 Map: TMap;
 Brush: TMapBrush;
 Layer: TMapLayer;
 I, ChangeX, ChangeY, NewX, NewY: Integer;
begin
 if Sender.Parameters and PL_MULTIPLE_VALUE = 0 then
 begin
  Map := TObject(LayersListBox.Tag) as TMap;
  if Map <> nil then
  begin
   for I := 0 to Map.Count - 1 do
    if LayersListBox.Selected[I] then
    begin
     Layer := Map.Layers[I];
     with Layer do
      case Sender.UserTag of
       0:
       begin
        Name := Sender.ValueStr;
        Sender.Changing := True;
        Sender.ValueStr := Name;
        Sender.Changing := False;
        PostMessage(Handle, WM_FILLLAYERPROPS, Integer(Map), 0);
       end;
       1: Flags := (Flags and not LF_VISIBLE) or
                   ((not Sender.PickListIndex + 1) and LF_VISIBLE);
       2: Flags := (Flags and not LF_LAYER_ACTIVE) or
                   ((not Sender.PickListIndex + 1) and LF_LAYER_ACTIVE);
       3:
       begin
        if (Sender.ValueStr <> '') and
           (FProject.FMaps.TileSetList.FindByName(Sender.ValueStr) = nil) then
        begin
         WideMessageDlg(WideFormat('No tile set named ''%s''', [Sender.ValueStr]), mtError, [mbOk], 0);
         Sender.Changing := True;
         if TileSet <> nil then
          Sender.ValueStr := TileSet.Name  else
          Sender.ValueStr := '';
         Sender.Changing := False;
         Exit;
        end;
        TileSetIndex := Sender.PickListIndex;
        TileContentModified;        
       end;
       4: FirstColor := Sender.Value;
       5: CellSize := Sender.Value;
       6: IndexShift := Sender.Value;
       7: IndexMask := Sender.Value;
       8: PaletteIndexShift := Sender.Value;
       9: PaletteIndexMask := Sender.Value;
       10: PriorityShift := Sender.Value;
       11: PriorityMask := Sender.Value;
       12: XFlipShift := Sender.Value;
       13: YFlipShift := Sender.Value;
       14: DrawInfo.TileIndex := Sender.Value;
       15: DrawInfo.PaletteIndex := Sender.Value;
       16: DrawInfo.Priority := Sender.Value;
       17: DrawInfo.XFlip := Sender.PickListIndex <> 0;
       18: DrawInfo.YFlip := Sender.PickListIndex <> 0;
       19, 20:
       with Layer as TBrushLayer do
       begin
        if Sender.UserTag = 19 then
        begin
         NewX := Sender.Value;
         NewY := HotSpotY;
        end else
        begin
         NewX := HotSpotX;
         NewY := Sender.Value;
        end;
        case WideMessageDlg('Fix distance values?', mtConfirmation,
                            [mbYes, mbNo, mbCancel], 0) of
         mrYes:
         begin
          ChangeX := HotSpotX - NewX;
          ChangeY := HotSpotY - NewY;
          HotSpotX := NewX;
          HotSpotY := NewY;
          FixLayerDistances(TBrushLayer(Layer), ChangeX, ChangeY);
          FixBrushDistances(Owner as TMapBrush, Layer, ChangeX, ChangeY);
          Brush := FProject.FMaps.BrushList.RootNode as TMapBrush;
          while Brush <> nil do
          begin
           if Owner = Brush.Extends then
            FixBrushDistances(Brush, Layer, ChangeX, ChangeY);
           Brush := Brush.Next as TMapBrush;
          end;
          PostMessage(Handle, WM_FILLLAYERPROPS, Integer(Map), 0);
         end;
         mrCancel:
         begin
          Sender.Changing := True;
          if Sender.UserTag = 19 then
           Sender.Value := HotSpotX else
           Sender.Value := HotSpotY;
          Sender.Changing := False; 
         end;
         else
         begin
          HotSpotX := NewX;
          HotSpotY := NewY;         
         end;
        end;
       end;
       21: CheckLinkValue(Layer as TBrushLayer, ldLeft);
       22: CheckLinkValue(Layer as TBrushLayer, ldRight);
       23: CheckLinkValue(Layer as TBrushLayer, ldUp);
       24: CheckLinkValue(Layer as TBrushLayer, ldDown);
       25: CheckLinkValue(Layer as TBrushLayer, ldLeftUp);
       26: CheckLinkValue(Layer as TBrushLayer, ldRightUp);
       27: CheckLinkValue(Layer as TBrushLayer, ldLeftDown);
       28: CheckLinkValue(Layer as TBrushLayer, ldRightDown);
       29:
       with Layer as TBrushLayer do
        if Sender.PickListIndex = 0 then
         Unfinished := Unfinished - [ldLeft] else
         Unfinished := Unfinished + [ldLeft];
       30:
       with Layer as TBrushLayer do
        if Sender.PickListIndex = 0 then
         Unfinished := Unfinished - [ldRight] else
         Unfinished := Unfinished + [ldRight];
       31:
       with Layer as TBrushLayer do
        if Sender.PickListIndex = 0 then
         Unfinished := Unfinished - [ldUp] else
         Unfinished := Unfinished + [ldUp];
       32:
       with Layer as TBrushLayer do
        if Sender.PickListIndex = 0 then
         Unfinished := Unfinished - [ldDown] else
         Unfinished := Unfinished + [ldDown];
       33:
       with Layer as TBrushLayer do
        if Sender.PickListIndex = 0 then
         Unfinished := Unfinished - [ldLeftUp] else
         Unfinished := Unfinished + [ldLeftUp];
       34:
       with Layer as TBrushLayer do
        if Sender.PickListIndex = 0 then
         Unfinished := Unfinished - [ldRightUp] else
         Unfinished := Unfinished + [ldRightUp];
       35:
       with Layer as TBrushLayer do
        if Sender.PickListIndex = 0 then
         Unfinished := Unfinished - [ldLeftDown] else
         Unfinished := Unfinished + [ldLeftDown];
       36:
       with Layer as TBrushLayer do
        if Sender.PickListIndex = 0 then
         Unfinished := Unfinished - [ldRightDown] else
         Unfinished := Unfinished + [ldRightDown];
       37..(37 + Ord(High(TLinkDirection))):
       with Layer as TBrushLayer do
        DrawSpots[TLinkDirection(Sender.UserTag - 37)].x := Sender.Value;
       (38 + Ord(High(TLinkDirection)))..
       (38 + Ord(High(TLinkDirection)) * 2):
       with Layer as TBrushLayer do
        DrawSpots[TLinkDirection(Sender.UserTag -
            (38 + Ord(High(TLinkDirection))))].y := Sender.Value;
       54:
       with Layer as TBrushLayer do
        if Sender.PickListIndex = 0 then
         Extended := Extended - [ldLeft] else
         Extended := Extended + [ldLeft];
       55:
       with Layer as TBrushLayer do
        if Sender.PickListIndex = 0 then
         Extended := Extended - [ldRight] else
         Extended := Extended + [ldRight];
       56:
       with Layer as TBrushLayer do
        if Sender.PickListIndex = 0 then
         Extended := Extended - [ldUp] else
         Extended := Extended + [ldUp];
       57:
       with Layer as TBrushLayer do
        if Sender.PickListIndex = 0 then
         Extended := Extended - [ldDown] else
         Extended := Extended + [ldDown];
       58:
       with Layer as TBrushLayer do
        if Sender.PickListIndex = 0 then
         Extended := Extended - [ldLeftUp] else
         Extended := Extended + [ldLeftUp];
       59:
       with Layer as TBrushLayer do
        if Sender.PickListIndex = 0 then
         Extended := Extended - [ldRightUp] else
         Extended := Extended + [ldRightUp];
       60:
       with Layer as TBrushLayer do
        if Sender.PickListIndex = 0 then
         Extended := Extended - [ldLeftDown] else
         Extended := Extended + [ldLeftDown];
       61:
       with Layer as TBrushLayer do
        if Sender.PickListIndex = 0 then
         Extended := Extended - [ldRightDown] else
         Extended := Extended + [ldRightDown];
       62:
       with Layer as TBrushLayer do
        IdentityTag := Sender.Value;
       63:
       with Layer as TBrushLayer do
        Randomize := Sender.PickListIndex <> 0;       
      end;
    end;
   case Sender.UserTag of
    0, 2, 14..18: RefreshLayersList(Map);
    1, 4..13:
    begin
     MapModified;
     RefreshLayersList(Map);
    end;
   end;
  end;
 end;
end;

procedure TMainForm.FillLayerProps(Map: TMap);
var
 ld: TLinkDirection;
 I: Integer;
 UseLayer, Layer: TMapLayer;
 BLayer: TBrushLayer absolute Layer;
 BUseLayer: TBrushLayer absolute UseLayer;
 VFlags: packed record Lo, Hi: LongInt end;
 VFlags64: Int64 absolute VFlags;

 ln: WideString;
 List: TTntStringList;
 ptp: ^TSmallPoint;
const
 ldNames: array [TLinkDirection] of WideString =
  ('Left',
   'Right',
   'Up',
   'Down',
   'Left/up',
   'Right/up',
   'Left/down',
   'Right/down');
begin
 LayersLabel.Caption := 'Layer properties';
 with LayerPropEditor, PropertyList do
 begin
  ClearList;
  UserTag := Integer(Map);
  if Map <> nil then
  begin
   VFlags64 := 0;
   UseLayer := nil;
   Layer := nil;
   for I := 0 to Map.Count - 1 do
   begin
    if LayersListBox.Selected[I] then
    begin
     Layer := Map.Layers[I];
     if UseLayer <> nil then
     begin
      if Layer.Name <> UseLayer.Name then
       VFlags.Lo := VFlags.Lo or (1 shl 0);
      if Layer.Flags and LF_VISIBLE <> UseLayer.Flags and LF_VISIBLE then
       VFlags.Lo := VFlags.Lo or (1 shl 1);
      if Layer.Flags and LF_LAYER_ACTIVE <> UseLayer.Flags and LF_LAYER_ACTIVE then
       VFlags.Lo := VFlags.Lo or (1 shl 2);
      if Layer.TileSet <> UseLayer.TileSet then
       VFlags.Lo := VFlags.Lo or (1 shl 3);
      if Layer.FirstColor <> UseLayer.FirstColor then
       VFlags.Lo := VFlags.Lo or (1 shl 4);
      if Layer.CellSize <> UseLayer.CellSize then
       VFlags.Lo := VFlags.Lo or (1 shl 5);
      if Layer.IndexShift <> UseLayer.IndexShift then
       VFlags.Lo := VFlags.Lo or (1 shl 6);
      if Layer.IndexMask <> UseLayer.IndexMask then
       VFlags.Lo := VFlags.Lo or (1 shl 7);
      if Layer.PaletteIndexShift <> UseLayer.PaletteIndexShift then
       VFlags.Lo := VFlags.Lo or (1 shl 8);
      if Layer.PaletteIndexMask <> UseLayer.PaletteIndexMask then
       VFlags.Lo := VFlags.Lo or (1 shl 9);
      if Layer.PriorityShift <> UseLayer.PriorityShift then
       VFlags.Lo := VFlags.Lo or (1 shl 10);
      if Layer.PriorityMask <> UseLayer.PriorityMask then
       VFlags.Lo := VFlags.Lo or (1 shl 11);
      if Layer.XFlipShift <> UseLayer.XFlipShift then
       VFlags.Lo := VFlags.Lo or (1 shl 12);
      if Layer.YFlipShift <> UseLayer.YFlipShift then
       VFlags.Lo := VFlags.Lo or (1 shl 13);
      if Layer.Flags and LF_TILE_SELECTED = 0 then
       Layer.DrawInfo.TileIndex := -1;
      if UseLayer.Flags and LF_TILE_SELECTED = 0 then
       UseLayer.DrawInfo.TileIndex := -1;
      if (Layer.DrawInfo.TileIndex <> UseLayer.DrawInfo.TileIndex) then
       VFlags.Lo := VFlags.Lo or (1 shl 14);
      if Layer.DrawInfo.PaletteIndex <> UseLayer.DrawInfo.PaletteIndex then
       VFlags.Lo := VFlags.Lo or (1 shl 15);
      if Layer.DrawInfo.Priority <> UseLayer.DrawInfo.Priority then
       VFlags.Lo := VFlags.Lo or (1 shl 16);
      if Layer.DrawInfo.XFlip <> UseLayer.DrawInfo.XFlip then
       VFlags.Lo := VFlags.Lo or (1 shl 17);
      if Layer.DrawInfo.YFlip <> UseLayer.DrawInfo.YFlip then
       VFlags.Lo := VFlags.Lo or (1 shl 18);
      if Map is TMapBrush then
      begin
       if BLayer.IdentityTag <> BUseLayer.IdentityTag then
        VFlags.Hi := VFlags.Hi or (1 shl (62 - 32));

       if BLayer.Randomize <> BUseLayer.Randomize then
        VFlags.Hi := VFlags.Hi or (1 shl (63 - 32));

       if BLayer.HotSpotX <> BUseLayer.HotSpotX then
        VFlags.Lo := VFlags.Lo or (1 shl 19);
       if BLayer.HotSpotY <> BUseLayer.HotSpotY then
        VFlags.Lo := VFlags.Lo or (1 shl 20);
       if (BLayer.LinkLeft <> BUseLayer.LinkLeft) then
        VFlags.Lo := VFlags.Lo or (1 shl 21);
       if (BLayer.LinkRight <> BUseLayer.LinkRight) then
        VFlags.Lo := VFlags.Lo or (1 shl 22);
       if (BLayer.LinkUp <> BUseLayer.LinkUp) then
        VFlags.Lo := VFlags.Lo or (1 shl 23);
       if (BLayer.LinkDown <> BUseLayer.LinkDown) then
        VFlags.Lo := VFlags.Lo or (1 shl 24);
       if (BLayer.LinkLeftUp <> BUseLayer.LinkLeftUp) then
        VFlags.Lo := VFlags.Lo or (1 shl 25);
       if (BLayer.LinkRightUp <> BUseLayer.LinkRightUp) then
        VFlags.Lo := VFlags.Lo or (1 shl 26);
       if (BLayer.LinkLeftDown <> BUseLayer.LinkLeftDown) then
        VFlags.Lo := VFlags.Lo or (1 shl 27);
       if (BLayer.LinkRightDown <> BUseLayer.LinkRightDown) then
        VFlags.Lo := VFlags.Lo or (1 shl 28);

       if (ldLeft in BLayer.Unfinished) <>
          (ldLeft in BUseLayer.Unfinished) then
        VFlags.Lo := VFlags.Lo or (1 shl 29);
       if (ldRight in BLayer.Unfinished) <>
          (ldRight in BUseLayer.Unfinished) then
        VFlags.Lo := VFlags.Lo or (1 shl 30);
       if (ldUp in BLayer.Unfinished) <>
          (ldUp in BUseLayer.Unfinished) then
        VFlags.Lo := VFlags.Lo or (1 shl 31);
       if (ldDown in BLayer.Unfinished) <>
          (ldDown in BUseLayer.Unfinished) then
        VFlags.Hi := VFlags.Hi or (1 shl 0);
       if (ldLeftUp in BLayer.Unfinished) <>
          (ldLeftUp in BUseLayer.Unfinished) then
        VFlags.Hi := VFlags.Hi or (1 shl 1);
       if (ldRightUp in BLayer.Unfinished) <>
          (ldRightUp in BUseLayer.Unfinished) then
        VFlags.Hi := VFlags.Hi or (1 shl 2);
       if (ldLeftDown in BLayer.Unfinished) <>
          (ldLeftDown in BUseLayer.Unfinished) then
        VFlags.Hi := VFlags.Hi or (1 shl 3);
       if (ldRightDown in BLayer.Unfinished) <>
          (ldRightDown in BUseLayer.Unfinished) then
        VFlags.Hi := VFlags.Hi or (1 shl 4);

        if (ldLeft in BLayer.Extended) <>
          (ldLeft in BUseLayer.Extended) then
        VFlags.Hi := VFlags.Hi or (1 shl (54 - 32));
       if (ldRight in BLayer.Extended) <>
          (ldRight in BUseLayer.Extended) then
        VFlags.Hi := VFlags.Hi or (1 shl (55 - 32));
       if (ldUp in BLayer.Extended) <>
          (ldUp in BUseLayer.Extended) then
        VFlags.Hi := VFlags.Hi or (1 shl (56 - 32));
       if (ldDown in BLayer.Extended) <>
          (ldDown in BUseLayer.Extended) then
        VFlags.Hi := VFlags.Hi or (1 shl (57 - 32));
       if (ldLeftUp in BLayer.Extended) <>
          (ldLeftUp in BUseLayer.Extended) then
        VFlags.Hi := VFlags.Hi or (1 shl (58 - 32));
       if (ldRightUp in BLayer.Extended) <>
          (ldRightUp in BUseLayer.Extended) then
        VFlags.Hi := VFlags.Hi or (1 shl (59 - 32));
       if (ldLeftDown in BLayer.Extended) <>
          (ldLeftDown in BUseLayer.Extended) then
        VFlags.Hi := VFlags.Hi or (1 shl (60 - 32));
       if (ldRightDown in BLayer.Extended) <>
          (ldRightDown in BUseLayer.Extended) then
        VFlags.Hi := VFlags.Hi or (1 shl (61 - 32));
       for ld := Low(TLinkDirection) to High(TLinkDirection) do
       begin
        ptp := Addr(BUseLayer.DrawSpots[ld]);
        with BLayer.DrawSpots[ld] do
        begin
         if x <> ptp.x then
          VFlags.Hi := VFlags.Hi or (1 shl (5 + Ord(ld)));
         if y <> ptp.y then
          VFlags.Hi := VFlags.Hi or (1 shl (6 + Ord(High(TLinkDirection)) + Ord(ld)));
        end;
       end;
      end;
     end else
      UseLayer := Layer;
    end;
   end;

   if UseLayer <> nil then
    with UseLayer do
    begin

     if UseLayer = Layer then
      LayersLabel.Caption := WideFormat('Layer %d properties (%s %d)',
      [UseLayer.Index + 1, MapTypeStr[Map.Owner = FProject.FMaps.BrushList], Map.Index + 1]) else
      LayersLabel.Caption := WideFormat('Selected layers'' properties (%s %d)',
      [MapTypeStr[Map.Owner = FProject.FMaps.BrushList], Map.Index + 1]);

     with AddString('Name', Name) do
     begin
      if VFlags.Lo and 1 <> 0 then
       Parameters := PL_MULTIPLE_VALUE;
      UserTag := 0;
     end;

     List := TTntStringList.Create;
     try
      with FProject.FMaps.TileSetList do
       for I := 0 to Count - 1 do
        List.Add(Sets[I].Name);

      List.CaseSensitive := True;
      with AddPickList('Tile set', List, False, TileSetIndex, False) do
      begin
       if VFlags.Lo and (1 shl 3) <> 0 then
        Parameters := PL_MULTIPLE_VALUE;
       UserTag := 3;
      end;
     finally
      List.Free;
     end;

     if Map is TMapBrush then
     begin
      with AddString('Brush data', '(...)', False).SubProperties do
      begin
       with AddDecimal('Hot spot X', BUseLayer.HotSpotX, Low(SmallInt), High(SmallInt)) do
       begin
        if VFlags.Lo and (1 shl 19) <> 0 then
         Parameters := PL_MULTIPLE_VALUE;
        UserTag := 19;
       end;
       with AddDecimal('Hot spot Y', BUseLayer.HotSpotY, Low(SmallInt), High(SmallInt)) do
       begin
        if VFlags.Lo and (1 shl 20) <> 0 then
         Parameters := PL_MULTIPLE_VALUE;
        UserTag := 20;
       end;

       with AddDecimal('Identity tag', BUseLayer.IdentityTag, Low(LongInt), MaxInt) do
       begin
        if VFlags.Hi and (1 shl (62 - 32)) <> 0 then
         Parameters := PL_MULTIPLE_VALUE;
        UserTag := 62;
       end;

       with AddBoolean('Randomize', BUseLayer.Randomize) do
       begin
        if VFlags.Hi and (1 shl (63 - 32)) <> 0 then
         Parameters := Parameters or PL_MULTIPLE_VALUE;
        UserTag := 63;
       end;

       List := TTntStringList.Create;
       try
        for I := 0 to Map.Count - 1 do
          List.Add(Map.Layers[I].Name);

        with Map as TMapBrush do
         if Extends <> nil then
          for I := 0 to Extends.Count - 1 do
           List.Add(WideFormat('%s -> %s', [Extends.Name, Extends.Layers[I].Name]));

        List.CaseSensitive := True;
        for ld := Low(TLinkDirection) to High(TLinkDirection) do
        begin
         with AddBoolean(WideFormat('%s unfinished', [ldNames[ld]]), ld in BUseLayer.Unfinished) do
         begin
          UserTag := 29 + Ord(ld);
          if VFlags64 and (Int64(1) shl UserTag) <> 0 then
           Parameters := Parameters or PL_MULTIPLE_VALUE;
         end;
         with AddPickList(WideFormat('%s link', [ldNames[ld]]), List, False,
              BUseLayer.LinkIndex[ld], False) do
         begin
          UserTag := 21 + Ord(ld);
          if VFlags.Lo and (1 shl UserTag) <> 0 then
           Parameters := Parameters or PL_MULTIPLE_VALUE;
         end;

         with AddBoolean('  Extended', ld in BUseLayer.Extended) do
         begin
          UserTag := 54 + Ord(ld);
          if VFlags64 and (Int64(1) shl UserTag) <> 0 then
           Parameters := Parameters or PL_MULTIPLE_VALUE;
         end;

         ptp := Addr(BUseLayer.DrawSpots[ld]);
         with AddDecimal('  Distance X', ptp.x, Low(SmallInt), High(SmallInt)) do
         begin
          I := 5 + Ord(ld);
          if VFlags.Hi and (1 shl I) <> 0 then
           Parameters := PL_MULTIPLE_VALUE;
          UserTag := 32 + I;
         end;
         with AddDecimal('  Distance Y', ptp.y, Low(SmallInt), High(SmallInt)) do
         begin
          I := 6 + Ord(High(TLinkDirection)) + Ord(ld);
          if VFlags.Hi and (1 shl I) <> 0 then
           Parameters := PL_MULTIPLE_VALUE;
          UserTag := 32 + I;
         end;
        end;
       finally
        List.Free;
       end;
      end;
     end;

     with AddBoolean('Visible', Flags and LF_VISIBLE <> 0) do
     begin
      if VFlags.Lo and (1 shl 1) <> 0 then
       Parameters := Parameters or PL_MULTIPLE_VALUE;
      UserTag := 1;
     end;

     with AddBoolean('Drawing enabled', Flags and LF_LAYER_ACTIVE <> 0) do
     begin
      if VFlags.Lo and (1 shl 2) <> 0 then
          Parameters := Parameters or PL_MULTIPLE_VALUE;
      UserTag := 2;
      with SubProperties do
      begin

       with AddDecimal('Tile index', DrawInfo.TileIndex, -1, MaxInt) do
       begin
        if VFlags.Lo and (1 shl 14) <> 0 then
         Parameters := PL_MULTIPLE_VALUE;
        UserTag := 14;
       end;

       with AddDecimal('Palette index', DrawInfo.PaletteIndex, -1, MaxInt) do
       begin
        if VFlags.Lo and (1 shl 15) <> 0 then
         Parameters := PL_MULTIPLE_VALUE;
        UserTag := 15;
       end;

       with AddDecimal('Priority', DrawInfo.Priority, -1, MaxInt) do
       begin
        if VFlags.Lo and (1 shl 16) <> 0 then
         Parameters := PL_MULTIPLE_VALUE;
        UserTag := 16;
       end;

       with AddBoolean('Horizontal flip', DrawInfo.XFlip) do
       begin
        if VFlags.Lo and (1 shl 17) <> 0 then
         Parameters := Parameters or PL_MULTIPLE_VALUE;
        UserTag := 17;
       end;

       with AddBoolean('Vertical flip', DrawInfo.YFlip) do
       begin
        if VFlags.Lo and (1 shl 18) <> 0 then
         Parameters := Parameters or PL_MULTIPLE_VALUE;
        UserTag := 18;
       end;
      end;
     end;

     with AddString('Layer format', '(...)') do
     begin
      ReadOnly := True;
      with SubProperties do
      begin

       with AddDecimal('Cell size', CellSize, 0, 8) do
       begin
        if VFlags.Lo and (1 shl 5) <> 0 then
         Parameters := PL_MULTIPLE_VALUE;
        UserTag := 5;
       end;

       with AddDecimal('Tile index shift', IndexShift, 0, 63) do
       begin
        if VFlags.Lo and (1 shl 6) <> 0 then
         Parameters := PL_MULTIPLE_VALUE;
        UserTag := 6;
       end;

       with AddHexadecimal('Tile index mask', IndexMask, 0, High(LongWord), 8) do
       begin
        if VFlags.Lo and (1 shl 7) <> 0 then
         Parameters := PL_MULTIPLE_VALUE;
        UserTag := 7;
       end;

       with AddDecimal('Palette index shift', PaletteIndexShift, -1, 63) do
       begin
        if VFlags.Lo and (1 shl 8) <> 0 then
         Parameters := PL_MULTIPLE_VALUE;
        UserTag := 8;
       end;

       with AddHexadecimal('Palette index mask', PaletteIndexMask, 0, High(LongWord), 8) do
       begin
        if VFlags.Lo and (1 shl 9) <> 0 then
         Parameters := PL_MULTIPLE_VALUE;
        UserTag := 9;
       end;

       with AddDecimal('Priority shift', PriorityShift, -1, 63) do
       begin
        if VFlags.Lo and (1 shl 10) <> 0 then
         Parameters := PL_MULTIPLE_VALUE;
        UserTag := 10;
       end;

       with AddHexadecimal('Priority mask', PriorityMask, 0, High(LongWord), 8) do
       begin
        if VFlags.Lo and (1 shl 11) <> 0 then
         Parameters := PL_MULTIPLE_VALUE;
        UserTag := 11;
       end;

       with AddDecimal('Horizontal flip shift ', XFlipShift, -1, 63) do
       begin
        if VFlags.Lo and (1 shl 12) <> 0 then
         Parameters := PL_MULTIPLE_VALUE;
        UserTag := 12;
       end;

       with AddDecimal('Vertical flip shift', YFlipShift, -1, 63) do
       begin
        if VFlags.Lo and (1 shl 13) <> 0 then
         Parameters := PL_MULTIPLE_VALUE;
        UserTag := 13;
       end;
      end;
     end;

     with AddDecimal('1st color index', FirstColor, 0, 255) do
     begin
      if VFlags.Lo and (1 shl 4) <> 0 then
        Parameters := PL_MULTIPLE_VALUE;
      UserTag := 4;
     end;
    end;


   {AddString('Name', MapName);
   AddDecimal('Map width', Width, 0, 1000);
   AddDecimal('Map height', Height, 0, 1000);
   AddDecimal('Tile width', TileWidth, 0, 256);
   AddDecimal('Tile height', TileHeight, 0, 256);
   AddDecimal('Layers count', Count, 0, MaxInt).ReadOnly := True; }
  end;
  RootNodeCount := Count;

 end;
end;

procedure TMainForm.TileSetImageFlagsValueChange(Sender: TPropertyListItem);
var
 Flags: TCompositeFlags;
 Old: Word;
 I, BCnt, Idx: Integer;
begin
 Idx := Sender.UserTag - 10;
 Flags := TilesFrame.TileSet.TileBitmap.FlagsList;
 if Sender.Value and 31 > 7 then
 begin
  WideMessageDlg(WideFormat('Direct color is unsupported.', [Sender.ValueStr]),
                 mtError, [mbOk], 0);
  Sender.Value := Flags[Idx];
 end else
 begin
  Old := Flags[Idx];
  Flags[Idx] := Sender.Value;
  BCnt := Length(Flags);
  for I := 0 to Length(Flags) - 1 do
   Inc(BCnt, Flags[I] and 31);
  if BCnt > 8 then
  begin
   WideMessageDlg(WideFormat('Direct color is unsupported.', [Sender.Value]),
                mtError, [mbOk], 0);
   Sender.Value := Old;
  end else
  begin
   BCnt := Sender.Value and 31 + 1;
   I := (Sender.Value shr PLANE_FORMAT_SHIFT) and 3;
   while 1 shl I > BCnt do
    Dec(I);
   Flags[Idx] := (Flags[Idx] and PLANE_FORMAT_MASK) or
                          (I shl PLANE_FORMAT_SHIFT);

   TilesFrame.TileSet.SwitchFormat(Flags);
   Saved := False;
   TileContentModified;
  end;
 end;
end;

procedure TMainForm.PropUpActionUpdate(Sender: TObject);
var
 Data: PPropertyData;
 Enable: Boolean;
begin
 Enable := TileSetPropEditor.FocusedNode <> nil;
 if Enable then
 begin
  Data := TileSetPropEditor.GetNodeData(TileSetPropEditor.FocusedNode);
  if Data.Owner <> nil then
  case Data.Item.UserTag of
   9:      Enable := Data.Item.Index > 0;
   11..41: Enable := True;
   else    Enable := False;
  end;
 end;
 PropUpAction.Enabled := Enable;
end;

procedure TMainForm.PropDownActionUpdate(Sender: TObject);
var
 Data: PPropertyData;
 Enable: Boolean;
begin
 Enable := TileSetPropEditor.FocusedNode <> nil;
 if Enable then
 begin
  Data := TileSetPropEditor.GetNodeData(TileSetPropEditor.FocusedNode);
  if Data.Owner <> nil then
  case Data.Item.UserTag of
   9:      Enable := Data.Item.Index < Data.Owner.Count - 1;
   10..40: Enable := (TilesFrame.TileSet <> nil) and
                     (Data.Item.UserTag - 10 <
                      Length(TilesFrame.TileSet.TileBitmap.CompositeFlags));
   else    Enable := False;
  end;
 end;
 PropDownAction.Enabled := Enable;
end;

procedure TMainForm.PropDeleteActionUpdate(Sender: TObject);
var
 Data: PPropertyData;
 Enable: Boolean;
begin
 Enable := TileSetPropEditor.FocusedNode <> nil;
 if Enable then
 begin
  Data := TileSetPropEditor.GetNodeData(TileSetPropEditor.FocusedNode);
  if Data.Owner <> nil then
  case Data.Item.UserTag of
   9, 11..41: Enable := True;
   10:        Enable := (TilesFrame.TileSet <> nil) and
                        (TilesFrame.TileSet.TileBitmap.CompositeFlags <> nil);
   else       Enable := False;
  end;
 end;
 PropDeleteAction.Enabled := Enable;
end;

procedure TMainForm.PropUpActionExecute(Sender: TObject);
begin
 TileSetPropEditor.PropertyUp(TileSetPropEditor.FocusedNode);
end;

procedure TMainForm.PropDownActionExecute(Sender: TObject);
begin
 TileSetPropEditor.PropertyDown(TileSetPropEditor.FocusedNode);
end;

procedure TMainForm.PropDeleteActionExecute(Sender: TObject);
begin
 TileSetPropEditor.PropertyRemove(TileSetPropEditor.FocusedNode);
end;

procedure TMainForm.TileSetPropEditorPropertyUp(
  Sender: TCustomPropertyEditor; Node: PVirtualNode; var Yes: Boolean);
var
 Data: PPropertyData;
 Idx: Integer;
 Value: Word;
 Flg: TCompositeFlags;
begin
 Data := Sender.GetNodeData(Node);
 if (Data <> nil) and (Data.Owner <> nil) then
 case Data.Item.UserTag of
  9:
  begin
   TilesFrame.TileSet.MoveAttr(Data.Item.Index, Data.Item.Index - 1);
   TilesFrameTileSetViewSelectionChanged(TilesFrame.TileSetView);
   Saved := False;
   Exit;
  end;
  11..41:
  with TilesFrame.TileSet do
  begin
   Idx := Data.Item.UserTag - 10;
   Flg := TileBitmap.FlagsList;
   Value := Flg[Idx];
   Flg[Idx] := Flg[Idx - 1];
   Flg[Idx - 1] := Value;

   SwitchFormat(Flg);
   Sender.InvalidateNode(Node.Parent);
   Saved := False;
   TileContentModified;
   Exit;
  end;
 end;
 Yes := False; 
end;

procedure TMainForm.TileSetPropEditorPropertyDown(
  Sender: TCustomPropertyEditor; Node: PVirtualNode; var Yes: Boolean);
var
 Data: PPropertyData;
 Idx: Integer;
 Value: Word;
 Flg: TCompositeFlags;
begin
 Data := Sender.GetNodeData(Node);
 if (Data <> nil) and (Data.Owner <> nil) then
 case Data.Item.UserTag of
  9:
  begin
   TilesFrame.TileSet.MoveAttr(Data.Item.Index, Data.Item.Index + 1);
   TilesFrameTileSetViewSelectionChanged(TilesFrame.TileSetView);
   Saved := False;
   Exit;
  end;
  10..40:
  with TilesFrame.TileSet do
  begin
   Idx := Data.Item.UserTag - 10;
   Flg := TileBitmap.FlagsList;
   if Idx < Length(Flg) - 1 then
   begin
    Value := Flg[Idx];
    Flg[Idx] := Flg[Idx + 1];
    Flg[Idx + 1] := Value;

    SwitchFormat(Flg);
    Sender.InvalidateNode(Node.Parent);
    Saved := False;
    TileContentModified;
    Exit;
   end;
  end;
 end;
 Yes := False;
end;

procedure TMainForm.PropAddActionUpdate(Sender: TObject);
var
 Data: PPropertyData;
 Enable: Boolean;
begin
 Enable := (TilesFrame.TileSet <> nil) and
           (TileSetPropEditor.FocusedNode <> nil);
 if Enable then
 begin
  Data := TileSetPropEditor.GetNodeData(TileSetPropEditor.FocusedNode);
  Enable := (Data.Owner <> nil) and
           ((Data.Item.UserTag in [7..41]) or (Data.Item.UserTag = -1));
 end;
 PropAddAction.Enabled := Enable;
end;

procedure TMainForm.PropAddActionExecute(Sender: TObject);
var
 Data: PPropertyData;
 L: Integer;
 Flg: TCompositeFlags;
begin
 if TilesFrame.TileSet <> nil then
  with TileSetPropEditor do
  begin
   Data := GetNodeData(FocusedNode);
   if (Data <> nil) and (Data.Owner <> nil)  then
   with TilesFrame.TileSet do
   case Data.Item.UserTag of
    -1, 7, 10..41:
    with TileBitmap, TileSetPropEditor do
    begin
     if TileBitmap.BitsCount < 8 then
     begin
      Flg := TileBitmap.FlagsList;
      SetLength(Flg, Length(Flg) + 1);
      GetRoute(FocusedNode, LastRoute);

      L := Length(LastRoute);
      case Data.Item.UserTag of
       7: Inc(LastRoute[L - 1], Length(Flg));
       10..41: LastRoute[L - 1] := 4 + Length(Flg) - 1;
       else
       begin
        SetLength(LastRoute, L - 1);
        Inc(LastRoute[L - 2]);
       end; 
      end;

      SwitchFormat(Flg);
      UserSelect := True;
      TilesBoxUpdate(True);
      UserSelect := False;
      TilesFrameTileSetViewSelectionChanged(TilesFrame.TileSetView);
      LayersListBox.Invalidate;
      MapFrame.MapModified := True;
      BrushFrame.MapModified := True;
     end else
      WideMessageDlg('Direct color is unsupported.', mtError, [mbOk], 0);
    end;
    8, 9:
    if AttrCount < 64 then
    begin
     AttrCount := AttrCount + 1;
     GetRoute(FocusedNode, LastRoute);
     L := Length(LastRoute);
     if Data.Item.UserTag = 8 then
      SetLength(LastRoute, L + 1) else
      Dec(L);
     LastRoute[L] := AttrCount - 1;
     UserSelect := True;
     TilesFrameTileSetChange(TilesFrame);
     UserSelect := False;
     TilesFrameTileSetViewSelectionChanged(TilesFrame.TileSetView);
    end else
     WideMessageDlg('Reached the limit of 64 attributes per tile set.',
                    mtError, [mbOk], 0);
   end;
  end;
end;        *)

procedure TMainForm.RefreshGraphicContent(TileSetRefresh: Boolean);
var
 I: Integer;
begin
 for I := 0 to Length(MapFramesList) - 1 do
  with MapFramesList[I] do
  begin
   MapFrame.MapEditView.Invalidate;
   LayersListBox.Invalidate;
  end;

 if TileSetRefresh then
 begin
  if MainFrame is TTileEditFrame then
   with TTileEditFrame(MainFrame) do
    RefreshImage(True);

  for I := 0 to Length(TileSetFramesList) - 1 do
   TileSetFramesList[I].TilesFrame.TileSetView.Invalidate;
 end;
end;

procedure TMainForm.MapFrameMapEditViewSelectionChanged(Sender: TCustomTileMapView);
var
 Frame: TMapFrame;
begin
 Frame := Sender.Owner as TMapFrame;
 Frame.MapEditViewSelectionChanged(Sender);
 if not FCellPropsFillPosted and
    ((Frame.Owner as TMapsFrame).PageControl.ActivePageIndex <> 0) then
 begin
{  Test := TObject(SelCellPropEditor.PropertyList.UserTag) as TMapFrame;
  if ((Test = nil) or (Test = Frame) or (Test.MapData = Frame.MapData)) and
     ((Frame.Owner as TMapsFrame).PageControl.ActivePageIndex = 0) then
  begin
   Test := Frame;
   Frame := nil;
   for I := 0 to Length(MapFramesList) - 1 do
    with MapFramesList[I] do
     if (Test <> MapFrame) and
        (PageControl.ActivePageIndex <> 0) then
      Frame := MapFrame;
  end else   }
  FCellPropsFillPosted := True;
  PostMessage(Handle, WM_FILLCELLPROPS, Integer(Frame), 0);
 end;
end;

procedure TMainForm.WMFillCellProps(var Message: TMessage);
var
 X, Y, I: Integer;
 Info, UseInfo: TCellInfo;
 Use: Boolean;
 VFlags: LongWord;
 Rect: TRect;
 Ptr: PByte;
 MapFrame: TMapFrame;
begin
// Exclude(FPostStates, medFillCellPropsPosted);
 FCellPropsFillPosted := False;
 MapFrame := TObject(Message.WParam) as TMapFrame;

 with SelCellPropEditor do
 begin
  ClearList;
  PropertyList.UserTag := 0;
  SelCellPropLabel.Caption := 'Cell properties';
  if MapFrame <> nil then
  begin
   with MapFrame do
    if (MapData <> nil) and (MapEditView.SomethingSelected) and
       (MapEditView.PasteBuffer = nil) and
       (not (MapFrame.Owner is TMapsFrame) or
       (TMapsFrame(Owner).PageControl.ActivePageIndex <> 0)) then
    with PropertyList do
    begin
     UserTag := Integer(MapFrame);
     Rect := MapEditView.SelectionRect;
     Rect.Left := Max(Rect.Left, 0);
     Rect.Right := Min(MapData.Width, Rect.Right);
     Rect.Top := Max(Rect.Top, 0);
     Rect.Bottom := Min(MapData.Height, Rect.Bottom);
     if (Rect.Right - Rect.Left > 0) and
        (Rect.Bottom - Rect.Top > 0) then
     begin
      if (Rect.Right - Rect.Left = 1) and
         (Rect.Bottom - Rect.Top = 1) then
      SelCellPropLabel.Caption := WideFormat('%s #%d, cell (%d, %d)',
                                             [MapTypeStr[MapFrame.BrushMode],
                                             MapData.Index + 1,
                                             Rect.Left,
                                             Rect.Top]) else
      SelCellPropLabel.Caption := WideFormat('%s #%d, cells ((%d, %d), (%d, %d))',
                                             [MapTypeStr[MapFrame.BrushMode],
                                                        MapData.Index + 1,
                                                        Rect.Left,
                                                        Rect.Top,
                                                        Rect.Right - 1,
                                                        Rect.Bottom - 1]);

      for I := 0 to MapData.Count - 1 do
       with MapData.Layers[I] do
       begin
        VFlags := 0;
        Use := False;

        AddString(WideFormat('Layer #%d', [Index + 1]), Name, False);

        for Y := Rect.Top to Rect.Bottom - 1 do
        begin
         Ptr := LayerData;
         Inc(Ptr, (Y * MapData.Width + Rect.Left) * CellSize);
         for X := Rect.Left to Rect.Right - 1 do
         begin
          if MapEditView.Selected[X, Y] then
          begin
           ReadCellInfo(Ptr^, Info);         
           if Use then
           begin
            if Info.TileIndex <> UseInfo.TileIndex then
             VFlags := VFlags or (1 shl 0);
            if Info.PaletteIndex <> UseInfo.PaletteIndex then
             VFlags := VFlags or (1 shl 1);
            if Info.Priority <> UseInfo.Priority then
             VFlags := VFlags or (1 shl 2);
            if Info.XFlip <> UseInfo.XFlip then
             VFlags := VFlags or (1 shl 3);
            if Info.YFlip <> UseInfo.YFlip then
             VFlags := VFlags or (1 shl 4);
           end else
           begin
            Use := True;
            UseInfo := Info;
           end; 
          end;
          Inc(Ptr, CellSize);
         end;
        end;

        with AddDecimal(' Tile index', UseInfo.TileIndex, 0, IndexMask) do
        begin
         if VFlags and (1 shl 0) <> 0 then
          Parameters := Parameters or PL_MULTIPLE_VALUE;
         UserTag := I * 5 + 0;
        end;

        if PaletteIndexShift >= 0 then
         with AddDecimal(' Palette index', UseInfo.PaletteIndex, 0, PaletteIndexMask) do
         begin
          if VFlags and (1 shl 1) <> 0 then
           Parameters := Parameters or PL_MULTIPLE_VALUE;
          UserTag := I * 5 + 1;
         end;

        if PriorityShift >= 0 then
         with AddDecimal(' Priority', UseInfo.Priority, 0, PriorityMask) do
         begin
          if VFlags and (1 shl 2) <> 0 then
           Parameters := Parameters or PL_MULTIPLE_VALUE;
          UserTag := I * 5 + 2;
         end;

        if XFlipShift >= 0 then
         with AddBoolean(' Horizontal flip', UseInfo.XFlip) do
         begin
          if VFlags and (1 shl 3) <> 0 then
           Parameters := Parameters or PL_MULTIPLE_VALUE;
          UserTag := I * 5 + 3;
         end;

        if YFlipShift >= 0 then
         with AddBoolean(' Vertical flip', UseInfo.YFlip) do
         begin
          if VFlags and (1 shl 4) <> 0 then
           Parameters := Parameters or PL_MULTIPLE_VALUE;
          UserTag := I * 5 + 4;
         end;

       end;
     end;
    end;
  end;

  RootNodeCount := PropertyList.Count;
 end;
end;
                                   (*
procedure TMainForm.WMFillTileProps(var Message: TMessage);
var
 UseTile, Tile, PrevTile: TTileItem;
 TilesFrame: TTilesFrame;
 VFlags: Word;
 VFlags64: Int64;
 Rect: TRect;
 AllEmpty: Boolean;
 X, Y, I, Idx, Cnt: Integer;
begin
 Exclude(FPostStates, medFillTilePropsPosted);
 TilesFrame := TObject(Message.WParam) as TTilesFrame;

 with SelTilePropEditor do
 begin
  ClearList;
  PropertyList.UserTag := 0;
  PaletteFrame.SelectColorRange(-1, 0);
  SelTilePropLabel.Caption := 'Tile properties';
  if TilesFrame <> nil then
  begin
   with TilesFrame do
    if (TileSet <> nil) and
       TileSetView.SomethingSelected and
       (TileSetView.PasteBuffer = nil) then
    begin
     PropertyList.UserTag := Integer(TilesFrame);
     Rect := TileSetView.SelectionRect;
     Rect.Left := Max(Rect.Left, 0);
     Rect.Right := Min(TileSetView.MapWidth, Rect.Right);
     Rect.Top := Max(Rect.Top, 0);
     Rect.Bottom := Min(TileSetView.MapHeight, Rect.Bottom);
     if (Rect.Right - Rect.Left > 0) and
        (Rect.Bottom - Rect.Top > 0) then
     begin
      if (Rect.Right - Rect.Left = 1) and
         (Rect.Bottom - Rect.Top = 1) then
       SelTilePropLabel.Caption := WideFormat('Tile set #%d, tile #%d',
                                              [TileSet.Index + 1,
                                               Rect.Top * TileSetView.MapWidth +
                                               Rect.Left]) else
       SelTilePropLabel.Caption := WideFormat('Tile set #%d, tiles ((%d, %d), (%d, %d))',
                                               [TileSet.Index + 1,
                                                 Rect.Left,
                                                 Rect.Top,
                                                 Rect.Right - 1,
                                                 Rect.Bottom - 1]);
      VFlags := 0;
      VFlags64 := 0;
      UseTile := nil;
      PrevTile := nil;
      AllEmpty := True;
      Cnt := 0;
      for Y := Rect.Top to Rect.Bottom - 1 do
      begin
       Idx := Y * TileSetView.MapWidth + Rect.Left;
       for X := Rect.Left to Rect.Right - 1 do
       begin
        if TileSetView.Selected[X, Y] then
        begin
         Inc(Cnt);
         Tile := TileSet.Indexed[Idx];
         if Tile <> TileSet.EmptyTile then
          AllEmpty := False;

         if PrevTile <> nil then
         begin
      //    if Idx <> PrevTile.TileIndex then
       //    VFlags := VFlags or 1;
          if (Tile = TileSet.EmptyTile) <> (PrevTile = TileSet.EmptyTile) then
           VFlags := VFlags or 2;
          if Tile.FirstColor <> PrevTile.FirstColor then
           VFlags := VFlags or 4;
          for I := 0 to TileSet.AttrCount - 1 do
           if Tile.Attributes[I] <> PrevTile.Attributes[I] then
            VFlags64 := VFlags64 or (1 shl I);
         end;
         if Tile <> TileSet.EmptyTile then
          UseTile := Tile;
         PrevTile := Tile; 
        end;
        Inc(Idx);
       end;
      end;

      if UseTile = nil then
       UseTile := PrevTile;

      if UseTile <> nil then
      begin

       if AllEmpty and (Cnt > 1) then
        VFlags64 := -1;
  {
       Idx := UseTile.TileIndex;
       if Idx < 0 then
       begin
        if (Cnt > 1) and (GetCapture <> 0) then
         VFlags := VFlags or 1 else
         Idx := SelectedTile;
       end;     }

    {   with PropertyList.AddDecimal('Tile index', Idx, 0, TileSet.LastIndex) do
       begin
        if VFlags and 1 <> 0 then
         Parameters := Parameters or PL_MULTIPLE_VALUE;
        UserTag := -1;
       end;      }
       with PropertyList.AddBoolean('Empty', UseTile = TileSet.EmptyTile) do
       begin
        if VFlags and 2 <> 0 then
         Parameters := Parameters or PL_MULTIPLE_VALUE;
        UserTag := -2;
       end;
       with PropertyList.AddDecimal('1st color index', UseTile.FirstColor, 0, 255) do
       begin
        if VFlags and 4 <> 0 then
        begin
         Parameters := Parameters or PL_MULTIPLE_VALUE;
         PaletteFrame.SelectColorRange(-1, 0);
        end else
        if TileSet.DrawFlags and DRAW_PIXEL_MODIFY <> 0 then
         PaletteFrame.SelectColorRange(UseTile.FirstColor,
                            1 shl TileSet.TileBitmap.BitsCount) else
         PaletteFrame.SelectColorRange(-1, 0);                            
        UserTag := -3;
       end;
       if (UseTile <> TileSet.EmptyTile) or (VFlags and 2 <> 0) then
        ShowTileProperties(TileSet, UseTile, VFlags64);
      end;
     end;
    end;
  end;

  RootNodeCount := PropertyList.Count;
 end;
end;
                          *)

procedure TMainForm.TilesFrameTileSetViewSelectionChanged(
  Sender: TCustomTileMapView);
var
 Frame: TTilesFrame;
begin
 Frame := Sender.Owner as TTilesFrame;
 with Frame do
 begin
  TileSetViewSelectionChanged(Sender);
  if TileSet <> nil then
   FProject.FTileSetInfo[TileSet.Index].SelectedTile := SelectedTile;
 end;

 TilePropsFrm.ShowSelectedTileProps(Frame);
         (*
 if not (medFillTilePropsPosted in FPostStates) and
 ((Frame.Owner as TTileSetsFrame).PageControl.ActivePageIndex <> 0) then
//    (FFocusedTilesFrame = Frame){.TileSetView.Focused} then
 begin
 { Test := TObject(SelTilePropEditor.PropertyList.UserTag) as TTilesFrame;
  if ((Test = nil) or (Test = Frame) or (Test.TileSet = Frame.TileSet)) and
     ((Frame.Owner as TTileSetsFrame).PageControl.ActivePageIndex = 0) then
  begin
   Test := Frame;
   Frame := nil;
   for I := 0 to Length(TileSetFramesList) - 1 do
    with TileSetFramesList[I] do
     if (Test <> TilesFrame) and
        (PageControl.ActivePageIndex <> 0) then
      Frame := TilesFrame;
  end else   }
  Include(FPostStates, medFillTilePropsPosted);
  PostMessage(TilePropsFrm.Handle, WM_FILLTILEPROPS, Integer(Frame), 0);
 end;        *)
end;

procedure TMainForm.SelCellValueChange(Sender: TPropertyListItem);
var
 Rect: TRect;
 Tagg, X, Y: Integer;
 Ptr: PByte;
 Event: THistoryEvent;
 MFrm: TMapsFrame;
 Str: WideString;
 Layer: TMapLayer;
begin
 with TObject(Sender.Owner.UserTag) as TMapFrame do
 begin
  if (MapData <> nil) and (MapEditView.SomethingSelected) and
     (MapEditView.PasteBuffer = nil) then
  begin
   MFrm := Owner as TMapsFrame;
   Rect := MapEditView.SelectionRect;
   Rect.Left := Max(Rect.Left, 0);
   Rect.Right := Min(MapData.Width, Rect.Right);
   Rect.Top := Max(Rect.Top, 0);
   Rect.Bottom := Min(MapData.Height, Rect.Bottom);
   if (Rect.Right - Rect.Left > 0) and
      (Rect.Bottom - Rect.Top > 0) then
   begin
    Layer := MapData.Layers[Sender.UserTag div 5];

    Tagg := Sender.UserTag mod 5;

    case Tagg of
     0:   Str := 'tile index';
     1:   Str := 'palette index';
     2:   Str := 'priority';
     3:   Str := 'horisontal flip';
     else Str := 'bertical flip';
    end;
    case Tagg of
     0..2: Str := WideFormat('%s = %d', [Str, Sender.Value]);
     else  Str := WideFormat('%s = %s', [Str, BoolToStr(Sender.PickListIndex <> 0, True)]);
    end;
    Event := MFrm.AddMapEvent(MapData,
     WideFormat('%s ''%s'', layer ''%s'': Selected cells %s',
     [MapTypeStr[BrushMode], MapData.Name, Layer.Name, Str]));
     
    with Layer do
    begin
     for Y := Rect.Top to Rect.Bottom - 1 do
     begin
      Ptr := LayerData;
      Inc(Ptr, (Y * MapData.Width + Rect.Left) * CellSize);
      for X := Rect.Left to Rect.Right - 1 do
      begin
       if MapEditView.Selected[X, Y] then
       begin
        case Tagg of
         0: WriteTileIndex(Ptr^, Sender.Value);
         1: WritePaletteIndex(Ptr^, Sender.Value);
         2: WritePriority(Ptr^, Sender.Value);
         3: WriteXFlip(Ptr^, Sender.PickListIndex <> 0);
         4: WriteYFlip(Ptr^, Sender.PickListIndex <> 0);
        end;
       end;
       Inc(Ptr, CellSize);
      end;
     end;
    end;

    MFrm.SetMapEventData(Event, MapData, ruRedo);
    
    Saved := False;
    RefreshMapContent(MapData, [rmcView]);
   end;
  end;
 end;
end;

function PrepareNewBuffer(View: TCustomTileMapView): TPasteBuffer;
var
 ts: TTileSet;
 X, SaveID: Integer;
 PB: TPasteBuffer;
begin
 ts := nil;
 PB := TObject(View.PasteBuffer) as TPasteBuffer;
 if View.Owner is TMapFrame then
 begin
  with PB do
   for X := 0 to Length(Layers) - 1 do
    with Layers[X].lFormat do
     if lfTileSet <> nil then
     begin
      ts := lfTileSet;
      Break;
     end;
 end else
 if View.Owner is TTilesFrame then
  ts := TTilesFrame(View.Owner).TileSet;

 if ts <> nil then
 begin
  SaveID := PB.ID;
  PB.ID := 775;
  Result := MakePasteBufferCopy(View, ts, PB);
  PB.ID := SaveID;
 end else
  Result := nil;
end;

procedure TMainForm.EditCopyActionExecute(Sender: TObject);
var
 View: TCustomTileMapView;
 Stream: TMemoryStream;
 NewBuf: TPasteBuffer;
 TileBuf: TBitmapBuffer;
 SaveID, X, Y, Idx, TileSz, TileX, TileY: Integer;
 Cvt: TBitmapConverter;
 Rect: TRect;
 PalBuf: TPaletteBuffer;
 Map, Temp: TMap;
 CRC: array of Int64Rec;
 AssignedIndex: array of Integer;
 I: TLinkDirection;
 Link: TBrushLayer;
 ts: TTileSet;
 DrawTileHeader: BitmapEx.TPicHeader;
 DrawTileBmp: TBitmapContainer;
 MP: PLongInt;
 pt: TPoint;
 CT: TColorTable;
begin
 with Sender as TAction do
 begin
  if ColorTablesListBox.Focused then
  begin
   CopyColorTables(TAction(Sender).Tag <> 0);
  end else
  if FFocusedTileSetsFrame <> nil then
  begin
   FFocusedTileSetsFrame.CopyTileSets(TAction(Sender).Tag <> 0);
  end else
  if FFocusedMapsFrame <> nil then
  begin
   with FFocusedMapsFrame do
    if LayersListBox.Focused then
     CopyLayers(TAction(Sender).Tag <> 0) else
    if MapsListBox.Focused then
     CopyMaps(TAction(Sender).Tag <> 0);
  end else
  begin
   View := nil;
   if FFocusedTilesFrame <> nil then
   begin
    with FFocusedTilesFrame do
     if TileSetView.SomethingSelected then
      View := TileSetView;
   end else
   if FFocusedMapFrame <> nil then
   begin
    with FFocusedMapFrame do
     if MapEditView.SomethingSelected then
      View := MapEditView;
   end else
   if FFocusedPaletteFrame <> nil then
   begin
    with FFocusedPaletteFrame do
     if PaletteView.SomethingSelected then
      View := PaletteView;
   end else
   if MainFrame is TTileEditFrame then
   begin
    View := TTileEditFrame(MainFrame).TileEditView;
    if not View.Focused or
       not View.SomethingSelected then
     View := nil;
   end;

   if View <> nil then
   begin
    if View.SelectionMode <> selMulti then
    begin
     pt := View.SelectedCell;
     DrawTool := dtMultiSelect;
     ToolMultiSelectAction.Checked := True;     
     PByteArray(View.SelectionBuffer)[pt.Y * View.MapWidth + pt.X] := 1;
     View.UpdateSelection;
    end;
    if View.Owner is TTileEditFrame then
    begin
     TileBuf := nil;
     if View.PasteBuffer = nil then
      View.FillPasteBuffer(Pointer(TileBuf), Rect, False, True) else
      TileBuf := View.MakePasteBufferCopy(View.PasteBuffer);
     if TileBuf <> nil then
     try
      Move(TTileEditFrame(View.Owner).RGBQuadsPtr^,
           TileBuf.FPicHeader.bhPalette, 4 * 256);

      Stream := TMemoryStream.Create;
      try
       if OpenClipboard(0) then
       try
        with TileBuf do
        begin
         Stream.WriteBuffer(FPicHeader.bhHead.bhInfoHeader,
                             SizeOf(BitmapEx.TPicHeader) -
                             SizeOf(TBitmapFileHeader));
         Stream.WriteBuffer(FPicData.ImageData^, FPicData.ImageSize);

         SetClipboardData(CF_DIB, GlobalHandle(Stream.Memory));

         Stream.Clear;

         Stream.WriteBuffer(FVisibility[0], Length(FVisibility));
        end;

        SetClipboardData(CF_TRANSP_MASK, GlobalHandle(Stream.Memory));
       finally
        CloseClipboard;
       end;
      finally
       Stream.Free;
      end;
     finally
      TileBuf.Free;
     end;
    end else
    if not (View.Owner is TPaletteFrame) then
    begin
     NewBuf := nil;
     if View.PasteBuffer = nil then
     begin
      View.FillPasteBuffer(View.PasteBuffer, Rect, False, False);

      if View.PasteBuffer <> nil then
      try
       NewBuf := PrepareNewBuffer(View);
      finally
       View.ReleasePasteBuffer;
      end;
     end else
      NewBuf := PrepareNewBuffer(View);

     if NewBuf <> nil then
     try
      Stream := TMemoryStream.Create;
      try
       if OpenClipboard(0) then
       try
        NewBuf.SaveToStream(Stream);
        SetClipboardData(CF_MAPDATA, GlobalHandle(Stream.Memory));

        DrawTileBmp := TBitmapContainer.Create;
        try
         FillChar(DrawTileHeader.bhPalette, SizeOf(TRGBQuads), 0);
         if View.Owner is TMapFrame then
         begin
          Map := TMapFrame(View.Owner).MapData;
          if Map <> nil then
           CT := Map.ColorTable else
           CT := nil;
         end else
         with View.Owner as TTilesFrame do
          CT := ColorTable;
         if CT <> nil then
          with CT do
           ColorFormat.ConvertToRGBQuad(ColorData, Addr(DrawTileHeader.bhPalette), ColorsCount);
         DrawTileBmp.ImageFlags := (8 - 1) or IMG_Y_FLIP;
         with NewBuf.TileSet, TileBitmap do
         begin
          FillPicHeader(NewBuf.Width * Width,
                        NewBuf.Height * Height,
                        DrawTileHeader, DrawTileBmp);

          if CT <> nil then
          begin
           TileBitmap.ImageData := EmptyTile.TileData;
           TileBitmap.FillData(CT.TransparentIndex);
          end;
    
          Cvt := TBitmapConverter.Create;
          try
           if BitCount = 8 then
            FillConverter(Cvt, [(8 - 1) or IMG_Y_FLIP]) else
            FillConverter(Cvt, [(8 - 1) or IMG_Y_FLIP], DRAW_PIXEL_MODIFY);
           MP := Pointer(NewBuf.Map);
           TileY := 0;
           for Y := 0 to NewBuf.Height - 1 do
           begin
            TileX := 0;
            for X := 0 to NewBuf.Width - 1 do
            begin
             with Indexed[MP^ and $7FFFFFFF] do
             begin
              ImageData := TileData;
              Cvt.SrcInfo.PixelModifier := FirstColor;
             end;
             Inc(MP);
             Draw(DrawTileBmp, TileX, TileY, Cvt);
             Inc(TileX, Width);
            end;
            Inc(TileY, Height);
           end;
          finally
           Cvt.Free;
          end;

         end;
         Stream.Clear;
         Stream.WriteBuffer(DrawTileHeader.bhHead.bhInfoHeader,
                             SizeOf(DrawTileHeader) -
                             SizeOf(TBitmapFileHeader));
         Stream.WriteBuffer(DrawTileBmp.ImageData^, DrawTileBmp.ImageSize);
        finally
         DrawTileBmp.Free;
        end;

        SetClipboardData(CF_DIB, GlobalHandle(Stream.Memory));
       finally
        CloseClipboard;
       end;
      finally
       Stream.Free;
      end;
     finally
      NewBuf.Free;
     end;

    end else
    begin
     PalBuf := nil;
     if View.PasteBuffer = nil then
      View.CopyMapData(Pointer(PalBuf), Rect) else
      PalBuf := View.PasteBuffer;
     try
      Stream := TMemoryStream.Create;
      try
       if OpenClipboard(0) then
       try
        PalBuf.SaveToStream(Stream);
        SetClipboardData(CF_PALETTEDATA, GlobalHandle(Stream.Memory));
       finally
        CloseClipboard;
       end;
      finally
       Stream.Free;
      end;
     finally
      if PalBuf <> View.PasteBuffer then
       PalBuf.Free;
     end;
    end;
    if Tag <> 0 then
     View.ClearSelection;
   end;
  end;
 end;
end;

procedure TMainForm.EditPasteActionExecute(Sender: TObject);
var
 View: TCustomTileMapView;
 Stream: TBufferStream;
 ClpHandle: THandle;
 NewBuf: TPasteBuffer;
 TileBuf: TBitmapBuffer;
 BmpBuf, Temp: Pointer;
 BmpBits: Pointer;
 DC: HDC;
 ClipboardClosed: Boolean;
 BmpHandle, OldHandle: THandle;
 Sz, Cnt, I, Idx: Integer;
 ClipboardFormats: TClipboardFormats;
 Header: BitmapEx.TPicHeader;
 Cvt: TBitmapConverter;
 BitsPtr, SrcPtr, DstPtr: Pbyte;
 FlagsPtr: PByte;
 Dest: TBitmapContainer;
 PalBuf: TPaletteBuffer;
 DrawTileHeader: BitmapEx.TPicHeader;
 DrawTileBmp: TBitmapContainer;
 TileX, TileY, TileIdx, X, Y, TileSz: Integer;
begin
 ClipboardFormats := GetClipboardFormats;
 if ColorTablesListBox.Focused then
 begin
  if cfColorTables in ClipboardFormats then
  begin
   PasteColorTables;
   Saved := False;
  end;
 end else
 if FFocusedTileSetsFrame <> nil then
 begin
  if cfTileSets in ClipboardFormats then
  begin
   FFocusedTileSetsFrame.PasteTileSets;
   Saved := False;
  end;
 end else
 if FFocusedMapsFrame <> nil then
 begin
  with FFocusedMapsFrame do
   if LayersListBox.Focused then
   begin
    if cfLayers in ClipboardFormats then
    begin
     PasteLayers;
     Saved := False;
    end;
   end else
   if MapsListBox.Focused then
   begin
    if (cfLayers in ClipboardFormats) or
       (cfMaps in ClipboardFormats) then
    begin
     PasteMaps;
     Saved := False;
    end;
   end;
 end else
 begin
  View := nil;
  if FFocusedMapFrame <> nil then
  begin
   if cfMapData in ClipboardFormats then
    with FFocusedMapFrame do
     if (MapData <> nil) and
        (MapData.Width > 0) and
        (MapData.Height > 0) then
      View := MapEditView;
  end else
  if FFocusedTilesFrame <> nil then
  begin
   if (cfMapData in ClipboardFormats) or
      (cfBitmap in ClipboardFormats) then
    with FFocusedTilesFrame do
     if (TileSet <> nil) and
        (TileSet.TilesCount > 0) then
      View := TileSetView;
  end else
  if FFocusedPaletteFrame <> nil then
  begin
   if cfPalette in ClipboardFormats then
    with FFocusedPaletteFrame do
     if (ColorTable <> nil) and
        (ColorTable.ColorsCount > 0) then
      View := PaletteView;
  end else
  if MainFrame is TTileEditFrame then
  with TTileEditFrame(MainFrame) do
   if TileEditView.Focused and
      (cfBitmap in ClipboardFormats) and
      (Tile <> nil) and
      (TileEditView.MapWidth > 0) and
      (TileEditView.MapHeight > 0) then
    View := TileEditView;

  if View <> nil then
  begin
   ClipboardClosed := False;
   try
    if OpenClipboard(0) then
    try
     if View.Owner is TPaletteFrame then
     begin
      ClpHandle := GetClipboardData(CF_PALETTEDATA);
      if ClpHandle <> 0 then
      begin
       Stream := TBufferStream.Create(GlobalLock(ClpHandle), GlobalSize(ClpHandle));
       try
        PalBuf := TPaletteBuffer.Create(0, 0);
        try
         if PalBuf.LoadFromStream(Stream) then
         begin
          with View.ViewRect do
           View.PasteMapData(PalBuf,
                       Bounds(Left + ((Right - Left) -
                       PalBuf.Width) div 2,
                       Top + ((Bottom - Top) -
                       PalBuf.Height) div 2,
                       PalBuf.Width, PalBuf.Height));
         end;
        except
         PalBuf.Free;
         raise;
        end;
       finally
        GlobalUnlock(ClpHandle);
        Stream.Free;
       end;
      end;
     end else
     if View.Owner is TTileEditFrame then
     begin
      ClpHandle := GetClipboardData(CF_DIB);
      if ClpHandle <> 0 then
       with TTileEditFrame(View.Owner) do
       begin
        BmpBuf := GlobalLock(ClpHandle);
        BmpHandle := 0;
        OldHandle := 0;
        TileBuf := nil;
        try
         try
          DC := CreateCompatibleDC(0);
          try
           if DC <> 0 then
           begin
            with TBitmapInfo(BmpBuf^) do
             TileBuf := TBitmapBuffer.Create(bmiHeader.biWidth,
                                             bmiHeader.biHeight);
            with TTileSet(Tile.Owner) do
            begin
             if DrawFlags and DRAW_PIXEL_MODIFY <> 0 then
              X := Tile.FirstColor else
              X := 0;

             Y := 1 shl BitCount;
            end;
            with TileBuf.FPicHeader do
            begin
             Move(PRGBQuads(RGBQuadsPtr)[X], bhPalette[0], Y * 4);

             BmpHandle := CreateDIBSection(DC,
                          TBitmapInfo(Addr(bhHead.bhInfoHeader)^),
                          DIB_RGB_COLORS, BmpBits, 0, 0);
            end;

            if BmpHandle <> 0 then
            begin
             OldHandle := SelectObject(DC, BmpHandle);

             GDIFlush;
             BitsPtr := BmpBuf;

             with TBitmapInfo(BmpBuf^).bmiHeader do
             begin
              Inc(BitsPtr, biSize);
              if (biCompression = BI_BITFIELDS) and
                 (biSize = SizeOf(TBitmapInfoHeader)) then
               Inc(BitsPtr, 3 * 4);
              if biBitCount <= 8 then
              begin
               if biClrUsed > 0 then
                Inc(BitsPtr, biClrUsed * 4) else
                Inc(BitsPtr, 256 * 4);
              end;
             end;
             with TileBuf.FPicHeader.bhHead.bhInfoHeader do
              StretchDIBits(DC, 0, 0, biWidth, biHeight, 0, 0, biWidth, biHeight,
                     BitsPtr, TBitmapInfo(BmpBuf^), DIB_RGB_COLORS, SRCCOPY);
            end;
           end;
          except
           if BmpHandle <> 0 then
           begin
            if OldHandle <> 0 then SelectObject(DC, OldHandle);
            DeleteObject(BmpHandle);
           end;

           if DC <> 0 then DeleteDC(DC);
          end;
         finally
          GlobalUnlock(ClpHandle);
         end;
         if TileBuf <> nil then
         begin
          ClpHandle := GetClipboardData(CF_TRANSP_MASK);
          if ClpHandle <> 0 then
          try
           SrcPtr := GlobalLock(ClpHandle);
           with TileBuf do                           
            Move(SrcPtr^, FVisibility[0], Length(FVisibility));
          finally
           GlobalUnlock(ClpHandle);
          end else
          with TileBuf do
           FillChar(FVisibility[0], Length(FVisibility), 1);
         end;
         CloseClipboard;
         ClipboardClosed := True;
         if DC <> 0 then
         try
          if BmpHandle <> 0 then
          try
           SrcPtr := BmpBits;
           with TileBuf.FPicData do
           begin
            DstPtr := ImageData;
            X := (Width + WidthRemainder) * Height;
           end;

           with TTileSet(Tile.Owner) do
            if DrawFlags and DRAW_PIXEL_MODIFY <> 0 then
             Y := Tile.FirstColor else
             Y := 0;

           GDIFlush;

           for Idx := 0 to X - 1 do
           begin
            DstPtr^ := SrcPtr^ + Y;
            Inc(SrcPtr);
            Inc(DstPtr);
           end;

           with View.ViewRect, TileBuf.FPicData do
            View.PasteMapData(TileBuf, 
                       Bounds(Left + ((Right - Left) - Width) div 2,
                              Top + ((Bottom - Top) - Height) div 2,
                              Width, Height));
           DrawTool := dtMultiSelect;
           ToolMultiSelectAction.Checked := True;
           Saved := False;
          finally
           if OldHandle <> 0 then SelectObject(DC, OldHandle);
           DeleteObject(BmpHandle);
          end;
         finally
          DeleteDC(DC);
         end;
        except
         TileBuf.Free;
         raise;
        end;
       end;
     end else
     begin
      if cfMapData in ClipboardFormats then
       ClpHandle := GetClipboardData(CF_MAPDATA) else
       ClpHandle := 0;
      if ClpHandle <> 0 then
      begin
       Stream := TBufferStream.Create(GlobalLock(ClpHandle), GlobalSize(ClpHandle));
       try
        NewBuf := TPasteBuffer.Create(0);
        try
         NewBuf.LoadFromStream(Stream, FProject.FMaps.TileSetList);
         with View.ViewRect do
          View.PasteMapData(View.MakePasteBufferCopy(NewBuf),
                      Bounds(Left + ((Right - Left) -
                      NewBuf.Width) div 2,
                      Top + ((Bottom - Top)  -
                      NewBuf.Height) div 2,
                      NewBuf.Width, NewBuf.Height));
         DrawTool := dtMultiSelect;
         ToolMultiSelectAction.Checked := True;
         Saved := False;
        finally
         NewBuf.Free;
        end;
       finally
        GlobalUnlock(ClpHandle);
        Stream.Free;
       end;
      end else
      if View.Owner is TTilesFrame then
      begin
       ClpHandle := GetClipboardData(CF_DIB);
       if (ClpHandle <> 0) then
       with TTilesFrame(View.Owner) do
        if ColorTable <> nil then
        begin
         BmpBuf := GlobalLock(ClpHandle);
         BmpHandle := 0;
         OldHandle := 0;
         try
          DC := CreateCompatibleDC(0);
          try
           if DC <> 0 then
           begin
            with TBitmapInfo(BmpBuf^) do
             FillBitmapHeader(Addr(Header.bhHead.bhMainHeader),
                              bmiHeader.biWidth,
                              bmiHeader.biHeight, 8);
            with Header.bhHead do
            begin
             bhInfoHeader.biClrUsed := ColorTable.ColorsCount;
             ColorTable.ColorFormat.ConvertToRGBQuad(ColorTable.ColorData,
                         Addr(Header.bhPalette), bhInfoHeader.biClrUsed);
             FillChar(Header.bhPalette[bhInfoHeader.biClrUsed],
                                (256 - bhInfoHeader.biClrUsed) * 4, 0);
            end;

            BmpHandle := CreateDIBSection(DC,
                         TBitmapInfo(Addr(Header.bhHead.bhInfoHeader)^),
                         DIB_RGB_COLORS, BmpBits, 0, 0);
            if BmpHandle <> 0 then
            begin
             OldHandle := SelectObject(DC, BmpHandle);

             GDIFlush;
             BitsPtr := BmpBuf;

             with TBitmapInfo(BmpBuf^).bmiHeader do
             begin
              Inc(BitsPtr, biSize);
              if (biCompression = BI_BITFIELDS) and
                 (biSize = SizeOf(TBitmapInfoHeader)) then
               Inc(BitsPtr, 3 * 4);
              if biBitCount <= 8 then
              begin
               if biClrUsed > 0 then
                Inc(BitsPtr, biClrUsed * 4) else
                Inc(BitsPtr, 256 * 4);
              end;
             end;
             with Header.bhHead.bhInfoHeader do
              StretchDIBits(DC, 0, 0, biWidth, biHeight, 0, 0, biWidth, biHeight,
                     BitsPtr, TBitmapInfo(BmpBuf^), DIB_RGB_COLORS, SRCCOPY);
            end;
           end;
          except
           if BmpHandle <> 0 then
           begin
            if OldHandle <> 0 then SelectObject(DC, OldHandle);
            DeleteObject(BmpHandle);
           end;
           if DC <> 0 then DeleteDC(DC);
           raise;
          end;
         finally
          GlobalUnlock(ClpHandle);
         end;
         CloseClipboard;
         ClipboardClosed := True;
         if DC <> 0 then
         try
          if BmpHandle <> 0 then
          try
           DrawTileBmp := TBitmapContainer.Create;
           with Header.bhHead.bhInfoHeader do
           try
            DrawTileBmp.ImageData := BmpBits;
            DrawTileBmp.Width := biWidth;
            DrawTileBmp.WidthRemainder := GetWidthBytes(biWidth, 8) - biWidth;
            DrawTileBmp.Height := biHeight;

            NewBuf := TPasteBuffer.Create(776);
            try
             NewBuf.TileSet := TTileSet.Create;
             with NewBuf.TileSet do
             begin
              AssignTileFormat(TileSet);
              Cvt := TBitmapConverter.Create([(8 - 1) or IMG_Y_FLIP],
                                             TileBitmap.FlagsList,
                                             DRAW_NO_CONVERT);
              try
               with NewBuf do
               begin
                Width := (biWidth + TileBitmap.Width - 1) div TileBitmap.Width;
                Height := (biHeight + TileBitmap.Height - 1) div TileBitmap.Height;

                SetLength(Map, Width * Height);
                FillChar(Pointer(Map)^, Length(Map) * 4, 0);
               end; 

               GDIFlush;

//               Idx := 0;
               TileY := 0;
               for Y := 0 to NewBuf.Height - 1 do
               begin
                TileX := 0;
                for X := 0 to NewBuf.Width - 1 do
                begin
                 with AddTile do
                 begin
                  TileBitmap.ImageData := TileData;
                  NewBuf.Map[TileIndex] := TileIndex;
                 end;
                 DrawTileBmp.Draw(TileBitmap, TileX, TileY, Cvt);

                 Dec(TileX, TileBitmap.Width);
                end;
                Dec(TileY, TileBitmap.Height);
               end;
              finally
               Cvt.Free;
              end;
             end;
             with View.ViewRect do
              View.PasteMapData(NewBuf, Bounds(Left + ((Right - Left) -
                                               NewBuf.Width) div 2,
                                               Top + ((Bottom - Top) -
                                               NewBuf.Height) div 2,
                                               NewBuf.Width, NewBuf.Height));
             DrawTool := dtMultiSelect;
             ToolMultiSelectAction.Checked := True;
             Saved := False;
            except
             NewBuf.Free;
             raise;
            end;
          {  finally
             Free;
            end;  }
           finally
            DrawTileBmp.Free;
        //    Cvt.Free;
           end;
          finally
           if OldHandle <> 0 then SelectObject(DC, OldHandle);
           DeleteObject(BmpHandle);
          end;
         finally
          DeleteDC(DC);
         end;

           {

         FOldHandle := SelectObject(FDC, FHandle);
         with TDIB do
         Stream := TBufferStream.Create(, Sz);
         try
          GetMem(BmpBuf, Sz);
          try
           Stream.ReadBuffer(BmpBuf^, Sz);
          finally
           FreeMem(BmpBuf);
          end;
          Stream.Seek(0, soFromBeginning)
         finally
          GlobalUnlock(ClpHandle);
          Stream.Free;
         end;
         Cnt := ColorTable.ColorsCount;
         ColorTable.ColorFormat.ConvertToRGBQuad(ColorTable.ColorData, @RGB, Cnt);
         for I := 0 to Cnt - 1 do
          Lab[I] := RGB2LAB(RGB[I]);
                      }
        { with ColorTable.ColorFormat,
              TColorQuantizer.Create(1 shl TileSet.TileBitmap.BitsCount,
              Max(RedCount, Max(BlueCount, Max(GreenCount, AlphaCount)))) do
         try

         finally
          Free;
         end;  }
        end;
      end;
     end;
    finally
     if not ClipboardClosed then
      CloseClipboard;
    end;
   except
    WideMessageDlg('Clipboard data is damaged', mtError, [mbOk], 0);
   end;
  end;
 end;
end;

procedure TMainForm.EditPasteActionUpdate(Sender: TObject);
var
 Enable: Boolean;
 ClipboardFormats: TClipboardFormats;
begin
 Enable := False;
 if ColorTablesListBox.Focused then
  Enable := cfColorTables in GetClipboardFormats else
 if FFocusedTileSetsFrame <> nil then
  Enable := cfTileSets in GetClipboardFormats else
 if FFocusedMapsFrame <> nil then
 begin
  with FFocusedMapsFrame do
   if MapsListBox.Focused then
   begin
    ClipboardFormats := GetClipboardFormats;
    Enable := (cfLayers in ClipboardFormats) or
              (cfMaps in ClipboardFormats);
   end else
   if LayersListBox.Focused then
    Enable := cfLayers in GetClipboardFormats;
 end else
 if FFocusedMapFrame <> nil then
 begin
  if cfMapData in GetClipboardFormats then
   with FFocusedMapFrame do
    Enable := (MapData <> nil) and
              (MapData.Width > 0) and
              (MapData.Height > 0);
 end else
 if FFocusedTilesFrame <> nil then
 begin
  ClipboardFormats := GetClipboardFormats;
  if (cfMapData in ClipboardFormats) or
     (cfBitmap in ClipboardFormats) then
   with FFocusedTilesFrame do
    Enable := (TileSet <> nil) and
              (TileSet.TilesCount > 0);
 end else
 if FFocusedPaletteFrame <> nil then
 begin
  if cfPalette in GetClipboardFormats then
   with FFocusedPaletteFrame do
    Enable := (ColorTable <> nil) and
              (ColorTable.ColorsCount > 0);
 end else
 if (MainFrame is TTileEditFrame) and
    TTileEditFrame(MainFrame).TileEditView.Focused then
 begin
  ClipboardFormats := GetClipboardFormats;
  if cfBitmap in ClipboardFormats then
   with TTileEditFrame(MainFrame) do
    Enable := (Tile <> nil) and
              (TileEditView.MapWidth > 0) and
              (TileEditView.MapHeight > 0);
 end;

 EditPasteAction.Enabled := Enable;
end;

procedure TMainForm.MapFrameMapEditViewMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
 with Sender as TCustomTileMapView do
  case FDrawTool of
   dtPencil: if not (ssAlt in Shift) then DefaultCursor := crPencil;
   dtBrush: DefaultCursor := crBrush;
  end;
end;
             (*
procedure TMainForm.ColorTablePropEditActionExecute(Sender: TObject);
begin
 PaletteBox.Visible := not PaletteBox.Visible;
 PalPropsPanel.Visible := not PalPropsPanel.Visible;
end;             *)

procedure TMainForm.MainFormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
 I: Integer;
 Cursor: TCursor;
begin
 if (Key = VK_MENU) and (DrawTool <> dtPicker) then
 begin
  if DrawTool in [dtPencil, dtSingleSelect] then
  begin
   if DrawTool = dtPencil then
    Cursor := crPencil else
    Cursor := crDefault;
   for I := 0 to Length(MapFramesList) - 1 do
    MapFramesList[I].MapFrame.MapEditView.DefaultCursor := Cursor;
  end;
  if DrawTool <> dtMagicWand then
   for I := 0 to Length(TileSetFramesList) - 1 do
    TileSetFramesList[I].TilesFrame.TileSetView.DefaultCursor := crDefault;
 end;
end;

procedure TMainForm.RefreshColorTableProps;
begin
 PostMessage(Handle, WM_REFRESHCTBLPROPS, 0, 0);
end;
           (*
procedure TMainForm.MapFrameGetLayerActive(Sender: TMapFrame;
  Layer: TMapLayer; var Active: Boolean);
var
 I: Integer;
begin
 if not Active then
  with Sender.MapData do
  begin
   for I := 0 to Count - 1 do
    if Layers[I].Flags and LF_LAYER_ACTIVE <> 0 then Exit;
    
   if Sender.BrushMode then
    Active := Layer = Layers[FProject.FBrushFocus[Index]] else
    Active := Layer = Layers[FProject.FFocusedList[Index]]
  end;
end;            *)

procedure TMainForm.ColorTableValueChange(Sender: TPropertyListItem);
var
 I: Integer;
 NewFormat: TColorFormat;
 Item: TColorTable;
 Str: WideString;
 Event: THistoryEvent;
begin
 if FProgressBar = nil then
  with FProject.FMaps.ColorTableList do
   case Sender.Index of
    0:
    begin
     for I := 0 to Count - 1 do
      if ColorTablesListBox.Selected[I] then
      begin
       Item := Nodes[I] as TColorTable;
       Str := Item.Name;
       Event := FProject.History.AddEvent(Item, '');
       Item.Name := Sender.ValueStr;
       Event.Text := WideFormat('Color table ''%s'': Name = ''%s''',
        [Str, Item.Name]);
       Event.RedoData := Item;
      end;

     RefreshColorTableProps;

     ColorTablesListBox.Invalidate;
     FillColorTablesCombo;

     for I := 0 to Length(MapFramesList) - 1 do
      MapFramesList[I].RefreshMapProps;
    end;
    1:
    begin
     NewFormat := TColorFormat.Create;
     try
      try
       NewFormat.FormatString := Sender.ValueStr;
      except
       WideMessageDlg('Unknown color format', mtError, [mbOk], 0);
       RefreshColorTableProps;
       Exit;
      end;
      Sender.Changing := True;
      Sender.ValueStr := NewFormat.FormatString;
      Sender.Changing := False;
      for I := 0 to Count - 1 do
       if ColorTablesListBox.Selected[I] then
       begin
        Item := Nodes[I] as TColorTable;

        Event := FProject.History.AddEvent(Item,
         WideFormat('Color table ''%s'': Color format = ''%s''',
         [Item.Name, Sender.ValueStr]));

        Item.ColorFormat := NewFormat;

        Event.RedoData := Item;
       end;

      ColorContentModified;
     finally
      NewFormat.Free;
     end;
    end;
    2:
    begin
     for I := 0 to Count - 1 do
      if ColorTablesListBox.Selected[I] then
      begin
       Item := Nodes[I] as TColorTable;

       Event := FProject.History.AddEvent(Item,
         WideFormat('Color table ''%s'': Colors count = %d',
         [Item.Name, Sender.Value]));

       if ColorTableIndex = I then
        PaletteFrame.ColorsCount := Sender.Value else
        Item.ColorsCount := Sender.Value;

       Event.RedoData := Item;
      end;

     ColorContentModified;
    end;
    3:
    begin
     for I := 0 to Count - 1 do
      if ColorTablesListBox.Selected[I] then
      begin
       Item := Nodes[I] as TColorTable;

       Event := FProject.History.AddEvent(Item,
        WideFormat('Color table ''%s'': Transparent index = %d',
        [Item.Name, Sender.Value]));

       Item.TransparentIndex := Sender.Value;

       Event.RedoData := Item;
      end;

     RefreshGraphicContent;
    end;
   end;
end;

              (*
procedure TMainForm.BrushNewActionUpdate(Sender: TObject);
begin
//
end;

procedure TMainForm.BrushNewActionExecute(Sender: TObject);
var
 W, H, I: Integer;
 Map, Brush: TMap;
 Layer: TMapLayer;
begin
 if TilesFrame.TileSet <> nil then
 begin
  W := TilesFrame.TileSet.TileWidth;
  H := TilesFrame.TileSet.TileHeight;
 end else
 begin
  W := 16;
  H := 16;
 end;
 Brush := FProject.FMaps.BrushList.AddMap('Brush 1', 1, 1, W, H);
 with Brush do
 begin
  if BrushFrame.MapEditView.Focused then
   Map := BrushFrame.MapData else
   Map := MapFrame.MapData;
  if Map <> nil then
  begin
   for I := 0 to Map.Count - 1 do
   begin
    Layer := Map.Layers[I];
    if (Layer.Flags and LF_VISIBLE <> 0) and
       (Layer.Flags and LF_LAYER_ACTIVE <> 0) then
    (AddNode as TMapLayer).AssignLayerInfo(Layer);
   end;
   ColorTableIndex := Map.ColorTableIndex;
  end else
   ColorTableIndex := ColorTableListBox.ItemIndex;
  FProject.Update;
  Saved := False;  
  BrushesListBox.Items.Add(WideFormat('%d. %s', [Index + 1, Name]));
  BrushesListBox.ItemIndex := Index;
  EditMode := emBrush;
  BrushesListBoxChange(nil);
 end;
end;

procedure TMainForm.BrushesListBoxChange(Sender: TObject);
var
 Brush: TMap;
begin
 Brush := SelectedBrush;
 if Brush <> nil then
  FProject.FInfo.SelectedBrushIndex := Brush.Index else
  FProject.FInfo.SelectedBrushIndex := 0;
 BrushFrame.SetMap(Brush, FProject.FInfo.BrushFrameScale, True);
end;

procedure TMainForm.LayerNewActionExecute(Sender: TObject);
var
 Map: TMap;
 I: Integer;
begin
 Map := TObject(LayersListBox.Tag) as TMap;
 if Map <> nil then
 begin
  if MapFrame.MapData = Map then
   MapFrame.MapEditView.Deselect;
  if BrushFrame.MapData = Map then
   BrushFrame.MapEditView.Deselect;
  with Map.AddList('Layer 1') as TMapLayer do
  begin
   if Map.Count > 1 then AssignLayerFormat(Map.LastNode.Prev as TMapLayer) else
   begin
    CellSize := 2;
    IndexMask := $FFFF;
   end;
  end;
  FProject.Update;
  Saved := False;
  LayersListBox.Count := 0;
  LayersListBox.Count := Map.Count;
  I := Map.Count - 1;
  if Map.Owner = FProject.FMaps.BrushList then
   FProject.FBrushFocus[Map.Index] := I else
   FProject.FFocusedList[Map.Index] := I;

  LayersListBox.ItemIndex := I;
  LayersListBox.Selected[I] := True;
  
  FillLayerProps(Map);
  TilesBoxUpdate;
 end;
end;

procedure TMainForm.LayerNewActionUpdate(Sender: TObject);
begin
 LayerNewAction.Enabled := (FProgressBar = nil) and (LayersListBox.Tag <> 0);
end;

procedure TMainForm.LayerUpActionExecute(Sender: TObject);
var
 Map: TMap;
 I, NewII, J, L: Integer;
 Deta3ched: array of TNode;
 Node, Prev: TNode;
begin
 Map := TObject(LayersListBox.Tag) as TMap;
 if (Map <> nil) and (Map.Count > 1) then
 begin
  if MapFrame.MapData = Map then
   MapFrame.MapEditView.Deselect;
  if BrushFrame.MapData = Map then
   BrushFrame.MapEditView.Deselect;
  LayerPropEditor.CancelEditNode;
  NewII := -1;
  Node := Map.LastNode;
  Map.FInternalFlags := [secLoading];
  try
   for I := LayersListBox.Count - 1 downto 0 do
   begin
    Prev := Node.Prev;
    if LayersListBox.Selected[I] then
    begin
     if NewII < 0 then NewII := I - 1;
     L := Length(Detached);
     SetLength(Detached, L + 1);
     Detached[L] := Node;
     J := Node.Index;
     Map.DetachNode(Node);
     Node.Index := J - 1;
    end;
    Node := Prev;
   end;

   LayersListBox.Deselect;
    
   for I := Length(Detached) - 1 downto 0  do
   begin
    Node := Detached[I];
    J := Node.Index;
    Map.AttachNode(Node, J);
    LayersListBox.Selected[J] := True;
   end;
  finally
   Map.FInternalFlags := [];
  end;

  LayersListBox.ItemIndex := NewII;
  LayersListBox.Invalidate;
  
  if Map.Owner = FProject.FMaps.BrushList then
   FProject.FBrushFocus[Map.Index] := NewII else
   FProject.FFocusedList[Map.Index] := NewII;
  Saved := False;

  FillLayerProps(Map);
  TilesBoxUpdate;
  MapFrame.MapEditView.Invalidate;
  BrushFrame.MapEditView.Invalidate;
 end;
end;

procedure TMainForm.LayerUpActionUpdate(Sender: TObject);
var
 Map: TMap;
begin
 Map := TObject(LayersListBox.Tag) as TMap;
 LayerUpAction.Enabled := (FProgressBar = nil) and (Map <> nil) and
                           (Map.Count > 1) and
                           (LayersListBox.SelCount > 0) and
                           not LayersListBox.Selected[0];
end;

procedure TMainForm.LayerDownActionExecute(Sender: TObject);
var
 Map: TMap;
 I, NewII, J, L: Integer;
 Detached: array of TNode;
 Node, Prev: TNode;
begin
 Map := TObject(LayersListBox.Tag) as TMap;
 if (Map <> nil) and (Map.Count > 1) then
 begin
  if MapFrame.MapData = Map then
   MapFrame.MapEditView.Deselect;
  if BrushFrame.MapData = Map then
   BrushFrame.MapEditView.Deselect;
  LayerPropEditor.CancelEditNode;

  Map.FInternalFlags := [secLoading];
  try
   NewII := -1;
   Node := Map.LastNode;
   for I := LayersListBox.Count - 1 downto 0 do
   begin
    Prev := Node.Prev;
    if LayersListBox.Selected[I] then
    begin
     NewII := I + 1;
     L := Length(Detached);
     SetLength(Detached, L + 1);
     Detached[L] := Node;
     J := Node.Index;
     Map.DetachNode(Node);
     Node.Index := J + 1;
    end;
    Node := Prev;
   end;

   LayersListBox.Deselect;

   for I := Length(Detached) - 1 downto 0  do
   begin
    Node := Detached[I];
    J := Node.Index;
    Map.AttachNode(Node, J);
    LayersListBox.Selected[J] := True;
   end;
  finally
   Map.FInternalFlags := [];
  end;

  LayersListBox.Invalidate;

  LayersListBox.ItemIndex := NewII;

  if Map.Owner = FProject.FMaps.BrushList then
   FProject.FBrushFocus[Map.Index] := NewII else
   FProject.FFocusedList[Map.Index] := NewII;
  Saved := False;

  FillLayerProps(Map);
  TilesBoxUpdate;
  MapFrame.MapEditView.Invalidate;
  BrushFrame.MapEditView.Invalidate;
 end;
end;

procedure TMainForm.LayerDownActionUpdate(Sender: TObject);
var
 Map: TMap;
begin
 Map := TObject(LayersListBox.Tag) as TMap;
 LayerDownAction.Enabled := (FProgressBar = nil) and (Map <> nil) and
                           (Map.Count > 1) and
                           (LayersListBox.SelCount > 0) and
                           not LayersListBox.Selected[Map.Count - 1];
end;

procedure TMainForm.LayerDeleteActionExecute(Sender: TObject);
var
 Map: TMap;
 I, Top: Integer;
 Node, Prev: TNode;
begin
 Map := TObject(LayersListBox.Tag) as TMap;
 if Map <> nil then
 begin
  if MapFrame.MapData = Map then
   MapFrame.MapEditView.Deselect;
  if BrushFrame.MapData = Map then
   BrushFrame.MapEditView.Deselect;
  LayerPropEditor.CancelEditNode;
  Top := -1;
  Node := Map.LastNode;
  for I := LayersListBox.Count - 1 downto 0 do
  begin
   Prev := Node.Prev;
   if LayersListBox.Selected[I] then
   begin
    if Top < 0 then Top := I;
    Map.Remove(Node);
   end;
   Node := Prev;
  end;

  FProject.FMaps.BrushList.Changed;

  LayersListBox.Count := Map.Count;
  if Top >= Map.Count then
   Dec(Top);
  if (Top >= 0) and (Map.Count > 0) then
  begin
   LayersListBox.ItemIndex := Top;
   LayersListBox.Selected[Top] := True;
  end;
  FProject.Update;
  if Map.Owner = FProject.FMaps.BrushList then
   FProject.FBrushFocus[Map.Index] := Top else
   FProject.FFocusedList[Map.Index] := Top;
  Saved := False;

  FillLayerProps(Map);
  TilesBoxUpdate;
  MapFrame.MapEditView.SelectionChanged;
  BrushFrame.MapEditView.SelectionChanged;
  MapFrame.MapEditView.Invalidate;
  BrushFrame.MapEditView.Invalidate;
 end;
end;

procedure TMainForm.LayerDeleteActionUpdate(Sender: TObject);
var
 Map: TMap;
begin
 Map := TObject(LayersListBox.Tag) as TMap;
 LayerDeleteAction.Enabled := (FProgressBar = nil) and (Map <> nil) and
                           (Map.Count > 0) and
                           (LayersListBox.SelCount > 0);
end;

procedure TMainForm.BrushUpActionExecute(Sender: TObject);
begin
//
end;

procedure TMainForm.BrushUpActionUpdate(Sender: TObject);
begin
//
end;

procedure TMainForm.BrushDownActionExecute(Sender: TObject);
begin
//
end;

procedure TMainForm.BrushDownActionUpdate(Sender: TObject);
begin
//
end;

procedure TMainForm.BrushDeleteActionExecute(Sender: TObject);
begin
//
end;

procedure TMainForm.BrushDeleteActionUpdate(Sender: TObject);
begin
//
end;

procedure TMainForm.MapChanged;
var
 Map: TMap;
begin
 Map := MapFrame.MapData;
 LayersListBox.Count := 0;  
 if Map <> nil then
 begin
  if EditMode = emBrush then
  begin
   MapPropertiesLabel.Caption := WideFormat('Brush %d properties', [Map.Index + 1]);
   LayersListLabel.Caption := WideFormat('Brush %d layers list', [Map.Index + 1])
  end else
  begin
   MapPropertiesLabel.Caption := WideFormat('Map %d properties', [Map.Index + 1]);
   LayersListLabel.Caption := WideFormat('Map %d layers list', [Map.Index + 1])   
  end;
  LayersListBox.Count := Map.Count;
  LayersListBox.Tag := Integer(Map);
  if Map.Count > 0 then
  begin
   if EditMode = emBrush then
    LayersListBox.ItemIndex := FProject.FBrushFocus[Map.Index] else
    LayersListBox.ItemIndex := FProject.FFocusedList[Map.Index];

   LayersListBox.Selected[LayersListBox.ItemIndex] := True;
  end;
  ColorTableListBox.ItemIndex := Map.ColorTableIndex;
 end else
 begin
  LayersListBox.Tag := 0;
  MapPropertiesLabel.Caption := 'Map properties';
  LayersListLabel.Caption := 'Map layers list'
 end;
    
 FillMapProps(Map);
 FillLayerProps(Map);
 RefreshColorTable;

 ColorTableListBoxChange(nil); // TilesBoxUpdate inside

end;

              
function LayerInRect(tl: TBrushLayer; const fr: TRect;
          var tr: TRect; Direction: TDrawDirection): Boolean;
var
 Intersection: TRect;
 //Dir: TBrushDirection;
begin
 tl.TilesFilled := True;
 with tl.Owner as TMapBrush do
 begin
  GetValue32Rect(tr, tl.QuickTiles, Width, Height,
                 LongWord(tl.TileSet.EmptyTile));

  Result := IntersectRect(Intersection, fr, tr);
  if Result then
  begin
   if Direction = [ddLeft] then
    Result := (bdHorizontal in tl.Directions) and
              (bdUnfinishedRight in tl.Directions) and
              (Intersection.Left >= tr.Left) else
   if Direction = [ddRight] then
    Result := (bdHorizontal in tl.Directions) and
              (bdUnfinishedLeft in tl.Directions) and
              (Intersection.Right <= tr.Right) else
   if Direction = [ddUp] then
    Result := (bdVertical in tl.Directions) and
              (bdUnfinishedBottom in tl.Directions) and
              (Intersection.Top >= tr.Top) else
   if Direction = [ddDown] then
    Result := (bdVertical in tl.Directions) and
              (bdUnfinishedTop in tl.Directions) and
              (Intersection.Bottom <= tr.Bottom) else
   begin
    Result := bdDiagonal in tl.Directions;
    if Result then
    begin
     if Direction = [ddLeft, ddUp] then
      Result := (bdUnfinishedRight in tl.Directions) and
                (bdUnfinishedBottom in tl.Directions) and
                (Intersection.Left >= tr.Left) and
                (Intersection.Top >= tr.Top) else
     if Direction = [ddLeft, ddDown] then
      Result := (bdUnfinishedRight in tl.Directions) and
                (bdUnfinishedTop in tl.Directions) and
                (Intersection.Left >= tr.Left) and
                (Intersection.Bottom <= tr.Bottom) else
     if Direction = [ddRight, ddUp] then
      Result := (bdUnfinishedLeft in tl.Directions) and
                (bdUnfinishedBottom in tl.Directions) and
                (Intersection.Right <= tr.Right) and
                (Intersection.Top >= tr.Top) else
     if Direction = [ddRight, ddDown] then
      Result := (bdUnfinishedLeft in tl.Directions) and
                (bdUnfinishedTop in tl.Directions) and
                (Intersection.Right <= tr.Right) and
                (Intersection.Bottom <= tr.Bottom);
    end;
   end;
  end;

{ if Result then
  begin
   if (fr.Bottom = Intersection.Bottom) and
      (fr.Top = Intersection.Top) then
    Dir := bdHorizontal else
   if (fr.Right = Intersection.Right) and
      (fr.Left = Intersection.Left) then
    Dir := bdVertical else
    Dir := bdDiagonal;

   Result := Dir in Directions;

   if Result then
   begin

   end;
  end;    }
 end;
end;           *)

function TMainForm.DrawWithBrushEx(Control: TCustomTileMapView;
  ABrush: TMapBrush; DrawX, DrawY: Integer): Boolean;
var
 XX, YY, CX, CY: Integer;
 ms: Boolean;
 PB: PByte;
begin
 Result := False;
 with Control.Owner as TMapFrame do
 begin
  if (MapData <> nil) and
     (MapData.Width > 0) and
     (MapData.Height > 0) and
     (MapData.Count > 0) and
     (ABrush <> nil) and
     (ABrush.Width > 0) and
     (ABrush.Height > 0) and
     (ABrush.Count > 0) then
  begin
   ms := Control.MultiSelected;
  { for I := 0 to MapData.Count - 1 do
   begin
    DestLayer := MapData.Layers[I];
    with DestLayer do
     if (LayerData <> nil) and
        (Flags and LF_VISIBLE <> 0) and
        (Flags and LF_LAYER_ACTIVE <> 0) then  }
     with BrushPattern^ do
     begin
      XX := DrawX - X;
      YY := DrawY - Y;
      PB := P;
      for CY := YY to YY + (H - 1) do
       for CX := XX to XX + (W - 1) do
       begin
        if (PB^ <> 0) and (not ms or Control.Selected[CX, CY]) then
         Result := ABrush.Draw(MapData, CX, CY) or Result;
        Inc(PB);
       end;
     end;
 //  end;
  end;
 end;
end;
                        (*
procedure TMainForm.WMFillLayerProps(var Message: TMessage);
begin
 FillLayerProps(TObject(Message.WParam) as TMap);
end;
                          *)
function TMainForm.ColorTablesListBoxDataFind(Control: TWinControl;
  FindString: String): Integer;
begin
 Result := FProject.FMaps.ColorTableList.FindIndexByFirstLetters(FindString);
end;

const DefaultPalette: array[0..767] of Byte = (
$00,$00,$00,$00,$00,$AA,$00,$AA,$00,$00,$AA,$AA,$AA,$00,$00,$AA,
$00,$AA,$AA,$55,$00,$AA,$AA,$AA,$55,$55,$55,$55,$55,$FF,$55,$FF,
$55,$55,$FF,$FF,$FF,$55,$55,$FF,$55,$FF,$FF,$FF,$55,$FF,$FF,$FF,
$00,$00,$00,$14,$14,$14,$20,$20,$20,$2C,$2C,$2C,$38,$38,$38,$45,
$45,$45,$51,$51,$51,$61,$61,$61,$71,$71,$71,$82,$82,$82,$92,$92,
$92,$A2,$A2,$A2,$B6,$B6,$B6,$CB,$CB,$CB,$E3,$E3,$E3,$FF,$FF,$FF,
$00,$00,$FF,$41,$00,$FF,$7D,$00,$FF,$BE,$00,$FF,$FF,$00,$FF,$FF,
$00,$BE,$FF,$00,$7D,$FF,$00,$41,$FF,$00,$00,$FF,$41,$00,$FF,$7D,
$00,$FF,$BE,$00,$FF,$FF,$00,$BE,$FF,$00,$7D,$FF,$00,$41,$FF,$00,
$00,$FF,$00,$00,$FF,$41,$00,$FF,$7D,$00,$FF,$BE,$00,$FF,$FF,$00,
$BE,$FF,$00,$7D,$FF,$00,$41,$FF,$7D,$7D,$FF,$9E,$7D,$FF,$BE,$7D,
$FF,$DF,$7D,$FF,$FF,$7D,$FF,$FF,$7D,$DF,$FF,$7D,$BE,$FF,$7D,$9E,
$FF,$7D,$7D,$FF,$9E,$7D,$FF,$BE,$7D,$FF,$DF,$7D,$FF,$FF,$7D,$DF,
$FF,$7D,$BE,$FF,$7D,$9E,$FF,$7D,$7D,$FF,$7D,$7D,$FF,$9E,$7D,$FF,
$BE,$7D,$FF,$DF,$7D,$FF,$FF,$7D,$DF,$FF,$7D,$BE,$FF,$7D,$9E,$FF,
$B6,$B6,$FF,$C7,$B6,$FF,$DB,$B6,$FF,$EB,$B6,$FF,$FF,$B6,$FF,$FF,
$B6,$EB,$FF,$B6,$DB,$FF,$B6,$C7,$FF,$B6,$B6,$FF,$C7,$B6,$FF,$DB,
$B6,$FF,$EB,$B6,$FF,$FF,$B6,$EB,$FF,$B6,$DB,$FF,$B6,$C7,$FF,$B6,
$B6,$FF,$B6,$B6,$FF,$C7,$B6,$FF,$DB,$B6,$FF,$EB,$B6,$FF,$FF,$B6,
$EB,$FF,$B6,$DB,$FF,$B6,$C7,$FF,$00,$00,$71,$1C,$00,$71,$38,$00,
$71,$55,$00,$71,$71,$00,$71,$71,$00,$55,$71,$00,$38,$71,$00,$1C,
$71,$00,$00,$71,$1C,$00,$71,$38,$00,$71,$55,$00,$71,$71,$00,$55,
$71,$00,$38,$71,$00,$1C,$71,$00,$00,$71,$00,$00,$71,$1C,$00,$71,
$38,$00,$71,$55,$00,$71,$71,$00,$55,$71,$00,$38,$71,$00,$1C,$71,
$38,$38,$71,$45,$38,$71,$55,$38,$71,$61,$38,$71,$71,$38,$71,$71,
$38,$61,$71,$38,$55,$71,$38,$45,$71,$38,$38,$71,$45,$38,$71,$55,
$38,$71,$61,$38,$71,$71,$38,$61,$71,$38,$55,$71,$38,$45,$71,$38,
$38,$71,$38,$38,$71,$45,$38,$71,$55,$38,$71,$61,$38,$71,$71,$38,
$61,$71,$38,$55,$71,$38,$45,$71,$51,$51,$71,$59,$51,$71,$61,$51,
$71,$69,$51,$71,$71,$51,$71,$71,$51,$69,$71,$51,$61,$71,$51,$59,
$71,$51,$51,$71,$59,$51,$71,$61,$51,$71,$69,$51,$71,$71,$51,$69,
$71,$51,$61,$71,$51,$59,$71,$51,$51,$71,$51,$51,$71,$59,$51,$71,
$61,$51,$71,$69,$51,$71,$71,$51,$69,$71,$51,$61,$71,$51,$59,$71,
$00,$00,$41,$10,$00,$41,$20,$00,$41,$30,$00,$41,$41,$00,$41,$41,
$00,$30,$41,$00,$20,$41,$00,$10,$41,$00,$00,$41,$10,$00,$41,$20,
$00,$41,$30,$00,$41,$41,$00,$30,$41,$00,$20,$41,$00,$10,$41,$00,
$00,$41,$00,$00,$41,$10,$00,$41,$20,$00,$41,$30,$00,$41,$41,$00,
$30,$41,$00,$20,$41,$00,$10,$41,$20,$20,$41,$28,$20,$41,$30,$20,
$41,$38,$20,$41,$41,$20,$41,$41,$20,$38,$41,$20,$30,$41,$20,$28,
$41,$20,$20,$41,$28,$20,$41,$30,$20,$41,$38,$20,$41,$41,$20,$38,
$41,$20,$30,$41,$20,$28,$41,$20,$20,$41,$20,$20,$41,$28,$20,$41,
$30,$20,$41,$38,$20,$41,$41,$20,$38,$41,$20,$30,$41,$20,$28,$41,
$2C,$2C,$41,$30,$2C,$41,$34,$2C,$41,$3C,$2C,$41,$41,$2C,$41,$41,
$2C,$3C,$41,$2C,$34,$41,$2C,$30,$41,$2C,$2C,$41,$30,$2C,$41,$34,
$2C,$41,$3C,$2C,$41,$41,$2C,$3C,$41,$2C,$34,$41,$2C,$30,$41,$2C,
$2C,$41,$2C,$2C,$41,$30,$2C,$41,$34,$2C,$41,$3C,$2C,$41,$41,$2C,
$3C,$41,$2C,$34,$41,$2C,$30,$41,$00,$00,$00,$00,$00,$00,$00,$00,
$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00);

procedure TMainForm.ColorTableNewActionExecute(Sender: TObject);
var
 Last: TColorTable;
 Event: THistoryEvent;
begin
 if FProgressBar = nil then
 with FProject.FMaps.ColorTableList do
 begin
  Event := AddColorTableListEvent('');
  Last := LastNode as TColorTable;
  with AddChild('Unnamed 1') as TColorTable do
  begin
   if Last <> nil then
   begin
    ColorFormat.Assign(Last.ColorFormat);
  //  ColorsCount := Last.ColorsCount;
    ColorsCount := 256;
    RGB_ColorFormat.ConvertTo(ColorFormat, @DefaultPalette, ColorData, 256);
//    TransparentIndex := Last.TransparentIndex;
    TransparentIndex := 0;
   end else
   begin
    ColorFormat.Assign(RGB_ColorFormat);
    ColorsCount := 256;
    Move(DefaultPalette, ColorData^, SizeOf(DefaultPalette));
    TransparentIndex := 0;
   end;

   Event.Text := WideFormat('New color table ''%s''', [Name]);
  end;

  ColorTableIndex := Count - 1;

  ColorTablesModified;

  Event.RedoData := FProject.FMaps.ColorTableList;
 end;
end;

procedure TMainForm.ColorTableUpActionExecute(Sender: TObject);
var
 I, NewII, J, L: Integer;
 Detached: array of TNode;
 Node, Prv: TNode;
 List: TColorTableList;
 Event: THistoryEvent;
begin
 if FProgressBar = nil then
 begin
  List := FProject.FMaps.ColorTableList;
  with List do
   if Count > 1 then
   begin
    if ColorTablesListBox.SelCount = 0 then
     ColorTablesListBox.Selected[ColorTableIndex] := True;

    if ColorTablesListBox.Selected[0] then Exit;

    Event := NewMoveColorTablesEvent(-1);

    NewII := -1;
    Node := LastNode;
    for I := Count - 1 downto 0 do
    begin
     Prv := Node.Prev;
     if ColorTablesListBox.Selected[I] then
     begin
      NewII := I - 1;
      L := Length(Detached);
      SetLength(Detached, L + 1);
      Detached[L] := Node;
      DetachNode(Node);
      Node.Index := NewII;
     end;
     Node := Prv;
    end;

    ColorTablesListBox.ClearSelection;

    for I := Length(Detached) - 1 downto 0  do
    begin
     Node := Detached[I];
     J := Node.Index;
     AttachNode(Node, J);
     ColorTablesListBox.Selected[J] := True;
    end;

    United := True;

    ColorTablesListBox.Invalidate;

    ColorTableIndex := NewII;

   { ColorTablesListBox.ItemIndex := NewII;
    ColorTablesListBox.Selected[NewII] := True;
    FillColorTablesCombo;}
    ColorTablesModified;

    Event.RedoData := List;
   end;
 end;  
end;

procedure TMainForm.ColorTableUpActionUpdate(Sender: TObject);
var
 Enable: Boolean;
begin
 Enable := False;
 if(FProgressBar = nil) and
   (FProject.FMaps.ColorTableList.Count > 1) then
 begin
  if ColorTablesListBox.SelCount > 0 then
   Enable := not ColorTablesListBox.Selected[0] else
   Enable := ColorTableIndex > 0;
 end;

 ColorTableUpAction.Enabled := Enable;
end;

function TMainForm.NewMoveColorTablesEvent(
  Shift: Integer): THistoryEvent;
var
 Str: WideString;
 NewII: Integer;
begin
 if ColorTablesListBox.SelCount = 1 then
 begin
  NewII := ColorTableIndex;
  Str := WideFormat('Change color table ''%s'' index to #%d',
        [FProject.FMaps.ColorTableList[NewII].Name, NewII + Shift + 1]);
 end else
  Str := 'Move color tables';

 Result := AddColorTableListEvent(Str);
end;

procedure TMainForm.ColorTableDownActionExecute(Sender: TObject);
var
 I, NewII, J, L: Integer;
 Detached: array of TNode;
 Node, Prv: TNode;
 List: TColorTableList;
 Event: THistoryEvent;
begin
 if FProgressBar = nil then
 begin
  List := FProject.FMaps.ColorTableList;
  with List do
   if Count > 1 then
   begin
    if ColorTablesListBox.SelCount = 0 then
     ColorTablesListBox.Selected[ColorTableIndex] := True;

    if ColorTablesListBox.Selected[Count - 1] then Exit;

    Event := NewMoveColorTablesEvent(+1);     

    NewII := -1;
    Node := LastNode;
    for I := Count - 1 downto 0 do
    begin
     Prv := Node.Prev;
     if ColorTablesListBox.Selected[I] then
     begin
      if NewII < 0 then NewII := I + 1;
      L := Length(Detached);
      SetLength(Detached, L + 1);
      Detached[L] := Node;
      DetachNode(Node);
      Node.Index := I + 1;
     end;
     Node := Prv;
    end;

    ColorTablesListBox.ClearSelection;

    for I := Length(Detached) - 1 downto 0  do
    begin
     Node := Detached[I];
     J := Node.Index;
     AttachNode(Node, J);
     ColorTablesListBox.Selected[J] := True;
    end;

    United := True;

    ColorTablesListBox.Invalidate;

    ColorTableIndex := NewII;

    ColorTablesModified;

    Event.RedoData := List;
    {
    ColorTablesListBox.ItemIndex := NewII;
    ColorTablesListBox.Selected[NewII] := True;
    FillColorTablesCombo;}
   end;
 end;
end;

procedure TMainForm.ColorTableDownActionUpdate(Sender: TObject);
var
 Enable: Boolean;
begin
 Enable := False;
 if FProgressBar = nil then
  with FProject.FMaps.ColorTableList do
   if Count > 1 then
   begin
    if ColorTablesListBox.SelCount > 0 then
     Enable := not ColorTablesListBox.Selected[Count - 1] else
     Enable := ColorTableIndex < Count - 1;
   end;

 ColorTableDownAction.Enabled := Enable;
end;

procedure TMainForm.ColorTablesModified(Deleted: Boolean);
var
 I: Integer;
begin
 with FProject.FMaps.ColorTableList do
  if ColorTablesListBox.Count <> Count then
   ColorTablesListBox.Count := Count else
   ColorTablesListBox.Invalidate;
 I := ColorTableIndex;
 if I < 0 then
 begin
  I := 0;
  ColorTableIndex := 0;
 end;
 FillColorTablesCombo;
 if ColorTablesListBox.Count > 0 then
 begin
  ColorTablesListBox.ItemIndex := I;
  ColorTablesListBox.Selected[I] := True;
 end;
 ColorTableSelected;
 for I := 0 to Length(MapFramesList) - 1 do
  with MapFramesList[I] do
  begin
   RefreshMapProps;
   if Deleted then
   begin
    RefreshColorTable;
    MapFrame.MapModified := True;
   end;
  end;

 Saved := False; 
end;

procedure TMainForm.ColorTableSelected;
var
 I: Integer;
begin
 PaletteFrame.ColorTable := FProject.FMaps.ColorTableList[ColorTableIndex];

 RefreshColorTableProps;
 
 for I := 0 to Length(TileSetFramesList) - 1 do
  with TileSetFramesList[I] do
   if TilesFrame.ColorTable <> PaletteFrame.ColorTable then
    TileSetChanged;

 if MainFrame is TTileEditFrame then
  with TTileEditFrame(MainFrame) do
   ColorTable := PaletteFrame.ColorTable;
end;

procedure TMainForm.ColorTableDeselect;
begin
 with PaletteFrame do
  if (ColorTable <> nil) and
     ColorTablesListBox.Selected[ColorTable.Index] then
   PaletteView.Deselect;

 PalPropEditor.AcceptEdit;
end;

procedure TMainForm.ColorTablesListBoxDblClick(Sender: TObject);
begin
//
end;

procedure TMainForm.ColorTableColorsModified(Item: TColorTable);
var
 I: Integer;
begin
 with PaletteFrame do
  if ColorTable = Item then
   PaletteView.Invalidate;
 
 for I := 0 to Length(MapFramesList) - 1 do
  with MapFramesList[I] do
   if (MapFrame.MapData <> nil) and
      (MapFrame.MapData.ColorTable = Item) then
   begin
    RefreshColorTable;
    LayersListBox.Invalidate;
   end;

 if MainFrame is TTileEditFrame then
  TTileEditFrame(MainFrame).RefreshColorTable;   

 for I := 0 to Length(TileSetFramesList) - 1 do
  with TileSetFramesList[I].TilesFrame do
   if ColorTable = Item then
    RefreshColorTable;

 Saved := False;   
end;

procedure TMainForm.ColorsExportActionExecute(Sender: TObject);
var
 Stream: TStream;
 X, Y, I, Cnt: Integer;
 List: TColorTableList;
 ct: TColorTable;
 Buf: array of LongWord;
begin
 if FProgressBar = nil then
  with PaletteFrame do
   if ColorTable <> nil then
   begin
    StartDialog;
    try
     SaveActDialog.FileName := FixedFileName(ColorTable.Name);
     if SaveActDialog.Execute then
     begin
      SetLength(Buf, ColorTable.ColorsCount);
      Cnt := 0;
      I := 0;
      with PaletteView do
       if MultiSelected then
       begin
        PasteBufferApply;
        for Y := 0 to MapHeight - 1 do
         for X := 0 to MapWidth - 1 do
         begin
          if Selected[X, Y] then
          begin
           Buf[Cnt] := RGBQuads[I];
           Inc(Cnt);
          end;
          Inc(I);
         end;
       end;
      if Cnt > 0 then
      begin
       ct := TColorTable.Create;
      end else
       ct := ColorTable;
      try
       if Cnt > 0 then
       begin
        ct.ColorFormat.Assign(ColorTable.ColorFormat);
        ct.ColorsCount := Cnt;
        ct.ColorFormat.ConvertFromRGBQuad(Buf, ct.ColorData, Cnt);
       end;
       Stream := TTntFileStream.Create(SaveActDialog.FileName, fmCreate);
       try
        case SaveActDialog.FilterIndex of
         1: ct.SaveActToStream(Stream);
         2:
         begin
          List := TColorTableList.Create;
          try
           List.AddNode.Assign(ct);
           List.SaveToStream(Stream);
          finally
           List.Free;
          end;
         end;
         3: Stream.WriteBuffer(ct.ColorData^, ct.TableSize);
        end;
       finally
        Stream.Free;
       end;
      finally
       if ct <> ColorTable then
         ct.Free;
      end;
     end;
    except
     WideMessageDlg('Error saving file', mtError, [mbOk], 0);
    end;
    EndDialog;
   end;
end;

procedure TMainForm.RefreshMapContent(Map: TMap;
            ContentSet: TRefreshMapContentSet);
var
 I: Integer;
begin
 if ContentSet <> [] then
  for I := 0 to Length(MapFramesList) - 1 do
   with MapFramesList[I] do
    if Map = MapFrame.MapData then
    begin
     if rmcView in ContentSet then
      MapFrame.MapEditView.Invalidate;
     if rmcLayersList in ContentSet then
      LayersListBox.Invalidate;
     if rmcLayerProps in ContentSet then
      RefreshLayerProps;
    end;
end;

procedure TMainForm.MapsFrameListBoxEnter(Sender: TObject);
begin
 FFocusedMapsFrame := (Sender as TCustomListBox).Owner as TMapsFrame;
end;

procedure TMainForm.MapsFrameListBoxExit(Sender: TObject);
begin
 FFocusedMapsFrame := nil;
end;

procedure TMainForm.TilesFrameTileSetsListBoxEnter(Sender: TObject);
begin
 FFocusedTileSetsFrame := (Sender as TCustomListBox).Owner as TTileSetsFrame;
end;

procedure TMainForm.TilesFrameTileSetsListBoxExit(Sender: TObject);
begin
 FFocusedTileSetsFrame := nil;
end;

procedure TMainForm.CopyColorTables(Cut: Boolean);
var
 Temp: TColorTableList;
 X: Integer;
 Stream: TMemoryStream;
begin
 if (FProgressBar = nil) and (ColorTablesListBox.SelCount > 0) then
 begin
  Temp := TColorTableList.Create;
  try
   Temp.FInternalFlags := [secLoading];
   with FProject.FMaps.ColorTableList do
    for X := 0 to Count - 1 do
     if ColorTablesListBox.Selected[X] then
      Temp.AddNode.Assign(Nodes[X]);

   if OpenClipboard(0) then
   try
    Stream := TMemoryStream.Create;
    try
     Temp.SaveToStream(Stream);
     SetClipboardData(CF_COLORTABLES, GlobalHandle(Stream.Memory));
    finally
     Stream.Free;
    end;
   finally
    CloseClipboard;
   end;
  finally
   Temp.Free;
  end;
  if Cut then ColorTableDeleteActionExecute(ColorTableDeleteAction);
 end;
end;

procedure TMainForm.PasteColorTables;
var
 Stream: TBufferStream;
 ClpHandle: THandle;
 Paste: TColorTableList;
 I, J, NewIndex: Integer;
begin
 if FProgressBar = nil then
 begin
  if OpenClipboard(0) then
  try
   ClpHandle := GetClipboardData(CF_COLORTABLES);
   if ClpHandle <> 0 then
   begin
    Stream := TBufferStream.Create(GlobalLock(ClpHandle), GlobalSize(ClpHandle));
    try
     Paste := TColorTableList.Create;
     try
      Paste.LoadFromStream(Stream);

      with FProject.FMaps.ColorTableList do
      begin
       NewIndex := Count;
       for J := 0 to Paste.Count - 1 do
        AddNode.Assign(Paste.Nodes[J]);
      end;
     finally
      Paste.Free;
     end;

     with FProject.FMaps.ColorTableList do
     begin
      ColorTablesListBox.Count := Count;

      for I := NewIndex to Count - 1 do
       ColorTablesListBox.Selected[I] := True;

      ColorTableIndex := NewIndex;

      ColorTablesModified(False);
     end; 
    finally
     GlobalUnlock(ClpHandle);
     Stream.Free;
    end;
   end;
  finally
   CloseClipboard; 
  end;
 end;
end;

procedure TMainForm.ColorContentModified;
var
 I: Integer;
begin
 PaletteFrame.PaletteView.Invalidate;
 
 for I := 0 to Length(MapFramesList) - 1 do
  with MapFramesList[I] do
  begin
   RefreshColorTable;
   LayersListBox.Invalidate;
  end;

 for I := 0 to Length(TileSetFramesList) - 1 do
  with TileSetFramesList[I] do
   TilesFrame.RefreshColorTable;

 Saved := False;   
end;

procedure TMainForm.MapsFrameLayersListBoxContextPopup(Sender: TObject;
  MousePos: TPoint; var Handled: Boolean);
begin
 with Sender as TLayerListBox do
 begin
  SetFocus;
  with Owner as TMapsFrame do
  begin
   with LayersPopup.Items do
   begin
    Items[0].Action := LayerNewAction;
    Items[2].Action := LayerUpAction;
    Items[3].Action := LayerDownAction;
    Items[13].Action := LayerDeleteAction;
   end;
  end;
 end;
end;

procedure TMainForm.MapsFrameMapsListBoxContextPopup(Sender: TObject;
  MousePos: TPoint; var Handled: Boolean);
begin
 with Sender as TCustomListBox do
 begin
  SetFocus;
  with Owner as TMapsFrame do
  begin
   if BrushMode then
   begin
    MapNewMenuItem.Action          := BrushNewAction;
    MapAddFromFileMenuItem.Action  := BrushAddFromFileAction;
    MapSaveToFileMenuItem.Action   := BrushSaveToFileAction;
    MapMoveUpMenuItem.Action       := BrushUpAction;
    MapMoveDownMenuItem.Action     := BrushDownAction;
    MapDeleteMenuItem.Action       := BrushDeleteAction;
   end else
   begin
    MapNewMenuItem.Action          := MapNewAction;
    MapAddFromFileMenuItem.Action  := MapAddFromFileAction;
    MapSaveToFileMenuItem.Action   := MapSaveToFileAction;
    MapMoveUpMenuItem.Action       := MapUpAction;
    MapMoveDownMenuItem.Action     := MapDownAction;
    MapDeleteMenuItem.Action       := MapDeleteAction;
   end;
  end;
 end;
end;

procedure TMainForm.ColorsMakeGradientActionExecute(Sender: TObject);
var
 X, Y, I, J, K, CI: Integer;
 CCnt, Cnt, w: Integer;
 cl, NewColor: TColor;
 NewRGB: PickColorDlg.TRGB absolute NewColor;
 StartRGB: PickColorDlg.TRGB;
 Rg, Bg, Gg: Double;
 Event: THistoryEvent;
begin
 if FProgressBar = nil then
  with PaletteFrame, PaletteView do
   if MultiSelected then
   begin
    PasteBufferApply;
    with CreatePickColorDialog do
    try
     ColorFormat := ColorTable.ColorFormat;    
     ColorPickerCursor := crDropper;
     Cnt := 0;
     CI := 0;
     cl := -1;
     for Y := 0 to MapHeight - 1 do
      for X := 0 to MapWidth - 1 do
      begin
       if Selected[X, Y] then
       begin
        cl := Colors[CI];
        if Cnt = 0 then
         ColorsCollection[0] := cl;
        Inc(Cnt);
       end;
       Inc(CI);
      end;
     Color := ColorsCollection[0];
     if Color <> cl then
      ColorsCollection[1] := cl;
     if Execute then
     begin
      CCnt := CollectionSize;
      if CCnt > 0 then
      begin
       CI := 0;
       I := 0;
       Event := FProject.History.AddEvent(ColorTable,
        WideFormat('Color table ''%s'': Make gradient', [ColorTable.Name]));
       if CCnt > Cnt then
       begin
        for Y := 0 to MapHeight - 1 do
         for X := 0 to MapWidth - 1 do
         begin
          if Selected[X, Y] then
          begin
           Colors[CI] := ColorsCollection[I];
           Inc(I);
          end;
          Inc(CI);
         end;
       end else
       begin
        if CCnt > 2 then
         w := Cnt div (CCnt - 1) else
         w := Cnt;
        TColor(StartRGB) := ColorsCollection[0];
        Rg := 0.0;
        Gg := 0.0;
        Bg := 0.0;
        j := w - 1;
        NewRGB.Z := 0;
        K := 0;

        for Y := 0 to MapHeight - 1 do
         for X := 0 to MapWidth - 1 do
         begin
          if Selected[X, Y] then
          begin
           if j = w then
           begin
            StartRGB := NewRGB;

            Inc(I);
            if I >= CCnt then
             I := 0;

            TColor(NewRGB) := ColorsCollection[I];

            if w > 1 then
            begin
             if (K + (w - 1) > Cnt) or
                (K + (w - 1) shl 1 > Cnt) then
              w := (Cnt - K) + 1;
             if w > 1 then
             begin
              Rg := (NewRGB.R - StartRGB.R) / (w - 1);
              Gg := (NewRGB.G - StartRGB.G) / (w - 1);
              Bg := (NewRGB.B - StartRGB.B) / (w - 1);
             end else
             begin
              Rg := 0.0;
              Gg := 0.0;
              Bg := 0.0;
             end;
            end;

            j := 1;
           end;

           NewRGB.R := StartRGB.R + Round(Rg * J);
           NewRGB.G := StartRGB.G + Round(Gg * J);
           NewRGB.B := StartRGB.B + Round(Bg * J);
           Inc(J);           

           Colors[CI] := NewColor;
           Inc(K);          
          end;
          Inc(CI);
         end;
       end;
       ColorTableColorsModified(ColorTable);

       Event.RedoData := ColorTable;
      end;
     end;
    finally
     Free;
    end;
   end;
end;

procedure TMainForm.ColorsMakeGradientActionUpdate(Sender: TObject);
begin
 ColorsMakeGradientAction.Enabled := (FProgressBar = nil) and
   (PaletteFrame.PaletteView.MultiSelected);
end;

procedure TMainForm.ColorReplaceActionExecute(Sender: TObject);
var
 I: Integer;
 Event: THistoryEvent;
begin
 if FProgressBar = nil then
  with PaletteFrame do
   if IndexUnderCursor >= 0 then
    with PaletteView do
    begin
     Deselect;
     Selected[TileX, TileY] := True;
     FillTempSelection(Bounds(0, 0, MapWidth, MapHeight)); 

     with CreatePickColorDialog do
     try
      ColorFormat := ColorTable.ColorFormat;
      for I := 0 to 15 do
       AddToCollection(FColorsCollection[I]);      
      ColorPickerCursor := crDropper;
      Color := Colors[IndexUnderCursor];
      if Execute then
      begin
       Event := FProject.History.AddEvent(ColorTable,
        WideFormat('Color table ''%s'': Color #%d = 0x%6.6x', [ColorTable.Name,
                          IndexUnderCursor, SwapLong(Color) shr 8]));
       Colors[IndexUnderCursor] := Color;
       ColorTableColorsModified(ColorTable);
       Event.RedoData := ColorTable;
      end;
      for I := 0 to 15 do
       FColorsCollection[I] := ColorsCollection[I];
     finally
      Free;
     end;
    end;
end;

procedure TMainForm.ColorReplaceActionUpdate(Sender: TObject);
begin
 ColorReplaceAction.Enabled := (FProgressBar = nil) and
   (PaletteFrame.IndexUnderCursor >= 0);
end;

procedure TMainForm.ListBoxContextPopup(Sender: TObject;
  MousePos: TPoint; var Handled: Boolean);
begin
 TWinControl(Sender).SetFocus;
end;

procedure TMainForm.TilesFrameTileSetsListBoxContextPopup(Sender: TObject;
  MousePos: TPoint; var Handled: Boolean);
begin
 with Sender as TCustomListBox do
 begin
  SetFocus;
  with Owner as TTileSetsFrame do
  begin
   TileSetNewMenuItem.Action := TileSetNewAction;
   TileSetAddFromFilesMenuItem.Action := TileSetAddFromFilesAction;
   TileSetSaveToFileMenuItem.Action := TileSetSaveToFileAction;
   TileSetMoveUpMenuItem.Action := TileSetUpAction;
   TileSetMoveDownMenuItem.Action := TileSetDownAction;
   TileSetDeleteMenuItem.Action := TileSetDeleteAction;
  end;
 end;
end;

procedure TMainForm.PaletteFramePaletteViewCustomMouseActionCheck(
  Sender: TCustomTileMapView; Shift: TShiftState; var Act: Boolean);
var
 c: LongWord;
 I: Integer;
 SP: PBoolean;
 NewValue: Boolean;
 Contiguous: Boolean;

begin
 if (DrawTool = dtMagicWand) and
    (ssLeft in Shift) then
 begin
  Sender.PasteBufferApply; 
  Contiguous := ssCtrl in Shift;
  if Contiguous then
   Shift := Shift - [ssCtrl];
  with PaletteFrame, PaletteView do
  if (Shift = SelectNewShiftState) or
     (Shift = SelectIncludeShiftState) or
     (Shift = SelectExcludeShiftState) then
  begin
   if not Contiguous then
   begin
    c := RGBQuads[IndexUnderCursor];
    SP := SelectionBuffer;
    NewValue := Shift <> SelectExcludeShiftState;
    if Shift = SelectNewShiftState then
     FillChar(SP^, MapWidth * MapHeight, 0);
    for I := 0 to ColorsCount - 1 do
    begin
     if RGBQuads[I] = c then
      SP^ := NewValue;
     Inc(SP);
    end;
    UpdateSelection;
   end else
    ContinguousPaletteSelect(TileX, TileY, Shift);
   UpdateMagicCursor(PaletteView, Shift);
  end;
 end else
 if Shift = [ssLeft, ssDouble] then
  ColorReplaceActionExecute(ColorReplaceAction);
 Act := False;
end;

procedure TMainForm.HistoryListBoxData(Control: TWinControl;
  Index: Integer; var Data: String);
begin
 // TODO 1 -oAlicia -cHistory: HistoryListBoxData
end;

procedure TMainForm.ColorTablesListBoxData(Control: TWinControl;
  Index: Integer; var Data: String);
var
 Item: TColorTable;
begin
 Item := FProject.FMaps.ColorTableList.Nodes[Index] as TColorTable;
 if Item <> nil then
  Data := UTF8Encode(WideFormat('%d. %s', [Item.Index + 1, Item.Name]));
end;

procedure TMainForm.ListBoxDrawItem(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
var
 WStr: WideString;
 AStr: AnsiString;
 Flags: Integer;
begin
 if Index >= 0 then
  with Control as TListBox do
  begin
   Canvas.FillRect(Rect);
   AStr := '';
   if (Style in [lbVirtual, lbVirtualOwnerDraw]) then
   begin
    if Assigned(OnData) then
     OnData(Control, Index, AStr);
   end else
    AStr := Items[Index];

   WStr := UTF8Decode(AStr);

   Flags := DrawTextBiDiModeFlags(DT_SINGLELINE or DT_VCENTER or DT_NOPREFIX);
   if not UseRightToLeftAlignment then
     Inc(Rect.Left, 2) else
     Dec(Rect.Right, 2);
   Windows.DrawTextW(Canvas.Handle, Pointer(WStr), Length(WStr), Rect, Flags);
  end;
end;

procedure TMainForm.InitMapsFrameEvents(Frame: TMapsFrame);
begin
 Frame.OnOpen                := MapsFrameMapOpen;
 Frame.OnMapSelected         := MapsFrameMapSelected;
 Frame.OnMapModified         := MapsFrameMapModified;
 Frame.OnMapsModified        := MapsFrameMapsModified;
 Frame.OnMapDeselect         := MapsFrameMapDeselect;
 Frame.OnMapNameModified     := MapsFrameMapNameModified;
 Frame.OnMapPropsModified    := MapsFrameMapPropsModified;
 Frame.OnRefreshMapContent   := MapsFrameRefreshMapContent;
 Frame.OnLayersModified      := MapsFrameLayersModified;
 Frame.OnLayerDblClick       := MapsFrameLayerDblClick;
 Frame.OnPageControlChange   := MapsFramePageControlChange;
 Frame.OnMapSelectionChanged := MapsFrameMapSelectionChanged;
 Frame.OnMapComboSelected    := MapsFrameMapComboSelected;
end;

procedure TMainForm.InitTileSetsFrameEvents(Frame: TTileSetsFrame);
begin
 Frame.OnOpen                    := TilesFrameTileSetOpen;
 Frame.OnTileSetModified         := TilesFrameTileSetModified;
 Frame.OnTileSetPropsModified    := TilesFrameTileSetPropsModified;
 Frame.OnTileSetsModified        := TilesFrameTileSetsModified;
 Frame.OnTileSetDeselect         := TilesFrameTileSetDeselect;
 Frame.OnTileSetNameModified     := TilesFrameTileSetNameModified;
 Frame.OnTileSetSelected         := TilesFrameTileSetSelected;
 Frame.OnTileSetSelectionChanged := TilesFrameTileSetSelectionChanged;
 Frame.OnPageControlChange       := TilesFramePageControlChange;
 Frame.OnTileSetComboSelected    := TilesFrameTileSetComboSelected;
 Frame.OnStartDialog             := FrameStartDialog;
 Frame.OnEndDialog               := FrameEndDialog;
 Frame.OnTilesModified           := TilesFrameTilesModified;
 Frame.OnBeforeUnhook            := TilesFrameBeforeUnhook;
 Frame.OnAfterUnhook             := TilesFrameAfterUnhook;
end;

procedure TMainForm.MapsFrameLayerDblClick(Sender: TMapsFrame;
                                           Layer: TMapLayer;
                                           TileClicked: Boolean);
var
 I: Integer;
 ATileSet: TTileSet;
 ATileIndex: Integer;
 Frame: TTileSetsFrame;
begin
 if Layer <> nil then
 begin
  ATileSet := Layer.TileSet;
  if ATileSet <> nil then
  begin
   ATileIndex := Layer.DrawInfo.TileIndex;

   for I := 0 to Length(TileSetFramesList) - 1 do
   begin
    Frame := TileSetFramesList[I];
    with Frame do
     if (ATileSet = TilesFrame.TileSet) and
        (PageControl.ActivePageIndex <> 0) then
     begin
      if ATileIndex >= 0 then
       TilesFrame.SelectedTile := ATileIndex else
       TilesFrame.SelectedTile := FProject.FTileSetInfo[ATileSet.Index].SelectedTile;
      TilesFrame.TileSetView.SetFocus;
      TilesFrameTileSetComboSelected(Frame);
      Exit;
     end;
   end;

   for I := 0 to Length(TileSetFramesList) - 1 do
   begin
    Frame := TileSetFramesList[I];
    with Frame do
     if PageControl.ActivePageIndex <> 0 then
     begin
      SelectTileSet(ATileSet.Index);

      if ATileIndex >= 0 then
      begin
       Application.ProcessMessages;
       TilesFrame.SelectedTile := ATileIndex;
      end;

      TilesFrame.TileSetView.SetFocus;
      TilesFrameTileSetComboSelected(Frame);
      Exit;
     end;
   end;

   with TilesFrm do
   begin
    SelectTileSet(ATileSet.Index);

    if TileClicked then
    begin
     PageControl.ActivePageIndex := 1;
     PageControlChange(PageControl);
     TilesFrame.TileSetView.SetFocus;
     TilesFrameTileSetComboSelected(TilesFrm);
    end;

    if ATileIndex >= 0 then
    begin
     Application.ProcessMessages;
     TilesFrame.SelectedTile := ATileIndex;
    end;
   end;
  end;
 end;
end;

procedure TMainForm.MapsFrameMapModified(Sender: TObject);
var
 I, J: Integer;
 Frame: TMapsFrame;
 Map: TMap;
 Nothing: Boolean;
begin
 with Sender as TMapsFrame do
 begin
  Map := MapFrame.MapData;
  for I := 0 to Length(MapFramesList) - 1 do
  begin
   Frame := MapFramesList[I];
   if (Frame <> Sender) and
      (Frame.BrushMode = BrushMode) then
    with Frame, MapFrame do
     if MapData <> nil then
     begin
      Nothing := True;
      for J := 0 to MapsListBox.Count - 1 do
       if MapsListBox.Selected[J] then
        if MapData.Index = J then
        begin
         MapModified := True;
         Nothing := False;
         Break;
        end;

      if Nothing and (MapData = Map) then
       MapModified := True;
     end;
  end;
 end;
 Saved := False;
end;

procedure TMainForm.MapsFrameMapOpen(Sender: TObject);
var
 I: Integer;
 Frame: TMapsFrame;
 Map: TMap;
 Source: TMapsFrame absolute Sender;
begin
 with Sender as TMapsFrame do
 begin
  Map := MapFrame.MapData;
  for I := 0 to Length(MapFramesList) - 1 do
  begin
   Frame := MapFramesList[I];
   if Frame <> Source then
    with Frame do
     if Source.BrushMode = BrushMode then
     begin
      if PageControl.ActivePageIndex = 0 then
      begin
       PageControl.ActivePageIndex := 1;
       PageControlChange(PageControl);
      end;
      if MapFrame.MapData <> Map then
      begin
       SelectMap(Map.Index);
       MapFrame.MapEditView.SetFocus;
      end;
      Exit;
     end;
  end;
  if BrushMode then
   EditMode := emBrush else
   EditMode := emMap;

  with TMapsFrame(MainFrame) do
  begin
   if PageControl.ActivePageIndex = 0 then
   begin
    PageControl.ActivePageIndex := 1;
    PageControlChange(PageControl);
   end;
   Application.ProcessMessages;
   SelectMap(Map.Index);
   MapFrame.MapEditView.SetFocus;
  end;
 end;

 MainEditActivePage := 1;
end;

procedure TMainForm.MapsFrameMapSelected(Sender: TObject);
begin
 with Sender as TMapsFrame do
 begin
  if PageControl.ActivePageIndex <> 0 then
  begin
   with MapFrame do
    if MapData <> nil then
     SelectColorTable(MapData.ColorTableIndex);
  end;
 end;
end;

procedure TMainForm.MapsFrameLayersModified(Sender: TObject);
var
 I: Integer;
 Frame: TMapsFrame;
 Source: TMapsFrame absolute Sender;
begin
 for I := 0 to Length(MapFramesList) - 1 do
 begin
  Frame := MapFramesList[I];
  if (Frame <> Source) and
     (Frame.BrushMode = Source.BrushMode) then
  begin
   Frame.MapFrame.MapEditView.Invalidate;
   Frame.RefreshLayersList;
  end;
 end;
 Saved := False; 
end;

procedure TMainForm.MapsFrameMapDeselect(Sender: TObject);
var
 I, J: Integer;
 Frame: TMapsFrame;
 Map: TMap;
 Nothing: Boolean;
begin
 with Sender as TMapsFrame do
 begin
  Map := MapFrame.MapData;
  for I := 0 to Length(MapFramesList) - 1 do
  begin
   Frame := MapFramesList[I];
   if (Frame <> Sender) and
      (Frame.BrushMode = BrushMode) then
    with Frame, MapFrame do
     if MapData <> nil then
     begin
      Nothing := True;
      for J := 0 to MapsListBox.Count - 1 do
       if (MapsListBox.Selected[J]) and
          (MapData.Index = J) then
       begin
        if PageControl.ActivePageIndex = 0 then
         MapPropEditor.AcceptEdit;

        MapEditView.Deselect;
        Nothing := False;
        Break;
       end;

      if Nothing and (Map <> nil) and (MapData = Map) then
      begin
       if PageControl.ActivePageIndex = 0 then
        MapPropEditor.AcceptEdit;

       MapEditView.Deselect;
      end;
     end;
  end;
 end;
end;

procedure TMainForm.MapsFrameMapNameModified(Sender: TObject);
var
 I: Integer;
 Frame: TMapsFrame;
 Source: TMapsFrame absolute Sender;
begin
 for I := 0 to Length(MapFramesList) - 1 do
 begin
  Frame := MapFramesList[I];
  if (Frame <> Source) and
     (Frame.BrushMode = Source.BrushMode) then
   with Frame do
   begin
    MapsListBox.Invalidate;
    FillComboBox;
    if PageControl.ActivePageIndex = 0 then
     RefreshMapProps;
   end;
 end;
 Saved := False;
end;

procedure TMainForm.MapsFrameMapsModified(Sender: TObject);
var
 I: Integer;
 Frame: TMapsFrame;
 Source: TMapsFrame absolute Sender;
begin
 for I := 0 to Length(MapFramesList) - 1 do
 begin
  Frame := MapFramesList[I];
  if (Frame <> Source) and
     (Frame.BrushMode = Source.BrushMode) then
   Frame.RefreshLists;
 end;
 Saved := False;
end;

procedure TMainForm.MapsFrameRefreshMapContent(Sender: TMapsFrame; Layers: Boolean);
var
 I: Integer;
 Frame: TMapsFrame;
 Map: TMap;
begin
 with Sender as TMapsFrame do
  Map := MapFrame.MapData;
 for I := 0 to Length(MapFramesList) - 1 do
 begin
  Frame := MapFramesList[I];
  if (Frame <> Sender) and
     (Frame.MapFrame.MapData = Map) then
  begin
   Frame.MapFrame.MapEditView.Invalidate;
   if Layers then
   begin
    Frame.LayersListBox.Invalidate;
    Frame.RefreshLayerProps;
   end;
  end;
 end;
end;

procedure TMainForm.MapsFrameMapPropsModified(Sender: TObject);
var
 I: Integer;
 Frame: TMapsFrame;
 Source: TMapsFrame absolute Sender;
begin
 for I := 0 to Length(MapFramesList) - 1 do
 begin
  Frame := MapFramesList[I];
  if (Frame <> Source) and
     (Frame.BrushMode = Source.BrushMode) and
     (Frame.PageControl.ActivePageIndex = 0) then
   Frame.RefreshMapProps;
 end;
 Saved := False;
end;

procedure TMainForm.ColTblViewModeBarChange(Sender: TObject);
var
 I: Integer;
begin
 ColorTablesListBox.Count := ColorTablesListBox.Count;
 I := ColorTableIndex;
 ColorTablesListBox.ItemIndex := I;
 ColorTablesListBox.Selected[I] := True;
 ColTblComboBox.ItemIndex := I; 

 if ColTblViewModeBar.TabIndex <> 0 then
 begin
  PaletteFrame.Hide;
  ColTblComboBox.Hide;
  ColorTablesListBox.Show;
  ColorTablesListBox.SetFocus;
 end else
 begin
  ColorTablesListBox.Hide;
  ColTblComboBox.Show;
  PaletteFrame.Show;
  PaletteFrame.PaletteView.SetFocus;
 end;
 RefreshColorTableProps;
end;

procedure TMainForm.FillColorTablesCombo;
var
 I: Integer;
begin
 ColTblComboBox.Clear;
 with FProject.FMaps.ColorTableList do
 begin
  for I := 0 to Count - 1 do
   with Nodes[I] as TColorTable do
    ColTblComboBox.Items.Add(WideFormat('%d. %s', [I + 1, Name]));

  ColTblComboBox.ItemIndex := ColorTableIndex;
 end;
end;

function TMainForm.GetColorTableIndex: Integer;
begin
 Result := FProject.FInfo.SelectedPaletteIndex;
 with FProject.FMaps.ColorTableList do
  if Result >= Count then
   Result := Count - 1;
end;

procedure TMainForm.SetColorTableIndex(Value: Integer);
begin
 FProject.FInfo.SelectedPaletteIndex := Value;
end;

procedure TMainForm.ColTblComboBoxChange(Sender: TObject);
var
 I: Integer;
begin
 I := ColTblComboBox.ItemIndex;
 ColorTableIndex := I;

 ColorTablesListBox.ClearSelection;
 ColorTablesListBox.ItemIndex := I;
 ColorTablesListBox.Selected[I] := True;
 ColorTableSelected;
end;

procedure TMainForm.TilesFrameTileSetPropsModified(Sender: TObject);
var
 I: Integer;
 Frame: TTileSetsFrame;
 Source: TTileSetsFrame absolute Sender;
begin
 for I := 0 to Length(TileSetFramesList) - 1 do
 begin
  Frame := TileSetFramesList[I];
  if Frame <> Source then
   Frame.RefreshTileSetProps(False);
 end;
 Saved := False;
end;

procedure TMainForm.TilesFrameTileSetDeselect(Sender: TObject);
var
 I, J: Integer;
 Frame: TTileSetsFrame;
 ts: TTileSet;
 Nothing: Boolean;
begin
 with Sender as TTileSetsFrame do
 begin
  ts := TilesFrame.TileSet;
  for I := 0 to Length(TileSetFramesList) - 1 do
  begin
   Frame := TileSetFramesList[I];
   if Frame <> Sender then
    with Frame, TilesFrame do
     if TileSet <> nil then
     begin
      Nothing := True;
      for J := 0 to TileSetsListBox.Count - 1 do
       if (TileSetsListBox.Selected[J]) and
          (TileSet.Index = J) then
       begin
        if PageControl.ActivePageIndex = 0 then
         TileSetPropEditor.AcceptEdit;

        TileSetView.Deselect;
        Nothing := False;
        Break;
       end;

      if Nothing and (ts <> nil) and (TileSet = ts) then
      begin
       if PageControl.ActivePageIndex = 0 then
        TileSetPropEditor.AcceptEdit;

       TileSetView.Deselect;
      end;
     end;
  end;
 end;
end;

procedure TMainForm.TilesFrameTileSetModified(Sender: TObject);
var
 I, J: Integer;
 Frame: TTileSetsFrame;
 ts: TTileSet;
 Nothing: Boolean;
begin
 with Sender as TTileSetsFrame do
  ts := TilesFrame.TileSet;
 for I := 0 to Length(TileSetFramesList) - 1 do
 begin
  Frame := TileSetFramesList[I];
  if Frame <> Sender then
   with Frame, TilesFrame do
    if TileSet <> nil then
    begin
     Nothing := True;
     for J := 0 to TileSetsListBox.Count - 1 do
      if TileSetsListBox.Selected[J] then
       if TileSet.Index = J then
       begin
        TileSetChanged;
        Nothing := False;
        Break;
       end;

     if Nothing and (TileSet = ts) then
      TileSetChanged;
    end;
 end;
 TileFormatModified;
end;

procedure TMainForm.TilesFrameTileSetNameModified(Sender: TObject);
var
 I: Integer;
 Frame: TTileSetsFrame;
 Source: TTileSetsFrame absolute Sender;
begin
 for I := 0 to Length(TileSetFramesList) - 1 do
 begin
  Frame := TileSetFramesList[I];
  if Frame <> Source then
   with Frame do
   begin
    TileSetsListBox.Invalidate;
    FillComboBox;
    RefreshTileSetProps(False);
   end;
 end;
 Saved := False;
end;

procedure TMainForm.TilesFrameTileSetOpen(Sender: TObject);
var
 I: Integer;
 Frame: TTileSetsFrame;
 ts: TTileSet;
 Source: TTileSetsFrame absolute Sender;
begin
 with Sender as TTileSetsFrame do
 begin
  ts := TilesFrame.TileSet;
  for I := 0 to Length(TileSetFramesList) - 1 do
  begin
   Frame := TileSetFramesList[I];
   if Frame <> Source then
    with Frame do
    begin
     if PageControl.ActivePageIndex = 0 then
     begin
      PageControl.ActivePageIndex := 1;
      PageControlChange(PageControl);
     end;
     if TilesFrame.TileSet <> ts then
     begin
      SelectTileSet(ts.Index);
      TilesFrame.TileSetView.SetFocus;
     end;
     Exit;
    end;
  end;

  EditMode := emTileSet;
  with TTileSetsFrame(MainFrame) do
  begin
   if PageControl.ActivePageIndex = 0 then
   begin
    PageControl.ActivePageIndex := 1;
    PageControlChange(PageControl);
   end;
   SelectTileSet(ts.Index);
   TilesFrame.TileSetView.SetFocus;
  end;
 end;

 MainEditActivePage := 1;
end;

procedure TMainForm.TilesFrameTileSetSelected(Sender: TObject);
begin
  //
end;

procedure TMainForm.TilesFrameTileSetsModified(Sender: TObject);
var
 I: Integer;
 Frame: TTileSetsFrame;
 MapsFrm: TMapsFrame;
begin
 for I := 0 to Length(TileSetFramesList) - 1 do
 begin
  Frame := TileSetFramesList[I];
  if Frame <> Sender then
   Frame.RefreshLists;
 end;
 for I := 0 to Length(MapFramesList) - 1 do
 begin
  MapsFrm := MapFramesList[I];
  MapsFrm.RefreshLayerProps;
  MapsFrm.MapFrame.MapEditView.Invalidate;
  MapsFrm.LayersListBox.Invalidate;
 end;

 if MainFrame is TTileEditFrame then
  with TTileEditFrame(MainFrame) do
   if Tile <> nil then
    PropsFrame.ShowTileProps(TTileSet(Tile.Owner), Tile.TileIndex);
 Saved := False;
end;

procedure TMainForm.MapsFramePageControlChange(Sender: TObject);
var
 Frame, Test: TMapsFrame;
 MapFrm: TMapFrame;
 I: Integer;
begin
 Frame := Sender as TMapsFrame;
 I := Frame.PageControl.ActivePageIndex;
 if Frame = MainFrame then
  MainEditActivePage := I else
 if Frame = MapsFrm then
 begin
  if I <> 0 then
   Include(FProject.FInfo.PageSetup, psMap) else
   Exclude(FProject.FInfo.PageSetup, psMap);
 end else
 if Frame = BrushesFrame then
 begin
  if I <> 0 then
   Include(FProject.FInfo.PageSetup, psBrush) else
   Exclude(FProject.FInfo.PageSetup, psBrush);
 end;

 if I = 0 then
 begin
  MapFrm := TObject(SelCellPropEditor.PropertyList.UserTag) as TMapFrame;
  if Frame.MapFrame = MapFrm then
  begin
   for I := 0 to Length(MapFramesList) - 1 do
   begin
    Test := MapFramesList[I];   // Try to select the same map cell
    if (Test <> Frame) and (Test.PageControl.ActivePageIndex <> 0) and
       (Test.MapFrame.MapData = MapFrm.MapData) then
    begin
     Perform(WM_FILLCELLPROPS, Integer(Test.MapFrame), 0);
     Exit;
    end;
   end;
   MapFrm := nil;
  end;
  if MapFrm = nil then
  begin
  // Select cell from map of the same type
   for I := 0 to Length(MapFramesList) - 1 do
   begin
    Test := MapFramesList[I];
    if (Test <> Frame) and
       (Test.BrushMode = Frame.BrushMode) and
       (Test.PageControl.ActivePageIndex <> 0) then
    begin
     Perform(WM_FILLCELLPROPS, Integer(Test.MapFrame), 0);
     Exit;
    end;
   end;
     // Select any map cell
   for I := 0 to Length(MapFramesList) - 1 do
   begin
    Test := MapFramesList[I];
    if (Test <> Frame) and (Test.PageControl.ActivePageIndex <> 0) then
    begin
     Perform(WM_FILLCELLPROPS, Integer(Test.MapFrame), 0);
     Exit;
    end;
   end;
   Perform(WM_FILLCELLPROPS, 0, 0);   
  end;
 end else
  Frame.MapFrame.MapEditView.SelectionChanged;
end;

procedure TMainForm.TilesFramePageControlChange(Sender: TObject);
var
 Frame, Test: TTileSetsFrame;
 TilesFrm: TTilesFrame;
 I: Integer;
begin
 Frame := Sender as TTileSetsFrame;
 I := Frame.PageControl.ActivePageIndex;
 if Frame = MainFrame then
  MainEditActivePage := I else
 if Frame = Self.TilesFrm then
 begin
  if I <> 0 then
   Include(FProject.FInfo.PageSetup, psTileSet) else
   Exclude(FProject.FInfo.PageSetup, psTileSet);
 end;
 if I = 0 then
 begin
  TilesFrm := TObject(TilePropsFrm.SelTilePropEditor.PropertyList.UserTag) as TTilesFrame;
  if Frame.TilesFrame = TilesFrm then
  begin
   for I := 0 to Length(TileSetFramesList) - 1 do
   begin
    Test := TileSetFramesList[I];   // Try to select a tile from the same tile set
    if (Test <> Frame) and (Test.PageControl.ActivePageIndex <> 0) and
       (Test.TilesFrame.TileSet = TilesFrm.TileSet) then
    begin
     TilePropsFrm.Perform(WM_FILLTILEPROPS, Integer(Test.TilesFrame), 0);
     Exit;
    end;
   end;
   TilesFrm := nil;   
  end; 
  if TilesFrm = nil then
  begin           // Select a tile from any tile set
   for I := 0 to Length(TileSetFramesList) - 1 do
   begin
    Test := TileSetFramesList[I];
    if (Test <> Frame) and (Test.PageControl.ActivePageIndex <> 0) then
    begin
     TilePropsFrm.Perform(WM_FILLTILEPROPS, Integer(Test.TilesFrame), 0);
     Exit;
    end;
   end;
   TilePropsFrm.Perform(WM_FILLTILEPROPS, 0, 0);
  end;
 end else
  Frame.TilesFrame.TileSetView.SelectionChanged;
end;

procedure TMainForm.SaveActDialogTypeChange(Sender: TObject);
begin
 case SaveActDialog.FilterIndex of
  1: SaveActDialog.DefaultExt := 'act';
  2: SaveActDialog.DefaultExt := 'cct';
  3: SaveActDialog.DefaultExt := 'bin';
 end;
end;

procedure TMainForm.TilesFrameTileSetSelectionChanged(Sender: TObject);
var
 I: Integer;
 Frame: TTileSetsFrame;
 ts: TTileSet;
 Source: TTileSetsFrame absolute Sender;
begin
 with Sender as TTileSetsFrame do
  ts := TilesFrame.TileSet;
 for I := 0 to Length(TileSetFramesList) - 1 do
 begin
  Frame := TileSetFramesList[I];
  if Frame <> Source then
   with Frame.TilesFrame do
    if TileSet = ts then
     TileSetView.SelectionChanged;
 end;
end;

procedure TMainForm.WMRefreshCTblProps(var Message: TMessage);
var
 VFlags, I: Integer;
 UseTable, ColorTable: TColorTable;
begin
 ColTblPropsLabel.Caption := 'Color table properties';
 with PalPropEditor, PropertyList do
 begin
  ClearList;
  with FProject.FMaps.ColorTableList do
   if Count > 0 then
   begin
    if ColorTablesListBox.SelCount = 0 then
     ColorTablesListBox.Selected[ColorTableIndex] := True;

    VFlags := 0;
    UseTable := nil;
    ColorTable := nil;

    for I := 0 to Count - 1 do
     if ColorTablesListBox.Selected[I] then
     begin
      ColorTable := Nodes[I] as TColorTable;
      if UseTable <> nil then
      begin
       if ColorTable.Name <> UseTable.Name then
        VFlags := VFlags or (1 shl 0);
       if not ColorTable.ColorFormat.SameAs(UseTable.ColorFormat) then
        VFlags := VFlags or (1 shl 1);
       if ColorTable.ColorsCount <> UseTable.ColorsCount then
        VFlags := VFlags or (1 shl 2);
       if ColorTable.TransparentIndex <> UseTable.TransparentIndex then
        VFlags := VFlags or (1 shl 3);
      end else
       UseTable := ColorTable;
      end;

    if UseTable <> nil then
     with UseTable do
     begin
      if UseTable = ColorTable then
        ColTblPropsLabel.Caption := WideFormat('Color table #%d properties', [Index + 1]) else
        ColTblPropsLabel.Caption := 'Selected color tables'' properties';
        
      with AddString('Name', Name) do
       if VFlags and (1 shl 0) <> 0 then
        Parameters := PL_MULTIPLE_VALUE;

      with AddString('Color format', ColorFormat.FormatString) do
       if VFlags and (1 shl 1) <> 0 then
        Parameters := PL_MULTIPLE_VALUE;

      with AddDecimal('Colors count', ColorsCount, 2, 256) do
       if VFlags and (1 shl 2) <> 0 then
        Parameters := PL_MULTIPLE_VALUE;

      with AddDecimal('Transparent index', TransparentIndex, 0, 255) do
       if VFlags and (1 shl 3) <> 0 then
        Parameters := PL_MULTIPLE_VALUE;
     end;
  end;
  RootNodeCount := Count;
 end;
end;

procedure TMainForm.ColorTableSaveToActionUpdate(Sender: TObject);
begin
 ColorTableSaveToAction.Enabled := (FProgressBar = nil) and
                 (FProject.FMaps.ColorTableList.Count > 0);
end;

procedure TMainForm.ColorTableSaveToActionExecute(Sender: TObject);
var
 I: Integer;
 Stream: TStream;
 List: TColorTableList;
 FName, Ext: WideString;
begin
 if FProgressBar = nil then
  with FProject.FMaps.ColorTableList do
   if Count > 0 then
   begin
    if ColorTablesListBox.SelCount = 0 then
     ColorTablesListBox.Selected[ColorTableIndex] := True;

    if ColorTablesListBox.SelCount = 1 then
    begin
     SaveActDialog.FilterIndex := 1;
     SaveActDialog.FileName := FixedFileName(Items[ColorTableIndex].Name);
    end else
    begin
     SaveActDialog.FilterIndex := 2;
     SaveActDialog.FileName := '';
    end;

    if SaveActDialog.Execute then
    begin
     case SaveActDialog.FilterIndex of
      1:
      if ColorTablesListBox.SelCount = 1 then
      begin
       Stream := TTntFileStream.Create(SaveActDialog.FileName, fmCreate);
       try
        Items[ColorTableIndex].SaveActToStream(Stream);
       finally
        Stream.Free;
       end;
      end else
      begin
       FName := WideChangeFileExt(SaveActDialog.FileName, '');

       for I := 0 to Count - 1 do
        if ColorTablesListBox.Selected[I] then
         with Nodes[I] as TColorTable do
         begin
          Stream := TTntFileStream.Create(WideFormat('%s %s.act',
                                         [FName, FixedFileName(Name)]), fmCreate);
          try
           SaveActToStream(Stream);
          finally
           Stream.Free;
          end;
         end;
      end;
      2:
      begin
       List := TColorTableList.Create;
       try
        for I := 0 to Count - 1 do
         if ColorTablesListBox.Selected[I] then
          List.AddNode.Assign(Nodes[I]);

        List.SaveToFile(SaveActDialog.FileName);
       finally
        List.Free;
       end;
      end;
      3:
      if ColorTablesListBox.SelCount = 1 then
      begin
       Stream := TTntFileStream.Create(SaveActDialog.FileName, fmCreate);
       try
        with Items[ColorTableIndex] do
         Stream.WriteBuffer(ColorData^, TableSize);
       finally
        Stream.Free;
       end;
      end else
      begin
       Ext := WideExtractFileExt(SaveActDialog.FileName);
       FName := WideChangeFileExt(SaveActDialog.FileName, '');
       for I := 0 to Count - 1 do
        if ColorTablesListBox.Selected[I] then
         with Nodes[I] as TColorTable do
         begin
          Stream := TTntFileStream.Create(WideFormat('%s %s%s',
                                         [FName, FixedFileName(Name), Ext]), fmCreate);
          try
           Stream.WriteBuffer(ColorData^, TableSize);
          finally
           Stream.Free;
          end;
         end;
      end;
     end;
    end;
   end;
end;

procedure TMainForm.SelectColorTable(Index: Integer);
begin
 if Index >= 0 then
 begin
  with ColorTablesListBox do
  begin
   ClearSelection;
   ItemIndex := Index;
   Selected[Index] := True;
  end;
  ColorTablesListBoxClick(ColorTablesListBox);
 end;
end;

procedure TMainForm.MapsFrameMapSelectionChanged(Sender: TObject);
var
 I: Integer;
 Frame: TMapsFrame;
 Map: TMap;
 Source: TMapsFrame absolute Sender;
begin
 with Sender as TMapsFrame do
  Map := MapFrame.MapData;
 for I := 0 to Length(MapFramesList) - 1 do
 begin
  Frame := MapFramesList[I];
  if Frame <> Source then
   with Frame.MapFrame do
    if MapData = Map then
     MapEditView.SelectionChanged;
 end;
end;

procedure TMainForm.MapsFrameMapComboSelected(Sender: TObject);
var
 I: Integer;
 Frame: TMapsFrame;
 Event: TNotifyEvent;
begin
 with Sender as TMapsFrame do
  for I := 0 to Length(MapFramesList) - 1 do
  begin
   Frame := MapFramesList[I];
   if (Frame <> Sender) and
      (Frame.BrushMode = BrushMode) and
      (Frame.PageControl.ActivePageIndex = 0) then
   begin
    Event := Frame.OnMapSelected;
    Frame.OnMapSelected := nil;
    Frame.SelectMap(MapIndex);
    Frame.OnMapSelected := Event;
   end;
  end
end;

procedure TMainForm.TilesFrameTileSetComboSelected(Sender: TObject);
var
 I: Integer;
 Frame: TTileSetsFrame;
 Event: TNotifyEvent;
begin
 with Sender as TTileSetsFrame do
  for I := 0 to Length(TileSetFramesList) - 1 do
  begin
   Frame := TileSetFramesList[I];
   if (Frame <> Sender) and
      (Frame.PageControl.ActivePageIndex = 0) then
   begin
    Event := Frame.OnTileSetSelected;
    Frame.OnTileSetSelected := nil;
    Frame.SelectTileSet(TileSetIndex);
    Frame.OnTileSetSelected := Event;
   end;
  end
end;

procedure TMainForm.SetMainEditActivePage(Value: Integer);
begin
 FMainEditActivePage := Value;
 if Value <> 0 then
  Include(FProject.FInfo.PageSetup, psMainFrame) else
  Exclude(FProject.FInfo.PageSetup, psMainFrame);
end;

procedure TMainForm.MapFrameMapEditViewContentsChanged(Sender: TObject);
var
 View: TCustomTileMapView;
 Frm: TMapFrame;
 Map: TMap;
 I: Integer;
 Event: THistoryEvent;
begin
 View := Sender as TCustomTileMapView;
 Frm := View.Owner as TMapFrame;
 Map := Frm.MapData;

 for I := 0 to Length(MapFramesList) - 1 do
  with MapFramesList[I].MapFrame do
   if Map = MapData then
    MapEditView.Invalidate;
 Saved := False;

 Event := FProject.History.LastNode as THistoryEvent;

 if Event <> nil then
  with Frm.Owner as TMapsFrame do
   SetMapEventData(Event, Map, ruRedo);
end;

procedure TMainForm.PaletteFramePaletteViewContentsChanged(
  Sender: TObject);
var
 I: Integer;
 Event: THistoryEvent;
begin
 if MainFrame is TTileEditFrame then
  TTileEditFrame(MainFrame).RefreshColorTable;
  
 for I := 0 to Length(TileSetFramesList) - 1 do
  TileSetFramesList[I].TilesFrame.RefreshColorTable;

 for I := 0 to Length(MapFramesList) - 1 do
  with MapFramesList[I] do
  begin
   RefreshColorTable;
   LayersListBox.Invalidate;
  end;

 Saved := False;
 Event := FProject.History.LastNode as THistoryEvent;
 if Event <> nil then
  Event.RedoData := PaletteFrame.ColorTable;
end;

procedure TMainForm.SizeToolActionExecute(Sender: TObject);
begin
 BrushPattern := Addr(Patterns[(Sender as TAction).Tag]);
end;

procedure TMainForm.MagicWandMouseEnter(Sender: TObject);
begin
 if DrawTool = dtMagicWand then
  UpdateMagicCursor(Sender as TCustomTileMapView, GlobalShiftState);
end;

procedure TMainForm.MagicWandMouseLeave(Sender: TObject);
begin
 if DrawTool = dtMagicWand then
  with Sender as TCustomTileMapView do
   DefaultCursor := crMagicWand;
end;

procedure TMainForm.MagicWandSysKeyDown(
  Sender: TCustomTileMapView; Key: Word; State: TShiftState);
begin
 if DrawTool = dtMagicWand then
  UpdateMagicCursor(Sender, State);
end;

procedure TMainForm.MagicWandSysKeyUp(
  Sender: TCustomTileMapView; Key: Word; State: TShiftState);
begin
 if DrawTool = dtMagicWand then
  Sender.DefaultCursor := crMagicWand;
end;

procedure TMainForm.PaletteFramePaletteViewMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
 PaletteFrame.PaletteViewMouseMove(Sender, Shift, X, Y);
 if DrawTool = dtMagicWand then
  UpdateMagicCursor(Sender as TCustomTileMapView, Shift);
end;

procedure TMainForm.ContinguousPaletteSelect(X, Y: Integer; Shift: TShiftState);
var
 SelBuf: PBoolean;
 c: LongWord;
 SelInc: Boolean;
 W, H: Integer;
 ct: TColorTable;

 procedure Beam(XInc, YInc, X, Y: Integer);
 var
  SP: PBoolean;
  Offset: Integer;
  Incr: Integer;
  PV: PInteger;
 begin
  if XInc <> 0 then
  begin
   Incr := XInc;
   PV := @X;
  end else
  begin
   Incr := YInc;
   PV := @Y;
  end;
  repeat
   Inc(PV^, Incr);
   if (X < 0) or
      (Y < 0) or
      (X >= W) or
      (Y >= H) then Break;

   Offset := Y * W + X;
   SP := SelBuf;
   Inc(SP, Offset);
   if (SP^ <> SelInc) and
      (ct.BGRA[Offset] = c) then
   begin
    SP^ := SelInc;
    if YInc >= 0 then
     Beam(0, -1, X, Y);
    if YInc <= 0 then
     Beam(0, +1, X, Y);
    if XInc >= 0 then
     Beam(-1, 0, X, Y);
    if XInc <= 0 then
     Beam(+1, 0, X, Y);
   end else
    Break;
  until False;
 end;

begin
 ct := PaletteFrame.ColorTable;
 if ct <> nil then
  with PaletteFrame.PaletteView do
  begin
   SelBuf := SelectionBuffer;
   W := MapWidth;

   H := Y * W + X;
   c := ct.BGRA[H];

   SelInc := Shift <> SelectExcludeShiftState;
   if Shift = SelectNewShiftState then
    FillChar(SelBuf^, W * MapHeight, 0);

   PBoolean(Integer(SelBuf) + H)^ := SelInc;

   H := MapHeight;

   Beam(0, -1, X, Y);
   Beam(0, +1, X, Y);
   Beam(-1, 0, X, Y);
   Beam(+1, 0, X, Y);

   UpdateSelection;
  end;
end;

procedure TMainForm.TilesFrameTileSetViewMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
var
 View: TCustomTileMapView;
begin
 View := Sender as TCustomTileMapView;
 (View.Owner as TTilesFrame).TileSetViewMouseMove(View, Shift, X, Y);
 if DrawTool = dtMagicWand then
  UpdateMagicCursor(View, Shift);
end;

procedure TMainForm.MapFrameMapEditViewMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
var
 View: TCustomTileMapView;
begin
 View := Sender as TCustomTileMapView;
 (View.Owner as TMapFrame).MapEditViewMouseMove(View, Shift, X, Y);
 if DrawTool = dtMagicWand then
  UpdateMagicCursor(View, Shift);
end;

procedure TMainForm.ToolDrawLineActionExecute(Sender: TObject);
begin
 with Sender as TAction do
  FDrawLine := Tag;
end;

procedure TMainForm.MapFrameMapEditViewCustomMouseActionFinish(
  Sender: TCustomTileMapView);
var
 Event: THistoryEvent;
 Frame: TMapFrame;
 MFrm: TMapsFrame;
begin
 if FBackup <> nil then
 begin
  if TObject(FBackup) is TMap then
   Event := THistoryEvent(TMap(FBackup).UserTag) else
   Event := THistoryEvent(FBackup);

  if Event <> nil then
  begin
   Frame := Sender.Owner as TMapFrame;
   MFrm := Frame.Owner as TMapsFrame;
   MFrm.SetMapEventData(Event, Frame.MapData, ruRedo);
  end;

  if FBackup <> Event then
   TObject(FBackup).Free;
  FBackup := nil;
 end;
end;

function TMainForm.DrawMapLine(Control: TCustomTileMapView;
  DrawUnit: TDrawUnitFunction;
  x1, y1, x2, y2: Integer): Boolean;

var
 dx, dx2, dy, dy2: Integer;

function BigY(x1, y1, x2, y2: Integer): Boolean;
var
 er, eps, t: Integer;
begin
 if x2 < x1 then
 begin
  t := x1;
  x1 := x2;
  x2 := t;
  t := y1;
  y1 := y2;
  y2 := t;
 end;
 if y1 < y2 then
  Eps := 1 else
  Eps := -1;
 Er := -dy;

 Result := False;

 while Abs(y2 - y1) >= 2 do
 begin
  Result := DrawUnit(Control, x1, y1) or Result;
  Result := DrawUnit(Control, x2, y2) or Result;
  Inc(er, dx2);
  if er >= 0 then
  begin
   Dec(Er, dy2);
   Inc(x1);
   Dec(x2);
  end;
  Inc(y1, Eps);
  Dec(y2, Eps);
  if y1 <> y2 then
  begin
   Result := DrawUnit(Control, x1, y1) or Result;
   Result := DrawUnit(Control, x2, y2) or Result;
  end else
   Result := DrawUnit(Control, x1, y1) or Result;
 end;
end;

function BigX(x1, y1, x2, y2: Integer): Boolean;
var
 er, eps, t: Integer;
begin
 if y2 < y1 then
 begin
  t := x1;
  x1 := x2;
  x2 := t;
  t := y1;
  y1 := y2;
  y2 := t;
 end;
 if x1 < x2 then
  Eps := 1 else
  Eps := -1;
 Er := -dx;

 Result := False;

 while Abs(x2 - x1) >= 2 do
 begin
  Result := DrawUnit(Control, x1, y1) or Result;
  Result := DrawUnit(Control, x2, y2) or Result;
  Inc(er, dy2);
  if er >= 0 then
  begin
   Dec(Er, dx2);
   Inc(y1);
   Dec(y2);
  end;
  Inc(x1, Eps);
  Dec(x2, Eps);
  if x1 <> x2 then
  begin
   Result := DrawUnit(Control, x1, y1) or Result;
   Result := DrawUnit(Control, x2, y2) or Result;
  end else
   Result := DrawUnit(Control, x1, y1) or Result;
 end;
end;

function Vline(x, y1, y2: Integer): Boolean;
var
 y: Integer;
begin
 if y1 > y2 then
 begin
  y := y1;
  y1 := y2;
  y2 := y;
 end;

 Result := False;
 
 for y := y1 to y2 do
  Result := DrawUnit(Control, x, y) or Result;
end;

function Hline(x1, x2, y: Integer): Boolean;
var
 x: Integer;
begin
 if x1 > x2 then
 begin
  x := x1;
  x1 := x2;
  x2 := x;
 end;

 Result := False;

 For x := x1 to x2 do
  Result := DrawUnit(Control, x, y) or Result;
end;

type
 LD = (ldUL, ldUR, ldDL, ldDR);

const LO: array[LD] of TPoint =
((x: -1; y: -1), (x: 1; y: -1), (x: -1; y: 1), (x: 1; y: 1));

var
 j: LD;
begin
 Result := False;
 for j := ldUL to ldDR do
  with LO[j] do
   if (x2 = x1 + x) and (y2 = y1 + y) then
   begin
    Result := DrawUnit(Control, x1, y1) or Result;
    Result := DrawUnit(Control, x2, y2) or Result;
    Exit;
   end;         
 if x1 = x2 then
  Result := VLine(x1, y1, y2) else
 if y1 = y2 then
  Result := HLine(x1, x2, y1) else
 begin
  dx := Abs(x2 - x1);
  dy := Abs(y2 - y1);
  dx2 := dx shl 1;
  dy2 := dy shl 1;
  if dx <= dy then
   Result := BigY(x1, y1, x2, y2) Else
   Result := BigX(x1, y1, x2, y2);
  end;
end;

function TMainForm.DrawWithBrush(Control: TCustomTileMapView; DrawX,
  DrawY: Integer): Boolean;
begin
 Result := DrawWithBrushEx(Control, BrushesFrame.MapFrame.MapData as TMapBrush, DrawX, DrawY);
end;

function TMainForm.UseBrushEraser(Control: TCustomTileMapView; DrawX,
  DrawY: Integer): Boolean;
begin
 Result := UseEraser(Control, True, DrawX, DrawY);
end;

function TMainForm.UseTileEraser(Control: TCustomTileMapView; DrawX,
  DrawY: Integer): Boolean;
begin
 Result := UseEraser(Control, False, DrawX, DrawY);
end;

function TMainForm.DrawMapRect(Control: TCustomTileMapView;
  DrawUnit: TDrawUnitFunction; x1, y1, x2, y2: Integer): Boolean;
begin
 Result := DrawMapLine(Control, DrawUnit, x1, y1, x2, y1);
 Result := DrawMapLine(Control, DrawUnit, x1, y1, x1, y2) or Result;
 Result := DrawMapLine(Control, DrawUnit, x2, y1, x2, y2) or Result;
 Result := DrawMapLine(Control, DrawUnit, x1, y2, x2, y2) or Result;
end;


procedure TMainForm.TileRemoved(Sender: TNodeList; Node: TNode);
begin
 if FLastTile = Node then
 begin
  FLastTile := nil;
  FProject.FInfo.SelectedTileIndex := -1;
  FProject.FInfo.SelectedTileTileSet := -1;  
 end;
 if MainFrame is TTileEditFrame then
  with TTileEditFrame(MainFrame) do
   if Tile = Node then
   begin
    TileEditView.Deselect(False);
    Tile := nil;
   end;
end;

procedure TMainForm.TilePropsUpdated(Sender: TObject);
begin
 if MainFrame is TTileEditFrame then
  with TTileEditFrame(MainFrame) do
   if Tile <> nil then
   begin
    if Sender = TilePropsFrm then
     PropsFrame.ShowTileProps(Tile.Owner as TTileSet, Tile.TileIndex) else
    if TilesFrm.TilesFrame.TileSetView.SelectedByIndex[Tile.TileIndex] then
     TilePropsFrm.ShowSelectedTileProps(TilesFrm.TilesFrame);
   end;

 Saved := False;
end;

procedure TMainForm.TilePropsFrameRefreshGraphicContent(Sender: TObject);
begin
 RefreshGraphicContent;
end;

procedure TMainForm.TileImageModified(Sender: TObject);
var
 Event: THistoryEvent;
begin
 RefreshGraphicContent;
 Saved := False;

 Event := FProject.History.LastNode as THistoryEvent;

 if Event <> nil then
  Event.RedoData := (Sender as TTileEditFrame).Tile;
end;

procedure TMainForm.TileScaleChange(Sender: TObject; var Value: Integer);
begin
 FProject.FInfo.Scales.scTileFrame := Value;
end;

procedure TMainForm.TileSetsStructureChanged(Sender: TObject);
var
 I: Integer;
begin
 with Sender as TTileSetList do
 begin
  for I := 0 to Count - 1 do
   with Nodes[I] as TTileSet do
    OnRemove := TileRemoved;
  if FLastTile <> nil then
  begin
   FProject.FInfo.SelectedTileIndex := FLastTile.TileIndex;
   FProject.FInfo.SelectedTileTileSet := FLastTile.Owner.Index;
  end;
 end;
end;

procedure TMainForm.TileDrawColorChange(Sender: TTileEditFrame; Mode: Boolean);
begin
 FProject.FInfo.DrawColors[Mode] := Sender.ColorIndex[Mode];
end;

procedure TMainForm.TileEditViewCustomMouseActionFinish(
  Sender: TCustomTileMapView);
begin
 if DrawTool in [dtPencil, dtBrush, dtEraser, dtFloodFill] then
  Sender.ContentsChanged;
 FreeMem(FBackup);
 FBackup := nil;
end;

procedure TMainForm.TileEditViewCustomMouseActionCheck(
  Sender: TCustomTileMapView; Shift: TShiftState; var Act: Boolean);
var
 Frame: TTileEditFrame;
 Event: THistoryEvent;
begin
 Frame := Sender.Owner as TTileEditFrame;
 if (Shift = [ssLeft, ssDouble]) and
    (DrawTool in [dtSingleSelect, dtMultiSelect]) then
 begin
  SelectInTileSetActionExecute(nil);
  Act := False;
 end else
 if (DrawTool = dtMagicWand) and
    (ssLeft in Shift) then
 begin
  Sender.PasteBufferApply;
  Frame.MagicWand(Sender.TileX, Sender.TileY, Shift);
  Act := False;
 end else
 if (DrawTool = dtFloodFill) and
    ((ssLeft in Shift) or
    (ssRight in Shift)) then
 begin
  Sender.PasteBufferApply;
  with Frame do
  begin
   Event := FProject.History.AddEvent(Tile,
     WideFormat('Tile set ''%s'', tile #%d: Flood fill',
      [TTileSet(Tile.Owner).Name, Tile.TileIndex]));

   Frame.FloodFill(Sender.TileX, Sender.TileY, ssRight in Shift);

   if Event.RedoData = nil then
    with FProject.History do
     Count := Count - 1;

  end;
  Act := False;
 end else
 begin
  Act := (Frame.Tile <> nil) and
       not (DrawTool in [dtSingleSelect, dtMultiSelect]) and
       ((ssLeft in Shift) or (ssRight in Shift));
  if Act then
  begin
   Sender.NewCursorPosition := True;
   case DrawTool of
    dtPencil,
    dtEraser,
    dtBrush:
    begin
     Sender.PasteBufferApply;
     with Frame do
      FProject.History.AddEvent(Tile,
       WideFormat('Tile set ''%s'', tile #%d: %s',
             [TTileSet(Tile.Owner).Name, Tile.TileIndex, DrawToolNames[DrawTool]]));
     if FDrawLine > 0 then
     begin
      with FClickPos do
      begin
       X := Sender.TileX;
       Y := Sender.TileY;
      end;

      FBackup := Frame.TileImageBackup;
     end;
    end;
   end;
  end;
 end;
end;

procedure TMainForm.TileEditViewCustomMouseAction(
  Sender: TCustomTileMapView; var Done: Boolean);
var
 DrawUnit: TDrawUnitFunction;
begin
 if Sender.NewCursorPosition then
  with Sender.Owner as TTileEditFrame do
   if Tile <> nil then
   begin
    case DrawTool of
     dtPencil,
     dtEraser,
     dtBrush:
     begin
      case DrawTool of
       dtPencil,
       dtBrush:
        DrawUnit := DrawTileWithPencil;
       dtEraser:
        DrawUnit := DrawTileUseEraser;
       else
        Exit; 
      end;

      if FDrawLine > 0 then
      begin
       Move(FBackup^, TileImageData^, TileImageSize);

       with FClickPos do
        case FDrawLine of
         1:
          DrawMapLine(Sender, DrawUnit, X, Y, Sender.TileX, Sender.TileY);
         else
          DrawMapRect(Sender, DrawUnit, X, Y, Sender.TileX, Sender.TileY);
        end;
      end else
       DrawUnit(Sender, Sender.TileX, Sender.TileY);
     end;
     dtPicker:
     begin
      PickColor(Sender.TileX, Sender.TileY, ssRight in GlobalShiftState);
      Exit;
     end;
    end; 
    Sender.Invalidate;
   end;
end;

procedure TMainForm.TileEditViewMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
var
 View: TCustomTileMapView;
begin
 View := Sender as TCustomTileMapView;
 (View.Owner as TTileEditFrame).TileEditViewMouseMove(View, Shift, X, Y);
 if DrawTool = dtMagicWand then
  UpdateMagicCursor(View, Shift);
end;

function TMainForm.DrawTileUseEraser(Control: TCustomTileMapView; DrawX,
  DrawY: Integer): Boolean;
var
 PB: PByte;
 XX, YY, CX, CY: Integer;
 Sel: Boolean;
begin
 Result := False;
 with Control.Owner as TTileEditFrame do
  if Tile <> nil then
  begin
   Sel := Control.SomethingSelected;
   with BrushPattern^ do
   begin
    XX := DrawX - X;
    YY := DrawY - Y;
    PB := P;
    for CY := YY to YY + (H - 1) do
     for CX := XX to XX + (W - 1) do
     begin
      if (PB^ <> 0) and (not Sel or Control.Selected[CX, CY]) then
      begin
       ErasePixel(CX, CY);
       Result := True;
      end;
      Inc(PB);
     end;
   end;
  end;
end;

function TMainForm.DrawTileWithPencil(Control: TCustomTileMapView; DrawX,
  DrawY: Integer): Boolean;
var
 RightButton: Boolean;
 PB: PByte;
 XX, YY, CX, CY: Integer;
 Sel: Boolean;
begin
 Result := False;
 with Control.Owner as TTileEditFrame do
  if Tile <> nil then
  begin
   RightButton := ssRight in GlobalShiftState;
   Sel := Control.SomethingSelected;
   with BrushPattern^ do
   begin
    XX := DrawX - X;
    YY := DrawY - Y;
    PB := P;
    for CY := YY to YY + (H - 1) do
     for CX := XX to XX + (W - 1) do
     begin
      if (PB^ <> 0) and (not Sel or Control.Selected[CX, CY]) then
      begin
       SetPixel(CX, CY, RightButton);
       Result := True;
      end;
      Inc(PB);
     end;
   end;
  end;
end;

procedure TMainForm.TilesFrameTileSetViewContextPopup(Sender: TObject;
  MousePos: TPoint; var Handled: Boolean);
begin
 with TWinControl(Sender) do
 begin
  TilesPopup.Tag := 1;
  TilesPopup.Items[0].Tag := Integer(Owner);
  with ClientToScreen(MousePos) do
   TilesPopup.Popup(X, Y);
 end;
 Handled := True;
end;

procedure TMainForm.TilesTileEditActionExecute(Sender: TObject);
begin
 if FProgressBar = nil then
 begin
  if not (Sender is TTilesFrame) then
   Sender := TObject(TilesPopup.Items[0].Tag);
  with TTilesFrame(Sender) do
   PostMessage(Self.Handle, WM_TILEEDIT, Integer(TileSet), IndexUnderCursor);
 end;
 TilesPopup.Tag := 0; 
end;

procedure TMainForm.TilesTileEditActionUpdate(Sender: TObject);
var
 Enable: Boolean;
begin
 Enable := TilesPopup.Tag <> 0;
 if Enable then
 begin
  if not (Sender is TTilesFrame) then
   Sender := TObject(TilesPopup.Items[0].Tag);
  with TTilesFrame(Sender) do
   Enable := (FProgressBar = nil) and
                                 (TileSet <> nil) and
                                 (TileSet.Indexed[SelectedTile] <>
                                  TileSet.EmptyTile);
 end;
 TilesTileEditAction.Enabled := Enable;
end;

procedure TMainForm.WMTileEdit(var Message: TMessage);
var
 TileSet: TTileSet;
begin
 SetEditMode(emTile);
 TileSet := TObject(Message.wParam) as TTileSet;
 with TTileEditFrame(MainFrame) do
 begin
  ColorTable := PaletteFrame.ColorTable;
  if TileSet <> nil then
  begin
   FLastTile := TileSet.Indexed[Message.lparam];
   FProject.FInfo.SelectedTileTileSet := TileSet.Index;
  end else
   FProject.FInfo.SelectedTileTileSet := -1;
  Tile := FLastTile;
  if FLastTile <> nil then
   FProject.FInfo.SelectedTileIndex := FLastTile.TileIndex else
   FProject.FInfo.SelectedTileIndex := -1;
  TileEditView.SetFocus;
 end;
end;

procedure TMainForm.ColorTablesListBoxKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
 if ssCtrl in Shift then
 case Key of
  VK_UP:
  begin
   ColorTableUpActionExecute(ColorTableUpAction);
   Key := 0;
  end;
  VK_DOWN:
  begin
   ColorTableDownActionExecute(ColorTableDownAction);
   Key := 0;
  end;
 end;
end;

procedure TMainForm.PluginActionExecute(Sender: TObject);
var
 Plg: TPlugin;
 Thread: TLoadSaveThread;
 Mode: TPluginDialogMode;
 MapList: TMapList;
begin
 StartDialog;
 try
  Mode := TPluginDialogMode((Sender as TAction).Tag);
  if PluginDialogExecute(FProject, Mode,
                         FPluginsPath,
                         FPluginsIniPath, FSelectedPlugin) then
  begin
   Plg := TPlugin.Create;
   try
    if Plg.LoadPlugin(FPluginsPath + FSelectedPlugin) then
    begin
     Application.CreateForm(TProcessDialog, ProcessDialog);
     try
      if Mode = pmLoad then
       ProcessDialog.Caption := WideFormat('Loading with %s', [Plg.FileName]) else
       ProcessDialog.Caption := WideFormat('Saving with %s', [Plg.FileName]);

      MapList := TMapList.Create;
      MapList.Assign(FProject.FMaps);
      Thread := TLoadSaveThread.Create(Mode, Self, Plg, MapList);
      try
       Thread.Resume;
       ProcessDialog.ShowModal;
       Thread.Terminate;
       Thread.WaitFor;
      finally
       Thread.Free;
      end;
     finally
      FreeAndNil(ProcessDialog);
     end;
    end;
   finally
    Plg.Free;
   end;
  end;
 finally
  EndDialog;
 end;
end;

procedure TMainForm.FrameEndDialog(Sender: TObject);
begin
 EndDialog;
end;

procedure TMainForm.FrameStartDialog(Sender: TObject);
begin
 StartDialog;
end;

procedure TMainForm.TilesFrameTilesModified(Sender: TObject);
begin
 TilesFrameTileSetViewContentsChanged((Sender as TTileSetsFrame).TilesFrame.TileSetView);
end;

procedure TMainForm.HistoryAdded(Sender: TNodeList; Node: TNode);
begin
 with FProject.History do
 begin
  HistoryListBox.Count := Count;
  HistoryListBox.ItemIndex := Position;
 end;
 HistoryListBox.Invalidate;
end;

procedure TMainForm.HistoryRemoveEvent(Sender: TNodeList; Node: TNode);
var
 I: Integer;
begin
 with FProject.History do
 begin
  HistoryListBox.Count := Count - 1;
  I := Position;
  if Node.Index = SavedPosition then
   SavedPosition := -1;
 end;
 with HistoryListBox do
 begin
  if I >= Count then
   I := Count - 1;
  ItemIndex := I;
  Invalidate;
 end;
end;

procedure TMainForm.HistoryEventApply(Sender: TObject);
var
 OldFocus: Boolean;
 CtblIdx: Integer;
begin
 OldFocus := HistoryListBox.Focused;
 CtblIdx := ColorTableIndex;
 RefreshLists(FProject.FInfo.EditMode, FProject.FInfo.PageSetup);
 PaletteFrame.ColorTable := nil;  
 SelectColorTable(CtblIdx);
 if OldFocus then
  HistoryListBox.SetFocus;
end;

procedure TMainForm.HistoryGetDestOwner(Sender: THistoryEvent;
  const OwnerIndexList: TIndexArray; Data: TNode; var Result: TNodeList);
begin
 if Data is TTileItem then
  Result := FProject.FMaps.TileSetList[OwnerIndexList[0]] else
 if Data is TTileSet then
  Result := FProject.FMaps.TileSetList else
 if Data is TColorTable then
  Result := FProject.FMaps.ColorTableList else
{ if Data is TBrushLayer then
  Result := FProject.FMaps.BrushList[OwnerIndexList[0]] else
 if Data is TMapBrush then
  Result := FProject.FMaps.BrushList;
 if Data is TMapLayer then
  Result := FProject.FMaps[OwnerIndexList[0]] else
 if Data is TMap then
  Result := FProject.FMaps else  }
  Result := Data.Owner as TNodeList;
end;

procedure TMainForm.TilesFrameAfterUnhook(Sender: TObject);
begin
 MapsFrm.SetListEventData(TObject(MapsFrm.sep11.Tag) as THistoryEvent, ruRedo);
 BrushesFrame.SetListEventData(TObject(BrushesFrame.sep11.Tag) as THistoryEvent, ruRedo);
end;

procedure TMainForm.TilesFrameBeforeUnhook(Sender: TObject);
var
 Str: WideString;
begin
 with Sender as TTileSetsFrame do
 begin
  if TileSetsListBox.SelCount = 1 then
   Str := WideFormat('Unhook tile set ''%s''', [
          FProject.FMaps.TileSetList[TileSetIndex].Name]) else
   Str := 'Unhook selected tile sets';

  MapsFrm.sep11.Tag := Integer(MapsFrm.AddListEvent(WideFormat('Maps: %s', [Str])));

  BrushesFrame.sep11.Tag := Integer(BrushesFrame.AddListEvent(WideFormat('Brushes: %s', [Str])));
 end;
end;

procedure TMainForm.ColorTableAfterUnhook;
begin
 MapsFrm.SetListEventData(TObject(MapsFrm.sep11.Tag) as THistoryEvent, ruRedo);
 BrushesFrame.SetListEventData(TObject(BrushesFrame.sep11.Tag) as THistoryEvent, ruRedo);
end;

procedure TMainForm.ColorTableBeforeUnhook;
var
 Str: WideString;
begin
 if ColorTablesListBox.SelCount = 1 then
  Str := WideFormat('Unhook color table ''%s''', [
         FProject.FMaps.ColorTableList[ColorTableIndex].Name]) else
  Str := 'Unhook selected color tables';

 MapsFrm.sep11.Tag := Integer(MapsFrm.AddListEvent(WideFormat('Maps: %s', [Str])));

 BrushesFrame.sep11.Tag := Integer(BrushesFrame.AddListEvent(WideFormat('Brushes: %s', [Str])));
end;

function TMainForm.AddColorTableListEvent(
  const Text: WideString): THistoryEvent;
begin
 Result := FProject.History.AddEvent(FProject.FMaps.ColorTableList, Text);
 Result.OnApply := FProject.ColorTableListApply;
end;

{ TLoadSaveThread }

constructor TLoadSaveThread.Create(Mode: TPluginDialogMode; MainForm: TMainForm;
  Plugin: TPlugin; MapList: TMapList);
begin
 inherited Create(True);
 Priority := tpNormal;
 FThreadMode := Mode;
 FMainForm := MainForm;
 FMapList := MapList;
 FPlugin := Plugin;
 if (MainForm <> nil) and (Plugin <> nil) then
  FIniFile := TStreamIniFileW.Create(MainForm.PluginsIniPath +
       WideChangeFileExt(Plugin.FileName, '.ini'));
end;

destructor TLoadSaveThread.Destroy;
begin
 inherited;
 if (FMainForm <> nil) and
    (FMapList <> FMainForm.FProject.FMaps) then
  FMapList.Free;
 FIniFile.Free;
end;

procedure TLoadSaveThread.Execute;
// Old1, Old2, Old3, Old4: LongWord;
begin
 with FMainForm do
 begin
  while not (Screen.ActiveForm is TProcessDialog) do;

  try
   FConfigIndex := FIniFile.ReadInteger(SActiveSection, LSStr[FThreadMode], 1);
   FConfigSection := WideFormat('%s%d', [LSStr[FThreadMode], FConfigIndex]);
   FData.Section := Pointer(FConfigSection);
   FData.ShowMessage := @PlugShowMessage;
   FData.SetProgressMax := @PrcDlg_SetProgressMax;
   FData.SetProgress := @PrcDlg_SetProgress;
   FData.SetStatusText := @PrcDlg_SetStatusText;
   FData.IsAborted := @PrcDlg_IsAborted;
   FData.Utils := TUtilsAdapter.Create;
   FData.IniFile := TIniFileAdapter.Create(FIniFile);
   FData.MapList := TMapListAdapter.Create(FMapList);
   FData.BrushList := TBrushListAdapter.Create(FMapList.BrushList);
   FData.ColorTableList := TColorTableListAdapter.Create(FMapList.ColorTableList);
   FData.TileSetList := TTileSetListAdapter.Create(FMapList.TileSetList);
   FData.ProjectInfo := Addr(FProject.FInfo);
   FData.TileSetInfo := Pointer(FProject.FTileSetInfo);
   FData.MapLayerFocus := Pointer(FProject.FMapLayerFocus);
   FData.BrushLayerFocus := Pointer(FProject.FBrushLayerFocus);
   {VirtualProtect(FData.ProjectInfo, SizeOf(TProjectInfo), PAGE_READONLY, Old1);
   VirtualProtect(FData.TileSetInfo, Length(FProject.FTileSetInfo) *
                       SizeOf(TTileSetInfo), PAGE_READONLY, Old2);
   VirtualProtect(FData.MapLayerFocus, Length(FProject.FMapLayerFocus),
                                PAGE_READONLY, Old3);
   VirtualProtect(FData.BrushLayerFocus, Length(FProject.FBrushLayerFocus),
                          PAGE_READONLY, Old4); }
   try
    case FThreadMode of
     pmLoad:
     begin
      case FPlugin.Load(@FData) of
       0:
       begin
        ProcessDialog.StatusText.Caption := 'Loaded successfully!';
        Synchronize(LoadedSuccessfully);
       end;
       -1: ProcessDialog.StatusText.Caption := 'Not loaded!';
      end;
     end;
     pmSave:
     case FPlugin.Save(@FData) of
      0: ProcessDialog.StatusText.Caption := 'Saved successfully!';
     -1: ProcessDialog.StatusText.Caption := 'Not saved!';
     end;
    end;
   except
    on E: Exception do
      ProcessDialog.StatusText.Caption := E.Message;
   end;
  finally
   Finalize(FData);
  end;
  ProcessDialog.Finished := True;  
 end;
end;

procedure TLoadSaveThread.LoadedSuccessfully;
var
 Event: THistoryEvent;
 Node: TStreamNode;
begin
 with FMainForm do
 begin
  EndDialog;
  Node := TStreamNode.Create;
  try
   FProject.FMaps.SaveToStream(Node.Stream);
   Event := FProject.History.AddEvent(Node, 'Load with plugin');
   Event.OnApply := FProject.ProjectApply;
   FProject.FMaps.Free;
   FProject.FMaps := FMapList;
   Saved := False;
   FMapList := nil;
   FProject.Update;
   if (FData.ProjectInfo <> nil) and
      (FData.ProjectInfo <> Addr(FProject.FInfo)) then
    Move(FData.ProjectInfo^, FProject.FInfo, SizeOf(TProjectInfo));

   if (FData.TileSetInfo <> nil) and
      (FData.TileSetInfo <> Pointer(FProject.FTileSetInfo)) then
    Move(FData.TileSetInfo^, FProject.FTileSetInfo[0],
         Length(FProject.FTileSetInfo) * SizeOf(TTileSetInfo));

   if (FData.MapLayerFocus <> nil) and
      (FData.MapLayerFocus <> Pointer(FProject.FMapLayerFocus)) then
    Move(FData.MapLayerFocus^, FProject.FMapLayerFocus[0],
         Length(FProject.FMapLayerFocus));

   if (FData.BrushLayerFocus <> nil) and
      (FData.BrushLayerFocus <> Pointer(FProject.FBrushLayerFocus)) then
    Move(FData.BrushLayerFocus^, FProject.FBrushLayerFocus[0],
         Length(FProject.FBrushLayerFocus));

   FLastTile := nil;
   FLastFocus := nil;


   Node.Stream.Clear;

   FProject.FMaps.SaveToStream(Node.Stream);

   Event.RedoData := Node;

   RefreshLists(emMap);
  finally
   Node.Free;
  end;
 end;
end;

procedure TMainForm.SelectInTileSetActionUpdate(Sender: TObject);
begin
 SelectInTileSetAction.Enabled := (FProgressBar = nil) and
                               (MainFrame is TTileEditFrame) and
                               (TTileEditFrame(MainFrame).Tile <> nil);
end;

procedure TMainForm.SelectInTileSetActionExecute(Sender: TObject);
begin
 if (FProgressBar = nil) and (MainFrame is TTileEditFrame) then
  with TTileEditFrame(MainFrame) do
   if Tile <> nil then
    with TilesFrm do
    begin
     PageControl.ActivePageIndex := 1;
     SelectTileSet(Tile.Owner.Index);

     if Tile.TileIndex >= 0 then
     begin
      Application.ProcessMessages;
      TilesFrame.SelectedTile := Tile.TileIndex;
     end;

     TilesFrame.TileSetView.SetFocus;
    end;
end;

procedure TMainForm.SelectFillActionUpdate(Sender: TObject);
var
 Enable: Boolean;
begin
 Enable := False;
 if FProgressBar = nil then
 begin
  if FFocusedMapFrame <> nil then
   Enable := FFocusedMapFrame.MapEditView.MultiSelected else
  if (MainFrame is TTileEditFrame) and
     TTileEditFrame(MainFrame).TileEditView.Focused then
   Enable := TTileEditFrame(MainFrame).TileEditView.SomethingSelected;
 end;
 SelectFillAction.Enabled := Enable;
end;

procedure MapFillSelected(Frame: TMapFrame);
var
 SelBuf: PBoolean;
 Map: TMap;
 Offset, I, J: Integer;
begin
 Map := Frame.MapData;
 if Map <> nil then
  with Frame.MapEditView do
   if MultiSelected then
   begin
    Offset := 0;

    SelBuf := SelectionBuffer;
    
    for I := 0 to MapWidth * MapHeight - 1 do
    begin
     if SelBuf^ then
      for J := 0 to Map.Count - 1 do
      begin
       with Map.Nodes[J] as TMapLayer do
       if (Flags and LF_LAYER_ACTIVE <> 0) and
          (Flags and LF_VISIBLE <> 0) then
        WriteCellInfo(PByteArray(LayerData)[Offset * CellSize], DrawInfo);
      end;

     Inc(SelBuf);
     Inc(Offset);
    end;

    Invalidate;
   end;
end;

procedure TMainForm.SelectFillActionExecute(Sender: TObject);
begin
 if (MainFrame is TTileEditFrame) and
    TTileEditFrame(MainFrame).TileEditView.Focused then
 begin
  with TTileEditFrame(MainFrame) do
   if TileEditView.SomethingSelected then
   begin
    TileEditView.PasteBufferApply;
    FillSelected;
   end;
 end else
 if FFocusedMapFrame <> nil then
 begin
  with FFocusedMapFrame do
   if MapEditView.MultiSelected then
   begin
    MapFillSelected(FFocusedMapFrame);
    Saved := False;
    RefreshMapContent(FFocusedMapFrame.MapData, [rmcView]);
   end;
 end;
end;

procedure TMainForm.HistoryListBoxDrawItem(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
var
 Flags: Integer;
 Event: THistoryEvent;
begin
 if Index >= 0 then
  with Control as TListBox do
  begin
   Canvas.FillRect(Rect);

   Flags := DrawTextBiDiModeFlags(DT_SINGLELINE or DT_VCENTER or DT_NOPREFIX);
   if not UseRightToLeftAlignment then
     Inc(Rect.Left, 2) else
     Dec(Rect.Right, 2);

   Event := FProject.History.Events[Index];
   if Event <> nil then
   begin
    with Canvas do
    begin
     if Index = FProject.History.Position then
      Font.Style := [fsBold] else
      Font.Style := [];
     if Index > FProject.History.Position then
      Font.Color := clSilver else
      Font.Color := clWindowText;      
    end;

    Windows.DrawTextW(Canvas.Handle,
                      Pointer(Event.Text), Length(Event.Text),
                      Rect, Flags);

  {  if Index > FProject.History.Position then
     with Canvas do
     begin
      Brush.Handle := FDottedBrush;
      FillRect(Rect);
      //Windows.FillRect(Handle, Rect, FDottedBrush)
     end;    }
   end;
  end;
end;

procedure TMainForm.HistoryListBoxClick(Sender: TObject);
var
 SPos: Integer;
begin
 with FProject.History do
 begin
  SPos := SavedPosition;
  Position := HistoryListBox.ItemIndex; 
  FSaved := Position = SPos;
  SavedPosition := SPos;
 end;

 ProgramTitleUpdate; 
  
 HistoryListBox.Invalidate;
end;

procedure TMainForm.TilesFrameTileSetViewBeforeContentChange(
  Sender: TCustomTileMapView; ChangeType: TChangeType);
var
 Frm: TTilesFrame;
begin
 Frm := Sender.Owner as TTilesFrame;
 FProject.History.AddEvent(Frm.TileSet,
                   WideFormat('Tile set ''%s'': %s', [Frm.TileSet.Name,
                   ChangeTypeStr[ChangeType]]));
end;

procedure TMainForm.TileEditViewBeforeContentChange(
  Sender: TCustomTileMapView; ChangeType: TChangeType);
var
 Frm: TTileEditFrame;
begin
 Frm := Sender.Owner as TTileEditFrame;
 with Frm.Tile do
  FProject.History.AddEvent(Frm.Tile,
                    WideFormat('Tile set ''%s'', tile #%d: %s',
                               [TTileSet(Owner).Name,
                                TileIndex,
                    ChangeTypeStr[ChangeType]]));
end;

procedure TMainForm.EditUndoActionUpdate(Sender: TObject);
begin
 EditUndoAction.Enabled := (FProgressBar = nil) and
                           (FProject.History.Position > 0);
end;

procedure TMainForm.EditRedoActionUpdate(Sender: TObject);
begin
 EditRedoAction.Enabled := (FProgressBar = nil) and
                           (FProject.History.Position < FProject.History.Count - 1);
end;

procedure TMainForm.EditUndoRedoActionExecute(Sender: TObject);
begin
 HistoryListBox.ItemIndex := FProject.History.Position + TAction(Sender).Tag;
 HistoryListBoxClick(Sender);
end;

procedure TMainForm.PaletteFramePaletteViewBeforeContentChange(
  Sender: TCustomTileMapView; ChangeType: TChangeType);
var
 Frm: TPaletteFrame;
begin
 Frm := Sender.Owner as TPaletteFrame;
 FProject.History.AddEvent(Frm.ColorTable,
                   WideFormat('Color table ''%s'': %s', [Frm.ColorTable.Name,
                   ChangeTypeStr[ChangeType]]));
end;


initialization
 CF_COLORTABLES := RegisterClipboardFormat('Kusharami Color Tables Container');
 CF_TRANSP_MASK := RegisterClipboardFormat('Kusharami Graphic Transparency Mask');
end.


