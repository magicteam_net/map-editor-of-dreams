unit me_plugin;

interface

uses
 Windows;

const
//BitmapEx const
 CONVERTER_CODE_SIZE = 256;

 CF_RGB = 'RGB888';
 CF_BGRZ = 'BGRZ8888';
 CF_BGRA = 'BGRA8888';
 CF_RGBA = 'RGBA8888';
 CF_RGBZ = 'RGBZ8888';
 CF_ARGB = 'ARGB8888';
 CF_MCGA = 'RZGZB62626';
 CF_GRAY = 'GRAY8';
 CFLG_NORMAL     = 0;
 CFLG_RGBQUAD    = 1;
 CFLG_GRAYSCALE  = 2;
 CFLG_ALPHA      = 1 shl 2;
 CFLG_BYTESWAP_SHIFT = 3;
 CFLG_BYTESWAP   = 1 shl CFLG_BYTESWAP_SHIFT;
 CFLG_8888       = 1 shl 4;

 // First 5 bits is an Image Bits Count minus 1
 IMG_BITS_REVERSE   = 1 shl 6; // if set then read bits from right to left
 IMG_PIXEL_BSWAP    = 1 shl 6; // if set then read pixel bytes from right to left
 IMG_PLANAR         = 1 shl 7; // if set then image have more than one plane
 IMG_PLANE_REVERSE  = 1 shl 8; // if set then read planes from last to first
 IMG_PLANE_VECTOR   = 1 shl 9; // if set then the image line made from planes
 IMG_FLIPS_SHIFT    = 10;
 IMG_BYTES_REVERSE  = 1 shl (IMG_FLIPS_SHIFT + 0); // if set then line bytes read from right to left
 IMG_Y_FLIP         = 1 shl (IMG_FLIPS_SHIFT + 1); // if set then lines read from last to first
 IMG_X_FLIP         = IMG_BYTES_REVERSE;
 IMG_X_FLIP4        = IMG_BYTES_REVERSE or IMG_BITS_REVERSE;
 IMG_BITS_SHIFT     = 1 shl 12; // sets when DRAW_X_FLIP
// IMG_ANGLE45        = 1 shl 13;
 IMG_COMPRESSED     = 1 shl 13; // may be compressed in linear mode only
                               // only transparent pixels are compressed
 PLANE_FORMAT_SHIFT = 14; //2 bits
 IMG_PLANE_1BIT     = 0 shl PLANE_FORMAT_SHIFT;
 IMG_PLANE_2BIT     = 1 shl PLANE_FORMAT_SHIFT;
 IMG_PLANE_4BIT     = 2 shl PLANE_FORMAT_SHIFT;
 IMG_PLANE_8BIT     = 3 shl PLANE_FORMAT_SHIFT;
 IMG_FORMAT_MASK = (1 shl PLANE_FORMAT_SHIFT) - 1;
 //Internal draw flags
 DRAW_BITS          = 1;
 DRAW_PIXEL_MODIFY_SHIFT = 1;
 DRAW_PIXEL_MODIFY  = 1 shl DRAW_PIXEL_MODIFY_SHIFT;
 DRAW_VALUE_CONVERT = 1 shl 2;
 DRAW_SRC_COMPOSITE = 1 shl 3;
 DRAW_DST_COMPOSITE = 1 shl 4;
 DRAW_SRC_PROC      = 1 shl 5;
 DRAW_DST_PROC      = 1 shl 6;
 DRAW_COMPRESSED    = 1 shl 7;
 //User draw flags
 DRAW_FLIPS_SHIFT   = 8;
 DRAW_X_FLIP        = 1 shl (DRAW_FLIPS_SHIFT + 0);
 DRAW_Y_FLIP        = 1 shl (DRAW_FLIPS_SHIFT + 1);
 DRAW_XY_FLIP       = DRAW_X_FLIP or DRAW_Y_FLIP;
 DRAW_TRANSP_SHIFT  = 10;
 DRAW_TRANSPARENT   = 1 shl DRAW_TRANSP_SHIFT; // transparent if equal
 DRAW_TRANSP_AE     = 2 shl DRAW_TRANSP_SHIFT; // transparent if above or equal
 DRAW_TRANSP_BE     = 3 shl DRAW_TRANSP_SHIFT; // transparent if below or equal
 DRAW_TRANSP_MASK   = 3 shl DRAW_TRANSP_SHIFT;
 DRAW_ALPHA_BLEND   = 1 shl 12;
 DRAW_NO_CONVERT    = 1 shl 13; // Move values from source to dest without any conversion
 //Converter flags
 SRC_IMGFMT_SHIFT = 16;
 SRC_INDEXED      = 0 shl SRC_IMGFMT_SHIFT;
 SRC_GRAYSCALE    = 1 shl SRC_IMGFMT_SHIFT;
 SRC_RGB          = 2 shl SRC_IMGFMT_SHIFT;
 DST_IMGFMT_SHIFT = 16 + 2;
 DST_INDEXED      = 0 shl DST_IMGFMT_SHIFT;
 DST_GRAYSCALE    = 1 shl DST_IMGFMT_SHIFT;
///////
 LD_LEFT      = 1;
 LD_RIGHT     = 1 shl 1;
 LD_UP        = 1 shl 2;
 LD_DOWN      = 1 shl 3;
 LD_LEFTUP    = 1 shl 4;
 LD_RIGHTUP   = 1 shl 5;
 LD_LEFTDOWN  = 1 shl 6;
 LD_RIGHTDOWN = 1 shl 7;

 // Tile set constants
 OPT_FIND_MIRRORING = 1;
 OPT_CHECK_COLORS = 1 shl 1;
 OPT_CHECK_EMPTY = 1 shl 2;

 //PropContainer constants
 //File open options
 FO_ReadOnly            = 1;
 FO_OverwritePrompt     = 1 shl 1;
 FO_HideReadOnly        = 1 shl 2;
 FO_NoChangeDir         = 1 shl 3;
 FO_ShowHelp            = 1 shl 4;
 FO_NoValidate          = 1 shl 5;
 FO_AllowMultiSelect    = 1 shl 6;
 FO_ExtensionDifferent  = 1 shl 7;
 FO_PathMustExist       = 1 shl 8;
 FO_FileMustExist       = 1 shl 9;
 FO_CreatePrompt        = 1 shl 10;
 FO_ShareAware          = 1 shl 11;
 FO_NoReadOnlyReturn    = 1 shl 12;
 FO_NoTestFileCreate    = 1 shl 13;
 FO_NoNetworkButton     = 1 shl 14;
 FO_NoLongNames         = 1 shl 15;
 FO_OldStyleDialog      = 1 shl 16;
 FO_NoDereferenceLinks  = 1 shl 17;
 FO_EnableIncludeNotify = 1 shl 18;
 FO_EnableSizing        = 1 shl 19;
 FO_DontAddToRecent     = 1 shl 20;
 FO_ForceShowHidden     = 1 shl 21;

 CLoadOptions = FO_FileMustExist or FO_HideReadOnly or FO_EnableSizing; 

 //Stream constants
 fmCreate = $FFFF;
 SO_Beginning = 0;
 SO_Current = 1;
 SO_End = 2;
 ZCL_NONE = 0;
 ZCL_FASTEST = 1;
 ZCL_DEFAULT = 2;
 ZCL_MAX = 3;

 //PropContaner
 PL_FIXEDPICK = 1;
 PL_BUFFER = 1;
 vtDecimal       = 0;
 vtHexadecimal   = 1;
 vtString        = 2;
 vtPickString    = 3;
 vtFloat         = 4;
 vtFilePickLoad  = 5;
 vtFilePickSave  = 6;
 vtDirectoryPick = 7;

type
 IRoot = interface
 ['{3BD3A27E-1973-41D0-9620-01D58310C915}']
    function GetPtr: Pointer; stdcall;
    procedure FreeObject; stdcall;
 end;

 IReadWrite = interface(IRoot)
 ['{A1FE8B5E-6B1C-41B7-9043-13EDE4AAE21C}']
 //public
    function Read(var Buffer; Count: LongInt): LongInt; stdcall;
    function Write(const Buffer; Count: LongInt): LongInt; stdcall;
 end;

 INode = interface(IRoot)
 ['{63564047-DEAB-4C00-8C91-08AD05139D21}']
 //private
    function GetUserTag: LongInt; stdcall;
    procedure SetUserTag(Value: LongInt); stdcall;

    function GetOwner: INode; stdcall;
    function GetNext: INode; stdcall;
    function GetPrev: INode; stdcall;

    function GetIndex: LongInt; stdcall;
 //public
    procedure Assign(const Source: INode); stdcall;
    procedure Changed; stdcall;
 //properties
    property Owner: INode read GetOwner;
    property Next: INode read GetNext;
    property Prev: INode read GetPrev;
    property Index: LongInt read GetIndex;
    
    property UserTag: LongInt read GetUserTag write SetUserTag;
 end;

 INodeList = interface(INode)
 ['{2FB4CC3C-1614-4B09-9160-803E332CA460}']
 //private
    function GetCount: LongInt; stdcall;
    procedure SetCount(Value: LongInt); stdcall;
    function GetMaxCount: LongInt; stdcall;
    procedure SetMaxCount(Value: LongInt); stdcall;

    function GetRootNode: INode; stdcall;
    function GetLastNode: INode; stdcall;
 //public
    procedure Clear; stdcall;
    function IsChild(const Node: INode): Boolean; stdcall;
    function AddNode: INode; stdcall;
    function InsertNode(const Source: INode; AIndex: LongInt): INode; stdcall;
    function InsertList(const Source: INodeList; AIndex: LongInt): INode; stdcall;
    procedure Exchange(Index1, Index2: LongInt); stdcall;
    procedure RemoveNode(const Node: INode); stdcall;
    procedure RemoveByIndex(Index: LongInt); stdcall;
    procedure MoveTo(CurIndex, NewIndex: LongInt); stdcall;
 //properties
    property Count: LongInt read GetCount write SetCount;
    property MaxCount: LongInt read GetMaxCount write SetMaxCount;

    property RootNode: INode read GetRootNode;
    property LastNode: INode read GetLastNode;
 end;

 INodeListEx = interface(INodeList)
 ['{0B61FDA4-BF4A-487F-9A14-0C86AF1A536F}']
    procedure LoadFromStream(const Stream: IReadWrite); stdcall;
    procedure SaveToStream(const Stream: IReadWrite); stdcall;
    procedure LoadFromFile(FileName: PWideChar); stdcall;
    procedure SaveToFile(FileName: PWideChar); stdcall;
 end;

 ISectionedItem = interface(INodeListEx)
 ['{4E982607-34C2-4223-A774-D13FE54A481D}']
 //private
    function GetName: PWideChar; stdcall;
    procedure SetName(Value: PWideChar); stdcall;
    function GetSignature: LongWord; stdcall;
    function CalculateChecksum: LongWord; stdcall;
    function CalculateDataSize: LongInt; stdcall;
    function GetTotalSize: LongInt; stdcall;
 //properties
    property Name: PWideChar read GetName write SetName;

    property Signature: LongWord read GetSignature;
    property Checksum: LongWord read CalculateChecksum;
    property DataSize: LongInt read CalculateDataSize;
    property TotalSize: LongInt read GetTotalSize;    
 end;

 ISectionedList = interface(ISectionedItem)
 ['{23EBD5E0-6E18-45F3-952B-E51DD212AA75}']
 //public
    function FindByContent(AChecksum: LongWord;
                           ADataSize: LongInt): ISectionedItem;

    function FindByName(AName: PWideChar): ISectionedItem;
    function FindIndexByFirstLetters(ALetters: PWideChar): LongInt;
    
    function AddChild(AName: PWideChar): ISectionedItem;
 end;

 IColorFormat = interface(IRoot)
 ['{E50291AE-F06E-43F8-8DC2-B11E0FCAA6F8}']
 //private
    function GetColorSize: LongInt; stdcall;
    procedure SetColorSize(Value: LongInt); stdcall;
    function GetUsedBits: LongInt; stdcall;
    procedure SetUsedBits(Value: LongInt); stdcall;
    function GetColorBits: LongInt; stdcall;

    function GetFlags: LongWord; stdcall;
    procedure SetFlags(Value: LongWord); stdcall;
    function GetAlpha: Boolean; stdcall;
    procedure SetAlpha(Value: Boolean); stdcall;
    function GetByteSwap: Boolean; stdcall;
    procedure SetByteSwap(Value: Boolean); stdcall;
    function GetChannelsCount: LongInt; stdcall;
    function GetFormatString: PAnsiChar; stdcall;
    procedure SetFormatString(Str: PAnsiChar); stdcall;

    function GetBlueMask: LongWord; stdcall;
    function GetGreenMask: LongWord; stdcall;
    function GetRedMask: LongWord; stdcall;
    function GetAlphaMask: LongWord; stdcall;

    function GetBlueCount: Byte; stdcall;
    function GetGreenCount: Byte; stdcall;
    function GetRedCount: Byte; stdcall;
    function GetAlphaCount: Byte; stdcall;

    function GetBlueShift: Byte; stdcall;
    function GetGreenShift: Byte; stdcall;
    function GetRedShift: Byte; stdcall;
    function GetAlphaShift: Byte; stdcall;

    function GetBlueRShift: Byte; stdcall;
    function GetGreenRShift: Byte; stdcall;
    function GetRedRShift: Byte; stdcall;
    function GetAlphaRShift: Byte; stdcall;
 //public
    procedure SetFormat(RBitMask, GBitMask, BBitMask, ABitMask: LongWord); stdcall;
    procedure Assign(const ClrFmt: IColorFormat); stdcall;
    procedure AssignMask(const ClrFmt: IColorFormat); stdcall;

    function ValueToRGBQuad(Value: LongWord): LongWord; stdcall;
    function ToRGBQuad(const Source): LongWord; stdcall;
    function FromRGBQuad(Source: LongWord): LongWord; stdcall;
    function ConvertValue(const DestFormat: IColorFormat;
                        const Source): LongWord; stdcall;
    function ConvertValue2(DestFormat: PAnsiChar; const Source): LongWord; stdcall;
    procedure ConvertToRGBQuad(Source, Dest: Pointer; Count: LongInt); stdcall;
    procedure ConvertFromRGBQuad(Source, Dest: Pointer; Count: LongInt); stdcall;
    procedure ConvertTo(const DestFormat: IColorFormat; Source, Dest: Pointer;
       Count: LongInt); stdcall;
    procedure ConvertTo2(DestFormat: PAnsiChar;
                        Source, Dest: Pointer; Count: LongInt); stdcall;
    procedure SetColor(Dest: Pointer; Index: LongInt; RGBQuad: LongWord); stdcall;
    procedure SetColor2(var Dest; RGBQuad: LongWord); stdcall;
    function SameAs(const CFormat: IColorFormat): Boolean; stdcall;
 //properties
    property ColorSize: LongInt read GetColorSize write SetColorSize;
    property UsedBits:  LongInt read GetUsedBits write SetUsedBits;
    property ColorBits: LongInt read GetColorBits;
    property Flags:     LongWord read GetFlags write SetFlags;
    property Alpha:     Boolean read GetAlpha write SetAlpha;
    property ByteSwap:  Boolean read GetByteSwap write SetByteSwap;
    property ChannelsCount: LongInt read GetChannelsCount;

    property BlueMask: LongWord read GetBlueMask;
    property GreenMask: LongWord read GetGreenMask;
    property RedMask: LongWord read GetRedMask;
    property AlphaMask: LongWord read GetAlphaMask;

    property BlueCount: Byte read GetBlueCount;
    property GreenCount: Byte read GetGreenCount;
    property RedCount: Byte read GetRedCount;
    property AlphaCount: Byte read GetAlphaCount;

    property BlueShift: Byte read GetBlueShift;
    property GreenShift: Byte read GetGreenShift;
    property RedShift: Byte read GetRedShift;
    property AlphaShift: Byte read GetAlphaShift;

    property BlueByteShift: Byte read GetBlueRShift;
    property GreenByteShift: Byte read GetGreenRShift;
    property RedByteShift: Byte read GetRedRShift;
    property AlphaByteShift: Byte read GetAlphaRShift;

    property FormatString: PAnsiChar read GetFormatString write SetFormatString;
 end;

 IColorTable = interface(ISectionedItem)
 ['{81AEAB98-011F-4FC4-851A-A56FED971E74}']
 //private
    function GetTableSize: LongInt; stdcall;
    function GetColor(X: LongInt): LongWord; stdcall;
    procedure SetColor(X: LongInt; Value: LongWord); stdcall;
    function GetRGBQuad(X: LongInt): LongWord; stdcall;
    procedure SetRGBQuad(X: LongInt; Value: LongWord); stdcall;
    function GetFormatString: PAnsiChar; stdcall;
    procedure SetFormatString(Value: PAnsiChar); stdcall;
    function GetColorFormat: IColorFormat; stdcall;
    procedure SetColorFormat(const Value: IColorFormat); stdcall;
    function GetColorData: Pointer; stdcall;
    function GetColorsCount: LongInt; stdcall;
    procedure SetColorsCount(Value: LongInt); stdcall;
    function GetTransparentIndex: LongInt; stdcall;
    procedure SetTransparentIndex(Value: LongInt); stdcall;
 //public
    procedure Reset(const AColorFmt: IColorFormat; ACount: LongInt = -1); stdcall;
    procedure Reset2(AColorFmt: PAnsiChar; ACount: LongInt = -1); stdcall;
    procedure LoadActFromStream(const Stream: IReadWrite); stdcall;
    procedure SaveActToStream(const Stream: IReadWrite); stdcall;
 //properties
    property ColorFormatString: PAnsiChar read GetFormatString write SetFormatString;
    property ColorFormat: IColorFormat read GetColorFormat write SetColorFormat;
    property ColorData: Pointer read GetColorData;
    property ColorsCount: LongInt read GetColorsCount write SetColorsCount;
    property TransparentIndex: LongInt read GetTransparentIndex write SetTransparentIndex;
    property TableSize: LongInt read GetTableSize;
    property RGBZ[X: LongInt]: LongWord read GetColor write SetColor;
    property BGRA[X: LongInt]: LongWord read GetRGBQuad write SetRGBQuad;
 end;

 IColorTableList = interface(ISectionedList)
 ['{3C9450E1-85C3-4051-ADF5-EE23D4941902}']
    function GetItem(Index: LongInt): IColorTable; stdcall;
 //public
    function AddColorTable(AName: PWideChar; const AColorFmt: IColorFormat;
                           ACount: LongInt = 256): IColorTable; stdcall;
    function AddColorTable2(AName: PWideChar; AColorFmt: PAnsiChar;
                           ACount: LongInt = 256): IColorTable; stdcall;
 //properties
    property Items[Index: LongInt]: IColorTable read GetItem; default;
 end;

 IBitmapContainer = interface(IRoot)
 ['{7309107A-F14A-4919-B3DC-FCB677B31514}']
 //private
    function GetImageSize: LongInt; stdcall;
    function GetBitsCountTotal: LongInt; stdcall;
    function GetPaletteSize: LongInt; stdcall;
    function GetPaletteCount: LongInt; stdcall;

    function GetPixelModifier: LongWord; stdcall;
    procedure SetPixelModifier(Value: LongWord); stdcall;
    function GetImageData: Pointer; stdcall;
    procedure SetImageData(Value: Pointer); stdcall;
    function GetColorTable: Pointer; stdcall;
    procedure SetColorTable(Value: Pointer); stdcall;
    function GetColorFormat: IColorFormat; stdcall;
    procedure SetColorFormat(const Value: IColorFormat); stdcall;
    function GetTransparentColor: LongWord; stdcall;
    procedure SetTransparentColor(Value: LongWord); stdcall;
    function GetLeft: LongInt; stdcall;
    procedure SetLeft(Value: LongInt); stdcall;
    function GetTop: LongInt; stdcall;
    procedure SetTop(Value: LongInt); stdcall;
    function GetWidth: Word; stdcall;
    procedure SetWidth(Value: Word); stdcall;
    function GetWidthRemainder: Word; stdcall;
    procedure SetWidthRemainder(Value: Word); stdcall;
    function GetWidthBytes: LongInt; stdcall;
    function GetHeight: Word; stdcall;
    procedure SetHeight(Value: Word); stdcall;
    function GetImageFormat: Byte; stdcall;
    procedure SetImageFormat(Value: Byte); stdcall;
    function GetImageFlags: Word; stdcall;
    procedure SetImageFlags(Value: Word); stdcall;
 //public
    function GetFlagsCount: LongInt; stdcall;
    procedure GetFlagsList(var Dest); stdcall;
    procedure SetFlagsList(const Value: array of Word); stdcall;
 //properties
    property PixelModifier: LongWord read GetPixelModifier write SetPixelModifier;
    property ImageData: Pointer read GetImageData write SetImageData;
    property ColorTable: Pointer read GetColorTable write SetColorTable;
    property ColorFormat: IColorFormat read GetColorFormat write SetColorFormat;
    property TransparentColor: LongWord read GetTransparentColor write SetTransparentColor;
    property Left: LongInt read GetLeft write SetLeft;
    property Top: LongInt read GetTop write SetTop;
    property Width: Word read GetWidth write SetWidth;
    property WidthRemainder: Word read GetWidthRemainder write SetWidthRemainder;
    property WidthBytes: LongInt read GetWidthBytes;
    property Height: Word read GetHeight write SetHeight;
    property ImageFormat: Byte read GetImageFormat write SetImageFormat;
    property ImageFlags: Word read GetImageFlags write SetImageFlags;

    property BitsCount: LongInt read GetBitsCountTotal;
    property ImageSize: LongInt read GetImageSize;
    property PaletteSize: LongInt read GetPaletteSize;
    property PaletteCount: LongInt read GetPaletteCount;
 end;

 IBitmapConverter = interface(IRoot)
 ['{F6638964-F582-41EC-BB0C-91C265BDAEB5}']
 //public
   procedure Reset(const SrcFlags, DstFlags: array of Word;
                   DrawFlags: LongInt = 0); stdcall;
 end;

 ITileItem = interface(INode)
 ['{FBAB6453-8B96-4528-9EB7-20C67260C9B4}']
 //private
    function GetTileSize: LongInt; stdcall;
    function GetNamedAttr(Name: PWideChar): OleVariant; stdcall;
    procedure SetNamedAttr(Name: PWideChar; const Value: OleVariant); stdcall;
    function GetTileIndex: LongInt; stdcall;
    procedure SetTileIndex(Value: LongInt); stdcall;
    function GetTileHeight: LongInt; stdcall;
    function GetTileWidth: LongInt; stdcall;
    function GetTileData: Pointer; stdcall;
    function GetFirstColor: Byte; stdcall;
    procedure SetFirstColor(Value: Byte); stdcall;
    function GetBufferIndex: LongInt; stdcall;
    function GetAttribute(Index: LongInt): OleVariant; stdcall;
    procedure SetAttribute(Index: LongInt; const Value: OleVariant); stdcall;
 //public
    procedure Load(const Src: IBitmapContainer;
     X, Y: LongInt; ADrawFlags: LongInt = 0); stdcall;
    procedure CvtLoad(const Src: IBitmapContainer;
     X, Y: LongInt; const Cvt: IBitmapConverter); stdcall;
    procedure Draw(const Dest: IBitmapContainer;
     X, Y: LongInt; ADrawFlags: LongInt = 0); stdcall;
    procedure CvtDraw(const Dest: IBitmapContainer;
     X, Y: LongInt; const Cvt: IBitmapConverter); stdcall;

    procedure AssignAttributes(const Source: ITileItem); stdcall;
 //properties
    property Width: LongInt read GetTileWidth;
    property Height: LongInt read GetTileHeight;
    property BufferIndex: LongInt read GetBufferIndex;
    property Attributes[Index: LongInt]: OleVariant read GetAttribute write SetAttribute;
    property NamedAttr[Name: PWideChar]: OleVariant read GetNamedAttr write SetNamedAttr;
    property TileSize: LongInt read GetTileSize;
    property TileData: Pointer read GetTileData;
    property TileIndex: LongInt read GetTileIndex write SetTileIndex;
    property FirstColor: Byte read GetFirstColor write SetFirstColor;
 end;

 PAttrDef = ^TAttrDef;
 TAttrDef = record
  adSelf: record end;
  adName: Pointer;
  adType: TVarType;
  adEnum: Pointer;
  adMaxArrayLen: LongInt;
  case LongInt of
   0: (adMin: Int64;
       adMax: Int64;
       adNumSys: Byte);
   1: (adPrecision: Byte);
   2: (adMaxStrLen: LongInt);
 end;

 PTileICvtRec = ^TTileICvtRec;
 TTileICvtRec = record
  tcvtLoad: IBitmapConverter;
  tcvtDraw: array[Boolean, Boolean] of IBitmapConverter;
 end;

 ITileSet = interface(ISectionedItem)
 ['{640878A6-DC3C-410D-B9EE-7EB83537D4FD}']
 //private
    function GetAttrEnumCount(Index: LongInt): LongInt; stdcall;
    procedure SetAttrEnumCount(Index, Value: LongInt); stdcall;
    function GetAttrEnumMember(AttrIndex, EnumIndex: LongInt): PWideChar; stdcall;
    procedure SetAttrEnumMember(AttrIndex, EnumIndex: LongInt; Value: PWideChar); stdcall;
    function GetAttrName(Index: LongInt): PWideChar; stdcall;
    procedure SetAttrName(Index: LongInt; Value: PWideChar); stdcall;
    function GetTilesCount: LongInt; stdcall;
    function GetIndexed(Index: LongInt): ITileItem; stdcall;
    function GetAttrCount: LongInt; stdcall;
    procedure SetAttrCount(Value: LongInt); stdcall;
    function GetAttrDef(Index: LongInt): PAttrDef; stdcall;
    function GetTransparentColor: LongWord; stdcall;
    procedure SetTransparentColor(Value: LongWord); stdcall;
    function GetTileHeight: LongInt; stdcall;
    function GetTileSize: LongInt; stdcall;
    function GetTileWidth: LongInt; stdcall;
    function GetBitCount: LongInt; stdcall;
    function GetTileItem(Index: LongInt): ITileItem; stdcall;
    function GetLastIndex: LongInt; stdcall;
    procedure SetLastIndex(Value: LongInt); stdcall;
    procedure SetTilesCount(Value: LongInt); stdcall;
    function GetEmptyTileIndex: LongInt; stdcall;
    function GetMaxIndex: LongInt; stdcall;
    procedure SetMaxIndex(Value: LongInt); stdcall;
    function GetFastList: Boolean; stdcall;
    procedure SetFastList(Value: Boolean); stdcall;    
    function GetHaveEmpty: Boolean; stdcall;

    function GetKeepBuffer: Boolean; stdcall;
    procedure SetKeepBuffer(Value: Boolean); stdcall;
    function GetDrawFlags: Word; stdcall;
    procedure SetDrawFlags(Value: Word); stdcall;
    function GetOptimizationFlags: Word; stdcall;
    procedure SetOptimizationFlags(Value: Word); stdcall;
    function GetTileBitmap: IBitmapContainer; stdcall;
    function GetEmptyTile: ITileItem; stdcall;
 //public
    function FindAttrByName(Name: PWideChar): LongInt; stdcall;
    function FindByData(const Source): ITileItem; stdcall;

    procedure DeleteAttr(Index: LongInt); stdcall;
    procedure MoveAttr(OldIndex, NewIndex: LongInt); stdcall;
    function AddTile: ITileItem; stdcall;
    function AddTileEx(const AttrList: array of OleVariant): ITileItem; stdcall;
    function AddFixed(Index: LongInt): ITileItem; stdcall;
    function AddOptimized(const Source): ITileItem; stdcall;
    function AddOptimizedEx(const Source; const Attr: array of OleVariant;
                          var XFlip, YFlip: Boolean): ITileItem; stdcall;

    procedure TileFlip(const Source; var Dest; X, Y: Boolean); stdcall;
    procedure TileLoad(const Src: IBitmapContainer; var Dest;
     X, Y: LongInt; ADrawFlags: LongInt = 0); stdcall;
    procedure TileCvtLoad(const Src: IBitmapContainer; var Dest;
     X, Y: LongInt; const Cvt: IBitmapConverter); stdcall;
    procedure TileDraw(const Source; const Dest: IBitmapContainer;
     X, Y: LongInt; ADrawFlags: LongInt = 0); stdcall;
    procedure TileCvtDraw(const Source; const Dest: IBitmapContainer;
     X, Y: LongInt; const Cvt: IBitmapConverter); stdcall;

    function FindEmptyIndex: LongInt; stdcall;

    procedure FillConverter(const Cvt: IBitmapConverter;
             const Flags: array of Word; ADrawFlags: Word = 0);  stdcall;
    procedure FillConvertRec(var CvtRec: TTileICvtRec;
                             const Flags: array of Word;
                              ADrawFlags: Word = 0); stdcall;

    procedure ReadFromStream(const Stream: IReadWrite;
     AIndex, ACount: LongInt; AOptimize: Boolean); stdcall;
    procedure WriteToStream(const Stream: IReadWrite;
     AIndex, ACount: LongInt; AUseEmptyTiles: Boolean); stdcall;

    procedure AssignAttrDefs(const Source: ITileSet); stdcall;
    procedure AssignTileFormat(const Source: ITileSet); stdcall;
    procedure ConvertFrom(const Source: ITileSet); stdcall;

    procedure SwitchFormat(const Flags: array of Word;
                           ATileWidth: LongInt = -1;
                           ATileHeight: LongInt = -1); stdcall;
    procedure Reallocate(const Flags: array of Word;
                         ATileWidth: LongInt = -1;
                         ATileHeight: LongInt = -1); stdcall;
 //properties
    property KeepBuffer: Boolean read GetKeepBuffer write SetKeepBuffer;
    property MaxIndex: LongInt read GetMaxIndex write SetMaxIndex;
    property EmptyTileIndex: LongInt read GetEmptyTileIndex;
    property TransparentColor: LongWord read GetTransparentColor
                                       write SetTransparentColor;
    property DrawFlags: Word read GetDrawFlags write SetDrawFlags;
    property AttrCount: LongInt read GetAttrCount write SetAttrCount;
    property TilesCount: LongInt read GetTilesCount write SetTilesCount;
    property OptimizationFlags: Word read GetOptimizationFlags write SetOptimizationFlags;
    property TileWidth: LongInt read GetTileWidth;
    property TileHeight: LongInt read GetTileHeight;
    property TileSize: LongInt read GetTileSize;
    property TileBitmap: IBitmapContainer read GetTileBitmap;
    property BitCount: LongInt read GetBitCount;
    property LastIndex: LongInt read GetLastIndex write SetLastIndex;
    property EmptyTile: ITileItem read GetEmptyTile;
    property FastList: Boolean read GetFastList write SetFastList;
    property HaveEmpty: Boolean read GetHaveEmpty;

    property AttrDefs[Index: LongInt]: PAttrDef read GetAttrDef;
    property AttrNames[Index: LongInt]: PWideChar read GetAttrName write SetAttrName;
    property AttrEnumCount[Index: LongInt]: LongInt read GetAttrEnumCount write SetAttrEnumCount;
    property AttrEnumMember[AttrIndex, EmumIndex: LongInt]: PWideChar
             read GetAttrEnumMember
            write SetAttrEnumMember;
    property Indexed[Index: LongInt]: ITileItem read GetIndexed;
    property Items[Index: LongInt]: ITileItem read GetTileItem;
 end;

 ITileSetList = interface(ISectionedList)
 ['{62A33A1D-6D1C-4FD0-82B6-35DC61CBA8DB}']
 //private
    function GetSet(X: LongInt): ITileSet; stdcall;
 //public
    procedure FillCvtRecList(AList: PTileICvtRec;
                const AFlags: array of Word; ADrawFlags: Word = 0); stdcall;
 //properties
    property Sets[X: LongInt]: ITileSet read GetSet; default; 
 end;

 TLayerFormatRec = packed record
  lfTileSet:           Pointer;
  lfIndexMask:         LongWord;
  lfPaletteIndexMask:  LongWord;
  lfPriorityMask:      LongWord;
  lfIndexShift:        ShortInt;
  lfPaletteIndexShift: ShortInt;
  lfPriorityShift:     ShortInt;
  lfXFlipShift:        ShortInt;
  lfYFlipShift:        ShortInt;
  lfCellSize:          Byte;
 end;

 PCellInfo = ^TCellInfo;
 TCellInfo = packed record
  TileIndex: LongInt;
  PaletteIndex: LongInt;
  Priority: LongInt;
  XFlip: Boolean;
  YFlip: Boolean;
 end;

 IMapLayer = interface(ISectionedItem)
 ['{ED74DBB2-37D8-4DAD-B40C-E5A3C900AD7F}']
 //private
    function GetCellSize: Byte; stdcall;
    procedure SetCellSize(Value: Byte); stdcall;
    procedure SetTileSetIndex(Value: LongInt); stdcall;
    function GetTileSetIndex: LongInt; stdcall;
    function GetLayerDataSize: LongInt; stdcall;
    function GetCellInfo(X, Y: LongInt): TCellInfo; stdcall;
    procedure SetCellInfo(X, Y: LongInt; const Value: TCellInfo); stdcall;
    function GetTileIndex(X, Y: LongInt): LongInt; stdcall;
    procedure SetTileIndex(X, Y: LongInt; Value: LongInt); stdcall;
    function GetIndexMask: LongWord; stdcall;
    procedure SetIndexMask(Value: LongWord); stdcall;
    function GetIndexShift: ShortInt; stdcall;
    procedure SetIndexShift(Value: ShortInt); stdcall;
    function GetPaletteIndexMask: LongWord; stdcall;
    procedure SetPaletteIndexMask(Value: LongWord); stdcall;
    function GetPaletteIndexShift: ShortInt; stdcall;
    procedure SetPaletteIndexShift(Value: ShortInt); stdcall;
    function GetPriorityMask: LongWord; stdcall;
    procedure SetPriorityMask(Value: LongWord); stdcall;
    function GetPriorityShift: ShortInt; stdcall;
    procedure SetPriorityShift(Value: ShortInt); stdcall;
    function GetXFlipShift: ShortInt; stdcall;
    procedure SetXFlipShift(Value: ShortInt); stdcall;
    function GetYFlipShift: ShortInt; stdcall;
    procedure SetYFlipShift(Value: ShortInt); stdcall;
    function GetTilesFilled: Boolean; stdcall;
    procedure SetTilesFilled(Value: Boolean); stdcall;
    function GetDrawInfo: PCellInfo; stdcall;
    function GetVisibleRect: TRect; stdcall;
    function GetLayerData: Pointer; stdcall;
    function GetTileSet: ITileSet; stdcall;
    function GetFirstColor: Byte; stdcall;
    procedure SetFirstColor(Value: Byte); stdcall;
    function GetFlags: Byte; stdcall;
    procedure SetFlags(Value: Byte); stdcall;
 //public
    function CompareLayerFormat(const AFmt: TLayerFormatRec): Boolean;
    procedure FillLayerFormat(var AFmt: TLayerFormatRec);
    function GetLayerFormat: TLayerFormatRec;

    procedure ReadCellInfo(const Data; var Info: TCellInfo);
    function ReadTileIndex(var Data): LongInt;
    function ReadPaletteIndex(var Data): LongInt;
    function ReadPriority(var Data): LongInt;
    function ReadXFlip(var Data): Boolean;
    function ReadYFlip(var Data): Boolean;

    procedure WriteCellInfo(var Data; const Info: TCellInfo);
    procedure WriteTileIndex(var Data; Value: LongInt);
    procedure WritePaletteIndex(var Data; Value: LongInt);
    procedure WritePriority(var Data; Value: LongInt);
    procedure WriteXFlip(var Data; Value: Boolean);
    procedure WriteYFlip(var Data; Value: Boolean);
    function FindCellInfo(const Info: TCellInfo): LongInt;

    procedure AssignLayerFormat(const Source: IMapLayer);
    procedure AssignLayerInfo(const Source: IMapLayer);

    procedure UpdateFormat(const NewFormat: TLayerFormatRec);
 //properties
    property VisibleRect: TRect read GetVisibleRect;
    property DrawInfo: PCellInfo read GetDrawInfo;
    property LayerData: Pointer read GetLayerData;
    property LayerDataSize: LongInt read GetLayerDataSize;
    property TileSet: ITileSet read GetTileSet;
    property TileSetIndex: LongInt read GetTileSetIndex write SetTileSetIndex;
    property TilesFilled: Boolean read GetTilesFilled write SetTilesFilled;

    property CellSize: Byte read GetCellSize write SetCellSize;
    property IndexMask: LongWord read GetIndexMask write SetIndexMask;
    property IndexShift: ShortInt read GetIndexShift write SetIndexShift;
    property XFlipShift: ShortInt read GetXFlipShift write SetXFlipShift;
    property YFlipShift: ShortInt read GetYFlipShift write SetYFlipShift;
    property PaletteIndexShift: ShortInt read GetPaletteIndexShift write SetPaletteIndexShift;
    property PaletteIndexMask: LongWord read GetPaletteIndexMask write SetPaletteIndexMask;
    property PriorityMask: LongWord read GetPriorityMask write SetPriorityMask;
    property PriorityShift: ShortInt read GetPriorityShift write SetPriorityShift;
    property FirstColor: Byte read GetFirstColor write SetFirstColor;
    property Flags: Byte read GetFlags write SetFlags;

    property Cells[X, Y: LongInt]: TCellInfo read GetCellInfo write SetCellInfo;
    property TileIndex[X, Y: LongInt]: LongInt read GetTileIndex write SetTileIndex;
 end;

 TLinkDirection = Byte;
 TLinkDirections = Byte;

 IMap = interface;

 IBrushLayer = interface(IMapLayer)
 ['{E80CBF01-87ED-4D60-A152-0150D8FB9EC0}']
 //private
    function GetHotSpotX: LongInt; stdcall;
    procedure SetHotSpotX(Value: LongInt); stdcall;
    function GetHotSpotY: LongInt; stdcall;
    procedure SetHotSpotY(Value: LongInt); stdcall;
    function GetIdentityTag: LongInt; stdcall;
    procedure SetIdentityTag(Value: LongInt); stdcall;
    function GetTailsFilled: Boolean; stdcall;
    procedure SetTailsFilled(Value: Boolean); stdcall;
    function GetTailsDirections: TLinkDirections; stdcall;
    function GetRandomize: Boolean; stdcall;
    procedure SetRandomize(Value: Boolean); stdcall;

    function GetUnfinished: TLinkDirections; stdcall;
    procedure SetUnfinished(Value: TLinkDirections); stdcall;
    function GetExtended: TLinkDirections; stdcall;
    procedure SetExtended(Value: TLinkDirections); stdcall;

    function GetRandomTwin: IBrushLayer; stdcall;

    function GetLinkIndex(Direction: TLinkDirection): LongInt; stdcall;
    procedure SetLinkIndex(Direction: TLinkDirection; Value: LongInt); stdcall;
    function GetLink(Direction: TLinkDirection): IBrushLayer; stdcall;
 //public
    function SitsHere(const Target: IMapLayer; X, Y: LongInt): Boolean; stdcall;
    function SpottedInRect(const Target: IMapLayer;
     const Rect: TRect; var pt: TPoint): Boolean; stdcall;
    function RectOnMap(X, Y: LongInt): TRect; stdcall;

    function DrawTail(const Dest: IMap;
     DestX, DestY: LongInt; Direction: TLinkDirection): Boolean; stdcall;
    function Draw(const DestLayer: IMapLayer;
     DestX, DestY: LongInt): Boolean; stdcall;
 //properties
    property HotSpotX: LongInt read GetHotSpotX write SetHotSpotX;
    property HotSpotY: LongInt read GetHotSpotY write SetHotSpotY;
    property IdentityTag: LongInt read GetIdentityTag write SetIdentityTag;
    property TailsFilled: Boolean read GetTailsFilled write SetTailsFilled;
    property TailsDirections: TLinkDirections read GetTailsDirections;
    property Randomize: Boolean read GetRandomize write SetRandomize;

    property Unfinished: TLinkDirections read GetUnfinished write SetUnfinished;
    property Extended: TLinkDirections read GetExtended write SetExtended;

    property RandomTwin: IBrushLayer read GetRandomTwin;

    property LinkIndex[Direction: TLinkDirection]: LongInt
        read GetLinkIndex
       write SetLinkIndex;
    property Links[Direction: TLinkDirection]: IBrushLayer read GetLink;
 end;

 IMap = interface(ISectionedList)
 ['{6990F31B-F197-48E5-BB4C-6A040E6DEE3C}']
 //private
    function GetMapX: LongInt; stdcall;
    procedure SetMapX(Value: LongInt); stdcall;
    function GetMapY: LongInt; stdcall;
    procedure SetMapY(Value: LongInt); stdcall;
    function GetWidth: LongInt; stdcall;
    procedure SetWidth(Value: LongInt); stdcall;
    function GetHeight: LongInt; stdcall;
    procedure SetHeight(Value: LongInt); stdcall;
    function GetTileWidth: LongInt; stdcall;
    procedure SetTileWidth(Value: LongInt); stdcall;
    function GetTileHeight: LongInt; stdcall;
    procedure SetTileHeight(Value: LongInt); stdcall;
    function GetColorTable: IColorTable; stdcall;
    function GetCtIndex: LongInt; stdcall;
    procedure SetCtIndex(Value: LongInt); stdcall;

    function GetLayer(Index: LongInt): IMapLayer; stdcall;
 //public
    procedure AssignMapInfo(const Source: IMap); stdcall;
    function FindLayerByTileSet(const ATileSet: ITileSet): IMapLayer; stdcall;
    procedure SetMapSize(AWidth, AHeight: LongInt); stdcall;

    function IsTileSetUsed(const ATileSet: ITileSet): Boolean; stdcall;
    procedure UnhookTileSet(const ATileSet: ITileSet); stdcall;

    function AddLayer(Name: PWideChar): IMapLayer; stdcall;
 //properties
    property MapX: LongInt read GetMapX write SetMapX;
    property MapY: LongInt read GetMapY write SetMapY;
    property Width: LongInt read GetWidth write SetWidth;
    property Height: LongInt read GetHeight write SetHeight;
    property TileWidth: LongInt read GetTileWidth write SetTileWidth;
    property TileHeight: LongInt read GetTileHeight write SetTileHeight;
    property ColorTable: IColorTable read GetColorTable;
    property ColorTableIndex: LongInt read GetCtIndex write SetCtIndex;

    property Layers[Index: LongInt]: IMapLayer read GetLayer;
 end;

 IMapBrush = interface(IMap)
 ['{1BED0B51-A212-46E6-B7C8-D46CAF270D9A}']
 //private
    function GetExtends: IMapBrush; stdcall;
    function GetExtendsIndex: LongInt; stdcall;
    procedure SetExtendsIndex(Value: LongInt); stdcall;
 //public
    function FindExactBrushLayer(const Dest: IMap;
     X, Y: LongInt; var pt: TPoint): IBrushLayer; stdcall;
    function FindBrushLayer(const Dest: IMap;
     X, Y: LongInt; var pt: TPoint): IBrushLayer; stdcall;
    function Draw(const Dest: IMap; DestX, DestY: LongInt): Boolean; stdcall;

    procedure RepointLinks; stdcall;
 //properties
    property Extends: IMapBrush read GetExtends;
    property ExtendsIndex: LongInt read GetExtendsIndex write SetExtendsIndex;
 end;

 IBaseMapList = interface(ISectionedList)
 ['{04314D6A-BEE1-4CAD-938B-D11A4E961D7F}']
 //private
    function GetColorTableList: IColorTableList; stdcall;
    function GetTileSetList: ITileSetList; stdcall;
    function GetMap(Index: LongInt): IMap; stdcall;
 //public
    function AddMap(AName: PWideChar;
     AWidth, AHeight, ATileW, ATileH: LongInt): IMap; stdcall;
    function IsColorTableUsed(const AColorTable: IColorTable): Boolean; stdcall;
    function IsTileSetUsed(const ATileSet: ITileSet): Boolean; stdcall;
    procedure UnhookTileSet(const ATileSet: ITileSet); stdcall;
    procedure UnhookColorTable(const AColorTable: IColorTable); stdcall;
 //properties
    property Maps[Index: LongInt]: IMap read GetMap; default;
 end;

 IBrushList = interface(IBaseMapList)
 ['{04314D6A-BEE1-4CAD-938B-D11A4E961D7F}']
 //private
    procedure SetColorTableList(const Value: IColorTableList); stdcall;
    procedure SetTileSetList(const Value: ITileSetList); stdcall;

    function GetIdentitiesFilled: Boolean; stdcall;
    procedure SetIdentitiesFilled(Value: Boolean); stdcall;

    function GetMapBrush(Index: LongInt): IMapBrush; stdcall;
 //public
    procedure CleanUpTails;  stdcall;
    function EraseBrushContentAt(const Dest: IMap;
     const DestRect: TRect): Boolean; stdcall;
    function IsBrushUsed(const ABrush: IMapBrush): Boolean; stdcall;
    procedure UnhookBrush(const ABrush: IMapBrush); stdcall;
 //properties
    property Brushes[Index: LongInt]: IMapBrush read GetMapBrush; default;

    property IdentitiesFilled: Boolean
             read GetIdentitiesFilled
            write SetIdentitiesFilled;

    property ColorTableList: IColorTableList
             read GetColorTableList
            write SetColorTableList;
    property TileSetList: ITileSetList
             read GetTileSetList
            write SetTileSetList;
 end;

 IMapList = interface(IBaseMapList)
 ['{04314D6A-BEE1-4CAD-938B-D11A4E961D7F}']
 //private
    function GetBrushList: IBrushList; stdcall;
 //properties
    property BrushList: IBrushList read GetBrushList;
    property ColorTableList: IColorTableList read GetColorTableList;
    property TileSetList: ITileSetList read GetTileSetList;
 end;

 IStream32 = interface(IReadWrite)
 ['{9B7F6EAF-0306-4943-B687-A9FBBDA38FB5}']
 //private
    function GetPos: LongInt; stdcall;
    function GetSize: LongInt; stdcall;
    procedure SetPos(Value: LongInt); stdcall;
    procedure SetSize(Value: LongInt); stdcall;
 //public
    procedure Seek(Offset, SeekOrigin: LongInt); stdcall;
    function CopyFrom(const Stream: IReadWrite;
                      Count: LongInt): LongInt; stdcall;
 //properties
    property Position: LongInt read GetPos write SetPos;
    property Size: LongInt read GetSize write SetSize;
 end;

 IMemoryStream = interface(IStream32)
 ['{C97E7083-9C87-4B67-A654-8EB75C18CD14}']
 //private
    function GetMemory: Pointer;
 //public
    procedure Clear; stdcall;
 //properties
    property Memory: Pointer read GetMemory;
 end;

 IStream64 = interface(IReadWrite)
 ['{737B631C-7CF5-4F7F-B902-8940E5A16B78}']
 //private
    function GetPos: Int64; stdcall;
    function GetSize: Int64; stdcall;
    procedure SetPos(const Value: Int64); stdcall;
    procedure SetSize(const Value: Int64); stdcall;
 //public
    procedure Seek(const Offset: Int64; SeekOrigin: LongInt); stdcall;
    function CopyFrom(const Stream: IReadWrite;
                      const Count: Int64): Int64; stdcall;
 //properties
    property Position: Int64 read GetPos write SetPos;
    property Size: Int64 read GetSize write SetSize;
 end;

 IStrings = interface(IRoot)
 ['{295E6898-C6AA-4367-99D2-ED49393D2780}']
    function GetCount: LongInt; stdcall;
    procedure Delete(Index: LongInt); stdcall;
    procedure Exchange(Index1, Index2: LongInt); stdcall;
    procedure Move(CurIndex, NewIndex: LongInt); stdcall;
    procedure Sort; stdcall;
    procedure AddStrings(const Strings: IStrings); stdcall;
    procedure Assign(const Source: IStrings); stdcall;
    function Equals(const Strings: IStrings): Boolean; stdcall;

    property Count: LongInt read GetCount;
 end;

 IStringList = interface(IStrings)
 ['{AA35A82E-1B4E-4201-B3E4-C8EE59C9EC5A}']
 //public
    function Add(Str: PAnsiChar): LongInt; stdcall;
    procedure Append(Str: PAnsiChar); stdcall;
    function Find(Str: PAnsiChar; var Index: LongInt): Boolean; stdcall;
    function IndexOf(Str: PAnsiChar): LongInt; stdcall;
    procedure Insert(Index: LongInt; Str: PAnsiChar); stdcall;

    function GetString(Index: LongInt): PAnsiChar; stdcall;
    procedure SetString(Index: LongInt; Str: PAnsiChar); stdcall;

    function GetText: PAnsiChar; stdcall;
    procedure SetText(Str: PAnsiChar); stdcall;
 //properties
    property Strings[Index: LongInt]: PAnsiChar
             read GetString write SetString; default;
    property Text: PAnsiChar read GetText write SetText;    
 end;

 IWideStringList = interface(IStrings)
 ['{0BF578AE-F616-44AA-99B4-D2290AD0D90C}']
 //public
    function Add(Str: PWideChar): LongInt; stdcall;
    procedure Append(Str: PWideChar); stdcall;
    function Find(Str: PWideChar; var Index: LongInt): Boolean; stdcall;
    function IndexOf(Str: PWideChar): LongInt; stdcall;
    procedure Insert(Index: LongInt; Str: PWideChar); stdcall;

    function GetString(Index: LongInt): PWideChar; stdcall;
    procedure SetString(Index: LongInt; Str: PWideChar); stdcall;

    function GetText: PWideChar; stdcall;
    procedure SetText(Str: PWideChar); stdcall;

 //properties
    property Strings[Index: LongInt]: PWideChar
             read GetString write SetString; default;
    property Text: PWideChar read GetText write SetText;
 end;

 //PropContainer types
 TValueType = Byte;
 IPropertyListItem = interface;
 IPropertyList = interface;
// TValueChangeProc = procedure(Sender: Pointer); stdcall;

 IPropertyListItem = interface(INode)
 ['{6A1B47D6-7724-4466-AEF9-3426CF0CD024}']
 //private
    function NameGet: PWideChar; stdcall;
    procedure NameSet(Value: PWideChar); stdcall;
    function ValueTypeGet: TValueType; stdcall;
    procedure ValueTypeSet(Value: TValueType); stdcall;
    function ValueStrGet: PWideChar; stdcall;
    procedure ValueStrSet(Value: PWideChar); stdcall;
    function DefValStrGet: PWideChar; stdcall;
    procedure DefValStrSet(Value: PWideChar); stdcall;
    function ValueGet: Int64; stdcall;
    procedure ValueSet(const Value: Int64); stdcall;
    function MinGet: Int64; stdcall;
    procedure MinSet(const Value: Int64); stdcall;
    function MaxGet: Int64; stdcall;
    procedure MaxSet(const Value: Int64); stdcall;
    function HexDigitsGet: Byte; stdcall;
    procedure HexDigitsSet(Value: Byte); stdcall;
    function ReadOnlyGet: Boolean; stdcall;
    procedure ReadOnlySet(Value: Boolean); stdcall;
    function ParamsGet: LongInt; stdcall;
    procedure ParamsSet(Value: LongInt); stdcall;
    function DataGet: Pointer; stdcall;
    function DataSizeGet: LongInt; stdcall;
    procedure DataSizeSet(Value: LongInt); stdcall;
    function GetPickListIndex: LongInt; stdcall;
    procedure SetPickListIndex(Value: LongInt); stdcall;
    function GetPickList: IWideStringList; stdcall;
    function SubPropsGet: IPropertyList; stdcall;
    function GetDataStream: IStream32; stdcall;
    function GetItemOwner: IPropertyListItem; stdcall;
    function GetMaxLength: LongInt; stdcall;
    procedure SetMaxLength(Value: LongInt); stdcall;
    function GetModified: Boolean; stdcall;
    function GetStep: Int64; stdcall;
    procedure SetStep(const Value: Int64); stdcall;
    function GetFloat: Double; stdcall;
    procedure SetFloat(const Value: Double); stdcall;
    function GetPrecision: Byte; stdcall;
    procedure SetPrecision(Value: Byte); stdcall;
{    function GetOnValueChange: TValueChangeProc; stdcall;
    procedure SetOnValueChange(Value: TValueChangeProc); stdcall;}
 //public
    procedure InitBoolean(AName: PWideChar; AValue: Boolean); stdcall;

    procedure InitFloat(AName: PWideChar; const AValue: Double;
             APrecision: Byte); stdcall;
    procedure InitDecimal(AName: PWideChar;
          const AValue, AMin, AMax: Int64); stdcall;
    procedure InitHexadecimal(AName: PWideChar;
          const AValue, AMin, AMax: Int64; ADigits: Byte); stdcall;

    procedure InitString(AName, AValue: PWideChar; Editable: Boolean); stdcall;

    procedure InitPickList(AName: PWideChar;
              const AList: IWideStringList;
              AIndex: LongInt;
              AFixed: Boolean); stdcall;
    procedure InitPickList2(AName, AList: PWideChar; AIndex: LongInt;
                           AFixed: Boolean); stdcall;
    procedure InitPickList3(AName, AValue, AList: PWideChar;
                           AFixed: Boolean); stdcall;
    procedure InitData(AName, Description: PWideChar); stdcall;
 //properties
    property Name: PWideChar read NameGet write NameSet;
    property ValueType: TValueType read ValueTypeGet write ValueTypeSet;
    property ValueStr: PWideChar read ValueStrGet write ValueStrSet;
    property DefaultStr: PWideChar read DefValStrGet write DefValStrSet;
    property Value: Int64 read ValueGet write ValueSet;
    property ValueMin: Int64 read MinGet write MinSet;
    property ValueMax: Int64 read MaxGet write MaxSet;
    property HexDigits: Byte read HexDigitsGet write HexDigitsSet;
    property ReadOnly: Boolean read ReadOnlyGet write ReadOnlySet;
    property Parameters: LongInt read ParamsGet write ParamsSet;
    property DataStream: IStream32 read GetDataStream;
    property Data: Pointer read DataGet;
    property DataSize: LongInt read DataSizeGet write DataSizeSet;
    property PickListIndex: LongInt read GetPickListIndex
                                   write SetPickListIndex;
    property PickList: IWideStringList read GetPickList;
    property SubProperties: IPropertyList read SubPropsGet;
    property ItemOwner: IPropertyListItem read GetItemOwner;
    property MaxLength: LongInt read GetMaxLength write SetMaxLength;
    property Modified: Boolean read GetModified;
    property Step: Int64 read GetStep write SetStep;
    property Float: Double read GetFloat write SetFloat;
    property Precision: Byte read GetPrecision write SetPrecision;
{    property OnValueChange: TValueChangeProc
             read GetOnValueChange
            write SetOnValueChange;}
 end;

 IPropertyList = interface(INodeListEx)
 ['{CF720291-343E-477E-8C6C-08556F8C0E21}']
 //private
    function PropertyGet(Index: LongInt): IPropertyListItem; stdcall;
 //public
    function AddProperty: IPropertyListItem; stdcall;

    function FloatAdd(AName: PWideChar; const AValue: Double;
             APrecision: Byte): IPropertyListItem; stdcall;
    function BooleanAdd(Name: PWideChar;
                       Value: Boolean): IPropertyListItem; stdcall;
    function DecimalAdd(Name: PWideChar;
                      const Value, Min, Max: Int64): IPropertyListItem; stdcall;
    function HexadecimalAdd(Name: PWideChar;
                            const Value, Min, Max: Int64;
                            Digits: Byte): IPropertyListItem; stdcall;
    function StringAdd(Name, Value: PWideChar;
                      Editable: Boolean): IPropertyListItem; stdcall;
    function PickListAdd(AName: PWideChar;
              const List: IWideStringList;
              Index: LongInt;
              Fixed: Boolean): IPropertyListItem; stdcall;
    function PickListAdd2(Name, List: PWideChar; Index: LongInt;
                        Fixed: Boolean): IPropertyListItem; stdcall;
    function PickListAdd3(Name, Value, List: PWideChar;
                        Fixed: Boolean): IPropertyListItem; stdcall;
    function FilePickAdd(Name, FileName, Filter, DefaultExt,
     InitialDir: PWideChar; Save: Boolean = False; Options: LongInt =
     FO_HideReadOnly or FO_EnableSizing): IPropertyListItem; stdcall;
    function DataAdd(Name, Description: PWideChar): IPropertyListItem; stdcall;
{    function GetOnValueChange: TValueChangeProc; stdcall;
    procedure SetOnValueChange(Value: TValueChangeProc); stdcall;}
 //properties
    property Properties[Index: LongInt]: IPropertyListItem read PropertyGet;
                                                                  {
    property OnValueChange: TValueChangeProc
             read GetOnValueChange
            write SetOnValueChange;                                }
 end;

 IIniFile = interface(IRoot)
 ['{0D706F2D-1146-4554-ADEE-9D8DEFAA2C6D}']
 //private
    function GetFileName: PWideChar; stdcall;
 //public
    procedure LoadFromStream(const Stream: IReadWrite); stdcall;
    procedure SaveToStream(const Stream: IReadWrite); stdcall;
    procedure LoadFromFile(FileName: PWideChar); stdcall;
    procedure SaveToFile(FileName: PWideChar); stdcall;
    procedure SetStrings(const List: IWideStringList); stdcall;
    procedure WriteString(Section, Ident, Value: PWideChar); stdcall;
    function ReadString(Section, Ident, Default: PWideChar): PWideChar; stdcall;
    function ReadInteger(Section, Ident: PWideChar; Default: Longint): Longint; stdcall;
    function ReadInt64(Section, Ident: PWideChar; const Default: Int64): Int64; stdcall;    
    procedure WriteInteger(Section, Ident: PWideChar; Value: Longint; HexDigits: Byte = 0); stdcall;
    procedure WriteInt64(Section, Ident: PWideChar; const Value: Int64; HexDigits: Byte = 0); stdcall;
    function ReadBool(Section, Ident: PWideChar; Default: Boolean): Boolean; stdcall;
    procedure WriteBool(Section, Ident: PWideChar; Value: Boolean); stdcall;
    function ReadBinaryStream(Section, Name: PWideChar; const Value: IReadWrite): LongInt; stdcall;
    function ReadDate(Section, Name: PWideChar; const Default: TDateTime): TDateTime; stdcall;
    function ReadDateTime(Section, Name: PWideChar; const Default: TDateTime): TDateTime; stdcall;
    function ReadFloat(Section, Name: PWideChar; const Default: Double): Double; stdcall;
    function ReadTime(Section, Name: PWideChar; const Default: TDateTime): TDateTime; stdcall;
    procedure WriteBinaryStream(Section, Name: PWideChar; const Value: IReadWrite); stdcall;
    procedure WriteDate(Section, Name: PWideChar; const Value: TDateTime); stdcall;
    procedure WriteDateTime(Section, Name: PWideChar; const Value: TDateTime); stdcall;
    procedure WriteFloat(Section, Name: PWideChar; const Value: Double); stdcall;
    procedure WriteTime(Section, Name: PWideChar; const Value: TDateTime); stdcall;
    procedure ReadSection(Section: PWideChar; const Strings: IWideStringList); stdcall;
    procedure ReadSections(const Strings: IWideStringList); stdcall;
    procedure ReadSectionValues(Section: PWideChar; const Strings: IWideStringList); stdcall;
    procedure EraseSection(Section: PWideChar); stdcall;
    procedure DeleteKey(Section, Ident: PWideChar); stdcall;
    function ValueExists(Section, Ident: PWideChar): Boolean; stdcall;
    function SectionExists(Section: PWideChar): Boolean; stdcall;
 //properties
    property FileName: PWideChar read GetFileName;
 end;

 IUtilsInterface = interface
 ['{C2893208-AE15-45E1-8150-7EE603BB52A4}']
 //Stream utils
   function CreateMemoryStream: IMemoryStream; stdcall;
   function CreateFileStream(FileName: PAnsiChar;
                             Mode: LongInt): IStream64; stdcall;
   function CreateFileStreamW(FileName: PWideChar;
                             Mode: LongInt): IStream64; stdcall;
   function CreateZLibCompressor(Level: LongInt; const Dest: IReadWrite): IStream32; stdcall;
   function CreateZLibDecompressor(const Source: IReadWrite): IStream32; stdcall;
 //Strings utils
   function CreateStringList: IStringList; stdcall;
   function CreateWideStringList: IWideStringList; stdcall;
 //Color utils
   function CreateColorFormat(Fmt: PAnsiChar): IColorFormat; stdcall;
   function CreateColorFormat2(RMask, GMask, BMask, AMask: LongWord): IColorFormat; stdcall;
   function CreateColorTableList: IColorTableList; stdcall;
 //BitmapEx utils
   function CreateBitmapContainer: IBitmapContainer; stdcall;
   function CreateBitmapConverter: IBitmapConverter; stdcall;
 //TilesEx utils
   function CreateTileSetList: ITileSetList; stdcall;
 //me_lib utils
   function CreateMapList: IMapList; stdcall;
   function CreateBrushList: IBrushList; stdcall;
 //PropertyList utils
   function CreatePropertyList: IPropertyList; stdcall;
 //Ini utils
   function CreateIniFile: IIniFile; stdcall;

//   function PropertyFromPointer(Ptr: Pointer): IPropertyListItem; stdcall;
 end;

const
 mbYes      = 1 shl 0;
 mbNo       = 1 shl 1;
 mbOK       = 1 shl 2;
 mbCancel   = 1 shl 3;
 mbAbort    = 1 shl 4;
 mbRetry    = 1 shl 5;
 mbIgnore   = 1 shl 6;
 mbAll      = 1 shl 7;
 mbNoToAll  = 1 shl 8;
 mbYesToAll = 1 shl 9;
 mbHelp     = 1 shl 10;

 mrNone     = 0;
 mrOk       = idOk;
 mrCancel   = idCancel;
 mrAbort    = idAbort;
 mrRetry    = idRetry;
 mrIgnore   = idIgnore;
 mrYes      = idYes;
 mrNo       = idNo;
 mrAll      = mrNo + 1;
 mrNoToAll  = mrAll + 1;
 mrYesToAll = mrNoToAll + 1;
 
type
 TMsgDlgType = (mtWarning, mtError, mtInformation, mtConfirmation, mtCustom);
 TShowMessageProc = function(Str: PWideChar; DlgType: TMsgDlgType;
                             Buttons: LongInt): LongInt; stdcall;
 TSetProgressProc = procedure(Value: LongInt); stdcall;
 TSetStatusTextProc = procedure(Text: PWideChar); stdcall;
 TIsAbortedFunc = function: Boolean; stdcall;

 PTileSetInfo = ^TTileSetInfo;
 TTileSetInfo = packed record
  Width: Word;
  Reserved: Word;
  SelectedTile: LongInt;
  OffsetX: LongInt;
  OffsetY: LongInt;
 end;

 PTileSetInfoList = ^TTileSetInfoList;
 TTileSetInfoList = array[Word] of TTileSetInfo;

 PProjectInfo = ^TProjectInfo;
 TProjectInfo = packed record
   SelectedMapIndex:     array[0..1] of SmallInt;  // 2, 4
   SelectedBrushIndex:   array[0..1] of SmallInt;  // 6, 8
   SelectedTileSetIndex: array[0..2] of SmallInt;  // 10, 12, 14
   SelectedPaletteIndex: SmallInt;                 // 16
   SelectedTileIndex:    LongInt;                  // 20
   SelectedTileTileSet:  SmallInt;                 // 22
   EditMode:             Byte;                     // 23
   PageSetup:            Byte;                     // 24
   scMapFrame:           array[0..1] of Byte;      // 1, 2
   scBrushFrame:         array[0..1] of Byte;      // 3, 4
   scTilesFrame:         array[0..2] of Byte;      // 5, 6
   scTileFrame:          Byte;

   DrawColors: array[Boolean] of Byte;
   Reserved: Word;
  end;

 PProcessData = ^TProcessData;
 TProcessData = packed record
 // read only
  Utils: IUtilsInterface;
  IniFile: IIniFile;  
  MapList: IMapList;
  BrushList: IBrushList;
  ColorTableList: IColorTableList;
  TileSetList: ITileSetList;
  ShowMessage: TShowMessageProc;
  SetProgressMax: TSetProgressProc;
  SetProgress: TSetProgressProc;
  SetStatusText: TSetStatusTextProc;
  IsAborted: TIsAbortedFunc;
 // rewritable
  Section: PWideChar;
  ProjectInfo: PProjectInfo;
  TileSetInfo: PTileSetInfo;
  MapLayerFocus: PByte;
  BrushLayerFocus: PByte;
 end;

 PPropertyItemRec = ^TPropertyItemRec;
 TPropertyItemRec = packed record
  Owner: LongInt;
  Name: PWideChar;
  IniIdent: PWideChar;
  ReadOnly: Boolean;  
  case ValueType: TValueType of
   0: (Value: Int64;
       ValueMin: Int64;
       ValueMax: Int64);
   1: (Hex: Int64;
       HexMin: Int64;
       HexMax: Int64;
       HexDigits: Byte); 
   2: (Float: Double;
       Precision: Byte);
   3: (ValueStr: PWideChar);
   4: (StringList: PWideChar;
       PickValue: PWideChar;
       PickIndex: LongInt;
       Fixed: Boolean);
   5: (FileName: PWideChar;
       DefaultExt: PWideChar;
       Filter: PWideChar;
       Options: LongInt);
 end;

 PParametersData = ^TParametersData;
 TParametersData = packed record
 // read only
  Utils: IUtilsInterface;
  IniFile: IIniFile;
  ShowMessage: TShowMessageProc;
 // rewritable
  Section: PWideChar;
  PropertyList: PPropertyItemRec;
  PropertyCount: LongInt;
 end;

 TPlanarFmt = (pfMatrix, pfVector); 

procedure InitArrayAttr(var Def: TAttrDef; AType: TVarType; AMaxLength: Integer = 0);
procedure InitStringAttr(var Def: TAttrDef; AMaxLength: Integer = 0);
procedure InitWStringAttr(var Def: TAttrDef; AMaxLength: Integer = 0);
procedure InitIntegerAttr(var Def: TAttrDef; AMin, AMax: Int64;
                             ANumSys: Integer = 0);
procedure InitFloatAttr(var Def: TAttrDef; Dbl: Boolean;
                     APrecision: Integer = 0);

//BitmapEx utils
function Linear(BitsCount: Integer; BytesReverse: Boolean = False): Word;
function Planar(BCnt, PlaneBCnt: Integer; Fmt: TPlanarFmt = pfMatrix;
                BytesReverse: Boolean = False): Word;
function GetPlaneBitsCount(Flags: Word): Integer;
function GetImageLineSize(Width, Rmndr, Flags: Word): Integer;
function FlagsGetBitsCount(const Flags: array of Word): Integer;


implementation

uses BmpImg;

procedure InitArrayAttr(var Def: TAttrDef; AType: TVarType; AMaxLength: Integer = 0);
begin
 case AType of
  varByte:
  begin
   Def.adMin := 0;
   Def.adMax := 255;
  end;
  varShortInt:
  begin
   Def.adMin := -128;
   Def.adMax := 127;
  end;
  varWord:
  begin
   Def.adMin := 0;
   Def.adMax := 65535;
  end;
  varSmallInt:
  begin
   Def.adMin := -32768;
   Def.adMax := 32767;
  end;
  varInteger:
  begin
   Def.adMin := Low(LongInt);
   Def.adMax := High(LongInt);
  end;
  varLongWord:
  begin
   Def.adMin := 0;
   Def.adMax := High(LongWord);
  end;
  varInt64:
  begin
   AType := varVariant;
   Def.adMin := Low(Int64);
   Def.adMax := High(Int64);
  end;
  varSingle: Def.adPrecision := 7;
  varDouble: Def.adPrecision := 15;
  varCurrency: Def.adPrecision := 2;
  varString,
  varOleStr: Def.adMaxStrLen := 0;
 end;
 Def.adType := varArray or AType;
 Def.adMaxArrayLen := AMaxLength; 
end;

procedure InitStringAttr(var Def: TAttrDef; AMaxLength: Integer = 0);
begin
 Def.adType := varString;
 Def.adMaxStrLen := AMaxLength;
end;

procedure InitWStringAttr(var Def: TAttrDef; AMaxLength: Integer = 0);
begin
 Def.adType := varOleStr;
 Def.adMaxStrLen := AMaxLength;
end;

procedure InitIntegerAttr(var Def: TAttrDef; AMin, AMax: Int64; ANumSys: Integer);
begin
 if AMin = AMax then
 begin
  Def.adMin := Low(Int64);
  Def.adMax := High(Int64);
 end else
 begin
  if (AMin >= Low(Byte)) and (AMax <= High(Byte)) then
   Def.adType := varByte else
  if (AMin >= Low(Word)) and (AMax <= High(Word)) then
   Def.adType := varWord else
  if (AMin >= Low(LongWord)) and (AMax <= High(LongWord)) then
   Def.adType := varLongWord else
  if (AMin >= Low(ShortInt)) and (AMax <= High(ShortInt)) then
   Def.adType := varShortInt else
  if (AMin >= Low(SmallInt)) and (AMax <= High(SmallInt)) then
   Def.adType := varSmallInt else
  if (AMin >= Low(LongInt)) and (AMax <= High(LongInt)) then
   Def.adType := varInteger else
   Def.adType := varInt64;
  Def.adMin := AMin;
  Def.adMax := AMax;
 end;
 Def.adNumSys := ANumSys;
end;

procedure InitFloatAttr(var Def: TAttrDef; Dbl: Boolean;
                     APrecision: Integer = 0);
begin
 if APrecision <= 0 then APrecision := 15;
 if APrecision < 2 then APrecision := 2;
 if Dbl then
 begin
  Def.adType := varDouble;
  if APrecision >= 15 then
   Def.adPrecision := 15 else
   Def.adPrecision := APrecision;
 end else
 begin
  Def.adType := varSingle;
  if APrecision >= 7 then
   Def.adPrecision := 7 else
   Def.adPrecision := APrecision;
 end;
end;


function Linear(BitsCount: Integer; BytesReverse: Boolean): Word;
begin
 Result := (Abs(BitsCount) - 1) and 31;
 if BitsCount < 0 then
  Result := Result or IMG_BITS_REVERSE or IMG_PIXEL_BSWAP;
 if BytesReverse then
  Result := Result or IMG_BYTES_REVERSE;
end;

function Planar(BCnt, PlaneBCnt: Integer;
                Fmt: TPlanarFmt; BytesReverse: Boolean): Word;
begin
 Result := ((Abs(BCnt) - 1) and 31) or IMG_PLANAR;
 if PlaneBCnt < 0 then
  Result := Result or IMG_BITS_REVERSE;
 if Fmt <> pfMatrix then
  Result := Result or IMG_PLANE_VECTOR;
 if BCnt < 0 then
  Result := Result or IMG_PLANE_REVERSE;
 case Abs(PlaneBCnt) of
  1:   Result := Result or IMG_PLANE_1BIT;
  2:   Result := Result or IMG_PLANE_2BIT;
  3,4: Result := Result or IMG_PLANE_4BIT;
  else Result := Result or IMG_PLANE_8BIT;
 end;
 if BytesReverse then
  Result := Result or IMG_BYTES_REVERSE;
end;

function GetPlaneBitsCount(Flags: Word): Integer;
begin
 Result := 1 shl (Flags shr PLANE_FORMAT_SHIFT);
end;

function GetImageLineSize(Width, Rmndr, Flags: Word): Integer;
var
 BCnt: Integer;
begin
 BCnt := (Flags and 31) + 1;
 if Flags and IMG_PLANAR <> 0 then
  Result := GetPlanarLineSizeRmndr(Width, Rmndr, GetPlaneBitsCount(Flags), BCnt) else
  Result := BmpImg.GetLineSize(Width, BCnt) + Rmndr;
end;

function FlagsGetBitsCount(const Flags: array of Word): Integer;
var
 I: Integer;
begin
 Result := 0;
 for I := 0 to Length(Flags) - 1 do
  Inc(Result, (Flags[I] and 31) + 1);
end;


end.
