unit me_palfrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, BitmapEx, ExtCtrls, TileMapView, TntStdCtrls,
  ComCtrls, TntComCtrls;

type
  TPaletteFrame = class(TFrame)
    PaletteView: TTileMapView;
    PaletteStatusBar: TTntStatusBar;
    procedure PaletteViewDrawCell(Sender: TCustomTileMapView;
      const Rect: TRect; CellX, CellY: Integer);
    procedure PaletteViewMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure PaletteViewClearCell(Sender: TCustomTileMapView; CellX,
      CellY: Integer);
    procedure PaletteViewCheckCompatibility(Sender: TCustomTileMapView;
      Data: Pointer; var Result: Boolean);
    procedure PaletteViewBufClearCell(Sender: TCustomTileMapView; CellX,
      CellY: Integer; Data: Pointer);
    procedure PaletteViewGetBufCellState(Sender: TCustomTileMapView;
      Data: Pointer; CellX, CellY: Integer; var Dirty: Boolean);
    procedure PaletteViewGetCellState(Sender: TCustomTileMapView; CellX,
      CellY: Integer; var Dirty: Boolean);
    procedure PaletteViewMakePasteBufferCopy(Sender: TCustomTileMapView;
      Source: Pointer; var Dest: Pointer);
    procedure PaletteViewPasteBufferApply(Sender: TCustomTileMapView;
      CellX, CellY: Integer; Data: Pointer);
    procedure PaletteViewReleasePasteBuffer(Sender: TCustomTileMapView;
      Data: Pointer);
    procedure PaletteViewCopyMapData(Sender: TCustomTileMapView;
      var Data: Pointer; const Rect: TRect; SelectionBuffer: Pointer);
  private
    FColorTable: TColorTable;
    FIndexUnderCursor: Integer;
    FColorRangeStart: Integer;
    FColorRangeEnd: Integer;
    function GetColor(X: Integer): TColor;
    procedure SetColor(X: Integer; Value: TColor);
    procedure SetColorHeight(Value: Integer);
    procedure SetColorsCount(Value: Integer);
    procedure SetColorWidth(Value: Integer);
    function GetColorWidth: Integer;
    function GetColorHeight: Integer;
    function GetColorsOnLine: Integer;
    procedure SetColorsOnLine(Value: Integer);
    function GetColorsCount: Integer;
    procedure CMMouseLeave(var Message: TMessage); message CM_MOUSELEAVE;
    function GetRGBQuad(X: Integer): LongWord;
    procedure SetRGBQuad(X: Integer; Value: LongWord);
    procedure SetColorTable(AColorTable: TColorTable);
    function GetFormatString: AnsiString;
    procedure SetFormatString(const Value: AnsiString);
  public
    property ColorFormatString: AnsiString read GetFormatString write SetFormatString;

    property IndexUnderCursor: Integer read FIndexUnderCursor;
    property ColorWidth: Integer read GetColorWidth write SetColorWidth;
    property ColorHeight: Integer read GetColorHeight write SetColorHeight;
    property ColorsCount: Integer read GetColorsCount write SetColorsCount;
    property ColorsOnLine: Integer read GetColorsOnLine write SetColorsOnLine;
    property Colors[X: Integer]: TColor read GetColor write SetColor;
    property RGBQuads[X: Integer]: LongWord read GetRGBQuad write SetRGBQuad;
    property ColorTable: TColorTable read FColorTable write SetColorTable;

    procedure SelectColorRange(Index, Count: Integer);
    procedure UpdateStatusBar;
    procedure UpdateColorIndex;
    procedure ColorTableModified;

    constructor Create(AOwner: TComponent); override;
  end;

  TPaletteBuffer = class
  public
   Width: LongInt;
   Height: LongInt;
   Data: array of LongWord;
   Flags: array of Boolean;
   constructor Create(AWidth, AHeight: Integer);
   procedure SaveToStream(Stream: TStream);
   function LoadFromStream(Stream: TStream): Boolean;
  end;

const
 PB_SIGN: LongWord = $56AA99AB;

var 
 CF_PALETTEDATA: Word;

implementation

{$R *.dfm}

uses HexUnit, MyClasses;

{ TPaletteBuffer }

constructor TPaletteBuffer.Create(AWidth, AHeight: Integer);
var
 Size: Integer;
begin
 Width := AWidth;
 Height := AHeight;
 Size := Width * Height;
 SetLength(Data, Size);
 SetLength(Flags, Size);
end;

function TPaletteBuffer.LoadFromStream(Stream: TStream): Boolean;
var
 Sign: LongWord;
 Size: Integer;
 W, H: LongInt;
begin
 Result := False;
 if (Stream.Read(Sign, 4) = 4) and
    (Sign = PB_SIGN) and
    (Stream.Read(W, 4) = 4) and
    (Stream.Read(H, 4) = 4) and
    (W >= 1) and
    (H >= 1) and
    (W <= 256) and
    (H <= 256) then
 begin
  Width := W;
  Height := H;
  Size := Width * Height;
  SetLength(Data, Size);
  SetLength(Flags, Size);
  Result := (Stream.Read(Data[0], Size * 4) = Size * 4) and
            (Stream.Read(Flags[0], Size) = Size);
 end;
end;

procedure TPaletteBuffer.SaveToStream(Stream: TStream);
var
 Size: Integer;
begin
 Stream.WriteBuffer(PB_SIGN, 4);
 Stream.WriteBuffer(Width, 4);
 Stream.WriteBuffer(Height, 4);
 Size := Width * Height;
 Stream.WriteBuffer(Data[0], Size * 4);
 Stream.WriteBuffer(Flags[0], Size);
end;

{ TPaletteFrame }

function TPaletteFrame.GetColor(X: Integer): TColor;
begin
 Result := FColorTable.RGBZ[X];
end;

function TPaletteFrame.GetColorHeight: Integer;
begin
 Result := PaletteView.TileHeight;
end;

function TPaletteFrame.GetColorsOnLine: Integer;
begin
 Result := PaletteView.MapWidth;
end;

function TPaletteFrame.GetColorWidth: Integer;
begin
 Result := PaletteView.TileWidth;
end;

procedure TPaletteFrame.PaletteViewDrawCell(Sender: TCustomTileMapView;
  const Rect: TRect; CellX, CellY: Integer);
var
 Color: TColor;
 Offset: Integer;
 NewRect: TRect;
begin
 with Sender, Canvas do
 begin
  if InternalIndex >= FColorTable.ColorsCount then
  begin
   Brush.Style := bsDiagCross;
   Color := clWhite;
  end else
  begin
   Brush.Style := bsSolid;
   Color := Colors[InternalIndex];
   if TObject(PasteBuffer) is TPaletteBuffer then
   begin
    Dec(CellX, PasteBufRect.Left);
    Dec(CellY, PasteBufRect.Top);
    with TPaletteBuffer(PasteBuffer) do
    if (CellX >= 0) and
       (CellY >= 0) and
       (CellX < Width) and
       (CellY < Height) then
    begin
     Offset := CellY * Width + CellX;
     if Flags[Offset] then
      Color := RGBZ_ColorFormat.FromRGBQuad(Data[Offset]);
    end;
   end;
  end;
  Brush.Color := Color;
  FillRect(Rect);

  if (InternalIndex >= FColorRangeStart) and
     (InternalIndex <= FColorRangeEnd) then
  begin
   NewRect := Rect;
   Inc(NewRect.Left);
   Inc(NewRect.Top);
   Pen.Style := psSolid;
   Pen.Mode := pmMergePenNot;
   Pen.Width := 1;
   Pen.Color := clGray;
   Brush.Style := bsClear;
   Rectangle(NewRect);
  end;
 end;
end;

procedure TPaletteFrame.SetColor(X: Integer; Value: TColor);
begin
 FColorTable.RGBZ[X] := Value;
 PaletteView.Invalidate;
end;

procedure TPaletteFrame.SetColorHeight(Value: Integer);
begin
 PaletteView.TileHeight := Value;
end;

function TPaletteFrame.GetColorsCount: Integer;
begin
 if FColorTable <> nil then
  Result := FColorTable.ColorsCount else
  Result := 0;
end;

procedure TPaletteFrame.SetColorsCount(Value: Integer);
var
 W, H: Integer;
begin
 PaletteView.Deselect;
 if FColorTable <> nil then
  FColorTable.ColorsCount := Value;
 if Value < 16 then
  W := Value else
  W := 16;
 if W > 0 then
  H := (Value + W - 1) div W else
  H := 0;

 PaletteView.SetMapSize(W, H);
 UpdateStatusBar; 
end;

procedure TPaletteFrame.SetColorsOnLine(Value: Integer);
begin
 PaletteView.MapWidth := Value;
end;

procedure TPaletteFrame.SetColorTable(AColorTable: TColorTable);
begin
 if AColorTable <> FColorTable then
 begin                               
  PaletteView.Deselect;
  FColorTable := AColorTable;
  if FColorTable <> nil then
   SetColorsCount(FColorTable.ColorsCount) else
   SetColorsCount(0);
  PaletteView.Invalidate;
 end;
end;

procedure TPaletteFrame.SetColorWidth(Value: Integer);
begin
 PaletteView.TileWidth := Value;
end;

(*
constructor TPaletteFrame.Create(AOwner: TComponent);
begin
 inherited;
 FCanvas := TControlCanvas.Create;
 FCanvas.Control := Self;
 FColorFormat := TColorFormat.Create;
 FColorsCount := 0;
end;

destructor TPaletteFrame.Destroy;
begin
 FColorFormat.Free;
 FCanvas.Free; 
 inherited;
end;

procedure TPaletteFrame.FrameResize(Sender: TObject);
begin
 RefreshScrollBar;
 Redraw;
end;

function TPaletteFrame.GetColor(X: Integer): TColor;
begin
 if (FColorTable <> nil) and (X >= 0) and (X < FColorsCount) then
  Result := RGBZ_ColorFormat.FromRGBQuad(FColorFormat.ToRGBQuad(
       Pointer(Integer(FColorTable) + X * FColorFormat.ColorSize)^)) else
  Result := 0;
end;

procedure TPaletteFrame.ScrollBarScroll(Sender: TObject;
  ScrollCode: TScrollCode; var ScrollPos: Integer);
begin
 FTopLine := ScrollPos;
 Redraw;
end;

procedure TPaletteFrame.Redraw;
var
 I, X, Y, W, Rows: Integer;
begin
 with FCanvas do
 begin
  Brush.Color := Self.Color;
  Brush.Style := bsSolid;
  FillRect(ClipRect);
 end;
 if FLines <= 0 then Exit;
 Rows := Min(CanvasHeight div FColorHeight, FLines);
 W := CanvasWidth div FColorWidth;
 with FCanvas do
 begin
  Pen.Width := 1;
  Pen.Mode := pmCopy;
  Pen.Style := psSolid;
  Pen.Color := clBlack;
  I := FTopLine * W;
  for Y := 0 to Rows - 1 do
  begin
   for X := 0 to W - 1 do
   if I < FColorsCount then
   begin
    Brush.Color := Colors[I];
    Rectangle(Bounds(X * FColorWidth, Y * FColorHeight, FColorWidth, FColorHeight));
    Inc(I);
   end else Exit;
  end;
 end;
end;

procedure TPaletteFrame.SetColor(X: Integer; Value: TColor);
begin
 if (FColorTable <> nil) and (X >= 0) and (X < FColorsCount) then
 begin
  FColorFormat.SetColor(FColorTable, X, RGBZ_ColorFormat.ValueToRGBQuad(Color));
  PaletteView.Invalidate;
 end;
end;

procedure TPaletteFrame.SetColorHeight(Value: Integer);
begin
 PaletteView.TileHeight := Value;
end;

procedure TPaletteFrame.SetColorsCount(Value: Integer);
begin
 if FColorsCount <> Value then
 begin
  if Value < 0 then Value := 0; 
  FColorsCount := Value;
  Refresh(FTopLine);
 end;
end;

procedure TPaletteFrame.SetColorWidth(Value: Integer);
begin
 if FColorWidth <> Value then
 begin
  if Value < 1 then Value := 1;
  FColorWidth := Value;
  Refresh(FTopLine);
 end;
end;

procedure TPaletteFrame.SetPalette(ColorFmt: TColorFormat; CTbl: Pointer; Cnt: Integer);
begin
 FColorFormat.Assign(ColorFmt);
 FColorTable := CTbl;
 FColorsCount := Cnt; 
 Refresh;
end;

function TPaletteFrame.GetColorWidth: Integer;
begin

end;

procedure TPaletteFrame.PaletteViewDrawCell(Sender: TCustomTileMapView;
  const Rect: TRect; CellX, CellY: Integer);
begin
//
end;

function TPaletteFrame.GetCanvasHeight: Integer;
begin

end;

function TPaletteFrame.GetCanvasWidth: Integer;
begin

end;

function TPaletteFrame.GetColorHeight: Integer;
begin

end;

function TPaletteFrame.GetColorsOnLine: Integer;
begin
 Result := PaletteView.MapWidth;
end;

procedure TPaletteFrame.SetColorsOnLine(Value: Integer);
begin
 PaletteView.MapWidth := Value;
end;

*)

procedure TPaletteFrame.UpdateStatusBar;
begin
 if FColorTable <> nil then
 begin
  with FColorTable do
   PaletteStatusBar.Panels[1].Text := WideFormat('%d colors', [ColorsCount]);
 end else
  PaletteStatusBar.Panels[1].Text := '';
 UpdateColorIndex;   
end;

procedure TPaletteFrame.PaletteViewMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
 UpdateColorIndex;
end;

procedure TPaletteFrame.UpdateColorIndex;
begin
 FIndexUnderCursor := -1;
 if FColorTable <> nil then
 begin
  with PaletteView, FColorTable do
   if (TileX >= 0) and (TileX < MapWidth) and
      (TileY >= 0) and (TileY < MapHeight) then
    FIndexUnderCursor := TileY * MapWidth + TileX;
  if (FIndexUnderCursor >= 0) and (FIndexUnderCursor < FColorTable.ColorsCount) then
  begin
   PaletteStatusBar.Panels[2].Text := WideFormat('Index: %d', [FIndexUnderCursor]);
   Exit;
  end;
 end;
 PaletteStatusBar.Panels[2].Text := '';
end;

procedure TPaletteFrame.CMMouseLeave(var Message: TMessage);
begin
 PaletteStatusBar.Panels[2].Text := '';
end;

procedure TPaletteFrame.SelectColorRange(Index, Count: Integer);
{var
 I: Integer;
 Temp: array of Boolean;
 Rect: TRect;}
begin
 FColorRangeStart := Index;
 FColorRangeEnd := Index + (Count - 1);
 if (Index >= 0) and (Count > 0) then
  PaletteView.ScrollTo(Index and 15, Index shr 4) else
  PaletteView.Invalidate;

{ if FColorTable <> nil then
 begin
  PaletteView.Deselect;
  Rect.Left := 0;
  Rect.Right := PaletteView.MapWidth;
  I := Index and 255;
  Inc(Index, Count);
  if Index <= 256 then
  begin
   Rect.Top := I div PaletteView.MapWidth;
   Rect.Bottom := Rect.Top +
   (Count + PaletteView.MapWidth - 1) div PaletteView.MapWidth +
               Ord((I + Count) mod PaletteView.MapWidth > 0);
  end else
  begin
   Rect.Top := 0;
   Rect.Bottom := 256;
  end;
  SetLength(Temp, (Rect.Right - Rect.Left) *
                  (Rect.Bottom - Rect.Top));
  if Index > 256 then
  begin
   for I := I to I + Count - 1 do
    Temp[I and 255] := True;
  end else
   FillChar(Temp[I mod PaletteView.MapWidth], Count, 1);

  PaletteView.ApplySelection(Temp, Rect, True);
 end;  }
end;

procedure TPaletteFrame.PaletteViewClearCell(Sender: TCustomTileMapView;
  CellX, CellY: Integer);
begin
 RGBQuads[CellY * ColorsOnLine + CellX] := 0;
end;

procedure TPaletteFrame.PaletteViewCheckCompatibility(
  Sender: TCustomTileMapView; Data: Pointer; var Result: Boolean);
begin
 Result := TObject(Data) is TPaletteBuffer;
end;

procedure TPaletteFrame.PaletteViewBufClearCell(Sender: TCustomTileMapView;
  CellX, CellY: Integer; Data: Pointer);
begin
 with TObject(Data) as TPaletteBuffer do
  Flags[CellY * Width + CellX] := False;
end;

procedure TPaletteFrame.PaletteViewGetBufCellState(
  Sender: TCustomTileMapView; Data: Pointer; CellX, CellY: Integer;
  var Dirty: Boolean);
begin
 with TObject(Data) as TPaletteBuffer do
  Dirty := Flags[CellY * Width + CellX];
end;

procedure TPaletteFrame.PaletteViewGetCellState(Sender: TCustomTileMapView;
  CellX, CellY: Integer; var Dirty: Boolean);
begin
 Dirty := CellY * Sender.MapWidth + CellX < ColorsCount;
end;

procedure TPaletteFrame.PaletteViewMakePasteBufferCopy(
  Sender: TCustomTileMapView; Source: Pointer; var Dest: Pointer);
var
 Src: TPaletteBuffer absolute Source;
 Dst: TPaletteBuffer;
 Size: Integer;
begin
 if TObject(Source) is TPaletteBuffer then
 begin
  Dst := TPaletteBuffer.Create(Src.Width, Src.Height);
  try
   Size := Src.Width * Src.Height;
   Move(Src.Data[0], Dst.Data[0], Size * 4);
   Move(Src.Flags[0], Dst.Flags[0], Size);
   Dest := Pointer(Dst);
  except
   Dst.Free;
   raise;
  end;
 end;
end;

procedure TPaletteFrame.PaletteViewPasteBufferApply(
  Sender: TCustomTileMapView; CellX, CellY: Integer; Data: Pointer);
var
 X, Y, SrcStride, DstStride, W: Integer;
 Src: PLongWord;
 Flg: PBoolean;
 Dst: PByte;
begin
 if (FColorTable <> nil) and (FColorTable.ColorData <> nil) then
  with FColorTable.ColorFormat, TObject(Data) as TPaletteBuffer do
  begin
   SrcStride := Width;
   DstStride := PaletteView.MapWidth * ColorSize;
   W := Width;

   Src := Pointer(Data);
   Flg := Pointer(Flags);
   Dst := FColorTable.ColorData;
   if CellX < 0 then
   begin
    Dec(Src, CellX);
    Dec(Flg, CellX);
    Inc(W, CellX);
   end else
    Inc(Dst, CellX * ColorSize);

   X := CellX + Width;
   if X > PaletteView.MapWidth then
    Dec(W, X - PaletteView.MapWidth);

   if W > 0 then
   begin
    if CellY < 0 then
    begin
     Y := CellY * Width;
     Dec(Src, Y);
     Dec(Flg, Y);
    end;

    Y := Max(0, CellY);
    Inc(Dst, Y * DstStride);
    Dec(DstStride, W * ColorSize);
    Dec(SrcStride, W);

    for Y := Y to Min(PaletteView.MapHeight, CellY + Height) - 1 do
    begin
     for X := 0 to W - 1 do
     begin
      if Flg^ then
       SetColor(Dst^, Src^);
      Inc(Src);
      Inc(Flg);
      Inc(Dst, ColorSize);
     end;
     Inc(Src, SrcStride);
     Inc(Flg, SrcStride);
     Inc(Dst, DstStride);
    end;
   end;
  end; 
end;

procedure TPaletteFrame.PaletteViewReleasePasteBuffer(
  Sender: TCustomTileMapView; Data: Pointer);
begin
 TObject(Data).Free;
end;

function TPaletteFrame.GetRGBQuad(X: Integer): LongWord;
begin
 Result := FColorTable.BGRA[X];
end;

procedure TPaletteFrame.SetRGBQuad(X: Integer; Value: LongWord);
begin
 FColorTable.BGRA[X] := Value;
end;

procedure TPaletteFrame.PaletteViewCopyMapData(Sender: TCustomTileMapView;
  var Data: Pointer; const Rect: TRect; SelectionBuffer: Pointer);
var
 PalBuf: TPaletteBuffer;
 Src: PByte;
 X, Y: Integer;
 RowStride, BigStride: Integer;
 SB, Flg: PBoolean;
 Dst: PLongWord;
begin
 if (FColorTable <> nil) and (FColorTable.ColorData <> nil) then
  with FColorTable.ColorFormat do
  begin
   PalBuf := TPaletteBuffer.Create(Rect.Right - Rect.Left, Rect.Bottom - Rect.Top);
   try
    X := Rect.Top * PaletteView.MapWidth + Rect.Left;
    Src := Addr(PByteArray(FColorTable.ColorData)[X * ColorSize]);
    SB := Addr(PByteArray(SelectionBuffer)[X]);
    RowStride := PaletteView.MapWidth - PalBuf.Width;
    BigStride := RowStride * ColorSize;

    Dst := Pointer(PalBuf.Data);
    Flg := Pointer(PalBuf.Flags);

    for Y := 0 to PalBuf.Height - 1 do
    begin
     for X := 0 to PalBuf.Width - 1 do
     begin
      Flg^ := SB^;
      if Flg^ then
       Dst^ := ToRGBQuad(Src^) else
       Dst^ := 0;
      Inc(Src, ColorSize);
      Inc(SB);
      Inc(Flg);
      Inc(Dst);
     end;
     Inc(SB, RowStride);
     Inc(Src, BigStride);
    end;
    Data := Pointer(PalBuf);
   except
    PalBuf.Free;
    raise;
   end;
  end;
end;

function TPaletteFrame.GetFormatString: AnsiString;
begin
 if FColorTable <> nil then
  Result := FColorTable.ColorFormat.FormatString else
  Result := '';
end;

procedure TPaletteFrame.SetFormatString(const Value: AnsiString);
begin
 if FColorTable <> nil then
  FColorTable.ColorFormatString := Value;
end;

constructor TPaletteFrame.Create(AOwner: TComponent);
begin
 inherited;
 FColorRangeEnd := -1;
end;

procedure TPaletteFrame.ColorTableModified;
var
 ct: TColorTable;
begin
 ct := FColorTable;
 FColorTable := nil;
 SetColorTable(ct);
end;

initialization
 CF_PALETTEDATA := RegisterClipboardFormat('Kusharami Color Table Data');
end.
