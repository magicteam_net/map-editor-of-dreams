object LoadTilesDialog: TLoadTilesDialog
  Left = 330
  Top = 691
  BorderStyle = bsDialog
  Caption = 'Load tiles'
  ClientHeight = 186
  ClientWidth = 301
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  object L1: TTntLabel
    Left = 8
    Top = 8
    Width = 142
    Height = 13
    Caption = 'File with tiles in custom format:'
  end
  object CustomTilesFileButton: TTntSpeedButton
    Left = 272
    Top = 24
    Width = 23
    Height = 22
    Caption = '...'
    OnClick = BrowseButtonClick
  end
  object L2: TTntLabel
    Left = 8
    Top = 48
    Width = 179
    Height = 13
    Caption = 'File with color information for each tile:'
  end
  object ColorInfoFileButton: TTntSpeedButton
    Tag = 1
    Left = 272
    Top = 64
    Width = 23
    Height = 22
    Caption = '...'
    OnClick = BrowseButtonClick
  end
  object TilesCountLabel: TTntLabel
    Left = 160
    Top = 96
    Width = 86
    Height = 13
    Caption = 'Tiles count (0: all):'
  end
  object OKBtn: TTntButton
    Left = 8
    Top = 152
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 0
  end
  object CancelBtn: TTntButton
    Left = 216
    Top = 152
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 1
  end
  object CustomTilesFileEdit: TTntEdit
    Left = 8
    Top = 24
    Width = 257
    Height = 21
    TabOrder = 2
  end
  object ColorInfoFileEdit: TTntEdit
    Left = 8
    Top = 64
    Width = 257
    Height = 21
    TabOrder = 3
  end
  object LoadingModeGroup: TTntRadioGroup
    Left = 8
    Top = 88
    Width = 129
    Height = 57
    Caption = 'Loading mode'
    ItemIndex = 0
    Items.Strings = (
      'Add tiles to the end'
      'Replace existing tiles')
    TabOrder = 4
  end
  object TilesCountEdit: TSpinEdit
    Left = 160
    Top = 112
    Width = 89
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 5
    Value = 0
  end
  object OpenDialog: TTntOpenDialog
    Filter = 'All files (*.*)|*.*'
    Left = 128
    Top = 152
  end
end
