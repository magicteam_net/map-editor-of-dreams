unit me_process;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, TntForms, StdCtrls, TntStdCtrls, ComCtrls, TntDialogs, Gauges;

type
  TProcessDialog = class(TTntForm)
    CancelButton: TTntButton;
    StatusText: TTntLabel;
    ProgressBar: TGauge;
    procedure TntFormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure CancelButtonClick(Sender: TObject);
    procedure TntFormCreate(Sender: TObject);
    procedure TntFormClose(Sender: TObject; var Action: TCloseAction);
  private
    FCanceled: Boolean;
    FFinished: Boolean;
    procedure SetFinished(Value: Boolean);
  public
    property Canceled: Boolean read FCanceled;
    property Finished: Boolean read FFinished write SetFinished;
  end;

procedure PrcDlg_SetProgressMax(Value: LongInt); stdcall;
procedure PrcDlg_SetProgress(Value: LongInt); stdcall;
procedure PrcDlg_SetStatusText(Text: PWideChar); stdcall;
function  PrcDlg_IsAborted: LongBool; stdcall;

function ProcessDialogExecute: Boolean;

var
 ProcessDialog: TProcessDialog;

implementation

{$R *.dfm}

function ProcessDialogExecute: Boolean;
begin
 try
  Application.CreateForm(TProcessDialog, ProcessDialog);

  Result := ProcessDialog.ShowModal = mrOk;
 finally
  FreeAndNil(ProcessDialog);
 end;
end;

procedure PrcDlg_SetProgressMax(Value: LongInt); stdcall;
begin
 ProcessDialog.ProgressBar.MaxValue := Value;
end;

procedure PrcDlg_SetProgress(Value: LongInt); stdcall;
begin
 ProcessDialog.ProgressBar.Progress := Value;
end;

procedure PrcDlg_SetStatusText(Text: PWideChar); stdcall;
begin
 ProcessDialog.StatusText.Caption := Text;
end;

function PrcDlg_IsAborted: LongBool; stdcall;
begin
 with ProcessDialog do
  Result := not FFinished and FCanceled;
end;

procedure TProcessDialog.TntFormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
 CanClose := FFinished or
 (WideMessageDlg('Are you sure you want to cancel operation?',
  mtConfirmation, [mbYes, mbNo], 0) = mrYes);
 FCanceled := CanClose;
end;

procedure TProcessDialog.CancelButtonClick(Sender: TObject);
begin
 Close;
end;

procedure TProcessDialog.TntFormCreate(Sender: TObject);
begin
 FCanceled := False;
end;

procedure TProcessDialog.SetFinished(Value: Boolean);
begin
 FFinished := Value;
 if FFinished then
  CancelButton.Caption := 'OK' else
  CancelButton.Caption := 'Cancel';
end;

procedure TProcessDialog.TntFormClose(Sender: TObject;
  var Action: TCloseAction);
begin
 if FFinished then
  ModalResult := mrOk else
  ModalResult := mrCancel;
end;

end.
