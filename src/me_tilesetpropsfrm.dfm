object TileSetsFrame: TTileSetsFrame
  Left = 0
  Top = 0
  Width = 309
  Height = 372
  TabOrder = 0
  object PageControl: TTntPageControl
    Left = 0
    Top = 0
    Width = 309
    Height = 372
    ActivePage = PropertiesPage
    Align = alClient
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -9
    Font.Name = 'Small Fonts'
    Font.Style = []
    ParentFont = False
    TabHeight = 14
    TabOrder = 0
    TabStop = False
    OnChange = PageControlChange
    object PropertiesPage: TTntTabSheet
      Caption = 'Properties'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      object Splitter2: TSplitter
        Left = 0
        Top = 53
        Width = 301
        Height = 3
        Cursor = crVSplit
        Align = alTop
        AutoSnap = False
        Beveled = True
        ResizeStyle = rsUpdate
      end
      object PropsPanel: TPanel
        Left = 0
        Top = 56
        Width = 301
        Height = 292
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object TileSetPropLabel: TTntLabel
          Left = 0
          Top = 0
          Width = 301
          Height = 13
          Align = alTop
          Caption = 'Tile set properties'
          FocusControl = TileSetPropEditor
          Transparent = True
          Layout = tlCenter
        end
        object TileSetPropEditor: TPropertyEditor
          Tag = 3
          Left = 0
          Top = 13
          Width = 301
          Height = 257
          BoldChangedValues = False
          ButtonFillMode = fmShaded
          Align = alClient
          ScrollBarOptions.ScrollBars = ssVertical
          TabOrder = 0
          PropertyWidth = 142
          ValueWidth = 155
          OnGetPropertyName = TileSetPropEditorGetPropertyName
          OnPropertyRemove = TileSetPropEditorPropertyRemove
          OnPropertyUp = TileSetPropEditorPropertyUp
          OnPropertyDown = TileSetPropEditorPropertyDown
          Columns = <
            item
              Margin = 0
              Options = [coEnabled, coParentBidiMode, coParentColor, coResizable, coVisible, coFixed]
              Position = 0
              Width = 142
            end
            item
              Margin = 0
              Options = [coEnabled, coParentBidiMode, coParentColor, coResizable, coVisible, coFixed]
              Position = 1
              Width = 155
            end>
        end
        object AttrToolBar: TTntToolBar
          Left = 0
          Top = 270
          Width = 301
          Height = 22
          Align = alBottom
          AutoSize = True
          EdgeInner = esNone
          EdgeOuter = esNone
          Flat = True
          Images = ImageList
          TabOrder = 1
          Transparent = False
          Wrapable = False
          object PropAddToolBtn: TTntToolButton
            Left = 0
            Top = 0
            Action = PropAddAction
          end
          object sep14: TTntToolButton
            Left = 23
            Top = 0
            Width = 8
            Caption = 'sep14'
            Style = tbsSeparator
          end
          object PropDeleteToolBtn: TTntToolButton
            Left = 31
            Top = 0
            Action = PropUpAction
          end
          object PropDownToolBtn: TTntToolButton
            Left = 54
            Top = 0
            Action = PropDownAction
          end
          object PropUpToolBtn: TTntToolButton
            Left = 77
            Top = 0
            Action = PropertyDeleteAction
          end
        end
      end
      object TileSetsListBox: TListBox
        Left = 0
        Top = 23
        Width = 301
        Height = 30
        Style = lbVirtualOwnerDraw
        Align = alTop
        ItemHeight = 13
        MultiSelect = True
        TabOrder = 1
        OnClick = TileSetsListBoxClick
        OnDataFind = TileSetsListBoxDataFind
        OnDblClick = TileSetsListBoxDblClick
        OnDrawItem = TileSetsListBoxDrawItem
        OnKeyDown = TileSetsListBoxKeyDown
      end
      object TileSetsToolBar: TTntToolBar
        Left = 0
        Top = 0
        Width = 301
        Height = 23
        EdgeInner = esNone
        EdgeOuter = esNone
        Flat = True
        Images = ImageList
        TabOrder = 2
        Transparent = False
        Wrapable = False
        object TileSetNewToolBtn: TTntToolButton
          Left = 0
          Top = 0
          Action = TileSetNewAction
        end
        object sep10: TTntToolButton
          Left = 23
          Top = 0
          Width = 8
          Caption = 'sep10'
          Style = tbsSeparator
        end
        object TileSetUpToolBtn: TTntToolButton
          Left = 31
          Top = 0
          Action = TileSetUpAction
        end
        object TileSetDownToolBtn: TTntToolButton
          Left = 54
          Top = 0
          Action = TileSetDownAction
        end
        object TileSetDeleteToolBtn: TTntToolButton
          Left = 77
          Top = 0
          Action = TileSetDeleteAction
        end
        object sep999: TTntToolButton
          Left = 100
          Top = 0
          Width = 8
          Style = tbsSeparator
        end
        object TileSetLabel: TTntLabel
          Left = 108
          Top = 0
          Width = 39
          Height = 22
          Caption = 'Tile sets'
          Layout = tlCenter
        end
      end
    end
    object VisualPage: TTntTabSheet
      Caption = 'Visualization'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      OnShow = VisualPageShow
      object TileSetsComboBox: TTntComboBox
        Left = 0
        Top = 0
        Width = 301
        Height = 21
        Align = alTop
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 0
        OnChange = TileSetsComboBoxChange
        OnKeyDown = TileSetsListBoxKeyDown
      end
      inline TilesFrame: TTilesFrame
        Left = 0
        Top = 21
        Width = 301
        Height = 327
        Align = alClient
        AutoScroll = False
        Color = clBtnFace
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Small Fonts'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 1
        inherited StatusBar: TTntStatusBar
          Width = 292
        end
        inherited TileSetView: TTileMapView
          Width = 292
          Height = 302
          OnSetPosition = TilesFrameTileSetViewSetPosition
        end
      end
    end
  end
  object ActionList: TTntActionList
    Left = 200
    Top = 152
    object TileSetNewAction: TTntAction
      Category = 'Tile sets'
      Caption = '&New Tile Set'
      Hint = 'New Tile Set|Adds a new tile set to the project'
      ImageIndex = 46
      OnExecute = TileSetNewActionExecute
      OnUpdate = TileSetNewActionUpdate
    end
    object TileSetAddFromFilesAction: TTntAction
      Category = 'Tile sets'
      Caption = '&Add From...'
      Hint = 'Add From|Loads tile sets from files and adds them to the list'
      ImageIndex = 62
      OnExecute = TileSetAddFromFilesActionExecute
      OnUpdate = TileSetNewActionUpdate
    end
    object TileSetSaveToFileAction: TTntAction
      Category = 'Tile sets'
      Caption = '&Save To...'
      Hint = 'Save To|Saves all selected tile sets to disk'
      ImageIndex = 38
      OnExecute = TileSetSaveToFileActionExecute
      OnUpdate = TileSetSaveToFileActionUpdate
    end
    object TileSetUpAction: TTntAction
      Category = 'Tile sets'
      Caption = 'Move &Up'
      Hint = 'Move Up|Moves all selected tile sets up the list'
      ImageIndex = 1
      OnExecute = TileSetUpActionExecute
      OnUpdate = TileSetUpActionUpdate
    end
    object TileSetDownAction: TTntAction
      Category = 'Tile sets'
      Caption = 'Move &Down'
      Hint = 'Move Down|Moves all selected tile sets down the list'
      ImageIndex = 2
      OnExecute = TileSetDownActionExecute
      OnUpdate = TileSetDownActionUpdate
    end
    object TileSetDeleteAction: TTntAction
      Category = 'Tile sets'
      Caption = 'D&elete'
      Hint = 'Delete|Deletes all selected tile sets from the project'
      ImageIndex = 0
      OnExecute = TileSetDeleteActionExecute
      OnUpdate = TileSetDeleteActionUpdate
    end
    object PropAddAction: TTntAction
      Category = 'Properties'
      ImageIndex = 48
      OnExecute = PropAddActionExecute
      OnUpdate = PropAddActionUpdate
    end
    object PropUpAction: TTntAction
      Category = 'Properties'
      Caption = 'Move &Up'
      ImageIndex = 1
      OnExecute = PropMoveActionExecute
      OnUpdate = PropUpActionUpdate
    end
    object PropDownAction: TTntAction
      Tag = 1
      Category = 'Properties'
      Caption = 'Move &Down'
      ImageIndex = 2
      OnExecute = PropMoveActionExecute
      OnUpdate = PropDownActionUpdate
    end
    object PropertyDeleteAction: TTntAction
      Tag = 2
      Category = 'Properties'
      Caption = 'D&elete'
      ImageIndex = 0
      OnExecute = PropMoveActionExecute
      OnUpdate = PropertyDeleteActionUpdate
    end
    object TilesImportAction: TTntAction
      Category = 'Tile sets'
      Caption = 'Import Tiles...'
      Hint = 'Import Tiles|Imports tiles into selected tile set from files'
      ImageIndex = 36
      OnExecute = TilesImportActionExecute
      OnUpdate = TilesImportExportActionUpdate
    end
    object TilesExportAction: TTntAction
      Category = 'Tile sets'
      Caption = 'Export Tiles...'
      Hint = 'Export Tiles|Saves all or selected tiles to a file'
      ImageIndex = 38
      OnExecute = TilesExportActionExecute
      OnUpdate = TilesImportExportActionUpdate
    end
  end
  object ImageList: TImageList
    Left = 104
    Top = 152
  end
  object OpenTilesDialog: TTntOpenDialog
    Filter = 'Customizable Tile Sets (*.cts)|*.cts|All Files (*.*)|*.*'
    Options = [ofHideReadOnly, ofFileMustExist, ofEnableSizing]
    Title = 'Load Tiles'
    Left = 173
    Top = 109
  end
  object SaveTilesDialog: TTntSaveDialog
    OnClose = SaveTilesDialogTypeChange
    OnShow = SaveTilesDialogTypeChange
    Filter = 'Customizable Tile Sets (*.cts)|*.cts|All Files (*.*)|*.*'
    Options = [ofOverwritePrompt, ofHideReadOnly, ofEnableSizing]
    Title = 'Save Tiles'
    OnTypeChange = SaveTilesDialogTypeChange
    Left = 99
    Top = 105
  end
end
