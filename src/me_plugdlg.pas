unit me_plugdlg;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, HexUnit,
  Forms, Dialogs, TntForms, StdCtrls, TntStdCtrls, VirtualTrees, TntClasses,
  TntSysUtils, ExtCtrls, PropEditor, PropContainer, MyClassesEx, me_project,
  Buttons, TntFileCtrl, me_plugutils, me_plugin, me_plugadapters, TntDialogs;

const
 WM_FILLPLUGPROPS = WM_USER + 1375;

type
  TPluginDialogMode = (pmLoad, pmSave);

  TPluginDialog = class(TTntForm)
    Bevel: TBevel;
    PlgListBox: TListBox;
    PropEditor: TPropertyEditor;
    OkButton: TTntButton;
    CancelButton: TTntButton;
    DescriptionMemo: TTntMemo;
    RefreshButton: TSpeedButton;
    OpenDialog: TTntOpenDialog;
    SaveDialog: TTntSaveDialog;
    procedure PlgListBoxClick(Sender: TObject);
    function PlgListBoxDataFind(Control: TWinControl;
      FindString: String): Integer;
    procedure PlgListBoxDrawItem(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    procedure TntFormCreate(Sender: TObject);
    procedure TntFormDestroy(Sender: TObject);
    procedure RefreshButtonClick(Sender: TObject);
    procedure PropEditorEllipsisClick(Sender: TCustomPropertyEditor;
      Node: PVirtualNode; var Accepted: Boolean);
    procedure ConfigValueChange(Sender: TPropertyListItem);
    procedure PlugValueChange(Sender: TPropertyListItem);    
  private
    FMode: TPluginDialogMode;
    FPlugins: TPluginList;
    FPath: WideString;
    FIniPath: WideString;
    FIniFile: TStreamIniFileW;
    FProject: TMapEditorProject;
    FConfigIndex: Integer;
    FConfigSection: WideString;
    procedure SetMode(Value: TPluginDialogMode);
    function GetSelectedPluginName: WideString;
    procedure WMFillPlugProps(var Message: TMessage); message WM_FILLPLUGPROPS;
  public
    property Mode: TPluginDialogMode read FMode write SetMode;
    property SelectedPluginName: WideString read GetSelectedPluginName;

    procedure UpdatePlugins(const SelectedPlugin: WideString);
    procedure FillPropertyList(Source: PPropertyItemRec; SourceCount: Integer);    
  end;

function PluginDialogExecute(Project: TMapEditorProject;
                             AMode: TPluginDialogMode;
                             const Path, IniPath: WideString;
                             var SelectedPlugin: WideString): Boolean;
function PlugShowMessage(Str: PWideChar; DlgType: TMsgDlgType;
             Buttons: LongInt): LongInt; stdcall;

var
 PluginDialog: TPluginDialog;

const
 LSStr: array[TPluginDialogMode] of WideString = ('Loader', 'Saver');
 SActiveSection: WideString = 'ActiveSection';

implementation

uses
 MyUtils;

{$R *.DFM}

function PlugShowMessage(Str: PWideChar; DlgType: TMsgDlgType;
             Buttons: LongInt): LongInt; stdcall;
var
 Btn: TMsgDlgButtons absolute Buttons;
begin
 Result := WideMessageDlg(Str, Dialogs.TMsgDlgType(DlgType), Btn, 0);
end;

function PluginDialogExecute(Project: TMapEditorProject;
                             AMode: TPluginDialogMode;
                             const Path, IniPath: WideString;
                             var SelectedPlugin: WideString): Boolean;
begin
 try
  Application.CreateForm(TPluginDialog, PluginDialog);

  PluginDialog.FProject := Project;
  PluginDialog.Mode := AMode;
  PluginDialog.FPath := WideIncludeTrailingPathDelimiter(Path);
  PluginDialog.FIniPath := WideIncludeTrailingPathDelimiter(IniPath);
  PluginDialog.UpdatePlugins(SelectedPlugin);

  Result := PluginDialog.ShowModal = mrOk;
  SelectedPlugin := PluginDialog.SelectedPluginName;
 finally
  FreeAndNil(PluginDialog);
 end;
end;

function NewProperty(List: TPropertyList; Source: PPropertyItemRec): TPropertyListItem;
var
 Temp: TOpenOptions;
 I: TOpenOption;
begin
 with Source^ do
 begin
  case ValueType of
   vtDecimal:
    Result := List.AddDecimal(Name, Value, ValueMin, ValueMax);
   vtHexadecimal:
    Result := List.AddHexadecimal(Name, Hex, HexMin, HexMax, HexDigits);
   vtString:
    Result := List.AddString(Name, ValueStr);
   vtPickString:
    if PickIndex < 0 then
     Result := List.AddPickList(Name, PickValue, StringList, Fixed) else
     Result := List.AddPickList(Name, StringList, PickIndex, Fixed);
   vtFloat:
    Result := List.AddFloat(Name, Float, Precision);
   vtFilePickLoad,
   vtFilePickSave:
   begin
    Temp := [];
    for I := Low(TOpenOption) to High(TOpenOption) do
    begin
     if Options and 1 <> 0 then Include(Temp, I);
     Options := Options shr 1;
    end;
    Result := List.AddFilePick(Name, FileName, Filter, DefaultExt, '',
                             ValueType = vtFilePickSave, Temp);
   end;
   vtDirectoryPick:
   begin
    Result := List.AddString(Name, ValueStr, True);
    Result.Parameters := PL_BUFFER or 8;
   end;
   else
    Result := nil;
  end;
  if Result <> nil then
  begin
   Result.ReadOnly := ReadOnly;
   Result.UserTag := Integer(IniIdent);
  end;
 end;
end;

procedure TPluginDialog.FillPropertyList(Source: PPropertyItemRec;
  SourceCount: Integer);
var
 I: Integer;
 Item: TPropertyListItem;
 Ref: array of TPropertyListItem;
begin
 SetLength(Ref, SourceCount);

 for I := 0 to SourceCount - 1 do
 begin
  if Source.Owner < 0 then
   Item := NewProperty(PropEditor.PropertyList, Source) else
  if Source.Owner < I then
   Item := NewProperty(Ref[Source.Owner].SubProperties, Source) else
   Item := nil;
  if Item <> nil then
  begin
   Ref[I] := Item;
   with Item do
   begin
    Changing := True;
    try
     case ValueType of
      PropContainer.vtDecimal:
       Value := FIniFile.ReadInt64(FConfigSection, PWideChar(UserTag), Value);
      PropContainer.vtHexadecimal:
       Value := FIniFile.ReadInt64(FConfigSection, PWideChar(UserTag), Value);
      PropContainer.vtString,
      PropContainer.vtPickString:
       ValueStr := FIniFile.ReadString(FConfigSection, PWideChar(UserTag), ValueStr);
      PropContainer.vtFloat:
       Float := FIniFile.ReadFloat(FConfigSection, PWideChar(UserTag), Float);
     end;
    finally
     Changing := False;
    end;
   end;
  end;
  Inc(Source);
 end;
end;

procedure TPluginDialog.PlgListBoxClick(Sender: TObject);
var
 Plugin: TPlugin;
 WStr: PWideChar;
begin
 DescriptionMemo.Clear;
 PropEditor.UserSelect := True;
 PropEditor.ClearList;
 try
  Plugin := FPlugins.Nodes[PlgListBox.ItemIndex] as TPlugin;
  if Plugin <> nil then
  begin
   if Plugin.Description(WStr) > 0 then
    DescriptionMemo.Text := WStr else
    DescriptionMemo.Text := 'No description';

   PropEditor.ClearList;

   FIniFile.FileName := FIniPath +
      WideChangeFileExt(Plugin.FileName, '.ini');

   FConfigIndex := FIniFile.ReadInteger(SActiveSection, LSStr[FMode], 1);
   FConfigSection := WideFormat('%s%d', [LSStr[FMode], FConfigIndex]);
   Perform(WM_FILLPLUGPROPS, 0, 0);
  end;
 except
  on E: Exception do
  begin
   DescriptionMemo.Clear;
   PropEditor.ClearList;
   with DescriptionMemo.Lines do
   begin
    Add('Plugin error:');
    Add(E.Message);
   end;
  end;
 end;
end;

function TPluginDialog.PlgListBoxDataFind(Control: TWinControl;
  FindString: String): Integer;
var
 I: Integer;
 WStr: WideString;
begin
 WStr := FindString;
 with FPlugins do
 begin
  for I := 0 to Count - 1 do
  begin
   with Nodes[I] as TPlugin do
    if Pos(WStr, FileName) = 1 then
    begin
     Result := I;
     Exit;
    end;
  end;
  Result := -1;
 end;
end;

procedure TPluginDialog.PlgListBoxDrawItem(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
var
 Flags: Integer;
 Plugin: TPlugin;
begin
 if Index >= 0 then
  with Control as TListBox do
  begin
   Canvas.FillRect(Rect);

   Flags := DrawTextBiDiModeFlags(DT_SINGLELINE or DT_VCENTER or DT_NOPREFIX);
   if not UseRightToLeftAlignment then
     Inc(Rect.Left, 2) else
     Dec(Rect.Right, 2);

   Plugin := FPlugins.Nodes[Index] as TPlugin;
   if Plugin <> nil then
    with Plugin do
     Windows.DrawTextW(Canvas.Handle, Pointer(FileName), Length(FileName),
                       Rect, Flags);
  end;
end;

procedure TPluginDialog.SetMode(Value: TPluginDialogMode);
begin
 FMode := Value;
 if FMode = pmLoad then
 begin
  OkButton.Caption := 'Load';
  Caption := 'Select plugin to load the project';
 end else
 begin
  OkButton.Caption := 'Save';
  Caption := 'Select plugin to save the project';
 end;
end;

procedure TPluginDialog.TntFormCreate(Sender: TObject);
begin
 FPlugins := TPluginList.Create;
 FIniFile := TStreamIniFileW.Create;
 PropEditor.PropertyList.OnValueChange := PlugValueChange;
end;

procedure TPluginDialog.TntFormDestroy(Sender: TObject);
begin
 FIniFile.Free;
 FPlugins.Free;
end;

procedure TPluginDialog.UpdatePlugins(const SelectedPlugin: WideString);
begin
 DescriptionMemo.Clear;
 PropEditor.UserSelect := True;
 PropEditor.ClearList;
 FPlugins.Load(FPath);
 PlgListBox.Count := FPlugins.Count;
 if FPlugins.Count > 0 then
 begin
  PlgListBox.ItemIndex := Max(0, FPlugins.FindIndexByName(SelectedPlugin));
  OkButton.Enabled := True;
 end else
  OkButton.Enabled := False;
 PlgListBoxClick(PlgListBox);
end;

procedure TPluginDialog.RefreshButtonClick(Sender: TObject);
begin
 UpdatePlugins(SelectedPluginName);
end;

function TPluginDialog.GetSelectedPluginName: WideString;
var
 Plg: TPlugin;
begin
 Plg := FPlugins.Nodes[PlgListBox.ItemIndex] as TPlugin;
 if Plg <> nil then
  Result := Plg.FileName else
  Result := '';
end;

procedure TPluginDialog.PropEditorEllipsisClick(
  Sender: TCustomPropertyEditor; Node: PVirtualNode;
  var Accepted: Boolean);
var
 Data: PPropertyData;
 Dialog: TTntOpenDialog;
 Str: WideString;
begin
 Data := Sender.GetNodeData(Node);
 with Data^ do
  if Item is TFilePickItem then
  begin
   with TFilePickItem(Item) do
   begin
    if Save then
     Dialog := SaveDialog else
     Dialog := OpenDialog;
    FillOpenDialog(Dialog);
    Accepted := Dialog.Execute;
    if Accepted then
     ValueStr := Dialog.FileName;
   end;
  end else
  if Item.Parameters and (PL_BUFFER or 8) = PL_BUFFER or 8 then
  begin
   Str := Item.ValueStr;
   Accepted := WideSelectDirectory(Item.Name, '', Str);
   if Accepted then
    Item.ValueStr := Str;
  end;
end;

procedure TPluginDialog.ConfigValueChange(Sender: TPropertyListItem);
begin
 FConfigIndex := Sender.Value;

 FIniFile.WriteInteger(SActiveSection, LSStr[FMode], FConfigIndex);
 FConfigSection := WideFormat('%s%d', [LSStr[FMode], FConfigIndex]);
 PostMessage(Handle, WM_FILLPLUGPROPS, 0, 0);
end;

procedure TPluginDialog.WMFillPlugProps(var Message: TMessage);
var
 Plugin: TPlugin;
 Data: TParametersData;
 Confirmed: Boolean;
begin
 with PropEditor do
 begin
  UserSelect := False;
  ClearList;
  Plugin := FPlugins.Nodes[PlgListBox.ItemIndex] as TPlugin;
  if Plugin <> nil then
  begin
   Data.Utils := TUtilsAdapter.Create;
   Data.IniFile := TIniFileAdapter.Create(FIniFile);
   Data.ShowMessage := @PlugShowMessage;
   Data.Section := Pointer(FConfigSection);
   Data.PropertyList := nil;
   Data.PropertyCount := 0;
   Confirmed := False;
   with Plugin do
    if FMode = pmLoad then
    begin
     if Addr(LoadingParameters) <> nil then
     begin
      LoadingParameters(@Data);
      Confirmed := True;
     end;
    end else
    begin
     if Addr(SavingParameters) <> nil then
     begin
      SavingParameters(@Data);
      Confirmed := True;
     end;
    end;

   if Confirmed then
   begin
    if Data.Section <> Pointer(FConfigSection) then
     FConfigSection := Data.Section;

    PropertyList.AddDecimal('Configuration #',
      FConfigIndex, 1, 256).OnValueChange := ConfigValueChange;

    FillPropertyList(Data.PropertyList, Data.PropertyCount);

    OkButton.ModalResult := mrOk;
   end else
    OkButton.ModalResult := mrNone;

   OkButton.Enabled := Confirmed;
   RootNodeCount := PropertyList.Count;
  end;
 end;
end;

procedure TPluginDialog.PlugValueChange(Sender: TPropertyListItem);
var
 Check: Boolean;
 Msg: WideString;
begin
 if Sender is TFilePickItem then
 with TFilePickItem(Sender) do
 begin
  Check := ValueStr <> '';
  if Check then
  begin
   if Save then
    Check := CheckFileName(ValueStr, DNIC) else
    Check := WideFileExists(ValueStr);
  end;
  if not Check then
  begin
   if ValueStr <> '' then
   begin
    if Save then
     Msg := WideFormat('''%s'' is invalid file name', [ValueStr]) else
     Msg := WideFormat('File ''%s'' is not found', [ValueStr]);
   end else
    Msg := 'File name must not be empty';

   WideMessageDlg(Msg, Dialogs.mtError, [Dialogs.mbOk], 0);
   Changing := True;
   try
    ValueStr := FIniFile.ReadString(FConfigSection, PWideChar(UserTag), '');
   finally
    Changing := False;
   end;
   Exit;
  end;
 end;

 with Sender do
 begin
  if Parameters and (PL_BUFFER or 8) = PL_BUFFER or 8 then
  begin
   if (ValueStr <> '') and not WideDirectoryExists(ValueStr) then
   begin
    WideMessageDlg(WideFormat('Directory ''%s'' is not found', [ValueStr]),
                      Dialogs.mtError, [Dialogs.mbOk], 0);
    Changing := True;
    try
     ValueStr := FIniFile.ReadString(FConfigSection, PWideChar(UserTag), '');
    finally
     Changing := False;
    end;
    Exit;
   end;
  end;
  case ValueType of
   PropContainer.vtDecimal:
    FIniFile.WriteInt64(FConfigSection, PWideChar(UserTag), Value);
   PropContainer.vtHexadecimal:
    FIniFile.WriteInt64(FConfigSection, PWideChar(UserTag), Value, HexDigits);
   PropContainer.vtString,
   PropContainer.vtPickString:
    FIniFile.WriteString(FConfigSection, PWideChar(UserTag), ValueStr);
   PropContainer.vtFloat:
    FIniFile.WriteFloat(FConfigSection, PWideChar(UserTag), Float);
  end;
 end; 
end;

end.
