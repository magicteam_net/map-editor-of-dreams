unit me_lib;

interface

uses
 SysUtils, Classes, HexUnit, TilesEx, NodeLst, MyClasses, BmpImg, BitmapEx, Types, Windows;

const
 LF_VISIBLE = 1 shl 0;
 LF_LAYER_ACTIVE = 1 shl 1;
 LF_TILE_SELECTED = 1 shl 2;
 LF_DRAW_X_FLIP = 1 shl 6;
 LF_DRAW_Y_FLIP = 1 shl 7;
 CIF_X_FLIP = 1;
 CIF_Y_FLIP = 1 shl 1;

 BLF_RANDOMIZE = 1 shl 5;


type
 TMap = class;

 TMapLayer = class;

 TLayerFormatRec = packed record
  lfTileSet:           TTileSet;
  lfIndexMask:         LongWord;
  lfPaletteIndexMask:  LongWord;
  lfPriorityMask:      LongWord;
  lfIndexShift:        ShortInt;
  lfPaletteIndexShift: ShortInt;
  lfPriorityShift:     ShortInt;
  lfXFlipShift:        ShortInt;
  lfYFlipShift:        ShortInt;
  lfCellSize:          Byte;
 end;

 TCellInfo = packed record
  TileIndex: LongInt;
  PaletteIndex: LongInt;
  Priority: LongInt;
  XFlip: Boolean;
  YFlip: Boolean;
 end;

 TCellInfoCmp = packed record
  Test1: Int64;
  Test2: LongInt;
  Test3: Word;
 end;

 TMapLayer = class(TBaseSectionedList)
  private
    FData: Pointer;

    FLayerFormatStart:  record end;
    FTileSet:           TTileSet;
    FIndexMask:         LongWord;
    FPaletteIndexMask:  LongWord;
    FPriorityMask:      LongWord;
    FIndexShift:        ShortInt;
    FPaletteIndexShift: ShortInt;
    FPriorityShift:     ShortInt;
    FXFlipShift:        ShortInt;
    FYFlipShift:        ShortInt;
    FCellSize:          Byte;

    FFirstColor:        Byte;
    FFlags:             Byte;
    FVisibleRect:       TRect;
    procedure SetCellSize(Value: Byte);
    procedure SetTileSetIndex(Value: Integer);
    function GetTileSetIndex: Integer;
    function GetLayerDataSize: Integer;
    function GetCellInfo(X, Y: Integer): TCellInfo;
    procedure SetCellInfo(X, Y: Integer; const Value: TCellInfo);
    function GetTileIndex(X, Y: Integer): Integer;
    procedure SetTileIndex(X, Y: Integer; Value: Integer);
    procedure SetIndexMask(Value: LongWord);
    procedure SetIndexShift(Value: ShortInt);
    procedure SetPaletteIndexMask(Value: LongWord);
    procedure SetPaletteIndexShift(Value: ShortInt);
    procedure SetPriorityMask(Value: LongWord);
    procedure SetPriorityShift(Value: ShortInt);
    procedure SetXFlipShift(Value: ShortInt);
    procedure SetYFlipShift(Value: ShortInt);
    function GetTilesFilled: Boolean;
    procedure SetTilesFilled(Value: Boolean);
    function GetVisibleRect: TRect;
  protected
    procedure Initialize; override;

    function CheckHeaderSize(var Size: LongInt): Boolean; override;
    function ReadHeader(Stream: TStream; var Header: TSectionHeader): Boolean; override;
    procedure ReadData(Stream: TStream; var Header: TSectionHeader); override;
    procedure WriteData(Stream: TStream; var Header: TSectionHeader); override;
    procedure FillHeader(var Header: TSectionHeader); override;
    function CalculateChecksum: TChecksum; override;
    function CalculateDataSize: LongInt; override;
  public
    DrawInfo: TCellInfo;
    QuickTiles: array of TTileItem;
    property VisibleRect: TRect read GetVisibleRect;
    property LayerData: Pointer read FData;
    property LayerDataSize: Integer read GetLayerDataSize;
    property TileSet: TTileSet read FTileSet;
    property TileSetIndex: Integer read GetTileSetIndex write SetTileSetIndex;
    property TilesFilled: Boolean read GetTilesFilled write SetTilesFilled;

    property CellSize: Byte read FCellSize write SetCellSize;
    property IndexMask: LongWord read FIndexMask write SetIndexMask;
    property IndexShift: ShortInt read FIndexShift write SetIndexShift;
    property XFlipShift: ShortInt read FXFlipShift write SetXFlipShift;
    property YFlipShift: ShortInt read FYFlipShift write SetYFlipShift;
    property PaletteIndexShift: ShortInt read FPaletteIndexShift write SetPaletteIndexShift;
    property PaletteIndexMask: LongWord read FPaletteIndexMask write SetPaletteIndexMask;
    property PriorityMask: LongWord read FPriorityMask write SetPriorityMask;
    property PriorityShift: ShortInt read FPriorityShift write SetPriorityShift;
    property FirstColor: Byte read FFirstColor write FFirstColor;

    property Cells[X, Y: Integer]: TCellInfo read GetCellInfo write SetCellInfo;
    property TileIndex[X, Y: Integer]: Integer read GetTileIndex write SetTileIndex;
    property Flags: Byte read FFlags write FFlags;

    function CompareLayerFormat(const AFmt: TLayerFormatRec): Boolean;
    procedure FillLayerFormat(var AFmt: TLayerFormatRec);
    function GetLayerFormat: TLayerFormatRec;

    procedure ReadCellInfo(const Data; var Info: TCellInfo);
    function ReadTileIndex(var Data): LongInt;
    function ReadPaletteIndex(var Data): LongInt;
    function ReadPriority(var Data): LongInt;
    function ReadXFlip(var Data): Boolean;
    function ReadYFlip(var Data): Boolean;

    procedure WriteCellInfo(var Data; const Info: TCellInfo);
    procedure WriteTileIndex(var Data; Value: LongInt);
    procedure WritePaletteIndex(var Data; Value: LongInt);
    procedure WritePriority(var Data; Value: LongInt);
    procedure WriteXFlip(var Data; Value: LongBool);
    procedure WriteYFlip(var Data; Value: LongBool);
    function FindCellInfo(const Info: TCellInfo): Integer;

    procedure AssignLayerFormat(Source: TMapLayer);
    procedure AssignLayerInfo(Source: TMapLayer);
    procedure Assign(Source: TNode); override;
    destructor Destroy; override;



    procedure UpdateFormat(const NewFormat: TLayerFormatRec);
 end;

 EMapLayerError = class(Exception);

 TMap = class(TSectionedList)
  private
    FColorTable: TColorTable;
    FMapX: Integer;
    FMapY: Integer;
    FWidth: Integer;
    FHeight: Integer;
    FTileWidth: Integer;
    FTileHeight: Integer;
    function GetLayer(Index: Integer): TMapLayer;
    function GetCtIndex: Integer;
    procedure SetCtIndex(Value: Integer);
    procedure SetHeight(Value: Integer);
    procedure SetWidth(Value: Integer);
  protected
    procedure Initialize; override;
    function CheckHeaderSize(var Size: LongInt): Boolean; override;
    function ReadHeader(Stream: TStream; var Header: TSectionHeader): Boolean; override;
    function ReadSetCount(var Header: TSectionHeader): LongInt; override;
    procedure FillHeader(var Header: TSectionHeader); override;
  public
    property MapX: Integer read FMapX write FMapX;
    property MapY: Integer read FMapY write FMapY;
    property Width: Integer read FWidth write SetWidth;
    property Height: Integer read FHeight write SetHeight;
    property TileWidth: Integer read FTileWidth write FTileWidth;
    property TileHeight: Integer read FTileHeight write FTileHeight;
    property ColorTable: TColorTable read FColorTable;
    property ColorTableIndex: Integer read GetCtIndex write SetCtIndex;

    property Layers[Index: Integer]: TMapLayer read GetLayer;

    procedure Assign(Source: TNode); override;
    procedure AssignMapInfo(Source: TMap); virtual;
    function FindLayerByTileSet(ATileSet: TTileSet): TMapLayer;
    procedure SetMapSize(AWidth, AHeight: Integer);

    function IsTileSetUsed(ATileSet: TTileSet): Boolean;
    procedure UnhookTileSet(ATileSet: TTileSet);    
 end;

 EMapError = class(Exception);

 TLinkDirection = (ldLeft,
                   ldRight,
                   ldUp,
                   ldDown,
                   ldLeftUp,
                   ldRightUp,
                   ldLeftDown,
                   ldRightDown);

 TLinkDirections = set of TLinkDirection;

 TBrushLayer = class;

 PLayerArray = ^TLayerArray;
 TLayerArray = array[TLinkDirection] of TBrushLayer;

 TDrawSpots = array [TLinkDirection] of TSmallPoint;

 PBrushLayerTailRec = ^TBrushLayerTailRec;
 TBrushLayerTailRec = record
  bloLayer: TBrushLayer;
  bloDistance: TSmallPoint;
  bloDirection: TLinkDirection;
 end;

 TBrushLayerTails = array of TBrushLayerTailRec;

 TBrushLayerArray = array of TBrushLayer;

 TBrushLayer = class(TMapLayer)
  private
    FIdentityTag: LongInt;
    FHotSpotX: Integer;
    FHotSpotY: Integer;
    FLinksStart: record end;
    FLinkLeft: TBrushLayer;
    FLinkRight: TBrushLayer;
    FLinkUp: TBrushLayer;
    FLinkDown: TBrushLayer;
    FLinkLeftUp: TBrushLayer;
    FLinkRightUp: TBrushLayer;
    FLinkLeftDown: TBrushLayer;
    FLinkRightDown: TBrushLayer;
    FUnfinished: TLinkDirections;
    FExtended: TLinkDirections;
    FTailsDirections: TLinkDirections;
    FRandomize: Boolean;
    function GetDownIndex: Integer;
    function GetLeftDownIndex: Integer;
    function GetLeftIndex: Integer;
    function GetLeftUpIndex: Integer;
    function GetRightDownIndex: Integer;
    function GetRightIndex: Integer;
    function GetRightUpIndex: Integer;
    function GetUpIndex: Integer;
    procedure SetDownIndex(Value: Integer);
    procedure SetLeftDownIndex(Value: Integer);
    procedure SetLeftIndex(Value: Integer);
    procedure SetLeftUpIndex(Value: Integer);
    procedure SetRightDownIndex(Value: Integer);
    procedure SetRightIndex(Value: Integer);
    procedure SetRightUpIndex(Value: Integer);
    procedure SetUpIndex(Value: Integer);
    function GetLinkIndexInternal(Link: TBrushLayer): Integer;
    procedure SetLinkIndexInternal(Value: Integer; var Link: TBrushLayer);
    function GetLinkIndex(Direction: TLinkDirection): Integer;
    procedure SetLinkIndex(Direction: TLinkDirection; Value: Integer);
    function GetLink(Direction: TLinkDirection): TBrushLayer;
    function ScanLayer(Target: TMapLayer; X, Y: Integer): Boolean;
    procedure SetIdentityTag(Value: LongInt);
    function GetTailsFilled: Boolean;
    procedure SetTailsFilled(Value: Boolean);
    function GetRandomTwin: TBrushLayer;
    function GetTailsDirections: TLinkDirections;
  protected
    procedure Initialize; override;
    function CheckSignature(Value: LongWord): Boolean; override;
    function CheckHeaderSize(var Size: LongInt): Boolean; override;
    function ReadHeader(Stream: TStream; var Header: TSectionHeader): Boolean; override;
    procedure FillHeader(var Header: TSectionHeader); override;
  public
    DrawSpots: TDrawSpots;
    Tails: TBrushLayerTails;
    property HotSpotX: Integer read FHotSpotX write FHotSpotX;
    property HotSpotY: Integer read FHotSpotY write FHotSpotY;
    property IdentityTag: LongInt read FIdentityTag write SetIdentityTag;
    property TailsFilled: Boolean read GetTailsFilled write SetTailsFilled;
    property TailsDirections: TLinkDirections read GetTailsDirections;
    property Randomize: Boolean read FRandomize write FRandomize;

    property LinkLeft: TBrushLayer read FLinkLeft;
    property LinkRight: TBrushLayer read FLinkRight;
    property LinkUp: TBrushLayer read FLinkUp;
    property LinkDown: TBrushLayer read FLinkDown;
    property LinkLeftUp: TBrushLayer read FLinkLeftUp;
    property LinkRightUp: TBrushLayer read FLinkRightUp;
    property LinkLeftDown: TBrushLayer read FLinkLeftDown;
    property LinkRightDown: TBrushLayer read FLinkRightDown;

    property LinkLeftIndex: Integer read GetLeftIndex write SetLeftIndex;
    property LinkRightIndex: Integer read GetRightIndex write SetRightIndex;
    property LinkUpIndex: Integer read GetUpIndex write SetUpIndex;
    property LinkDownIndex: Integer read GetDownIndex write SetDownIndex;
    property LinkLeftUpIndex: Integer read GetLeftUpIndex write SetLeftUpIndex;
    property LinkRightUpIndex: Integer read GetRightUpIndex write SetRightUpIndex;
    property LinkLeftDownIndex: Integer read GetLeftDownIndex write SetLeftDownIndex;
    property LinkRightDownIndex: Integer read GetRightDownIndex write SetRightDownIndex;

    property Unfinished: TLinkDirections read FUnfinished write FUnfinished;
    property Extended: TLinkDirections read FExtended write FExtended;

    property RandomTwin: TBrushLayer read GetRandomTwin;

    function IsTwin(Layer: TBrushLayer): Boolean;

    property LinkIndex[Direction: TLinkDirection]: Integer read GetLinkIndex write SetLinkIndex;
    property Links[Direction: TLinkDirection]: TBrushLayer read GetLink;

    function SitsHere(Target: TMapLayer; X, Y: Integer): Boolean;
    function SpottedInRect(Target: TMapLayer; const Rect: TRect; var pt: TPoint): Boolean;
    function RectOnMap(X, Y: Integer): TRect;
    function FindTail(Tail: TBrushLayer;
     Directions: TLinkDirections = []): Integer;

    procedure Assign(Source: TNode); override;
    function DrawTail(Dest: TMap; DestX, DestY: Integer; Direction: TLinkDirection): Boolean;
    function Draw(DestLayer: TMapLayer; DestX, DestY: Integer): Boolean;
 end;

 TMapBrush = class(TMap)
  private
    FExtends: TMapBrush;
    function GetExtendsIndex: Integer;
    procedure SetExtendsIndex(Value: Integer);
  protected
    procedure Initialize; override;

    function CheckSignature(Value: LongWord): Boolean; override;
    function CheckHeaderSize(var Size: LongInt): Boolean; override;
    function ReadHeader(Stream: TStream; var Header: TSectionHeader): Boolean; override;
    function ReadSetCount(var Header: TSectionHeader): LongInt; override;
    procedure FillHeader(var Header: TSectionHeader); override;
    procedure AssignBrushInfo(Source: TMapBrush);
  public
    property Extends: TMapBrush read FExtends write FExtends;
    property ExtendsIndex: Integer read GetExtendsIndex write SetExtendsIndex;

    function FindBrushLayerSittingHere(Dest: TMap; X, Y: Integer): TBrushLayer;
    function FindExactBrushLayerSittingHere(Dest: TMap; X, Y: Integer): TBrushLayer;
    function FindExactBrushLayer(Dest: TMap; X, Y: Integer; var pt: TPoint): TBrushLayer; overload;
    function FindExactBrushLayer(DestLayer: TMapLayer; X, Y: Integer; var pt: TPoint): TBrushLayer; overload;
    function FindBrushLayer(Dest: TMap; X, Y: Integer; var pt: TPoint): TBrushLayer;

    function Draw(Dest: TMap; DestX, DestY: Integer): Boolean;
    procedure Assign(Source: TNode); override;
    procedure AssignMapInfo(Source: TMap); override;
    procedure Changed; override;
    procedure RepointLinks;
 end;

 TBaseMapList = class(TSectionedList)
  private
    function GetMap(Index: Integer): TMap;
  protected
    FColorTableList: TColorTableList;
    FTileSetList: TTileSetList;
    procedure Initialize; override;
  public
    property ColorTableList: TColorTableList read FColorTableList;
    property TileSetList: TTileSetList read FTileSetList;
    property Maps[Index: Integer]: TMap read GetMap; default;

    function AddMap(const AName: WideString; AWidth, AHeight, ATileW, ATileH: Integer): TMap;
    function IsColorTableUsed(AColorTable: TColorTable): Boolean;
    function IsTileSetUsed(ATileSet: TTileSet): Boolean;
    procedure UnhookTileSet(ATileSet: TTileSet);
    procedure UnhookColorTable(AColorTable: TColorTable);

    procedure TileSetsLock;
    procedure TileSetsUnlock;

    procedure ColorTablesLock;
    procedure ColorTablesUnlock;
 end;

 TInternalMapList = class(TBaseMapList)
  public
    procedure Assign(Source: TNode); override;
 end;

 TLayerIdentityRec = packed record Layer1, Layer2: TBrushLayer end;

 TBrushList = class(TBaseMapList)
  private
    function GetMapBrush(Index: Integer): TMapBrush;
    function GetIdentitiesFilled: Boolean;
    procedure SetIdentitiesFilled(Value: Boolean);
  protected
    procedure Initialize; override;

    procedure ReadData(Stream: TStream; var Header: TSectionHeader); override;
  public
    IdentityList: array of TLayerIdentityRec;
    property Brushes[Index: Integer]: TMapBrush read GetMapBrush; default;
    property ColorTableList: TColorTableList read FColorTableList
                                            write FColorTableList;
    property TileSetList: TTileSetList read FTileSetList
                                      write FTileSetList;
    property IdentitiesFilled: Boolean read GetIdentitiesFilled write SetIdentitiesFilled;

    procedure CleanUpTails;

    function EraseBrushContentAt(Dest: TMap; const DestRect: TRect): Boolean;
    procedure Changed; override;

    function IsBrushUsed(ABrush: TMapBrush): Boolean;
    procedure UnhookBrush(ABrush: TMapBrush);
 end;

 TInternalBrushList = class(TBrushList)
  public
    procedure Assign(Source: TNode); override;
 end;


 PTempData = ^TTempData;
 TTempData = record
  Stream: TStream;
  HeadPtr: ^TSectionHeader;
 end;

 TMapList = class(TBaseMapList)
  private
    FBrushList: TBrushList;
  protected
    procedure Initialize; override;
    procedure ClearData; override;

    function ReadSectionHeader(Stream: TStream; var Header: TSectionHeader): Boolean; override;
    function CheckHeaderSize(var Size: LongInt): Boolean; override;
    function ReadHeader(Stream: TStream; var Header: TSectionHeader): Boolean; override;
    procedure ReadData(Stream: TStream; var Header: TSectionHeader); override;
    procedure WriteData(Stream: TStream; var Header: TSectionHeader); override;

    procedure Progress(Sender: TObject; const Rec: TProgressRec); override;

    function CalculateChecksum: TChecksum; override;
    function CalculateDataSize: LongInt; override;

    procedure SetOnProgress(Value: TSectionProgressEvent); override;
  public
    property BrushList: TBrushList read FBrushList;

    procedure Assign(Source: TNode); override;
    destructor Destroy; override;
 end;

 EMapListFormatError = class(Exception);

 TLayerIndex = (liBackground, liForeground, liObjects);

const
 LayerNames: array[TLayerIndex] of WideString =
 ('Background',
  'Foreground',
  'Objects');

 DirReverse: array[TLinkDirection] of TLinkDirection =
                  (ldRight,
                   ldLeft,
                   ldDown,
                   ldUp,
                   ldRightDown,
                   ldLeftDown,
                   ldRightUp,
                   ldLeftUp);

 DirRelate: array[0..1, TLinkDirection] of TLinkDirection =
  ((ldLeftUp,   // ldLeft:
    ldRightUp,  // ldRight:
    ldLeftUp,   // ldUp:
    ldLeftDown, // ldDown:
    ldLeft,     // ldLeftUp,
    ldRight,    // ldRightUp,
    ldLeft,     // ldLeftDown:
    ldRight),   // ldRightDown:
   (ldLeftDown,  // ldLeft:
    ldRightDown, // ldRight:
    ldRightUp,   // ldUp:
    ldRightDown, // ldDown:
    ldUp,        // ldLeftUp,
    ldUp,        // ldRightUp,
    ldDown,      // ldLeftDown:
    ldDown));     // ldRightDown:

 DirCoord: array[TLinkDirection] of TSmallPoint =
 ((X: -1;  Y:  0), // Left
  (X: +1;  Y:  0), // Right
  (X:  0;  Y: -1), // Up
  (X:  0;  Y: +1), // Down
  (X: -1;  Y: -1), // LeftUp
  (X: +1;  Y: -1), // RightUp
  (X: -1;  Y: +1), // LeftDown
  (X: +1;  Y: +1));// RightDown


function LoadObjectData(Source: TStream; const Name: AnsiString;
                        var Data: Pointer): Integer;

procedure CellInfoRead(const Format: TLayerFormatRec;
                        const Data; var Info: TCellInfo);
function PaletteIndexRead(const Format: TLayerFormatRec; const Data): LongInt;
function PriorityRead(const Format: TLayerFormatRec; const Data): LongInt;
function TileIndexRead(const Format: TLayerFormatRec; const Data): LongInt;
function XFlipRead(const Format: TLayerFormatRec; const Data): Boolean;
function YFlipRead(const Format: TLayerFormatRec; const Data): Boolean;

procedure CellInfoWrite(const Format: TLayerFormatRec;
                        var Data; const Info: TCellInfo);
procedure PaletteIndexWrite(const Format: TLayerFormatRec; var Data; Value: LongInt);
procedure PriorityWrite(const Format: TLayerFormatRec; var Data; Value: LongInt);
procedure TileIndexWrite(const Format: TLayerFormatRec; var Data; Value: LongInt);
procedure XFlipWrite(const Format: TLayerFormatRec; var Data; Value: LongBool);
procedure YFlipWrite(const Format: TLayerFormatRec; var Data; Value: LongBool);


const
 MAPS_SIGN = Ord('M') or
            (Ord('A') shl 8) or
            (Ord('P') shl 16) or
            (Ord('S') shl 24);
 MLST_SIGN = Ord('M') or
            (Ord('L') shl 8) or
            (Ord('S') shl 16) or
            (Ord('T') shl 24);
 BLST_SIGN = Ord('B') or
            (Ord('L') shl 8) or
            (Ord('S') shl 16) or
            (Ord('T') shl 24);
 MAP_SIGN =  Ord('M') or
            (Ord('A') shl 8) or
            (Ord('P') shl 16) or
            (Ord('0') shl 24);
 BRUSH_SIGN = Ord('B') or
             (Ord('R') shl 8) or
             (Ord('S') shl 16) or
             (Ord('H') shl 24);
 LAYER_SIGN =
             Ord('L') or
            (Ord('A') shl 8) or
            (Ord('Y') shl 16) or
            (Ord('R') shl 24);
 BRUSH_LAYER_SIGN =
             Ord('B') or
            (Ord('R') shl 8) or
            (Ord('L') shl 16) or
            (Ord('R') shl 24);

type
 TMapHeaderV1 = packed record
  mapOffsetX:     LongInt;   // 4
  mapOffsetY:     LongInt;   // 8
  mapWidth:       Word;      // 10
  mapHeight:      Word;      // 12
  mapTileWidth:   Word;      // 14
  mapTileHeight:  Word;      // 16
  mapColorTblIdx: SmallInt;  // 18
  mapLayersCount: Byte;      // 19
  mapNameLength:  Byte;      // 20
 end;

 TMapHeaderV2 = packed record
  mapOffsetX:      LongInt;  // 4
  mapOffsetY:      LongInt;  // 8
  mapWidth:        Word;     // 10
  mapHeight:       Word;     // 12
  mapTileWidth:    Word;     // 14
  mapTileHeight:   Word;     // 16
  mapColorTblIdx:  SmallInt; // 18
  mapLayersCount:  Byte;     // 19
  mapReserved: array [0..2] of Byte;     // 22
  mapNameLength:   Word;     // 24
 end;

 TMapHeader = TMapHeaderV2;

 TBrushHeader = packed record
  MainHeader: TMapHeader;
  brReserved: LongWord;
 end;

 TLayerHeaderV1 = packed record
  lhIndexMask:         LongWord; // 4

  lhIndexShift:        Byte;
  lhXFlipShift:        ShortInt;
  lhYFlipShift:        ShortInt;
  lhPaletteIndexShift: Byte;      // 8

  lhPaletteIndexMask:  LongWord;  // 12

  lhTileSetIndex:      SmallInt;
  lhCellSize:          Word;      // 16

  lhFlags:             Word;
  lhFirstColor:        Byte;
  lhNameLength:        Byte;      // 20
 end;

 TLayerHeaderV2 = packed record
  lhIndexMask:         LongWord;  // 4
  lhPaletteIndexMask:  Word;      // 6
  lhPriorityMask:      Word;      // 8

  lhIndexShift:        ShortInt;  // 9
  lhPaletteIndexShift: ShortInt;  // 10
  lhPriorityShift:     ShortInt;  // 10
  lhXFlipShift:        ShortInt;  // 12
  lhYFlipShift:        ShortInt;  // 13

  lhFlags:             Byte;      // 14
  lhCellSize:          Byte;      // 15
  lhFirstColor:        Byte;      // 16

  lhDrawTileIndex:     LongInt;   // 20
  lhDrawPaletteIndex:  SmallInt;  // 22
  lhDrawPriority:      SmallInt;  // 24
  lhTileSetIndex:      SmallInt;  // 26
  lhNameLength:        Word;      // 28
 end;

 TLayerHeader = TLayerHeaderV2;

 TBrushLayerHeaderV1 = packed record
  MainHeader:    TLayerHeader;
  blhHotSpot:    TSmallPoint;
  blhUnfinished: TLinkDirections;
  blhExtended:   TLinkDirections;
 end;

 TBrushLayerHeaderV2 = packed record
  V1:            TBrushLayerHeaderV1;
  blhLinks: array [TLinkDirection] of ShortInt;
 end;

 TBrushLayerHeaderV3 = packed record
  V2:             TBrushLayerHeaderV2;
  blhDrawSpots:   TDrawSpots;
 end;

 TBrushLayerHeaderV4 = packed record
  V3:             TBrushLayerHeaderV3;
  blhIdentityTag: LongInt;
 end;

 TBrushLayerHeader = TBrushLayerHeaderV4;

 TPBLayerRec = record
                lData: Pointer;
                lFormat: TLayerFormatRec;
               end;

 TPasteBuffer = class
  ID: Integer;
  Layers: array of TPBLayerRec;
  Width: Integer;
  Height: Integer;
  TileSet: TTileSet;
  Map: array of LongInt;

  procedure SaveToStream(Stream: TStream);
  procedure LoadFromStream(Stream: TStream; TileSets: TTileSetList);

  constructor Create(BufID: Integer);
  procedure Clear;
  destructor Destroy; override;
 end;

type
 PDrawInfo = ^TDrawInfo;
 TDrawInfo = record
  TileSet: TTileSet;
  Tile: TTileItem;
  Info: TCellInfo;
  FC: Byte;
  Next: PDrawInfo;
  Prev: PDrawInfo;
 end;

type
 TDrawInfoRec = record
  First: PDrawInfo;
  Middle: PDrawInfo;
  Last: PDrawInfo;
 end;

procedure AddDrawInfo(var i: TDrawInfoRec; N: PDrawInfo);

implementation

const
 PB_SIGN = $B00CAB00;

type
 TPasteBufferHeader = packed record
  Signature:   LongWord;
  ID:          LongInt;
  LayersCount: LongInt;
  Width:       LongInt;
  Height:      LongInt;
  TilesOffset: LongInt;
  LayerOffset: LongInt;
 end;

 TPB_LayerHeader = packed record
  TileSetDataSize:  LongInt;
  TileSetChecksum:  LongWord;
  LayerDataSize:    LongInt;
 end;

procedure AddDrawInfo(var i: TDrawInfoRec; N: PDrawInfo);
var
 Go, Oops: PDrawInfo;
begin
 if i.First = nil then
 begin
  N.Prev := nil;
  N.Next := nil;
  i.First := N;
  i.Middle := N;
  i.Last := N;
 end else
 begin
  if N.Info.Priority < i.First.Info.Priority then
  begin
   N.Next := i.First;
   i.First.Prev := N;
   i.First := N;
  end else
  if N.Info.Priority >= i.Last.Info.Priority then
  begin
   N.Prev := i.Last;
   N.Next := nil;
   i.Last.Next := N;
   i.Last := N;
  end else
  if N.Info.Priority = i.First.Info.Priority then
  begin
   N.Prev := i.First;
   N.Next := i.First.Next;
   i.First.Next := N;
   if i.Last = i.First then
   begin
    i.Last := N;
    i.Middle := N;
   end;
  end else
  begin
   Go := i.Middle;
   while N.Info.Priority < Go.Info.Priority do
    Go := Go.Prev;
   if Go <> i.Middle then
   begin
    Oops := Go.Next;
    Go.Next := N;
    Oops.Prev := N;
    N.Prev := Go;
    N.Next := Oops;
   end else
   begin
    while N.Info.Priority >= Go.Info.Priority do
     Go := Go.Next;
    Oops := Go.Prev;
    Go.Prev := N;
    Oops.Next := N;
    N.Prev := Oops;
    N.Next := Go;
   end;
  end;
 end;
end;

function PaletteIndexRead(const Format: TLayerFormatRec; const Data): LongInt;
// EAX = Self
// EDX = Data
asm
 push ebx
 push esi
 mov ebx,eax
 mov esi,edx
 xor eax,eax
 mov al,[ebx + TLayerFormatRec.lfCellSize]
 jmp dword ptr [@@rci_jmp + eax * 4 - 4]
@@rci_jmp:
 dd @@rci1
 dd @@rci2
 dd @@rci3
 dd @@rci4
 dd @@rci5
 dd @@rci6
 dd @@rci7
 dd @@rci8
@@rci1:
 mov dl,[esi]
 jmp @@rci32bit
@@rci3:
 mov dh,[esi+2]
 bswap edx
 mov dx,[esi]
 jmp @@rci32bit
@@rci4:
 mov edx,[esi]
 jmp @@rci32bit
@@rci2:
 mov dx,[esi]

@@rci32bit:
 or eax,-1
 mov cl,[ebx + TLayerFormatRec.lfPaletteIndexShift]
 test cl,cl
 js @@rci32_end
 mov eax,edx
 shr eax,cl
 and eax,[ebx + TLayerFormatRec.lfPaletteIndexMask]
@@rci32_end:
 pop esi
 pop ebx
 ret

@@rci5:
 mov dl,[esi+4]
 jmp @@rci64bit
@@rci6:
 mov dx,[esi+4]
 jmp @@rci64bit
@@rci7:
 mov dh,[esi+6]
 bswap edx
 mov dx,[esi+4]
 jmp @@rci64bit
@@rci8:
 mov edx,[esi+4]

@@rci64bit:
 or eax,-1
 mov cl,[ebx + TLayerFormatRec.lfPaletteIndexShift]
 test cl,cl
 js @@rci64_end
 mov eax,[esi]
 call _Shr64to32
 and eax,[ebx + TLayerFormatRec.lfPaletteIndexMask]
@@rci64_end:
 pop esi
 pop ebx
end;

function PriorityRead(const Format: TLayerFormatRec; const Data): LongInt;
// EAX = Self
// EDX = Data
asm
 push ebx
 push esi
 mov ebx,eax
 mov esi,edx
 xor eax,eax
 mov al,[ebx + TLayerFormatRec.lfCellSize]
 jmp dword ptr [@@rci_jmp + eax * 4 - 4]
@@rci_jmp:
 dd @@rci1
 dd @@rci2
 dd @@rci3
 dd @@rci4
 dd @@rci5
 dd @@rci6
 dd @@rci7
 dd @@rci8
@@rci1:
 mov dl,[esi]
 jmp @@rci32bit
@@rci3:
 mov dh,[esi+2]
 bswap edx
 mov dx,[esi]
 jmp @@rci32bit
@@rci4:
 mov edx,[esi]
 jmp @@rci32bit
@@rci2:
 mov dx,[esi]

@@rci32bit:
 or eax,-1
 mov cl,[ebx + TLayerFormatRec.lfPriorityShift]
 test cl,cl
 js @@rci32_end
 mov eax,edx
 shr eax,cl
 and eax,[ebx + TLayerFormatRec.lfPriorityMask]
@@rci32_end:
 pop esi
 pop ebx
 ret

@@rci5:
 mov dl,[esi+4]
 jmp @@rci64bit
@@rci6:
 mov dx,[esi+4]
 jmp @@rci64bit
@@rci7:
 mov dh,[esi+6]
 bswap edx
 mov dx,[esi+4]
 jmp @@rci64bit
@@rci8:
 mov edx,[esi+4]

@@rci64bit:
 or eax,-1
 mov cl,[ebx + TLayerFormatRec.lfPriorityShift]
 test cl,cl
 js @@rci64_end
 mov eax,[esi]
 call _Shr64to32
 and eax,[ebx + TLayerFormatRec.lfPriorityMask]
@@rci64_end:
 pop esi
 pop ebx
end;

function TileIndexRead(const Format: TLayerFormatRec; const Data): LongInt;
// EAX = Self
// EDX = Data
asm
 push ebx
 push esi
 mov ebx,eax
 mov esi,edx
 xor eax,eax
 mov al,[ebx + TLayerFormatRec.lfCellSize]
 jmp dword ptr [@@rci_jmp + eax * 4 - 4]
@@rci_jmp:
 dd @@rci1
 dd @@rci2
 dd @@rci3
 dd @@rci4
 dd @@rci5
 dd @@rci6
 dd @@rci7
 dd @@rci8
@@rci1:
 mov dl,[esi]
 jmp @@rci32bit
@@rci3:
 mov dh,[esi+2]
 bswap edx
 mov dx,[esi]
 jmp @@rci32bit
@@rci4:
 mov edx,[esi]
 jmp @@rci32bit
@@rci2:
 mov dx,[esi]

@@rci32bit:
 or eax,-1
 mov cl,[ebx + TLayerFormatRec.lfIndexShift]
 test cl,cl
 js @@rci32_end
 mov eax,edx
 shr eax,cl
 and eax,[ebx + TLayerFormatRec.lfIndexMask]
@@rci32_end:
 pop esi
 pop ebx
 ret

@@rci5:
 mov dl,[esi+4]
 jmp @@rci64bit
@@rci6:
 mov dx,[esi+4]
 jmp @@rci64bit
@@rci7:
 mov dh,[esi+6]
 bswap edx
 mov dx,[esi+4]
 jmp @@rci64bit
@@rci8:
 mov edx,[esi+4]

@@rci64bit:
 or eax,-1
 mov cl,[ebx + TLayerFormatRec.lfIndexShift]
 test cl,cl
 js @@rci64_end
 mov eax,[esi]
 call _Shr64to32
 and eax,[ebx + TLayerFormatRec.lfIndexMask]
@@rci64_end:
 pop esi
 pop ebx
end;

function XFlipRead(const Format: TLayerFormatRec; const Data): Boolean;
// EAX = Self
// EDX = Data
asm
 push ebx
 push esi
 mov ebx,eax
 mov esi,edx
 xor eax,eax
 mov al,[ebx + TLayerFormatRec.lfCellSize]
 jmp dword ptr [@@rci_jmp + eax * 4 - 4]
@@rci_jmp:
 dd @@rci1
 dd @@rci2
 dd @@rci3
 dd @@rci4
 dd @@rci5
 dd @@rci6
 dd @@rci7
 dd @@rci8
@@rci1:
 mov dl,[esi]
 jmp @@rci32bit
@@rci3:
 mov dh,[esi+2]
 bswap edx
 mov dx,[esi]
 jmp @@rci32bit
@@rci4:
 mov edx,[esi]
 jmp @@rci32bit
@@rci2:
 mov dx,[esi]

@@rci32bit:
 xor eax,eax
 mov cl,[ebx + TLayerFormatRec.lfXFlipShift]
 test cl,cl
 js @@rci32_end
 mov eax,edx
 shr eax,cl
 and eax,1
@@rci32_end:
 pop esi
 pop ebx
 ret

@@rci5:
 mov dl,[esi+4]
 jmp @@rci64bit
@@rci6:
 mov dx,[esi+4]
 jmp @@rci64bit
@@rci7:
 mov dh,[esi+6]
 bswap edx
 mov dx,[esi+4]
 jmp @@rci64bit
@@rci8:
 mov edx,[esi+4]

@@rci64bit:
 xor eax,eax
 mov cl,[ebx + TLayerFormatRec.lfXFlipShift]
 test cl,cl
 js @@rci64_end
 mov eax,[esi]
 call _Shr64to32
 and eax,1
@@rci64_end:
 pop esi
 pop ebx
end;

function YFlipRead(const Format: TLayerFormatRec; const Data): Boolean;
// EAX = Self
// EDX = Data
asm
 push ebx
 push esi
 mov ebx,eax
 mov esi,edx
 xor eax,eax
 mov al,[ebx + TLayerFormatRec.lfCellSize]
 jmp dword ptr [@@rci_jmp + eax * 4 - 4]
@@rci_jmp:
 dd @@rci1
 dd @@rci2
 dd @@rci3
 dd @@rci4
 dd @@rci5
 dd @@rci6
 dd @@rci7
 dd @@rci8
@@rci1:
 mov dl,[esi]
 jmp @@rci32bit
@@rci3:
 mov dh,[esi+2]
 bswap edx
 mov dx,[esi]
 jmp @@rci32bit
@@rci4:
 mov edx,[esi]
 jmp @@rci32bit
@@rci2:
 mov dx,[esi]

@@rci32bit:
 xor eax,eax
 mov cl,[ebx + TLayerFormatRec.lfYFlipShift]
 test cl,cl
 js @@rci32_end
 mov eax,edx
 shr eax,cl
 and eax,1
@@rci32_end:
 pop esi
 pop ebx
 ret

@@rci5:
 mov dl,[esi+4]
 jmp @@rci64bit
@@rci6:
 mov dx,[esi+4]
 jmp @@rci64bit
@@rci7:
 mov dh,[esi+6]
 bswap edx
 mov dx,[esi+4]
 jmp @@rci64bit
@@rci8:
 mov edx,[esi+4]

@@rci64bit:
 xor eax,eax
 mov cl,[ebx + TLayerFormatRec.lfYFlipShift]
 test cl,cl
 js @@rci64_end
 mov eax,[esi]
 call _Shr64to32
 and eax,1
@@rci64_end:
 pop esi
 pop ebx
end;

procedure CellInfoRead(const Format: TLayerFormatRec;
                        const Data; var Info: TCellInfo);
// EAX = Format
// EDX = Data
// ECX = Info
asm
 push ebx
 push esi
 push edi
 mov ebx,eax
 mov esi,edx
 mov edi,ecx
 xor eax,eax
 mov al,[ebx + TLayerFormatRec.lfCellSize]
 jmp dword ptr [@@rci_jmp + eax * 4 - 4]
@@rci_jmp:
 dd @@rci1
 dd @@rci2
 dd @@rci3
 dd @@rci4
 dd @@rci5
 dd @@rci6
 dd @@rci7
 dd @@rci8
@@rci5:
 mov dl,[esi+4]
 jmp @@rci64bit
@@rci6:
 mov dx,[esi+4]
 jmp @@rci64bit
@@rci7:
 mov dh,[esi+6]
 bswap edx
 mov dx,[esi+4]
 jmp @@rci64bit
@@rci8:
 mov edx,[esi+4]

@@rci64bit:

 xor eax,eax
 mov cl,[ebx + TLayerFormatRec.lfXFlipShift]
 test cl,cl
 js @@rci64_no_x
 mov eax,[esi]
 call _Shr64to32
 and eax,1
@@rci64_no_x:
 mov [edi + TCellInfo.XFlip],al

 xor eax,eax
 mov cl,[ebx + TLayerFormatRec.lfYFlipShift]
 test cl,cl
 js @@rci64_no_y
 mov eax,[esi]
 call _Shr64to32
 and eax,1
@@rci64_no_y:
 mov [edi + TCellInfo.YFlip],al

 or eax,-1
 mov cl,[ebx + TLayerFormatRec.lfIndexShift]
 test cl,cl
 js @@rci64_no_index
 mov eax,[esi]
 call _Shr64to32
 and eax,[ebx + TLayerFormatRec.lfIndexMask]
@@rci64_no_index:
 mov [edi + TCellInfo.TileIndex],eax

 or eax,-1
 mov cl,[ebx + TLayerFormatRec.lfPaletteIndexShift]
 test cl,cl
 js @@rci64_no_palette
 mov eax,[esi]
 call _Shr64to32
 and eax,[ebx + TLayerFormatRec.lfPaletteIndexMask]
@@rci64_no_palette:
 mov [edi + TCellInfo.PaletteIndex],eax

 or eax,-1
 mov cl,[ebx + TLayerFormatRec.lfPriorityShift]
 test cl,cl
 js @@rci64_no_priority
 mov eax,[esi]
 call _Shr64to32
 and eax,[ebx + TLayerFormatRec.lfPriorityMask]
@@rci64_no_priority:
 mov [edi + TCellInfo.Priority],eax
 pop edi
 pop esi
 pop ebx
 ret

@@rci1:
 mov al,[esi]
 jmp @@rci32bit
@@rci3:
 mov ah,[esi+2]
 bswap eax
 mov ax,[esi]
 jmp @@rci32bit
@@rci4:
 mov eax,[esi]
 jmp @@rci32bit
@@rci2:
 mov ax,[esi]

@@rci32bit:
 xor edx,edx
 mov cl,[ebx + TLayerFormatRec.lfXFlipShift]
 test cl,cl
 js @@rci32_no_x
 mov edx,eax
 shr edx,cl
 and edx,1
@@rci32_no_x:
 mov [edi + TCellInfo.XFlip],dl

 xor edx,edx
 mov cl,[ebx + TLayerFormatRec.lfYFlipShift]
 test cl,cl
 js @@rci32_no_y
 mov edx,eax
 shr edx,cl
 and edx,1
@@rci32_no_y:
 mov [edi + TCellInfo.YFlip],dl

 or edx,-1
 mov cl,[ebx + TLayerFormatRec.lfIndexShift]
 test cl,cl
 js @@rci32_no_index
 mov edx,eax
 shr edx,cl
 and edx,[ebx + TLayerFormatRec.lfIndexMask]
@@rci32_no_index:
 mov [edi + TCellInfo.TileIndex],edx

 or edx,-1
 mov cl,[ebx + TLayerFormatRec.lfPaletteIndexShift]
 test cl,cl
 js @@rci32_no_palette
 mov edx,eax
 shr edx,cl
 and edx,[ebx + TLayerFormatRec.lfPaletteIndexMask]
@@rci32_no_palette:
 mov [edi + TCellInfo.PaletteIndex],edx

 or edx,-1
 mov cl,[ebx + TLayerFormatRec.lfPriorityShift]
 test cl,cl
 js @@rci32_no_priority
 mov edx,eax
 shr edx,cl
 and edx,[ebx + TLayerFormatRec.lfPriorityMask]
@@rci32_no_priority:
 mov [edi + TCellInfo.Priority],edx

@@rci_end:
 pop edi
 pop esi
 pop ebx
end;

procedure PaletteIndexWrite(const Format: TLayerFormatRec; var Data; Value: Integer);
// EAX = Self
// EDX = Data
// ECX = Value
asm
 push ebp
 push esi
 push edi
 mov ebp,eax // EBP = Self
 mov edi,edx // EDI = Data
 mov esi,ecx // ESI = Value
 cmp byte ptr [ebp + TLayerFormatRec.lfCellSize],4
 ja @@wci64bit

@@wci32bit:
 mov cl,[ebp + TLayerFormatRec.lfPaletteIndexShift]
 test cl,cl
 js @@wri32_end
 mov eax,[ebp + TLayerFormatRec.lfPaletteIndexMask]
 and esi,eax
 shl esi,cl
 shl eax,cl
 mov edx,esi

 not eax
 mov cl,[ebp + TLayerFormatRec.lfCellSize]
 dec cl
 jz @@wri1
 dec cl
 jz @@wri2
 dec cl
 jz @@wri3
@@wri4:
 and [edi],eax
 or [edi],edx
 pop edi
 pop esi
 pop ebp
 ret
@@wri3:
 and [edi],ax
 bswap eax
 and [edi+2],ah
 or [edi],dx
 bswap edx
 or [edi+2],dh
 pop edi
 pop esi
 pop ebp
 ret
@@wri2:
 and [edi],ax
 or [edi],dx
 pop edi
 pop esi
 pop ebp
 ret
@@wri1:
 and [edi],al
 or [edi],dl

@@wri32_end:
 pop edi
 pop esi
 pop ebp
 ret


@@wci64bit:
 push ebx

 mov cl,[ebp + TLayerFormatRec.lfPaletteIndexShift]
 test cl,cl
 js @@wri64_end
 xor edx,edx
 mov ebx,[ebp + TLayerFormatRec.lfPaletteIndexMask]
 and esi,ebx
 call _Shl64_ESI_EDX

 xor eax,eax
 call _Shl64_EBX_EAX
 not ebx
 not eax

 and [edi],ebx
 or [edi],esi

 mov cl,[ebp + TLayerFormatRec.lfCellSize]
 sub cl,5
 jz @@wri5
 dec cl
 jz @@wri6
 dec cl
 jz @@wri7
@@wri8:
 and [edi+4],eax
 or [edi+4],edx
 pop ebx
 pop edi
 pop esi
 pop ebp
 ret
@@wri7:
 and [edi+4],ax
 bswap eax
 and [edi+6],ah
 or [edi+4],dx
 bswap edx
 or [edi+6],dh
 pop ebx
 pop edi
 pop esi
 pop ebp
 ret
@@wri6:
 and [edi+4],ax
 or [edi+4],dx
 pop ebx
 pop edi
 pop esi
 pop ebp
 ret
@@wri5:
 and [edi+4],al
 or [edi+4],dl

@@wri64_end:
 pop ebx
 pop edi
 pop esi
 pop ebp
end;

procedure PriorityWrite(const Format: TLayerFormatRec; var Data; Value: Integer);
// EAX = Self
// EDX = Data
// ECX = Value
asm
 push ebp
 push esi
 push edi
 mov ebp,eax // EBP = Self
 mov edi,edx // EDI = Data
 mov esi,ecx // ESI = Value
 cmp byte ptr [ebp + TLayerFormatRec.lfCellSize],4
 ja @@wci64bit

@@wci32bit:
 mov cl,[ebp + TLayerFormatRec.lfPriorityShift]
 test cl,cl
 js @@wri32_end
 mov eax,[ebp + TLayerFormatRec.lfPriorityMask]
 and esi,eax
 shl esi,cl
 shl eax,cl
 mov edx,esi

 not eax
 mov cl,[ebp + TLayerFormatRec.lfCellSize]
 dec cl
 jz @@wri1
 dec cl
 jz @@wri2
 dec cl
 jz @@wri3
@@wri4:
 and [edi],eax
 or [edi],edx
 pop edi
 pop esi
 pop ebp
 ret
@@wri3:
 and [edi],ax
 bswap eax
 and [edi+2],ah
 or [edi],dx
 bswap edx
 or [edi+2],dh
 pop edi
 pop esi
 pop ebp
 ret
@@wri2:
 and [edi],ax
 or [edi],dx
 pop edi
 pop esi
 pop ebp
 ret
@@wri1:
 and [edi],al
 or [edi],dl

@@wri32_end:
 pop edi
 pop esi
 pop ebp
 ret


@@wci64bit:
 push ebx

 mov cl,[ebp + TLayerFormatRec.lfPriorityShift]
 test cl,cl
 js @@wri64_end
 xor edx,edx
 mov ebx,[ebp + TLayerFormatRec.lfPriorityMask]
 and esi,ebx
 call _Shl64_ESI_EDX

 xor eax,eax
 call _Shl64_EBX_EAX
 not ebx
 not eax

 and [edi],ebx
 or [edi],esi

 mov cl,[ebp + TLayerFormatRec.lfCellSize]
 sub cl,5
 jz @@wri5
 dec cl
 jz @@wri6
 dec cl
 jz @@wri7
@@wri8:
 and [edi+4],eax
 or [edi+4],edx
 pop ebx
 pop edi
 pop esi
 pop ebp
 ret
@@wri7:
 and [edi+4],ax
 bswap eax
 and [edi+6],ah
 or [edi+4],dx
 bswap edx
 or [edi+6],dh
 pop ebx
 pop edi
 pop esi
 pop ebp
 ret
@@wri6:
 and [edi+4],ax
 or [edi+4],dx
 pop ebx
 pop edi
 pop esi
 pop ebp
 ret
@@wri5:
 and [edi+4],al
 or [edi+4],dl

@@wri64_end:
 pop ebx
 pop edi
 pop esi
 pop ebp
end;

procedure TileIndexWrite(const Format: TLayerFormatRec; var Data; Value: Integer);
// EAX = Self
// EDX = Data
// ECX = Value
asm
 push ebp
 push esi
 push edi
 mov ebp,eax // EBP = Self
 mov edi,edx // EDI = Data
 mov esi,ecx // ESI = Value
 cmp byte ptr [ebp + TLayerFormatRec.lfCellSize],4
 ja @@wci64bit

@@wci32bit:
 mov cl,[ebp + TLayerFormatRec.lfIndexShift]
 test cl,cl
 js @@wri32_end
 mov eax,[ebp + TLayerFormatRec.lfIndexMask]
 and esi,eax
 shl esi,cl
 shl eax,cl
 mov edx,esi

 not eax
 mov cl,[ebp + TLayerFormatRec.lfCellSize]
 dec cl
 jz @@wri1
 dec cl
 jz @@wri2
 dec cl
 jz @@wri3
@@wri4:
 and [edi],eax
 or [edi],edx
 pop edi
 pop esi
 pop ebp
 ret
@@wri3:
 and [edi],ax
 bswap eax
 and [edi+2],ah
 or [edi],dx
 bswap edx
 or [edi+2],dh
 pop edi
 pop esi
 pop ebp
 ret
@@wri2:
 and [edi],ax
 or [edi],dx
 pop edi
 pop esi
 pop ebp
 ret
@@wri1:
 and [edi],al
 or [edi],dl

@@wri32_end:
 pop edi
 pop esi
 pop ebp
 ret


@@wci64bit:
 push ebx

 mov cl,[ebp + TLayerFormatRec.lfIndexShift]
 test cl,cl
 js @@wri64_end
 xor edx,edx
 mov ebx,[ebp + TLayerFormatRec.lfIndexMask]
 and esi,ebx
 call _Shl64_ESI_EDX

 xor eax,eax
 call _Shl64_EBX_EAX
 not ebx
 not eax

 and [edi],ebx
 or [edi],esi

 mov cl,[ebp + TLayerFormatRec.lfCellSize]
 sub cl,5
 jz @@wri5
 dec cl
 jz @@wri6
 dec cl
 jz @@wri7
@@wri8:
 and [edi+4],eax
 or [edi+4],edx
 pop ebx
 pop edi
 pop esi
 pop ebp
 ret
@@wri7:
 and [edi+4],ax
 bswap eax
 and [edi+6],ah
 or [edi+4],dx
 bswap edx
 or [edi+6],dh
 pop ebx
 pop edi
 pop esi
 pop ebp
 ret
@@wri6:
 and [edi+4],ax
 or [edi+4],dx
 pop ebx
 pop edi
 pop esi
 pop ebp
 ret
@@wri5:
 and [edi+4],al
 or [edi+4],dl

@@wri64_end:
 pop ebx
 pop edi
 pop esi
 pop ebp
end;

procedure XFlipWrite(const Format: TLayerFormatRec; var Data; Value: LongBool);
// EAX = Self
// EDX = Data
// ECX = Value
asm
 push ebp
 push esi
 push edi
 mov ebp,eax // EBP = Self
 mov edi,edx // EDI = Data
 mov esi,ecx // ESI = Value
 cmp byte ptr [ebp + TLayerFormatRec.lfCellSize],4
 ja @@wci64bit

@@wci32bit:
 mov cl,[ebp + TLayerFormatRec.lfXFlipShift]
 test cl,cl
 js @@wri32_end
 xor edx,edx
 test esi,esi
 setnz dl
 shl edx,cl
 mov eax,1
 shl eax,cl

 not eax
 mov cl,[ebp + TLayerFormatRec.lfCellSize]
 dec cl
 jz @@wri1
 dec cl
 jz @@wri2
 dec cl
 jz @@wri3
@@wri4:
 and [edi],eax
 or [edi],edx
 pop edi
 pop esi
 pop ebp
 ret
@@wri3:
 and [edi],ax
 bswap eax
 and [edi+2],ah
 or [edi],dx
 bswap edx
 or [edi+2],dh
 pop edi
 pop esi
 pop ebp
 ret
@@wri2:
 and [edi],ax
 or [edi],dx
 pop edi
 pop esi
 pop ebp
 ret
@@wri1:
 and [edi],al
 or [edi],dl

@@wri32_end:
 pop edi
 pop esi
 pop ebp
 ret


@@wci64bit:
 push ebx

 mov cl,[ebp + TLayerFormatRec.lfXFlipShift]
 test cl,cl
 js @@wri64_end
 xor edx,edx
 xor eax,eax
 test esi,esi
 jz @@skip_shift
 setnz al
 call System.@_llshl
 mov esi,eax
@@skip_shift:

 xor eax,eax
 mov ebx,1
 call _Shl64_EBX_EAX
 not ebx
 not eax

 and [edi],ebx
 or [edi],esi

 mov cl,[ebp + TLayerFormatRec.lfCellSize]
 sub cl,5
 jz @@wri5
 dec cl
 jz @@wri6
 dec cl
 jz @@wri7
@@wri8:
 and [edi+4],eax
 or [edi+4],edx
 pop ebx
 pop edi
 pop esi
 pop ebp
 ret
@@wri7:
 and [edi+4],ax
 bswap eax
 and [edi+6],ah
 or [edi+4],dx
 bswap edx
 or [edi+6],dh
 pop ebx
 pop edi
 pop esi
 pop ebp
 ret
@@wri6:
 and [edi+4],ax
 or [edi+4],dx
 pop ebx
 pop edi
 pop esi
 pop ebp
 ret
@@wri5:
 and [edi+4],al
 or [edi+4],dl

@@wri64_end:
 pop ebx
 pop edi
 pop esi
 pop ebp
end;

procedure YFlipWrite(const Format: TLayerFormatRec; var Data; Value: LongBool);
// EAX = Self
// EDX = Data
// ECX = Value
asm
 push ebp
 push esi
 push edi
 mov ebp,eax // EBP = Self
 mov edi,edx // EDI = Data
 mov esi,ecx // ESI = Value
 cmp byte ptr [ebp + TLayerFormatRec.lfCellSize],4
 ja @@wci64bit

@@wci32bit:
 mov cl,[ebp + TLayerFormatRec.lfYFlipShift]
 test cl,cl
 js @@wri32_end
 xor edx,edx
 test esi,esi
 setnz dl
 shl edx,cl
 mov eax,1
 shl eax,cl

 not eax
 mov cl,[ebp + TLayerFormatRec.lfCellSize]
 dec cl
 jz @@wri1
 dec cl
 jz @@wri2
 dec cl
 jz @@wri3
@@wri4:
 and [edi],eax
 or [edi],edx
 pop edi
 pop esi
 pop ebp
 ret
@@wri3:
 and [edi],ax
 bswap eax
 and [edi+2],ah
 or [edi],dx
 bswap edx
 or [edi+2],dh
 pop edi
 pop esi
 pop ebp
 ret
@@wri2:
 and [edi],ax
 or [edi],dx
 pop edi
 pop esi
 pop ebp
 ret
@@wri1:
 and [edi],al
 or [edi],dl

@@wri32_end:
 pop edi
 pop esi
 pop ebp
 ret


@@wci64bit:
 push ebx

 mov cl,[ebp + TLayerFormatRec.lfYFlipShift]
 test cl,cl
 js @@wri64_end
 xor edx,edx
 xor eax,eax
 test esi,esi
 jz @@skip_shift
 setnz al
 call System.@_llshl
 mov esi,eax
@@skip_shift:

 xor eax,eax
 mov ebx,1
 call _Shl64_EBX_EAX
 not ebx
 not eax

 and [edi],ebx
 or [edi],esi

 mov cl,[ebp + TLayerFormatRec.lfCellSize]
 sub cl,5
 jz @@wri5
 dec cl
 jz @@wri6
 dec cl
 jz @@wri7
@@wri8:
 and [edi+4],eax
 or [edi+4],edx
 pop ebx
 pop edi
 pop esi
 pop ebp
 ret
@@wri7:
 and [edi+4],ax
 bswap eax
 and [edi+6],ah
 or [edi+4],dx
 bswap edx
 or [edi+6],dh
 pop ebx
 pop edi
 pop esi
 pop ebp
 ret
@@wri6:
 and [edi+4],ax
 or [edi+4],dx
 pop ebx
 pop edi
 pop esi
 pop ebp
 ret
@@wri5:
 and [edi+4],al
 or [edi+4],dl

@@wri64_end:
 pop ebx
 pop edi
 pop esi
 pop ebp
end;

procedure CellInfoWrite(const Format: TLayerFormatRec;
                        var Data; const Info: TCellInfo);
// EAX = AFormat
// EDX = Data
// ECX = Info
asm
 push ebx
 push esi
 push edi
 mov ebx,eax // EBX = AFormat
 mov edi,edx // EDI = Data
 mov esi,ecx // ESI = Info
 cmp byte ptr [ebx + TLayerFormatRec.lfCellSize],4
 ja @@wci64bit

@@wci32bit:
 xor eax,eax
 mov cl,[ebx + TLayerFormatRec.lfIndexShift]
 test cl,cl
 js @@wri32_no_index
 mov eax,[esi + TCellInfo.TileIndex]
 and eax,[ebx + TLayerFormatRec.lfIndexMask]
 shl eax,cl
@@wri32_no_index:
 mov edx,eax

 mov cl,[ebx + TLayerFormatRec.lfPaletteIndexShift]
 test cl,cl
 js @@wri32_no_palette
 mov eax,[esi + TCellInfo.PaletteIndex]
 and eax,[ebx + TLayerFormatRec.lfPaletteIndexMask]
 shl eax,cl
 or edx,eax
@@wri32_no_palette:

 mov cl,[ebx + TLayerFormatRec.lfPriorityShift]
 test cl,cl
 js @@wri32_no_priority
 mov eax,[esi + TCellInfo.Priority]
 and eax,[ebx + TLayerFormatRec.lfPriorityMask]
 shl eax,cl
 or edx,eax
@@wri32_no_priority:

 mov cl,[ebx + TLayerFormatRec.lfXFlipShift]
 test cl,cl
 js @@wri32_no_x
 xor eax,eax
 mov al,byte ptr [esi + TCellInfo.XFlip]
 test al,al
 jz @@wri32_no_x
 setnz al
 shl eax,cl
 or edx,eax
@@wri32_no_x:

 mov cl,[ebx + TLayerFormatRec.lfYFlipShift]
 test cl,cl
 js @@wri32_no_y
 xor eax,eax
 mov al,byte ptr [esi + TCellInfo.YFlip]
 test al,al
 jz @@wri32_no_y
 setnz al
 shl eax,cl
 or edx,eax
@@wri32_no_y:

 mov cl,[ebx + TLayerFormatRec.lfCellSize]
 dec cl
 jz @@wri1
 dec cl
 jz @@wri2
 dec cl
 jz @@wri3
@@wri4:
 mov [edi],edx
 pop edi
 pop esi
 pop ebx
 ret
@@wri3:
 mov [edi],dx
 bswap edx
 mov [edi+2],dh
 pop edi
 pop esi
 pop ebx
 ret
@@wri2:
 mov [edi],dx
 pop edi
 pop esi
 pop ebx
 ret
@@wri1:
 mov [edi],dl
 pop edi
 pop esi
 pop ebx
 ret


@@wci64bit:
 sub esp,8

 xor eax,eax
 xor edx,edx
 mov cl,[ebx + TLayerFormatRec.lfIndexShift]
 test cl,cl
 js @@wri64_no_index
 mov eax,[esi + TCellInfo.TileIndex]
 and eax,[ebx + TLayerFormatRec.lfIndexMask]
 call System.@_llshl
@@wri64_no_index:
 mov [esp],eax
 mov [esp+4],edx

 mov cl,[ebx + TLayerFormatRec.lfPaletteIndexShift]
 test cl,cl
 js @@wri64_no_palette
 mov eax,[esi + TCellInfo.PaletteIndex]
 and eax,[ebx + TLayerFormatRec.lfPaletteIndexMask]
 xor edx,edx
 call System.@_llshl
 or [esp],eax
 or [esp+4],edx
@@wri64_no_palette:

 mov cl,[ebx + TLayerFormatRec.lfPriorityShift]
 test cl,cl
 js @@wri64_no_priority
 mov eax,[esi + TCellInfo.Priority]
 and eax,[ebx + TLayerFormatRec.lfPriorityMask]
 xor edx,edx
 call System.@_llshl
 or [esp],eax
 or [esp+4],edx
@@wri64_no_priority:

 mov cl,[ebx + TLayerFormatRec.lfXFlipShift]
 test cl,cl
 js @@wri64_no_x
 xor eax,eax
 mov al,byte ptr [esi + TCellInfo.XFlip]
 test al,al
 jz @@wri64_no_x
 setnz al
 xor edx,edx
 call System.@_llshl
 or [esp],eax
 or [esp+4],edx
@@wri64_no_x:

 mov cl,[ebx + TLayerFormatRec.lfYFlipShift]
 test cl,cl
 js @@wri64_no_y
 xor eax,eax
 mov al,byte ptr [esi + TCellInfo.YFlip]
 test al,al
 jz @@wri64_no_y
 setnz al
 xor edx,edx
 call System.@_llshl
 or [esp],eax
 or [esp+4],edx
@@wri64_no_y:

 mov esi,esp
 xor ecx,ecx
 mov cl,[ebx + TLayerFormatRec.lfCellSize]
 rep movsb

 add esp,8

@@wci_end:
 pop edi
 pop esi
 pop ebx
end;

procedure MapListFormatError;
begin
 raise EMapListFormatError.Create('Map list data format error');
end;

function LoadObjectData(Source: TStream; const Name: AnsiString;
                        var Data: Pointer): Integer;
type
 TDataRec = packed record
  unk1: Byte;
  DataSize: Word;
  unk2: LongWord;
 end;

 TNameRec = packed record
  unk1: Byte;
  unk2: Byte;
  Size: Byte;
 end;

var
 Tag: Byte;
 Size: Word;
 Head: TDataRec;
 Buf: Pointer;
 Found: Boolean;
 Last: Int64;
 LastSize: Integer;
 PB: PByte;
begin
 Result := 0;
 Found := False;
 Last := Source.Position;
 LastSize := Source.Size - Last;
 repeat
  Source.Read(Tag, 1);
  Source.Read(Size, 2);
  case Tag of
   $80, $88, $96, $9A: if not Found then Source.Seek(Size, soFromCurrent) else Break;
   $8A: Break;
   $90:
   begin
    if Found then Break;
    GetMem(Buf, Size);
    try
     Source.Read(Buf^, Size);
     with TNameRec(Buf^) do
     begin
      Dec(Size);
      Found := (Size = Length(Name)) and
        CompareMem(Pointer(Integer(Buf) + SizeOf(TNameRec) + 1),
                   Pointer(Name), Size);
     end;
    finally
     FreeMem(Buf);
    end;
   end;
   $98:
   begin
    if Found then Break;
    if Size <> SizeOf(TDataRec) then Break;
    Source.Read(Head, Size);
   end;
   $A0:
   begin
    Source.Seek(-3, soFromCurrent);
    Result := Head.DataSize;
    ReallocMem(Data, Result);
    PB := Data;
    while Result > 0 do
    begin
     Source.Seek(6, soFromCurrent);
     Size := Min(Result, 1024);
     Source.Read(PB^, Size);
     Inc(PB, Size);
     Dec(Result, Size);
     Source.Seek(1, soFromCurrent); // skip checksum
    end;
    Result := Head.DataSize;
   end else
    Break;
  end;
 until False;
 if not Found then
 begin
  Source.Position := Last;
  Result := LastSize;
  ReallocMem(Data, Result);
  Source.Read(Data^, Result);
 end;
end;

{ TMapLayer }

procedure TMapLayer.Assign(Source: TNode);
var
 Src: TMapLayer absolute Source;
 Y, CopySize, SrcStride, DstStride: Integer;
 SrcP, DstP: PByte;
begin
 if Source <> Self then
 begin
  inherited;
  AssignLayerInfo(Source as TMapLayer);

  SrcP := Src.LayerData;
  DstP := LayerData;

  SrcStride := (Src.Owner as TMap).Width * CellSize;
  DstStride := (Owner as TMap).Width * CellSize;
  CopySize := Min(SrcStride, DstStride);
  for Y := 0 to Min(TMap(Owner).Height, TMap(Src.Owner).Height) - 1 do
  begin
   Move(SrcP^, DstP^, CopySize);
   Inc(SrcP, SrcStride);
   Inc(DstP, DstStride);
  end;
  TilesFilled := False;
 end;
end;

procedure TMapLayer.AssignLayerFormat(Source: TMapLayer);
begin
 if Source <> nil then
 begin
  SetCellSize(Source.CellSize);
  Move(Source.FLayerFormatStart, FLayerFormatStart, SizeOf(TLayerFormatRec));
  FFirstColor := Source.FFirstColor;
 end else
  raise EMapLayerError.Create('Cannot assign layer format if source layer is undefined');
end;

procedure TMapLayer.AssignLayerInfo(Source: TMapLayer);
begin
 AssignLayerFormat(Source);
 Name := Source.FName;
 if (Owner.Owner is TBaseMapList) and
    (secCustom in TBaseMapList(Owner.Owner).FInternalFlags) then
  TileSetIndex := Source.TileSetIndex else
  FTileSet := Source.FTileSet;
 FFlags := Source.FFlags;
 DrawInfo := Source.DrawInfo;
end;

function TMapLayer.CalculateChecksum: TChecksum;
begin
 Result := CalcCheckSum(FData^, LayerDataSize) +
           inherited CalculateChecksum;
end;

function TMapLayer.CalculateDataSize: LongInt;
begin
 Result := LayerDataSize + inherited CalculateDataSize;
end;

function TMapLayer.CheckHeaderSize(var Size: LongInt): Boolean;
begin
 Result := (Size = SizeOf(TLayerHeaderV1)) or inherited CheckHeaderSize(Size);
end;

function TMapLayer.CompareLayerFormat(const AFmt: TLayerFormatRec): Boolean;
begin
 Result := CompareMem(@AFmt, @FLayerFormatStart, SizeOf(TLayerFormatRec));
end;

destructor TMapLayer.Destroy;
begin
 FreeMem(FData);
 inherited;
end;

procedure TMapLayer.FillHeader(var Header: TSectionHeader);
begin
 with TLayerHeader(Addr(Header.dh)^) do
 begin
  lhIndexMask         := FIndexMask;
  lhPaletteIndexMask  := FPaletteIndexMask;
  lhPriorityMask      := FPriorityMask;


  lhIndexShift        := FIndexShift;
  lhPaletteIndexShift := FPaletteIndexShift;
  lhPriorityShift     := FPriorityShift;
  lhXFlipShift        := FXFlipShift;
  lhYFlipShift        := FYFlipShift;

  lhFlags             := FFlags;

  if DrawInfo.XFlip then
   lhFlags := lhFlags or LF_DRAW_X_FLIP else
   lhFlags := lhFlags and not LF_DRAW_X_FLIP;

  if DrawInfo.YFlip then
   lhFlags := lhFlags or LF_DRAW_Y_FLIP else
   lhFlags := lhFlags and not LF_DRAW_Y_FLIP;

  lhCellSize          := FCellSize;
  lhFirstColor        := FFirstColor;

  lhDrawTileIndex     := DrawInfo.TileIndex;
  lhDrawPaletteIndex  := DrawInfo.PaletteIndex;
  lhDrawPriority      := DrawInfo.Priority;
  lhTileSetIndex      := TileSetIndex;
  lhNameLength        := Min(Length(Name), FMaxNameLength);
 end;
end;

procedure TMapLayer.FillLayerFormat(var AFmt: TLayerFormatRec);
begin
 Move(FLayerFormatStart, AFmt, SizeOf(TLayerFormatRec));
end;

function TMapLayer.FindCellInfo(const Info: TCellInfo): Integer;
var
 P: PByte;
 P16: PWord absolute P;
 P32: PLongWord absolute P;
 P64: PInt64 absolute P;
 Size: Integer;
 Buf: array[0..7] of Byte;
 Found: Boolean;
begin
 WriteCellInfo(Buf, Info);
 P := LayerData;
 Size := LayerDataSize;
 while Size > 0 do
 begin
  Found := False;
  case CellSize of
   1: Found := P^ = Buf[0];
   2: Found := P16^ = Word(Addr(Buf)^);
   4: Found := P32^ = LongWord(Addr(Buf)^);
   3, 5..7: Found := CompareMem(P, @Buf, CellSize);
   8: Found := P64^ = Int64(Addr(Buf)^);
  end;
  if Found then
  begin
   Result := Integer(P) - Integer(LayerData);
   Exit;
  end;
  Inc(P, CellSize);
 end;
 Result := -1;
end;

function TMapLayer.GetCellInfo(X, Y: Integer): TCellInfo;
begin
 with TMap(Owner)  do
  if (FData <> nil) and
     (X >= 0) and (X < Width) and
     (Y >= 0) and (Y < Height) then
  ReadCellInfo(PByteArray(FData)[(Y * Width + X) * CellSize], Result) else
  Result.TileIndex := -1;
end;

function TMapLayer.GetLayerDataSize: Integer;
begin
 with FOwner as TMap do
  Result := FWidth * FCellSize * FHeight;
end;

function TMapLayer.GetLayerFormat: TLayerFormatRec;
begin
 FillLayerFormat(Result);
end;

function TMapLayer.GetTileIndex(X, Y: Integer): Integer;
begin
 with Owner as TMap do
  if (FData <> nil) and
     (X >= 0) and (X < Width) and
     (Y >= 0) and (Y < Height) then
   Result := ReadTileIndex(PByteArray(FData)[(Y * Width + X) * CellSize]) else
   Result := -1;
end;

function TMapLayer.GetTileSetIndex: Integer;
begin
 if FTileSet <> nil then
  Result := FTileSet.Index else
  Result := -1;
end;

function TMapLayer.GetTilesFilled: Boolean;
begin
 Result := (QuickTiles <> nil) and (FTileSet <> nil);
end;

function TMapLayer.GetVisibleRect: TRect;
begin
 TilesFilled := True;
 Result := FVisibleRect;
end;

procedure TMapLayer.Initialize;
begin
 FAssignableClass := TMapLayer;
 FIndexShift := 0;
 FPaletteIndexShift := -1;
 FPriorityShift := -1;
 FXFlipShift := -1;
 FYFlipShift := -1;
 FCellSize := 1;
 FFlags := LF_VISIBLE or LF_LAYER_ACTIVE;
 DrawInfo.TileIndex := -1;
 FSignature := LAYER_SIGN;
 FHeaderSize := SizeOf(TLayerHeader);
 FMaxNameLength := High(Word);
end;

procedure TMapLayer.ReadCellInfo(const Data; var Info: TCellInfo);
begin
 CellInfoRead(TLayerFormatRec(Addr(FLayerFormatStart)^), Data, Info);
end;

procedure TMapLayer.ReadData(Stream: TStream; var Header: TSectionHeader);
var
 Rec: TProgressRec;
begin
 inherited;
 Rec.Stream := Stream;
 Rec.Header := @Header;
 Rec.ProgressID := SPID_READ_DATA;
 Read(Rec, FData^, LayerDataSize);
end;

function TMapLayer.ReadHeader(Stream: TStream; var Header: TSectionHeader): Boolean;
var
 V1: ^TLayerHeaderV1;
 V2: ^TLayerHeaderV2 absolute V1;
begin
 Result := inherited ReadHeader(Stream, Header);
 if Result then
 begin
  V1 := Addr(Header.dh);
  case Header.shHeaderSize of
   SizeOf(TLayerHeaderV1):
   begin
    TileSetIndex := V1.lhTileSetIndex;
    CellSize := V1.lhCellSize;
    FIndexMask := V1.lhIndexMask;
    FIndexShift := V1.lhIndexShift;
    FXFlipShift := V1.lhXFlipShift;
    FYFlipShift := V1.lhYFlipShift;

    FPaletteIndexMask := V1.lhPaletteIndexMask;
    if FPaletteIndexMask <> 0 then
     FPaletteIndexShift := V1.lhPaletteIndexShift else
     FPaletteIndexShift := -1;
    FPriorityShift := -1;
    FPriorityMask := 0;
    FFirstColor := V1.lhFirstColor;
    FFlags := (V1.lhFlags and 1) or LF_LAYER_ACTIVE;
    FillChar(DrawInfo, SizeOf(TCellInfo), 0);
    DrawInfo.TileIndex := -1;
    SetLength(FName, V1.lhNameLength);
   end;
   else
   if Header.shHeaderSize >= SizeOf(TLayerHeaderV2) then
   begin
    FIndexMask := V2.lhIndexMask;
    FPaletteIndexMask := V2.lhPaletteIndexMask;
    FPriorityMask := V2.lhPriorityMask;

    FIndexShift := V2.lhIndexShift;
    FPaletteIndexShift := V2.lhPaletteIndexShift;
    FPriorityShift := V2.lhPriorityShift;
    FXFlipShift := V2.lhXFlipShift;
    FYFlipShift := V2.lhYFlipShift;

    FFlags := V2.lhFlags;
    FFirstColor := V2.lhFirstColor;
    DrawInfo.TileIndex := V2.lhDrawTileIndex;
    DrawInfo.PaletteIndex := V2.lhDrawPaletteIndex;
    DrawInfo.Priority := V2.lhDrawPriority;
    DrawInfo.XFlip := V2.lhFlags and LF_DRAW_X_FLIP <> 0;
    DrawInfo.YFlip := V2.lhFlags and LF_DRAW_Y_FLIP <> 0;

    TileSetIndex := V2.lhTileSetIndex;
    CellSize := V2.lhCellSize;
    SetLength(FName, V2.lhNameLength);
   end else Result := False;
  end;
 end;
end;

function TMapLayer.ReadPaletteIndex(var Data): LongInt;
begin
 Result := PaletteIndexRead(TLayerFormatRec(Addr(FLayerFormatStart)^), Data);
end;

function TMapLayer.ReadPriority(var Data): LongInt;
begin
 Result := PriorityRead(TLayerFormatRec(Addr(FLayerFormatStart)^), Data);
end;

function TMapLayer.ReadTileIndex(var Data): LongInt;
begin
 Result := TileIndexRead(TLayerFormatRec(Addr(FLayerFormatStart)^), Data);
end;

function TMapLayer.ReadXFlip(var Data): Boolean;
begin
 Result := XFlipRead(TLayerFormatRec(Addr(FLayerFormatStart)^), Data);
end;

function TMapLayer.ReadYFlip(var Data): Boolean;
begin
 Result := YFlipRead(TLayerFormatRec(Addr(FLayerFormatStart)^), Data);
end;

procedure TMapLayer.SetCellInfo(X, Y: Integer; const Value: TCellInfo);
begin
 with Owner as TMap do
  if (FData <> nil) and
     (X >= 0) and (X < Width) and
     (Y >= 0) and (Y < Height) then
   WriteCellInfo(PByteArray(FData)[(Y * Width + X) * CellSize], Value);
end;

procedure TMapLayer.SetCellSize(Value: Byte);
var
 Temp: array of TCellInfo;
 I, Len: Integer;
 DataPtr: PByte;
 InfoPtr: ^TCellInfo;
begin
 Value := Max(Min(Value, 8), 0);
 if FCellSize <> Value then
 begin
  if Value <> 0 then
  begin
   with Owner as TMap do Len := FWidth * FHeight;

   if (FData <> nil) and (Len > 0) then
   begin
    SetLength(Temp, Len);
    DataPtr := FData;
    InfoPtr := Pointer(Temp);

    for I := 0 to Len - 1 do
    begin
     ReadCellInfo(DataPtr^, InfoPtr^);
     Inc(DataPtr, FCellSize);
     Inc(InfoPtr);
    end;

    ReallocMem(FData, Len * Value);

    DataPtr := FData;
    InfoPtr := Pointer(Temp);

    FCellSize := Value;
    for I := 0 to Len - 1 do
    begin
     WriteCellInfo(DataPtr^, InfoPtr^);
     Inc(DataPtr, FCellSize);
     Inc(InfoPtr);
    end;
    Exit;
   end else
   begin
    Len := Len * Value;
    ReallocMem(FData, Len);
    FillChar(FData^, Len, 0);
   end;
  end else
  begin
   FreeMem(FData);
   FData := nil;
  end;

  FCellSize := Value;
  TilesFilled := False;
 end;
end;

procedure TMapLayer.SetIndexMask(Value: LongWord);
var
 NewFmt: TLayerFormatRec;
begin
 if FIndexMask <> Value then
 begin
  FillLayerFormat(NewFmt);
  NewFmt.lfIndexMask := Value;
  UpdateFormat(NewFmt);
 end;
end;

procedure TMapLayer.SetIndexShift(Value: ShortInt);
var
 NewFmt: TLayerFormatRec;
begin
 if FIndexShift <> Value then
 begin
  FillLayerFormat(NewFmt);
  NewFmt.lfIndexShift := Value;
  UpdateFormat(NewFmt);
 end;
end;

procedure TMapLayer.SetPaletteIndexMask(Value: LongWord);
var
 NewFmt: TLayerFormatRec;
begin
 if FPaletteIndexMask <> Value then
 begin
  FillLayerFormat(NewFmt);
  NewFmt.lfPaletteIndexMask := Value;
  UpdateFormat(NewFmt);
 end;
end;

procedure TMapLayer.SetPaletteIndexShift(Value: ShortInt);
var
 NewFmt: TLayerFormatRec;
begin
 if FPaletteIndexShift <> Value then
 begin
  FillLayerFormat(NewFmt);
  NewFmt.lfPaletteIndexShift := Value;
  UpdateFormat(NewFmt);
 end;
end;

procedure TMapLayer.SetPriorityMask(Value: LongWord);
var
 NewFmt: TLayerFormatRec;
begin
 if FPriorityMask <> Value then
 begin
  FillLayerFormat(NewFmt);
  NewFmt.lfPriorityMask := Value;
  UpdateFormat(NewFmt);
 end;
end;

procedure TMapLayer.SetPriorityShift(Value: ShortInt);
var
 NewFmt: TLayerFormatRec;
begin
 if FPriorityShift <> Value then
 begin
  FillLayerFormat(NewFmt);
  NewFmt.lfPriorityShift := Value;
  UpdateFormat(NewFmt);
 end;
end;

procedure TMapLayer.SetTileIndex(X, Y: Integer; Value: Integer);
begin
 with Owner as TMap do
 if (X >= 0) and
    (Y >= 0) and
    (X < Width) and
    (Y < Height) then
  WriteTileIndex(PByteArray(FData)[(Y * Width + X) * CellSize], Value);
end;

procedure TMapLayer.SetTileSetIndex(Value: Integer);
var
 OldTileSet: TTileSet;
 lst: TTileSetList;
begin
 OldTileSet := FTileSet;
 lst := (Owner.Owner as TBaseMapList).FTileSetList;
 if lst <> nil then
 begin
  FTileSet := lst.Sets[Value];
  TilesFilled := TilesFilled and (OldTileSet <> FTileSet);
 end else
 begin
  FTileSet := nil;
  TilesFilled := False;
 end;
 if Owner.Owner is TBrushList then
  TBrushList(Owner.Owner).IdentitiesFilled := False;
end;

procedure TMapLayer.SetTilesFilled(Value: Boolean);
var
 I: Integer;
 P: PByte;
begin
 if not Value then
 begin
  Finalize(QuickTiles);
  FillChar(FVisibleRect, SizeOf(TRect), 0);
 end else
 if (QuickTiles = nil) and (FTileSet <> nil) and (LayerData <> nil) then
 begin
  P := LayerData;
  SetLength(QuickTiles, LayerDataSize);
  for I := 0 to Length(QuickTiles) - 1 do
  begin
   QuickTiles[I] := FTileSet.Indexed[ReadTileIndex(P^)];
   Inc(P, FCellSize);
  end;
  with Owner as TMap do
   GetValue32Rect(FVisibleRect, QuickTiles, Width, Height,
            LongWord(TileSet.EmptyTile));
 end;
end;

procedure TMapLayer.SetXFlipShift(Value: ShortInt);
var
 NewFmt: TLayerFormatRec;
begin
 if FXFlipShift <> Value then
 begin
  FillLayerFormat(NewFmt);
  NewFmt.lfXFlipShift := Value;
  UpdateFormat(NewFmt);
 end;
end;

procedure TMapLayer.SetYFlipShift(Value: ShortInt);
var
 NewFmt: TLayerFormatRec;
begin
 if FYFlipShift <> Value then
 begin
  FillLayerFormat(NewFmt);
  NewFmt.lfYFlipShift := Value;
  UpdateFormat(NewFmt);
 end;
end;

procedure TMapLayer.UpdateFormat(const NewFormat: TLayerFormatRec);
var
 Temp: array of TCellInfo;
 Len, I: Integer;
 DataPtr: PByte;
 InfoPtr: ^TCellInfo;
begin
 with Owner as TMap do
  Len := FWidth * FHeight;

 SetLength(Temp, Len);

 if FData <> nil then
 begin
  DataPtr := FData;
  InfoPtr := Pointer(Temp);

  for I := 0 to Len - 1 do
  begin
   ReadCellInfo(DataPtr^, InfoPtr^);
   Inc(DataPtr, FCellSize);
   Inc(InfoPtr);
  end;
 end;

 Move(NewFormat, FLayerFormatStart, SizeOf(TLayerFormatRec));

 ReallocMem(FData, Len * FCellSize);

 DataPtr := FData;
 InfoPtr := Pointer(Temp);

 for I := 0 to Len - 1 do
 begin
  WriteCellInfo(DataPtr^, InfoPtr^);
  Inc(DataPtr, FCellSize);
  Inc(InfoPtr);
 end;
end;

procedure TMapLayer.WriteCellInfo(var Data; const Info: TCellInfo);
begin
 CellInfoWrite(TLayerFormatRec(Addr(FLayerFormatStart)^), Data, Info);
 TilesFilled := False;
end;

procedure TMapLayer.WriteData(Stream: TStream; var Header: TSectionHeader);
var
 lh: ^TLayerHeader;
 Rec: TProgressRec;
begin
 inherited;
 lh := Addr(Header.dh);
 Rec.Stream := Stream;
 Rec.Header := @Header;
 Rec.ProgressID := SPID_WRITE_DATA;

 Write(Rec, FData^, LayerDataSize);
end;

procedure TMapLayer.WritePaletteIndex(var Data; Value: LongInt);
begin
 PaletteIndexWrite(TLayerFormatRec(Addr(FLayerFormatStart)^), Data, Value);
end;

procedure TMapLayer.WritePriority(var Data; Value: LongInt);
begin
 PriorityWrite(TLayerFormatRec(Addr(FLayerFormatStart)^), Data, Value);
end;

procedure TMapLayer.WriteTileIndex(var Data; Value: LongInt);
begin
 TileIndexWrite(TLayerFormatRec(Addr(FLayerFormatStart)^), Data, Value);
 TilesFilled := False;
end;

procedure TMapLayer.WriteXFlip(var Data; Value: LongBool);
begin
 XFlipWrite(TLayerFormatRec(Addr(FLayerFormatStart)^), Data, Value);
end;

procedure TMapLayer.WriteYFlip(var Data; Value: LongBool);
begin
 YFlipWrite(TLayerFormatRec(Addr(FLayerFormatStart)^), Data, Value);
end;

{ TMap }

procedure TMap.Assign(Source: TNode);
var
 Src: TMap absolute Source;
begin
 if Source <> Self then
 begin
  if Source is TMap then
  begin
   FMapX := Src.FMapX;
   FMapY := Src.FMapY;
   FWidth := Src.FWidth;
   FHeight := Src.FHeight;
   FTileWidth := Src.FTileWidth;
   FTileHeight := Src.FTileHeight;
   if (Owner is TBaseMapList) and
      (secCustom in TBaseMapList(Owner).FInternalFlags) then
    ColorTableIndex := Src.ColorTableIndex else
    FColorTable := Src.FColorTable;
  end;
  inherited;
 end;
end;

procedure TMap.AssignMapInfo(Source: TMap);
var
 SrcNode: TNode;
begin
 if Source <> nil then
 begin
  Clear;
  FWidth := Source.FWidth;
  FHeight := Source.FHeight;
  FTileWidth := Source.FTileWidth;
  FTileHeight := Source.FTileHeight;
  if (Owner is TBaseMapList) and
     (secCustom in TBaseMapList(Owner).FInternalFlags) then
   ColorTableIndex := Source.ColorTableIndex else
   FColorTable := Source.FColorTable;
  SrcNode := Source.RootNode;
  while SrcNode <> nil do
  begin
   (AddNode as TMapLayer).AssignLayerInfo(SrcNode as TMapLayer);
   SrcNode := SrcNode.Next;
  end;
 end else
  raise EMapError.Create('Cannot assign map info if source map is undefined');
end;

function TMap.CheckHeaderSize(var Size: Integer): Boolean;
begin
 Result := (Size = SizeOf(TMapHeaderV1)) or inherited CheckHeaderSize(Size);
end;

procedure TMap.FillHeader(var Header: TSectionHeader);
var
 mh: ^TMapHeader;
begin
 mh := Addr(Header.dh);
 mh.mapLayersCount := Count;
 FillChar(mh.mapReserved, SizeOf(mh.mapReserved), 0);
 mh.mapOffsetX := FMapX;
 mh.mapOffsetY := FMapY;
 mh.mapWidth := FWidth;
 mh.mapHeight := FHeight;
 mh.mapTileWidth := FTileWidth;
 mh.mapTileHeight := FTileHeight;
 mh.mapColorTblIdx := ColorTableIndex;
 mh.mapNameLength := Min(Length(FName), FMaxNameLength);
end;

function TMap.FindLayerByTileSet(ATileSet: TTileSet): TMapLayer;
var
 I: Integer;
begin
 for I := 0 to Count - 1 do
 begin
  Result := Nodes[I] as TMapLayer;
  if Result.FTileSet = ATileSet then
   Exit;
 end;
 Result := nil;
end;

function TMap.GetCtIndex: Integer;
begin
 if FColorTable <> nil then
  Result := FColorTable.Index else
  Result := -1;
end;

function TMap.GetLayer(Index: Integer): TMapLayer;
begin
 Result := Nodes[Index] as TMapLayer;
end;

procedure TMap.Initialize;
begin
 FAssignableClass := TMap;
 FNodeClass := TMapLayer;
 FSignature := MAP_SIGN;
 FHeaderSize := SizeOf(TMapHeader);
 MaxCount := 32;
 FMaxNameLength := High(Word); 
end;

function TMap.IsTileSetUsed(ATileSet: TTileSet): Boolean;
var
 J: Integer;
begin
 if ATileSet <> nil then
  for J := 0 to Count - 1 do
   with Nodes[J] as TMapLayer do
    if ATileSet = TileSet then
    begin
     Result := True;
     Exit;
    end;

 Result := False;
end;

function TMap.ReadHeader(Stream: TStream; var Header: TSectionHeader): Boolean;
var
 V1: ^TMapHeaderV1;
 V2: ^TMapHeaderV2 absolute V1;
begin
 Result := inherited ReadHeader(Stream, Header);
 if Result then
 begin
  V1 := Addr(Header.dh);
  case Header.shHeaderSize of
   SizeOf(TMapHeaderV1):
   begin
    FMapX := V1.mapOffsetX;
    FMapY := V1.mapOffsetY;
    FWidth := V1.mapWidth;
    FHeight := V1.mapHeight;
    FTileWidth := V1.mapTileWidth;
    FTileHeight := V1.mapTileHeight;
    ColorTableIndex := V1.mapColorTblIdx;
    SetLength(FName, V1.mapNameLength);
   end;
   else
   if Header.shHeaderSize >= SizeOf(TMapHeaderV2) then
   begin
    FMapX := V2.mapOffsetX;
    FMapY := V2.mapOffsetY;
    FWidth := V2.mapWidth;
    FHeight := V2.mapHeight;
    FTileWidth := V2.mapTileWidth;
    FTileHeight := V2.mapTileHeight;
    ColorTableIndex := V2.mapColorTblIdx;
    SetLength(FName, V2.mapNameLength);
   end else Result := False;
  end;
 end;
end;

function TMap.ReadSetCount(var Header: TSectionHeader): LongInt;
var
 V1: ^TMapHeaderV1;
 V2: ^TMapHeaderV2 absolute V1;
begin
 V1 := Addr(Header.dh);
 case Header.shHeaderSize of
  SizeOf(TMapHeaderV1): Result := V1.mapLayersCount;
  SizeOf(TMapHeaderV2): Result := V2.mapLayersCount;
  else                  Result := 0;
 end;
end;

procedure TMap.SetCtIndex(Value: Integer);
var
 lst: TColorTableList;
begin
 lst := (Owner as TBaseMapList).FColorTableList;
 if lst <> nil then
  FColorTable := lst.Items[Value] else
  FColorTable := nil;
end;

procedure TMap.SetHeight(Value: Integer);
begin
 SetMapSize(FWidth, Value);
end;

procedure TMap.SetMapSize(AWidth, AHeight: Integer);
var
 I, X, Y, MW, Len, NewLen: Integer;
 SrcStride, DstStride: Integer;
 Temp: array of TCellInfo;
 DataPtr: PByte;
 InfoPtr: ^TCellInfo;
begin
 if (FWidth <> AWidth) or (FHeight <> AHeight) then
 begin
  Len := FWidth * FHeight;
  SetLength(Temp, Len);

  for I := 0 to Count - 1 do
   with Layers[I] do
   begin
    if (FData <> nil) and (Len > 0) then
    begin
     DataPtr := FData;
     InfoPtr := Pointer(Temp);

     for X := 0 to Len - 1 do
     begin
      ReadCellInfo(DataPtr^, InfoPtr^);
      Inc(DataPtr, FCellSize);
      Inc(InfoPtr);
     end;
    end;

    NewLen := AWidth * FCellSize * AHeight;
    ReallocMem(FData, NewLen);
    FillChar(FData^, NewLen, 0);

    MW := Min(AWidth, FWidth);
    SrcStride := FWidth - MW;
    DstStride := (AWidth - MW) * FCellSize;

    InfoPtr := Pointer(Temp);
    DataPtr := FData;

    for Y := 0 to Min(AHeight, FHeight) - 1 do
    begin
     for X := 0 to MW - 1 do
     begin
      WriteCellInfo(DataPtr^, InfoPtr^);
      Inc(DataPtr, FCellSize);
      Inc(InfoPtr);
     end;
     Inc(InfoPtr, SrcStride);
     Inc(DataPtr, DstStride);
    end;
    TilesFilled := False;
   end;
  FWidth := AWidth;
  FHeight := AHeight;
 end;
end;

procedure TMap.SetWidth(Value: Integer);
begin
 SetMapSize(Value, FHeight);
end;

procedure TMap.UnhookTileSet(ATileSet: TTileSet);
var
 J: Integer;
begin
 for J := 0 to Count - 1 do
  with Nodes[J] as TMapLayer do
   if ATileSet = FTileSet then
    FTileSet := nil;
end;

{ TMapList }

procedure TMapList.Assign(Source: TNode);
var
 Src: TBaseMapList absolute Source;
 OldInternalFlags: set of TInternalFlags;
begin
 if Source <> Self then
 begin
  CheckSource(Source);
  FColorTableList.Assign(Src.FColorTableList);
  FTileSetList.Assign(Src.FTileSetList);
  if Source is TMapList then
  begin
   FBrushList.TileSetList := FTileSetList;
   FBrushList.ColorTableList := FColorTableList;
   with FBrushList do
   begin
    OldInternalFlags := FInternalFlags;
    Include(FInternalFlags, secCustom);
    try
     Assign(TMapList(Source).FBrushList);
    finally
     FInternalFlags := OldInternalFlags;    
    end;
   end;
  end;
  OldInternalFlags := FInternalFlags;
  Include(FInternalFlags, secCustom);
  try
   inherited;
  finally
   FInternalFlags := OldInternalFlags;
  end;
 end;
end;

function TMapList.CalculateChecksum: TChecksum;
begin
 Result := FColorTableList.Checksum +
           FTileSetList.Checksum +
           FBrushList.Checksum +
           inherited CalculateChecksum;
end;

function TMapList.CalculateDataSize: LongInt;
begin
 Result := FColorTableList.TotalSize +
           FTileSetList.TotalSize +
           FBrushList.TotalSize +
           inherited CalculateDataSize;
end;

function TMapList.CheckHeaderSize(var Size: Integer): Boolean;
begin
 Result := (Size = SizeOf(TListDataHeaderV1)) or inherited CheckHeaderSize(Size);
end;

procedure TMapList.ClearData;
begin
 if not (secCustom in FInternalFlags) then
 begin
  FBrushList.Clear;
  FColorTableList.Clear;
  FTileSetList.Clear;
 end;
end;

destructor TMapList.Destroy;
begin
 inherited;
 FBrushList.Free;
 FTileSetList.Free;
 FColorTableList.Free;
end;

procedure TMapList.Initialize;
begin
 inherited;
 FColorTableList := TColorTableList.Create;
 FColorTableList.Owner := Self;
 FColorTableList.MaxCount := High(SmallInt) + 1;
 FTileSetList := TTileSetList.Create;
 FTileSetList.Owner := Self;
 FTileSetList.MaxCount := High(SmallInt) + 1;
 FBrushList := TBrushList.Create;
 FBrushList.Owner := Self;
 FBrushList.FColorTableList := FColorTableList;
 FBrushList.FTileSetList := FTileSetList;
 FSignature := MLST_SIGN;
end;

procedure TMapList.Progress(Sender: TObject; const Rec: TProgressRec);
var
 lst: TBaseSectionedList absolute Sender;
 //sec: TBaseSectionedList;
// tset: TTileSet;
{ Yes: Boolean;
 Cl: Byte;
 I, AddSize: Integer; }
begin
 if Sender = Self then
 begin
  if Rec.Header.shSignature = CLST_SIGN then
  case Rec.ProgressID of
   SPID_READ_DATA_END:
   begin
    Include(FInternalFlags, secSkipChecksumTest);
    Include(FInternalFlags, secSkipDataSizeTest);
{    Rec.Header.shDataSize := 0;
     // will raise an exception ESectionDataSizeError, must be handled by user
    Rec.Header.shChecksum := 0;
     // will raise an exception ESectionChecksumError, must be handled by user
 } end;
  end;
 end else
 begin
 { if (Sender is TColorTable) or
     (Sender is TTileSet) or
     (Sender is TMapLayer) then
  begin
   sec := Pointer(Sender);
//   Sender := TNode(Sender).Owner;
  end else
   sec := nil;   }
  {
  if Sender is TTileSet then
  begin
   tset := Pointer(Sender);
   Sender := TNode(Sender).Owner;
  end else
   tset := nil; }

  if FBrushList = nil then
{   ((Sender is TColorTableList) or
     (Sender is TTileSetList) or
     (Sender is TMap)) and
     (lst.Owner = Self) and
     (FBrushList = nil) then}
  case Rec.ProgressID of
   SPID_READ_SEC_HEADER_END: Rec.Stream.Seek(-8, soFromCurrent);
   SPID_READ_DATA_END:
 {  if sec <> nil then
   begin
    Include(sec.FInternalFlags, secSkipChecksumTest);
    Include(sec.FInternalFlags, secSkipDataSizeTest);
 //   Rec.Header.shDataSize := sec.DataSize;
 //   Rec.Header.shChecksum := sec.Checksum;
   end else
 {  if tset <> nil then with tset do
   begin
    Include(tset.FInternalFlags, secSkipChecksumTest);
    Include(tset.FInternalFlags, secSkipDataSizeTest);
 {   AddSize := 0;
    if Count > 0 then
    begin
     Yes := True;
     for I := 0 to TilesCount - 1 do
      if IndexedItems[I].TileIndex <> I then
      begin
       Yes := False;
       Break;
      end;
     if Yes then
      Inc(AddSize, Count * 4) else
      Yes := True;

     Cl := (RootNode as TTileItem).FirstColor;
     for I := 1 to Count - 1 do
      if Items[I].FirstColor <> Cl then
      begin
       Yes := False;
       Break;
      end;
     if Yes then Inc(AddSize, Count);
    end;
    Rec.Header.shDataSize := DataSize + AddSize;
    Rec.Header.shChecksum := Checksum;    }
  // end else
   begin
    Include(lst.FInternalFlags, secSkipChecksumTest);
    Include(lst.FInternalFlags, secSkipDataSizeTest);
  //  Rec.Header.shDataSize := lst.DataSize - lst.Count * 8;
 //   Rec.Header.shChecksum := lst.Checksum;
   end;
  end;
 end;
 inherited;
end;

procedure TMapList.ReadData(Stream: TStream; var Header: TSectionHeader);
var
 h: packed record
     Signature: LongWord;
     HeaderSize: LongInt;
     h: TListDataHeaderV1;
    end;
 SavePos: Int64;
 BL: TBrushList;
begin
 if Header.shSignature <> CLST_SIGN then
 begin
  FColorTableList.LoadFromStream(Stream);
  FTileSetList.LoadFromStream(Stream);
  FBrushList.LoadFromStream(Stream);
  inherited;
 end else
 begin
  SavePos := Stream.Position;
  if (Stream.Read(h, SizeOf(h)) <> SizeOf(h)) or
     (h.Signature <> MLST_SIGN) or
     (h.HeaderSize <> SizeOf(TListDataHeaderV1)) then
   SectionSignatureError(SavePos, Stream.Position);
  Count := h.h.lshItemsCount;
  BL := FBrushList;
  FBrushList := nil;
  inherited;
  FBrushList := BL;
 end;
end;

function TMapList.ReadHeader(Stream: TStream; var Header: TSectionHeader): Boolean;
var
 BL: TBrushList;
begin
 if Header.shSignature = CLST_SIGN then
 begin
  Clear;
  BL := FBrushList;
  FBrushList := nil;
  FColorTableList.LoadFromStream(Stream);
  FTileSetList.LoadFromStream(Stream);
  FBrushList := BL;
  Result := True;
 end else
  Result := inherited ReadHeader(Stream, Header);
end;

function TMapList.ReadSectionHeader(Stream: TStream;
         var Header: TSectionHeader): Boolean;
begin
 Result := Stream.Read(Header, 8) = 8;
 if Result then
 begin
  if Header.shSignature = CLST_SIGN then
  begin
   Result := Header.shHeaderSize = SizeOf(TListDataHeaderV1);
   Stream.Seek(-8, soFromCurrent);
  end else
  begin
   Result := (Stream.Read(Header.shDataSize, 8) = 8) and
             CheckSignature(Header.shSignature) and
             CheckHeaderSize(Header.shHeaderSize);
   if Result then
    OperationProgress(Stream, Header, SPID_READ_SECTION_HEADER, 16);
  end;
 end;
end;

procedure TMapList.SetOnProgress(Value: TSectionProgressEvent);
begin
 FOnProgress := Value;
 FColorTableList.OnProgress := Value;
 FTileSetList.OnProgress := Value;
 FBrushList.OnProgress := Value;
end;

procedure TMapList.WriteData(Stream: TStream; var Header: TSectionHeader);
begin
 FColorTableList.SaveToStream(Stream);
 FTileSetList.SaveToStream(Stream);
 FBrushList.SaveToStream(Stream);
 inherited;
end;

{ TBaseMapList }

function TBaseMapList.AddMap(const AName: WideString; AWidth, AHeight,
  ATileW, ATileH: Integer): TMap;
begin
 Result := AddNode as TMap;
 Result.Name := AName;
 Result.FWidth := AWidth;
 Result.FHeight := AHeight;
 if ATileW < 1 then ATileW := 1;
 if ATileH < 1 then ATileH := 1;
 Result.FTileWidth := ATileW;
 Result.FTileHeight := ATileH;
end;

type
 PLockRec = ^TLockRec;
 TLockRec = record
  Checksum: TCheckSum;
  DataSize: LongInt;
 end;

procedure TBaseMapList.ColorTablesLock;
var
 Map: TNode;
 Rec: PLockRec;
begin
 Map := RootNode;
 while Map <> nil do
 begin
  with Map as TMap do
  begin
   New(Rec);
   if FColorTable = nil then
   begin
    Rec.Checksum := 0;
    Rec.DataSize := 0;
   end else
   begin
    Rec.Checksum := FColorTable.Checksum;
    Rec.DataSize := FColorTable.DataSize;
   end;
   FColorTable := Pointer(Rec);
  end;
{   if FColorTable = nil then
    FColorTable := Pointer(-1) else
    FColorTable := Pointer(FColorTable.Index);}

  Map := Map.Next;
 end;
end;

procedure TBaseMapList.ColorTablesUnlock;
var
 Map: TNode;
 Rec: PLockRec;
begin
 Map := RootNode;
 while Map <> nil do
 begin
  with Map as TMap do
  begin
   Rec := PLockRec(FColorTable);
   FColorTable := FColorTableList.FindByContent(Rec.Checksum,
        Rec.DataSize) as TColorTable;
   Dispose(Rec);
  end;
 {  if (Integer(FColorTable) < 0) or
      (FColorTableList = nil) then
    FColorTable := nil else
    FColorTable := FColorTableList[Integer(FColorTable)];}

  Map := Map.Next;
 end;
end;

function TBaseMapList.GetMap(Index: Integer): TMap;
begin
 Result := Nodes[Index] as TMap;
end;

procedure TBaseMapList.Initialize;
begin
 inherited;
 FAssignableClass := TBaseMapList;
 FNodeClass := TMap;
 MaxCount := High(SmallInt) + 1;
 FSignature := MAPS_SIGN;
 FHeaderSize := SizeOf(TListDataHeaderV1); 
end;

function TBaseMapList.IsColorTableUsed(AColorTable: TColorTable): Boolean;
var
 J: Integer;
begin
 if AColorTable <> nil then
  for J := 0 to Count - 1 do
   with Nodes[J] as TMap do
    if AColorTable = ColorTable then
    begin
     Result := True;
     Exit;
    end;
 Result := False;
end;

function TBaseMapList.IsTileSetUsed(ATileSet: TTileSet): Boolean;
var
 J: Integer;
begin
 if ATileSet <> nil then
  for J := 0 to Count - 1 do
   if (Nodes[J] as TMap).IsTileSetUsed(ATileSet) then
   begin
    Result := True;
    Exit;
   end;
 Result := False;
end;

procedure TBaseMapList.TileSetsLock;
var
 Layer: TNode;
 Map: TNode;
 Rec: PLockRec;
begin
 Map := RootNode;
 while Map <> nil do
 begin
  with Map as TMap do
   Layer := RootNode;
  while Layer <> nil do
  begin
   with Layer as TMapLayer do
   begin
    New(Rec);
    if FTileSet = nil then
    begin
     Rec.Checksum := 0;
     Rec.DataSize := 0;
    end else
    begin
     Rec.Checksum := FTileSet.Checksum;
     Rec.DataSize := FTileSet.DataSize;
    end;
    FTileSet := Pointer(Rec);
   end;
{    if FTileSet = nil then
     FTileSet := Pointer(-1) else
     FTileSet := Pointer(FTileSet.Index);}

   Layer := Layer.Next;
  end;
  Map := Map.Next;
 end;
end;

procedure TBaseMapList.TileSetsUnlock;
var
 Layer: TNode;
 Map: TNode;
 Rec: PLockRec;
begin
 Map := RootNode;
 while Map <> nil do
 begin
  with Map as TMap do
   Layer := RootNode;
  while Layer <> nil do
  begin
   with Layer as TMapLayer do
   begin
    Rec := PLockRec(FTileSet);
    FTileSet := FTileSetList.FindByContent(Rec.Checksum, Rec.DataSize) as TTileSet;
    Dispose(Rec);
   { if (Integer(FTileSet) < 0) or
       (FTileSetList = nil) then
     FTileSet := nil else
     FTileSet := FTileSetList[Integer(FTileSet)];}
   end;  

   Layer := Layer.Next;
  end;
  Map := Map.Next;
 end;
end;

procedure TBaseMapList.UnhookColorTable(AColorTable: TColorTable);
var
 J: Integer;
begin
 for J := 0 to Count - 1 do
  with Nodes[J] as TMap do
   if AColorTable = FColorTable then
    FColorTable := nil;
end;

procedure TBaseMapList.UnhookTileSet(ATileSet: TTileSet);
var
 J: Integer;
begin
 for J := 0 to Count - 1 do
  (Nodes[J] as TMap).UnhookTileSet(ATileSet);
end;

{ TBrushList }

procedure TBrushList.Changed;
var
 Brush: TMapBrush;
begin
 inherited;
 if not (secLoading in FInternalFlags) then
 begin
  Brush := RootNode as TMapBrush;
  while Brush <> nil do
  begin
   if not IsChild(Brush.FExtends) then
    Brush.FExtends := nil;
   Brush.Changed;
   Brush := Brush.Next as TMapBrush;
  end;
 end else
  IdentitiesFilled := False;
 CleanUpTails;  
end;

procedure TBrushList.CleanUpTails;
var
 Brush: TMapBrush;
 Layer: TBrushLayer;
begin
 Brush := RootNode as TMapBrush;
 while Brush <> nil do
 begin
  Layer := Brush.RootNode as TBrushLayer;
  while Layer <> nil do
  begin
   Layer.TailsFilled := False;
   Layer := Layer.Next as TBrushLayer;
  end;
  Brush := Brush.Next as TMapBrush;
 end;
end;

function TBrushList.EraseBrushContentAt(Dest: TMap; const DestRect: TRect): Boolean;

 procedure EraseBrushLayer(Layer: TBrushLayer;
                           DestX, DestY: Integer);
 var
  EmptyTileIndex: Integer;
  Rect, LRect: TRect;
  W, H, J, FI, WW, HH, IX, IY, XX, YY: Integer;
  Tail: TBrushLayer;
  pt: TPoint;
  Found: Boolean;
 // Tails: TBrushLayerTails;
  FoundList: array of TPoint;
  rd: TLinkDirection;
  DestLayer: TMapLayer;
 begin
  DestLayer := Dest.FindLayerByTileSet(Layer.TileSet);
  if DestLayer = nil then Exit;
  DestLayer.TileSet.MaxIndex := DestLayer.IndexMask;
  EmptyTileIndex := DestLayer.TileSet.EmptyTileIndex;
  with Layer.VisibleRect do
  begin
   W := Right - Left;
   H := Bottom - Top;

   IX := DestX - (Layer.FHotSpotX - Left);
   IY := DestY - (Layer.FHotSpotY - Top);
   with Dest do
    if (IX >= Width) or
       (IY >= Height) or
       (IX + W <= 0) or
       (IY + H <= 0) then
     Exit;
   for YY := 0 to H - 1 do
    for XX := 0 to W - 1 do
     DestLayer.TileIndex[XX + IX, YY + IY] := EmptyTileIndex;
  end;

  Layer.TailsFilled := True;
  SetLength(FoundList, Length(Layer.Tails));
  FI := 0;
  for J := 0 to Length(Layer.Tails) - 1 do
   with Layer.Tails[J] do
   begin
    pt.X := DestX + bloDistance.x;
    pt.Y := DestY + bloDistance.y;

    Found := False;
    for XX := 0 to FI - 1 do
     with FoundList[XX] do
      if (X = pt.x) and (Y = pt.Y) then
      begin
       Found := True;
       Break;
      end;
    if Found then Continue;

    if DestLayer.TileSet <> bloLayer.TileSet then
     DestLayer := Dest.FindLayerByTileSet(bloLayer.TileSet);

    if (DestLayer <> nil) and
       bloLayer.SitsHere(DestLayer, pt.X, pt.Y) then
    begin
     FoundList[FI] := pt;
     Inc(FI);

     rd := DirReverse[bloDirection];
     Tail := PLayerArray(Addr(bloLayer.FLinksStart))[rd];

     if (Tail <> nil) and
        (Tail <> bloLayer) and
        (Tail.TileSet <> nil) then
     begin
      if (bloDirection in Tail.FUnfinished) and
         (Tail.FUnfinished - [bloDirection] = []) then
      begin
       Rect := Tail.VisibleRect;
       WW := Rect.Right - Rect.Left;
       HH := Rect.Bottom - Rect.Top;
       with bloLayer.DrawSpots[rd] do
       begin
        XX := (pt.X + x) - (Tail.FHotSpotX - Rect.Left);
        YY := (pt.Y + y) - (Tail.FHotSpotY - Rect.Top);
       end;
       if not IntersectRect(LRect, Bounds(XX, YY, WW, HH), DestRect) then
       begin
        if DestLayer.TileSet <> Tail.TileSet then
         DestLayer := Dest.FindLayerByTileSet(Tail.TileSet);
         
        if DestLayer <> nil then
         for IY := 0 to HH - 1 do
          for IX := 0 to WW - 1 do
           DestLayer.Cells[IX + XX, IY + YY] :=
            Tail.Cells[IX + Rect.Left, IY + Rect.Top];
        Continue;
       end;
      end;
      EraseBrushLayer(bloLayer, pt.X, pt.Y);
     end;
    end;
   end;
 end; // TBrushList.EraseBrushContentAt.EraseBrushLayer

var
 J, K, X, Y: Integer;
 Layer: TBrushLayer;
 DestLayer: TMapLayer;
 pt: TPoint;
begin
 Result := False;
 if Dest <> nil then
  for J := 0 to Count - 1 do
   with Brushes[J] do
    for K := 0 to Count - 1 do
    begin
     Layer := Nodes[K] as TBrushLayer;
     if Layer.TileSet <> nil then
     begin
      DestLayer := Dest.FindLayerByTileSet(Layer.TileSet);
      if DestLayer <> nil then
       for Y := DestRect.Top to DestRect.Bottom - 1 do
        for X := DestRect.Left to DestRect.Right - 1 do
        begin
         if Layer.SpottedInRect(DestLayer, Bounds(X, Y, 1, 1), pt) then
         begin
          EraseBrushLayer(Layer, pt.X, pt.Y);
          Result := True;
         end;
        end;
     end;
    end;
end;

function TBrushList.GetIdentitiesFilled: Boolean;
begin
 Result := IdentityList <> nil;
end;

function TBrushList.GetMapBrush(Index: Integer): TMapBrush;
begin
 Result := Nodes[Index] as TMapBrush;
end;

procedure TBrushList.Initialize;
begin
 inherited;
 FNodeClass := TMapBrush;
 FSignature := BLST_SIGN;
end;

function TBrushList.IsBrushUsed(ABrush: TMapBrush): Boolean;
var
 J: Integer;
begin
 if ABrush <> nil then
  for J := 0 to Count - 1 do
   with Nodes[J] as TMapBrush do
    if ABrush = FExtends then
    begin
     Result := True;
     Exit;
    end;
 Result := False;
end;

procedure TBrushList.ReadData(Stream: TStream; var Header: TSectionHeader);
var
 I: Integer;
 Brush: TMapBrush;
begin
 inherited;
 for I := 0 to Count - 1 do
 begin
  Brush := Nodes[I] as TMapBrush;
  Brush.ExtendsIndex := Integer(Brush.FExtends);
  Brush.RepointLinks;
 end;
end;

procedure TBrushList.SetIdentitiesFilled(Value: Boolean);
var
 TempList: array of TLayerIdentityRec;
function IsTwins(ALayer1, ALayer2: TBrushLayer): Boolean;
var
 I: Integer;
 lrx: TBrushLayer;
begin
 Result := ALayer1 = ALayer2;
 if not Result and (ALayer1 <> nil) and (ALayer2 <> nil) then
 begin
  for I := 0 to Length(TempList) - 1 do
   with TempList[I] do
   begin
    if ALayer1 = Layer1 then
     lrx := Layer2 else
    if ALayer1 = Layer2 then
     lrx := Layer1 else
     lrx := nil;

    if lrx = ALayer2 then
    begin
     Result := True;
     Exit;
    end;
  end;
 end;
end;
var
 L, K, I, XX, YY: Integer;
 fl, lrx: TBrushLayer;
 Brush, BExt: TMapBrush;
 Dir: TLinkDirection;
 Success: Boolean;
 Ptr: ^TLayerIdentityRec;
begin
 if not Value then
  Finalize(IdentityList) else
 if IdentityList = nil then
 begin
  Brush := RootNode as TMapBrush;
  while Brush <> nil do
  begin
   BExt := Brush.Extends;
   while BExt <> nil do
   begin
    for K := 0 to BExt.Count - 1 do
    begin
     fl := BExt.Nodes[K] as TBrushLayer;
     if fl.FIdentityTag <> 0 then
     begin
      with fl.VisibleRect do
      begin
       XX := fl.FHotSpotX - Left;
       YY := fl.FHotSpotY - Top;
      end;
      for I := 0 to Brush.Count - 1 do
      begin
       lrx := Brush.Nodes[I] as TBrushLayer;
       if (lrx.TileSet = fl.TileSet) and
          (lrx.FIdentityTag = fl.FIdentityTag) and
          (lrx.FUnfinished = fl.FUnfinished) and
          (lrx.FExtended = fl.FExtended) then
        with lrx.VisibleRect do
         if (XX = lrx.FHotSpotX - Left) and
            (YY = lrx.FHotSpotY - Top) then
         begin
          Success := True;
          for Dir := Low(TLinkDirection) to High(TLinkDirection) do
           if LongWord(lrx.DrawSpots[Dir]) <>
              LongWord(fl.DrawSpots[Dir]) then
           begin
            Success := False;
            Break;
           end;

          if Success then
          begin
           L := Length(TempList);
           SetLength(TempList, L + 1);
           with TempList[L] do
           begin
            Layer1 := fl;
            Layer2 := lrx;
           end;
           Break;
          end;
         end;
      end;
     end;
    end;
    BExt := BExt.Extends; 
   end;
   Brush := Brush.Next as TMapBrush;
  end;
  SetLength(IdentityList, Length(TempList));
  Ptr := Pointer(IdentityList);
  L := 0;
  for K := 0 to Length(TempList) - 1 do
   with TempList[K] do
   begin
    Success := True;
    for Dir := Low(TLinkDirection) to High(TLinkDirection) do
     if not IsTwins(PLayerArray(Addr(Layer1.FLinksStart))[Dir],
                    PLayerArray(Addr(Layer2.FLinksStart))[Dir]) then
     begin
      Success := False;
      Break;
     end;

    if Success then
    begin
     Ptr.Layer1 := Layer1;
     Ptr.Layer2 := Layer2;
     Inc(Ptr);
     Inc(L);
    end else
    begin
     Layer1 := nil;
     Layer2 := nil;
    end;
   end;
  SetLength(IdentityList, L);
 end;
end;

procedure TBrushList.UnhookBrush(ABrush: TMapBrush);
var
 J: Integer;
begin
 for J := 0 to Count - 1 do
  with Nodes[J] as TMapBrush do
   if ABrush = FExtends then
    FExtends := nil;
end;

{ TMapBrush }

procedure TMapBrush.Assign(Source: TNode);
var
 OldFlags: set of TInternalFlags;
begin
 if Source <> Self then
 begin
  OldFlags := FInternalFlags;
  Include(FInternalFlags, secLoading);
  try
   inherited;
   if Source is TMapBrush then
    AssignBrushInfo(TMapBrush(Source));
  finally
   FInternalFlags := OldFlags;
  end;
 end;
end;

procedure TMapBrush.AssignBrushInfo(Source: TMapBrush);
var
 X: Integer;
 I: TLinkDirection;
 Link: TBrushLayer;
begin
  if (Owner is TBaseMapList) and
     (secCustom in TBaseMapList(Owner).FInternalFlags) then
  ExtendsIndex := Source.ExtendsIndex else
  FExtends := Source.FExtends;
 for X := 0 to Count - 1 do
  with Nodes[X] as TBrushLayer do
   for I := Low(TLinkDirection) to High(TLinkDirection) do
   begin
    Link := PLayerArray(Addr((Source.Nodes[X] as TBrushLayer).FLinksStart))[I];
    if Source.IsChild(Link) then
     LinkIndex[I] := Link.Index;
   end;
end;

procedure TMapBrush.AssignMapInfo(Source: TMap);
begin
 inherited;
 if Source is TMapBrush then
  AssignBrushInfo(TMapBrush(Source));
end;

procedure TMapBrush.Changed;

 procedure CheckLink(Layer: TBrushLayer; var Link: TBrushLayer);
 begin
  if Link <> nil then
  begin
   if (not IsChild(Link) and
      ((FExtends = nil) or not FExtends.IsChild(Link))) then
    Link := nil;
  end;
 end;
 
var
 Layer: TBrushLayer;
begin
 inherited;
 if not (secLoading in FInternalFlags) then
 begin
  Layer := RootNode as TBrushLayer;
  while Layer <> nil do
  begin
   CheckLink(Layer, Layer.FLinkLeft);
   CheckLink(Layer, Layer.FLinkRight);
   CheckLink(Layer, Layer.FLinkUp);
   CheckLink(Layer, Layer.FLinkDown);
   CheckLink(Layer, Layer.FLinkLeftUp);
   CheckLink(Layer, Layer.FLinkRightUp);
   CheckLink(Layer, Layer.FLinkLeftDown);
   CheckLink(Layer, Layer.FLinkRightDown);
   Layer := Layer.Next as TBrushLayer;
  end;
 end;
 if Owner is TBrushList then
  TBrushList(Owner).IdentitiesFilled := False;
end;

function TMapBrush.CheckHeaderSize(var Size: Integer): Boolean;
begin
 Result := (Size = SizeOf(TMapHeader)) or inherited CheckHeaderSize(Size);
end;

function TMapBrush.CheckSignature(Value: LongWord): Boolean;
begin
 Result := (Value = MAP_SIGN) or inherited CheckSignature(Value);
end;

function TMapBrush.Draw(Dest: TMap; DestX, DestY: Integer): Boolean;
type
 TShortPoint = packed record
  x: ShortInt;
  y: ShortInt;
 end;
const
 PosShift: array[0..8] of TShortPoint = (
 (x:  0; y:  0), (x:  0; y: -1), (x:  0; y: +1),
 (x: -1; y:  0), (x: -1; y: -1), (x: -1; y: +1),
 (x: +1; y:  0), (x: +1; y: -1), (x: +1; y: +1));

var
 Layer: TBrushLayer;
 FindRect: TRect;
 pt: TPoint;
 dir: TLinkDirections;
 ld: TLinkDirection;
 dl: TBrushLayer;
 I: Integer;
 BrushList: TBrushList;
 DestLayer: TMapLayer;
begin
 Result := False;
 BrushList := Owner as TBrushList;
 BrushList.IdentitiesFilled := True;

 Layer := FindBrushLayerSittingHere(Dest, DestX, DestY);

 if Layer <> nil then
 begin
  // if exact layer is found, just redraw it or draw a random clone
  with Layer.RandomTwin do
  begin
   DestLayer := Dest.FindLayerByTileSet(TileSet);
   if DestLayer = nil then Exit;
   Result := Draw(DestLayer, DestX, DestY);
  end;
  Exit;
 end;

 // Searching layer at the nearest spot
 FindRect := Bounds(DestX - 1, DestY - 1, 3, 3);

 for I := 0 to 8 do
 begin
  with PosShift[I] do
   Layer := FindBrushLayer(Dest, DestX + x, DestY + y, pt);
  if Layer <> nil then Break;
 end;

 if Layer <> nil then
 begin
  dir := [];

  if pt.X > DestX then
   Include(dir, ldLeft) else
  if pt.X < DestX then
   Include(dir, ldRight);

  if pt.Y > DestY then
   Include(dir, ldUp) else
  if pt.Y < DestY then
   Include(dir, ldDown);

  if dir <> [] then
  begin
   if dir = [ldLeft] then
    ld := ldLeft else
   if dir = [ldRight] then
    ld := ldRight else
   if dir = [ldUp] then
    ld := ldUp else
   if dir = [ldDown] then
    ld := ldDown else
   if dir = [ldLeft, ldUp] then
    ld := ldLeftUp else
   if dir = [ldLeft, ldDown] then
    ld := ldLeftDown else
   if dir = [ldRight, ldUp] then
    ld := ldRightUp else
    ld := ldRightDown;

   dl := PLayerArray(Addr(Layer.FLinksStart))[ld];
   if dl <> nil then
    Layer.TailsFilled := True;

   if (dl = nil) or
      not ((ld in Layer.TailsDirections) or
      (DirRelate[0, ld] in Layer.TailsDirections) or
      (DirRelate[1, ld] in Layer.TailsDirections)) then
   begin
    if dl <> nil then
    begin
     with Layer.DrawSpots[ld] do
      with dl.RandomTwin do
      begin
       DestLayer := Dest.FindLayerByTileSet(TileSet);
       if DestLayer = nil then Exit;
       Result := Draw(DestLayer, pt.X + x, pt.Y + y);
      end;
    end else
    begin
     dl := RootNode as TBrushLayer;

     if (dl = nil) or
        IntersectRect(FindRect, Layer.RectOnMap(pt.X, pt.Y),
                             dl.RectOnMap(DestX, DestY)) then
     begin
      dl := Layer;
      DestX := pt.X;
      DestY := pt.Y;
     end;
      
     with dl.RandomTwin do
     begin
      DestLayer := Dest.FindLayerByTileSet(TileSet);
      if DestLayer = nil then Exit;
      Result := Draw(DestLayer, DestX, DestY);
     end;
    end;
   end else
    Result := Layer.DrawTail(Dest, pt.X, pt.Y, ld);
  end;
 end else
 begin
  Layer := RootNode as TBrushLayer;

  if Layer <> nil then
    with Layer.RandomTwin do
    begin
     DestLayer := Dest.FindLayerByTileSet(TileSet);
     if DestLayer = nil then Exit;
     Result := Draw(DestLayer, DestX, DestY);
    end;
 end;
end;

procedure TMapBrush.FillHeader(var Header: TSectionHeader);
var
 bh: ^TBrushHeader;
 i: SmallInt;
begin
 inherited;
 bh := Addr(Header.dh);
 I := ExtendsIndex;
 bh.MainHeader.mapReserved[1] := I;
 bh.MainHeader.mapReserved[2] := I shr 8;
 bh.brReserved := 0;
end;

function TMapBrush.FindBrushLayer(Dest: TMap; X, Y: Integer;
           var pt: TPoint): TBrushLayer;
var
 Node: TNode;
 Brush: TMapBrush;
begin
 Result := nil;
 if Dest <> nil then
 begin
  Result := FindExactBrushLayer(Dest, X, Y, pt);

  if Result = nil then
  begin
   Brush := Extends;
   while Brush <> nil do
   begin
    Result := Brush.FindExactBrushLayer(Dest, X, Y, pt);
    if Result <> nil then
     Exit;
    Brush := Brush.Extends;
   end;
  end;

  if Result = nil then
   with Owner as TBrushList do
   begin
    Node := RootNode;
    while Node <> nil do
    begin
     with Node as TMapBrush do
     begin
      Brush := Extends;
      while Brush <> nil do
      begin
       if Brush = Self then
       begin
        Result := FindExactBrushLayer(Dest, X, Y, pt);
        if Result <> nil then
         Exit else
         Break;
       end;
       Brush := Brush.Extends;
      end;
     end;
     Node := Node.Next;
    end;
   end;
 end;
end;

function TMapBrush.FindBrushLayerSittingHere(Dest: TMap; X,
  Y: Integer): TBrushLayer;
var
 Node: TNode;
 Brush: TMapBrush;
begin
 Result := nil;
 if Dest <> nil then
 begin
  Result := FindExactBrushLayerSittingHere(Dest, X, Y);

  if Result = nil then
  begin
   Brush := Extends;
   while Brush <> nil do
   begin
    Result := Brush.FindExactBrushLayerSittingHere(Dest, X, Y);
    if Result <> nil then
     Exit;
    Brush := Brush.Extends;
   end;
  end;

  if Result = nil then
   with Owner as TBrushList do
   begin
    Node := RootNode;
    while Node <> nil do
    begin
     with Node as TMapBrush do
     begin
      Brush := Extends;
      while Brush <> nil do
      begin
       if Brush = Self then
       begin
        Result := FindExactBrushLayerSittingHere(Dest, X, Y);
        if Result <> nil then
         Exit else
         Break;
       end;
       Brush := Brush.Extends;
      end;
     end;
     Node := Node.Next;
    end;
   end;
 end;
end;

function TMapBrush.FindExactBrushLayer(Dest: TMap; X, Y: Integer;
  var pt: TPoint): TBrushLayer;
var
 FindRect: TRect;
 DestLayer: TMapLayer;
begin
 if Dest <> nil then
 begin
  FindRect := Bounds(X, Y, 1, 1);
  Result := RootNode as TBrushLayer;
  DestLayer := nil;
  while Result <> nil do
  begin
   if (DestLayer = nil) or (DestLayer.TileSet <> Result.TileSet) then
    DestLayer := Dest.FindLayerByTileSet(Result.TileSet);

   if (DestLayer <> nil) and
      Result.SpottedInRect(DestLayer, FindRect, pt) then
    Break;

   Result := Result.Next as TBrushLayer;
  end;
 end else
  Result := nil;
end;

function TMapBrush.FindExactBrushLayer(DestLayer: TMapLayer; X,
  Y: Integer; var pt: TPoint): TBrushLayer;
var
 FindRect: TRect;
begin
 if DestLayer <> nil then
 begin
  FindRect := Bounds(X, Y, 1, 1);
  Result := RootNode as TBrushLayer;
  while Result <> nil do
  begin
   if Result.SpottedInRect(DestLayer, FindRect, pt) then
    Break;

   Result := Result.Next as TBrushLayer;
  end;
 end else
  Result := nil;
end;

function TMapBrush.FindExactBrushLayerSittingHere(Dest: TMap; X,
  Y: Integer): TBrushLayer;
var
 DestLayer: TMapLayer;
begin
 Result := nil;
 if Dest <> nil then
 begin
  Result := RootNode as TBrushLayer;
  DestLayer := nil;
  while Result <> nil do
  begin
   if (DestLayer = nil) or (DestLayer.TileSet <> Result.TileSet) then
    DestLayer := Dest.FindLayerByTileSet(Result.TileSet);

   if (DestLayer <> nil) and
      Result.SitsHere(DestLayer, X, Y) then
    Break;

   Result := Result.Next as TBrushLayer;
  end;
 end;
end;

function TMapBrush.GetExtendsIndex: Integer;
begin
 if (FExtends <> nil) and (FExtends <> Self) then
  Result := FExtends.Index else
  Result := -1;
end;

procedure TMapBrush.Initialize;
begin
 inherited;
 FNodeClass := TBrushLayer;
 FSignature := BRUSH_SIGN;
 FHeaderSize := SizeOf(TBrushHeader);
end;

function TMapBrush.ReadHeader(Stream: TStream; var Header: TSectionHeader): Boolean;
var
 bh: ^TBrushHeader;
begin
 Result := inherited ReadHeader(Stream, Header);
 if Result and (Header.shHeaderSize = SizeOf(TBrushHeader)) then
 begin
  bh := Addr(Header.dh);
  Integer(FExtends) := SmallInt(bh.MainHeader.mapReserved[1] + bh.MainHeader.mapReserved[2] shl 8);
 end else
  Integer(FExtends) := -1;
end;

function TMapBrush.ReadSetCount(var Header: TSectionHeader): LongInt;
var
 bh: ^TBrushHeader;
begin
 bh := Addr(Header.dh);
 if Header.shHeaderSize = SizeOf(TBrushHeader) then
  Result := bh.MainHeader.mapLayersCount else
  Result := inherited ReadSetCount(Header);
end;

procedure TMapBrush.RepointLinks;
var
 J: Integer;
begin
 for J := 0 to Count - 1 do
  with Nodes[J] as TBrushLayer do
  begin
   LinkLeftIndex := Integer(FLinkLeft);
   LinkRightIndex := Integer(FLinkRight);
   LinkUpIndex := Integer(FLinkUp);
   LinkDownIndex := Integer(FLinkDown);
   LinkLeftUpIndex := Integer(FLinkLeftUp);
   LinkRightUpIndex := Integer(FLinkRightUp);
   LinkLeftDownIndex := Integer(FLinkLeftDown);
   LinkRightDownIndex := Integer(FLinkRightDown);
  end;
end;

procedure TMapBrush.SetExtendsIndex(Value: Integer);
begin
 FExtends := (Owner as TBrushList).Brushes[Value];
 if FExtends = Self then
  FExtends := nil;
 with Owner as TBrushList do
 begin
  IdentitiesFilled := False;
  CleanUpTails;
 end;
end;

{ TBrushLayer }

procedure TBrushLayer.Assign(Source: TNode);
begin
 if Source <> Self then
 begin
  inherited;
  if Source is TBrushLayer then
   with TBrushLayer(Source) do
   begin
    Self.FHotSpotX := FHotSpotX;
    Self.FHotSpotY := FHotSpotY;
    Self.FUnfinished := FUnfinished;
    Self.FExtended := FExtended;
    Self.FIdentityTag := FIdentityTag;
    Self.FRandomize := FRandomize;
    Self.DrawSpots := DrawSpots;
    if (Owner.Owner is TBaseMapList) and
       (secCustom in TBaseMapList(Owner.Owner).FInternalFlags) then
    begin
     Self.LinkLeftIndex := LinkLeftIndex;
     Self.LinkRightIndex := LinkRightIndex;
     Self.LinkUpIndex := LinkUpIndex;
     Self.LinkDownIndex := LinkDownIndex;
     Self.LinkLeftUpIndex := LinkLeftUpIndex;
     Self.LinkRightUpIndex := LinkRightUpIndex;
     Self.LinkLeftDownIndex := LinkLeftDownIndex;
     Self.LinkRightDownIndex := LinkRightDownIndex;
    end else
     Move(FLinksStart, Self.FLinksStart,
      (Ord(High(TLinkDirection)) + 1) * SizeOf(Pointer));
   end;
 end;
end;

function TBrushLayer.CheckHeaderSize(var Size: Integer): Boolean;
begin
 Result := (Size = SizeOf(TLayerHeader)) or
           (Size = SizeOf(TBrushLayerHeaderV1)) or
           (Size = SizeOf(TBrushLayerHeaderV2)) or
           (Size = SizeOf(TBrushLayerHeaderV3)) or
           inherited CheckHeaderSize(Size);
end;

function TBrushLayer.CheckSignature(Value: LongWord): Boolean;
begin
 Result := (Value = LAYER_SIGN) or inherited CheckSignature(Value);
end;

function TBrushLayer.Draw(DestLayer: TMapLayer; DestX, DestY: Integer): Boolean;
var
 XX, YY, OX, OY, DX, DY, W, H, I, J, K: Integer;
 EmptyTileIndex: Integer;
 ld, rd, sd: TLinkDirection;
 pt: TPoint;
 Rect: TRect;
 Tail: TBrushLayer;
// Tails, RTails: TBrushLayerTails;
 HaveSomething, ReverseSet, SearchSet: set of TLinkDirection;
 Same: Boolean;
 Map: TMap;
 NextLayer: TMapLayer;
begin
 Result := False;
 if DestLayer.TileSet <> TileSet then Exit;
 Map := DestLayer.Owner as TMap;
   {
 J := Length(DrawList);
 for I := 0 to J - 1 do
  if DrawList[I] = Self then
   Exit;

 SetLength(DrawList, J + 1);
 DrawList[J] := Self;
                           }
 Same := SitsHere(DestLayer, DestX, DestY);

 TailsFilled := True;

 HaveSomething := [];
 for ld := Low(TLinkDirection) to High(TLinkDirection) do
  if ld in FUnfinished then
  begin
   rd := DirReverse[ld];
   ReverseSet := [rd, DirRelate[0, rd], DirRelate[1, rd]];   
   for I := 0 to Length(Tails) - 1 do
    with Tails[I] do
     if bloDirection = ld then
     begin
      pt.X := DestX + bloDistance.x;
      pt.Y := DestY + bloDistance.y;

      if bloLayer.TileSet <> DestLayer.TileSet then
       NextLayer := Map.FindLayerByTileSet(bloLayer.TileSet) else
       NextLayer := DestLayer;

      if (NextLayer <> nil) and bloLayer.SitsHere(NextLayer, pt.X, pt.Y) then
      begin
       if not Same and (rd in bloLayer.FUnfinished) then
       begin
        if ReverseSet - bloLayer.TailsDirections <> ReverseSet then
         for J := 0 to Length(bloLayer.Tails) - 1 do
          with bloLayer.Tails[J] do
           if (bloLayer <> Self) and
              (bloDirection in ReverseSet) then
           begin
            OX := pt.X + bloDistance.x;
            OY := pt.Y + bloDistance.y;

            if bloLayer.TileSet <> DestLayer.TileSet then
             NextLayer := Map.FindLayerByTileSet(bloLayer.TileSet) else
             NextLayer := DestLayer;

            if (NextLayer <> nil) and bloLayer.SitsHere(NextLayer, OX, OY) then
            begin
             NextLayer.TileSet.MaxIndex := NextLayer.IndexMask;
             EmptyTileIndex := NextLayer.TileSet.EmptyTileIndex;
             
             with bloLayer.RectOnMap(OX, OY) do
              for YY := Top to Bottom - 1 do
               for XX := Left to Right - 1 do
                NextLayer.TileIndex[XX, YY] := EmptyTileIndex;

             if bloDirection in [ldLeft, ldRight] then
              SearchSet := [ldUp, ldDown] else
             if bloDirection in [ldUp, ldDown] then
              SearchSet := [ldLeft, ldRight] else
             begin
              sd := DirReverse[bloDirection];
              SearchSet := [DirRelate[0, sd], DirRelate[1, sd]];
             end;

             if SearchSet - bloLayer.TailsDirections <> SearchSet then
              for K := 0 to Length(bloLayer.Tails) - 1 do
               with bloLayer.Tails[K] do
                if bloDirection in SearchSet then
                begin
                 DX := OX + bloDistance.x;
                 DY := OY + bloDistance.y;
                 if bloLayer.TileSet <> DestLayer.TileSet then
                  NextLayer := Map.FindLayerByTileSet(bloLayer.TileSet) else
                  NextLayer := DestLayer;

                 if (NextLayer <> nil) and bloLayer.SitsHere(NextLayer, DX, DY) then
                 begin
                  NextLayer.TileSet.MaxIndex := NextLayer.IndexMask;
                  EmptyTileIndex := NextLayer.TileSet.EmptyTileIndex;

                  with bloLayer.RectOnMap(DX, DY) do
                   for YY := Top to Bottom - 1 do
                    for XX := Left to Right - 1 do
                     NextLayer.TileIndex[XX, YY] := EmptyTileIndex;

                  Exclude(SearchSet, bloDirection);
                  if SearchSet = [] then Break;   
                 end;
                end;
             Result := True;
             Break;
            end;
          end;
       end;
       Include(HaveSomething, ld);
       Break;
      end;
     end;
  end;

 if not Same then
  with VisibleRect do
  begin
   W := Right - Left;
   H := Bottom - Top;
   DX := DestX - (FHotSpotX - Left);
   DY := DestY - (FHotSpotY - Top);
   (Owner.Owner as TBrushList).EraseBrushContentAt(Map, Bounds(DX, DY, W, H));
   for YY := 0 to H - 1 do
    for XX := 0 to W - 1 do
     DestLayer.Cells[XX + DX, YY + DY] := Cells[XX + Left, YY + Top];
   Result := True;
  end;

 for ld := Low(TLinkDirection) to High(TLinkDirection) do
  if (ld in FUnfinished) and not (ld in HaveSomething) then
   Result := DrawTail(Map, DestX, DestY, ld) or Result;
end;

function TBrushLayer.DrawTail(Dest: TMap;
      DestX, DestY: Integer; Direction: TLinkDirection): Boolean;
var
 I, J, L, XX, YY, DX, DY, W, H: Integer;
 tpt, cpt, ftpt, fpt: TPoint;
 FitsBetter, Tail, FoundTail, Tail2: TBrushLayer;
 MaxTails, EmptyTileIndex: Integer;
 HaveSomething, FindSet, CheckSet: TLinkDirections;
 DestLayer: TMapLayer;
 Dir: TLinkDirection;
 Rect, Dummy: TRect;
 BL: TBrushList;
begin
 Result := False;
 FitsBetter := nil;
 Tail := PLayerArray(Addr(FLinksStart))[Direction];
 if (Tail <> nil) and (Tail <> Self) then
 begin
  FindSet := [Direction, DirRelate[0, Direction], DirRelate[1, Direction]];
  if not (Direction in FUnfinished) {and not (Direction in FExtended)} then
   with DrawSpots[Direction] do
   begin
    FitsBetter := Tail;
    fpt.X := DestX + x;
    fpt.Y := DestY + y;
   end;

  if FitsBetter = nil then
  begin
//   TailsFilled := True;

   if FindSet - TailsDirections <> FindSet then
   begin
    MaxTails := -1;
    for I := 0 to Length(Tails) - 1 do
     with Tails[I] do
      if bloDirection in FindSet then
      begin
       HaveSomething := [DirReverse[bloDirection]];
       CheckSet := [Low(TLinkDirection)..High(TLinkDirection)] - HaveSomething;

//       bloLayer.TailsFilled := True;

       tpt.X := DestX + bloDistance.x;
       tpt.Y := DestY + bloDistance.y;

       L := 0;

       if CheckSet - bloLayer.TailsDirections <> CheckSet then
        for J := 0 to Length(bloLayer.Tails) - 1 do
         with bloLayer.Tails[J] do
          if bloDirection in CheckSet then
          begin
           cpt.X := tpt.X + bloDistance.x;
           cpt.Y := tpt.Y + bloDistance.y;

           DestLayer := Dest.FindLayerByTileSet(bloLayer.TileSet);

           if (DestLayer <> nil) and
              bloLayer.SitsHere(DestLayer, cpt.X, cpt.Y) then
           begin
            Include(HaveSomething, bloDirection);
            Inc(L);
           end;
          end;

       if (HaveSomething = bloLayer.FUnfinished) and (MaxTails < L) then
       begin
        MaxTails := L;
        FitsBetter := bloLayer;
        fpt := tpt;
       end;
      end;
   end;

   if FitsBetter = nil then
    with DrawSpots[Direction] do
    begin
     FitsBetter := PLayerArray(Addr(FLinksStart))[Direction];
     fpt.X := DestX + x;
     fpt.Y := DestY + y;
    end;
  end;
 end;

 if FitsBetter <> nil then
 begin
  if not (Direction in FitsBetter.FUnfinished) then
  begin
   Rect := FitsBetter.RectOnMap(fpt.X, fpt.Y);
   FoundTail := nil;
   with Rect, Owner as TMapBrush do
    for YY := Top to Bottom - 1 do
    begin
     for XX := Left to Right - 1 do
     begin
      FoundTail := FindBrushLayer(Dest, XX, YY, ftpt);
      if FoundTail <> nil then Break;
     end;
     if FoundTail <> nil then Break;
    end;
   Tail := nil;
   if FoundTail <> nil then
   begin
    if (FoundTail = FitsBetter) and (Int64(ftpt) = Int64(fpt)) then
    begin
     if Tail <> nil then;
     Exit;
    end;
    Tail2 := PLayerArray(Addr(FitsBetter.FLinksStart))[Direction];
    if Tail2 <> nil then
    begin
     with Tail2.DrawSpots[Direction] do
      if ((x <> 0) or (y <> 0)) and
         (Direction in Tail2.FUnfinished) and
        FitsBetter.IsTwin(PLayerArray(Addr(Tail2.FLinksStart))[Direction]) then
     begin
      with FoundTail.DrawSpots[Direction] do
      begin
       cpt.X := ftpt.X + x;
       cpt.Y := ftpt.Y + y;
      end;
      Tail := PLayerArray(Addr(FoundTail.FLinksStart))[Direction];
     end;
    end;
   end else
    Tail2 := nil;

   if Tail <> nil then
   begin
    Dir := DirReverse[Direction];
    CheckSet := [Dir, DirRelate[0, Dir], DirRelate[1, Dir]];
    if CheckSet - Tail.TailsDirections <> CheckSet then
     for I := 0 to Length(Tail.Tails) - 1 do
      with Tail.Tails[I] do
       if bloDirection in CheckSet then
       begin
        J := FindTail(bloLayer, FindSet);
        if J >= 0 then
        begin
         DX := cpt.X + bloDistance.x;
         DY := cpt.Y + bloDistance.y;
         with Tails[J] do
         begin
          tpt.X := DestX + bloDistance.x;
          tpt.Y := DestY + bloDistance.y;
          MaxTails := 0;
          with Tail2.DrawSpots[Direction] do
           while IntersectRect(Dummy, Rect, Tail2.RectOnMap(tpt.X, tpt.Y)) and
                 ((DX <> tpt.X) or (DY <> tpt.Y)) do
           begin
            Inc(tpt.X, x);
            Inc(tpt.Y, y);
            Inc(MaxTails);
           end;

          if (DX = tpt.X) and (DY = tpt.Y) then
          begin
           DestLayer := Dest.FindLayerByTileSet(FoundTail.TileSet);
           if DestLayer <> nil then
            with FoundTail.RectOnMap(ftpt.X, ftpt.Y) do
            begin
             DestLayer.TileSet.MaxIndex := DestLayer.IndexMask;
             EmptyTileIndex := DestLayer.TileSet.EmptyTileIndex;
             for YY := Top to Bottom - 1 do
              for XX := Left to Right - 1 do
               DestLayer.TileIndex[XX, YY] := EmptyTileIndex;
            end;

           with FitsBetter.DrawSpots[Direction] do
           begin
            DestX := fpt.X + x;
            DestY := fpt.Y + y;
           end;
           if Tail2.TileSet <> FoundTail.TileSet then
            DestLayer := Dest.FindLayerByTileSet(Tail2.TileSet);

           if DestLayer <> nil then
           begin
            DestLayer.TileSet.MaxIndex := DestLayer.IndexMask;
            EmptyTileIndex := DestLayer.TileSet.EmptyTileIndex;
            BL := Owner.Owner as TBrushList;
            with Tail2.DrawSpots[Direction] do
             for L := 0 to MaxTails - 1 do
              with Tail2.RandomTwin, VisibleRect do
              begin
               W := Right - Left;
               H := Bottom - Top;
               DX := DestX - (FHotSpotX - Left);
               DY := DestY - (FHotSpotY - Top);
               Inc(DestX, x);
               Inc(DestY, y);

               BL.EraseBrushContentAt(Dest, Bounds(DX, DY, W, H));

               for YY := 0 to H - 1 do
                for XX := 0 to W - 1 do
                 DestLayer.Cells[XX + DX, YY + DY] :=
                           Cells[XX + Left, YY + Top];
              end;
            Result := True;
           end;
           FitsBetter := bloLayer;
           fpt := tpt;
           Break;
          end;
         end;
        end;
       end;
   end;
  end;
  with FitsBetter.RandomTwin do
  begin
   DestLayer := Dest.FindLayerByTileSet(TileSet);
   if DestLayer <> nil then
    Result := Draw(DestLayer, fpt.X, fpt.Y) or Result;
  end;
 end;
end;

procedure TBrushLayer.FillHeader(var Header: TSectionHeader);
var
 V1: ^TBrushLayerHeaderV1;
 V2: ^TBrushLayerHeaderV2 absolute V1;
 V3: ^TBrushLayerHeaderV3 absolute V1;
 V4: ^TBrushLayerHeaderV4 absolute V1;
begin
 inherited;
 V1 := Addr(Header.dh);
 with V1.MainHeader do
  if FRandomize then
   lhFlags := lhFlags or BLF_RANDOMIZE else
   lhFlags := lhFlags and not BLF_RANDOMIZE;
 V1.blhHotSpot.x          := FHotSpotX;
 V1.blhHotSpot.y          := FHotSpotY;
 V1.blhUnfinished         := FUnfinished;
 V1.blhExtended           := FExtended;
 V2.blhLinks[ldLeft]      := LinkLeftIndex;
 V2.blhLinks[ldRight]     := LinkRightIndex;
 V2.blhLinks[ldUp]        := LinkUpIndex;
 V2.blhLinks[ldDown]      := LinkDownIndex;
 V2.blhLinks[ldLeftUp]    := LinkLeftUpIndex;
 V2.blhLinks[ldRightUp]   := LinkRightUpIndex;
 V2.blhLinks[ldLeftDown]  := LinkLeftDownIndex;
 V2.blhLinks[ldRightDown] := LinkRightDownIndex;
 V3.blhDrawSpots          := DrawSpots;
 V4.blhIdentityTag        := FIdentityTag;
end;

function TBrushLayer.FindTail(Tail: TBrushLayer; Directions: TLinkDirections): Integer;
var
 I: Integer;
begin
 TailsFilled := True;
 if Directions = [] then
 begin
  for I := 0 to Length(Tails) - 1 do
   with Tails[I] do
    if (bloLayer = Tail) then
    begin
     Result := I;
     Exit;
    end;
 end else
 begin
  for I := 0 to Length(Tails) - 1 do
   with Tails[I] do
    if (bloLayer = Tail) and
       (bloDirection in Directions) then
    begin
     Result := I;
     Exit;
    end;
 end;
 Result := -1;  
end;

function TBrushLayer.GetDownIndex: Integer;
begin
 Result := GetLinkIndexInternal(FLinkDown);
end;

function TBrushLayer.GetLeftDownIndex: Integer;
begin
 Result := GetLinkIndexInternal(FLinkLeftDown);
end;

function TBrushLayer.GetLeftIndex: Integer;
begin
 Result := GetLinkIndexInternal(FLinkLeft);
end;

function TBrushLayer.GetLeftUpIndex: Integer;
begin
 Result := GetLinkIndexInternal(FLinkLeftUp);
end;

function TBrushLayer.GetLink(Direction: TLinkDirection): TBrushLayer;
begin
 Result := PLayerArray(@FLinksStart)[Direction];
end;

function TBrushLayer.GetLinkIndex(Direction: TLinkDirection): Integer;
begin
 case Direction of
  ldLeft:       Result := LinkLeftIndex;
  ldRight:      Result := LinkRightIndex;
  ldUp:         Result := LinkUpIndex;
  ldDown:       Result := LinkDownIndex;
  ldLeftUp:     Result := LinkLeftUpIndex;
  ldRightUp:    Result := LinkRightUpIndex;
  ldLeftDown:   Result := LinkLeftDownIndex;
  ldRightDown:  Result := LinkRightDownIndex;
  else          Result := -1;
 end;
end;

function TBrushLayer.GetLinkIndexInternal(Link: TBrushLayer): Integer;
begin
 Result := -1;
 if Link <> nil then
 begin
  if Link.Owner = Owner then
   Result := Link.Index else
  if Link.Owner = (Owner as TMapBrush).FExtends then
   Result := Link.Index + (Owner as TMapBrush).Count;
 end;
end;

function TBrushLayer.GetRandomTwin: TBrushLayer;
var
 I, IL, L: Integer;
 lrx: TBrushLayer;
 RandomList: array of TBrushLayer;
begin
 Result := Self;
 if Result.Randomize then
  with Owner.Owner as TBrushList do
  begin
   IdentitiesFilled := True;
   IL := Length(IdentityList);
   SetLength(RandomList, IL);
   L := 0;
   for I := 0 to IL - 1 do
    with IdentityList[I] do
    begin
     if Layer1 = Self then
      lrx := Layer2 else
     if Layer2 = Self then
      lrx := Layer1 else
      lrx := nil;

     if (lrx <> nil) and lrx.Randomize then
     begin
      RandomList[L] := lrx;
      Inc(L);
     end;
    end;
   if L > 0 then
   begin
    I := Random(L + 1);
    if I > 0 then
     Result := RandomList[I - 1];
   end;
  end;
end;

function TBrushLayer.GetRightDownIndex: Integer;
begin
 Result := GetLinkIndexInternal(FLinkRightDown);
end;

function TBrushLayer.GetRightIndex: Integer;
begin
 Result := GetLinkIndexInternal(FLinkRight);
end;

function TBrushLayer.GetRightUpIndex: Integer;
begin
 Result := GetLinkIndexInternal(FLinkRightUp);
end;

function TBrushLayer.GetTailsDirections: TLinkDirections;
begin
 TailsFilled := True;
 Result := FTailsDirections;
end;

function TBrushLayer.GetTailsFilled: Boolean;
begin
 Result := Tails <> nil;
end;

function TBrushLayer.GetUpIndex: Integer;
begin
 Result := GetLinkIndexInternal(FLinkUp);
end;

procedure TBrushLayer.Initialize;
begin
 inherited;
 FSignature := BRUSH_LAYER_SIGN;
 FHeaderSize := SizeOf(TBrushLayerHeader);
end;

function TBrushLayer.IsTwin(Layer: TBrushLayer): Boolean;
var
 I: Integer;
 lrx: TBrushLayer;
begin
 Result := Layer = Self;
 if not Result then
  with Owner.Owner as TBrushList do
  begin
   IdentitiesFilled := True;
   for I := 0 to Length(IdentityList) - 1 do
    with IdentityList[I] do
    begin
     if Self = Layer1 then
      lrx := Layer2 else
     if Self = Layer2 then
      lrx := Layer1 else
      lrx := nil;

     if lrx = Layer then
     begin
      Result := True;
      Exit;
     end;
    end;
  end;
end;

function TBrushLayer.ReadHeader(Stream: TStream;
  var Header: TSectionHeader): Boolean;
var
 V1: ^TBrushLayerHeaderV1;
 V2: ^TBrushLayerHeaderV2 absolute V1;
 V3: ^TBrushLayerHeaderV3 absolute V1;
 V4: ^TBrushLayerHeaderV4 absolute V1;
begin
 Result := inherited ReadHeader(Stream, Header);
 if Result then
 begin
  V1 := Addr(Header.dh);
  FRandomize  := V1.MainHeader.lhFlags and BLF_RANDOMIZE <> 0;
  FHotSpotX   := V1.blhHotSpot.x;
  FHotSpotY   := V1.blhHotSpot.y;
  FUnfinished := V1.blhUnfinished;
  FExtended   := V1.blhExtended;

  if Header.shHeaderSize >= SizeOf(TBrushLayerHeaderV4) then
   FIdentityTag  := V4.blhIdentityTag;

  if Header.shHeaderSize >= SizeOf(TBrushLayerHeaderV3) then
   DrawSpots     := V3.blhDrawSpots;

  if Header.shHeaderSize >= SizeOf(TBrushLayerHeaderV2) then
  begin
   FLinkLeft      := Pointer(V2.blhLinks[ldLeft]);
   FLinkRight     := Pointer(V2.blhLinks[ldRight]);
   FLinkUp        := Pointer(V2.blhLinks[ldUp]);
   FLinkDown      := Pointer(V2.blhLinks[ldDown]);
   FLinkLeftUp    := Pointer(V2.blhLinks[ldLeftUp]);
   FLinkRightUp   := Pointer(V2.blhLinks[ldRightUp]);
   FLinkLeftDown  := Pointer(V2.blhLinks[ldLeftDown]);
   FLinkRightDown := Pointer(V2.blhLinks[ldRightDown]);
   Exit;
  end;
  
  FLinkLeft      := Pointer(-1);
  FLinkRight     := Pointer(-1);
  FLinkUp        := Pointer(-1);
  FLinkDown      := Pointer(-1);
  FLinkLeftUp    := Pointer(-1);
  FLinkRightUp   := Pointer(-1);
  FLinkLeftDown  := Pointer(-1);
  FLinkRightDown := Pointer(-1);
 end;
end;

function TBrushLayer.RectOnMap(X, Y: Integer): TRect;
begin
 with VisibleRect do
  Result := Bounds(X - (FHotSpotX - Left),
                   Y - (FHotSpotY - Top),
                   Right - Left,
                   Bottom - Top);
end;

function TBrushLayer.ScanLayer(Target: TMapLayer;
          X, Y: Integer): Boolean;
var
 SrcInfo, DstInfo: TCellInfo;
 Dst: PByte;
 XX, YY, DstStride, DstWidth, DstHeight: Integer;
// Dirty: Boolean;
 Tile: TTileItem;
begin
 with VisibleRect do
 begin
  DstWidth := Right - Left;
  DstHeight := Bottom - Top;
  DstStride := (TMap(Owner).Width - DstWidth) * CellSize;
  Dst := Addr(PByteArray(LayerData)[(Top * TMap(Owner).Width +
                                     Left) * CellSize]);
  Result := True;
 // Dirty := False;
  for YY := 0 to DstHeight - 1 do
  begin
   for XX := 0 to DstWidth - 1 do
   begin
    SrcInfo := Target.Cells[XX + X, YY + Y];
    if SrcInfo.TileIndex >= 0 then
    begin
     ReadCellInfo(Dst^, DstInfo);
     Tile := TileSet.Indexed[SrcInfo.TileIndex];
    // if not Dirty then
    //  Dirty := Tile <> TileSet.EmptyTile;
     if (Tile <> TileSet.Indexed[DstInfo.TileIndex]) or
       not CompareMem(Addr(SrcInfo.PaletteIndex),
                      Addr(DstInfo.PaletteIndex),
                      SizeOf(TCellInfo) - 4) then
     begin
      Result := False;
      Break;
     end;
    end;
    Inc(Dst, CellSize);
   end;
   if Result = False then Break;
   Inc(Dst, DstStride);
  end;
 end;
 //Result := Dirty and Result;
end;

procedure TBrushLayer.SetDownIndex(Value: Integer);
begin
 SetLinkIndexInternal(Value, FLinkDown);
end;

procedure TBrushLayer.SetIdentityTag(Value: LongInt);
begin
 FIdentityTag := Value;
 (Owner.Owner as TBrushList).IdentitiesFilled := False;
end;

procedure TBrushLayer.SetLeftDownIndex(Value: Integer);
begin
 SetLinkIndexInternal(Value, FLinkLeftDown);
end;

procedure TBrushLayer.SetLeftIndex(Value: Integer);
begin
 SetLinkIndexInternal(Value, FLinkLeft);
end;

procedure TBrushLayer.SetLeftUpIndex(Value: Integer);
begin
 SetLinkIndexInternal(Value, FLinkLeftUp);
end;

procedure TBrushLayer.SetLinkIndex(Direction: TLinkDirection; Value: Integer);
begin
 case Direction of
  ldLeft:       LinkLeftIndex := Value;
  ldRight:      LinkRightIndex := Value;
  ldUp:         LinkUpIndex := Value;
  ldDown:       LinkDownIndex := Value;
  ldLeftUp:     LinkLeftUpIndex := Value;
  ldRightUp:    LinkRightUpIndex := Value;
  ldLeftDown:   LinkLeftDownIndex := Value;
  ldRightDown:  LinkRightDownIndex := Value;
 end;
end;

procedure TBrushLayer.SetLinkIndexInternal(Value: Integer; var Link: TBrushLayer);
begin
 with Owner as TMapBrush do
 begin
  if Value >= Count then
  begin
   if FExtends <> nil then
    Link := FExtends.Nodes[Value - Count] as TBrushLayer else
    Link := nil;
  end else
   Link := Nodes[Value] as TBrushLayer;

  (Owner as TBrushList).CleanUpTails;
 end;
end;

procedure TBrushLayer.SetRightDownIndex(Value: Integer);
begin
 SetLinkIndexInternal(Value, FLinkRightDown);
end;

procedure TBrushLayer.SetRightIndex(Value: Integer);
begin
 SetLinkIndexInternal(Value, FLinkRight);
end;

procedure TBrushLayer.SetRightUpIndex(Value: Integer);
begin
 SetLinkIndexInternal(Value, FLinkRightUp);
end;

procedure TBrushLayer.SetTailsFilled(Value: Boolean);

procedure AddRec(Layer: TBrushLayer; const Distance: TSmallPoint;
         Direction: TLinkDirection; Reverse: Boolean);
var
 I: Integer;
 NewRec: TBrushLayerTailRec;
begin
 with NewRec do
 begin
  bloLayer := Layer;
  if Reverse then
  begin
   bloDistance.x := -Distance.x;
   bloDistance.y := -Distance.y;
   bloDirection := DirReverse[Direction];
  end else
  begin
   bloDistance := Distance;
   bloDirection := Direction;
  end;
 end;
 for I := 0 to Length(Tails) - 1 do
  if CompareMem(Addr(Tails[I]), @NewRec, SizeOf(TBrushLayerTailRec)) then
   Exit;
 I := Length(Tails);
 SetLength(Tails, I + 1);
 Tails[I] := NewRec;
 Include(FTailsDirections, NewRec.bloDirection); 
end;

var
 J, K, XX: Integer;
 fl, test, lrx: TBrushLayer;
 I, RI, dr: TLinkDirection;
 Distance: TSmallPoint;
 ThisBrush, Brush: TMapBrush;
begin
 if not Value then
 begin
  Finalize(Tails);
  FTailsDirections := [];
 end else
 if Tails = nil then
 begin
  ThisBrush := Owner as TMapBrush;
  with ThisBrush.Owner as TBrushList do
  begin
   IdentitiesFilled := True;
   for J := 0 to Count - 1 do
   begin
    Brush := Nodes[J] as TMapBrush;
    if (Brush = ThisBrush) or (Brush.FExtends = ThisBrush) then
     for K := 0 to Brush.Count - 1 do
     begin
      fl := Brush.Nodes[K] as TBrushLayer;
      if fl = Self then
      begin
       for I := Low(TLinkDirection) to High(TLinkDirection) do
        if I in fl.FUnfinished then
        begin
         test := PLayerArray(Addr(fl.FLinksStart))[I];
         if test <> nil then
         begin
          AddRec(test, fl.DrawSpots[I], I, False);

          for XX := -1 to 1 do
          begin
           if XX < 0 then
            RI := I else
            RI := DirRelate[XX, I];

           if RI in fl.FExtended then
           begin
            lrx := PLayerArray(Addr(test.FLinksStart))[RI];
            if (lrx <> nil) and
               (DirReverse[I] in lrx.FUnfinished) then
            begin
             Distance := fl.DrawSpots[I];
             with test.DrawSpots[RI] do
             begin
              Inc(Distance.x, x);
              Inc(Distance.y, y);
             end;
             AddRec(lrx, Distance, I, False);
            end;
           end;
          end;
         end;
        end;
      end else
      begin
       for I := Low(TLinkDirection) to High(TLinkDirection) do
       begin
        test := PLayerArray(Addr(fl.FLinksStart))[I];
        if test = Self then
        begin
         if I in fl.FUnfinished then
          AddRec(fl, fl.DrawSpots[I], I, True)
         else
          for XX := -1 to 1 do
          begin
           RI := DirReverse[I];
           if XX >= 0 then
           begin
            RI := DirRelate[XX, RI];
            dr := DirRelate[XX, I];
           end else
            dr := I;

           if (RI in test.FUnfinished) and
              (dr in fl.FUnfinished) then
            AddRec(fl, fl.DrawSpots[I], dr, True);
          end;
        end;
       end;
      end;
     end;
   end;

   for J := 0 to Length(Tails) - 1 do
    with Tails[J] do
    begin
     for K := 0 to Length(IdentityList) - 1 do
      with IdentityList[K] do
      begin
       if Layer1 = bloLayer then
        lrx := Layer2 else
       if Layer2 = bloLayer then
        lrx := Layer1 else
        lrx := nil;

       if lrx <> nil then
        AddRec(lrx, bloDistance, bloDirection, False);
      end;
    end;
  end;
 end;
end;

procedure TBrushLayer.SetUpIndex(Value: Integer);
begin
 SetLinkIndexInternal(Value, FLinkUp);
end;

function TBrushLayer.SitsHere(Target: TMapLayer; X, Y: Integer): Boolean;
begin
 if (Target <> nil) and (TileSet <> nil) and (TileSet = Target.TileSet) then
 begin
  with RectOnMap(X, Y) do
   Result := ScanLayer(Target, Left, Top);
 end else
  Result := False;
end;

function TBrushLayer.SpottedInRect(Target: TMapLayer; const Rect: TRect;
  var pt: TPoint): Boolean;
var
 dr: TRect;
 DstWidth, DstHeight: Integer;
 XX, YY, SX: Integer;
begin
 if (Target <> nil) and (TileSet <> nil) and (TileSet = Target.TileSet) then
 begin
  dr := VisibleRect;

  DstWidth := dr.Right - dr.Left;
  DstHeight := dr.Bottom - dr.Top;

  if (DstWidth > 0) and (DstHeight > 0) then
  begin
   SX := Rect.Left - (DstWidth - 1);
   for YY := Rect.Top - (DstHeight - 1) to Rect.Bottom - 1 do
    for XX := SX to Rect.Right - 1 do
    begin
     Result := ScanLayer(Target, XX, YY);
     if Result then
     begin
      pt := Point(XX + (FHotSpotX - dr.Left),
                  YY + (FHotSpotY - dr.Top));
      Exit;
     end;
    end;
  end;
 end;
 Result := False;
end;

{ TPasteBuffer }

procedure TPasteBuffer.Clear;
var
 I: Integer;
begin
 for I := 0 to Length(Layers) - 1 do
  FreeMem(Layers[I].lData);
 Finalize(Layers);
 FreeAndNil(TileSet);
 Finalize(Map);
end;

constructor TPasteBuffer.Create(BufID: Integer);
begin
 ID := BufID;
end;

destructor TPasteBuffer.Destroy;
begin
 Clear;
 inherited;
end;

procedure TPasteBuffer.LoadFromStream(Stream: TStream;
  TileSets: TTileSetList);
var
 Head: TPasteBufferHeader;
 Sz, I: Integer;
 StartPos: Int64;
 PBLH: TPB_LayerHeader;
begin
 StartPos := Stream.Position;
 Clear;
 Stream.ReadBuffer(Head, SizeOf(Head));
 if Head.Signature = PB_SIGN then
 case Head.ID of
  666,
  776,
  777,
  888:
  begin
   ID := Head.ID;
   Width := Head.Width;
   Height := Head.Height;

   Sz := Head.Width * Head.Height;

   SetLength(Layers, Head.LayersCount);

   if Head.TilesOffset > 0 then
   begin
    Stream.Position := StartPos + Head.TilesOffset;
    TileSet := TTileSet.Create;
    TileSet.LoadFromStream(Stream);
    SetLength(Map, Sz);
    Stream.ReadBuffer(Map[0], Sz * 4);
   end else
   begin
    TileSet := nil;
    Map := nil;
   end;

   Stream.Position := Head.LayerOffset;

   for I := 0 to Head.LayersCount - 1 do
    with Layers[I] do
    begin
     Stream.ReadBuffer(PBLH, SizeOf(PBLH));
     Stream.ReadBuffer(lFormat.lfIndexMask, SizeOf(lFormat) - SizeOf(Pointer));

     if (PBLH.LayerDataSize <> 0) and
        (PBLH.LayerDataSize <> Sz * lFormat.lfCellSize) then
      raise Exception.Create('Bad data');

     if PBLH.TileSetDataSize > 0 then
     begin
      lFormat.lfTileSet := TileSets.FindByContent(PBLH.TileSetChecksum,
                             PBLH.TileSetDataSize) as TTileSet;
     end else
      lFormat.lfTileSet := nil;

     if PBLH.LayerDataSize > 0 then
     begin
      GetMem(lData, PBLH.LayerDataSize);
      Stream.ReadBuffer(lData^, PBLH.LayerDataSize);
     end else
      lData := nil;
    end;
  end;
  else
   raise Exception.CreateFmt('%d is an unknown paste buffer ID', [Head.ID]);
 end else
   raise Exception.Create('Unknown format');
end;

procedure TPasteBuffer.SaveToStream(Stream: TStream);
var
 Head: TPasteBufferHeader;
 I, Sz: Integer;
 PBLH: TPB_LayerHeader;
begin
 Head.Signature := PB_SIGN;
 Head.ID := ID;
 Head.LayersCount := Length(Layers);
 Head.Width := Width;
 Head.Height := Height;

 Sz := Width * Height;

 if ((ID = 888) or (ID = 776))
     and (TileSet <> nil) then
 begin
  Head.TilesOffset := SizeOf(Head);
  Head.LayerOffset := SizeOf(Head) + Sz * 4 + TileSet.TotalSize;
 end else
 begin
  Head.TilesOffset := 0;
  Head.LayerOffset := SizeOf(Head);
 end;

 Stream.WriteBuffer(Head, SizeOf(Head));

 if TileSet <> nil then
 begin
  TileSet.SaveToStream(Stream);
  Stream.WriteBuffer(Map[0], Sz * 4);
 end;

 for I := 0 to Head.LayersCount - 1 do
  with Layers[I] do
  begin
   if lFormat.lfTileSet <> nil then
   begin
    PBLH.TileSetDataSize := lFormat.lfTileSet.DataSize;
    PBLH.TileSetChecksum := lFormat.lfTileSet.Checksum;
   end else
   begin
    PBLH.TileSetDataSize := 0;
    PBLH.TileSetChecksum := 0;
   end;
   if lData <> nil then
    PBLH.LayerDataSize := Sz * lFormat.lfCellSize else
    PBLH.LayerDataSize := 0;
   Stream.WriteBuffer(PBLH, SizeOf(PBLH));
   Stream.WriteBuffer(lFormat.lfIndexMask, SizeOf(lFormat) - SizeOf(Pointer));
   if lData <> nil then
    Stream.WriteBuffer(lData^, PBLH.LayerDataSize);
  end;
end;

{ TInternalMapList }

procedure TInternalMapList.Assign(Source: TNode);
var
 Src: TBaseMapList absolute Source;
begin
 if Source is TBaseMapList then
 begin
  FColorTableList := Src.FColorTableList;
  FTileSetList := Src.FTileSetList;
 end;
 inherited;
end;

{ TInternalBrushList }

procedure TInternalBrushList.Assign(Source: TNode);
var
 Src: TBaseMapList absolute Source;
begin
 if Source is TBaseMapList then
 begin
  FColorTableList := Src.FColorTableList;
  FTileSetList := Src.FTileSetList;
 end;
 inherited;
end;

initialization
 Randomize;
end.
