object TileEditFrame: TTileEditFrame
  Left = 0
  Top = 0
  Width = 654
  Height = 442
  TabOrder = 0
  object Splitter: TSplitter
    Left = 198
    Top = 0
    Height = 442
    AutoSnap = False
    ResizeStyle = rsUpdate
  end
  object Panel: TPanel
    Left = 201
    Top = 0
    Width = 453
    Height = 442
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object StatusBar: TTntStatusBar
      Left = 0
      Top = 0
      Width = 453
      Height = 14
      Align = alTop
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -9
      Font.Name = 'Small Fonts'
      Font.Style = []
      Panels = <
        item
          Text = 'Tile'
          Width = 40
        end
        item
          Width = 50
        end
        item
          Width = 40
        end
        item
          Width = 40
        end
        item
          Width = 70
        end
        item
          Width = 50
        end>
      ParentColor = True
      UseSystemFont = False
    end
    object TileEditView: TTileMapView
      Left = 0
      Top = 14
      Width = 453
      Height = 428
      MapWidth = 0
      MapHeight = 0
      OffsetX = 0
      OffsetY = 0
      ShowGrid = False
      SelectionMode = selMulti
      GridStyle = psDot
      SelectStyle = psSolid
      OnDrawCell = TileEditViewDrawCell
      OnSelectionChanged = TileEditViewSelectionChanged
      OnGetCellState = TileEditViewGetCellState
      OnGetBufCellState = TileEditViewGetBufCellState
      OnCopyMapData = TileEditViewCopyMapData
      OnPasteBufferApply = TileEditViewPasteBufferApply
      OnClearCell = TileEditViewClearCell
      OnBufClearCell = TileEditViewBufClearCell
      OnReleasePasteBuffer = TileEditViewReleasePasteBuffer
      OnCheckCompatibility = TileEditViewCheckCompatibility
      OnMakePasteBufferCopy = TileEditViewMakePasteBufferCopy
      OnContentsChanged = TileEditViewContentsChanged
      Align = alClient
      Constraints.MinHeight = 32
      DragCursor = 983
      TabOrder = 1
      OnEnter = TileEditViewEnter
      OnMouseMove = TileEditViewMouseMove
      OnMouseWheelDown = TileEditViewMouseWheelDown
      OnMouseWheelUp = TileEditViewMouseWheelUp
    end
  end
  object LeftPanel: TPanel
    Left = 0
    Top = 0
    Width = 198
    Height = 442
    Align = alLeft
    BevelOuter = bvNone
    TabOrder = 1
    inline PropsFrame: TTilePropsFrame
      Left = 0
      Top = 57
      Width = 198
      Height = 385
      Align = alClient
      TabOrder = 0
      inherited SelTilePropLabel: TTntLabel
        Width = 198
      end
      inherited SelTilePropEditor: TPropertyEditor
        Width = 198
        Height = 372
        ValueWidth = 104
        OnEnter = TileEditViewEnter
        Columns = <
          item
            Margin = 0
            Options = [coEnabled, coParentBidiMode, coParentColor, coResizable, coVisible, coFixed]
            Position = 0
            Width = 90
          end
          item
            Margin = 0
            Options = [coEnabled, coParentBidiMode, coParentColor, coResizable, coVisible, coFixed]
            Position = 1
            Width = 104
          end>
      end
    end
    object ColorPanel: TPanel
      Left = 0
      Top = 0
      Width = 198
      Height = 57
      Align = alTop
      BevelInner = bvRaised
      BevelOuter = bvLowered
      TabOrder = 1
      object Color1Label: TTntLabel
        Left = 40
        Top = 10
        Width = 3
        Height = 13
        Layout = tlCenter
      end
      object Color2Label: TTntLabel
        Left = 40
        Top = 34
        Width = 3
        Height = 13
        Layout = tlCenter
      end
      object Color1Preview: TmbColorPreview
        Left = 8
        Top = 8
        Width = 25
        Height = 17
        OnMouseDown = ColorPreviewMouseDown
      end
      object Color2Preview: TmbColorPreview
        Tag = 1
        Left = 8
        Top = 32
        Width = 25
        Height = 17
        OnMouseDown = ColorPreviewMouseDown
      end
    end
  end
end
