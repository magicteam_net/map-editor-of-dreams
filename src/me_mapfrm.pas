unit me_mapfrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, BmpImg, BitmapEx, TilesEx, DIB, me_lib,
  ComCtrls, TileMapView, TntComCtrls;

type       {
  TSpecialImage = class(TImage)
  protected
    procedure WMRButtonDown(var Message: TWMRButtonDown); message WM_RBUTTONDOWN;
    procedure WMRButtonUp(var Message: TWMRButtonUp); message WM_RBUTTONUP;
    procedure WMMButtonDown(var Message: TWMRButtonDown); message WM_MBUTTONDOWN;
    procedure WMMButtonUp(var Message: TWMRButtonUp); message WM_MBUTTONUP;
  end;
            }

  TMapFrame = class;
  TMapFrameEvent = procedure(Sender: TMapFrame) of object;
  TScaleChangeEvent = procedure(Sender: TObject; var Value: Integer) of object;
  TGetLayerActiveEvent = procedure(Sender: TMapFrame; Layer: TMapLayer; var Active: Boolean) of object;

  TMapFrame = class(TFrame)
    MapStatusBar: TTntStatusBar;
    MapEditView: TTileMapView;
    procedure MapEditViewDrawCell(Sender: TCustomTileMapView;
              const Rect: TRect; CellX, CellY: Integer);
    procedure MapEditViewMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure MapEditViewReleasePasteBuffer(Sender: TCustomTileMapView;
      Data: Pointer);
    procedure MapEditViewCopyMapData(Sender: TCustomTileMapView;
      var Data: Pointer; const Rect: TRect; SelectionBuffer: Pointer);
    procedure MapEditViewPasteBufferApply(Sender: TCustomTileMapView;
      CellX, CellY: Integer; Data: Pointer);
    procedure MapEditViewGetCellState(Sender: TCustomTileMapView; CellX,
      CellY: Integer; var Dirty: Boolean);
    procedure MapEditViewCheckCompatibility(Sender: TCustomTileMapView;
      Data: Pointer; var Result: Boolean);
    procedure MapEditViewGetBufCellState(Sender: TCustomTileMapView;
      Data: Pointer; CellX, CellY: Integer; var Dirty: Boolean);
    procedure MapEditViewBufClearCell(Sender: TCustomTileMapView; CellX,
      CellY: Integer; Data: Pointer);
    procedure MapEditViewClearCell(Sender: TCustomTileMapView; CellX,
      CellY: Integer);
    procedure MapEditViewMouseWheelDown(Sender: TObject;
      Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
    procedure MapEditViewMouseWheelUp(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure MapEditViewSetPosition(Sender: TCustomTileMapView; var NewX,
      NewY: Integer);
    procedure MapEditViewMakePasteBufferCopy(Sender: TCustomTileMapView;
      Source: Pointer; var Dest: Pointer);
    procedure MapEditViewEnter(Sender: TObject);
    procedure MapEditViewSelectionChanged(Sender: TCustomTileMapView);
  protected
    function DoGetLayerActive(Layer: TMapLayer): Boolean; virtual;  
  private
    FScale: Integer;
    FMapData: TMap;
    FConverters: TCvtRecList;
    FPicHeader: TPicHeader;
    FPicData: TBitmapContainer;

    FMapModified: Boolean;
    FBrushMode: Boolean;
    FOnMapChange: TMapFrameEvent;
    FOnScaleChange: TScaleChangeEvent;
    FOnGetLayerActive: TGetLayerActiveEvent;    
    procedure CMMouseLeave(var Message: TMessage); message CM_MOUSELEAVE;
    procedure SetScale(Value: Integer);
    procedure SetMapModified(Value: Boolean);
    procedure SetMapData(Value: TMap);
  public
    property MapModified: Boolean read FMapModified write SetMapModified;
    property Converters: TCvtRecList read FConverters;
    property Scale: Integer read FScale write SetScale;
    property MapData: TMap read FMapData write SetMapData;
    property BrushMode: Boolean read FBrushMode;

    property OnMapChange: TMapFrameEvent read FOnMapChange
                                        write FOnMapChange;
    property OnScaleChange: TScaleChangeEvent read FOnScaleChange
                                             write FOnScaleChange;
    property OnGetLayerActive: TGetLayerActiveEvent
                               read FOnGetLayerActive
                              write FOnGetLayerActive;

    procedure SetMap(AMapData: TMap; AScale: Integer = 0; BrushMode: Boolean = False);
    procedure TilesChanged(DoRepaint: Boolean = True);
    procedure RefreshColorTable(DoRepaint: Boolean = True);
    procedure UpdateCoordinates;
    procedure DoSetScale(Value: Integer; DoRepaint: Boolean);
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure FillConverters(MapList: TBaseMapList);
  end;

const
 MapTypeStr: array[Boolean] of WideString =
 ('Map',
  'Brush');
 MapTypeStrLC: array[Boolean] of WideString =
 ('map',
  'brush');
 MapTypeStr2: array[Boolean] of WideString =
 ('maps',
  'brushes');
 MapTypeStr3: array[Boolean] of WideString =
 ('Maps',
  'Brushes');

var
 CF_MAPDATA: Word;  

implementation

uses
  HexUnit;

{$R *.dfm}

{ TMapFrame }

constructor TMapFrame.Create(AOwner: TComponent);
begin
 inherited;
 FPicData := TBitmapContainer.Create;
 FPicData.ImageFlags := (8 - 1) or IMG_Y_FLIP;
 DoSetScale(2, False);
end;

destructor TMapFrame.Destroy;
begin
 FreeMem(FPicData.ImageData);
 FPicData.Free;
 CvtRecListFree(FConverters);
 inherited;
end;

procedure TMapFrame.MapEditViewDrawCell(Sender: TCustomTileMapView;
  const Rect: TRect; CellX, CellY: Integer);
var
 I: Integer;
 BC, DataOffset: Integer;
 MB: PByte; Fill: Integer;
 Info: TPasteBuffer;
 DrawInf: TDrawInfoRec;
 DrawNode: PDrawInfo;
 Tile: TTileItem;
 Cvt: TBitmapConverter;
 Layr: ^TPBLayerRec;
 DrawNodes: array [0..31] of TDrawInfo;
label inside;
begin
 DataOffset := 0;
 if TObject(Sender.PasteBuffer) is TPasteBuffer then
 begin
  Info := TPasteBuffer(Sender.PasteBuffer);
  Dec(CellX, Sender.PasteBufRect.Left);
  Dec(CellY, Sender.PasteBufRect.Top);
  if (CellX < 0) or (CellX >= Info.Width) or
     (CellY < 0) or (CellY >= Info.Height) then
   Info := nil else
   DataOffset := CellY * Info.Width + CellX;
 end else
  Info := nil;

 DrawInf.First := nil;
 DrawInf.Middle := nil;
 DrawInf.Last := nil;
// Fill := -1;

 DrawNode := @DrawNodes;

 for I := 0 to FMapData.Count - 1 do
  with FMapData.Layers[I] do
   if Assigned(TileSet) and
      (Flags and LF_VISIBLE <> 0) then
   begin
//    Fill := FirstColor + TileSet.TransparentColor;
    
    Layr := nil;
    if Info <> nil then
    begin
     Layr := Addr(Info.Layers[I]);
     if Layr.lData = nil then
      Layr := nil;
    end;

    if Layr <> nil then
    begin
     MB := Layr.lData;
     Inc(MB, DataOffset * Layr.lFormat.lfCellSize);
     CellInfoRead(Layr.lFormat, MB^, DrawNode.Info);
     Tile := TileSet.Indexed[DrawNode.Info.TileIndex];
    end else
    begin
     goto inside;
     Tile := nil;
    end;

    if (Tile = TileSet.EmptyTile)  then
    begin
     inside:
     if LayerData = nil then Continue;
     MB := Addr(PByteArray(LayerData)[Sender.InternalIndex * CellSize]);
     ReadCellInfo(MB^, DrawNode.Info);
     Tile := TileSet.Indexed[DrawNode.Info.TileIndex];
     if Tile = TileSet.EmptyTile then
      Continue;
    end;

    DrawNode.TileSet := TileSet;
    DrawNode.Tile := Tile;
    DrawNode.FC := FirstColor;
    AddDrawInfo(DrawInf, DrawNode);
    Inc(DrawNode);
   end;

 if DrawInf.First <> nil then
  with DrawInf.First^ do
   Fill := Tile.FirstColor + FC + TileSet.TransparentColor
 else
 if FMapData.ColorTable <> nil then
  Fill := FMapData.ColorTable.TransparentIndex else
  Fill := 0;

 FillChar(FPicData.ImageData^, FPicHeader.bhHead.bhInfoHeader.biSizeImage, Fill);

 while DrawInf.First <> nil do
 begin
  DrawNode := DrawInf.First;
  DrawInf.First := DrawInf.First.Next;
  with DrawNode^ do
  begin
   Cvt := FConverters[TileSet.Index].tcvtDraw[Info.XFlip, Info.YFlip];
   BC := TileSet.TileBitmap.BitsCount;
   if (BC < 8) and (Info.PaletteIndex >= 0) then
    Cvt.SrcInfo.PixelModifier := FC + Info.PaletteIndex shl BC else
    Cvt.SrcInfo.PixelModifier := FC + Tile.FirstColor;

   TileSet.TileDraw(Tile.TileData^, FPicData, 0, 0, Cvt);
  end;
 end;

 GdiFlush;
 StretchDIBits(Sender.Canvas.Handle,
               Rect.Left, Rect.Top,
               Rect.Right - Rect.Left,
               Rect.Bottom - Rect.Top,
               0, 0,
               FPicHeader.bhHead.bhInfoHeader.biWidth,
               FPicHeader.bhHead.bhInfoHeader.biHeight,
               FPicData.ImageData,
               TBitmapInfo(Addr(FPicHeader.bhHead.bhInfoHeader)^),
               DIB_RGB_COLORS, SRCCOPY);
end;

procedure TMapFrame.MapEditViewMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
var
 W, H: Integer;
begin
 UpdateCoordinates;
 if MapEditView.MouseMode in [mmMultiSelectNew,
                              mmMultiSelectInclude,
                              mmMultiSelectExclude] then
 begin
  with MapEditView.TempSelectionRect do
  begin
   W := Right - Left;
   H := Bottom - Top;
   if (W > 0) and (H > 0) then
    MapStatusBar.Panels[5].Text :=
        WideFormat('Selection: (%d, %d) %dx%d', [Left, Top, W, H]) else
    MapStatusBar.Panels[5].Text := '';
  end;
 end;
end;

procedure TMapFrame.RefreshColorTable(DoRepaint: Boolean);
begin
 if (FMapData <> nil) and
    (FMapData.ColorTable <> nil) then with FMapData.ColorTable do
  ColorFormat.ConvertToRGBQuad(ColorData, Addr(FPicHeader.bhPalette), ColorsCount) else
  FillChar(FPicHeader.bhPalette, SizeOf(TRGBQuads), 0);
 if DoRepaint then
  MapEditView.Invalidate;
end;

procedure TMapFrame.SetMap(AMapData: TMap; AScale: Integer; BrushMode: Boolean);
var
 X, Y: Integer;
begin
 if AScale <= 0 then AScale := FScale;
 if (FMapData <> AMapData) or FMapModified or
    (AScale <> FScale) then
 begin
  FMapModified := False;
  MapEditView.Deselect;
  FMapData := AMapData;
  if FMapData <> nil then
  begin
   X := FMapData.MapX * FScale;
   Y := FMapData.MapY * FScale;
  end else
  begin
   X := 0;
   Y := 0;
  end;

  if Assigned(FOnMapChange) then
   FOnMapChange(Self);

  DoSetScale(AScale, False);

  TilesChanged(False);
  RefreshColorTable(False);

  if FMapData <> nil then
  begin
   MapEditView.SetMapSize(FMapData.Width, FMapData.Height, False);
   MapEditView.SetPosition(X, Y, True);
   if MapEditView.SelectionMode = selSingle then
    with MapEditView.ViewRect do
     MapEditView.Selected[Left + (Right - Left) div 2,
                           Top + (Bottom - Top) div 2] := True;

   FBrushMode := BrushMode;
  end else
   MapEditView.SetMapSize(0, 0, True);

  MapEditView.SelectionChanged;
  UpdateCoordinates;
 end;
 MapStatusBar.Panels[0].Text := MapTypeStr[BrushMode];
end;

procedure TMapFrame.SetScale(Value: Integer);
begin
 DoSetScale(Value, True);
end;

procedure TMapFrame.TilesChanged(DoRepaint: Boolean);
begin
 if FMapData <> nil then
 with FMapData do
 begin
  FillPicHeader(TileWidth, TileHeight, FPicHeader, FPicData);
  FillConverters(Owner as TBaseMapList);
 end;
 if DoRepaint then
  MapEditView.Invalidate;                              
end;

procedure TMapFrame.UpdateCoordinates;
var
 I: Integer;
begin
 if FMapData <> nil then
 begin
  with MapEditView do
   if (TileX >= 0) and (TileX < MapWidth) and
      (TileY >= 0) and (TileY < MapHeight) then
    I := TileY * MapWidth + TileX else
    I := -1;
  if I >= 0 then
  begin
   with MapEditView do
   begin
    MapStatusBar.Panels[2].Text := WideFormat('X: %d', [TileX]);
    MapStatusBar.Panels[3].Text := WideFormat('Y: %d', [TileY]);
   end;
   MapStatusBar.Panels[4].Text := WideFormat('Offset: %d', [I]);
   Exit;
  end;
 end;
 MapStatusBar.Panels[2].Text := '';
 MapStatusBar.Panels[3].Text := '';
 MapStatusBar.Panels[4].Text := '';
end;

procedure TMapFrame.CMMouseLeave(var Message: TMessage);
begin
 MapStatusBar.Panels[2].Text := '';
 MapStatusBar.Panels[3].Text := '';
 MapStatusBar.Panels[4].Text := '';
end;

procedure TMapFrame.MapEditViewReleasePasteBuffer(
  Sender: TCustomTileMapView; Data: Pointer);
begin
 TObject(Data).Free;
end;

procedure TMapFrame.MapEditViewCopyMapData(Sender: TCustomTileMapView;
  var Data: Pointer; const Rect: TRect; SelectionBuffer: Pointer);
var
 Info: TPasteBuffer;
 I, X, J, Y: Integer;
 RowStride, BigStride: Integer;
 SB: PBoolean;
 Src: PByte;
 Src16: PWord absolute Src;
 Src32: PLongWord absolute Src;
 Dst: PByte;
 Dst16: PWord absolute Dst;
 Dst32: PLongWord absolute Dst;
 EmptyTileIndex: Int64;
 Layer: TMapLayer;
begin
 if FMapData <> nil then
 begin
  Info := TPasteBuffer.Create(666);
  try
   Info.Width := Rect.Right - Rect.Left;
   Info.Height := Rect.Bottom - Rect.Top;
   J := 0;
   SetLength(Info.Layers, FMapData.Count);
   for I := 0 to FMapData.Count - 1 do
   begin
    Layer := FMapData.Layers[I];
    with Layer do
     if Assigned(TileSet) and
        (Flags and LF_VISIBLE <> 0) and
         DoGetLayerActive(Layer) then
     begin
      with Info.Layers[I] do
      begin
       GetMem(lData, Info.Width * CellSize * Info.Height);
       FillLayerFormat(lFormat);
      end;
      Inc(J);
     end;
   end;

   if J > 0 then
   begin
    Data := Info;
    //SetLength(Info.Layers, J);
    for I := 0 to FMapData.Count - 1 do
     with Info.Layers[I] do
      if lData <> nil then
       with FMapData.Layers[I] do
        if LayerData <> nil then
        begin
         TileSet.MaxIndex := IndexMask;
         EmptyTileIndex := TileSet.EmptyTileIndex and IndexMask;
         EmptyTileIndex := EmptyTileIndex shl IndexShift;
         X := Rect.Top * FMapData.Width + Rect.Left;
         SB := Addr(PByteArray(SelectionBuffer)[X]);
         Src := Addr(PByteArray(LayerData)[X * CellSize]);
         Dst := lData;
         RowStride := FMapData.Width - Info.Width;
         BigStride := RowStride * CellSize;
         for Y := 0 to Info.Height - 1 do
         begin
          case CellSize of
           1:
           for X := 0 to Info.Width - 1 do
           begin
            if SB^ then
             Dst^ := Src^ else
             Dst^ := EmptyTileIndex;
            Inc(SB);
            Inc(Src);
            Inc(Dst);
           end;
           2:
           for X := 0 to Info.Width - 1 do
           begin
            if SB^ then
             Dst16^ := Src16^ else
             Dst16^ := EmptyTileIndex;
            Inc(SB);
            Inc(Src16);
            Inc(Dst16);
           end;
           4:
           for X := 0 to Info.Width - 1 do
           begin
            if SB^ then
             Dst32^ := Src32^ else
             Dst32^ := EmptyTileIndex;
            Inc(SB);
            Inc(Src32);
            Inc(Dst32);
           end;
           else
           for X := 0 to Info.Width - 1 do
           begin
            if SB^ then
             Move(Src^, Dst^, CellSize) else
             Move(EmptyTileIndex, Dst^, CellSize);
            Inc(SB);           
            Inc(Src, CellSize);
            Inc(Dst, CellSize);
           end;
          end;
          Inc(SB, RowStride);
          Inc(Src, BigStride);
         end;
        end;
   end else
   begin
    Info.Free;
    Data := nil;
   end;
  except
   Info.Free;
   raise;
  end;
 end
end;

procedure PBA(Dest: TMapLayer; const Source: TPBLayerRec;
            SrcWidth, SrcHeight, CellX, CellY: Integer);
var
 X, Y, W, SrcStride, DstStride: Integer;
 Src: PByte;
 Dst: PByte;
 FMapData: TMap;
 Tile: TTileItem;
 Info: TCellInfo;
begin
 FMapData := TMap(Dest.Owner);
 SrcStride := SrcWidth * Source.lFormat.lfCellSize;
 DstStride := FMapData.Width * Dest.CellSize;
 W := SrcWidth;

 Src := Source.lData;
 Dst := Dest.LayerData;
 if CellX < 0 then
 begin
  Dec(Src, CellX * Source.lFormat.lfCellSize);
  Inc(W, CellX);
 end else
  Inc(Dst, CellX * Dest.CellSize);

 X := CellX + SrcWidth;
 if X > FMapData.Width then
  Dec(W, X - FMapData.Width);
 if W > 0 then
 begin
  if CellY < 0 then
   Dec(Src, CellY * SrcStride); 
  Dec(SrcStride, W * Source.lFormat.lfCellSize);
  Y := Max(0, CellY);
  Inc(Dst, Y * DstStride);
  Dec(DstStride, W * Dest.CellSize);
  with Dest do
   for Y := Y to Min(FMapData.Height, CellY + SrcHeight) - 1 do
   begin
    for X := 0 to W - 1 do
    begin
     CellInfoRead(Source.lFormat, Src^, Info);
     Tile := TileSet.Indexed[Info.TileIndex];
     if Tile <> TileSet.EmptyTile then
      WriteCellInfo(Dst^, Info);
     Inc(Src, Source.lFormat.lfCellSize);
     Inc(Dst, CellSize);
    end;
    Inc(Src, SrcStride);
    Inc(Dst, DstStride);
   end;
 end;
end;

procedure TMapFrame.MapEditViewPasteBufferApply(Sender: TCustomTileMapView;
  CellX, CellY: Integer; Data: Pointer);
var
 Info: TPasteBuffer;
 LR: TPBLayerRec;
 ML: TMapLayer;
 I: Integer;
begin
 if (FMapData <> nil) and (TObject(Data) is TPasteBuffer) then
 begin
  Info := TPasteBuffer(Data);
  case Info.ID of
   666, 888:
   for I := 0 to FMapData.Count - 1 do
   begin
    LR := Info.Layers[I];
    if LR.lData <> nil then
    begin
     ML := FMapData.Layers[I];
     if (ML.LayerData <> nil) and
        (ML.TileSet <> nil) and
        (ML.Flags and LF_VISIBLE <> 0) then
      PBA(ML, LR, Info.Width, Info.Height, CellX, CellY);
    end;
   end;
   777:
   begin
    LR := Info.Layers[0];
    if (LR.lData <> nil) and
       (LR.lFormat.lfTileSet <> nil) then
     for I := 0 to FMapData.Count - 1 do
     begin
      ML := FMapData.Layers[I];
      if (ML.LayerData <> nil) and
         (ML.TileSet = LR.lFormat.lfTileSet) and
         (ML.Flags and LF_VISIBLE <> 0) then
       PBA(ML, LR, Info.Width, Info.Height, CellX, CellY);
     end;
   end;
  end;
 end      
end;

procedure TMapFrame.MapEditViewCheckCompatibility(
  Sender: TCustomTileMapView; Data: Pointer; var Result: Boolean);
var
 Info: TPasteBuffer absolute Data;
 I: Integer;
begin
 if (FMapData <> nil) and
    (FMapData.Width > 0) and
    (FMapData.Height > 0) and
    (TObject(Data) is TPasteBuffer) and
    (Info.Width > 0) and
    (Info.Height > 0) and
    ((Info.ID = 666) or
     (Info.ID = 777) or
     (Info.ID = 888)) then
 begin
  case Info.ID of
   666, 888:
   begin
    if Length(Info.Layers) = 1 then
    begin
     with Info.Layers[0] do
      Result := (lData <> nil) and
               (FMapData.FindLayerByTileSet(lFormat.lfTileSet) <> nil);
    end else
    begin
     Result := False;
     for I := 0 to Length(Info.Layers) - 1 do
      with Info.Layers[I] do
       if (lData <> nil) and
          (FMapData.FindLayerByTileSet(lFormat.lfTileSet) <> nil) then
       begin
        Result := True;
        Exit;
       end;
    end;
   end;
   777:
   begin
    with Info.Layers[0] do
     Result := (lData <> nil) and
               (FMapData.FindLayerByTileSet(lFormat.lfTileSet) <> nil);
   end
  end; 
 end;
end;

procedure TMapFrame.MapEditViewGetCellState(Sender: TCustomTileMapView;
  CellX, CellY: Integer; var Dirty: Boolean);
var
 I, Idx, DataOffset: Integer;
 Layer: TMapLayer;
begin
 if FMapData <> nil then
 begin
  DataOffset := CellY * FMapData.Width + CellX;
  for I := 0 to FMapData.Count - 1 do
  begin
   Layer := FMapData.Layers[I];
   with Layer do
    if (LayerData <> nil ) and (TileSet <> nil) and
       (Flags and LF_VISIBLE <> 0) and
       DoGetLayerActive(Layer) then
    begin
     Idx := ReadTileIndex(PByteArray(LayerData)[DataOffset * CellSize]);
     Dirty := TileSet.Indexed[Idx] <> TileSet.EmptyTile;
     if Dirty then Exit;
    end;
  end;
 end;
end;

procedure TMapFrame.MapEditViewGetBufCellState(Sender: TCustomTileMapView;
  Data: Pointer; CellX, CellY: Integer; var Dirty: Boolean);
var
 Info: TPasteBuffer absolute Data;
 I, Idx, DataOffset: Integer;
begin
 if TObject(Data) is TPasteBuffer then
 begin
  DataOffset := CellY * Info.Width + CellX;
  for I := 0 to Length(Info.Layers) - 1 do
   with Info.Layers[I], lFormat do
    if Assigned(lData) and Assigned(lfTileSet) then
    begin
     Idx := TileIndexRead(lFormat, PByteArray(lData)[DataOffset * lfCellSize]);
     Dirty := lfTileSet.Indexed[Idx] <> lfTileSet.EmptyTile;
     if Dirty then Exit;
    end;
 end;
end;

procedure TMapFrame.MapEditViewBufClearCell(Sender: TCustomTileMapView;
  CellX, CellY: Integer; Data: Pointer);
var
 Info: TPasteBuffer absolute Data;
 I, DataOffset: Integer;
 Layer: TMapLayer;
 Nothing: Boolean;
begin
 if TObject(Data) is TPasteBuffer then
 begin
  DataOffset := CellY * Info.Width + CellX;
  Nothing := True;
  if Length(Info.Layers) = FMapData.Count then
   for I := 0 to Length(Info.Layers) - 1 do
   begin
    Layer := FMapData.Layers[I];
    if DoGetLayerActive(Layer) then
     with Info.Layers[I], lFormat do
      if Assigned(lData) and Assigned(lfTileSet) then
      begin
       lfTileSet.MaxIndex := lfIndexMask;
       TileIndexWrite(lFormat, PByteArray(lData)[DataOffset * lfCellSize],
                                                   lfTileSet.EmptyTileIndex);
       Nothing := False;
      end;
   end;
   
  if Nothing then
   for I := 0 to Length(Info.Layers) - 1 do
    with Info.Layers[I], lFormat do
     if Assigned(lData) and Assigned(lfTileSet) then
     begin
      lfTileSet.MaxIndex := lfIndexMask;
      TileIndexWrite(lFormat, PByteArray(lData)[DataOffset * lfCellSize],
                                                  lfTileSet.EmptyTileIndex);
     end;
 end;
end;

procedure TMapFrame.MapEditViewClearCell(Sender: TCustomTileMapView; CellX,
  CellY: Integer);
var
 I, DataOffset: Integer;
 Layer: TMapLayer;
begin
 if FMapData <> nil then
 begin
  DataOffset := CellY * FMapData.Width + CellX;
  for I := 0 to FMapData.Count - 1 do
  begin
   Layer := FMapData.Layers[I];
   with Layer do
    if (LayerData <> nil) and (TileSet <> nil) and
       (Flags and LF_VISIBLE <> 0) and
       DoGetLayerActive(Layer) then
    begin
     TileSet.MaxIndex := IndexMask;
     WriteTileIndex(PByteArray(LayerData)[DataOffset * CellSize],
                                                     TileSet.EmptyTileIndex);
    end;
  end;
 end;
end;

procedure TMapFrame.DoSetScale(Value: Integer; DoRepaint: Boolean);
var
 OldScale, X, Y: Integer;
begin
 if Value < 1 then Value := 1;
 if Value > 16 then Value := 16;
 OldScale := FScale;
 if Assigned(FOnScaleChange) then
  FOnScaleChange(Self, Value);
 FScale := Value;
 MapStatusBar.Panels[1].Text := WideFormat('Scale: x%d', [FScale]);
 if FMapData <> nil then
 begin
  if MapEditView.ClientLeft > 0 then
   X := ((MapEditView.MapWidth * MapEditView.TileWidth) shr 1) div OldScale else
   X := (MapEditView.OffsetX + MapEditView.ClientWidth shr 1) div OldScale;

  if MapEditView.ClientTop > 0 then
   Y := ((MapEditView.MapHeight * MapEditView.TileHeight)  shr 1) div OldScale else
   Y := (MapEditView.OffsetY + MapEditView.ClientHeight shr 1) div OldScale;

  MapEditView.SetTileSize(FMapData.TileWidth * FScale,
                          FMapData.TileHeight * FScale, False);
  MapEditView.SetPosition(X * FScale - MapEditView.ClientWidth shr 1,
                          Y * FScale - MapEditView.ClientHeight shr 1, DoRepaint);
 end else
 if DoRepaint then
  Invalidate;
end;

procedure TMapFrame.MapEditViewMouseWheelDown(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
 if ssShift in Shift then
 begin
  SetScale(FScale - 1);
  Handled := True;
 end;
end;

procedure TMapFrame.MapEditViewMouseWheelUp(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
 if ssShift in Shift then
 begin
  SetScale(FScale + 1);
  Handled := True;
 end;
end;

procedure TMapFrame.MapEditViewSetPosition(Sender: TCustomTileMapView;
  var NewX, NewY: Integer);
begin
 if FMapData <> nil then
 begin
  FMapData.MapX := NewX div FScale;
  FMapData.MapY := NewY div FScale;  
 end;
end;

procedure TMapFrame.MapEditViewMakePasteBufferCopy(
  Sender: TCustomTileMapView; Source: Pointer; var Dest: Pointer);
var
 Src: TPasteBuffer absolute Source;
 Dst: TPasteBuffer;
 I, J, Len: Integer;
 SrcL, DstL: ^TPBLayerRec;
 Layer: TMapLayer;
 Nothing: Boolean;
begin
 if (FMapData <> nil) and (TObject(Source) is TPasteBuffer) then
 begin
  Dst := TPasteBuffer.Create(666);
  try
   Dst.Width := Src.Width;
   Dst.Height := Src.Height;
   Nothing := True;
   SetLength(Dst.Layers, FMapData.Count);
   case Src.ID of
    666, 888:
    begin
     for I := 0 to Length(Src.Layers) - 1 do
     begin
      SrcL := Addr(Src.Layers[I]);
      for J := 0 to FMapData.Count - 1 do
      begin
       Layer := FMapData.Layers[J];
       if (Layer.TileSet = SrcL.lFormat.lfTileSet) and
         (Layer.Flags and LF_VISIBLE <> 0) and
          DoGetLayerActive(Layer) then
       begin
        DstL := Addr(Dst.Layers[J]);
        DstL.lFormat := SrcL.lFormat;
        Len := Src.Width * SrcL.lFormat.lfCellSize * Src.Height;
        ReallocMem(DstL.lData, Len);
        Move(SrcL.lData^, DstL.lData^, Len);
        Nothing := False;
       end;
      end;
     end;
    end;
    777:
    begin
     SrcL := Addr(Src.Layers[0]);
     for I := 0 to FMapData.Count - 1 do
     begin
      Layer := FMapData.Layers[I];
      with Layer do
       if (TileSet <> nil) and
          (Flags and LF_VISIBLE <> 0) and
          DoGetLayerActive(Layer) then
         if (SrcL.lData <> nil) and
            (SrcL.lFormat.lfTileSet = TileSet) then
         begin
          DstL := Addr(Dst.Layers[I]);
          DstL.lFormat := SrcL.lFormat;
          Len := (Src.Width * Src.Height) * DstL.lFormat.lfCellSize;
          GetMem(DstL.lData, Len);
          Move(SrcL.lData^, DstL.lData^, Len);
          Nothing := False;
         end;
     end;
    end;
   end;
   if Nothing then
    Dst.Free else
    Dest := Dst;
  except
   Dst.Free;
   raise;
  end;
 end;
end;

procedure TMapFrame.FillConverters(MapList: TBaseMapList);
begin
 MapList.TileSetList.FillCvtRecList(FConverters,
                              [(8 - 1) or IMG_Y_FLIP],
                              DRAW_TRANSPARENT);
end;


procedure TMapFrame.SetMapModified(Value: Boolean);
begin
 FMapModified := Value;
 if Value then
  SetMap(FMapData, FScale, FBrushMode);
end;

procedure TMapFrame.MapEditViewEnter(Sender: TObject);
begin
 MapEditView.SelectionChanged;
end;

procedure TMapFrame.MapEditViewSelectionChanged(Sender: TCustomTileMapView);
begin
 if Sender.MultiSelected then
 begin
  with Sender.SelectionRect do
   MapStatusBar.Panels[5].Text := WideFormat('Selection: (%d, %d) %dx%d', [Left, Top,
   Right - Left, Bottom - Top]);
 end else
  MapStatusBar.Panels[5].Text := '';
end;

function TMapFrame.DoGetLayerActive(Layer: TMapLayer): Boolean;
begin
 Result := Layer.Flags and LF_LAYER_ACTIVE <> 0;
 if Assigned(FOnGetLayerActive) then
  FOnGetLayerActive(Self, Layer, Result);
end;

procedure TMapFrame.SetMapData(Value: TMap);
begin
 SetMap(Value);
end;

initialization
 CF_MAPDATA := RegisterClipboardFormat('Kusharami Map Data');
end.
