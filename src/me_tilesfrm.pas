unit me_tilesfrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, TilesEx, BmpImg, BitmapEx, ComCtrls, CommCtrl,
  TntComCtrls, TileMapView, me_lib;

type
  PRGBQuads = ^TRGBQuads;

  TTilesFrame = class;
  TTileFrameEvent = procedure(Sender: TTilesFrame) of object;

  TScaleChangeEvent = procedure(Sender: TObject; var Value: Integer) of object;  

  TTilesFrame = class(TFrame)
    StatusBar: TTntStatusBar;
    TileSetView: TTileMapView;
    procedure TileSetViewDrawCell(Sender: TCustomTileMapView;
      const Rect: TRect; CellX, CellY: Integer);
    procedure TileSetViewSelectionChanged(Sender: TCustomTileMapView);
    procedure TileSetViewMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure TileSetViewMouseWheelDown(Sender: TObject;
      Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
    procedure TileSetViewMouseWheelUp(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure TileSetViewBufClearCell(Sender: TCustomTileMapView; CellX,
      CellY: Integer; Data: Pointer);
    procedure TileSetViewCheckCompatibility(Sender: TCustomTileMapView;
      Data: Pointer; var Result: Boolean);
    procedure TileSetViewClearCell(Sender: TCustomTileMapView; CellX,
      CellY: Integer);
    procedure TileSetViewCopyMapData(Sender: TCustomTileMapView;
      var Data: Pointer; const Rect: TRect; SelectionBuffer: Pointer);
    procedure TileSetViewGetBufCellState(Sender: TCustomTileMapView;
      Data: Pointer; CellX, CellY: Integer; var Dirty: Boolean);
    procedure TileSetViewGetCellState(Sender: TCustomTileMapView; CellX,
      CellY: Integer; var Dirty: Boolean);
    procedure TileSetViewPasteBufferApply(Sender: TCustomTileMapView;
      CellX, CellY: Integer; Data: Pointer);
    procedure TileSetViewMakePasteBufferCopy(Sender: TCustomTileMapView;
      Source: Pointer; var Dest: Pointer);
    procedure TileSetViewReleasePasteBuffer(Sender: TCustomTileMapView;
      Data: Pointer);
    procedure TileSetViewEnter(Sender: TObject);
    procedure TileSetViewAfterPaint(Sender: TCustomTileMapView);
  private
    FTileSet: TTileSet; //Link to the tiles list
    FColorTable: TColorTable;
    FScale: Integer;
    FConverter, FCvt1, FCvt2: TBitmapConverter;
    FFirstColor: Byte; //Index of the first color in palette to convert to 8bpp
    FCustomColorMode: Boolean;
    FOnTileSetChange: TTileFrameEvent;
    FSelectedTile: Integer;
    FPicHeader: TPicHeader;
    FPicData: TBitmapContainer;
    FTileSetModified: Boolean;
    FIndexUnderCursor: Integer;
    FOnScaleChange: TScaleChangeEvent;
    procedure SetScale(Value: Integer);
    procedure SetColorTable(Value: TColorTable);
    procedure SetSelectedTile(Value: Integer);
    procedure CMMouseLeave(var Message: TMessage); message CM_MOUSELEAVE;
    procedure SetTileSet(Value: TTileSet);
    procedure SetTileSetModified(Value: Boolean);
  public
    property IndexUnderCursor: Integer read FIndexUnderCursor;
    property CustomColorMode: Boolean read FCustomColorMode write FCustomColorMode;
    property TileSet: TTileSet read FTileSet write SetTileSet;
    property FirstColor: Byte read FFirstColor write FFirstColor;
    property SelectedTile: Integer read FSelectedTile write SetSelectedTile;
    property Scale: Integer read FScale write SetScale;
    property ColorTable: TColorTable read FColorTable write SetColorTable;
    property TileSetModified: Boolean read FTileSetModified write SetTileSetModified;

    procedure UpdateTileIndex;
    procedure UpdateSelectedTile;

    procedure RefreshColorTable(DoRepaint: Boolean = True);
    procedure DoSetScale(Value: Integer; DoRepaint: Boolean = True);
    procedure TilesChanged(DoRepaint: Boolean = True);
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure DoSetTileSet(Value: TTileSet; Allways: Boolean = False;
                           AColorTable: TColorTable = Pointer(-1);
                           TilesInARow: Integer = -1;
                           ASelectedTile: Integer = -1;
                           X: Integer = -1;
                           Y: Integer = -1;
                           AScale: Integer = 0);
  published
    property OnTileSetChange: TTileFrameEvent read FOnTileSetChange write FOnTileSetChange;
    property OnScaleChange: TScaleChangeEvent read FOnScaleChange
                                             write FOnScaleChange;
  end;

function MakePasteBufferCopy(View: TCustomTileMapView; TileSet: TTileSet;
                             Source: TPasteBuffer): TPasteBuffer;

implementation

uses HexUnit;

{$R *.dfm}
{$R cursor.res}

function MakePasteBufferCopy(View: TCustomTileMapView; TileSet: TTileSet;
                             Source: TPasteBuffer): TPasteBuffer;
var
 I, J, X, L, WidthBytes, Len, BC, Sz, Y, TilesSize, MaxW, MaxH: Integer;
 SrcL, DstL: ^TPBLayerRec;
 Cvt: TBitmapConverter;
 PT, PP, CP: PByte;
 Tile: TTileItem;
 DrawNodes: array of TDrawInfo;
 DrawNode: PDrawInfo;
 DrawInf: TDrawInfoRec;
 CvtList: TCvtRecList;
 FillValue: LongWord;
 TSZ: Integer;
 Same, Dummy: Boolean;
 ts: TTileSet;
 TempTile: Pointer;
 FlgLst: TCompositeFlags;
 CellInfo: TCellInfo;
 AttrList: array of Variant;
begin
 if (Source <> nil) and (TileSet <> nil) then
 begin
  Result := TPasteBuffer.Create(777);
  try
   Result.Width := Source.Width;
   Result.Height := Source.Height;
   if Source.ID = 777 then
   begin
    with Source.Layers[0] do
     if (lData <> nil) and
        (lFormat.lfTileSet = TileSet) then
     begin
      SetLength(Result.Layers, 1);
      DstL := Addr(Result.Layers[0]);
      DstL.lFormat := lFormat;
      Len := Source.Width * lFormat.lfCellSize * Source.Height;
      GetMem(DstL.lData, Len);
      Move(lData^, DstL.lData^, Len);
      Exit;
     end;
   end;
   case Source.ID of
    666, 775, 776, 777, 888:
    begin
     DstL := nil;
     if (Source.ID = 666) or (Source.ID = 888) then
     begin
      for I := Length(Source.Layers) - 1 downto 0 do
      begin
       SrcL := Addr(Source.Layers[I]);
       if (SrcL.lData <> nil) and
          (SrcL.lFormat.lfTileSet = TileSet) then
       begin
        DstL := SrcL;
        Break;
       end;
      end;
      if DstL <> nil then
      begin
       for I := 0 to Length(Source.Layers) - 1 do
       begin
        SrcL := Addr(Source.Layers[I]);
        if (SrcL.lData <> nil) and (SrcL <> DstL) then
        begin
         DstL := nil;
         Break;
        end;
       end;
      end;
     end;
     if DstL <> nil then
     begin
      SrcL := DstL;
      SetLength(Result.Layers, 1);
      with Result.Layers[0] do
      begin
       lFormat := SrcL.lFormat;
       Len := Source.Width * SrcL.lFormat.lfCellSize * Source.Height;
       GetMem(lData, Len);
       Move(SrcL.lData^, lData^, Len);
      end;
     end else
     begin
      if (Source.ID = 776) and (Source.TileSet = nil) then
       Result.ID := 777 else
       Result.ID := Source.ID;
      case Result.ID of
       666, 775, 777:
       begin
        if Result.ID = 777 then
         Result.ID := 776 else
         Result.ID := 888;
        MaxW := TileSet.TileWidth;
        MaxH := TileSet.TileHeight;
        SetLength(Result.Layers, Length(Source.Layers));
        ts := nil;
        L := 0;
        Same := True;
        for X := 0 to Length(Source.Layers) - 1 do
        begin
         SrcL := Addr(Source.Layers[X]);
         with SrcL.lFormat do
          if lfTileSet <> nil then
          begin
           if lfTileSet.TileWidth > MaxW then
            MaxW := lfTileSet.TileWidth;
           if lfTileSet.TileWidth > MaxH then
            MaxH := lfTileSet.TileWidth;
           if Same and (ts <> nil) and
            ((ts.TileWidth <> lfTileSet.TileWidth) or
             (ts.TileHeight <> lfTileSet.TileHeight) or
             (ts.TransparentColor <> lfTileSet.TransparentColor) or
             (ts.DrawFlags <> lfTileSet.DrawFlags) or
             (ts.TileBitmap.ImageFormat <> lfTileSet.TileBitmap.ImageFormat) or
             (ts.TileBitmap.ImageFlags <> lfTileSet.TileBitmap.ImageFlags) or
             (L <> Length(lfTileSet.TileBitmap.CompositeFlags)) or
             not CompareMem(ts.TileBitmap.CompositeFlags,
                lfTileSet.TileBitmap.CompositeFlags, L shl 1)) then
            Same := False;
           if ts = nil then
           begin
            ts := lfTileSet;
            L := Length(ts.TileBitmap.CompositeFlags);
           end;
          end;
         with Result.Layers[X] do
         begin
          lFormat := SrcL.lFormat;
          Len := Source.Width * SrcL.lFormat.lfCellSize * Source.Height;
          GetMem(lData, Len);
          Move(SrcL.lData^, lData^, Len);
         end;
        end;
        Len := Source.Width * Source.Height;
        SetLength(Result.Map, Len);

        Result.TileSet := TTileSet.Create;
        with Result.TileSet do
        begin
         OptimizationFlags := OPT_CHECK_EMPTY or OPT_CHECK_COLORS;
         SetLength(FlgLst, 1);
         FlgLst[0] := 8 - 1;
         if Same then
         begin
          if ts <> nil then
           AssignAttrDefs(ts);
          I := -1;
          for Y := 0 to Len - 1 do
          begin
           for X := 0 to Length(Source.Layers) - 1 do
           begin
            SrcL := Addr(Source.Layers[X]);
            if SrcL.lData <> nil then
            begin
             PP := Addr(PByteArray(SrcL.lData)[Y * SrcL.lFormat.lfCellSize]);
             if SrcL.lFormat.lfTileSet <> nil then
              with SrcL.lFormat.lfTileSet do
              begin
               BC := TileBitmap.BitsCount;
               CellInfoRead(SrcL.lFormat, PP^, CellInfo);
               Tile := Indexed[CellInfo.TileIndex];
               if (Tile <> nil) and (Tile <> EmptyTile) then
               begin
                if I >= 0 then
                begin
                 if (BC < 8) and (CellInfo.PaletteIndex >= 0) then
                  J := CellInfo.PaletteIndex shl BC else
                  J := Tile.FirstColor;
                 if J <> I then
                 begin
                  Same := False;
                  Break;
                 end;
                end else
                begin
                 if (BC < 8) and (CellInfo.PaletteIndex >= 0) then
                  I := CellInfo.PaletteIndex shl BC else
                  I := Tile.FirstColor;
                end;
               end;
              end;
            end;
            if not Same then
             Break;
           end;
          end;
          if Same then
           FlgLst := ts.TileBitmap.FlagsList;
         end;

         SwitchFormat(FlgLst, MaxW, MaxH);

         if not Same then
          DrawFlags := 0;

         SetLength(DrawNodes, Length(Source.Layers));
         SetLength(CvtList, Length(Source.Layers));

         TSZ := TileSize;
         GetMem(TempTile, TSZ);
         try
          try
           for X := 0 to Length(Source.Layers) - 1 do
            with Source.Layers[X] do
            begin
             ConvertRecInit(CvtList[X]);
             if (lData <> nil) and (lFormat.lfTileSet <> nil) then
              lFormat.lfTileSet.FillConvertRec(CvtList[X],
                    TileBitmap.FlagsList, DRAW_TRANSPARENT);
            end;

           for Y := 0 to Len - 1 do
           begin
            DrawInf.First := nil;
            DrawNode := Pointer(DrawNodes);
            for X := 0 to Length(Source.Layers) - 1 do
            begin
             SrcL := Addr(Source.Layers[X]);
             if SrcL.lData <> nil then
             begin
              PP := Addr(PByteArray(SrcL.lData)[Y * SrcL.lFormat.lfCellSize]);
              if SrcL.lFormat.lfTileSet <> nil then
               with SrcL.lFormat.lfTileSet do
               begin
                CellInfoRead(SrcL.lFormat, PP^, DrawNode.Info);

                Tile := Indexed[DrawNode.Info.TileIndex];
                if (Tile <> nil) and (Tile <> EmptyTile) then
                begin
                 DrawNode.TileSet := SrcL.lFormat.lfTileSet;
                 DrawNode.Tile := Tile;
                 DrawNode.FC := X;
                 AddDrawInfo(DrawInf, DrawNode);
                 Inc(DrawNode);
                end;
               end;
             end;
            end;

            TileBitmap.ImageData := TempTile;
            FillValue := TileSet.TransparentColor;
            DrawNode := DrawInf.First;

            if not Same then
            begin
             if DrawNode <> nil then
              Inc(FillValue, DrawNode.Tile.FirstColor);
            end;

            TileBitmap.FillData(FillValue);

            I := 0;

            while DrawNode <> nil do
            begin
             with DrawNode^ do
             begin
              Cvt := CvtList[FC].tcvtDraw[Info.XFlip, Info.YFlip];

              BC := TileSet.TileBitmap.BitsCount;

              if (BC < 8) and (Info.PaletteIndex >= 0) then
               I := Info.PaletteIndex shl BC else
               I := Tile.FirstColor;

              if not Same then
               Cvt.SrcInfo.PixelModifier := I else
               Cvt.SrcInfo.PixelModifier := 0;

              TileSet.TileDraw(Tile.TileData^, TileBitmap, 0, 0, Cvt);
             end;
             DrawNode := DrawNode.Next;
            end;

            X := 1;
            DrawNode := DrawInf.First;
            if (AttrCount > 0) and
               ((DrawNode <> nil) and
                (DrawNode.Next = nil)) then
             Inc(X, AttrCount);

            SetLength(AttrList, X);
            AttrList[0] := I;
            if X > 1 then
             with DrawNode.Tile do
              for X := 0 to Min(Length(Attributes), AttrCount) - 1 do
               AttrList[X + 1] := Attributes[X];

            Result.Map[Y] := AddOptimized(TempTile^, AttrList, Dummy, Dummy).TileIndex;
           end;
          finally
           CvtRecListFree(CvtList);
          end;
         finally
          FreeMem(TempTile);
         end;
        end;
       end;
       else
       with Result do
       begin
        L := 0;
        Len := Source.Width * Source.Height;
        for I := 0 to Length(Source.Layers) - 1 do
        begin
         SrcL := Addr(Source.Layers[I]);
         if (SrcL.lData <> nil) and
            (SrcL.lFormat.lfTileSet <> nil) then
         begin
          SetLength(Layers, L + 1);
          with Layers[L] do
          begin
           lFormat := SrcL.lFormat;
           X := Len * lFormat.lfCellSize;
           GetMem(lData, X);
           Move(SrcL.lData^, lData^, X);
          end;
         end;
         Inc(L);
        end;
        TileSet := TTileSet.Create;
        TileSet.Assign(Source.TileSet);
        SetLength(Map, Len);
        Move(Source.Map[0], Map[0], Len shl 2);
       end;
      end;
     end;
     Exit;
    end;
   end;
   FreeAndNil(Result);
  except
   Result.Free;
   raise;
  end;
 end;
end;

{ TTilesFrame }

constructor TTilesFrame.Create(AOwner: TComponent);
begin
 inherited;
 FConverter := TBitmapConverter.Create;
 FCvt1.Free;
 FCvt2.Free;
 FPicData := TBitmapContainer.Create;
 FPicData.ImageFlags := (8 - 1) or IMG_Y_FLIP;
 TileSetView.MapWidth := 18;
 DoSetScale(2, False);
end;

destructor TTilesFrame.Destroy;
begin
 FreeMem(FPicData.ImageData);
 FPicData.Free;
 FConverter.Free;
 inherited;
end;

procedure TTilesFrame.SetColorTable(Value: TColorTable);
begin
 FColorTable := Value;
 RefreshColorTable;
end;

procedure TTilesFrame.SetScale(Value: Integer);
begin
 DoSetScale(Value, True);
end;

procedure TTilesFrame.SetSelectedTile(Value: Integer);
begin
 FSelectedTile := Value;
 if FTileSet <> nil then
  Value := Max(0, Min(FTileSet.TilesCount - 1, Value)) else
  Value := -1;
 if Value >= 0 then
  TileSetView.SelectedByIndex[Value] := True;
end;

procedure TTilesFrame.DoSetTileSet(Value: TTileSet; Allways: Boolean;
   AColorTable: TColorTable;
   TilesInARow, ASelectedTile, X, Y, AScale: Integer);
begin
 if AColorTable = Pointer(-1) then AColorTable := FColorTable;
 if ASelectedTile < 0 then ASelectedTile := FSelectedTile;
 if AScale <= 0 then AScale := FScale;
 if TilesInARow < 0 then TilesInARow := TileSetView.MapWidth;
 if X = -1 then X := TileSetView.OffsetX div FScale;
 if Y = -1 then Y := TileSetView.OffsetY div FScale;
 if Allways or FTileSetModified or (FTileSet <> Value) or
    (ASelectedTile <> FSelectedTile) or
    (AScale <> FScale) or
    (TilesInARow <> TileSetView.MapWidth) or
    (X <> TileSetView.OffsetX div FScale) or
    (Y <> TileSetView.OffsetY div FScale) then
 begin
  FTileSetModified := False;
  TileSetView.Deselect;
  FTileSet := Value;
  DoSetScale(AScale, False);
  if TilesInARow = 0 then
   TilesInARow := 256;
  FColorTable := AColorTable;
  RefreshColorTable(False);
  TileSetView.SetMapSize(TilesInARow, TileSetView.MapHeight, False);
 

  FSelectedTile := ASelectedTile;
  if (X >= 0) or (Y >= 0) then
  begin
   TilesChanged(False);

   if (X + 1 > 0) and (Y + 1 > 0) then
    TileSetView.SetPosition(X * FScale, Y * FScale, True);
  end else
   TilesChanged(True);
   
  TileSetView.SelectionChanged;

  UpdateTileIndex;
 end;
end;

procedure TTilesFrame.TilesChanged(DoRepaint: Boolean);
var
 SaveTile: Integer;
begin
 if Assigned(FOnTileSetChange) then
  FOnTileSetChange(Self);
 SaveTile := FSelectedTile;
 if FTileSet <> nil then
 begin
  with FTileSet do
  begin
   FillPicHeader(TileWidth, TileHeight, FPicHeader, FPicData);
   FillConverter(FConverter, [(8 - 1) or IMG_Y_FLIP]);
   with TileSetView do
    if MapWidth > 0 then
     SetMapSize(MapWidth, (TilesCount + (MapWidth - 1)) div MapWidth, DoRepaint) else
     SetMapSize(MapWidth, 0, DoRepaint);
  end;
 end else
  TileSetView.SetMapSize(TileSetView.MapWidth, 0);
 if TileSetView.SelectionMode <> selMulti then
  SetSelectedTile(SaveTile) else
  FSelectedTile := SaveTile;

 if DoRepaint then
  TileSetView.Invalidate;
end;

procedure TTilesFrame.TileSetViewDrawCell(Sender: TCustomTileMapView;
  const Rect: TRect; CellX, CellY: Integer);
var
 Info: TPasteBuffer;
 Tile: TTileItem;
 X: Integer;
 Color: TColor;
 cvt: TBitmapConverter;
begin
 with Sender, FTileSet do
 begin
  cvt := nil;
  Tile := nil;

  if TObject(PasteBuffer) is TPasteBuffer then
  begin
   Info := TPasteBuffer(PasteBuffer);
   Dec(CellX, PasteBufRect.Left);
   Dec(CellY, PasteBufRect.Top);
   if (CellX < 0) or (CellX >= Info.Width) or
      (CellY < 0) or (CellY >= Info.Height) then
    Info := nil else
   begin
    X := CellY * Info.Width + CellX;
    case Info.ID of
     888, 776:
     with Info.TileSet do
     begin
      Tile := Indexed[Info.Map[X]];

      if FCvt1 = nil then
      begin
       FCvt1 := TBitmapConverter.Create;
       FillConverter(FCvt1, [(8 - 1) or IMG_Y_FLIP]);
      end;
      cvt := FCvt1;
     end;
     777:
     with Info.Layers[0] do
      if (lData <> nil) and (lFormat.lfTileSet <> nil) then //and (lFormat.lfTileSet = FTileSet) then
       with lFormat.lfTileSet do
       begin
        Tile := Indexed[TileIndexRead(lFormat,
        PByteArray(lData)[X * lFormat.lfCellSize])];
        if Tile = EmptyTile then
         Tile := nil;

        if FCvt2 = nil then
        begin
         FCvt2 := TBitmapConverter.Create;
         FillConverter(FCvt2, [(8 - 1) or IMG_Y_FLIP]);
        end;
        cvt := FCvt2;
       end;
    end;
   end;
  end else
   Info := nil;


  if ((Info <> nil) and (Tile <> nil)) or (InternalIndex < TilesCount) then
  begin
   if Tile = nil then
   begin
    Tile := Indexed[InternalIndex];
    if Tile = EmptyTile then
     Tile := nil;
    cvt := FConverter;
   end;
   if Tile <> nil then
    with Tile do
    begin
     if FCustomColorMode then
      cvt.SrcInfo.PixelModifier := FFirstColor else
      cvt.SrcInfo.PixelModifier := FirstColor;

     TileDraw(TileData^, FPicData, 0, 0, cvt);
    end;
  end;

  if Tile <> nil then
  begin
   GdiFlush;
   StretchDIBits(Canvas.Handle,
                 Rect.Left, Rect.Top,
                 Rect.Right - Rect.Left,
                 Rect.Bottom - Rect.Top,
                 0, 0,
                 FPicHeader.bhHead.bhInfoHeader.biWidth,
                 FPicHeader.bhHead.bhInfoHeader.biHeight,
                 FPicData.ImageData,
                 TBitmapInfo(Addr(FPicHeader.bhHead.bhInfoHeader)^),
                 DIB_RGB_COLORS, SRCCOPY);
  end;

  if Tile = nil then
   with Canvas do
   begin
    if ColorTable <> nil then
     Color := ColorTable.RGBZ[EmptyTile.FirstColor + FTileSet.TransparentColor] else
     Color := clWhite;
    Brush.Color := Color;
    Brush.Style := bsSolid;
    FillRect(Rect);
    Pen.Color := clWhite;
    Pen.Style := psSolid;
    Pen.Mode := pmCopy;
    Brush.Color := (not Color) and $FFFFFF;
    Brush.Style := bsDiagCross;
    Rectangle(Rect);
   end;
 end;
end;

procedure TTilesFrame.UpdateTileIndex;
begin
 FIndexUnderCursor := -1;
 if FTileSet <> nil then
 begin
  with TileSetView do
   if (TileX >= 0) and (TileX < MapWidth) and
      (TileY >= 0) and (TileY < MapHeight) then
    FIndexUnderCursor := TileY * MapWidth + TileX;
  if FIndexUnderCursor >= 0 then
  begin
   with TileSetView do
   begin
    StatusBar.Panels[2].Text := WideFormat('X: %d', [TileX]);
    StatusBar.Panels[3].Text := WideFormat('Y: %d', [TileY]);
   end;
   if FIndexUnderCursor < FTileSet.TilesCount then
    StatusBar.Panels[4].Text := WideFormat('Index: %d', [FIndexUnderCursor]) else
    StatusBar.Panels[4].Text := '';
   Exit;
  end;
 end;
 StatusBar.Panels[2].Text := '';
 StatusBar.Panels[3].Text := '';
 StatusBar.Panels[4].Text := '';
end;

procedure TTilesFrame.TileSetViewSelectionChanged(Sender: TCustomTileMapView);
begin
 FSelectedTile := Sender.SelectedCellIndex;

 if Sender.MultiSelected then
 begin
  with Sender.SelectionRect do
   StatusBar.Panels[5].Text := WideFormat('Selection: (%d, %d) %dx%d', [Left, Top,
   Right - Left, Bottom - Top]);
 end else
  StatusBar.Panels[5].Text := '';
end;

procedure TTilesFrame.RefreshColorTable(DoRepaint: Boolean);
begin
 if FTileSet <> nil then
 begin
  if FColorTable <> nil then
   with FColorTable do
    ColorFormat.ConvertToRGBQuad(ColorData, Addr(FPicHeader.bhPalette), ColorsCount) else
    FillChar(FPicHeader.bhPalette, SizeOf(TRGBQuads), 0);
  if DoRepaint then
   TileSetView.Invalidate;
 end;
end;

procedure TTilesFrame.TileSetViewMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
var
 W, H: Integer;
begin
 UpdateTileIndex;
 if TileSetView.MouseMode in [mmMultiSelectNew,
                              mmMultiSelectInclude,
                              mmMultiSelectExclude] then
 begin
  with TileSetView.TempSelectionRect do
  begin
   W := Right - Left;
   H := Bottom - Top;
   if (W > 0) and (H > 0) then
    StatusBar.Panels[5].Text :=
        WideFormat('Selection: (%d, %d) %dx%d', [Left, Top, W, H]) else
    StatusBar.Panels[5].Text := '';
  end;
 end;
end;

procedure TTilesFrame.UpdateSelectedTile;
begin
 with TileSetView do
  DrawCell(SelectedCell.X, SelectedCell.Y);
end;

procedure TTilesFrame.CMMouseLeave(var Message: TMessage);
begin
 StatusBar.Panels[2].Text := '';
 StatusBar.Panels[3].Text := '';
 StatusBar.Panels[4].Text := '';
end;

procedure TTilesFrame.TileSetViewMouseWheelDown(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
 if ssShift in Shift then
 begin
  SetScale(FScale - 1);
  Handled := True;
 end;
end;

procedure TTilesFrame.TileSetViewMouseWheelUp(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
 if ssShift in Shift then
 begin
  SetScale(FScale + 1);
  Handled := True;
 end;
end;

procedure TTilesFrame.SetTileSet(Value: TTileSet);
begin
 DoSetTileSet(Value);
end;

procedure TTilesFrame.DoSetScale(Value: Integer; DoRepaint: Boolean);
var
 OldScale, X, Y: Integer;
begin
 if Value < 1 then Value := 1;
 if Value > 16 then Value := 16;
 if Assigned(FOnScaleChange) then
  FOnScaleChange(Self, Value);
 OldScale := FScale;
 FScale := Value;
 StatusBar.Panels[1].Text := WideFormat('Scale: x%d', [FScale]);
 if FTileSet <> nil then
 begin
  if TileSetView.ClientLeft > 0 then
   X := ((TileSetView.MapWidth * TileSetView.TileWidth) shr 1) div OldScale else
   X := (TileSetView.OffsetX + TileSetView.ClientWidth shr 1) div OldScale;

  if TileSetView.ClientTop > 0 then
   Y := ((TileSetView.MapHeight * TileSetView.TileHeight)  shr 1) div OldScale else
   Y := (TileSetView.OffsetY + TileSetView.ClientHeight shr 1) div OldScale;

  TileSetView.SetTileSize(FTileSet.TileWidth * Value,
                          FTileSet.TileHeight * Value);
  TileSetView.SetPosition(X * FScale - TileSetView.ClientWidth shr 1,
                          Y * FScale - TileSetView.ClientHeight shr 1, DoRepaint);
 end else
 if DoRepaint then
  Invalidate;
end;

procedure TTilesFrame.TileSetViewBufClearCell(Sender: TCustomTileMapView;
  CellX, CellY: Integer; Data: Pointer);
var
 Info: TPasteBuffer absolute Data;
 I: Integer;
begin
 if TObject(Data) is TPasteBuffer then
 begin
  I := CellY * Info.Width + CellX;
  case Info.ID of
   777:
   begin
    with Info.Layers[0], lFormat do
     if Assigned(lData) and Assigned(lfTileSet) then
      TileIndexWrite(lFormat,
       PByteArray(lData)[I * lfCellSize],
        lfTileSet.EmptyTileIndex);
   end;
   888, 776:
    Info.Map[I] := -1;
  end;
 end;
end;

procedure TTilesFrame.TileSetViewCheckCompatibility(
  Sender: TCustomTileMapView; Data: Pointer; var Result: Boolean);
var
 Info: TPasteBuffer absolute Data;
 I: Integer;
begin
 Result := TObject(Data) is TPasteBuffer;
 if Result then
 case Info.ID of
  666, 776, 777, 888:
  if (FTileSet <> nil) and
     (TileSetView.MapWidth > 0) and
     (TileSetView.MapHeight > 0) and
     (Info.Width > 0) and
     (Info.Height > 0) then
  begin
   case Info.ID of
    666:
    begin
     Result := False;
     for I := 0 to Length(Info.Layers) - 1 do
      with Info.Layers[I] do
       if lData <> nil then
       begin
        Result := True;
        Exit;
       end;
    end;
    777:
    with Info.Layers[0] do
     Result := lData <> nil;
    776, 888:
     Result := Info.TileSet <> nil;
   end;
  end;
 end; 
end;

procedure TTilesFrame.TileSetViewClearCell(Sender: TCustomTileMapView;
  CellX, CellY: Integer);
var
 I: Integer;
 Item: TTileItem;
 obj: TObject;
 Info: TPasteBuffer absolute Obj;
begin
 if FTileSet <> nil then
 begin
  obj := Sender.PasteBuffer;
  if (obj is TPasteBuffer) and (Info.ID = 777) then
  begin
   Info.ID := 776;
   TileSetViewMakePasteBufferCopy(Sender, Info, Sender.PasteBuffer);
   Info.Free;
  end;
  I := CellY * TileSetView.MapWidth + CellX;
  with FTileSet do
  begin
   Item := Indexed[I];
   if Item <> EmptyTile then
    Remove(Item);
  end;
 end;
end;

procedure TTilesFrame.TileSetViewCopyMapData(Sender: TCustomTileMapView;
  var Data: Pointer; const Rect: TRect; SelectionBuffer: Pointer);
var
 Info: TPasteBuffer;
 X, RowStride, Src, Y, ETI: Integer;
 SB: PBoolean;
 Dst32: PLongWord;
begin
 if FTileSet <> nil then
 begin
  Info := TPasteBuffer.Create(777);
  try
   Info.Width := Rect.Right - Rect.Left;
   Info.Height := Rect.Bottom - Rect.Top;
   SetLength(Info.Layers, 1);
   with Info.Layers[0] do
   begin
    lFormat.lfTileSet := FTileSet;
    lFormat.lfIndexMask := $7FFFFFFF;
    lFormat.lfIndexShift := 0;
    lFormat.lfPaletteIndexShift := -1;
    lFormat.lfPriorityShift := -1;
    lFormat.lfXFlipShift := -1;
    lFormat.lfYFlipShift := -1;
    lFormat.lfCellSize := 4;

    GetMem(lData, Info.Width * 4 * Info.Height);


    Src := Rect.Top * Sender.MapWidth + Rect.Left;
    SB := Addr(PByteArray(SelectionBuffer)[Src]);
    RowStride := TileSetView.MapWidth - Info.Width;

    Dst32 := lData;
    FTileSet.MaxCount := FTileSet.TilesCount;
    FTileSet.MaxIndex := FTileSet.LastIndex;
    ETI := FTileSet.EmptyTileIndex;

    for Y := 0 to Info.Height - 1 do
    begin
     for X := 0 to Info.Width - 1 do
     begin
      if SB^ then
       Dst32^ := Src else
       Dst32^ := ETI;
      Inc(Src);
      Inc(SB);
      Inc(Dst32);
     end;
     Inc(Src, RowStride);
     Inc(SB, RowStride);
    end;
   end;
   Data := Info;
  except
   Info.Free;
   raise;
  end;
 end
end;

procedure TTilesFrame.TileSetViewGetBufCellState(
  Sender: TCustomTileMapView; Data: Pointer; CellX, CellY: Integer;
  var Dirty: Boolean);
var
 Info: TPasteBuffer absolute Data;
begin
 if TObject(Data) is TPasteBuffer then
 case Info.ID of
  777:
  with Info.Layers[0], lFormat do
   if Assigned(lData) and Assigned(lfTileSet) then
    Dirty := lfTileSet.Indexed[TileIndexRead(lFormat,
    PByteArray(lData)[(CellY * Info.Width + CellX) * lfCellSize])] <>
     lfTileSet.EmptyTile;
  776, 888: 
   Dirty := Info.Map[CellY * Info.Width + CellX] >= 0;
 end;
end;

procedure TTilesFrame.TileSetViewGetCellState(Sender: TCustomTileMapView;
  CellX, CellY: Integer; var Dirty: Boolean);
begin
 if FTileSet <> nil then
 begin
  Dirty := FTileSet.Indexed[CellY * TileSetView.MapWidth +
                                 CellX] <> FTileSet.EmptyTile;
  if Dirty then Exit;
 end;
end;

procedure TTilesFrame.TileSetViewPasteBufferApply(
  Sender: TCustomTileMapView; CellX, CellY: Integer; Data: Pointer);
var
 Info: TPasteBuffer absolute Data;
 I, X, W, Y, TSZ, BC, CSZ: Integer;
 SrcStride, DstStride, Dst: Integer;
 Src: PByte;
 Src16: PSmallInt absolute Src;
 Src32: PLongWord absolute Src;
 TilePtr: PByte;
 TileSrc, TileDst: TTileItem;
 Cvt: TBitmapConverter;
 SavePtr: Pointer;
 Bits8: Boolean;
begin
 if (FTileSet <> nil) and (TObject(Data) is TPasteBuffer) then
 begin
  if Info.ID = 777 then
  begin
   with Info.Layers[0]  do
    if lData <> nil then
    begin
     CSZ := lFormat.lfCellSize;
     Src := lData;
    end else
     Exit;
  end else
  if Info.TileSet <> nil then
  begin
   CSZ := 1;
   Src := nil;
  end else
   Exit;

  SrcStride := Info.Width * CSZ;
  DstStride := TileSetView.MapWidth;
  W := Info.Width;

  Dst := 0;
  if CellX < 0 then
  begin
   Dec(Src, CellX * CSZ);
   Inc(W, CellX);
  end else
   Inc(Dst, CellX);

  X := CellX + Info.Width;
  if X > TileSetView.MapWidth then
   Dec(W, X - TileSetView.MapWidth);
  if W > 0 then
  begin
   if CellY < 0 then
    Dec(Src, (CellY * Info.Width) * CSZ);
   Y := Max(0, CellY);
   Inc(Dst, Y * DstStride);
   Dec(DstStride, W);
   Dec(SrcStride, W * CSZ);
   TSZ := FTileSet.TileSize;
   if Info.ID = 777 then
   begin
    with Info.Layers[0] do
    begin
     for Y := Y to Min(TileSetView.MapHeight, CellY + Info.Height) - 1 do
     begin
      for X := 0 to W - 1 do
      begin
       TileSrc := FTileSet.Indexed[TileIndexRead(lFormat, Src^)];
       if TileSrc <> FTileSet.EmptyTile then
       begin
        TileDst := FTileSet.AddFixed(Dst);
        Move(TileSrc.TileData^, TileDst.TileData^, TSZ);
        for I := 0 to FTileSet.AttrCount - 1 do
         TileDst.Attributes[I] := TileSrc.Attributes[I];
        TileDst.FirstColor := TileSrc.FirstColor;
       end;
       Inc(Src, lFormat.lfCellSize);
       Inc(Dst);
      end;
      Inc(Src, SrcStride);
      Inc(Dst, DstStride);
     end;
    end;
   end else
   with Info.TileSet do
   begin
    BC := TileBitmap.BitsCount;
    Bits8 := (BC = 8) and (BC = FTileSet.TileBitmap.BitsCount);
    Cvt := TBitmapConverter.Create;
    try
     FillConverter(Cvt, FTileSet.TileBitmap.FlagsList, DRAW_PIXEL_MODIFY);

     for Y := Y to Min(TileSetView.MapHeight, CellY + Info.Height) - 1 do
     begin
      for X := 0 to W - 1 do
      begin
       TileSrc := Indexed[Info.Map[Integer(Src)]];

       if TileSrc <> nil then
       begin
        TileDst := FTileSet.AddFixed(Dst);

        if not Bits8 then
        begin
         TileDst.FirstColor := TileSrc.FirstColor;
         if BC = 8 then
          TileSrc.FirstColor := -TileSrc.FirstColor;
        end else
         TileDst.FirstColor := 0;

        TileBitmap.ImageData := TileSrc.TileData;
        TileDst.Load(TileBitmap, 0, 0, Cvt);
        if not Bits8 and (BC = 8) then
         TileSrc.FirstColor := -TileSrc.FirstColor;

        TileDst.AssignAttributes(TileSrc);         
       end;

       Inc(Src);
       Inc(Dst);
      end;
      Inc(Src, SrcStride);
      Inc(Dst, DstStride);
     end;
    finally
     Cvt.Free;
    end;
   end;
  end;
 end;
end;

procedure TTilesFrame.TileSetViewMakePasteBufferCopy(
  Sender: TCustomTileMapView; Source: Pointer; var Dest: Pointer);
begin
 Dest := MakePasteBufferCopy(Sender, FTileSet, Source);
end;

procedure TTilesFrame.TileSetViewReleasePasteBuffer(
  Sender: TCustomTileMapView; Data: Pointer);
begin
 TObject(Data).Free;
end;

procedure TTilesFrame.SetTileSetModified(Value: Boolean);
begin
 FTileSetModified := Value;
 if Value then TileSet := FTileSet;
end;

procedure TTilesFrame.TileSetViewEnter(Sender: TObject);
begin
 TileSetView.SelectionChanged;
end;

procedure TTilesFrame.TileSetViewAfterPaint(Sender: TCustomTileMapView);
begin
 FreeAndNil(FCvt1);
 FreeAndNil(FCvt2);
end;

end.
