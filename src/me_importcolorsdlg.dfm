object ImportColorsDlg: TImportColorsDlg
  Left = 40
  Top = 164
  BorderStyle = bsDialog
  Caption = 'Import Colors'
  ClientHeight = 292
  ClientWidth = 468
  Color = clBtnFace
  ParentFont = True
  OldCreateOrder = True
  Position = poDesktopCenter
  OnCreate = TntFormCreate
  OnDestroy = TntFormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel: TBevel
    Left = 8
    Top = 8
    Width = 454
    Height = 247
    Shape = bsFrame
  end
  object OKBtn: TTntButton
    Left = 13
    Top = 261
    Width = 75
    Height = 25
    Caption = 'OK'
    ModalResult = 1
    TabOrder = 0
  end
  object CancelBtn: TTntButton
    Left = 93
    Top = 261
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 1
  end
  object PalPropEditor: TPropertyEditor
    Tag = 4
    Left = 16
    Top = 16
    Width = 215
    Height = 229
    BoldChangedValues = False
    ButtonFillMode = fmShaded
    ScrollBarOptions.ScrollBars = ssVertical
    TabOrder = 2
    PropertyWidth = 105
    ValueWidth = 106
    OnEnter = PalPropEditorEnter
    OnExit = PalPropEditorExit
    OnEllipsisClick = PalPropEditorEllipsisClick
    Columns = <
      item
        Margin = 0
        Options = [coEnabled, coParentBidiMode, coParentColor, coResizable, coVisible, coFixed]
        Position = 0
        Width = 105
      end
      item
        Margin = 0
        Options = [coEnabled, coParentBidiMode, coParentColor, coResizable, coVisible, coFixed]
        Position = 1
        Width = 106
      end>
  end
  object ViewPanel: TPanel
    Left = 237
    Top = 16
    Width = 215
    Height = 49
    BevelInner = bvLowered
    BevelOuter = bvNone
    TabOrder = 3
    object PaletteView: TTileMapView
      Left = 1
      Top = 15
      Width = 196
      Height = 16
      MapWidth = 16
      MapHeight = 1
      TileWidth = 12
      TileHeight = 12
      OffsetX = 0
      OffsetY = 0
      GridStyle = psSolid
      GridMode = pmMask
      DragScrollShiftState = []
      ScrollBarsAllwaysVisible = False
      OnDrawCell = PaletteViewDrawCell
      OnSelectionChanged = PaletteViewSelectionChanged
      Align = alClient
      DragCursor = 983
      TabOrder = 0
      OnMouseMove = PaletteViewMouseMove
    end
    object VScrollBar: TScrollBar
      Left = 197
      Top = 15
      Width = 17
      Height = 16
      Align = alRight
      Kind = sbVertical
      PageSize = 0
      TabOrder = 1
      TabStop = False
      OnChange = ScrollBarChange
      OnScroll = VScrollBarScroll
    end
    object StatusBar: TTntStatusBar
      Left = 1
      Top = 1
      Width = 213
      Height = 14
      Align = alTop
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clBtnText
      Font.Height = -9
      Font.Name = 'Small Fonts'
      Font.Style = []
      Panels = <
        item
          Width = 28
        end
        item
          Width = 90
        end
        item
          Width = 95
        end
        item
          Width = 50
        end>
      UseSystemFont = False
    end
    object HScrollBarPanel: TPanel
      Left = 1
      Top = 31
      Width = 213
      Height = 17
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 3
      object HScrollBar: TScrollBar
        Left = 0
        Top = 0
        Width = 196
        Height = 17
        PageSize = 0
        TabOrder = 0
        TabStop = False
        OnChange = ScrollBarChange
        OnScroll = HScrollBarScroll
      end
    end
  end
  object OpenDialog: TTntOpenDialog
    Filter = 'All Files (*.*)|*.*'
    Options = [ofHideReadOnly, ofFileMustExist, ofEnableSizing]
    Title = 'Select file to import colors from'
    Left = 272
    Top = 136
  end
end
