object ProcessDialog: TProcessDialog
  Left = 364
  Top = 441
  BorderStyle = bsDialog
  ClientHeight = 96
  ClientWidth = 297
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnClose = TntFormClose
  OnCloseQuery = TntFormCloseQuery
  OnCreate = TntFormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object StatusText: TTntLabel
    Left = 8
    Top = 16
    Width = 281
    Height = 13
    Alignment = taCenter
    AutoSize = False
  end
  object ProgressBar: TGauge
    Left = 8
    Top = 40
    Width = 281
    Height = 19
    Color = clWhite
    ParentColor = False
    Progress = 0
  end
  object CancelButton: TTntButton
    Left = 111
    Top = 64
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    Default = True
    TabOrder = 0
    OnClick = CancelButtonClick
  end
end
