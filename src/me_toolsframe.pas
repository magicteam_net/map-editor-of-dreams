unit me_toolsframe;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, me_palfrm, StdCtrls, TntStdCtrls, ExtCtrls, ActnList,
  TntActnList, ImgList, ComCtrls, TntComCtrls, ToolWin, VirtualTrees,
  PropEditor;

type
  TToolsFrame = class(TFrame)
    HistoryListBox: TListBox;
    HistoryPanel: TPanel;
    HistoryLabel: TTntLabel;
    PaletteFrame: TPaletteFrame;
    ColorTablesListBoxPanel: TPanel;
    ColorTablesLabel: TTntLabel;
    ColorTablesListBox: TListBox;
    ColorTablesToolBar: TTntToolBar;
    TileSetNewToolBtn: TTntToolButton;
    TileSetUpToolBtn: TTntToolButton;
    TileSetDownToolBtn: TTntToolButton;
    TileSetDeleteToolBtn: TTntToolButton;
    sep1: TTntToolButton;
    DrawingToolsBar: TTntToolBar;
    ToolSingleSelectButton: TTntToolButton;
    ToolMultiSelectButton: TTntToolButton;
    ToolEllipseButton: TTntToolButton;
    ToolEraserButton: TTntToolButton;
    ToolPolygon: TTntToolButton;
    ToolPickerButton: TTntToolButton;
    ToolRectangleButton: TTntToolButton;
    ToolBrushButton: TTntToolButton;
    ToolPencilButton: TTntToolButton;
    ToolMagicWandButton: TTntToolButton;
    ToolFloodFillButton: TTntToolButton;
    ToolLineButton: TTntToolButton;
    Splitter1: TSplitter;
    Splitter2: TSplitter;
    ColorTablePanel: TPanel;
    FileEditToolBar: TTntToolBar;
    FileNewButton: TTntToolButton;
    FileSaveButton: TTntToolButton;
    EditPasteButton: TTntToolButton;
    FileOpenButton: TTntToolButton;
    EditUndoButton: TTntToolButton;
    EditCutButton: TTntToolButton;
    EditRedoButton: TTntToolButton;
    FileSaveAsButton: TTntToolButton;
    ViewZoomOutButton: TTntToolButton;
    ViewZoomInButton: TTntToolButton;
    EditClearSelectionButton: TTntToolButton;
    EditCopyButton: TTntToolButton;
    PalPropsPanel: TPanel;
    PalPropsLabel: TTntLabel;
    PalPropEditor: TPropertyEditor;
    Splitter3: TSplitter;
    ToolsPanel: TPanel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

end.
