object MapsFrame: TMapsFrame
  Left = 0
  Top = 0
  Width = 301
  Height = 395
  TabOrder = 0
  object PageControl: TTntPageControl
    Left = 0
    Top = 0
    Width = 301
    Height = 395
    ActivePage = PropsPage
    Align = alClient
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -9
    Font.Name = 'Small Fonts'
    Font.Style = []
    ParentFont = False
    TabHeight = 14
    TabOrder = 0
    TabStop = False
    OnChange = PageControlChange
    object PropsPage: TTntTabSheet
      Caption = 'Properties'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      object Splitter3: TSplitter
        Left = 0
        Top = 146
        Width = 293
        Height = 3
        Cursor = crVSplit
        Align = alTop
        AutoSnap = False
        Beveled = True
        ResizeStyle = rsUpdate
      end
      object MapsListPanel: TPanel
        Left = 0
        Top = 0
        Width = 293
        Height = 146
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object Splitter1: TSplitter
          Left = 0
          Top = 53
          Width = 293
          Height = 3
          Cursor = crVSplit
          Align = alBottom
          AutoSnap = False
          Beveled = True
          ResizeStyle = rsUpdate
        end
        object MapsListBox: TListBox
          Left = 0
          Top = 23
          Width = 293
          Height = 30
          Hint = '|Double click to open the map in central panel'
          Style = lbVirtualOwnerDraw
          Align = alClient
          ItemHeight = 13
          MultiSelect = True
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = MapsListBoxClick
          OnDataFind = MapsListBoxDataFind
          OnDblClick = MapsListBoxDblClick
          OnDrawItem = MapsListBoxDrawItem
          OnKeyDown = MapsListBoxKeyDown
        end
        object MapsToolBar: TTntToolBar
          Left = 0
          Top = 0
          Width = 293
          Height = 23
          EdgeInner = esNone
          EdgeOuter = esNone
          Flat = True
          Images = ImageList
          TabOrder = 1
          Transparent = False
          Wrapable = False
          object NewMapToolBtn: TTntToolButton
            Left = 0
            Top = 0
            Action = MapNewAction
          end
          object sep11: TTntToolButton
            Left = 23
            Top = 0
            Width = 8
            Caption = 'sep11'
            Style = tbsSeparator
          end
          object MapUpToolBtn: TTntToolButton
            Left = 31
            Top = 0
            Action = MapUpAction
          end
          object MapDownToolBtn: TTntToolButton
            Left = 54
            Top = 0
            Action = MapDownAction
          end
          object MapDeleteToolBtn: TTntToolButton
            Left = 77
            Top = 0
            Action = MapDeleteAction
          end
          object sep25: TTntToolButton
            Left = 100
            Top = 0
            Width = 8
            Style = tbsSeparator
          end
          object MapsListLabel: TTntLabel
            Left = 108
            Top = 0
            Width = 25
            Height = 22
            Caption = 'Maps'
            FocusControl = MapsListBox
            Layout = tlCenter
          end
        end
        object MapPropsPanel: TPanel
          Left = 0
          Top = 56
          Width = 293
          Height = 90
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 2
          object MapPropertiesLabel: TTntLabel
            Left = 0
            Top = 0
            Width = 293
            Height = 13
            Align = alTop
            Caption = 'Map properties'
            FocusControl = MapPropEditor
            Transparent = True
            Layout = tlCenter
          end
          object MapPropEditor: TPropertyEditor
            Left = 0
            Top = 13
            Width = 293
            Height = 77
            BoldChangedValues = False
            ButtonFillMode = fmShaded
            Align = alClient
            ScrollBarOptions.ScrollBars = ssVertical
            TabOrder = 0
            PropertyWidth = 90
            ValueWidth = 199
            Columns = <
              item
                Margin = 0
                Options = [coEnabled, coParentBidiMode, coParentColor, coResizable, coVisible, coFixed]
                Position = 0
                Width = 90
              end
              item
                Margin = 0
                Options = [coEnabled, coParentBidiMode, coParentColor, coResizable, coVisible, coFixed]
                Position = 1
                Width = 199
              end>
          end
        end
      end
      object LayerListPanel: TPanel
        Left = 0
        Top = 149
        Width = 293
        Height = 222
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object Splitter2: TSplitter
          Left = 0
          Top = 135
          Width = 293
          Height = 3
          Cursor = crVSplit
          Align = alBottom
          AutoSnap = False
          Beveled = True
          ResizeStyle = rsUpdate
        end
        object LayersListBox: TLayerListBox
          Left = 0
          Top = 23
          Width = 293
          Height = 112
          OnGetItemClickMode = LayersListBoxGetItemClickMode
          OnDrawPic = LayersListBoxDrawPic
          OnItemClick = LayersListBoxItemClick
          OnUnicodeGetData = LayersListBoxUnicodeGetData
          Style = lbVirtualOwnerDraw
          Align = alClient
          MultiSelect = True
          TabOrder = 0
          OnClick = LayersListBoxClick
          OnDataFind = LayersListBoxDataFind
          OnDblClick = LayersListBoxDblClick
          OnKeyDown = LayersListBoxKeyDown
        end
        object LayersToolBar: TTntToolBar
          Left = 0
          Top = 0
          Width = 293
          Height = 23
          EdgeInner = esNone
          EdgeOuter = esNone
          Flat = True
          Images = ImageList
          TabOrder = 1
          Transparent = False
          Wrapable = False
          object NewLayerToolBtn: TTntToolButton
            Left = 0
            Top = 0
            Action = LayerNewAction
          end
          object LayersToolSep: TTntToolButton
            Left = 23
            Top = 0
            Width = 8
            Caption = 'LayersToolSep'
            Style = tbsSeparator
          end
          object LayerUpToolBtn: TTntToolButton
            Left = 31
            Top = 0
            Action = LayerUpAction
          end
          object LayerDownToolBtn: TTntToolButton
            Left = 54
            Top = 0
            Action = LayerDownAction
          end
          object LayerDeleteToolBtn: TTntToolButton
            Left = 77
            Top = 0
            Action = LayerDeleteAction
          end
          object sep99: TTntToolButton
            Left = 100
            Top = 0
            Width = 8
            Style = tbsSeparator
          end
          object LayersListLabel: TTntLabel
            Left = 108
            Top = 0
            Width = 52
            Height = 22
            Caption = 'Map layers'
            FocusControl = LayersListBox
            Layout = tlCenter
          end
        end
        object Panel1: TPanel
          Left = 0
          Top = 138
          Width = 293
          Height = 84
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 2
          object LayersLabel: TTntLabel
            Left = 0
            Top = 0
            Width = 293
            Height = 13
            Align = alTop
            Caption = 'Layer properties'
            FocusControl = LayerPropEditor
            Transparent = True
            Layout = tlCenter
          end
          object LayerPropEditor: TPropertyEditor
            Tag = 1
            Left = 0
            Top = 13
            Width = 293
            Height = 71
            BoldChangedValues = False
            ButtonFillMode = fmShaded
            Align = alClient
            ScrollBarOptions.ScrollBars = ssVertical
            TabOrder = 0
            PropertyWidth = 120
            ValueWidth = 169
            Columns = <
              item
                Margin = 0
                Options = [coEnabled, coParentBidiMode, coParentColor, coResizable, coVisible, coFixed]
                Position = 0
                Width = 120
              end
              item
                Margin = 0
                Options = [coEnabled, coParentBidiMode, coParentColor, coResizable, coVisible, coFixed]
                Position = 1
                Width = 169
              end>
          end
        end
      end
    end
    object VisualPage: TTntTabSheet
      Caption = 'Visualization'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -9
      Font.Name = 'Small Fonts'
      Font.Style = []
      ParentFont = False
      OnShow = VisualPageShow
      object MapsComboBox: TTntComboBox
        Left = 0
        Top = 0
        Width = 293
        Height = 21
        Align = alTop
        Style = csDropDownList
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ItemHeight = 13
        ParentFont = False
        TabOrder = 0
        OnChange = MapsComboBoxChange
        OnKeyDown = MapsListBoxKeyDown
      end
      inline MapFrame: TMapFrame
        Left = 0
        Top = 21
        Width = 293
        Height = 350
        Align = alClient
        AutoScroll = False
        Color = clBtnFace
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Small Fonts'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 1
        inherited MapStatusBar: TTntStatusBar
          Width = 293
        end
        inherited MapEditView: TTileMapView
          Width = 293
          Height = 336
          OnBeforeContentChange = MapFrameMapEditViewBeforeContentChange
          Font.Height = -11
          Font.Name = 'Tahoma'
          ParentFont = False
        end
      end
    end
  end
  object ActionList: TTntActionList
    Left = 216
    Top = 112
    object MapNewAction: TTntAction
      Category = 'Maps'
      Caption = '&New Map'
      Hint = 'New Map|Adds a new map to the project'
      ImageIndex = 5
      OnExecute = MapNewActionExecute
      OnUpdate = MapNewActionUpdate
    end
    object MapUpAction: TTntAction
      Category = 'Maps'
      Caption = 'Move &Up'
      Hint = 'Move Up|Moves all selected maps up the list'
      ImageIndex = 1
      OnExecute = MapUpActionExecute
      OnUpdate = MapUpActionUpdate
    end
    object MapDownAction: TTntAction
      Category = 'Maps'
      Caption = 'Move &Down'
      Hint = 'Move Down|Moves all selected maps down the list'
      ImageIndex = 2
      OnExecute = MapDownActionExecute
      OnUpdate = MapDownActionUpdate
    end
    object MapDeleteAction: TTntAction
      Category = 'Maps'
      Caption = 'D&elete'
      Hint = 'Delete|Deletes all selected maps from the project'
      ImageIndex = 0
      OnExecute = MapDeleteActionExecute
      OnUpdate = MapDeleteActionUpdate
    end
    object BrushNewAction: TTntAction
      Category = 'Brushes'
      Caption = '&New Brush'
      Hint = 'New Brush|Adds a new empty brush to the project'
      ImageIndex = 9
      OnExecute = MapNewActionExecute
      OnUpdate = MapNewActionUpdate
    end
    object BrushUpAction: TTntAction
      Category = 'Brushes'
      Caption = 'Move &Up'
      Hint = 'Move Up|Moves all selected brushes up the list'
      ImageIndex = 1
      OnExecute = MapUpActionExecute
      OnUpdate = MapUpActionUpdate
    end
    object BrushDownAction: TTntAction
      Category = 'Brushes'
      Caption = 'Move &Down'
      Hint = 'Move Down|Moves all selected brushes down the list'
      ImageIndex = 2
      OnExecute = MapDownActionExecute
      OnUpdate = MapDownActionUpdate
    end
    object BrushDeleteAction: TTntAction
      Category = 'Brushes'
      Caption = 'D&elete'
      Hint = 'Delete|Deletes all selected brushes from the project'
      ImageIndex = 0
      OnExecute = MapDeleteActionExecute
      OnUpdate = MapDeleteActionUpdate
    end
    object LayerNewAction: TTntAction
      Category = 'Layers'
      Caption = '&New Layer'
      Hint = 'New Layer|Adds a new empty layer to the map'
      ImageIndex = 6
      OnExecute = LayerNewActionExecute
      OnUpdate = LayerNewActionUpdate
    end
    object LayerUpAction: TTntAction
      Category = 'Layers'
      Caption = 'Move &Up'
      Hint = 'Move Up|Moves all selected layers up the list'
      ImageIndex = 1
      OnExecute = LayerUpActionExecute
      OnUpdate = LayerUpActionUpdate
    end
    object LayerDownAction: TTntAction
      Category = 'Layers'
      Caption = 'Move &Down'
      Hint = 'Move Down|Moves all selected layers down the list'
      ImageIndex = 2
      OnExecute = LayerDownActionExecute
      OnUpdate = LayerDownActionUpdate
    end
    object LayerDeleteAction: TTntAction
      Category = 'Layers'
      Caption = 'D&elete'
      Hint = 'Delete|Deletes all selected layers'
      ImageIndex = 0
      OnExecute = LayerDeleteActionExecute
      OnUpdate = LayerDeleteActionUpdate
    end
    object BrushAddFromFileAction: TTntAction
      Category = 'Brushes'
      Caption = '&Add From...'
      Hint = 'Add From|Loads brushes from files and adds them to the project'
      ImageIndex = 36
      OnExecute = MapAddFromFileActionExecute
      OnUpdate = MapNewActionUpdate
    end
    object BrushSaveToFileAction: TTntAction
      Category = 'Brushes'
      Caption = '&Save To...'
      Hint = 'Save To|Saves all selected brushes to disk'
      ImageIndex = 38
      OnExecute = MapSaveToFileActionExecute
      OnUpdate = MapSaveToFileActionUpdate
    end
    object MapAddFromFileAction: TTntAction
      Category = 'Maps'
      Caption = '&Add From...'
      Hint = 'Add From|Loads maps from file and adds them to the project'
      ImageIndex = 36
      OnExecute = MapAddFromFileActionExecute
      OnUpdate = MapNewActionUpdate
    end
    object MapSaveToFileAction: TTntAction
      Category = 'Maps'
      Caption = '&Save To...'
      Hint = 'Save To|Saves all selected maps to disk'
      ImageIndex = 38
      OnExecute = MapSaveToFileActionExecute
      OnUpdate = MapSaveToFileActionUpdate
    end
  end
  object ImageList: TImageList
    Left = 104
    Top = 104
  end
  object OpenDialog: TTntOpenDialog
    DefaultExt = 'cmp'
    Filter = 'Customizable Maps (*.cmp)|*.cmp'
    Options = [ofHideReadOnly, ofFileMustExist, ofEnableSizing]
    Title = 'Open Maps'
    Left = 32
    Top = 96
  end
  object SaveDialog: TTntSaveDialog
    DefaultExt = 'cmp'
    Filter = 'Customizable Maps (*.cmp)|*.cmp'
    Options = [ofOverwritePrompt, ofHideReadOnly, ofEnableSizing]
    Title = 'Save Maps'
    Left = 160
    Top = 96
  end
end
