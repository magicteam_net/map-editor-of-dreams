object TilesFrame: TTilesFrame
  Left = 0
  Top = 0
  Width = 443
  Height = 269
  Align = alClient
  AutoScroll = False
  Color = clBtnFace
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -9
  Font.Name = 'Small Fonts'
  Font.Style = []
  ParentBackground = False
  ParentColor = False
  ParentFont = False
  TabOrder = 0
  object StatusBar: TTntStatusBar
    Left = 0
    Top = 0
    Width = 443
    Height = 14
    Align = alTop
    Panels = <
      item
        Text = 'Tile set'
        Width = 40
      end
      item
        Width = 50
      end
      item
        Width = 40
      end
      item
        Width = 40
      end
      item
        Width = 65
      end
      item
        Width = 50
      end>
    ParentColor = True
    ParentFont = True
    UseSystemFont = False
  end
  object TileSetView: TTileMapView
    Left = 0
    Top = 14
    Width = 443
    Height = 255
    MapWidth = 0
    MapHeight = 0
    TileWidth = 32
    TileHeight = 32
    OffsetX = 0
    OffsetY = 0
    ShowGrid = False
    GridStyle = psSolid
    SelectStyle = psSolid
    OnDrawCell = TileSetViewDrawCell
    OnSelectionChanged = TileSetViewSelectionChanged
    OnAfterPaint = TileSetViewAfterPaint
    OnGetCellState = TileSetViewGetCellState
    OnGetBufCellState = TileSetViewGetBufCellState
    OnCopyMapData = TileSetViewCopyMapData
    OnPasteBufferApply = TileSetViewPasteBufferApply
    OnClearCell = TileSetViewClearCell
    OnBufClearCell = TileSetViewBufClearCell
    OnReleasePasteBuffer = TileSetViewReleasePasteBuffer
    OnCheckCompatibility = TileSetViewCheckCompatibility
    OnMakePasteBufferCopy = TileSetViewMakePasteBufferCopy
    Align = alClient
    Constraints.MinHeight = 32
    DragCursor = 983
    TabOrder = 1
    OnEnter = TileSetViewEnter
    OnMouseMove = TileSetViewMouseMove
    OnMouseWheelDown = TileSetViewMouseWheelDown
    OnMouseWheelUp = TileSetViewMouseWheelUp
  end
end
